import logging
from collections.abc import Awaitable
from typing import (
    cast,
    no_type_check,
)

from starlette import (
    status,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    HTMLResponse,
    JSONResponse,
    RedirectResponse,
    Response,
)
from starlette.routing import (
    Mount,
    Route,
)
from starlette.staticfiles import (
    StaticFiles,
)
from starlette.templating import (
    Jinja2Templates,
)

from integrates.audit import AuditContext, add_audit_context
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    BASE_URL,
    FI_ENVIRONMENT,
    STARTDIR,
)
from integrates.custom_exceptions import InvalidAuthorization
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    jira_installations as jira_installations_model,
)
from integrates.db_model import (
    organizations as organizations_model,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.jira_installations.types import (
    UserJiraSecurityInstallRequest,
)
from integrates.db_model.organizations.types import OrganizationMetadataToUpdate
from integrates.db_model.stakeholders.types import StakeholderMetadataToUpdate
from integrates.jira_security_integration import (
    utils,
)
from integrates.organization_access.domain import get_stakeholder_organizations_ids
from integrates.sessions import (
    domain as sessions_domain,
)

INTEGRATION_URL = "https://localhost:8001" if FI_ENVIRONMENT == "development" else BASE_URL
TEMPLATES_DIR: str = f"{STARTDIR}/integrates/back/integrates/" "jira_security_integration/templates"
TEMPLATING_ENGINE = Jinja2Templates(directory=TEMPLATES_DIR)
LOGO_URL = (
    "https://res.cloudinary.com/fluid-attacks/image/upload/v1707416305/"
    "integrates/login/sidebarLogo.svg"
)
NO_AUTH_HEADER_ERROR = "Invalid or missing authorization header"
TRANSACTIONAL_LOGGER = logging.getLogger("transactional")


async def jira_associate(_request: Request) -> Response:
    add_audit_context(AuditContext(mechanism="JIRA"))
    return Response(status_code=status.HTTP_204_NO_CONTENT)


async def jira_user_associate(_request: Request) -> Response:
    form = await _request.form()
    token = form.get("token")
    jwt = str(form.get("jwt"))
    base_url = form.get("base_url")
    if isinstance(token, str) and not utils.is_valid_jwt(token=token):
        return RedirectResponse(
            url=f"/jira-security/settings?jwt={jwt}&xdm_e={base_url}&error=invalid_jwt",
            status_code=303,
        )
    try:
        decoded_token: Item = sessions_domain.decode_token(token=str(token))
    except InvalidAuthorization:
        return RedirectResponse(
            url=f"/jira-security/settings?jwt={jwt}&xdm_e={base_url}&error=invalid_jwt",
            status_code=303,
        )
    user_email: str = decoded_token["user_email"]
    add_audit_context(AuditContext(author=user_email, mechanism="JIRA"))

    if token is not None:
        TRANSACTIONAL_LOGGER.info(
            "Accepted Jira Conditions",
            extra={
                "extra": {
                    "user_email": str(user_email),
                    "used_token": str(token),
                    "install_base_url": str(base_url),
                    "install_jwt": str(jwt),
                }
            },
        )
        await utils.add_association(api_token=str(token), jira_jwt=jwt, base_url=str(base_url))
        await utils.bulk_workspaces(jira_jwt=jwt, base_url=str(base_url))
        await batch_dal.put_action(
            action=Action.BULK_JIRA_VULNERABILITIES,
            attempt_duration_seconds=7200,
            subject=str(user_email),
            additional_info={  # type: ignore
                "jira_jwt": jwt,
                "base_url": str(base_url),
            },
            queue=IntegratesBatchQueue.SMALL,
            entity="jira",
        )
    return RedirectResponse(
        url=f"/jira-security/settings?jwt={jwt}&xdm_e={base_url}",
        status_code=303,
    )


async def jira_disassociate(_request: Request) -> Response:
    add_audit_context(AuditContext(mechanism="JIRA"))
    return Response(status_code=status.HTTP_204_NO_CONTENT)


async def jira_install_response(_request: Request) -> Response:
    add_audit_context(AuditContext(mechanism="JIRA"))
    body = await cast(Awaitable[dict[str, str]], _request.json())
    await utils.add_install(body)
    await jira_installations_model.update_shared_secret(
        client_key=body["clientKey"],
        base_url=body["baseUrl"],
        shared_secret=body["sharedSecret"],
    )
    return Response(status_code=status.HTTP_200_OK)


@no_type_check
async def jira_containers_response(_request: Request) -> JSONResponse:
    add_audit_context(AuditContext(mechanism="JIRA"))
    loaders: Dataloaders = get_new_context()
    token = ""  # nosec CWE-259
    auth_header = _request.headers.get("authorization")

    if auth_header and auth_header.startswith("JWT "):
        token = auth_header[4:]

    url = str(_request.url)
    method = str(_request.method)
    valid_jwt = await utils.check_request_jwt(jwt=token, url=url, method=method)

    if not valid_jwt:
        return Response(status_code=status.HTTP_401_UNAUTHORIZED)

    if _request.method == "POST":
        body = await cast(Awaitable[dict[str, str]], _request.json())
        post_ids = (id for id in body["ids"])
        post_groups = await loaders.group.load_many(post_ids)
        post_containers = [
            {
                "id": group.name,
                "name": group.name,
                "url": (
                    f"{INTEGRATION_URL}/orgs"
                    f"/{await utils.jira_get_organization_name(group.organization_id)}/groups"
                    f"/{group.name}/vulns"
                ),
                "avatarUrl": LOGO_URL,
                "lastUpdatedDate": "null",
            }
            for group in post_groups
        ]

        response_data_post: dict[str, list[str]] = {"containers": post_containers}
        return JSONResponse(response_data_post)

    organization_id = f"ORG#{_request.query_params.get('workspaceId')}"
    organization = await loaders.organization.load(organization_id)
    org_groups = await loaders.organization_groups.load(organization_id)

    installs = await loaders.user_jira_install.load(
        UserJiraSecurityInstallRequest(
            client_key=utils.get_iss_from_jwt(jwt_token=token), limit=None
        ),
    )

    base_url = installs[0].base_url
    user_email = await utils.check_installation_associated_email(jwt=token, base_url=base_url)
    access_groups = await loaders.stakeholder_groups_access.load(user_email)
    groups = await loaders.group.load_many(
        [group.group_name for group in access_groups if group.state.role == "group_manager"]
    )

    groups = [group for group in org_groups if group in groups]

    containers = [
        {
            "id": group.name,
            "name": group.name,
            "url": f"{INTEGRATION_URL}/orgs/{organization.name}/groups/{group.name}/vulns",
            "avatarUrl": LOGO_URL,
            "lastUpdatedDate": "null",
        }
        for group in groups
    ]

    response_data: dict[str, list[str]] = {"containers": containers}
    return JSONResponse(response_data)


@no_type_check
async def jira_uninstall_response(_request: Request) -> Response:
    add_audit_context(AuditContext(mechanism="JIRA"))
    loaders: Dataloaders = get_new_context()
    body = await cast(Awaitable[dict[str, str]], _request.json())
    auth_header = _request.headers.get("authorization")
    if auth_header is None or not auth_header.startswith("JWT "):
        return JSONResponse(
            content={"error": NO_AUTH_HEADER_ERROR},
            status_code=status.HTTP_401_UNAUTHORIZED,
        )

    jwt = auth_header.split("JWT ")[1]
    user_email = await utils.check_installation_associated_email(jwt=jwt, base_url=body["baseUrl"])
    await utils.delete_bulk_vulnerabilities(body=body)
    await utils.delete_bulk_workspaces(body=body)
    if user_email is not None:
        await stakeholders_model.update_metadata(
            metadata=StakeholderMetadataToUpdate(
                jira_client_key="",
            ),
            email=user_email,
        )
    access_orgs = await get_stakeholder_organizations_ids(loaders, user_email, True)

    for org in await loaders.organization.load_many(access_orgs):
        if org is not None:
            await organizations_model.update_metadata(
                metadata=OrganizationMetadataToUpdate(
                    jira_associated_id="",
                ),
                organization_id=org.id,
                organization_name=org.name,
            )
    await jira_installations_model.remove_jira_installation(
        client_key=body["clientKey"],
    )
    return Response(status_code=status.HTTP_200_OK)


@no_type_check
async def jira_workspaces_response(request: Request) -> JSONResponse:
    add_audit_context(AuditContext(mechanism="JIRA"))
    loaders: Dataloaders = get_new_context()
    auth_header = request.headers.get("Authorization")
    if auth_header is None:
        return JSONResponse(
            content={"error": NO_AUTH_HEADER_ERROR},
            status_code=status.HTTP_401_UNAUTHORIZED,
        )
    if not auth_header.startswith("JWT "):
        return JSONResponse(
            content={"error": NO_AUTH_HEADER_ERROR},
            status_code=status.HTTP_401_UNAUTHORIZED,
        )

    jwt = auth_header.split("JWT ")[1]
    installs = await loaders.user_jira_install.load(
        UserJiraSecurityInstallRequest(client_key=utils.get_iss_from_jwt(jwt), limit=None)
    )
    base_url = installs[0].base_url

    url = str(request.url)
    method = str(request.method)
    valid_jwt = await utils.check_request_jwt(jwt=jwt, url=url, method=method)

    if not valid_jwt:
        Response(status_code=status.HTTP_401_UNAUTHORIZED)

    user_email = await utils.check_installation_associated_email(jwt=jwt, base_url=base_url)

    access_orgs = await get_stakeholder_organizations_ids(loaders, user_email, True)

    orgs = list(await loaders.organization.load_many(access_orgs))

    workspaces = [
        {
            "id": org.id.replace("ORG#", ""),
            "name": org.name,
            "url": f"{INTEGRATION_URL}/orgs/{org.name}/groups",
            "avatarUrl": LOGO_URL,
        }
        for org in orgs
    ]

    response_data: dict[str, list[str]] = {"workspaces": workspaces}

    return JSONResponse(response_data)


@no_type_check
async def jira_settings_response(_request: Request) -> HTMLResponse:
    add_audit_context(AuditContext(mechanism="JIRA"))
    form = await _request.form()
    jwt = (
        form.get("jwt")
        if _request.query_params.get("jwt") is None
        else _request.query_params.get("jwt")
    )
    base_url = _request.query_params.get("xdm_e")
    error = _request.query_params.get("error")
    associated_email = await utils.check_installation_associated_email(
        jwt=str(jwt), base_url=str(base_url)
    )
    url = str(_request.url)
    method = _request.method

    if error == "invalid_jwt":
        return TEMPLATING_ENGINE.TemplateResponse(
            name="settings_add_token.html",
            context={
                "jwt": jwt,
                "base_url": base_url,
                "error": "invalid_jwt",
            },
            request=_request,
        )

    if associated_email == "":
        check_jwt = await utils.check_request_jwt(
            jwt,
            url,
            method=method,
        )

        if check_jwt:
            return TEMPLATING_ENGINE.TemplateResponse(
                name="settings_add_token.html",
                context={
                    "jwt": jwt,
                    "base_url": base_url,
                },
                request=_request,
            )
        return Response(status_code=status.HTTP_401_UNAUTHORIZED)
    return TEMPLATING_ENGINE.TemplateResponse(
        name="settings.html",
        context={
            "associated_email": associated_email,
            "jwt": jwt,
            "base_url": base_url,
        },
        request=_request,
    )


ROUTES = [
    Mount(
        "/jira-security/static",
        StaticFiles(directory=f"{TEMPLATES_DIR}/static"),
        name="jira_security_static",
    ),
    Route("/user_associate", jira_user_associate, methods=["POST"]),
    Route("/associate", jira_associate, methods=["PUT"]),
    Route("/containers", jira_containers_response, methods=["GET", "POST"]),
    Route("/disassociate", jira_disassociate, methods=["PUT"]),
    Route("/install", jira_install_response, methods=["POST"]),
    Route("/settings", jira_settings_response),
    Route("/uninstall", jira_uninstall_response, methods=["POST"]),
    Route("/workspaces", jira_workspaces_response, methods=["POST"]),
]
