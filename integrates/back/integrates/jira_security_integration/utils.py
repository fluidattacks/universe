import base64
import json
import logging
import re
from contextlib import suppress
from datetime import (
    datetime,
)
from typing import (
    no_type_check,
)

import fluidattacks_core.http
from atlassian_jwt import (
    encode_token,
    url_utils,
)
from jwcrypto.jwk import (
    JWK,
)
from jwcrypto.jws import (
    InvalidJWSObject,
    InvalidJWSSignature,
)
from jwcrypto.jwt import (
    JWT,
    JWTExpired,
)
from more_itertools import (
    chunked,
    flatten,
)

from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    BASE_URL,
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import JiraInstallAlreadyCreated
from integrates.custom_utils import (
    findings as findings_utils,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    jira_installations,
)
from integrates.db_model import (
    organizations as organizations_model,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.jira_installations.types import (
    JiraInstallState,
    JiraSecurityInstall,
    JiraSecurityInstallRequest,
    UserJiraSecurityInstallRequest,
)
from integrates.db_model.organizations.types import OrganizationMetadataToUpdate
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    Vulnerability,
)
from integrates.organization_access.domain import get_stakeholder_organizations_ids
from integrates.sessions import (
    domain as sessions_domain,
)

CONTENT_TYPE = "application/json"
INTEGRATION_URL = "https://localhost:8001" if FI_ENVIRONMENT == "development" else BASE_URL
TRANSACTIONAL_LOGGER = logging.getLogger("transactional")


def _base64_url_decode(encoded_str: str) -> str:
    padding = "=" * (4 - (len(encoded_str) % 4))
    encoded_str += padding
    return base64.urlsafe_b64decode(encoded_str).decode("utf-8")


def parse_jira_jwt_token(jwt_token: str) -> list[str]:
    encoded_segments = jwt_token.split(".")
    decoded_segments = []
    if len(encoded_segments) >= 2:
        for segment in encoded_segments[:2]:
            decoded_segments.append(_base64_url_decode(segment))
    return decoded_segments


def get_iss_from_jwt(jwt_token: str) -> str:
    decoded_segments = parse_jira_jwt_token(jwt_token)
    payload: Item = json.loads(decoded_segments[1])
    return payload.get("iss") or ""


def get_sub_from_jwt(jwt_token: str) -> str:
    decoded_segments = parse_jira_jwt_token(jwt_token)
    payload: Item = json.loads(decoded_segments[1])
    return payload.get("sub") or ""


def get_qsh_from_jwt(jwt_token: str) -> str:
    decoded_segments = parse_jira_jwt_token(jwt_token)
    payload: Item = json.loads(decoded_segments[1])
    return payload.get("qsh") or ""


async def check_installation_associated_email(jwt: str | None, base_url: str) -> str | None:
    if jwt is None:
        return ""
    loaders: Dataloaders = get_new_context()
    client_key = get_iss_from_jwt(jwt)
    base_url = base_url.lower().strip()
    install: JiraSecurityInstall | None = await loaders.jira_install.load(
        JiraSecurityInstallRequest(base_url=base_url, client_key=client_key),
    )
    if install is None:
        return ""
    return install.associated_email


async def add_association(api_token: str, jira_jwt: str, base_url: str) -> None:
    decoded_token: Item = sessions_domain.decode_token(api_token)
    user_email: str = decoded_token["user_email"]
    client_key = get_iss_from_jwt(jira_jwt)
    await jira_installations.update_jira_install_state(
        base_url=base_url,
        client_key=client_key,
        state=JiraInstallState(
            modified_by=user_email,
            modified_date=get_utc_now().isoformat(),
            used_api_token=api_token,
            used_jira_jwt=jira_jwt,
        ),
    )
    await jira_installations.update_associate_email(
        base_url=base_url,
        client_key=client_key,
        associated_email=user_email,
    )
    await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            jira_client_key=client_key,
        ),
        email=user_email,
    )


async def add_install(body: dict[str, str]) -> None:
    data = JiraSecurityInstall(
        associated_email="",
        base_url=body["baseUrl"],
        client_key=body["clientKey"],
        cloud_id=body["cloudId"],
        description=body["description"],
        display_url=body["displayUrl"],
        event_type=body["eventType"],
        installation_id=body["installationId"],
        key=body["key"],
        plugins_version=body["pluginsVersion"],
        product_type=body["productType"],
        public_key=body["publicKey"],
        server_version=body["serverVersion"],
        shared_secret=body["sharedSecret"],
        state=JiraInstallState(
            modified_by="",
            modified_date=get_utc_now().isoformat(),
            used_api_token="",
            used_jira_jwt="",
        ),
    )
    with suppress(JiraInstallAlreadyCreated):
        await jira_installations.add(jira_install=data)


async def jira_get_organization_name(organization_id: str) -> str:
    loaders: Dataloaders = get_new_context()
    organization = await loaders.organization.load(organization_id)
    if organization is None:
        return ""
    return organization.name


def is_valid_jwt(token: str) -> bool:
    jwt_pattern = r"^[A-Za-z0-9_-]+\.[A-Za-z0-9_-]+\.[A-Za-z0-9_-]+$"

    if re.match(jwt_pattern, token):
        return True
    return False


async def get_severity_text_value(vulnerability: Vulnerability) -> str:
    loaders: Dataloaders = get_new_context()
    finding = await loaders.finding.load(vulnerability.finding_id)
    if (
        finding is None
        or findings_utils.is_deleted(finding)
        or vulns_utils.is_deleted(vulnerability)
    ):
        return "unknown"
    score = vulns_utils.get_severity_threat_score(vulnerability, finding)
    if score >= 9:
        return "critical"
    if score > 6.9:
        return "high"
    if score > 3.9:
        return "medium"
    if score >= 0.1:
        return "low"
    return "unknown"


def get_type_from_technique(technique: VulnerabilityTechnique | None) -> str:
    if technique is VulnerabilityTechnique.SAST:
        return "sast"
    if technique is VulnerabilityTechnique.DAST:
        return "dast"
    if technique is VulnerabilityTechnique.SCA:
        return "sca"
    return "unknown"


async def get_finding_title(finding_id: str) -> str:
    loaders: Dataloaders = get_new_context()
    finding = await loaders.finding.load(finding_id)
    if finding is None:
        return ""
    return finding.title


async def get_platform_url(group_name: str, finding: str, vulnerability: str) -> str:
    loaders: Dataloaders = get_new_context()
    group = await loaders.group.load(group_name)
    if group is None:
        return ""
    organization = await loaders.organization.load(group.organization_id)
    if organization is None:
        return ""
    url = (
        f"{INTEGRATION_URL}/orgs/{organization.name}/groups/"
        f"{group.name}/vulns/{finding}/locations/{vulnerability}"
    )
    return url


def format_date(date: datetime) -> str:
    return date.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"


@no_type_check
async def check_request_jwt(jwt: str | None, url: str | None, method: str | None) -> bool:
    loaders: Dataloaders = get_new_context()

    if jwt is None or url is None or method is None:
        return False

    issuer = get_iss_from_jwt(jwt)
    qsh = get_qsh_from_jwt(jwt)
    installation = await loaders.user_jira_install.load(
        UserJiraSecurityInstallRequest(client_key=issuer, limit=None),
    )
    expected_qsh = url_utils.hash_url(method, url)
    if qsh != expected_qsh:
        return False

    try:
        jwt_token = JWT(jwt=jwt)
        secret = installation[0].shared_secret
        jws_key = JWK.from_password(secret)
        jwt_token.validate(jws_key)
        return True
    except (InvalidJWSObject, InvalidJWSSignature, JWTExpired):
        return False


@no_type_check
async def _get_bulked_workspace(client_key: str, base_url: str):
    loaders: Dataloaders = get_new_context()

    jira_install: JiraSecurityInstall | None = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url=base_url,
            client_key=client_key,
        )
    )

    if jira_install is None:
        return None

    url = f"{jira_install.base_url}/rest/security/1.0/linkedWorkspaces"
    path = "/rest/security/1.0/linkedWorkspaces"

    token: str = encode_token(
        http_method="GET",
        url=path,
        clientKey=jira_install.key,
        sharedSecret=jira_install.shared_secret,
    )

    headers = {
        "Authorization": f"JWT {token}",
    }

    response = await fluidattacks_core.http.request(
        method="GET", url=url, headers=headers, timeout=600
    )

    if response.ok:
        return await response.json()
    return None


async def _build_delete_vulns_request(
    base_url: str,
    client_key: str,
    workspace_id: str,
) -> tuple[str, dict[str, str]]:
    loaders = get_new_context()

    jira_install = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url=base_url,
            client_key=client_key,
        ),
    )

    if jira_install is None:
        return "", {}

    path = f"/rest/security/1.0/bulkByProperties?workspaceId={workspace_id}"
    url = f"{jira_install.base_url}{path}"

    token: str = encode_token(
        http_method="DELETE",
        url=path,
        clientKey=jira_install.key,
        sharedSecret=jira_install.shared_secret,
    )

    headers = {
        "Authorization": f"JWT {token}",
    }

    return url, headers


@no_type_check
async def delete_bulk_vulnerabilities(body: dict[str, str]) -> None:
    base_url = body["baseUrl"]
    client_key = body["clientKey"]
    workspaces = await _get_bulked_workspace(client_key=client_key, base_url=base_url)
    if workspaces is not None:
        workspace_ids = workspaces.get("workspaceIds", [])
        for workspace_id in workspace_ids:
            url, headers = await _build_delete_vulns_request(
                base_url=base_url,
                client_key=client_key,
                workspace_id=workspace_id,
            )

            await fluidattacks_core.http.request(method="DELETE", url=url, headers=headers)

            TRANSACTIONAL_LOGGER.info(
                "Deleted vulnerabilities from Jira",
                extra={
                    "extra": {
                        "url": url,
                        "headers": headers,
                    },
                },
            )


async def _build_bulk_workspaces_request(
    jira_jwt: str,
    base_url: str,
) -> tuple[str, str, dict[str, str]]:
    loaders = get_new_context()
    client_key = get_iss_from_jwt(jira_jwt)

    jira_install = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url=base_url,
            client_key=client_key,
        ),
    )

    if jira_install is None:
        return "", "", {}

    access_orgs = await get_stakeholder_organizations_ids(
        loaders, jira_install.associated_email, True
    )

    orgs: list[str] = [
        org.id for org in await loaders.organization.load_many(access_orgs) if org is not None
    ]

    for org in await loaders.organization.load_many(access_orgs):
        if org is not None:
            await organizations_model.update_metadata(
                metadata=OrganizationMetadataToUpdate(
                    jira_associated_id=f"CLIENT_KEY#{jira_install.client_key}SITE#{jira_install.base_url}"
                ),
                organization_id=org.id,
                organization_name=org.name,
            )

    path = "/rest/security/1.0/linkedWorkspaces/bulk"
    url = f"{jira_install.base_url}{path}"
    payload = json.dumps(
        {
            "workspaceIds": [
                org.replace("ORG#", "")
                for org in orgs  # type: ignore
            ],
        },
    )

    token: str = encode_token(
        http_method="POST",
        url=path,
        clientKey=jira_install.key,
        sharedSecret=jira_install.shared_secret,
    )

    headers = {
        "Content-Type": CONTENT_TYPE,
        "Authorization": f"JWT {token}",
    }

    return url, payload, headers


async def bulk_workspaces(jira_jwt: str, base_url: str) -> None:
    url, payload, headers = await _build_bulk_workspaces_request(
        jira_jwt=jira_jwt,
        base_url=base_url,
    )

    await fluidattacks_core.http.request(
        method="POST",
        url=url,
        json=json.loads(payload),  # type: ignore
        headers=headers,
    )

    TRANSACTIONAL_LOGGER.info(
        "Bulked workspaces to Jira",
        extra={
            "extra": {
                "url": url,
                "headers": headers,
                "payload": payload,
            },
        },
    )


async def _build_delete_workspaces_request(
    client_key: str,
    base_url: str,
    workspace_id: str,
) -> tuple[str, dict[str, str]]:
    loaders = get_new_context()

    jira_install = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url=base_url,
            client_key=client_key,
        ),
    )

    if jira_install is None:
        return "", {}

    url = (
        f"{jira_install.base_url}"
        f"/rest/security/1.0/linkedWorkspaces/bulk?workspaceIds={workspace_id}"
    )
    url_for_token = f"/rest/security/1.0/linkedWorkspaces/bulk?workspaceIds={workspace_id}"

    token: str = encode_token(
        http_method="DELETE",
        url=url_for_token,
        clientKey=jira_install.key,
        sharedSecret=jira_install.shared_secret,
    )

    headers = {
        "Authorization": f"JWT {token}",
    }

    return url, headers


@no_type_check
async def delete_bulk_workspaces(body: dict[str, str]) -> None:
    base_url = body["baseUrl"]
    client_key = body["clientKey"]
    workspaces = await _get_bulked_workspace(client_key=client_key, base_url=base_url)
    if workspaces is not None:
        workspace_ids = workspaces.get("workspaceIds", [])
        for workspace_id in workspace_ids:
            url, headers = await _build_delete_workspaces_request(
                client_key=client_key,
                base_url=base_url,
                workspace_id=workspace_id,
            )

            await fluidattacks_core.http.request(method="DELETE", url=url, headers=headers)

            TRANSACTIONAL_LOGGER.info(
                "Delete workspaces from Jira",
                extra={
                    "extra": {
                        "url": url,
                        "headers": headers,
                    },
                },
            )


async def _build_bulk_vulnerabilities_request(
    jira_jwt: str,
    base_url: str,
) -> tuple[str, list[str], dict[str, str]]:
    loaders = get_new_context()
    client_key = get_iss_from_jwt(jira_jwt)
    jira_install = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url=base_url,
            client_key=client_key,
        ),
    )

    if jira_install is None:
        return "", [""], {}

    path = "/rest/security/1.0/bulk"
    url = f"{jira_install.base_url}{path}"

    user_email = jira_install.associated_email
    access_orgs = await get_stakeholder_organizations_ids(loaders, user_email, True)
    orgs = [org.id for org in await loaders.organization.load_many(access_orgs) if org is not None]
    org_groups = [
        group
        for group in flatten(await loaders.organization_groups.load_many(list(orgs)))
        if group is not None
    ]

    access_groups = await loaders.stakeholder_groups_access.load(user_email)
    groups_to_bulk = await loaders.group.load_many(
        [group.group_name for group in access_groups if group.state.role == "group_manager"]
    )
    groups = [group for group in org_groups if group in groups_to_bulk]

    group_vulns = await loaders.group_vulnerabilities.load_many(
        [GroupVulnerabilitiesRequest(group_name=group.name) for group in groups]
    )

    vulnerabilities = [edge.node for vuln in group_vulns for edge in vuln.edges]
    timestamp = int(datetime.now().timestamp())

    payload_data = [
        {
            "schemaVersion": "1.0",
            "id": vulnerability.id,
            "updateSequenceNumber": timestamp,
            "containerId": vulnerability.group_name,
            "displayName": (
                f"{await get_finding_title(vulnerability.finding_id)} - "
                f"{vulnerability.state.where}"
            ),
            "description": f"Where: {vulnerability.state.where}",
            "url": await get_platform_url(
                vulnerability.group_name,
                vulnerability.finding_id,
                vulnerability.id,
            ),
            "type": get_type_from_technique(vulnerability.technique),
            "introducedDate": format_date(vulnerability.created_date),
            "lastUpdated": format_date(
                vulnerability.treatment.modified_date
                if vulnerability.treatment
                else vulnerability.created_date,
            ),
            "severity": {
                "level": await get_severity_text_value(vulnerability),
            },
            "status": "open",
            "additionalInfo": {
                "content": vulnerability.state.where,
                "url": await get_platform_url(
                    vulnerability.group_name,
                    vulnerability.finding_id,
                    vulnerability.id,
                ),
            },
        }
        for vulnerability_list in chunked(vulnerabilities, 500)
        for vulnerability in vulnerability_list
        if vulnerability is not None
        and vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
    ]

    payload = [
        json.dumps(
            {  # type: ignore
                "vulnerabilities": [  # type: ignore
                    payload_data,
                ],
            }
        )
        for payload_data in payload_data
    ]

    token: str = encode_token(
        http_method="POST",
        url=path,
        clientKey=jira_install.key,
        sharedSecret=jira_install.shared_secret,
        timeout_secs=7200,
    )
    headers = {
        "Content-Type": CONTENT_TYPE,
        "Authorization": f"JWT {token}",
    }
    return url, payload, headers


async def bulk_vulnerabilities(jira_jwt: str, base_url: str) -> None:
    url, payload, headers = await _build_bulk_vulnerabilities_request(
        jira_jwt=jira_jwt,
        base_url=base_url,
    )

    for payload_data in payload:
        response = await fluidattacks_core.http.request(
            method="POST",
            url=url,
            json=json.loads(payload_data),  # type: ignore
            headers=headers,
        )

        TRANSACTIONAL_LOGGER.info(
            "Bulk vulnerabilities to Jira",
            extra={
                "extra": {
                    "url": url,
                    "headers": headers,
                    "payload": payload_data,
                    "response_status": response.status,
                },
            },
        )
