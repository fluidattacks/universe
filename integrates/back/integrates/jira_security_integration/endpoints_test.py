import json

from integrates.batch import dal as batch_dal
from integrates.db_model import jira_installations as jira_installations_model
from integrates.jira_security_integration import utils
from integrates.jira_security_integration.endpoints import (
    jira_containers_response,
    jira_install_response,
    jira_settings_response,
    jira_uninstall_response,
    jira_user_associate,
    jira_workspaces_response,
)
from integrates.sessions import domain
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    JiraSecurityInstallFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import create_fake_client_request

CONTAINERS_URL = "/jira-security/containers"
UNINSTALL_URL = "/jira-security/uninstall"
WORKSPACES_URL = "/jira-security/workspaces"
SETTINGS_URL = "/jira-security/settings"
EXAMPLE_URL = "https://example.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[
        Mock(utils, "add_association", "async", None),
        Mock(utils, "bulk_workspaces", "async", None),
        Mock(batch_dal, "put_action", "async", None),
    ],
)
async def test_jira_user_associate_bad_token() -> None:
    request_expired_token = create_fake_client_request(
        method="POST",
        url=SETTINGS_URL,
        form_data={
            "token": (
                "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJzdWIi"
                "OiAidXNlckBleGFtcGxlLmNvbSIsICJxc2giOiAiODZiODAzYjEwOT"
                "MzODI4NmNjYWZkOTYzMjc5YWQ3YzdjOGFhNmI2MjM5N2FjYjE3OWZ"
                "hZjI0YTUzMjNiMjE4ZiIsICJpc3MiOiAidGVzdC1pc3N1ZXIiLCAia"
                "WF0IjogMTA1MjQyODMyMywgImV4cCI6IDEwNTI0Mjg5MjN9.27"
                "GuA2uZx1UK4tbtjcbD_a-qsLZdc2SB2TjT2H5ToqA"
            ),
            "base_url": "https://example_bad.com",
            "jwt": ("bad_jwt"),
        },
    )

    response = await jira_user_associate(_request=request_expired_token)  # type: ignore
    assert response.status_code == 303


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[
        Mock(utils, "add_association", "async", None),
        Mock(utils, "bulk_workspaces", "async", None),
        Mock(batch_dal, "put_action", "async", None),
        Mock(domain, "decode_token", "sync", {"user_email": "testemail@test.com"}),
    ],
)
async def test_jira_user_associate() -> None:
    request = create_fake_client_request(
        method="POST",
        url=SETTINGS_URL,
        form_data={
            "token": (
                "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJzdWIiOiAidXNlckB"
                "leGFtcGxlLmNvbSIsICJxc2giOiAiODZiODAzYjEwOTMzODI4NmNjYWZkOTY"
                "zMjc5YWQ3YzdjOGFhNmI2MjM5N2FjYjE3OWZhZjI0YTUzMjNiMjE4ZiIsICJp"
                "c3MiOiAidGVzdC1pc3N1ZXIiLCAiaWF0IjogMTczMTY5MDMyMywgImV4cCI6"
                "IDE3MzE2OTM5MjN9.L7XRBGHbPfxnrtDC-b_w-Ytjg3i0yyKNT5j0rksDcss"
            ),
            "base_url": EXAMPLE_URL,
            "jwt": (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
                "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik"
                "pvaG4gRG9lIiwiaXNzIjoiZXhhbXBsZS5jb20ifQ."
                "signature"
            ),
        },
    )
    request_bad_token = create_fake_client_request(
        method="POST",
        url=SETTINGS_URL,
        form_data={
            "token": ("bad_token"),
            "base_url": "https://example_bad.com",
            "jwt": ("bad_jwt"),
        },
    )

    response = await jira_user_associate(_request=request)  # type: ignore[arg-type]
    response_bad_token = await jira_user_associate(_request=request_bad_token)  # type: ignore[arg-type]

    assert response_bad_token.status_code == 303
    assert response.status_code == 303


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[
        Mock(utils, "add_install", "async", None),
        Mock(jira_installations_model, "update_shared_secret", "async", None),
    ],
)
async def test_jira_install() -> None:
    request = create_fake_client_request(
        method="POST",
        url="/jira-security/install",
        form_data={
            "token": (
                "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJzdWIiOiAidXNlckB"
                "leGFtcGxlLmNvbSIsICJxc2giOiAiODZiODAzYjEwOTMzODI4NmNjYWZkOTY"
                "zMjc5YWQ3YzdjOGFhNmI2MjM5N2FjYjE3OWZhZjI0YTUzMjNiMjE4ZiIsICJp"
                "c3MiOiAidGVzdC1pc3N1ZXIiLCAiaWF0IjogMTczMTY5MDMyMywgImV4cCI6"
                "IDE3MzE2OTM5MjN9.L7XRBGHbPfxnrtDC-b_w-Ytjg3i0yyKNT5j0rksDcss"
            ),
            "base_url": EXAMPLE_URL,
            "jwt": (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
                "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik"
                "pvaG4gRG9lIiwiaXNzIjoiZXhhbXBsZS5jb20ifQ."
                "signature"
            ),
        },
        json_data={  # type: ignore
            "clientKey": "example",
            "baseUrl": EXAMPLE_URL,
            "sharedSecret": "shared_secret",
        },
    )

    response = await jira_install_response(_request=request)  # type: ignore[arg-type]

    assert response.status_code == 200


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[Mock(utils, "check_request_jwt", "async", False)],
)
async def test_jira_containers_unauthorized() -> None:
    request = create_fake_client_request(
        method="GET",
        url=CONTAINERS_URL,
        headers={"Authorization": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ"},
        query_params={"workspaceId": "1234"},
    )

    response = await jira_containers_response(_request=request)  # type: ignore

    assert response.status_code == 401  # type: ignore


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
            organizations=[OrganizationFaker(id="1234", name="org-name")],
            groups=[
                GroupFaker(name="test-group-1", organization_id="1234"),
                GroupFaker(name="test-group-2", organization_id="no-org"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="user@example.com",
                    group_name="test-group-1",
                    state=GroupAccessStateFaker(role="group_manager"),
                )
            ],
        )
    ),
    others=[Mock(utils, "check_request_jwt", "async", True)],
)
async def test_jira_containers_get() -> None:
    request = create_fake_client_request(
        method="GET",
        url=CONTAINERS_URL,
        headers={
            "authorization": (
                "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e"
                "yJpc3MiOiJleGFtcGxlIiwic3ViIjoiMTIzNDU2Nzg5M"
                "CIsIm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOT"
                "AyMn0.5osSYkZsoAPbja53ylflRNbtobz"
                "H0T-x6g1qKDZ2YxU"
            )
        },
        query_params={"workspaceId": "1234"},
    )

    response = await jira_containers_response(_request=request)  # type: ignore

    assert response.status_code == 200  # type: ignore
    assert (
        json.loads(response.body.decode("utf-8")).get("containers")[0].get("id") == "test-group-1"  # type: ignore
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
            organizations=[OrganizationFaker(id="1234", name="org-name")],
            groups=[
                GroupFaker(name="test-group-1", organization_id="1234"),
                GroupFaker(name="test-group-2", organization_id="no-org"),
            ],
        )
    ),
    others=[Mock(utils, "check_request_jwt", "async", True)],
)
async def test_jira_containers_post() -> None:
    request = create_fake_client_request(
        method="POST",
        url=CONTAINERS_URL,
        headers={"Authorization": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"},
        query_params={"workspaceId": "1234"},
        json_data={"ids": ["test-group-1"]},  # type: ignore
    )

    response = await jira_containers_response(_request=request)  # type: ignore

    assert response.status_code == 200  # type: ignore
    assert (
        json.loads(response.body.decode("utf-8")).get("containers")[0].get("id") == "test-group-1"  # type: ignore
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[Mock(utils, "check_request_jwt", "async", True)],
)
async def test_jira_uninstall_unathorized() -> None:
    request = create_fake_client_request(
        method="POST",
        url="jira-security/uninstall",
        headers={},
        query_params={"workspaceId": "1234"},
    )

    response = await jira_uninstall_response(_request=request)  # type: ignore

    assert response.status_code == 401  # type: ignore


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[
        Mock(utils, "check_installation_associated_email", "async", "user@example.com"),
        Mock(utils, "delete_bulk_vulnerabilities", "async", None),
        Mock(utils, "delete_bulk_workspaces", "async", None),
    ],
)
async def test_jira_uninstall() -> None:
    request = create_fake_client_request(
        method="POST",
        url="jira-security/uninstall",
        headers={"authorization": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"},
        query_params={"workspaceId": "1234"},
        json_data={"baseUrl": EXAMPLE_URL, "clientKey": "example"},  # type: ignore
    )

    response = await jira_uninstall_response(_request=request)  # type: ignore

    assert response.status_code == 200  # type: ignore


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
            stakeholders=[StakeholderFaker(email="user@example.com")],
            organization_access=[
                OrganizationAccessFaker(email="user@example.com", organization_id="1234")
            ],
            organizations=[OrganizationFaker(id="1234", name="org-name")],
            groups=[
                GroupFaker(name="test-group-1", organization_id="1234"),
                GroupFaker(name="test-group-2", organization_id="no-org"),
            ],
        )
    ),
    others=[
        Mock(utils, "check_request_jwt", "async", True),
        Mock(utils, "check_installation_associated_email", "async", "user@example.com"),
    ],
)
async def test_jira_workspaces() -> None:
    request_no_auth_header = create_fake_client_request(
        method="POST",
        url=WORKSPACES_URL,
        headers={"Content-Type": "application/json"},
        json_data={"ids": ["test-group-1"]},  # type: ignore
    )

    request_auth_header_no_jwt = create_fake_client_request(
        method="POST",
        url=WORKSPACES_URL,
        headers={"Authorization": "Bearer any_token"},
        json_data={"ids": ["test-group-1"]},  # type: ignore
    )

    request_complete = create_fake_client_request(
        method="POST",
        url=WORKSPACES_URL,
        headers={
            "Authorization": (
                "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJleGFtcGxlIiwic3Vi"
                "IjoiMTIzNDU2Nzg5MCIsIm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTczMzExMzc2MH0.2Ck"
                "c_HtRT_DM1pf_fWuPTaURldR86b1O7jki00YGpyw"
            )
        },
        json_data={"ids": ["test-group-1"]},  # type: ignore
    )

    response_no_auth_header = await jira_workspaces_response(request=request_no_auth_header)  # type: ignore
    response_auth_header_no_jwt = await jira_workspaces_response(request=request_auth_header_no_jwt)  # type: ignore
    response_complete = await jira_workspaces_response(request=request_complete)  # type: ignore

    assert response_auth_header_no_jwt.status_code == 401  # type: ignore
    assert response_no_auth_header.status_code == 401  # type: ignore
    assert response_complete.status_code == 200  # type: ignore
    assert (
        json.loads(response_complete.body.decode("utf-8")).get("workspaces")[0].get("id") == "1234"  # type: ignore
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[Mock(utils, "check_installation_associated_email", "async", "user@example.com")],
)
async def test_jira_settings_response() -> None:
    request = create_fake_client_request(
        method="GET",
        url=SETTINGS_URL,
        query_params={"jwt": "test-jwt", "xdm_e": EXAMPLE_URL},
    )

    response = await jira_settings_response(_request=request)  # type: ignore

    assert response.template.name == "settings.html"  # type: ignore
    assert response.context["associated_email"] == "user@example.com"  # type: ignore
    assert response.context["jwt"] == "test-jwt"  # type: ignore
    assert response.context["base_url"] == EXAMPLE_URL  # type: ignore


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[
        Mock(utils, "check_installation_associated_email", "async", ""),
        Mock(utils, "check_request_jwt", "async", True),
    ],
)
async def test_jira_settings_response_no_associated() -> None:
    request = create_fake_client_request(
        method="GET",
        url=SETTINGS_URL,
        query_params={"jwt": "test-jwt", "xdm_e": EXAMPLE_URL},
    )

    response = await jira_settings_response(_request=request)  # type: ignore

    assert response.template.name == "settings_add_token.html"  # type: ignore
    assert response.context["jwt"] == "test-jwt"  # type: ignore
    assert response.context["base_url"] == EXAMPLE_URL  # type: ignore


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="",
                    base_url=EXAMPLE_URL,
                    client_key="example",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        )
    ),
    others=[
        Mock(utils, "check_installation_associated_email", "async", ""),
        Mock(utils, "check_request_jwt", "async", False),
    ],
)
async def test_jira_settings_response_no_associated_2() -> None:
    request = create_fake_client_request(
        method="GET",
        url=SETTINGS_URL,
        query_params={"jwt": "test-jwt", "xdm_e": EXAMPLE_URL},
    )

    response = await jira_settings_response(_request=request)  # type: ignore

    assert response.status_code == 401  # type: ignore
