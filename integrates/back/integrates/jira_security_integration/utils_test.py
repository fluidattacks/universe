import json
from datetime import datetime
from decimal import Decimal

import fluidattacks_core.http

from integrates import custom_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.jira_installations.types import (
    JiraInstallState,
    JiraSecurityInstall,
    JiraSecurityInstallRequest,
)
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
)
from integrates.sessions import domain as sessions_domain
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    JiraSecurityInstallFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import create_fake_client_response, freeze_time, parametrize

from .utils import (
    _build_bulk_vulnerabilities_request,
    _build_bulk_workspaces_request,
    _build_delete_vulns_request,
    _build_delete_workspaces_request,
    _get_bulked_workspace,
    add_association,
    add_install,
    check_installation_associated_email,
    check_request_jwt,
    delete_bulk_vulnerabilities,
    format_date,
    get_finding_title,
    get_iss_from_jwt,
    get_platform_url,
    get_qsh_from_jwt,
    get_severity_text_value,
    get_sub_from_jwt,
    get_type_from_technique,
    jira_get_organization_name,
)


@parametrize(
    args=["org_id", "expected_name"],
    cases=[
        [
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            "okada",
        ],
        ["no-org", ""],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                    name="okada",
                ),
            ],
        ),
    )
)
async def test_org_name(org_id: str, expected_name: str) -> None:
    org_name = await jira_get_organization_name(org_id)

    assert org_name == expected_name


@parametrize(
    args=["jwt_token", "expected_iss"],
    cases=[
        [
            """
            eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
            eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik
            pvaG4gRG9lIiwiaXNzIjoiZXhhbXBsZS5jb20ifQ.
            signature
            """,
            "example.com",
        ],
    ],
)
async def test_get_iss_from_jwt(jwt_token: str, expected_iss: str) -> None:  # type: ignore[misc]
    result = get_iss_from_jwt(jwt_token)
    assert result == expected_iss


@parametrize(
    args=["vulnerability_id", "expected_severity"],
    cases=[
        ["test-vuln-critical", "critical"],
        ["test-vuln-high", "high"],
        ["test-vuln-medium", "medium"],
        ["test-vuln-low", "low"],
        ["test-vuln-no-finding", "unknown"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    id="test-vuln-critical",
                    finding_id="test-finding-1",
                    severity_score=SeverityScore(threat_score=Decimal("9.0")),
                ),
                VulnerabilityFaker(
                    id="test-vuln-high",
                    finding_id="test-finding-1",
                    severity_score=SeverityScore(threat_score=Decimal("7.0")),
                ),
                VulnerabilityFaker(
                    id="test-vuln-medium",
                    finding_id="test-finding-1",
                    severity_score=SeverityScore(threat_score=Decimal("4.0")),
                ),
                VulnerabilityFaker(
                    id="test-vuln-low",
                    finding_id="test-finding-1",
                    severity_score=SeverityScore(threat_score=Decimal("1.0")),
                ),
                VulnerabilityFaker(
                    id="test-vuln-no-finding",
                    finding_id="test-finding-no-finding",
                    severity_score=None,
                ),
            ],
            findings=[
                FindingFaker(
                    id="test-finding-1",
                    group_name="test-group-1",
                ),
            ],
        ),
    )
)
async def test_get_severity_text_value(
    vulnerability_id: str,
    expected_severity: str,
) -> None:
    loaders: Dataloaders = get_new_context()
    vuln = await loaders.vulnerability.load(vulnerability_id)
    assert vuln is not None
    severity_text = await get_severity_text_value(vuln)
    assert severity_text == expected_severity


@parametrize(
    args=["finding_id", "expected_name"],
    cases=[
        ["test-finding-1", "finding-1"],
        ["test-finding-no-finding", ""],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id="test-finding-1",
                    group_name="test-group-1",
                    title="finding-1",
                ),
            ],
        ),
    )
)
async def test_get_finding_title(finding_id: str, expected_name: str) -> None:
    finding_name = await get_finding_title(finding_id)
    assert finding_name == expected_name


@parametrize(
    args=["group_name", "finding", "vulnerability", "expected_url"],
    cases=[
        [
            "test-group-1",
            "finding",
            "vulnerability",
            (
                "https://localhost:8001/orgs/org-name/groups/"
                "test-group-1/vulns/finding/locations/vulnerability"
            ),
        ],
        [
            "test-group-2",
            "finding",
            "vulnerability",
            "",
        ],
        [
            "test-group-3",
            "finding",
            "vulnerability",
            "",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="test-group-1", organization_id="org-id"),
                GroupFaker(name="test-group-2", organization_id="no-org"),
            ],
            organizations=[
                OrganizationFaker(
                    id="org-id",
                    name="org-name",
                ),
            ],
        ),
    )
)
async def test_get_platform_url(
    group_name: str,
    finding: str,
    vulnerability: str,
    expected_url: str,
) -> None:
    url = await get_platform_url(group_name, finding, vulnerability)
    assert url == expected_url


@parametrize(
    args=["technique", "expected_type"],
    cases=[
        [VulnerabilityTechnique.DAST, "dast"],
        [VulnerabilityTechnique.SAST, "sast"],
        [VulnerabilityTechnique.SCA, "sca"],
        [VulnerabilityTechnique.CLOUD, "unknown"],
    ],
)
async def test_get_type_from_technique(  # type: ignore[misc]
    technique: VulnerabilityTechnique,
    expected_type: str,
) -> None:
    vuln_type = get_type_from_technique(technique)
    assert vuln_type == expected_type


@parametrize(
    args=["date"],
    cases=[
        [datetime.fromisoformat("2019-09-13T13:17:41+00:00")],
    ],
)
async def test_format_date(date: datetime) -> None:  # type: ignore[misc]
    formatted_date = format_date(date)
    assert formatted_date == "2019-09-13T13:17:41.000Z"


@parametrize(
    args=["jwt", "expected_qsh"],
    cases=[
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1w"
                "bGUuY29tIiwicXNoIjoidGVzdF9xc2giLCJpYXQiOjE3MzA4MzY2MjEsImV4c"
                "CI6MTczMDg0MDIyMX0.81S2qhzl_Oq2ll5ycGmc9PtvP13Q_WsEMaSVHx62"
                "NV0"
            ),
            "test_qsh",
        ],
    ],
)
async def test_get_qsh_from_jwt(jwt: str, expected_qsh: str) -> None:  # type: ignore[misc]
    qsh = get_qsh_from_jwt(jwt)
    assert qsh == expected_qsh


@parametrize(
    args=["jwt", "expected_sub"],
    cases=[
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0OjU2"
                "Nzg6OTAwMCIsIm5hbWUiOiJ0ZXN0IHVzZXIiLCJpYXQiOjE2MjI1NDc4MD"
                "B9.PN_S6a8mjU-W7Q8geF__7F7D2ALHFInb_2xnLroAxpE"
            ),
            "1234:5678:9000",
        ],
    ],
)
async def test_get_sub_from_jwt(jwt: str, expected_sub: str) -> None:  # type: ignore[misc]
    sub = get_sub_from_jwt(jwt)
    assert sub == expected_sub


@parametrize(
    args=["jwt", "url", "method", "expected_result"],
    cases=[
        [
            (
                "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJzdWIiOiAidXNlckB"
                "leGFtcGxlLmNvbSIsICJxc2giOiAiODZiODAzYjEwOTMzODI4NmNjYWZkOTY"
                "zMjc5YWQ3YzdjOGFhNmI2MjM5N2FjYjE3OWZhZjI0YTUzMjNiMjE4ZiIsICJp"
                "c3MiOiAidGVzdC1pc3N1ZXIiLCAiaWF0IjogMTczMTY5MDMyMywgImV4cCI6"
                "IDE3MzE2OTM5MjN9.L7XRBGHbPfxnrtDC-b_w-Ytjg3i0yyKNT5j0rksDcss"
            ),
            "https://example.com/rest/api/2/issue",
            "GET",
            True,
        ],
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1w"
                "bGUuY29tIiwicXNoIjoidGVzdF9xc2giLCJpc3MiOiJ0ZXN0LWlzc3VlciIsI"
                "mlhdCI6MTczMDgzNjYyMSwiZXhwIjoxNzMwODQwMjIxfQ.81S2qhzl_Oq2ll5"
                "ycGmc9PtvP13Q_WsEMaSVHx62NV0"
            ),
            "https://example.com/rest/api/2/issue",
            "POST",
            False,
        ],
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1w"
                "bGUuY29tIiwicXNoIjoidGVzdF9xc2giLCJpc3MiOiJ0ZXN0LWlzc3VlciIsI"
                "mlhdCI6MTczMDgzNjYyMSwiZXhwIjoxNzMwODQwMjIxfQ.81S2qhzl_Oq2ll5"
                "ycGmc9PtvP13Q_WsEMaSVHx62NV0"
            ),
            None,
            "GET",
            False,
        ],
        [
            None,
            "https://example.com/rest/api/2/issue",
            "GET",
            False,
        ],
    ],
)
@freeze_time("2024-10-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_check_request_jwt(
    jwt: str | None,
    url: str | None,
    method: str | None,
    expected_result: bool,
) -> None:
    result = await check_request_jwt(jwt, url, method)  # type: ignore
    assert result == expected_result  # type: ignore


@parametrize(
    args=["jwt", "url", "method"],
    cases=[
        [
            (
                "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJzdWIiOiAidXNlckB"
                "leGFtcGxlLmNvbSIsICJxc2giOiAiODZiODAzYjEwOTMzODI4NmNjYWZkOTY"
                "zMjc5YWQ3YzdjOGFhNmI2MjM5N2FjYjE3OWZhZjI0YTUzMjNiMjE4ZiIsICJ"
                "pc3MiOiAidGVzdC1pc3N1ZXIiLCAiaWF0IjogMTczMDgzNjYyMSwgImV4cCI"
                "6IDE3MzA4NDAyMjF9.4-PAo_8odgXTp82mzqs4XkDjxvva4KOoZVurEhhKRSg"
            ),
            "https://example.com/rest/api/2/issue",
            "GET",
        ],
        [
            (
                "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJzdWIiOiAidXNlckB"
                "leGFtcGxlLmNvbSIsICJxc2giOiAiODZiODAzYjEwOTMzODI4NmNjYWZkOTY"
                "zMjc5YWQ3YzdjOGFhNmI2MjM5N2FjYjE3OWZhZjI0YTUzMjNiMjE4ZiIsICJ"
                "pc3MiOiAidGVzdC1pc3N1ZXIiLCAiaWF0IjogMTczMTY5NTg4NCwgImV4cCI"
                "6IDE3MzE2OTk0ODR9.kJDhA5_SMmXmCVMLdiShJCPl_-WkzhZZAxybLCzsIF0"
            ),
            "https://example.com/rest/api/2/issue",
            "GET",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_check_request_jwt_expired(
    jwt: str | None,
    url: str | None,
    method: str | None,
) -> None:
    result = await check_request_jwt(jwt, url, method)  # type: ignore
    assert result is False  # type: ignore


@freeze_time("2024-10-14T00:00:00")
@parametrize(
    args=[
        "client_key",
        "base_url",
        "workspace_id",
        "expected_url",
    ],
    cases=[
        [
            "test-issuer",
            "https://example.com",
            "123456789000",
            ("https://example.com/rest/security/1.0/bulkByProperties?workspaceId=123456789000"),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_build_delete_vulns_request(
    client_key: str,
    base_url: str,
    workspace_id: str,
    expected_url: str,
) -> None:
    url, headers = await _build_delete_vulns_request(base_url, client_key, workspace_id)

    assert url == expected_url
    assert headers["Authorization"] == (
        "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjb20udGVzdGluZy50ZXN0aW5nLWppc"
        "mEtYXBwIiwiZXhwIjoxNzI4ODY3NjAwLCJpYXQiOjE3Mjg4NjQwMDAsImlzcyI6ImNvbS50ZXN0aW5"
        "nLnRlc3RpbmctamlyYS1hcHAiLCJxc2giOiJkNTNlZDNiODBhNWU5MzBjOThkMTRhZTc5YmZkYWQzNW"
        "Q1MzNmZTdhYmRjODMwNzQ2ZTJkZDY5MDhkZTJhYTAwIn0.MAa9o4RG8XN6m82nbpZrH3j60yiR-48uX4w6QHg_w4k"
    )


@freeze_time("2024-10-14T00:00:00")
@parametrize(
    args=[
        "jira_jwt",
        "base_url",
        "expected_url",
        "expected_payload",
    ],
    cases=[
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4"
                "YW1wbGUuY29tIiwic3ViIjoiMTIzNDU2Nzg5MDAwIiwiaXNzIjoidGVz"
                "dC1pc3N1ZXIiLCJpYXQiOjE2MjI1NDc4MDB9.PN_S6a8mjU-W7Q8ge"
                "F__7F7D2ALHFInb_2xnLroAxpE"
            ),
            "https://example.com",
            "https://example.com/rest/security/1.0/linkedWorkspaces/bulk",
            json.dumps({"workspaceIds": ["org1", "org2"]}),  # type: ignore
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
                OrganizationFaker(id="org2"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="user@example.com",
                    organization_id="org1",
                ),
                OrganizationAccessFaker(
                    email="user@example.com",
                    organization_id="org2",
                ),
            ],
        ),
    )
)
async def test_build_bulk_workspaces_request(
    jira_jwt: str,
    base_url: str,
    expected_url: str,
    expected_payload: str,
) -> None:
    url, payload, headers = await _build_bulk_workspaces_request(
        jira_jwt,
        base_url,
    )

    assert url == expected_url
    assert payload == expected_payload
    assert headers["Content-Type"] == "application/json"
    assert headers["Authorization"] == (
        "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjb20"
        "udGVzdGluZy50ZXN0aW5nLWppcmEtYXBwIiwiZXhwIjoxNzI4ODY3NjAwLC"
        "JpYXQiOjE3Mjg4NjQwMDAsImlzcyI6ImNvbS50ZXN0aW5nLnRlc3Rpbmctamly"
        "YS1hcHAiLCJxc2giOiI2MWNkNjRhN2VjNzFhMjBiNWNhNDc4MWNlMjU4MmY5"
        "Y2NiNzcxMjc4NmYwMmU4YWI2OWQxYzFjMGQ2ZjQzYWNlIn0.bwyVJJdxweLW"
        "GHGcL0F20-YXKb9i6MSAsbudZQ4wJ4o"
    )


@freeze_time("2024-10-14T00:00:00")
@parametrize(
    args=[
        "client_key",
        "base_url",
        "workspace_id",
        "expected_url",
    ],
    cases=[
        [
            "test-issuer",
            "https://example.com",
            "org1",
            "https://example.com/rest/security/1.0/linkedWorkspaces/bulk?workspaceIds=org1",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
                OrganizationFaker(id="org2"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="user@example.com",
                    organization_id="org1",
                ),
                OrganizationAccessFaker(
                    email="user@example.com",
                    organization_id="org2",
                ),
            ],
        ),
    )
)
async def test_build_delete_workspaces_request(
    client_key: str,
    base_url: str,
    workspace_id: str,
    expected_url: str,
) -> None:
    url, headers = await _build_delete_workspaces_request(client_key, base_url, workspace_id)

    assert url == expected_url
    assert headers["Authorization"] == (
        "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjb20udGVzdGluZy50ZXN"
        "0aW5nLWppcmEtYXBwIiwiZXhwIjoxNzI4ODY3NjAwLCJpYXQiOjE3Mjg4NjQwMDAsImlzcyI6"
        "ImNvbS50ZXN0aW5nLnRlc3RpbmctamlyYS1hcHAiLCJxc2giOiJiOTRlNTk3NjkzMDI2YjY1M"
        "mYzM2RjMDViNzc2ZDM2YjFhNmUyNzM2Y2M3Mzk1MzdmMmU3ZWZlMjZmYTExMGJiIn0.e55sc"
        "V-wHh66-bT9O_APYeuJaxCVVTVRtRBpJFWAUyA"
    )


@freeze_time("2024-10-14T00:00:00")
@parametrize(
    args=[
        "jira_jwt",
        "base_url",
        "expected_url",
        "expected_payload",
    ],
    cases=[
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4"
                "YW1wbGUuY29tIiwic3ViIjoiMTIzNDU2Nzg5MDAwIiwiaXNzIjoidGVz"
                "dC1pc3N1ZXIiLCJpYXQiOjE2MjI1NDc4MDB9.PN_S6a8mjU-W7Q8ge"
                "F__7F7D2ALHFInb_2xnLroAxpE"
            ),
            "https://example.com",
            "https://example.com/rest/security/1.0/bulk",
            [
                json.dumps(
                    {  # type: ignore
                        "vulnerabilities": [
                            {  # type: ignore
                                "schemaVersion": "1.0",
                                "id": "vuln-1",
                                "updateSequenceNumber": 1728864000,
                                "containerId": "group-1",
                                "displayName": "finding-1 - description-1",
                                "description": "Where: description-1",
                                "url": (
                                    "https://localhost:8001"
                                    "/orgs/org-name/groups/"
                                    "group-1/vulns/finding-1/locations/vuln-1"
                                ),
                                "type": "dast",
                                "introducedDate": "2024-10-14T00:00:00.000Z",
                                "lastUpdated": "2019-04-10T12:59:52.000Z",
                                "severity": {"level": "unknown"},  # type: ignore
                                "status": "open",
                                "additionalInfo": {  # type: ignore
                                    "content": "description-1",
                                    "url": (
                                        "https://localhost:8001"
                                        "/orgs/org-name/groups/"
                                        "group-1/vulns/finding-1/loca"
                                        "tions/vuln-1"
                                    ),
                                },
                            },
                        ],
                    },
                )
            ],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    associated_email="user@example.com",
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
            organizations=[
                OrganizationFaker(
                    id="org1",
                    name="org-name",
                ),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="user@example.com",
                    organization_id="org1",
                ),
            ],
            groups=[
                GroupFaker(name="group-1", organization_id="org1"),
                GroupFaker(name="group-2", organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="user@example.com",
                    group_name="group-1",
                    state=GroupAccessStateFaker(
                        role="group_manager",
                    ),
                )
            ],
            stakeholders=[StakeholderFaker(email="user@example.com")],
            findings=[
                FindingFaker(
                    id="finding-1",
                    group_name="group-1",
                    title="finding-1",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln-1",
                    finding_id="finding-1",
                    group_name="group-1",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="description-1",
                    ),
                    technique=VulnerabilityTechnique.DAST,
                    created_date=datetime(2024, 10, 14),
                ),
                VulnerabilityFaker(
                    id="vuln-2",
                    finding_id="finding-1",
                    group_name="group-2",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="description-2",
                    ),
                    technique=VulnerabilityTechnique.DAST,
                    created_date=datetime(2024, 10, 14),
                ),
            ],
        ),
    ),
    others=[
        Mock(custom_utils.vulnerabilities, "get_severity_threat_score", "sync", Decimal("0.0"))
    ],
)
async def test_build_bulk_vulnerabilities_request(
    jira_jwt: str,
    base_url: str,
    expected_url: str,
    expected_payload: list[str],
) -> None:
    url, payload, headers = await _build_bulk_vulnerabilities_request(
        jira_jwt,
        base_url,
    )

    assert url == expected_url
    assert payload == expected_payload
    assert headers["Content-Type"] == "application/json"
    assert headers["Authorization"] == (
        "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjb20udGVz"
        "dGluZy50ZXN0aW5nLWppcmEtYXBwIiwiZXhwIjoxNzI4ODcxMjAwLCJpYXQi"
        "OjE3Mjg4NjQwMDAsImlzcyI6ImNvbS50ZXN0aW5nLnRlc3RpbmctamlyYS1h"
        "cHAiLCJxc2giOiI2OGI2NzU3MjE3Nzk4M2RkMjE4YjYzNTMzNjg3MGJlYjk2"
        "ZTRmYTcyNmZiYzU2YWY5NmVlODM3MTA0MDM2ZDRiIn0.qLpGT3u5O7fJ1x"
        "u_gbBSD37-q_z5bY-pkmWxqQZ5_rM"
    )


@parametrize(
    args=["jwt", "base_url", "expected_email"],
    cases=[
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1w"
                "bGUuY29tIiwiaXNzIjoidGVzdC1pc3N1ZXIifQ.81S2qhzl_Oq2ll5yc"
                "Gmc9PtvP13Q_WsEMaSVHx62NV0"
            ),
            "https://example.com",
            "user@example.com",
        ],
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1w"
                "bGUuY29tIiwiaXNzIjoidGVzdC1pc3N1ZXIifQ.81S2qhzl_Oq2ll5y"
                "cGmc9PtvP13Q_WsEMaSVHx62NV0"
            ),
            "https://nonexistent.com",
            "",
        ],
        [None, "https://example.com", ""],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-issuer",
                    associated_email="user@example.com",
                ),
            ],
        ),
    )
)
async def test_check_installation_associated_email(
    jwt: str | None,
    base_url: str,
    expected_email: str,
) -> None:
    email = await check_installation_associated_email(jwt, base_url)
    assert email == expected_email


@parametrize(
    args=["api_token", "jira_jwt", "base_url"],
    cases=[
        [
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2VtY"
                "WlsIjoidXNlckBleGFtcGxlLmNvbSJ9.81S2qhzl_Oq2ll5yc"
                "Gmc9PtvP13Q_WsEMaSVHx62NV0"
            ),
            (
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1w"
                "bGUuY29tIiwiaXNzIjoidGVzdC1pc3N1ZXIifQ.81S"
                "2qhzl_Oq2ll5ycGmc9PtvP13Q_WsEMaSVHx62NV0"
            ),
            "https://example.com",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
            stakeholders=[
                StakeholderFaker(
                    email="user@example.com",
                ),
            ],
        ),
    ),
    others=[Mock(sessions_domain, "decode_token", "sync", {"user_email": "user@example.com"})],
)
async def test_add_association(
    api_token: str,
    jira_jwt: str,
    base_url: str,
) -> None:
    await add_association(api_token, jira_jwt, base_url)
    loaders: Dataloaders = get_new_context()
    install = await loaders.jira_install.load(
        JiraSecurityInstallRequest(base_url=base_url, client_key="test-issuer"),
    )
    assert install is not None
    assert install.associated_email == "user@example.com"
    stakeholder = await loaders.stakeholder.load("user@example.com")
    assert stakeholder is not None
    assert stakeholder.jira_client_key == "test-issuer"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        ),
    ),
    others=[
        Mock(
            fluidattacks_core.http,
            "request",
            "async",
            create_fake_client_response(
                status="200",
                json_data={"workspaceIds": ["workspace-1", "workspace-2"]},  # type: ignore[misc]
            ),
        )
    ],
)
async def test__get_bulked_workspace_pass() -> None:
    result = await _get_bulked_workspace("test-issuer", "https://example.com")  # type: ignore
    assert result == {"workspaceIds": ["workspace-1", "workspace-2"]}  # type: ignore


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-issuer",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        ),
    ),
    others=[
        Mock(
            fluidattacks_core.http,
            "request",
            "async",
            create_fake_client_response(
                status="404",
                json_data={},  # type: ignore[misc]
            ),
        )
    ],
)
async def test__get_bulked_workspace_fail() -> None:
    result = await _get_bulked_workspace("test-issuer", "https://nonexistent.com")  # type: ignore
    assert result is None  # type: ignore


@freeze_time("2024-10-14T00:00:00")
@parametrize(
    args=["body", "expected_install"],
    cases=[
        [
            {
                "associated_email": "",
                "baseUrl": "https://example.com",
                "clientKey": "test-client-key",
                "cloudId": "test-cloud-id",
                "description": "test-description",
                "displayUrl": "https://example.com/display",
                "eventType": "installed",
                "installationId": "test-installation-id",
                "key": "test-key",
                "pluginsVersion": "1.0.0",
                "productType": "jira",
                "publicKey": "test-public-key",
                "serverVersion": "1.0.0",
                "sharedSecret": "test-shared-secret",
            },
            JiraSecurityInstall(
                associated_email="",
                base_url="https://example.com",
                client_key="test-client-key",
                cloud_id="test-cloud-id",
                description="test-description",
                display_url="https://example.com/display",
                event_type="installed",
                installation_id="test-installation-id",
                key="test-key",
                plugins_version="1.0.0",
                product_type="jira",
                public_key="test-public-key",
                server_version="1.0.0",
                shared_secret="test-shared-secret",  # noqa: S106
                state=JiraInstallState(
                    modified_by="",
                    modified_date="2024-10-14T00:00:00+00:00",
                    used_api_token="",
                    used_jira_jwt="",
                ),
            ),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[],
        ),
    )
)
async def test_add_install(body: dict[str, str], expected_install: JiraSecurityInstall) -> None:
    await add_install(body)
    loaders: Dataloaders = get_new_context()
    install = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url=body["baseUrl"],
            client_key=body["clientKey"],
        ),
    )
    assert install is not None
    assert install == expected_install


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-client-key",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        ),
    ),
    others=[
        Mock(
            fluidattacks_core.http,
            "request",
            "async",
            create_fake_client_response(
                status="200",
                json_data={"workspaceIds": ["workspace-1", "workspace-2"]},  # type: ignore
            ),
        )
    ],
)
async def test_delete_bulk_vulnerabilities() -> None:
    body = {
        "baseUrl": "https://example.com",
        "clientKey": "test-client-key",
    }
    expected_workspace_ids = ["workspace-1", "workspace-2"]

    await delete_bulk_vulnerabilities(body)  # type: ignore

    for workspace_id in expected_workspace_ids:
        url, headers = await _build_delete_vulns_request(
            base_url=body["baseUrl"],
            client_key=body["clientKey"],
            workspace_id=workspace_id,
        )
        assert (
            url
            == f"https://example.com/rest/security/1.0/bulkByProperties?workspaceId={workspace_id}"
        )
        assert headers["Authorization"].startswith("JWT ")
