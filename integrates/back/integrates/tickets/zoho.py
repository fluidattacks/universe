import contextlib
import time
from collections.abc import (
    AsyncGenerator,
    Awaitable,
    Callable,
)
from typing import (
    NotRequired,
    TypedDict,
)

import aiohttp
from starlette.datastructures import (
    UploadFile,
)

from integrates.context import (
    FI_ZOHO_CLIENT_ID,
    FI_ZOHO_CLIENT_SECRET,
    FI_ZOHO_DEPARTMENT_ID,
    FI_ZOHO_ORG_ID,
    FI_ZOHO_REFRESH_TOKEN,
)


class ZohoDeskException(Exception):
    pass


class TokenResponse(TypedDict):
    access_token: NotRequired[str]
    expires_in: int


def create_token_getter() -> Callable[[], Awaitable[str]]:
    current_token = None
    current_expiration = None

    async def _access_token() -> str:
        """
        Returns a valid zoho access token, generating a new one when needed.

        https://desk.zoho.com/support/APIDocument.do#OauthTokens#RefreshingAccessTokens
        """
        nonlocal current_token, current_expiration
        current_time = int(time.time())

        if current_token and current_expiration and current_time < current_expiration:
            return current_token

        async with aiohttp.ClientSession() as session:
            async with session.post(
                "https://accounts.zoho.com/oauth/v2/token",
                params={
                    "client_id": FI_ZOHO_CLIENT_ID,
                    "client_secret": FI_ZOHO_CLIENT_SECRET,
                    "grant_type": "refresh_token",
                    "refresh_token": FI_ZOHO_REFRESH_TOKEN,
                },
            ) as response:
                if not response.ok:
                    raise ZohoDeskException("Couldn't get access token", response.status)
                content: TokenResponse = await response.json()
                if "access_token" not in content:
                    raise ZohoDeskException(
                        "Access token not found in response",
                        response.status,
                        content,
                    )
                current_token = content["access_token"]
                current_expiration = current_time + content["expires_in"]
                return current_token

    return _access_token


_get_access_token = create_token_getter()


@contextlib.asynccontextmanager
async def zoho_session() -> AsyncGenerator[aiohttp.ClientSession, None]:
    access_token = await _get_access_token()

    async with aiohttp.ClientSession(
        headers={
            "authorization": f"Zoho-oauthtoken {access_token}",
            "orgId": FI_ZOHO_ORG_ID,
        },
    ) as session:
        yield session


async def _upload_attachment(ticket_id: str, attachment: UploadFile) -> None:
    """
    Uploads attachments to a ticket.

    https://desk.zoho.com/support/APIDocument.do#TicketAttachments#TicketAttachments_CreateTicketattachment
    """
    async with zoho_session() as session:
        data = aiohttp.FormData()
        data.add_field("file", attachment.file, filename=attachment.filename)

        async with session.post(
            f"https://desk.zoho.com/api/v1/tickets/{ticket_id}/attachments",
            data=data,
        ) as response:
            content = await response.json()
            if not response.ok:
                raise ZohoDeskException("Couldn't upload attachment", response.status, content)


class TicketResponse(TypedDict):
    id: str


async def create_ticket(
    *,
    subject: str,
    description: str,
    requester_email: str,
    attachments: tuple[UploadFile, ...] | None = None,
) -> None:
    """
    Creates a new ticket.

    https://desk.zoho.com/support/APIDocument.do#Tickets#Tickets_Createaticket
    """
    if not FI_ZOHO_CLIENT_ID:
        return

    async with zoho_session() as session:
        async with session.post(
            "https://desk.zoho.com/api/v1/tickets",
            json={
                "channel": "Email",
                "contact": {"email": requester_email},
                "departmentId": FI_ZOHO_DEPARTMENT_ID,
                "description": description.replace("\n", "<br>"),
                "email": requester_email,
                "language": "English",
                "status": "Open",
                "subject": subject,
            },
        ) as response:
            content: TicketResponse = await response.json()
            if not response.ok:
                raise ZohoDeskException("Couldn't create ticket", response.status, content)

            if attachments:
                for attachment in attachments:
                    await _upload_attachment(content["id"], attachment)
