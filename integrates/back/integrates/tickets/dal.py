import logging
import logging.config

from starlette.datastructures import (
    UploadFile,
)

from integrates.settings import (
    LOGGING,
)
from integrates.tickets import (
    zoho,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


async def create_ticket(
    *,
    subject: str,
    description: str,
    requester_email: str,
    attachments: tuple[UploadFile, ...] | None = None,
) -> None:
    try:
        await zoho.create_ticket(
            subject=subject,
            description=description,
            requester_email=requester_email,
            attachments=attachments,
        )
        TRANSACTIONS_LOGGER.info(
            "Ticket created",
            extra={
                "subject": subject,
                "description": description,
                "requester_email": requester_email,
            },
        )
    except zoho.ZohoDeskException as exception:
        LOGGER.exception(exception, extra={"extra": locals()})
