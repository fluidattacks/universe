{ config, inputs, pkgs, ... }:
let
  utils = import inputs.utils { inherit pkgs; };
  cert = import inputs.cert { inherit utils; };
in {
  cachix = utils.cachix.config.default;

  packages = [
    pkgs.file
    pkgs.gcc.cc.lib
    pkgs.git
    pkgs.git-remote-codecommit
    pkgs.openssh
    pkgs.p7zip
    pkgs.jq
    pkgs.skopeo
    pkgs.tokei

    utils.shell.aws
    utils.shell.sops

    # Python dependencies outside of poetry
    config.languages.python.package.pkgs.ruff
    config.languages.python.package.pkgs.pycurl
    config.languages.python.package.pkgs.python_magic
  ];

  languages.python = {
    enable = true;
    package = pkgs.python311;
    poetry = {
      enable = true;
      install = {
        enable = true;
        installRootPackage = true;
      };
      activate.enable = true;
      package = pkgs.poetry;
    };
  };

  scripts = {
    lint.exec = utils.shell.strict ./scripts/lint.sh;
    environment.exec = utils.shell.strict ./scripts/environment.sh;
    server.exec = utils.shell.strict ./scripts/server.sh;
    integrates-back.exec = utils.shell.strict ''
      . environment \
        "$1" \
        "${inputs.secrets}" \
        "${inputs.finding-codes}" \
        "${inputs.criteria}" \
        "${inputs.compute-sizes}" \
        "${inputs.db-design}"
      . server "$1" "${cert}"
    '';
  };
}
