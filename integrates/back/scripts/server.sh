# shellcheck shell=bash

function serve {
  local env="${1-}"
  local cert="${2}"

  local workers_per_core=1
  local cores
  cores=$(nproc --all)
  local recommended_workers=$((workers_per_core * cores))

  # The current value of alb.ingress.kubernetes.io/load-balancer-attributes
  local load_balancer_timeout=60
  # We must wait a little longer to let the load balancer close the connection first
  # https://docs.aws.amazon.com/elasticloadbalancing/latest/application/application-load-balancers.html#connection-idle-timeout
  local keep_alive_timeout=$((load_balancer_timeout + 5))

  local config
  local common_config=(
    # The TCP host/address to bind to
    --bind "0.0.0.0:8001"
    # Path to the hypercorn config file
    --config 'python:integrates.settings.hypercorn'
    # Time to wait after SIGTERM or Ctrl-C for any remaining requests
    --graceful-timeout 45
    # Seconds to keep inactive connections alive before closing. [5]
    --keep-alive "${keep_alive_timeout}"
    # The type of workers to use. [asyncio]
    --worker-class 'uvloop'
  )

  local local_config=(
    "${common_config[@]}"
    # SSL certificate file
    --certfile="${cert}/cert.crt"
    # Enable debug mode, i.e. extra logging and checks
    --debug
    # SSL key file
    --keyfile="${cert}/cert.key"
    # Enable automatic reloads on code changes
    --reload
    # The number of worker processes for handling requests
    --workers 1
  )

  local k8s_config=(
    "${common_config[@]}"
    # The number of worker processes for handling requests
    --workers "${recommended_workers}"
  )

  case "${env}" in
    dev | prod-local)
      config=("${local_config[@]}")
      ;;
    eph | prod)
      config=("${k8s_config[@]}")
      ;;
    *)
      error "First argument must be one of: dev, eph, prod, prod-local"
      ;;
  esac
  # kill_port 8001
  hypercorn "${config[@]}" "integrates.app.app:APP"
}

function serve_daemon {
  { serve "${@}" & }
  # wait_port 300 localhost:8001
}

function main {
  export DAEMON="${DAEMON:-false}"

  case "${DAEMON-}" in
    true) serve_daemon "${@}" ;;
    *) serve "${@}" ;;
  esac
}

main "${@}"
