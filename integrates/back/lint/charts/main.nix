{ inputs, makeScript, projectPath, ... }:
makeScript {
  name = "integrates-back-lint-charts";
  entrypoint = ./entrypoint.sh;
  replace = {
    __argNPMPath__ = projectPath "/integrates/back/lint/charts/npm";
    __argSrcPath__ =
      projectPath "/integrates/back/integrates/app/templates/static/graphics";
  };
  searchPaths.bin = [ inputs.nixpkgs.nodejs_20 ];
}
