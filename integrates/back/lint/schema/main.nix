{ inputs, makeScript, projectPath, ... }:
makeScript {
  name = "integrates-back-lint-schema";
  entrypoint = ./entrypoint.sh;
  searchPaths.bin =
    [ inputs.nixpkgs.git inputs.nixpkgs.openssh inputs.nixpkgs.nodejs_20 ];
  replace = {
    __argNPMPath__ = projectPath "/integrates/back/lint/schema/npm";
  };
}
