# Production
resource "aws_s3_bucket" "integrates_continuos_repositories" {
  bucket = "integrates.continuous-repositories"

  tags = {
    "Name"              = "integrates.continuous-repositories"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
    "NOFLUID"           = "f335_due_to_generation_significant_volume_data"
  }
}

resource "aws_s3_bucket_logging" "integrates_continuos_repositories" {
  bucket = aws_s3_bucket.integrates_continuos_repositories.id

  target_bucket = "common.logging"
  target_prefix = "log/integrates"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "integrates_continuos_repositories" {
  bucket = aws_s3_bucket.integrates_continuos_repositories.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "integrates_continuos_repositories" {
  bucket = aws_s3_bucket.integrates_continuos_repositories.id

  versioning_configuration {
    // NOFLUID Logs are disabled due to their generation of a significant volume of data.
    status = "Disabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "continuous_repositories_lifecycle" {
  bucket = aws_s3_bucket.integrates_continuos_repositories.id
  rule {
    id     = "RemoveOldRepositories"
    status = "Enabled"

    expiration {
      days = 10
    }

    filter {}
  }
}
