# Production
resource "aws_s3_bucket" "integrates" {
  bucket = "integrates"

  tags = {
    "Name"              = "integrates"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_s3_bucket_public_access_block" "integrates" {
  bucket = aws_s3_bucket.integrates.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_acl" "integrates" {
  bucket = aws_s3_bucket.integrates.id

  acl = "private"
}

resource "aws_s3_bucket_logging" "integrates" {
  bucket = aws_s3_bucket.integrates.id

  target_bucket = "common.logging"
  target_prefix = "log/integrates"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "integrates" {
  bucket = aws_s3_bucket.integrates.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "integrates" {
  bucket = aws_s3_bucket.integrates.id

  rule {
    id     = "analytics"
    status = "Enabled"

    filter {
      prefix = "analytics/"
    }
    noncurrent_version_expiration {
      noncurrent_days = 14
    }
    expiration {
      days = 14
    }
  }
  rule {
    id     = "reports"
    status = "Enabled"

    filter {
      prefix = "reports/"
    }
    expiration {
      # 1 month + some timezone skews
      days = 32
    }
  }
}

resource "aws_s3_bucket_versioning" "integrates" {
  bucket = aws_s3_bucket.integrates.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_cors_configuration" "integrates" {
  bucket = aws_s3_bucket.integrates.id

  cors_rule {
    allowed_headers = ["*"]
    // NOFLUID put its a method needed for upload files to aws S3.
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["https://app.fluidattacks.com", "https://localhost:*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }
}

# Development

resource "aws_s3_bucket" "storage_dev" {
  bucket = "integrates.dev"

  tags = {
    "Name"              = "integrates.dev"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
    "NOFLUID"           = "f335_due_to_generation_significant_volume_data"
  }
}

resource "aws_s3_bucket_public_access_block" "storage_dev" {
  bucket = aws_s3_bucket.storage_dev.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_acl" "storage_dev" {
  bucket = aws_s3_bucket.storage_dev.id

  acl = "private"
}

resource "aws_s3_bucket_logging" "storage_dev" {
  bucket = aws_s3_bucket.storage_dev.id

  target_bucket = "common.logging"
  target_prefix = "log/integrates.dev"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "storage_dev" {
  bucket = aws_s3_bucket.storage_dev.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "storage_dev" {
  bucket = aws_s3_bucket.storage_dev.id

  rule {
    id     = "analytics"
    status = "Enabled"

    filter {
      prefix = "analytics/"
    }
    noncurrent_version_expiration {
      noncurrent_days = 14
    }
    expiration {
      days = 14
    }
  }
  rule {
    id     = "reports"
    status = "Enabled"

    filter {
      prefix = "reports/"
    }
    expiration {
      # 1 month + some timezone skews
      days = 32
    }
  }
}

resource "aws_s3_bucket_versioning" "storage_dev" {
  bucket = aws_s3_bucket.storage_dev.id

  versioning_configuration {
    // NOFLUID Logs are disabled due to their generation of a significant volume of data.
    status = "Disabled"
  }
}

resource "aws_s3_bucket_cors_configuration" "storage_dev" {
  bucket = aws_s3_bucket.storage_dev.id

  cors_rule {
    allowed_headers = ["*"]
    // NOFLUID put its a method needed for upload files to aws S3.
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["https://*.app.fluidattacks.com", "https://localhost:*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }
}
