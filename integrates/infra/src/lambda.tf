variable "aws_aurora_endpoint" {
  type    = string
  default = ""
}
variable "aws_dynamodb_host" {
  type = string
}
variable "aws_opensearch_host" {
  type = string
}
variable "azure_oauth2_issues_secret" {
  type = string
}
variable "azure_oauth2_issues_secret_old" {
  type = string
}
variable "bugsnag_api_key_streams" {
  type = string
}
variable "ci_commit_sha" {
  type = string
}
variable "gitlab_issues_oauth2_app_id" {
  type = string
}
variable "gitlab_issues_oauth2_secret" {
  type = string
}
variable "google_chat_webhook_url" {
  type = string
}
variable "webhook_poc_key" {
  type = string
}
variable "webhook_poc_org" {
  type = string
}
variable "webhook_poc_url" {
  type = string
}

resource "aws_security_group" "integrates_lambda" {
  name    = "integrates_lambda"
  vpc_id  = data.aws_vpc.main.id
  ingress = []
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name"              = "integrates_lambda"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
    "NOFLUID"           = "f024_allow_egress_to_reach_anywhere"
  }
}

data "aws_iam_policy_document" "integrates_lambda_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    sid     = "AssumeRolePolicy"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "integrates_lambda" {
  assume_role_policy = data.aws_iam_policy_document.integrates_lambda_assume.json
  name               = "integrates_lambda"

  tags = {
    "Name"              = "integrates_lambda"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

data "aws_iam_policy_document" "integrates_lambda_policy" {
  statement {
    actions   = ["rds-db:connect"]
    effect    = "Allow"
    resources = ["arn:aws:rds-db:${var.region}:${var.accountId}:dbuser:*/integrates_lambda"]
    sid       = "auroraPolicy"
  }

  statement {
    actions = [
      "dynamodb:BatchWriteItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:Query",
      "dynamodb:UpdateItem",
    ]
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*",
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*/index/*"
    ]
    sid = "dbPolicy"
  }


  statement {
    actions   = ["kms:Decrypt"]
    effect    = "Allow"
    resources = ["arn:aws:kms:${var.region}:${var.accountId}:key/*"]
    sid       = "kmsPolicy"
  }


  statement {
    actions = [
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes",
      "sqs:ReceiveMessage",
      "sqs:SendMessage"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:sqs:${var.region}:${var.accountId}:integrates*"
    ]
    sid = "dlqPolicy"
  }

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:${var.region}:${var.accountId}:log-group:*"
    ]
    sid = "logPolicy"
  }

  statement {
    actions = [
      "ec2:DescribeNetworkInterfaces",
    ]
    effect    = "Allow"
    resources = ["*"]
    sid       = "networkPolicyAll"
  }
  statement {
    actions = [
      "ec2:AssignPrivateIpAddresses",
      "ec2:CreateNetworkInterface",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:UnassignPrivateIpAddresses"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:ec2:${var.region}:${var.accountId}:*/*",
      "arn:aws:ec2:${var.region}:${var.accountId}:subnet/*",
      "arn:aws:ec2:${var.region}:${var.accountId}:security-group/*",
    ]
    sid = "networkPolicy"
  }

  statement {
    actions = [
      "es:ESHttpPost",
    ]
    effect = "Allow"
    resources = [
      "arn:aws:es:${var.region}:${var.accountId}:domain/integrates*"
    ]
    sid = "searchPolicy"
  }

  statement {
    actions = ["aoss:DashboardsAccessAll", "aoss:APIAccessAll"]
    effect  = "Allow"
    resources = [
      "arn:aws:aoss:${var.region}:${var.accountId}:collection/*",
      "arn:aws:aoss:${var.region}:${var.accountId}:dashboards/*",
    ]
    sid = "historicSearchPolicy"
  }

  statement {
    actions = [
      "dynamodb:DescribeStream",
      "dynamodb:GetRecords",
      "dynamodb:GetShardIterator",
      "dynamodb:ListStreams"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*"
    ]
    sid = "streamsPolicy"
  }
}

resource "aws_iam_policy" "integrates_lambda_policy" {
  name   = "integrates_lambda_policy"
  policy = data.aws_iam_policy_document.integrates_lambda_policy.json
  tags = {
    "Name"              = "integrates_lambda_policy"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_iam_role_policy_attachment" "main" {
  role       = aws_iam_role.integrates_lambda.name
  policy_arn = aws_iam_policy.integrates_lambda_policy.arn
}

resource "null_resource" "pip_install_streams" {
  provisioner "local-exec" {
    command = <<EOT
      poetry export \
        --format=requirements.txt \
        --directory=../../streams \
        > ../../streams/requirements.txt

      pip install \
        --no-compile \
        --requirement ../../streams/requirements.txt \
        --target ../../streams/python/
    EOT
  }

  triggers = {
    always_run = timestamp()
  }
}

data "archive_file" "integrates_streams_deps" {
  depends_on       = [null_resource.pip_install_streams]
  output_file_mode = "0666"
  output_path      = "integrates_streams_deps.zip"
  source_dir       = "../../streams"
  type             = "zip"
}

resource "aws_lambda_layer_version" "integrates_streams_deps" {
  compatible_architectures = ["arm64"]
  compatible_runtimes      = ["python3.11"]
  filename                 = data.archive_file.integrates_streams_deps.output_path
  layer_name               = "integrates_streams_deps"
  source_code_hash         = data.archive_file.integrates_streams_deps.output_base64sha256
}

data "archive_file" "integrates_streams" {
  excludes         = ["deps"]
  output_file_mode = "0666"
  output_path      = "integrates_streams.zip"
  source_dir       = "../../streams"
  type             = "zip"
}

data "local_file" "triggers" {
  filename = "../../streams/triggers.json"
}

locals {
  triggers = {
    for key, value in jsondecode(data.local_file.triggers.content) : key => value
    if key != "$schema"
  }
}

resource "aws_lambda_function" "integrates_streams" {
  for_each = local.triggers

  architectures                  = ["arm64"]
  depends_on                     = [data.archive_file.integrates_streams]
  filename                       = data.archive_file.integrates_streams.output_path
  function_name                  = "integrates_streams_${each.key}"
  handler                        = "streams.${each.value.handler}"
  layers                         = [aws_lambda_layer_version.integrates_streams_deps.arn]
  memory_size                    = each.value.memory_size
  package_type                   = "Zip"
  reserved_concurrent_executions = each.value.reserved_concurrency
  role                           = aws_iam_role.integrates_lambda.arn
  runtime                        = "python3.11"
  source_code_hash               = data.archive_file.integrates_streams.output_base64sha256
  timeout                        = each.value.timeout

  dead_letter_config {
    target_arn = aws_sqs_queue.integrates_streams_dlq.arn
  }

  environment {
    variables = {
      AWS_DYNAMODB_HOST              = var.aws_dynamodb_host
      AWS_OPENSEARCH_HOST            = var.aws_opensearch_host
      AZURE_DEVOPS_CACHE_DIR         = "/tmp/" // https://github.com/microsoft/azure-devops-python-api/issues/90
      AZURE_OAUTH2_ISSUES_SECRET     = var.azure_oauth2_issues_secret
      AZURE_OAUTH2_ISSUES_SECRET_OLD = var.azure_oauth2_issues_secret_old
      BUGSNAG_API_KEY_STREAMS        = var.bugsnag_api_key_streams
      CI_COMMIT_SHA                  = var.ci_commit_sha
      ENABLE_TELEMETRY               = "true"
      GITLAB_ISSUES_OAUTH2_APP_ID    = var.gitlab_issues_oauth2_app_id
      GITLAB_ISSUES_OAUTH2_SECRET    = var.gitlab_issues_oauth2_secret
      GOOGLE_CHAT_WEBHOOK_URL        = var.google_chat_webhook_url
      WEBHOOK_POC_KEY                = var.webhook_poc_key
      WEBHOOK_POC_ORG                = var.webhook_poc_org
      WEBHOOK_POC_URL                = var.webhook_poc_url
    }
  }

  ephemeral_storage {
    size = 512
  }

  logging_config {
    log_format = "JSON"
  }

  dynamic "vpc_config" {
    for_each = each.value.uses_vpc ? [1] : []

    content {
      security_group_ids = [aws_security_group.integrates_lambda.id]
      subnet_ids         = [for subnet in data.aws_subnet.lambda : subnet.id]
    }
  }

  tags = {
    "Name"              = "integrates_streams_${each.key}"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_lambda_event_source_mapping" "integrates_streams" {
  for_each = local.triggers

  batch_size                         = each.value.batch_size
  bisect_batch_on_function_error     = true
  enabled                            = each.value.enabled
  event_source_arn                   = aws_dynamodb_table.integrates_vms.stream_arn
  function_name                      = aws_lambda_function.integrates_streams[each.key].arn
  maximum_batching_window_in_seconds = each.value.batch_window
  maximum_retry_attempts             = -1
  starting_position                  = "TRIM_HORIZON"

  filter_criteria {
    dynamic "filter" {
      for_each = each.value.filters

      content {
        pattern = jsonencode(filter.value)
      }
    }
  }
}


resource "null_resource" "pip_install_audit" {
  provisioner "local-exec" {
    command = <<EOT
      poetry export \
        --format=requirements.txt \
        --directory=./audit \
        > ./audit/requirements.txt

      pip install \
        --no-compile \
        --requirement ./audit/requirements.txt \
        --target ./audit/python/
    EOT
  }

  triggers = {
    always_run = timestamp()
  }
}

data "archive_file" "integrates_audit_deps" {
  depends_on  = [null_resource.pip_install_audit]
  output_path = "integrates_audit_deps.zip"
  source_dir  = "./audit"
  type        = "zip"
}

resource "aws_lambda_layer_version" "integrates_audit_deps" {
  compatible_architectures = ["arm64"]
  compatible_runtimes      = ["python3.11"]
  filename                 = data.archive_file.integrates_audit_deps.output_path
  layer_name               = "integrates_audit_deps"
  source_code_hash         = data.archive_file.integrates_audit_deps.output_base64sha256
}

data "archive_file" "integrates_audit" {
  output_path = "integrates_audit.zip"
  source_dir  = "./audit"
  type        = "zip"
}

resource "aws_lambda_function" "integrates_audit" {
  architectures    = ["arm64"]
  depends_on       = [data.archive_file.integrates_audit]
  filename         = data.archive_file.integrates_audit.output_path
  function_name    = "integrates_audit"
  handler          = "audit.handler.process"
  layers           = [aws_lambda_layer_version.integrates_audit_deps.arn]
  memory_size      = 512
  package_type     = "Zip"
  role             = aws_iam_role.integrates_lambda.arn
  runtime          = "python3.11"
  source_code_hash = data.archive_file.integrates_audit.output_base64sha256
  timeout          = 10

  environment {
    variables = {
      AWS_AURORA_ENDPOINT     = var.aws_aurora_endpoint
      BUGSNAG_API_KEY_STREAMS = var.bugsnag_api_key_streams
    }
  }

  logging_config {
    log_format = "JSON"
  }

  vpc_config {
    security_group_ids = [aws_security_group.integrates_lambda.id]
    subnet_ids         = [for subnet in data.aws_subnet.lambda : subnet.id]
  }

  tags = {
    "Name"              = "integrates_audit"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_lambda_event_source_mapping" "example" {
  event_source_arn        = aws_sqs_queue.integrates_audit.arn
  function_name           = aws_lambda_function.integrates_audit.arn
  function_response_types = ["ReportBatchItemFailures"]
}
