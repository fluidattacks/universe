resource "aws_sqs_queue" "integrates_tasks_dev" {
  name                      = "integrates_tasks_dev"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_tasks_dev"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_sqs_queue" "integrates_clone" {
  name                      = "integrates_clone"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_clone"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_snippet" {
  name                      = "integrates_snippet"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_snippet"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_sqs_queue" "integrates_llm_report" {
  name                      = "integrates_llm_report"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_llm_report"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_sqs_queue" "integrates_llm_scan" {
  name                        = "integrates_llm_scan.fifo"
  fifo_queue                  = true
  content_based_deduplication = true
  kms_master_key_id           = "alias/aws/sqs"
  visibility_timeout_seconds  = aws_lambda_function.integrates_audit.timeout * 6

  tags = {
    "Name"              = "integrates_llm_scan"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_refresh" {
  name                      = "integrates_refresh"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_refresh"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_sqs_queue" "integrates_report" {
  name                      = "integrates_report"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_report"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_sqs_queue" "integrates_report_soon" {
  name                      = "integrates_report_soon"
  delay_seconds             = 5
  max_message_size          = 16384
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_report_soon"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_streams_dlq" {
  name                      = "integrates_streams_dlq"
  delay_seconds             = 5
  max_message_size          = 262144
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  kms_master_key_id         = "alias/aws/sqs"

  tags = {
    "Name"              = "integrates_streams_dlq"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_dead_queue" {
  name                       = "integrates_dead_queue"
  delay_seconds              = 5
  max_message_size           = 2048
  message_retention_seconds  = 259200
  receive_wait_time_seconds  = 10
  fifo_queue                 = false
  kms_master_key_id          = "alias/aws/sqs"
  visibility_timeout_seconds = 1800

  tags = {
    "Name"              = "integrates_dead_queue"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_sbom" {
  name                      = "integrates_sbom"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_sbom"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}


resource "aws_sqs_queue" "integrates_packages" {
  name                      = "integrates_packages"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_packages"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_mail_sbom" {
  name                      = "integrates_mail_sbom"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false
  kms_master_key_id         = "alias/aws/sqs"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_dead_queue.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_mail_sbom"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_audit_dlq" {
  name                        = "integrates_audit_dlq.fifo"
  fifo_queue                  = true
  content_based_deduplication = true
  kms_master_key_id           = "alias/aws/sqs"
  visibility_timeout_seconds  = aws_lambda_function.integrates_audit.timeout * 6

  tags = {
    "Name"              = "integrates_audit_dlq"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sqs_queue" "integrates_audit" {
  name                        = "integrates_audit.fifo"
  content_based_deduplication = true
  deduplication_scope         = "messageGroup"
  fifo_queue                  = true
  fifo_throughput_limit       = "perMessageGroupId"
  kms_master_key_id           = "alias/aws/sqs"
  // https://docs.aws.amazon.com/lambda/latest/dg/services-sqs-configure.html#events-sqs-eventsource
  visibility_timeout_seconds = aws_lambda_function.integrates_audit.timeout * 6

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.integrates_audit_dlq.arn
    maxReceiveCount     = 3
  })
  tags = {
    "Name"              = "integrates_audit"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
