# https://github.com/fluidattacks/makes
{ inputs, libGit, makeScript, outputs, ... }:
let
  browserBin = if inputs.nixpkgs.stdenv.isLinux then
    "${inputs.nixpkgs-pw-47.playwright-driver.browsers}/chromium-1134/chrome-linux/chrome"
  else
    "${inputs.nixpkgs-pw-47.playwright-driver.browsers}/chromium-1134/chrome-mac/Chromium.app/Contents/MacOS/Chromium";
in {
  deployTerraform = {
    modules = {
      integratesDesign = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareProd"
          outputs."/secretsForTerraformFromEnv/integratesDesign"
        ];
        src = "/integrates/design/infra";
        version = "1.0";
      };
    };
  };
  inputs = {
    nixpkgs-pw-47 = let
      owner = "NixOS";
      repo = "nixpkgs";
      rev = "658e7223191d2598641d50ee4e898126768fe847";
      src = builtins.fetchTarball {
        sha256 = "0i462pchz264lnb1r1wvw0ijn9m687npp6fg2xvp14kw1j4v206c";
        url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
      };
    in import src { };
  };
  lintTerraform = {
    modules = {
      integratesDesign = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
          outputs."/secretsForTerraformFromEnv/integratesDesign"
        ];
        src = "/integrates/design/infra";
        version = "1.0";
      };
    };
  };
  secretsForTerraformFromEnv = {
    integratesDesign = {
      cloudflareAccountId = "CLOUDFLARE_ACCOUNT_ID";
      cloudflareApiKey = "CLOUDFLARE_API_KEY";
      cloudflareEmail = "CLOUDFLARE_EMAIL";
    };
  };
  testTerraform = {
    modules = {
      integratesDesign = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
          outputs."/secretsForTerraformFromEnv/integratesDesign"
        ];
        src = "/integrates/design/infra";
        version = "1.0";
      };
    };
  };
  jobs."/integrates/design" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "integrates-design";
    replace = { __argDesignBuild__ = outputs."/integrates/design/build"; };
    searchPaths = {
      bin = [
        inputs.nixpkgs.bash
        inputs.nixpkgs.git
        inputs.nixpkgs.gnused
        inputs.nixpkgs.findutils
        inputs.nixpkgs.nodejs_20
        inputs.nixpkgs-pw-47.playwright
      ] ++ inputs.nixpkgs.lib.optionals inputs.nixpkgs.stdenv.isDarwin [
        (inputs.makeImpureCmd {
          cmd = "open";
          path = "/usr/bin/open";
        })
      ];
      export = [
        [ "PLAYWRIGHT_NODEJS_PATH" "${inputs.nixpkgs.nodejs_20}/bin/node" "" ]
        [ "PLAYWRIGHT_SKIP_VALIDATE_HOST_REQUIREMENTS" "true" "" ]
        [ "PLAYWRIGHT_CHROMIUM_EXECUTABLE_PATH" "${browserBin}" "" ]
        [
          "PLAYWRIGHT_BROWSERS_PATH"
          inputs.nixpkgs-pw-47.playwright-driver.browsers
          ""
        ]
      ];
      source = [ libGit outputs."/common/utils/aws" ];
    };
  };
}
