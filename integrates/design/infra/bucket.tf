# Production

resource "aws_s3_bucket" "default" {
  bucket = "design.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}"

  tags = {
    "Name"              = "design.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
    "Access"            = "private"
    "NOFLUID"           = "f325_public_bucket"
  }
}

# Logging
resource "aws_s3_bucket_logging" "default" {
  bucket = aws_s3_bucket.default.id

  target_bucket = "common.logging"
  target_prefix = "log/design.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}"
}

# Versioning
resource "aws_s3_bucket_versioning" "default" {
  bucket = aws_s3_bucket.default.id

  versioning_configuration {
    status = "Enabled"
  }
}

# Security
resource "aws_s3_bucket_ownership_controls" "default" {
  bucket = aws_s3_bucket.default.id

  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "default" {
  bucket = aws_s3_bucket.default.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "default" {
  bucket = aws_s3_bucket.default.id

  acl = "private"
}

resource "aws_s3_bucket_website_configuration" "default" {
  bucket = aws_s3_bucket.default.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404/index.html"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "default" {
  bucket = aws_s3_bucket.default.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_cors_configuration" "default" {
  bucket = aws_s3_bucket.default.id

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = ["https://app.fluidattacks.com"]
    expose_headers  = ["GET", "HEAD"]
    max_age_seconds = 3000
  }
}

data "aws_iam_policy_document" "default" {
  statement {
    sid    = "CloudFlare"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "${aws_s3_bucket.default.arn}/*",
    ]
    condition {
      test     = "IpAddress"
      variable = "aws:SourceIp"
      values   = data.cloudflare_ip_ranges.cloudflare.cidr_blocks
    }
  }
}

resource "aws_s3_bucket_policy" "default" {
  bucket = aws_s3_bucket.default.id
  policy = data.aws_iam_policy_document.default.json
}
