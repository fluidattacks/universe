resource "cloudflare_worker_script" "default" {
  account_id = var.cloudflareAccountId
  name       = "design_headers"
  content    = file("js/headers/default.js")
}

resource "cloudflare_worker_route" "default" {
  zone_id     = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  pattern     = "design.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}/*"
  script_name = cloudflare_worker_script.default.name
}
