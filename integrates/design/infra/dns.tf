resource "cloudflare_record" "default" {
  zone_id = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  name    = "design.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}"
  type    = "CNAME"
  value   = aws_s3_bucket_website_configuration.default.website_endpoint
  proxied = true
  ttl     = 1
}
