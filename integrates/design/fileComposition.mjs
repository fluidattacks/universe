// @ts-check

import { createFileComposition } from "eslint-plugin-project-structure";

export const fileCompositionConfig = createFileComposition({
  filesRules: [
    {
      filePattern: "**/index.tsx",
      rootSelectorsLimits: [{ selector: ["interface", "type"], limit: 0 }],
    },
    {
      filePattern: "(?!@core).*/*.ts",
      rules: [
        {
          selector: ["type"],
          format: "T{PascalCase}",
        },
        {
          selector: ["interface"],
          format: "I{PascalCase}Props",
        },
      ],
    },
  ],
});
