// @ts-check

import { createFolderStructure } from "eslint-plugin-project-structure";

export const folderStructureConfig = createFolderStructure({
  rules: {
    component_directory: {
      children: [
        {
          ruleId: "component_directory",
        },
        {
          name: "index(.(stories|template))?.tsx",
        },
        {
          name: "(constants|index|styles|types|utils)?.ts",
        },
      ],
      name: "{kebab-case}",
    },
    generic_directory: {
      children: [
        {
          ruleId: "generic_directory",
        },
        {
          name: "{kebab-case}.ts",
        },
        {
          name: "index?.tsx",
        },
      ],
      name: "{kebab-case}",
    },
  },
  structure: [
    { name: "*" },
    {
      name: "src",
      children: [
        {
          name: "components",
          children: [
            {
              name: "@core",
              ruleId: "component_directory",
            },
            {
              ruleId: "component_directory",
            },
          ],
        },
        {
          name: "hooks",
          ruleId: "generic_directory",
        },
        {
          name: "utils",
          ruleId: "generic_directory",
        },
        { name: "index.ts" },
      ],
    },
  ],
});
