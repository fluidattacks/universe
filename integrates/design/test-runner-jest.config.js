const { getJestConfig } = require('@storybook/test-runner');

module.exports = {
  // The default configuration comes from @storybook/test-runner
  ...getJestConfig(),
  /** Add your own overrides below
   * @see https://jestjs.io/docs/configuration
   */
  preset: "jest-playwright-preset",
  rootDir: "../design/src/components",
  testEnvironmentOptions: {
    "jest-playwright": {
      browsers: ["chromium"],
      config: { contextOptions: { bypassCSP: true } },
      launchOptions: {
        ...(process.env.PLAYWRIGHT_CHROMIUM_EXECUTABLE_PATH
          ? {
              executablePath: process.env.PLAYWRIGHT_CHROMIUM_EXECUTABLE_PATH,
              timeout: 30000,
            }
          : {}),
      },
    },
  },
  testTimeout: 60000,
};
