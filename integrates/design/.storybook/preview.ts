import "../src/styles/global.css";
import { Preview } from "@storybook/react";
import { DocsPage } from "./DocsPage";

import { CustomThemeProvider } from "../src/components/colors";

const fluidViewports = {
  tablet: {
    name: "tablet",
    styles: {
      width: "960px",
      height: "1112px",
    },
  },
  sm: {
    name: "sm",
    styles: {
      width: "1024px",
      height: "745px",
    },
  },
  md: {
    name: "md",
    styles: {
      width: "1440px",
      height: "1064px",
    },
  },
  lg: {
    name: "lg",
    styles: {
      width: "1920px",
      height: "1490px",
    },
  },
};

const preview: Preview = {
  decorators: [(Story) => CustomThemeProvider({ children: Story() })],
  initialGlobals: {
    backgrounds: { value: "light" },
  },
  parameters: {
    backgrounds: {
      options: {
        dark: { name: "dark", value: "#333333" },
        fluid_gray: { name: "fluid_gray", value: "#e9e9ed" },
        light: { name: "light", value: "#ffffff" },
      },
    },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    docs: {
      page: DocsPage,
      source: {
        excludeDecorators: true,
        state: "open",
        type: "dynamic",
      },
    },
    options: {
      storySort: {
        method: "alphabetical",
        order: ["Introduction", "Essentials", "Components"],
      }
    },
    viewport: {
      viewports: fluidViewports,
    },
  },
};

export default preview;
