import {
  Controls,
  Description,
  DocsContext,
  DocsContextProps,
  Primary,
  Source,
  Stories,
  Subtitle,
  Title,
} from "@storybook/addon-docs";
import { ReactRenderer } from "@storybook/react";
import React, { Context, useContext } from "react";
import { styled } from "styled-components";

const ImportStatement = (): JSX.Element => {
  const context = useContext(
    DocsContext as Context<DocsContextProps<ReactRenderer>>
  );
  const defaultStory = context.componentStories()[0];
  const component = defaultStory.component?.displayName;
  const statement = `import { ${component} } from "@fluidattacks/design"`;

  return <Source dark={true} language={"typescript"} code={statement} />;
};

const SBDocsTheme = styled.div.attrs({
  className: "sb-theme",
})`
  font-family: Roboto, sans-serif;
`;

const DocsPage = (): JSX.Element => (
  <SBDocsTheme>
    <Title />
    <Subtitle />
    <Description />
    <ImportStatement />
    <Primary />
    <Controls sort={"alpha"} />
    <Stories />
  </SBDocsTheme>
);

export { DocsPage };
