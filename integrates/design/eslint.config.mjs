import tseslint from "typescript-eslint";
import pluginFunctional from "eslint-plugin-functional";
import pluginImport from "eslint-plugin-import";
import pluginJsdoc from "eslint-plugin-jsdoc";
import pluginJSX from "eslint-plugin-jsx-a11y";
import pluginPrettierRecommended from "eslint-plugin-prettier/recommended";
import { projectStructurePlugin } from "eslint-plugin-project-structure";
import pluginReact from "eslint-plugin-react";
import pluginHooks from "eslint-plugin-react-hooks";
import { fileCompositionConfig } from "./fileComposition.mjs";
import { folderStructureConfig } from "./folderStructure.mjs";

export default [
  ...tseslint.configs.recommended,
  pluginFunctional.configs.externalTypeScriptRecommended,
  pluginFunctional.configs.recommended,
  pluginFunctional.configs.stylistic,
  pluginImport.flatConfigs.recommended,
  pluginJSX.flatConfigs.strict,
  pluginPrettierRecommended,
  pluginReact.configs.flat.recommended,
  { files: ["**/*.{ts,jsx,tsx}"] },
  { files: ["**/*.js"], languageOptions: { sourceType: "script" } },
  {
    files: ["**/types.ts"],
    ignores: ["**/@core/**"],
    plugins: { jsdoc: pluginJsdoc },
    rules: {
      "jsdoc/require-jsdoc": [
        "error",
        { contexts: ["TSInterfaceDeclaration"], enableFixer: false },
      ],
    },
  },
  {
    files: ["**/*.stories.tsx"],
    rules: {
      "functional/functional-parameters": "off",
      "functional/immutable-data": "off",
    },
  },
  {
    ignores: [
      ".storybook",
      "**/dist/**",
      "**/infra",
      "**/index.d.ts",
      "**/*.mjs",
      "**/*.config.js",
      "**/*.config.ts",
    ],
  },
  {
    languageOptions: {
      parserOptions: {
        ecmaFeatures: { jsx: true },
        ecmaVersion: "es2022",
        projectService: true,
        tsconfigRootDir: import.meta.dirname,
      },
    },
  },
  {
    plugins: {
      "project-structure": projectStructurePlugin,
      "react-hooks": pluginHooks,
    },
  },
  {
    rules: {
      "@typescript-eslint/consistent-type-definitions": "error",
      "@typescript-eslint/explicit-function-return-type": "error",
      "@typescript-eslint/no-non-null-assertion": "error",
      "@typescript-eslint/prefer-nullish-coalescing": "error",
      "@typescript-eslint/prefer-optional-chain": "error",
      "functional/no-conditional-statements": "off",
      "functional/no-expression-statements": "off",
      "functional/no-mixed-types": "off",
      "functional/no-return-void": "off",
      "import/default": "error",
      "import/export": "error",
      "import/exports-last": "error",
      "import/first": "error",
      "import/group-exports": "error",
      "import/newline-after-import": "error",
      "import/no-absolute-path": ["error", { commonjs: false }],
      "import/no-cycle": ["error", { ignoreExternal: true }],
      "import/no-deprecated": "error",
      "import/no-namespace": "error",
      "import/no-self-import": "error",
      "import/no-unresolved": "error",
      "import/order": [
        "error",
        {
          alphabetize: { order: "asc", caseInsensitive: true },
          groups: ["builtin", "external", "sibling"],
          "newlines-between": "always",
        },
      ],
      "project-structure/file-composition": ["error", fileCompositionConfig],
      "project-structure/folder-structure": ["error", folderStructureConfig],
      "react/jsx-curly-brace-presence": ["error", "always"],
      "react/jsx-no-bind": [
        "error",
        {
          allowFunctions: true,
        },
      ],
      "react/jsx-sort-props": "error",
      "react/no-typos": "error",
      "react/react-in-jsx-scope": "off",
      "react-hooks/exhaustive-deps": "error",
      "react-hooks/rules-of-hooks": "error",
      "sort-imports": ["error", { ignoreDeclarationSort: true }],
    },
  },
  {
    settings: {
      "import/resolver": {
        typescript: true,
        node: true,
      },
      react: {
        version: "detect",
      },
    },
  },
];
