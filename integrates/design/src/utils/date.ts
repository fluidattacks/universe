import dayjs from "dayjs";

const formatDate = (value: string): string =>
  value && value !== "-" ? dayjs(value).format("YYYY-MM-DD") : "-";

const formatDateTime = (value: string): string =>
  value && value !== "-" ? dayjs(value).format("YYYY-MM-DD hh:mm A") : "-";

const formatDateTimeInInput = (value: string): string => {
  return value && value !== "-"
    ? dayjs(value).format("YYYY/MM/DD hh:mm A")
    : "-";
};

export { formatDate, formatDateTime, formatDateTimeInInput };
