type TNums0To7 = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;

/**
 * Little flag component props.
 * @interface ILittleFlagProps
 * @extends React.HTMLAttributes<HTMLSpanElement>
 * @property { number } [ml] - The margin left value for the little flag.
 * @property { string } [bgColor] - The background color for the little flag.
 * @property { string } [txtDecoration] - The text decoration for the little flag.
 */
interface ILittleFlagProps
  extends Partial<React.HTMLAttributes<HTMLSpanElement>> {
  ml?: TNums0To7;
  bgColor?: string;
  txtDecoration?: string;
}

export type { ILittleFlagProps };
