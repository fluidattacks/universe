import { styled } from "styled-components";

interface IStyledLittleFlagProps {
  $bgColor?: string;
  $txtDecoration?: string;
}

const LittleFlag = styled.span<IStyledLittleFlagProps>`
  ${({
    theme,
    $bgColor = theme.palette.secondary[500],
    $txtDecoration = "none",
  }): string => `
  background-color: ${$bgColor};
  border-radius: 5px;
  color: ${theme.palette.white};
  font-size: 10px;
  padding: ${theme.spacing[0.125]} ${theme.spacing[0.25]} ${theme.spacing[0.125]} ${theme.spacing[0.25]};
  text-decoration: ${$txtDecoration}
  `}
`;

export { LittleFlag };
