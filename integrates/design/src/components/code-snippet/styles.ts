import { styled } from "styled-components";

const CodeContainer = styled.div`
  ${({ theme }): string => `
    background-color: ${theme.palette.gray[100]};
    border-radius: ${theme.spacing[0.25]};
    display: inline-flex;
    padding: ${theme.spacing[1]};
    position: relative;
    width: fit-content;

    .line-highlight.language-none {
      background: ${theme.palette.gray[100]};
    }

    .line-highlight {
      background: hsl(354deg 88% 93% / 50%);
      font-family: ${theme.typography.type.mono};
      font-size: ${theme.typography.text.xs};
      line-height: ${theme.spacing[1]};
    }

    .line-highlight::before,
    .line-highlight[data-end]::after {
      background: ${theme.palette.primary[300]};
      background-color: hsl(355deg 89% 36%);
      color: ${theme.palette.white};
    }

    pre code {
      color: #161616;
      font-family: ${theme.typography.type.mono};
      font-size: ${theme.typography.text.xs};
      line-height: ${theme.spacing[1]};
    }

    pre:not(.line-highlight) {
      margin-top: unset;
      margin-bottom: unset;
      padding: unset;
      width: 100%;
    }

    span:hover {
      background-color: ${theme.palette.gray[200]};
    }

    .no-data,
    pre.line-highlight {
      background-color: ${theme.palette.gray[100]};
      padding-top: unset;
      padding-bottom: unset;
    }
  `}
`;

const ButtonsContainer = styled.div`
  ${({ theme }): string => `
    align-items: end;
    background-color: ${theme.palette.gray[100]};
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    min-width: 103px;
    margin-right: ${theme.spacing[1]};
    padding-top: ${theme.spacing[1]};
    padding-bottom: ${theme.spacing[1]};
    position: relative;
    padding-left: unset;

    button:hover:not([disabled]) {
      background-color: ${theme.palette.gray[200]};
    }
  `}
`;

export { ButtonsContainer, CodeContainer };
