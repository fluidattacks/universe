type TSnippet = "normal" | "location";

/**
 * Snippet props.
 * @interface ISnippet
 * @property {string} content - The code snippet content.
 * @property {number} offset - The offset of the code snippet in the source code.
 */
interface ISnippet {
  content: string;
  offset: number;
}

/**
 * Code snippet component props.
 * @property {string} [noCodeMessage] - Message to display when no code snippet is provided.
 * @property {ISnippet | string} snippet - The code snippet object.
 * @property {string} [specific] - Additional information related to the code snippet.
 * @property {TSnippet} [variant] - Type of snippet.
 */
interface ICodeSnippetProps {
  noCodeMessage?: string;
  snippet: ISnippet | string;
  specific?: string;
  variant?: TSnippet;
}

export type { ICodeSnippetProps };
