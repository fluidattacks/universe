import _ from "lodash";
import { highlightAll } from "prismjs";
import { useEffect } from "react";

import "prismjs/plugins/line-highlight/prism-line-highlight.js";
import "prismjs/plugins/line-highlight/prism-line-highlight.css";

import { CodeContainer } from "../styles";
import type { ICodeSnippetProps } from "../types";

const LocationCode = ({
  noCodeMessage = "Code snippet not available",
  snippet,
  specific,
}: Readonly<ICodeSnippetProps>): JSX.Element => {
  useEffect((): void => {
    highlightAll();
  }, []);

  return (
    <CodeContainer data-private={true}>
      <pre
        className={"line-highlight"}
        data-line-offset={
          _.isNil(snippet) || _.isString(snippet) ? 0 : snippet.offset + 0.7
        }
        {...(specific !== undefined
          ? { "data-line": String(Number(specific)) }
          : {})}
      >
        {_.isNil(snippet) ||
        _.isString(snippet) ||
        _.isEmpty(snippet.content) ? (
          <div className={"no-data"}>
            <p>{noCodeMessage}</p>
          </div>
        ) : (
          <code className={"language-none"}>{snippet.content}</code>
        )}
      </pre>
    </CodeContainer>
  );
};

export { LocationCode };
