import type { IFiltersListProps } from "../filters-list/types";
import type { IUseModal } from "hooks/use-modal";

/**
 * Options filters modal properties.
 * @interface ISingleColumFiltersProps
 * @property {IFiltersListProps["options"]} options - The options to display.
 * @property {IFiltersListProps["onApplyFilters"]} onApplyFilters - The handler for apply filters.
 * @property {IUseModal} modalRef - The modal reference.
 */
interface IColumFiltersProps<IData extends object>
  extends IFiltersListProps<IData> {
  modalRef: IUseModal;
}

export type { IColumFiltersProps };
