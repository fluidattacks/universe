import type { Meta, StoryFn } from "@storybook/react";
import { useEffect } from "react";

import { IColumFiltersProps } from "./types";

import { ColumFilters } from ".";
import { useModal } from "hooks/use-modal";

const config: Meta = {
  component: ColumFilters,
  tags: ["autodocs"],
  title: "Components/Modal/ColumFilters",
};

const Modal: StoryFn<Readonly<IColumFiltersProps<object>>> = (
  props,
): JSX.Element => {
  const modal = useModal("filters-modal");
  useEffect(() => {
    modal.open();
  }, [modal]);

  return (
    <div style={{ minHeight: "100vh" }}>
      <ColumFilters {...props} modalRef={modal} onApplyFilters={console.log} />
    </div>
  );
};

const Default = Modal.bind({});
Default.args = {
  options: [
    {
      label: "LOC",
      filterOptions: [
        {
          label: "LOC",
          key: (item: object): boolean =>
            (item as { type?: string }).type === "Type",
          type: "checkboxes",
          options: [
            {
              value: "LOC Option 1",
              checked: true,
            },
            {
              value: "LOC Option 2",
            },
            {
              value: "LOC Option 3",
            },
            {
              value: "LOC Option 4",
            },
            {
              value: "LOC Option 5",
              checked: true,
            },
          ],
        },
      ],
    },
    {
      label: "Status",
      filterOptions: [
        {
          label: "Status",
          key: (item: object): boolean =>
            (item as { type?: string }).type === "Type",
          type: "checkboxes",
          options: [
            {
              value: "Status Option 1",
              checked: true,
            },
            {
              value: "Status Option 2",
            },
            {
              value: "Status Option 3",
            },
            {
              value: "Status Option 4",
            },
            {
              value: "Status Option 5",
              checked: true,
            },
          ],
        },
      ],
    },
    {
      label: "Modified Date",
      filterOptions: [
        {
          label: "Modified Date",
          key: (item: object): boolean =>
            (item as { type?: string }).type === "Type",
          type: "checkboxes",
          options: [
            {
              value: "Modified Date Option 1",
              checked: true,
            },
            {
              value: "Modified Date Option 2",
            },
            {
              value: "Modified Date Option 3",
            },
            {
              value: "Modified Date Option 4",
            },
            {
              value: "Modified Date Option 5",
              checked: true,
            },
          ],
        },
      ],
    },
    {
      label: "Last Commit",
      filterOptions: [
        {
          label: "Last Commit",
          key: (item: object): boolean =>
            (item as { type?: string }).type === "Type",
          type: "checkboxes",
          options: [
            {
              value: "Last Commit Option 1",
              checked: true,
            },
            {
              value: "Last Commit Option 2",
            },
            {
              value: "Last Commit Option 3",
            },
            {
              value: "Last Commit Option 4",
            },
            {
              value: "Last Commit Option 5",
              checked: true,
            },
          ],
        },
      ],
    },
  ],
};

export default config;

export { Default };
