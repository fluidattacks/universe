import type { Property } from "csstype";
import { useEffect, useRef, useState } from "react";
import { useTheme } from "styled-components";

import { IColumFiltersProps } from "./types";

import { ModalContainer } from "../../styles";
import { FiltersList } from "../filters-list";
import { ModalWrapper } from "../styles";
import { IconButton } from "components/icon-button";

const ColumFilters = <IData extends object>({
  options,
  onApplyFilters,
  modalRef,
  variant,
}: Readonly<IColumFiltersProps<IData>>): JSX.Element => {
  const [direction, setDirection] = useState<Property.FlexDirection>("row");
  const [handler, setHandler] = useState(true);

  const theme = useTheme();
  const containerRef = useRef<HTMLDivElement>(null);
  const button = (
    <IconButton
      icon={"close"}
      iconColor={theme.palette.gray[400]}
      iconSize={"xs"}
      iconTransform={"grow-2"}
      iconType={"fa-light"}
      id={"modal-close"}
      onClick={modalRef.close}
      px={0.25}
      py={0.25}
      variant={"ghost"}
    />
  );

  useEffect((): VoidFunction | void => {
    const handleClick = (event: Readonly<MouseEvent>): void => {
      const element = containerRef.current;
      if (element && !element.contains(event.target as Node)) {
        modalRef.close();
      }
    };
    if (handler) {
      document.addEventListener("mousedown", handleClick, true);

      // eslint-disable-next-line functional/functional-parameters
      return (): void => {
        document.removeEventListener("mousedown", handleClick, true);
      };
    }
  }, [containerRef, handler, modalRef]);

  return (
    <ModalWrapper $bgColor={"transparent"} aria-label={modalRef.name}>
      <ModalContainer
        $size={"fixed"}
        alignItems={direction === "row" ? "start" : "end"}
        display={"flex"}
        flexDirection={direction}
        gap={0.5}
        id={"filters-list"}
        position={"relative"}
        px={1.25}
        py={1.25}
        ref={containerRef}
        style={{ width: "max-content" }}
      >
        {direction === "column" ? button : undefined}
        <FiltersList
          onApplyFilters={onApplyFilters}
          options={options}
          setDirection={setDirection}
          setHandler={setHandler}
          variant={variant}
        />
        {direction === "row" ? button : undefined}
      </ModalContainer>
    </ModalWrapper>
  );
};

export { ColumFilters };
