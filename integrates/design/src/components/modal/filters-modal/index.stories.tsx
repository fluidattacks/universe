import type { Meta, StoryFn } from "@storybook/react";
import { useMemo } from "react";

import { IFiltersProps } from "./types";

import { AppliedFilters, useFilters } from ".";

const config: Meta = {
  tags: ["autodocs"],
  title: "Components/Modal/Filters",
};

interface ITestData {
  fileName: string;
  LOC: string;
  status: string;
  modifiedDate: string;
  lastCommit: string;
  linesCount: number;
  type: string;
}

const Modal: StoryFn<Readonly<IFiltersProps<ITestData>>> = (
  props,
): JSX.Element => {
  const { Filters, options, removeFilter } = useFilters({ ...props });
  // Random id to avoid conflicts with other modals on storybook
  const randomId = useMemo((): string => {
    return Math.random().toString(36).substring(7);
  }, []);

  return (
    <div id={randomId} style={{ minHeight: "100vh" }}>
      <Filters />
      <AppliedFilters onClose={removeFilter} options={options} />
    </div>
  );
};

const Default = Modal.bind({});
Default.args = {
  dataset: [
    {
      fileName: "main.ts",
      LOC: "LOC Option 1",
      status: "Status Option 1",
      modifiedDate: "2023-01-15",
      lastCommit: "2023-01-10",
      linesCount: 150,
      type: "Page",
    },
    {
      fileName: "utils.ts",
      LOC: "LOC Option 2",
      status: "Status Option 2",
      modifiedDate: "2023-03-20",
      lastCommit: "2023-03-15",
      linesCount: 300,
      type: "Component",
    },
    {
      fileName: "index.tsx",
      LOC: "LOC Option 3",
      status: "Status Option 1",
      modifiedDate: "2023-06-05",
      lastCommit: "2023-06-01",
      linesCount: 200,
      type: "Utility",
    },
    {
      fileName: "types.ts",
      LOC: "LOC Option 4",
      status: "Status Option 2",
      modifiedDate: "2023-09-10",
      lastCommit: "2023-09-05",
      linesCount: 100,
      type: "Context",
    },
    {
      fileName: "constants.ts",
      LOC: "LOC Option 5",
      status: "Status Option 1",
      modifiedDate: "2023-12-25",
      lastCommit: "2023-12-20",
      linesCount: 850,
      type: "Hook",
    },
  ],
  options: [
    {
      label: "File Name",
      filterOptions: [
        {
          key: "fileName",
          label: "File name is",
          type: "text",
        },
        {
          filterFn: "includesInsensitive",
          key: "fileName",
          label: "File name contains",
          type: "text",
        },
      ],
    },
    {
      label: "LOC",
      filterOptions: [
        {
          label: "LOC",
          key: "LOC",
          type: "checkboxes",
          options: [
            { value: "LOC Option 1", checked: true },
            { value: "LOC Option 2" },
            { value: "LOC Option 3" },
            { value: "LOC Option 4" },
            { value: "LOC Option 5", checked: true },
          ],
        },
      ],
    },
    {
      label: "Status",
      filterOptions: [
        {
          label: "Status",
          key: "status",
          type: "radioButton",
          options: [
            { label: "Status Option 1", value: "Status Option 1" },
            { label: "Status Option 2", value: "Status Option 2" },
          ],
        },
      ],
    },
    {
      label: "Modified Date",
      filterOptions: [
        {
          key: "modifiedDate",
          type: "dateRange",
          label: "Modified between",
        },
      ],
    },
    {
      label: "Last Commit",
      filterOptions: [
        {
          key: "lastCommit",
          type: "dateRange",
          label: "Committed between",
        },
      ],
    },
    {
      label: "Lines Count",
      filterOptions: [
        {
          key: "linesCount",
          type: "numberRange",
          label: "Number of lines",
        },
      ],
    },
    {
      label: "Type",
      filterOptions: [
        {
          key: "type",
          label: "Select file type",
          type: "select",
          options: [
            { label: "Component", value: "Component" },
            { label: "Utility", value: "Utility" },
            { label: "Page", value: "Page" },
            { label: "Hook", value: "Hook" },
            { label: "Context", value: "Context" },
          ],
        },
      ],
      icon: "wrench",
    },
  ],
  localStorageKey: "filters-key",
  setFilteredDataset: (value): void => {
    console.log("Filtered dataset updated:", value);
  },
};

const Grouped = Modal.bind({});
Grouped.args = {
  dataset: Default.args.dataset,
  options: [
    {
      label: "File Name",
      group: "File Details",
      filterOptions: [
        {
          key: "fileName",
          label: "File name is",
          type: "text",
        },
        {
          filterFn: "includesInsensitive",
          key: "fileName",
          label: "File name contains",
          type: "text",
        },
      ],
      icon: "file-exclamation",
    },
    {
      label: "LOC",
      group: "File Details",
      filterOptions: [
        {
          label: "LOC",
          key: "LOC",
          type: "checkboxes",
          options: [
            { value: "LOC Option 1", checked: true },
            { value: "LOC Option 2" },
            { value: "LOC Option 3" },
            { value: "LOC Option 4" },
            { value: "LOC Option 5", checked: true },
          ],
        },
      ],
      icon: "file-exclamation",
    },
    {
      label: "Status",
      group: "Current State",
      filterOptions: [
        {
          label: "Status",
          key: "status",
          type: "radioButton",
          options: [
            { label: "Status Option 1", value: "Status Option 1" },
            { label: "Status Option 2", value: "Status Option 2" },
          ],
        },
      ],
    },
    {
      label: "Modified Date",
      group: "Dates",
      filterOptions: [
        {
          key: "modifiedDate",
          type: "dateRange",
          label: "Modified between",
        },
      ],
      icon: "clock",
    },
    {
      label: "Last Commit",
      group: "Dates",
      filterOptions: [
        {
          key: "lastCommit",
          type: "dateRange",
          label: "Committed between",
        },
      ],
      icon: "clock",
    },
    {
      label: "Lines Count",
      group: "Metrics",
      filterOptions: [
        {
          key: "linesCount",
          type: "numberRange",
          label: "Number of lines",
        },
      ],
      icon: "wrench",
    },
    {
      label: "Type",
      group: "Metrics",
      filterOptions: [
        {
          key: "type",
          label: "Select file type",
          type: "select",
          options: [
            { label: "Component", value: "Component" },
            { label: "Utility", value: "Utility" },
            { label: "Page", value: "Page" },
            { label: "Hook", value: "Hook" },
            { label: "Context", value: "Context" },
          ],
        },
      ],
      icon: "wrench",
    },
  ],
  localStorageKey: "grouped-filters-key",
  setFilteredDataset: Default.args.setFilteredDataset,
  variant: "grouped",
};

export default config;
export { Default, Grouped };
