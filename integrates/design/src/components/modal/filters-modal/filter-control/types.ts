import { Control, UseFormRegister, UseFormSetValue } from "react-hook-form";

import type { IFormFieldVales } from "../filter-form/types";
import type { IFilterOptions } from "../types";

/**
 * Interface representing the props for the FilterControl component.
 * @interface IFilterControlProps
 * @property {IFilterOptions} options - filter options.
 * @property {UseFormRegister<IFormFieldValues>} register - Function to register form fields.
 */
interface IFilterControlProps<IData extends object> {
  control: Control<IFormFieldVales>;
  option: IFilterOptions<IData>;
  register: UseFormRegister<IFormFieldVales>;
  setValue?: UseFormSetValue<IFormFieldVales>;
}

export type { IFilterControlProps };
