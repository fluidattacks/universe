import { IOptionsProps } from "../filters-list";
import type { IFilterOptions } from "../types";
import { IListItemProps } from "components/list-item/types";

/**
 * Interface representing an applied filter.
 * @interface IAppliedFilter
 * @property {IListItemProps["icon"]} [icon] - Optional icon for the filter.
 * @property {IFilterOptions<object>} filter - The filter options.
 * @property {string} [filterValue] - The optional filter value.
 * @property {string} label - The label for the filter.
 * @property {function} onClose - Function to handle closing/resetting the filter.
 */
interface IAppliedFilter<IData extends object> {
  icon?: IListItemProps["icon"];
  filter: IFilterOptions<IData>;
  filterValue?: string;
  label: string;
  onClose: (filterToRemove: Readonly<IFilterOptions<IData>>) => () => void;
}

/**
 * Interface representing applied filters.
 * @interface IAppliedFilters
 * @property {IOptionsProps<object>[]} options - Array of options.
 * @property {object[]} [dataset] - Optional array of data objects.
 * @property {function} onClose - Function to handle closing/resetting filters.
 */
interface IAppliedFilters<IData extends object> {
  options: IOptionsProps<IData>[];
  onClose: (filterToRemove: Readonly<IFilterOptions<IData>>) => () => void;
}

export type { IAppliedFilter, IAppliedFilters };
