import { useCallback, useEffect, useState } from "react";
import { useTheme } from "styled-components";

import { StyledList } from "./styles";
import { IFiltersListProps, IOptionsProps } from "./types";

import { FilterForm } from "../filter-form";
import { IFormFieldVales } from "../filter-form/types";
import { IFilterOptions } from "../types";
import { Container } from "components/container";
import { IListItemProps } from "components/list-item/types";
import { groupOptions } from "components/modal/utils";
import { Tag } from "components/tag";
import { Text } from "components/typography";

const FiltersList = <IData extends object>({
  options,
  variant,
  onApplyFilters,
  setDirection,
  setHandler,
}: Readonly<IFiltersListProps<IData>>): JSX.Element => {
  const theme = useTheme();
  const [items, setItems] = useState<IOptionsProps<IData>[]>(options);
  const [filterOptions, setFilterOptions] = useState<IFilterOptions<IData>[]>(
    [],
  );
  const [appliedFilters, setAppliedFilters] = useState<
    Record<string, readonly string[]>
  >({});
  const [selectedFilter, setSelectedFilter] = useState<string>("");

  const onClickHandler = useCallback(
    (label: string): VoidFunction => {
      // eslint-disable-next-line functional/functional-parameters
      return (): void => {
        const selected = items.find((option) => option.label === label);
        const enableHandler = !selected?.filterOptions.some(
          (option): boolean =>
            option.type === "dateRange" || option.type === "select",
        );
        setHandler?.(enableHandler ?? true);
        setSelectedFilter(label);
        setFilterOptions(selected?.filterOptions ?? []);
      };
    },
    [items, setHandler],
  );

  const onApplyHandler = useCallback(
    (filters: Readonly<IFormFieldVales>): void => {
      const selectedOptions = [
        ...filters.checkbox,
        ...Object.values(filters.text ?? {}),
        ...(filters.radio ? [filters.radio] : []),
        ...(filters.minValue ? [String(filters.minValue)] : []),
        ...(filters.maxValue ? [String(filters.maxValue)] : []),
      ];

      // upsert key and its applied filters on applyFilters Record
      setAppliedFilters((prev) => {
        return selectedOptions.length > 0
          ? { ...prev, [selectedFilter]: selectedOptions }
          : prev;
      });
      // update checked status on filterOptions for the selected filter
      const updateFilterOptions = (
        fOptions: Readonly<IFilterOptions<IData>>,
      ): IFilterOptions<IData> => {
        return {
          ...fOptions,
          options: fOptions.options?.map((option) => ({
            ...option,
            checked:
              filters.checkbox.includes(option.value) ||
              filters.radio === option.value,
          })),
          value: ["text", "select"].includes(fOptions.type)
            ? (filters.text?.[
                `${String(fOptions.key)}-${
                  fOptions.filterFn ?? "caseInsensitive"
                }`
              ] ?? fOptions.value)
            : fOptions.value,
          minValue: ["numberRange", "dateRange"].includes(fOptions.type)
            ? filters.minValue
            : fOptions.minValue,
          maxValue: ["numberRange", "dateRange"].includes(fOptions.type)
            ? filters.maxValue
            : fOptions.maxValue,
        };
      };

      const updateItems = (
        item: Readonly<IOptionsProps<IData>>,
      ): IOptionsProps<IData> =>
        item.label === selectedFilter
          ? {
              ...item,
              filterOptions: item.filterOptions.map(updateFilterOptions),
            }
          : item;

      const updatedItems = items.map(updateItems);
      setItems((prev) => prev.map(updateItems));

      const filterOptions = updatedItems.flatMap(
        ({ filterOptions }) => filterOptions,
      );
      // call onApplyFilters with recently applied filters
      onApplyFilters(filterOptions);
      // reset selected filter and filter options
      setFilterOptions([]);
    },
    [items, onApplyFilters, selectedFilter],
  );

  const setListItem = useCallback(
    (label: string, icon: IListItemProps["icon"]): JSX.Element => {
      const key = `li-filter-option-${label}`;

      return (
        <StyledList
          icon={icon}
          key={key}
          onClick={onClickHandler(label)}
          style={{ minWidth: "180px" }}
          value={key}
          width={"100%"}
        >
          {label}
          {appliedFilters[label] === undefined ? undefined : (
            <Tag tagLabel={appliedFilters[label].length.toString()} />
          )}
        </StyledList>
      );
    },
    [appliedFilters, onClickHandler],
  );

  useEffect(() => {
    const initialFilters = options.reduce(
      (acc, { label, filterOptions }): Record<string, readonly string[]> => {
        const valuesApplyFilters = filterOptions
          .flatMap(({ value, minValue, maxValue }) => [
            minValue,
            maxValue,
            value,
          ])
          .filter(Boolean);

        const optionApplyFilters = filterOptions
          .flatMap(({ options }) => options ?? [])
          .filter(({ checked = false }) => checked)
          .map(({ value }) => value);

        const updatedFilterOptions = [
          ...valuesApplyFilters,
          ...optionApplyFilters,
        ];

        if (updatedFilterOptions.length === 0) return acc;

        return {
          ...acc,
          [label]: updatedFilterOptions,
        };
      },
      {},
    );
    setAppliedFilters(initialFilters);
    if (filterOptions.length > 0 && setDirection) setDirection("column");
  }, [filterOptions, options, setDirection]);

  if (variant === "grouped") {
    return (
      <Container
        display={"flex"}
        flexDirection={"column"}
        flexGrow={1}
        maxHeight={"360px"}
        style={{ columnGap: "10px" }}
        width={"100%"}
        wrap={"wrap"}
      >
        {filterOptions.length > 0 ? (
          <FilterForm
            id={"filters-options"}
            onSubmit={onApplyHandler}
            options={filterOptions}
          />
        ) : (
          Object.entries(groupOptions(options)).map(
            ([group, items]): JSX.Element => (
              <Container key={group}>
                <Text color={theme.palette.gray[400]} mb={0.25} size={"sm"}>
                  {group}
                </Text>
                {items.map(
                  ({ label, icon = "filter-list" }): JSX.Element =>
                    setListItem(label, icon),
                )}
              </Container>
            ),
          )
        )}
      </Container>
    );
  }

  return (
    <Container
      display={"flex"}
      flexDirection={"column"}
      flexGrow={1}
      maxHeight={filterOptions.length > 0 ? "auto" : "200px"}
      style={{ columnGap: "10px" }}
      width={"100%"}
      wrap={"wrap"}
    >
      {filterOptions.length > 0 ? (
        <FilterForm
          id={"filters-options"}
          onSubmit={onApplyHandler}
          options={filterOptions}
        />
      ) : (
        options.map(
          ({ label, icon = "filter-list" }): JSX.Element =>
            setListItem(label, icon),
        )
      )}
    </Container>
  );
};

export { FiltersList };
export type { IOptionsProps };
