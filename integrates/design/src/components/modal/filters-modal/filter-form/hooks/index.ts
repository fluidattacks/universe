import { useState } from "react";
import { useForm } from "react-hook-form";

import { IUseFilterForm } from "./types";

import { IFilterOptions } from "../../types";
import { IFormFieldVales } from "../types";

const useFilterForm = <IData extends object>(
  options: ReadonlyArray<IFilterOptions<IData>>,
): IUseFilterForm<IData> => {
  const [items, setItems] = useState<IFilterOptions<IData>[]>([...options]);

  const { control, register, handleSubmit, setValue } =
    useForm<IFormFieldVales>({
      defaultValues: {
        checkbox: options
          .filter((option) => option.type === "checkboxes")
          .flatMap(
            (option) =>
              option.options
                ?.filter((opt) => opt.checked)
                .map((opt) => opt.value) ?? [],
          ),
        radio:
          options
            .find((option) => option.type === "radioButton")
            ?.options?.find(({ checked }) => checked)?.value ?? "",
        text: options
          .filter((option) => ["text", "select"].includes(option.type))
          .reduce(
            (acc, option) => ({
              ...acc,
              [`${String(option.key)}-${option.filterFn ?? "caseInsensitive"}`]:
                option.value ?? "",
            }),
            {},
          ),
        minValue: options.find((option) =>
          ["numberRange", "dateRange"].includes(option.type),
        )?.minValue,
        maxValue: options.find((option) =>
          ["numberRange", "dateRange"].includes(option.type),
        )?.maxValue,
      },
    });

  return {
    control,
    items,
    setItems,
    register,
    handleSubmit,
    setValue,
  };
};

export { useFilterForm };
