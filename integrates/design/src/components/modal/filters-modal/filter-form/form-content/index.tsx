import { IFilterFormContentProps } from "./types";

import FilterControl from "../../filter-control";

export const FilterFormContent = <IData extends object>({
  control,
  items,
  register,
  setValue,
}: Readonly<IFilterFormContentProps<IData>>): JSX.Element => (
  <>
    {items.map((option, index) => {
      const key = `${option.type}-${option.value}-${option.filterFn ?? "default"}`;
      return (
        <div key={key}>
          <FilterControl
            control={control}
            option={option}
            register={register}
            setValue={setValue}
          />
          {index < items.length - 1 && (
            <hr style={{ margin: "1.25rem 0 0.625rem" }} />
          )}
        </div>
      );
    })}
  </>
);
