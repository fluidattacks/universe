import _ from "lodash";

import {
  formatCheckValues,
  formatDateRange,
  formatLabel,
  formatNumberRange,
  formatValue,
} from "./utils";

import { AppliedFilter } from "../applied-filter";
import type { IAppliedFilters } from "../applied-filter/types";
import { Container } from "components/container";

const AppliedFilters = <IData extends object>({
  options,
  onClose,
}: Readonly<IAppliedFilters<IData>>): JSX.Element | null => {
  const activeFilters = options
    .flatMap((option) =>
      option.filterOptions.map((filter): JSX.Element | null => {
        const { filterFn, label, type, value, options, minValue, maxValue } =
          filter;

        switch (type) {
          case "numberRange": {
            const formattedValue = formatNumberRange(minValue, maxValue);

            return (
              <AppliedFilter
                filter={filter}
                filterValue={formattedValue}
                icon={option.icon}
                key={label}
                label={option.label}
                onClose={onClose}
              />
            );
          }
          case "dateRange": {
            const formattedValue = formatDateRange(
              minValue?.toString(),
              maxValue?.toString(),
            );

            return (
              <AppliedFilter
                filter={filter}
                filterValue={formattedValue}
                icon={option.icon}
                key={label}
                label={option.label}
                onClose={onClose}
              />
            );
          }

          case "radioButton":
          case "checkboxes": {
            const formattedCheckValues = formatCheckValues(options);

            return (
              <AppliedFilter
                filter={filter}
                filterValue={formattedCheckValues}
                icon={option.icon}
                key={label}
                label={option.label}
                onClose={onClose}
              />
            );
          }

          case undefined:
          case "select":
          case "text":
          default: {
            const formattedValue = formatValue(value);
            const formattedLabel = formatLabel(type, option.label, filterFn);

            return (
              <AppliedFilter
                filter={filter}
                filterValue={formattedValue}
                icon={option.icon}
                key={label}
                label={formattedLabel}
                onClose={onClose}
              />
            );
          }
        }
      }),
    )
    .filter((filter): boolean => {
      if (filter === null) return false;

      const { filterValue } = filter.props;

      return !_.isNil(filterValue) && !_.isEmpty(String(filterValue));
    });

  return activeFilters.length > 0 ? (
    <Container
      alignItems={"center"}
      display={"flex"}
      gap={0.25}
      pb={1}
      width={"100%"}
      wrap={"wrap"}
    >
      {activeFilters}
    </Container>
  ) : null;
};

export { AppliedFilters };
