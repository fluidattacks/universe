import type { Meta, StoryFn } from "@storybook/react";
import React, { useEffect } from "react";
import { MemoryRouter, Route, Routes } from "react-router-dom";

import type { IModalProps } from "./types";

import { Modal } from ".";
import { useModal } from "../../hooks/use-modal";
import { IconButton } from "../icon-button";

const mockClick = (): void => {
  // Mock click function without any implementation
};

const config: Meta = {
  component: Modal,
  parameters: {
    controls: {
      exclude: ["_portal", "modalRef", "children"],
    },
  },
  tags: ["autodocs"],
  title: "Components/Modal",
};

const Template: StoryFn<
  React.PropsWithChildren<IModalProps & { id: string }>
> = (props: Readonly<IModalProps & { id: string }>): JSX.Element => {
  const { id } = props;
  const modal = useModal(id);

  useEffect((): void => {
    modal.open();
  }, [modal]);

  return (
    <div className={"h-[600px]"}>
      <MemoryRouter initialEntries={["/"]}>
        <Routes>
          <Route
            element={<Modal {...props} modalRef={modal} size={"sm"} />}
            path={"/"}
          />
        </Routes>
      </MemoryRouter>
    </div>
  );
};

const Default = Template.bind({});
Default.args = {
  _portal: false,
  cancelButton: {
    onClick: mockClick,
    text: "Cancel",
  },
  confirmButton: {
    onClick: mockClick,
    text: "Confirm",
  },
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  id: "default",
  title: "Heading modal",
};

const ImageModal = Template.bind({});
ImageModal.args = {
  _portal: false,
  confirmButton: {
    onClick: mockClick,
    text: "Confirm",
  },
  content: {
    imageSrc: "integrates/resources/onlyDesktop",
  },
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  id: "dontshow",
  otherActions: (
    <IconButton
      icon={"copy"}
      iconColor={"#2f394b"}
      iconSize={"xs"}
      iconType={"fa-light"}
      px={0.25}
      py={0.25}
      variant={"ghost"}
    />
  ),
  size: "md",
  title: "Heading modal",
};

const ImageTwoActions = Template.bind({});
ImageTwoActions.args = {
  _portal: false,
  cancelButton: {
    onClick: mockClick,
    text: "Cancel",
  },
  confirmButton: {
    onClick: mockClick,
    text: "Confirm",
  },
  content: {
    imageFramed: true,
    imageSrc: "integrates/resources/onlyDesktop",
    imageText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  },
  id: "twoactions",
  size: "md",
  title: "Heading modal",
};

const WitThabs = Template.bind({});
WitThabs.args = {
  _portal: false,
  cancelButton: {
    onClick: mockClick,
    text: "Cancel",
  },
  confirmButton: {
    onClick: mockClick,
    text: "Confirm",
  },
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  children: (
    <div>
      <p>{"Tab content"}</p>
    </div>
  ),
  tabItems: [
    {
      id: "tab1",
      label: "Tab 1",
      link: "/",
    },
    {
      id: "tab2",
      label: "Tab 2",
      link: "#",
    },
    {
      id: "tab3",
      label: "Tab 3",
      link: "#",
    },
  ],
  id: "withtabs",
  size: "md",
  title: "Heading modal",
};

export default config;
export { Default, ImageModal, ImageTwoActions, WitThabs };
