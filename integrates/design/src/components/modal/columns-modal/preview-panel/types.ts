import { IColumnModalProps } from "hooks/use-columns-modal";

/**
 * @interface IPreviewPanelProps
 * @property { TSortableItem } items - The items to display
 * @property { Function } onReorder - The function to call when the list is reordered
 * @property { string } title - The title of the component
 * @property { string } [description] - The description of the component
 * @property { Function } [onClickClose] - The function to call when the close button is clicked
 */
interface IPreviewPanelProps {
  items: IColumnModalProps[];
  onReorder: (newOrder: Readonly<IColumnModalProps[]>) => void;
  title: string;
  description?: string;
  onClickClose: (value: string) => void;
}

export type { IPreviewPanelProps };
