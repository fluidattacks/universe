import styled from "styled-components";

import { ListItem } from "components/list-item";

const StyledReorderItem = styled(ListItem)`
  margin-top: 10px;
  border: 1px solid ${({ theme }): string => theme.palette.gray[300]};
  border-radius: 4px;
  min-width: unset;

  svg {
    color: ${({ theme }): string => theme.palette.gray[400]};
  }

  &[aria-disabled="true"] {
    background: ${({ theme }): string => theme.palette.gray[100]};
    color: ${({ theme }): string => theme.palette.gray[500]};
  }

  &[aria-selected="true"]:not([aria-disabled="true"]) {
    background-color: unset;
  }
`;

export { StyledReorderItem };
