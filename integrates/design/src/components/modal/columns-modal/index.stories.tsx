import type { Meta, StoryFn } from "@storybook/react";
import { expect, screen, userEvent, within } from "@storybook/test";
import { useEffect, useMemo } from "react";

import { IColumnsModalProps } from "./types";

import { ColumnsModal } from ".";
import {
  type IColumnModalProps,
  useColumnsModal,
} from "../../../hooks/use-columns-modal";

const config: Meta = {
  component: ColumnsModal,
  tags: ["autodocs"],
  title: "Components/Modal/ColumsModal",
};

const Modal: StoryFn<Readonly<IColumnsModalProps>> = (props): JSX.Element => {
  const items = useMemo(
    (): IColumnModalProps[] => [
      {
        id: "column1",
        locked: true,
        name: "Column 1",
        visible: true,
        group: "Group 1",
      },
      {
        id: "column2",
        locked: true,
        name: "Column 2",
        visible: true,
        group: "Group 1",
      },
      {
        id: "column3",
        locked: false,
        name: "Column 3",
        visible: false,
        group: "Group 1",
      },
      {
        id: "column4",
        locked: false,
        name: "Column 4",
        visible: true,
        group: "Group 2",
      },
      {
        id: "column5",
        locked: false,
        name: "Column 5",
        visible: true,
        group: "Group 3",
      },
      {
        id: "column6",
        locked: false,
        name: "Column 6",
        visible: true,
        group: "Group 3",
      },
    ],
    [],
  );
  const modal = useColumnsModal({
    columnOrder: ["column1", "column2", "column4", "column5", "column6"],
    name: "test",
    items,
  });

  // Random id to avoid conflicts with other modals on storybook
  const randonId = useMemo((): string => {
    return Math.random().toString(36).substring(7);
  }, []);
  useEffect((): void => {
    modal.open();
  }, [modal]);

  return <ColumnsModal {...props} _containerId={randonId} modalRef={modal} />;
};

const Default = Modal.bind({});
Default.args = {
  title: "Columns Modal",
  variant: "ungrouped",
};

Default.play = async ({ step }): Promise<void> => {
  const searchPanel = screen.getByTestId("columns-visibility");

  await step("should render checked/visible columns", async () => {
    const checkedColums = [
      "Column 1",
      "Column 2",
      "Column 4",
      "Column 5",
      "Column 6",
    ];

    checkedColums.forEach((column) => {
      expect(screen.getByRole("listitem", { name: column }));
    });
  });

  await step("search columns", async () => {
    await userEvent.type(
      screen.getByRole("searchbox", { name: "" }),
      "Column 1",
    );
    expect(
      within(searchPanel).getByRole("listitem", {
        name: "li-checkbox-Column 1",
      }),
    ).toHaveAttribute("aria-selected", "true");
    expect(
      within(searchPanel).queryByRole("listitem", {
        name: "li-checkbox-Column 2",
      }),
    ).not.toBeInTheDocument();
    expect(
      within(searchPanel).queryByRole("listitem", {
        name: "li-checkbox-Column 3",
      }),
    ).not.toBeInTheDocument();
  });
};

const Grouped = Modal.bind({});
Grouped.args = {
  title: "Columns Modal",
  variant: "grouped",
};

export default config;

export { Default, Grouped };
