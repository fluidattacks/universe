import { useCallback, useRef, useState } from "react";
import { createPortal } from "react-dom";
import { useTheme } from "styled-components";

import { IColumnsModalProps } from "./types";

import { ModalContainer, ModalWrapper } from "../styles";
import { Button } from "components/button";
import { Container } from "components/container";
import { Divider } from "components/divider";
import { IconButton } from "components/icon-button";
import { PreviewPanel } from "components/modal/columns-modal/preview-panel";
import { SearchPanel } from "components/modal/columns-modal/search-panel";
import { Heading } from "components/typography";

const ColumnsModal = ({
  _containerId = "portals",
  title,
  modalRef,
  variant,
}: Readonly<IColumnsModalProps>): JSX.Element => {
  const theme = useTheme();
  const scrollRef = useRef<HTMLDivElement>(null);
  const [height, setHeight] = useState<number | undefined>(undefined);

  const onChangeHandler = useCallback(
    (event: Readonly<React.ChangeEvent<HTMLInputElement>>): void => {
      const { checked, value } = event.target;

      const columns = modalRef.columns.map((option) => {
        if (option.name === value) {
          option.toggleVisibility?.(checked);
          return { ...option, visible: checked };
        }

        return option;
      });

      modalRef.setColumns(columns);
    },
    [modalRef],
  );

  const onClickCloseHandler = useCallback(
    (value: string): void => {
      const newVisibility = modalRef.columns.map((option) => {
        if (option.id === value) {
          option.toggleVisibility?.(false);
          return { ...option, visible: false };
        }

        return option;
      });

      modalRef.setColumns(newVisibility);
    },
    [modalRef],
  );

  const onScroll = useCallback((): void => {
    const currentHeight = scrollRef.current?.scrollHeight;
    if (height !== currentHeight) {
      setHeight(currentHeight);
    }
  }, [height]);

  if (modalRef.isOpen) {
    return createPortal(
      <ModalWrapper aria-label={modalRef.name} aria-modal={"true"}>
        <ModalContainer $size={"md"} flexDirection={"column"}>
          <Container
            display={"flex"}
            justify={"space-between"}
            px={1.25}
            py={1.25}
          >
            <Heading size={"sm"}>{title}</Heading>
            <IconButton
              icon={"close"}
              iconColor={theme.palette.gray[400]}
              iconSize={"xs"}
              iconTransform={"grow-2"}
              iconType={"fa-light"}
              id={"modal-close"}
              onClick={modalRef.close}
              px={0.25}
              py={0.25}
              variant={"ghost"}
            />
          </Container>
          <Divider mb={0} mt={0} />
          <Container
            display={"flex"}
            gap={1.25}
            onScroll={onScroll}
            px={1.25}
            ref={scrollRef}
            scroll={"y"}
            width={"100%"}
          >
            <SearchPanel
              columns={modalRef.columns}
              formId={"columns-visibility"}
              onChangeHandler={onChangeHandler}
              variant={variant}
            />
            <Container
              borderLeft={`2px solid ${theme.palette.gray[100]}`}
              height={height ? `${height}px` : "auto"}
            />
            <PreviewPanel
              description={"Click and drag to reorder the columns"}
              items={modalRef.columns}
              onClickClose={onClickCloseHandler}
              onReorder={modalRef.setColumns}
              title={"Reorder columns"}
            />
          </Container>
          <Divider mb={0} mt={0} />
          <Container
            display={"flex"}
            justify={"space-between"}
            px={1.25}
            py={1.25}
          >
            <Button
              disabled={!modalRef.hasChanged}
              form={"columns-visibility"}
              id={"columns-modal-cancel"}
              justify={"center"}
              onClick={modalRef.resetToDefault}
              type={"reset"}
              variant={"tertiary"}
            >
              {"Reset to default"}
            </Button>
            <Button
              id={"columns-modal-submit"}
              justify={"center"}
              onClick={modalRef.close}
              variant={"primary"}
            >
              {"Save"}
            </Button>
          </Container>
        </ModalContainer>
      </ModalWrapper>,
      document.getElementById(_containerId) ?? document.body,
    );
  }

  return <div />;
};

export { ColumnsModal };
