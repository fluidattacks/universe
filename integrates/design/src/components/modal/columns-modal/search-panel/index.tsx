import { useCallback, useEffect, useRef, useState } from "react";

import { StyledList } from "./styles";
import { ISearchPanelProps } from "./types";

import { CardWithOptions } from "components/card/card-with-options";
import { Checkbox } from "components/checkbox";
import { Container } from "components/container";
import { groupOptions } from "components/modal/utils";
import { Search } from "components/search";

const SearchPanel = ({
  columns,
  formId,
  onChangeHandler,
  variant,
}: Readonly<ISearchPanelProps>): JSX.Element => {
  const searchFormRef = useRef<HTMLFormElement>(null);
  const [items, setItems] = useState<ISearchPanelProps["columns"]>([]);
  const [searchValue, setSearchValue] = useState<string>("");

  const onSearchHandler = useCallback(
    (event: Readonly<React.ChangeEvent<HTMLInputElement>>): void => {
      const { value } = event.target;
      setSearchValue(value);
    },
    [],
  );

  const onResetHandler = useCallback((): void => {
    if (searchFormRef.current) {
      searchFormRef.current.reset();
      setSearchValue("");
    }
  }, [searchFormRef]);

  useEffect(() => {
    setItems(
      columns.filter((option) =>
        variant === "grouped"
          ? option.name.toLowerCase().includes(searchValue.toLowerCase()) ||
            option.group?.includes(searchValue.toLowerCase())
          : option.name.toLowerCase().includes(searchValue.toLowerCase()),
      ),
    );
  }, [columns, searchValue, variant]);

  if (variant === "grouped") {
    return (
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={0.625}
        height={"100%"}
        id={"columns-buttons"}
        py={1.25}
        width={"55%"}
      >
        <form id={"search-form"} ref={searchFormRef}>
          <Search
            name={"search"}
            onChange={onSearchHandler}
            onClear={onResetHandler}
            value={searchValue}
          />
        </form>
        <form
          data-testid={formId}
          id={formId}
          onReset={onResetHandler}
          style={{
            display: "flex",
            flexDirection: "column",
            gap: "0.625rem",
          }}
        >
          {Object.entries(groupOptions(items)).map(
            ([group, props], index): JSX.Element => {
              const key = `li-checkbox${group}`;

              return (
                <CardWithOptions
                  checkboxesName={group}
                  defaultCollapsed={index === 0}
                  key={key}
                  onChange={onChangeHandler}
                  options={props}
                  title={group}
                />
              );
            },
          )}
        </form>
      </Container>
    );
  }

  return (
    <Container height={"100%"} id={"columns-buttons"} py={1.25} width={"55%"}>
      <form id={"search-form"} ref={searchFormRef}>
        <Search
          name={"search"}
          onChange={onSearchHandler}
          onClear={onResetHandler}
          value={searchValue}
        />
      </form>
      <form
        data-testid={formId}
        id={formId}
        onReset={onResetHandler}
        style={{
          paddingTop: "0.625rem",
        }}
      >
        {items.map((option): JSX.Element => {
          const key = `li-checkbox-${option.name}`;

          return (
            <StyledList
              key={key}
              selected={option.visible}
              value={key}
              width={"content"}
            >
              <Checkbox
                checked={option.visible}
                disabled={option.locked}
                label={option.name}
                name={option.name}
                onChange={onChangeHandler}
                value={option.name}
              />
            </StyledList>
          );
        })}
      </form>
    </Container>
  );
};

export { SearchPanel };
