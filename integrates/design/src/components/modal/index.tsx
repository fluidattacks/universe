import _ from "lodash";
import { PropsWithChildren } from "react";
import { createPortal } from "react-dom";
import { useTheme } from "styled-components";

import { ModalFooter } from "./footer";
import { Header } from "./header";
import { ModalConfirm } from "./modal-confirm";
import {
  ImageContainer,
  ModalContainer,
  ModalContent,
  ModalWrapper,
} from "./styles";
import type { IModalProps } from "./types";

import { CloudImage } from "components/cloud-image";
import { Container } from "components/container";
import { Tabs } from "components/tabs";
import { Text } from "components/typography";

const Modal = ({
  _portal = true,
  alert,
  cancelButton = undefined,
  children = undefined,
  confirmButton = undefined,
  content = undefined,
  description = undefined,
  modalRef,
  otherActions = undefined,
  onClose = undefined,
  tabItems = undefined,
  title = undefined,
  size,
  id = "modal-container",
}: Readonly<PropsWithChildren<IModalProps>>): JSX.Element | null => {
  const theme = useTheme();

  const modal: JSX.Element = (
    <ModalWrapper
      aria-label={modalRef.name}
      aria-modal={"true"}
      id={"modal-wrapper"}
    >
      <ModalContainer
        $size={size}
        data-testid={id}
        flexDirection={"column"}
        id={id}
      >
        {!_.isEmpty(title) && !_.isNil(title) && (
          <Header
            description={description}
            modalRef={modalRef}
            onClose={onClose}
            otherActions={otherActions}
            title={title}
          />
        )}
        {tabItems && (
          <Container
            borderTop={`1px solid ${theme.palette.gray[300]}`}
            display={"block"}
            mb={1.25}
            position={"sticky"}
          >
            <Tabs items={tabItems} />
          </Container>
        )}
        {(_.isObject(content) || children) && (
          <ModalContent>
            {children}
            {_.isObject(content) && _.isString(content.imageSrc) && (
              <ImageContainer $framed={content.imageFramed}>
                <CloudImage alt={"modal-img"} publicId={content.imageSrc} />
              </ImageContainer>
            )}
            {_.isObject(content) && _.isString(content.imageText) && (
              <Text size={"sm"}>{content.imageText}</Text>
            )}
          </ModalContent>
        )}
        <ModalFooter
          alert={alert}
          cancelButton={cancelButton}
          confirmButton={confirmButton}
          modalRef={modalRef}
        />
      </ModalContainer>
    </ModalWrapper>
  );

  if (_portal && modalRef.isOpen) {
    return createPortal(
      modal,
      document.getElementById("portals") ?? document.body,
    );
  }
  if (modalRef.isOpen) {
    return modal;
  }

  return null;
};

export { Modal, ModalConfirm, ModalFooter };
export { ColumnsModal } from "./columns-modal";
export { ColumFilters } from "./filters-modal/column-filters";
export { AppliedFilters, useFilters, formatCheckValues } from "./filters-modal";
export type { IFilterOptions, IUseFilterProps } from "./filters-modal";
export type { IOptionsProps } from "./filters-modal/filters-list";
