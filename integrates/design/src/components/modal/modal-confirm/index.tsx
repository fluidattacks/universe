import type { IModalConfirmProps } from "../types";
import { Button } from "components/button";
import { Container } from "components/container";

const ModalConfirm = ({
  disabled,
  id = "modal-confirm",
  onCancel,
  onConfirm = "submit",
  txtCancel,
  txtConfirm,
}: Readonly<IModalConfirmProps>): JSX.Element => {
  const isSubmit = onConfirm === "submit";

  return (
    <Container
      display={"flex"}
      gap={0.75}
      maxWidth={onCancel ? "392px" : "190px"}
      mt={1.5}
    >
      {onCancel ? (
        <Button
          id={`${id}-cancel`}
          justify={"center"}
          onClick={onCancel}
          variant={"tertiary"}
          width={"100%"}
        >
          {txtCancel ?? "Cancel"}
        </Button>
      ) : undefined}
      <Button
        disabled={disabled}
        id={id}
        justify={"center"}
        onClick={isSubmit ? undefined : onConfirm}
        type={isSubmit ? "submit" : "button"}
        variant={"primary"}
        width={"100%"}
      >
        {txtConfirm ?? "Confirm"}
      </Button>
    </Container>
  );
};

export { ModalConfirm };
