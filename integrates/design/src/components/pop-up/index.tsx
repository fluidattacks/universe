import { isObject } from "lodash";
import { PropsWithChildren, useCallback } from "react";
import { createPortal } from "react-dom";
import { useTheme } from "styled-components";

import { Description } from "./description";
import type { IPopUpProps } from "./types";

import { Button } from "components/button";
import { CloudImage } from "components/cloud-image";
import { Container } from "components/container";
import { Heading } from "components/typography";

const PopUp = ({
  _portal = false,
  cancelButton,
  darkBackground = false,
  children,
  confirmButton,
  container,
  description,
  highlightDescription = "",
  image,
  maxWidth,
  title,
  titleColor,
}: Readonly<PropsWithChildren<IPopUpProps>>): JSX.Element => {
  const theme = useTheme();
  const { alt, src, width = "77px", height = "77px" } = image;

  const handleCancel = useCallback((): void => {
    cancelButton?.onClick();
  }, [cancelButton]);
  const handleConfirm = useCallback((): void => {
    confirmButton?.onClick();
  }, [confirmButton]);

  const component = (
    <Container
      alignItems={"center"}
      aria-label={title}
      aria-modal={"true"}
      bgColor={darkBackground ? "rgba(52, 64, 84, 70%)" : ""}
      display={"flex"}
      height={"100vh"}
      justify={"center"}
      left={"0"}
      position={_portal ? "fixed" : "absolute"}
      top={"0px"}
      width={"100%"}
      zIndex={99999}
    >
      <Container
        bgColor={theme.palette.white}
        border={`1px solid ${theme.palette.gray[200]}`}
        borderRadius={"8px"}
        display={"inline-flex"}
        flexDirection={"column"}
        maxWidth={maxWidth}
        pb={2.5}
        pl={2.5}
        pr={2.5}
        pt={2.5}
        shadow={"lg"}
        width={"100%"}
        zIndex={11}
      >
        <Container
          alignItems={"center"}
          display={"flex"}
          flexDirection={"column"}
          width={"100%"}
        >
          <Container>
            <CloudImage
              alt={alt}
              height={height}
              publicId={src}
              width={width}
            />
          </Container>
          <Container
            display={"flex"}
            flexDirection={"column"}
            gap={0.5}
            mb={1.5}
            mt={1.5}
          >
            <Heading
              color={titleColor}
              fontWeight={"bold"}
              lineSpacing={2.5}
              size={"lg"}
              textAlign={"center"}
            >
              {title}
            </Heading>
            <Description
              description={description}
              highlightDescription={highlightDescription}
            />
          </Container>
        </Container>
        {children && (
          <Container mb={1} scroll={"y"}>
            <Container height={"100%"}>{children}</Container>
          </Container>
        )}
        <Container
          alignItems={"flex-start"}
          display={"flex"}
          gap={1}
          justify={"center"}
        >
          {isObject(confirmButton) ? (
            <Button
              key={confirmButton?.key}
              onClick={handleConfirm}
              variant={confirmButton?.variant ?? "primary"}
            >
              {confirmButton?.text}
            </Button>
          ) : undefined}
          {isObject(cancelButton) ? (
            <Button
              key={cancelButton?.key}
              onClick={handleCancel}
              variant={cancelButton?.variant ?? "tertiary"}
            >
              {cancelButton?.text}
            </Button>
          ) : undefined}
        </Container>
      </Container>
    </Container>
  );

  if (_portal) {
    return createPortal(component, container ?? document.body);
  }

  return component;
};

export { PopUp };
