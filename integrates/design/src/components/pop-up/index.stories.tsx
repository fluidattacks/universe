import type { Meta, StoryFn } from "@storybook/react";

import type { IPopUpProps } from "./types";

import { PopUp } from ".";

const mockClick = (): void => {
  "test";
};

const config: Meta = {
  component: PopUp,
  parameters: {
    controls: {
      exclude: ["_portal"],
    },
  },
  tags: ["autodocs"],
  title: "Components/PopUp",
};

const Template: StoryFn<IPopUpProps> = (
  props: Readonly<IPopUpProps>,
): JSX.Element => {
  return (
    <div style={{ height: "500px", position: "relative" }}>
      <PopUp {...props} />
    </div>
  );
};

const Default = Template.bind({});
Default.args = {
  _portal: false,
  cancelButton: {
    onClick: mockClick,
    text: "Get help",
  },
  confirmButton: {
    onClick: mockClick,
    text: "Add payment method",
  },
  description:
    "Please add a Payment Method to continue using our platform or contact our team for more information.",
  image: {
    alt: "Interdit",
    src: "integrates/resources/interdit",
  },
  maxWidth: "598px",
  title: "Access Denied",
};

const HighlightToSide = Template.bind({});
HighlightToSide.args = {
  _portal: false,
  cancelButton: {
    onClick: mockClick,
    text: "Cancel",
  },
  confirmButton: {
    onClick: mockClick,
    text: "Go to the platform",
  },
  container: document.getElementById("pop-up-container-1"),
  darkBackground: true,
  description: " is already registered in our Fluid Attack's platform.",
  highlightDescription: "yourmail@yourmail.com",
  image: {
    alt: "freeTrialError",
    height: "80px",
    src: "integrates/resources/memberIcon",
    width: "80px",
  },
  maxWidth: "740px",
  title: "Oops!",
};

export default config;
export { Default, HighlightToSide };
