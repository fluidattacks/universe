import type { TVariant } from "components/button/types";

/**
 * Pop up button props.
 * @interface IButtonProps
 * @property { string } [key] - The name of the button.
 * @property { Function } onClick - The function to call when the button is clicked
 * @property { string } text - The text of the button.
 * @property { TVariant } [variant] - The variant of the button.
 */
interface IButtonProps {
  key?: string;
  onClick: () => void;
  text: string;
  variant?: TVariant;
}

/**
 * Pop up image props.
 * @interface IImageProps
 * @property { string } alt - The alt of the image.
 * @property { string } src - The URL of the image.
 * @property { string } [height] - The height of the image.
 * @property { string } [width] - The weight of the image.
 */
interface IImageProps {
  alt: string;
  src: string;
  height?: string;
  width?: string;
}

/**
 * Pop up component props.
 * @interface IPopUpProps
 * @property { boolean } [_portal] - Whether the pop up should be rendered in a portal.
 * @property { boolean } [darkBackground] - Whether the pop up should have a dark background.
 * @property { IButtonProps } [cancelButton] - The cancel button of the pop up.
 * @property { IButtonProps } [confirmButton] - The confirm button of the pop up.
 * @property { HTMLElement | null } [container] - The container of the pop up.
 * @property { IImageProps } image - The image of the pop up.
 * @property { string } title - The title of the pop up.
 * @property { string } [titleColor] - The color of the title.
 * @property { string } description - The description of the pop up.
 * @property { string[] | string } [highlightDescription] - The highlight description of the pop up.
 * @property { string } [maxWidth] - The max width of the pop up.
 */
interface IPopUpProps {
  _portal?: boolean;
  darkBackground?: boolean;
  cancelButton?: IButtonProps;
  confirmButton?: IButtonProps;
  container?: HTMLElement | null;
  image: IImageProps;
  title: string;
  titleColor?: string;
  description: string;
  highlightDescription?: string[] | string;
  maxWidth?: string;
}

export type { IPopUpProps };
