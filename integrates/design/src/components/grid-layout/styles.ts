import { styled } from "styled-components";

import { IPaddingModifiable } from "components/@core";

interface IStyledGridProps {
  $columns?: number;
  $px?: IPaddingModifiable["px"];
  $py?: IPaddingModifiable["py"];
}

const Grid = styled.div`
  display: grid;
  grid-template: "sidebar content" 50vh / minmax(72px, 200px) auto;
  transition: all 0.25s ease-in-out;
`;

const GridSidebar = styled.div`
  grid-area: sidebar;
  height: 100%;
`;

const GridContent = styled.div`
  display: grid;
  margin: 0 24px;
  gap: 24px;
  grid-area: content;
  grid-template-columns: repeat(12, minmax(30px, auto));

  > * {
    grid-column: span 12;
  }

  ${({ theme }): string => `
    @media (width <= ${theme.breakpoints.sm}) {
      grid-template-columns: repeat(6, minmax(30px, auto));

      > * {
        grid-column: span 6;
      }
    }
  `}
`;

const DebugContainer = styled.div`
  grid-column: span 1;
  height: 100%;
  background-color: ${({ theme }): string => theme.palette.primary[50]};
`;

// Website Grid Container for all resolutions
const StyledGrid = styled.div<IStyledGridProps>`
  ${({ theme, $columns, $py = 0, $px = 0 }): string => `
      display: grid;
      gap: ${theme.spacing[1]};
      height: 100%;
      padding-top: ${theme.spacing[$py]};
      padding-bottom: ${theme.spacing[$py]};
      padding-left: ${theme.spacing[$px]};
      padding-right: ${theme.spacing[$px]};
      width: ${$columns ? "auto" : "var(--grid-width)"};
      grid-template: auto;
      grid-template-columns: repeat(
        ${$columns ?? "var(--grid-columns)"}, minmax(30px, auto)
      );

      @media screen
        and (min-width: ${theme.breakpoints.tablet})
        and (max-width: ${theme.breakpoints.md})
      {
        --grid-columns: 8;
        --grid-width: 1280px;
      }

      @media screen
        and (min-width: ${theme.breakpoints.mobile})
        and (max-width: ${theme.breakpoints.tablet})
      {
        --grid-columns: 6;
        --grid-width: 800px;
      }

      @media screen and (max-width: ${theme.breakpoints.mobile}) {
        --grid-columns: 4;
        --grid-width: 320px;
      }
    `}
`;

export { Grid, GridSidebar, GridContent, DebugContainer, StyledGrid };
