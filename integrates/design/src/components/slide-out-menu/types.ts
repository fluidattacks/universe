import { IIconModifiable } from "components/@core";

/**
 * Menu item component props.
 * @interface IMenuItemProps
 * @extends IIconModifiable
 * @property { JSX.Element } [customBadge] - Custom badge element.
 * @property { string } description - Description for the menu item.
 * @property { Function } onClick - Event handler for the menu item click.
 * @property { boolean } [requiresUpgrade] - Indicates if the menu item requires upgrade.
 * @property { string } title - Title for the menu item.
 */
interface IMenuItemProps extends Pick<IIconModifiable, "icon"> {
  customBadge?: JSX.Element;
  description: string;
  onClick: () => void;
  requiresUpgrade?: boolean;
  title: string;
}

/**
 * Slide out menu component props.
 * @interface ISlideOutMenuProps
 * @property { string } [closeIconId] - Icon ID for the close button.
 * @property { boolean } isOpen - Indicates if the slide out menu is open.
 * @property { IMenuItemProps[] } items - Menu items to be displayed.
 * @property { Function } onClose - Event handler for the close button click.
 * @property { string } [primaryButtonText] - Primary button text.
 * @property { Function } [primaryOnClick] - Event handler for the primary button click.
 * @property { string } [secondaryButtonText] - Secondary button text.
 * @property { Function } [secondaryOnClick] - Event handler for the secondary button click.
 * @property { string } title - Title for the slide out menu.
 */
interface ISlideOutMenuProps {
  closeIconId?: string;
  isOpen: boolean;
  items?: IMenuItemProps[];
  onClose: () => void;
  primaryButtonText?: string;
  primaryOnClick?: () => void;
  secondaryButtonText?: string;
  secondaryOnClick?:
    | (() => void)
    | ((event: Readonly<React.FormEvent>) => void);
  title: string;
}

export type { ISlideOutMenuProps, IMenuItemProps };
