import type { ReactNode } from "react";

import { IDisplayModifiable, IIconModifiable } from "components/@core";

type TPlace = "bottom" | "left" | "right" | "top";
type TEffect = "float" | "solid";

/**
 * Tooltip component props
 * @interface ITooltipProps
 * @extends IIconModifiable
 * @extends IDisplayModifiable
 * @property {boolean} [disabled] - If the tooltip icon is disabled.
 * @property {TEffect} [effect] - Tooltip visual effect.
 * @property {boolean} [hide] - If the tooltip has a hideout event.
 * @property {string} id - The tooltip id, required for it to work.
 * @property {ReactNode} [nodeTip] - The tooltip content when is not plain text.
 * @property {TPlace} [place] - Where to locate the tooltip from parent node.
 * @property {string} [tip] - The tooltip content, only as string.
 * @property {title} [title] - The tooltip title.
 */
interface ITooltipProps
  extends Partial<IIconModifiable>,
    Pick<IDisplayModifiable, "display" | "height" | "maxWidth" | "width"> {
  disabled?: boolean;
  effect?: TEffect;
  hide?: boolean;
  id: string;
  nodeTip?: ReactNode;
  place?: TPlace;
  tip?: string;
  title?: string;
}

export type { ITooltipProps, TPlace };
