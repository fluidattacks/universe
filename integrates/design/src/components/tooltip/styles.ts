import { styled } from "styled-components";

import { BaseComponent } from "components/@core";

const TooltipBox = styled(BaseComponent)``;

const StyledTooltip = styled.div<{ $maxWidth?: string }>`
  ${({ theme, $maxWidth = "320px" }): string => `
    background-color: ${theme.palette.gray[700]};
    border-radius: 8px;
    color: ${theme.palette.white};
    font-size: ${theme.typography.text.xs};
    max-width: ${$maxWidth};
    padding: ${theme.spacing[0.75]};
    width: max-content;
    z-index: 999999;
  `}
`;

export { TooltipBox, StyledTooltip };
