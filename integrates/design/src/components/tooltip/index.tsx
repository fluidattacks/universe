import {
  autoUpdate,
  flip,
  offset,
  shift,
  useFloating,
  useFocus,
  useHover,
  useInteractions,
  useRole,
} from "@floating-ui/react";
import { isEmpty } from "lodash";
import type { PropsWithChildren } from "react";
import { StrictMode, useState } from "react";
import { createPortal } from "react-dom";
import { useTheme } from "styled-components";

import { StyledTooltip, TooltipBox } from "./styles";
import type { ITooltipProps } from "./types";

import { Icon } from "components/icon";
import { Text } from "components/typography";

const Tooltip = ({
  children,
  disabled = false,
  display = "inline-block",
  height,
  icon,
  iconColor,
  iconSize,
  id,
  maxWidth,
  nodeTip,
  place = "bottom",
  tip = "",
  title = "",
  hide = tip === "" && title === "" && nodeTip === undefined,
  width,
}: Readonly<PropsWithChildren<ITooltipProps>>): JSX.Element => {
  const theme = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const { refs, floatingStyles, context } = useFloating({
    middleware: [
      flip({
        fallbackAxisSideDirection: "start",
      }),
      offset({ mainAxis: 10 }),
      shift(),
    ],
    onOpenChange: setIsOpen,
    open: isOpen,
    placement: place,
    whileElementsMounted: autoUpdate,
  });

  const hover = useHover(context, {
    delay: { close: 0, open: 500 },
    move: false,
  });
  const focus = useFocus(context);
  const role = useRole(context, { role: icon ? "label" : "tooltip" });

  const { getReferenceProps, getFloatingProps } = useInteractions([
    hover,
    focus,
    role,
  ]);

  if (disabled) {
    return <StrictMode>{children}</StrictMode>;
  }

  return (
    <>
      <TooltipBox
        {...getReferenceProps()}
        display={display}
        height={height}
        id={id}
        ref={refs.setReference}
        width={width}
      >
        {icon ? (
          <Icon
            clickable={true}
            disabled={disabled}
            icon={icon}
            iconColor={iconColor}
            iconSize={iconSize ?? "xxs"}
            iconType={isOpen ? "fa-solid" : "fa-light"}
          />
        ) : (
          children
        )}
      </TooltipBox>
      {isOpen &&
        !hide &&
        createPortal(
          <StyledTooltip
            {...getFloatingProps()}
            $maxWidth={maxWidth}
            id={`floating-${id}`}
            ref={refs.setFloating}
            style={floatingStyles}
          >
            {isEmpty(title) ? undefined : (
              <Text
                color={theme.palette.white}
                fontWeight={"bold"}
                lineSpacing={1.5}
                size={"sm"}
                textAlign={"start"}
                wordWrap={"break-word"}
              >
                {title}
              </Text>
            )}
            {isEmpty(tip) ? undefined : (
              <Text
                color={theme.palette.gray[100]}
                lineSpacing={1.5}
                size={"sm"}
                textAlign={"start"}
                wordWrap={"break-word"}
              >
                {tip}
              </Text>
            )}
            {isEmpty(nodeTip) ? undefined : nodeTip}
          </StyledTooltip>,
          document.getElementById("root") ?? document.body,
        )}
    </>
  );
};

export { Tooltip };
