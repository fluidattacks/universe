type TSize = "lg" | "md" | "sm";
type TColor = "red" | "white";

/**
 * Loading component props.
 * @interface ILoadingProps
 * @property { TColor } [color] - Color of the loading circle.
 * @property { string } [label] - Label to display inside the loading circle.
 * @property { TSize } [size] - Size of the loading circle
 */
interface ILoadingProps {
  color?: TColor;
  label?: string;
  size?: TSize;
}

export type { ILoadingProps, TColor, TSize };
