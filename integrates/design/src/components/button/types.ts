import type { ButtonHTMLAttributes } from "react";

import type {
  IBorderModifiable,
  IDisplayModifiable,
  IIconModifiable,
  IMarginModifiable,
  IPaddingModifiable,
  ITextModifiable,
  TMode,
} from "components/@core";
import type { TPlace } from "components/tooltip/types";

type TVariant = "primary" | "secondary" | "tertiary" | "ghost" | "link";

/**
 * Button component props.
 * @interface IButtonProps
 * @extends IBorderModifiable
 * @extends IDisplayModifiable
 * @extends IMarginModifiable
 * @extends IPaddingModifiable
 * @extends IIconModifiable
 * @extends ITextModifiable
 * @extends ButtonHTMLAttributes<HTMLButtonElement>
 * @property {boolean} [external] - Whether the link should open in a new tab.
 * @property {string} [href] - Link URL for link variant.
 * @property {boolean} [showArrow] - Show arrow icon.
 * @property {string} [tag] - The content of the tag to display on button.
 * @property {string} [tooltip] - Tooltip message.
 * @property {TPlace} [tooltipPlace] - Tooltip message relative place.
 * @property {boolean} [underline] - Text underlined for link variant.
 * @property {TVariant} [variant] - The button variant type.
 */
interface IButtonProps
  extends IBorderModifiable,
    IDisplayModifiable,
    IMarginModifiable,
    IPaddingModifiable,
    Partial<IIconModifiable>,
    Pick<ITextModifiable, "fontSize">,
    ButtonHTMLAttributes<HTMLButtonElement> {
  external?: boolean;
  href?: string;
  showArrow?: boolean;
  tag?: string;
  tooltip?: string;
  tooltipPlace?: TPlace;
  underline?: boolean;
  variant?: TVariant;
}

export type { IButtonProps, TMode, TVariant };
