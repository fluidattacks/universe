import type { Ref } from "react";
import { forwardRef, useCallback } from "react";

import { StyledButton } from "./styles";
import type { IButtonProps } from "./types";

import { Icon } from "components/icon";
import { Tag } from "components/tag";
import { Tooltip } from "components/tooltip";
import { openUrl } from "utils/open-url";

const Button = forwardRef(function Button(
  {
    children,
    disabled = false,
    external = false,
    fontSize,
    href,
    id,
    icon,
    name,
    onClick,
    showArrow = false,
    tag,
    type = "button",
    tooltip,
    tooltipPlace = "bottom",
    underline = false,
    value,
    variant = "primary",
    width,
    ...props
  }: Readonly<IButtonProps>,
  ref: Ref<HTMLButtonElement>,
): JSX.Element {
  const hasTooltip = tooltip !== undefined;
  const tooltipId = `${id}-tooltip`;

  const handleClickLink = useCallback((): void => {
    if (href) {
      openUrl(href, external);
    }
  }, [external, href]);

  const button = (
    <StyledButton
      $size={fontSize}
      $variant={variant}
      aria-disabled={disabled}
      className={underline ? "underline" : undefined}
      disabled={disabled}
      id={id}
      name={name}
      onClick={variant === "link" ? handleClickLink : onClick}
      ref={ref}
      type={type}
      value={value}
      width={width}
      {...(hasTooltip ? { "data-tooltip-id": tooltipId } : {})}
      {...props}
    >
      {icon !== undefined && (
        <Icon icon={icon} iconSize={"xs"} iconType={"fa-light"} />
      )}
      {children}
      {(showArrow || variant === "link") && (
        <Icon icon={"arrow-right"} iconSize={"xs"} iconType={"fa-light"} />
      )}
      {tag === undefined ? null : <Tag tagLabel={tag} variant={"default"} />}
    </StyledButton>
  );
  return hasTooltip ? (
    <Tooltip
      disabled={!hasTooltip}
      id={tooltipId}
      place={tooltipPlace}
      tip={tooltip}
      width={width}
    >
      {button}
    </Tooltip>
  ) : (
    button
  );
});

export type { IButtonProps };
export { Button };
