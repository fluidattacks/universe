import type { Meta, StoryFn } from "@storybook/react";

import type { IPriorityScoreProps } from "./types";

import { PriorityScore } from ".";

const config: Meta = {
  component: PriorityScore,
  tags: ["autodocs"],
  title: "Components/PriorityScore",
};

const Template: StoryFn<IPriorityScoreProps> = (props): JSX.Element => {
  return <PriorityScore {...props} />;
};

const Default = Template.bind({});
Default.args = {
  score: 36.44,
};

export { Default };
export default config;
