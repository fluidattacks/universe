/**
 * Priority score component props.
 * @interface IPriorityScoreProps
 * @property { number } score - The priority score.
 */
interface IPriorityScoreProps {
  score: number;
}

export type { IPriorityScoreProps };
