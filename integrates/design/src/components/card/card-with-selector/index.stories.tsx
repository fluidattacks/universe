import type { Meta, StoryFn } from "@storybook/react";

import { CardWithSelector } from ".";
import type { ICardWithSelectorProps } from "../types";

const config: Meta = {
  component: CardWithSelector,
  parameters: { a11y: { disable: true } },
  tags: ["autodocs"],
  title: "Components/Cards/CardWithSelector",
};

const Default: StoryFn<Readonly<ICardWithSelectorProps>> = (): JSX.Element => (
  <CardWithSelector
    imageId={"integrates/login/sidebarLogo"}
    selectorProps={{ name: "company", value: "FluidAttacks" }}
    title={"Fluid"}
  />
);

export default config;
export { Default };
