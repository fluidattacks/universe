import type { Meta, StoryFn } from "@storybook/react";
import { useCallback, useState } from "react";

import { CardWithSwitch } from ".";
import type { IToggleProps } from "../../toggle/types";
import type { ICardWithSwitchProps } from "../types";

const config: Meta = {
  component: CardWithSwitch,
  tags: ["autodocs"],
  title: "Components/Cards/CardWithSwitch",
};

const Template: StoryFn<ICardWithSwitchProps> = ({
  toggles,
  ...props
}: Readonly<ICardWithSwitchProps>): JSX.Element => {
  const [checked, setChecked] = useState([
    ...toggles.map((t): boolean => t.defaultChecked === true),
  ]);

  const handleChange = useCallback(
    (idx: number): (() => void) =>
      (): void => {
        setChecked((checks): boolean[] => {
          checks[idx] = !checks[idx];

          return [...checks];
        });
      },
    [],
  );

  return (
    <div className={"inline-flex flex-col"}>
      <CardWithSwitch
        {...props}
        toggles={toggles.map(
          (t, idx): IToggleProps => ({
            ...t,
            defaultChecked: checked[idx],
            onChange: handleChange(idx),
          }),
        )}
      />
    </div>
  );
};

const Default = Template.bind({});
Default.args = {
  minWidth: "319px",
  title: "Event",
  toggles: [
    {
      defaultChecked: false,
      disabled: false,
      justify: "space-between",
      leftDescription: "Email",
      name: "toggle",
      value: "test",
    },
  ],
};

const withTwoToggles = Template.bind({});
withTwoToggles.args = {
  minWidth: "319px",
  title: "Many events",
  toggles: [
    {
      defaultChecked: false,
      disabled: false,
      justify: "space-between",
      leftDescription: "Email",
      name: "toggle",
      value: "test",
    },
    {
      defaultChecked: true,
      disabled: false,
      justify: "space-between",
      leftDescription: "SMS",
      name: "toggle",
      value: "test",
    },
  ],
};

export default config;
export { Default, withTwoToggles };
