import type { Meta, StoryObj } from "@storybook/react";

import { CardWithInput } from ".";
import { Container } from "../../container";
import type { ICardWithInputProps } from "../types";

const config: Meta = {
  component: CardWithInput,
  parameters: { a11y: { disable: true } },
  tags: ["autodocs"],
  title: "Components/Cards/CardWithInput",
};

const CardInputTemplate: StoryObj<{
  props: ICardWithInputProps;
}> = {
  render: ({ props }): JSX.Element => {
    return (
      <Container display={"flex"} gap={1} width={"400px"} wrap={"wrap"}>
        <CardWithInput {...props} minWidth={"100%"} />
      </Container>
    );
  },
};

const Default = {
  ...CardInputTemplate,
  args: {
    props: {
      description:
        "Each input is a form, so each card must be handled as such.",
      name: "defaultInput",
    },
  },
};

const WithTitle = {
  ...CardInputTemplate,
  args: {
    props: {
      description:
        "Each input is a form, so each card must be handled as such.",
      name: "defaultInput",
      title: "Title",
    },
  },
};

const WithIcon = {
  ...CardInputTemplate,
  args: {
    props: {
      description: "This is a description with an icon.",
      name: "withIcon",
      placeholder: "Enter a input",
    },
  },
};

const InputAlignStart = {
  ...CardInputTemplate,
  args: {
    props: {
      align: "start",
      description:
        "It is possible to induce the application's server into\nmaking requests to an arbitrary domain.",
      name: "textAlignStart",
      placeholder: "Enter a input",
    },
  },
};

const NumberInput = {
  ...CardInputTemplate,
  args: {
    props: {
      description:
        "Each input is a form, so each card must be handled as such.",
      inputType: "number",
      name: "numberInput",
      placeholder: "Enter a number",
    },
  },
};

const DateInput = {
  ...CardInputTemplate,
  args: {
    props: {
      description:
        "Each input is a form, so each card must be handled as such.",
      inputType: "date",
      name: "dateInput",
    },
  },
};

export {
  Default,
  WithTitle,
  WithIcon,
  InputAlignStart,
  NumberInput,
  DateInput,
};
export default config;
