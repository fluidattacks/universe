import type { Property } from "csstype";
import { styled } from "styled-components";

interface ICardInputContainerProps {
  $align?: Property.TextAlign;
  $minHeight?: string;
  $minWidth?: string;
}

const CardInputContainer = styled.div<ICardInputContainerProps>`
  ${({
    theme,
    $align = "center",
    $minWidth = "auto",
    $minHeight = "auto",
  }): string => `
    display: flex;
    background-color: ${theme.palette.white};
    border-radius: ${theme.spacing[0.25]};
    border: 1px solid ${theme.palette.gray[200]};
    flex-direction: column;
    justify-content: center;
    gap: ${theme.spacing[0.5]};
    min-width: ${$minWidth};
    min-height: ${$minHeight};
    padding: ${theme.spacing[1]};

    input {
      color: ${theme.palette.gray[800]};
      line-height: ${theme.spacing[1.5]};
      text-align: ${$align};
    }
  `}
`;

export { CardInputContainer };
