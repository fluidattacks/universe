import { IconName } from "@fortawesome/free-solid-svg-icons";
import type { Property } from "csstype";
import type { MouseEventHandler } from "react";
import { FieldValues, UseFormRegister } from "react-hook-form";

import type { TSpacing } from "components/@core/types";
import type { TFileType } from "components/file-preview/types";
import { IRadioButtonProps } from "components/radio-button/types";
import { IToggleProps } from "components/toggle/types";

type TInputType = "date" | "number" | "text";

/**
 * Card component shared props.
 * @interface ISharedProps
 * @property { string } [authorEmail] - Card author email.
 * @property { string } [date] - Card date.
 * @property { string } [description] - Card description.
 * @property { string } [title] - Card title.
 */
interface ISharedProps {
  authorEmail?: string;
  date?: string;
  description?: string;
  title: string;
}

/**
 * Card header component props.
 * @interface ICardHeader
 * @extends ISharedProps
 * @property { string } [textAlign] - Card title text alignment.
 * @property { string } [tooltip] - Card tooltip.
 * @property { string } id - Card id.
 * @property { string } [titleColor] - Card title color.
 * @property { string } [descriptionColor] - Card description color.
 * @property { TSpacing } [textSpacing] - Card text spacing.
 */
interface ICardHeader extends ISharedProps {
  textAlign?: Property.TextAlign;
  tooltip?: string;
  id: string;
  titleColor?: string;
  descriptionColor?: string;
  textSpacing?: TSpacing;
}

/**
 * Card with image component props.
 * @interface ICardWithImageProps
 * @extends ISharedProps
 * @property { string } alt - Card image alt.
 * @property { string } [src] - Card image source.
 * @property { boolean } [isEditing] - Card is editing.
 * @property { boolean } [showMaximize] - Card show maximize.
 * @property { boolean } [hideDescription] - Card hide description.
 * @property { MouseEventHandler<HTMLDivElement> } [onClick] - Card onClick.
 * @property { TFileType } [headerType] - Card header type.
 */
interface ICardWithImageProps extends ISharedProps {
  alt: string;
  hideDescription?: boolean;
  onClick?: MouseEventHandler<HTMLDivElement>;
  src: string;
  headerType?: TFileType;
  isEditing: boolean;
  showMaximize?: boolean;
  videoViewStatus?: (stage: "start" | "half" | "end") => void;
}

/**
 * Card with switch component props.
 * @interface ICardWithSwitchProps
 * @property { string } [minWidth] - Card min width.
 * @property { string } [height] - Card height.
 * @property { IToggleProps[] } toggles - Card toggles.
 */
interface ICardWithSwitchProps extends ICardHeader {
  minWidth?: string;
  height?: string;
  toggles: IToggleProps[];
}

/**
 * Card with selector component props.
 * @interface ICardWithSelectorProps
 * @extends ISharedProps
 * @property { string } [alt] - Card image alt.
 * @property { string } [imageId] - Card image id.
 * @property { IconName } [icon] - Card icon.
 * @property { Function } [onClick] - Card onClick.
 * @property { UseFormRegister<T> } [register] - Input form registration.
 * @property { IRadioButtonProps } selectorProps - Card selector props.
 * @property { string } [width] - Card width.
 */
interface ICardWithSelectorProps
  extends Pick<ISharedProps, "description" | "title"> {
  alt?: string;
  imageId?: string;
  icon?: IconName;
  onClick?: () => void;
  register?: UseFormRegister<FieldValues>;
  selectorProps: IRadioButtonProps;
  width?: string;
}

/**
 * Card with input component props.
 * @interface ICardWithInputProps
 * @extends ICardHeader
 * @property { string } [align] - Card input align.
 * @property { Interface } [inputNumberProps] - Card input number props.
 * @property { boolean } [disabled] - Card disabled.
 * @property { string } [inputType] - Card input type.
 * @property { string } [minHeight] - Card min height.
 * @property { string } [minWidth] - Card min width.
 * @property { string } name - Card name.
 * @property { string } [placeholder] - Card placeholder.
 */
interface ICardWithInputProps extends ICardHeader {
  align?: Property.TextAlign;
  inputNumberProps?: {
    decimalPlaces?: number;
    max?: number;
    min?: number;
  };
  disabled?: boolean;
  inputType?: TInputType;
  minHeight?: string;
  minWidth?: string;
  name: string;
  placeholder?: string;
}

export type {
  ICardHeader,
  ICardWithImageProps,
  ICardWithSwitchProps,
  ICardWithSelectorProps,
  ICardWithInputProps,
};
