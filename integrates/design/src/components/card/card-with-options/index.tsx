import { useCallback, useState } from "react";
import { useTheme } from "styled-components";

import { ICardWithOptionsProps } from "./types";

import { Checkbox } from "components/checkbox";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const CardWithOptions = ({
  options,
  title,
  checkboxesName,
  defaultCollapsed,
  onChange,
}: Readonly<ICardWithOptionsProps>): JSX.Element => {
  const theme = useTheme();
  const [collapsed, setCollapsed] = useState(defaultCollapsed ?? false);

  const handleCollapse = useCallback((): void => {
    setCollapsed((currentValue): boolean => !currentValue);
  }, []);

  return (
    <Container
      aria-label={title}
      bgColor={collapsed ? theme.palette.gray[50] : theme.palette.white}
      border={"1px solid" + theme.palette.gray[300]}
      borderRadius={"8px"}
      display={"inline-flex"}
      flexDirection={"column"}
      gap={0.625}
      px={0.75}
      py={0.625}
      role={"region"}
      width={"100%"}
    >
      <Container
        alignItems={"center"}
        cursor={"pointer"}
        display={"flex"}
        onClick={handleCollapse}
      >
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"sm"}>
          {title}
        </Text>
        <Icon
          icon={collapsed ? "chevron-up" : "chevron-down"}
          iconColor={theme.palette.gray[400]}
          iconSize={"xs"}
        />
      </Container>
      {collapsed ? (
        <Container display={"flex"} gap={0.75} wrap={"wrap"}>
          {options.map(
            (option): JSX.Element => (
              <Container
                border={"1px solid"}
                borderColor={theme.palette.gray[300]}
                borderRadius={"4px"}
                key={option.name}
                px={0.5}
                py={0.5}
              >
                <Checkbox
                  checked={option.visible}
                  disabled={option.locked}
                  label={option.name}
                  name={`${checkboxesName}-${option.name}`}
                  onChange={onChange}
                  value={option.name}
                />
              </Container>
            ),
          )}
        </Container>
      ) : undefined}
    </Container>
  );
};

export { CardWithOptions };
