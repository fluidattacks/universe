import type { Meta, StoryFn } from "@storybook/react";

import type { ICardWithOptionsProps } from "./types";

import { CardWithOptions } from ".";
import { Container } from "../../container";

const config: Meta = {
  component: CardWithOptions,
  parameters: { a11y: { disable: true } },
  tags: ["autodocs"],
  title: "Components/Cards/CardWithOptions",
};

const ListItemTemplate: StoryFn<Readonly<ICardWithOptionsProps>> = (
  props,
): JSX.Element => {
  return (
    <Container width={"350px"}>
      <CardWithOptions {...props} />
    </Container>
  );
};

const Default = ListItemTemplate.bind({});
Default.args = {
  title: "Card with options",
  checkboxesName: "checkboxes",
  options: [
    {
      id: "option1",
      name: "Option 1",
      visible: true,
      group: "ption 1",
      locked: true,
    },
    {
      id: "option2",
      name: "Option 2",
      visible: true,
      group: "Option 1",
      locked: false,
    },
    {
      id: "option3",
      name: "Option 3",
      visible: false,
      group: "option3",
      locked: false,
    },
  ],
};

export default config;
export { Default };
