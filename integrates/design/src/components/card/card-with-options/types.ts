import { IColumnModalProps } from "hooks/use-columns-modal";

/**
 * Card with switch component props.
 * @interface ICardWithOptionsProps
 * @property { string } checkboxesName - The name of the checkboxes.
 * @property { boolean } [defaultCollapsed] - Whether the card should be collapsed.
 * @property { IOptionsProps[] } options - The properties of the options to display.
 * @property { string } title - The title of the card.
 * @property { (event: Readonly<React.ChangeEvent<HTMLInputElement>>) => void } [onChange] - The function to call when the value of the checkbox changes.
 */
interface ICardWithOptionsProps {
  checkboxesName: string;
  defaultCollapsed?: boolean;
  options: IColumnModalProps[];
  title: string;
  onChange: (event: Readonly<React.ChangeEvent<HTMLInputElement>>) => void;
}

export type { ICardWithOptionsProps };
