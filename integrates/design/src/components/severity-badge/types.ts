type TSeverityBadgeVariant = "critical" | "disable" | "high" | "low" | "medium";

/**
 * Severity badge variant props.
 * @interface IVariant
 * @property {string} bgColor - Background color of the badge.
 * @property {string} colorL - Color of the left text.
 * @property {string} colorR - Color of the right text.
 */
interface IVariant {
  bgColor: string;
  colorL: string;
  colorR: string;
}

/**
 * Severity badge component props.
 * @interface ISeverityBadgeProps
 * @property { string } [textL] - Left text of the badge.
 * @property { string } textR - Right text of the badge.
 * @property { TSeverityBadgeVariant } variant - Variant of the badge.
 */
interface ISeverityBadgeProps {
  textL?: string;
  textR: string;
  variant: TSeverityBadgeVariant;
}

export type { TSeverityBadgeVariant, ISeverityBadgeProps, IVariant };
