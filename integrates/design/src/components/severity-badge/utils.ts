import { IVariant, TSeverityBadgeVariant } from "./types";

import { theme } from "components/colors";

const variants: Record<TSeverityBadgeVariant, IVariant> = {
  critical: {
    bgColor: theme.palette.primary[700],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.white,
  },
  disable: {
    bgColor: theme.palette.gray[200],
    colorL: theme.palette.gray[400],
    colorR: theme.palette.gray[400],
  },
  high: {
    bgColor: theme.palette.error[500],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.white,
  },
  low: {
    bgColor: theme.palette.warning[200],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.gray[800],
  },
  medium: {
    bgColor: theme.palette.warning[500],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.gray[800],
  },
};

export { variants };
