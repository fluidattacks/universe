import { isUndefined } from "lodash";
import { PropsWithChildren } from "react";
import { useTheme } from "styled-components";

import type { IIndicatorCardProps } from "./types";

import { Container } from "components/container";
import { Tooltip } from "components/tooltip";
import { Heading, Text } from "components/typography";

const IndicatorCard = ({
  description,
  height = "auto",
  leftIconName,
  children,
  rightIconName,
  title,
  tooltipId,
  tooltipTip,
  tooltipPlace = "bottom",
  variant = "centered",
  width = "318px",
}: Readonly<PropsWithChildren<IIndicatorCardProps>>): JSX.Element => {
  const theme = useTheme();
  const isCentered = variant === "centered";

  const tooltip = tooltipId && (
    <Tooltip
      icon={rightIconName ?? leftIconName}
      iconColor={theme.palette.gray[800]}
      id={tooltipId}
      place={tooltipPlace}
      tip={tooltipTip}
    />
  );

  return (
    <Container
      alignItems={isCentered ? "center" : "flex-start"}
      bgColor={theme.palette.gray[50]}
      border={`1px solid ${theme.palette.gray[200]}`}
      borderRadius={"4px"}
      display={"inline-flex"}
      flexDirection={isCentered ? "column" : "column-reverse"}
      gap={0.5}
      height={height}
      justify={"center"}
      padding={[1.25, 1.25, 1.25, 1.25]}
      width={width}
    >
      <Container alignItems={"center"} display={"flex"} gap={0.5}>
        {isCentered || isUndefined(leftIconName) ? undefined : tooltip}
        <Text
          color={isCentered ? theme.palette.gray[800] : theme.palette.gray[400]}
          size={"md"}
          textAlign={isCentered ? "center" : "start"}
        >
          {description}
        </Text>
        {isCentered || isUndefined(rightIconName) ? undefined : tooltip}
      </Container>
      <Container alignItems={"center"} display={"flex"} gap={0.5} my={0.5}>
        {isCentered && !isUndefined(leftIconName) ? tooltip : undefined}
        <div style={{ display: "flex", gap: "12px" }}>
          {isUndefined(title) ? undefined : (
            <Heading fontWeight={"bold"} size={"md"}>
              {title}
            </Heading>
          )}
          {children}
        </div>
        {isCentered && !isUndefined(rightIconName) ? tooltip : undefined}
      </Container>
    </Container>
  );
};

export type { IIndicatorCardProps };
export { IndicatorCard };
