import type { IconName } from "@fortawesome/free-solid-svg-icons";

import type { TPlace } from "components/tooltip/types";

type TVariant = "centered" | "left-aligned";

/**
 * Indicator card component props.
 * @interface IIndicatorCardProps
 * @property { string } [description] - Description of the indicator card.
 * @property { string } [height] - Height of the indicator card.
 * @property { IconName } [leftIconName] - Name of the left icon.
 * @property { IconName } [rightIconName] - Name of the right icon.
 * @property { string } [title] - Title of the indicator card.
 * @property { string } [tooltipId] - Id of the tooltip.
 * @property { string } [tooltipTip] - Tip of the tooltip.
 * @property { TPlace } [tooltipPlace] - Place of the tooltip.
 * @property { TVariant } [variant] - Variant of the indicator card.
 * @property { string } [width] - Width of the indicator card.
 */
interface IIndicatorCardProps {
  description?: string;
  height?: string;
  leftIconName?: IconName;
  rightIconName?: IconName;
  title?: string;
  tooltipId?: string;
  tooltipTip?: string;
  tooltipPlace?: TPlace;
  variant?: TVariant;
  width?: string;
}

export type { IIndicatorCardProps };
