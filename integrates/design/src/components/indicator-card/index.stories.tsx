import type { Meta, StoryFn } from "@storybook/react";

import type { IIndicatorCardProps } from "./types";

import { IndicatorCard } from ".";

const config: Meta = {
  component: IndicatorCard,
  tags: ["autodocs"],
  title: "Components/Cards/IndicatorCard",
};

const Default: StoryFn<Readonly<IIndicatorCardProps>> = (): JSX.Element => (
  <IndicatorCard
    description={"Covered authors"}
    rightIconName={"circle-info"}
    title={"268"}
  />
);

const LeftAligned: StoryFn<Readonly<IIndicatorCardProps>> = (): JSX.Element => (
  <IndicatorCard
    description={"Status"}
    leftIconName={"envelope"}
    title={"Vulnerable"}
    variant={"left-aligned"}
  />
);

export default config;
export { Default, LeftAligned };
