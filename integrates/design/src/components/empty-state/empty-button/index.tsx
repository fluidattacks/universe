import React from "react";

import type { IButtonProps } from "../types";
import { Button } from "components/button";
import type { TVariant } from "components/button/types";

const EmptyButton: React.FC<
  Readonly<{ buttonProps: IButtonProps; variant: TVariant }>
> = ({ buttonProps, variant }): JSX.Element => {
  return (
    <Button onClick={buttonProps.onClick} variant={variant}>
      {buttonProps.text}
    </Button>
  );
};

export { EmptyButton };
