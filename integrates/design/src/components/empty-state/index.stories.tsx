import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { IEmptyProps } from "./types";

import { EmptyState } from ".";

const config: Meta = {
  component: EmptyState,
  tags: ["autodocs"],
  title: "Components/EmptyState",
};

const mockClick = (): void => {
  // Mock click function without any implementation
};

const Template: StoryFn<React.PropsWithChildren<IEmptyProps>> = (
  props,
): JSX.Element => <EmptyState {...props} />;

const Default = Template.bind({});
Default.args = {
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  title: "Here a heading",
};

const OneAction = Template.bind({});
OneAction.args = {
  confirmButton: {
    onClick: mockClick,
    text: "Confirm",
  },
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  title: "Here a heading",
};

const TwoAction = Template.bind({});
TwoAction.args = {
  cancelButton: {
    onClick: mockClick,
    text: "Cancel",
  },
  confirmButton: {
    onClick: mockClick,
    text: "Confirm",
  },
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  title: "Here a heading",
};

const WithQuestion = Template.bind({});
WithQuestion.args = {
  cancelButton: {
    onClick: mockClick,
    text: "Cancel",
  },
  confirmButton: {
    onClick: mockClick,
    text: "Confirm",
  },
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  question: "Are you sure?",
  title: "Here a heading",
};

export default config;
export { Default, OneAction, TwoAction, WithQuestion };
