import type { DefaultTheme } from "styled-components";
import { styled } from "styled-components";

interface IEmptyContainerProps {
  $padding: keyof DefaultTheme["spacing"];
}

const EmptyContainer = styled.div<IEmptyContainerProps>`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: ${({ theme, $padding }): string => theme.spacing[$padding]};
  max-width: 900px;
  white-space: pre-line;
  margin: 0 auto;
`;

const EmptyImageContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: transparent;
  width: 96px;
  height: 96px;
  margin-bottom: ${({ theme }): string => theme.spacing[2]};

  > img {
    width: 100%;
  }
`;

const EmptyButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0;
  margin-bottom: ${({ theme }): string => theme.spacing[2]};

  > * {
    margin-right: ${({ theme }): string => theme.spacing[0.75]};
  }

  > *:last-child {
    margin-right: 0;
  }
`;

export { EmptyContainer, EmptyImageContainer, EmptyButtonContainer };
