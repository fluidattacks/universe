import { EmptyButton } from "./empty-button";
import {
  EmptyButtonContainer,
  EmptyContainer,
  EmptyImageContainer,
} from "./styles";
import type { IEmptyProps } from "./types";

import type { TSize } from "components/@core";
import { CloudImage } from "components/cloud-image";
import { Heading, Text } from "components/typography";

const sizes: Record<string, { description: TSize; title: TSize }> = {
  md: { description: "md", title: "md" },
  sm: { description: "sm", title: "xs" },
};

const EmptyState = ({
  cancelButton = undefined,
  confirmButton = undefined,
  description,
  imageSrc = "integrates/empty/addRoot",
  padding = 1.25,
  title,
  size = "md",
}: Readonly<IEmptyProps>): JSX.Element => {
  const hasButtons = (confirmButton ?? cancelButton) !== undefined;

  return (
    <EmptyContainer $padding={padding}>
      <EmptyImageContainer>
        <CloudImage alt={"empty-icon"} publicId={imageSrc} />
      </EmptyImageContainer>
      <Heading
        fontWeight={"bold"}
        lineSpacing={1.75}
        mb={0.25}
        size={sizes[size].title}
        textAlign={"center"}
      >
        {title}
      </Heading>
      <Text
        mb={hasButtons ? 2 : undefined}
        size={sizes[size].description}
        textAlign={"center"}
      >
        {description}
      </Text>
      {hasButtons ? (
        <EmptyButtonContainer>
          {cancelButton && (
            <EmptyButton buttonProps={cancelButton} variant={"tertiary"} />
          )}
          {confirmButton && (
            <EmptyButton buttonProps={confirmButton} variant={"primary"} />
          )}
        </EmptyButtonContainer>
      ) : undefined}
    </EmptyContainer>
  );
};

export { EmptyState };
