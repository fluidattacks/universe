import type { StyledOptions } from "styled-components";

import { hiddenProps } from "./constants";
import type {
  IBorderModifiable,
  IDisplayModifiable,
  IInteractionModifiable,
  IMarginModifiable,
  IPaddingModifiable,
  IPositionModifiable,
  ITextModifiable,
} from "./types";

import { theme } from "components/colors";

/**
 * Format a property string if it's not `undefined`
 * @returns `<name>: <value>;` or empty string if `value` is `undefined`
 */
const formatPropIf = (name: string, value?: number | string): string => {
  return value === undefined ? "" : `${name}: ${value};`;
};

const setPadding = (props: Readonly<IPaddingModifiable>): string => {
  const pt = props.py ?? props.pt ?? 0;
  const pr = props.px ?? props.pr ?? 0;
  const pb = props.py ?? props.pb ?? 0;
  const pl = props.px ?? props.pl ?? 0;

  if (props.padding) {
    const [top, right, bottom, left] = props.padding;

    return `padding: ${theme.spacing[top]} ${theme.spacing[right]} ${theme.spacing[bottom]} ${theme.spacing[left]};`;
  }

  return `padding: ${theme.spacing[pt]} ${theme.spacing[pr]} ${theme.spacing[pb]} ${theme.spacing[pl]};`;
};

const setMargin = (props: Readonly<IMarginModifiable>): string => {
  const mt = props.my ?? props.mt ?? 0;
  const mr = props.mx ?? props.mr ?? 0;
  const mb = props.my ?? props.mb ?? 0;
  const ml = props.mx ?? props.ml ?? 0;

  if (props.margin) {
    const [top, right, bottom, left] = props.margin;

    return `margin: ${theme.spacing[top]} ${theme.spacing[right]} ${theme.spacing[bottom]} ${theme.spacing[left]};`;
  }

  return `margin: ${theme.spacing[mt]} ${theme.spacing[mr]} ${theme.spacing[mb]} ${theme.spacing[ml]};`;
};

const setPosition = (props: Readonly<IPositionModifiable>): string => {
  const position = formatPropIf("position", props.position);
  const top = formatPropIf("top", props.top);
  const right = formatPropIf("right", props.right);
  const bottom = formatPropIf("bottom", props.bottom);
  const left = formatPropIf("left", props.left);
  const zIndex = formatPropIf("z-index", props.zIndex);

  return `
    ${position}
    ${top}
    ${right}
    ${bottom}
    ${left}
    ${zIndex}
  `;
};

const setBorder = (props: Readonly<IBorderModifiable>): string => {
  const border = formatPropIf("border", props.border);
  const borderTop = formatPropIf("border-top", props.borderTop);
  const borderRight = formatPropIf("border-right", props.borderRight);
  const borderBottom = formatPropIf("border-bottom", props.borderBottom);
  const borderLeft = formatPropIf("border-left", props.borderLeft);
  const borderColor = formatPropIf("border-color", props.borderColor);
  const borderRadius = formatPropIf("border-radius", props.borderRadius);

  return `
    ${border}
    ${borderTop}
    ${borderRight}
    ${borderBottom}
    ${borderLeft}
    ${borderColor}
    ${borderRadius}
  `;
};

const getScrollX = (scroll: string): string => {
  return scroll.includes("x") ? "overflow-x: auto;" : "";
};

const getScrollY = (scroll: string): string => {
  return scroll.includes("y") ? "overflow-y: auto;" : "";
};

const setDisplay = (props: Readonly<IDisplayModifiable>): string => {
  const scroll = props.scroll
    ? `
      ${getScrollX(props.scroll)}
      ${getScrollY(props.scroll)}
    `
    : "";

  const visibility = formatPropIf("visibility", props.visibility);
  const display = formatPropIf("display", props.display);
  const height = formatPropIf("height", props.height);
  const width = formatPropIf("width", props.width);
  const maxHeight = formatPropIf("max-height", props.maxHeight);
  const maxWidth = formatPropIf("max-width", props.maxWidth);
  const minHeight = formatPropIf("min-height", props.minHeight);
  const minWidth = formatPropIf("min-width", props.minWidth);
  const shadow = formatPropIf(
    "box-shadow",
    props.shadow ? theme.shadows[props.shadow] : undefined,
  );
  const gap = formatPropIf(
    "gap",
    props.gap === undefined ? undefined : theme.spacing[props.gap],
  );
  const bgColor = formatPropIf("background-color", props.bgColor);
  const bgGradient = formatPropIf("background", props.bgGradient);
  const flexDirection = formatPropIf("flex-direction", props.flexDirection);
  const flexGrow = formatPropIf("flex-grow", props.flexGrow);
  const justify = formatPropIf("justify-content", props.justify);
  const justifySelf = formatPropIf("justify-self", props.justifySelf);
  const alignItems = formatPropIf("align-items", props.alignItems);
  const alignSelf = formatPropIf("align-self", props.alignSelf);
  const wrap = formatPropIf("flex-wrap", props.wrap);

  return `
    ${scroll}
    ${visibility}
    ${display}
    ${height}
    ${width}
    ${maxHeight}
    ${maxWidth}
    ${minHeight}
    ${minWidth}
    ${shadow}
    ${bgColor}
    ${bgGradient}
    ${gap}
    ${flexDirection}
    ${flexGrow}
    ${justify}
    ${justifySelf}
    ${alignItems}
    ${alignSelf}
    ${wrap}
  `;
};

const setText = (props: Readonly<ITextModifiable>): string => {
  const color = formatPropIf("color", props.color);
  const fontFamily = formatPropIf("font-family", props.fontFamily);
  const fontSize = formatPropIf("font-size", props.fontSize);
  const fontWeight = formatPropIf("font-weight", props.fontWeight);
  const letterSpacing = formatPropIf("line-spacing", props.letterSpacing);
  const lineSpacing = formatPropIf("line-height", props.lineSpacing);
  const textAlign = formatPropIf("text-align", props.textAlign);
  const textDecoration = formatPropIf("text-decoration", props.textDecoration);
  const textOverflow = formatPropIf("text-overflow", props.textOverflow);
  const whiteSpace = formatPropIf("white-space", props.whiteSpace);
  const wordBreak = formatPropIf("word-break", props.wordBreak);
  const wordWrap = formatPropIf("word-wrap", props.wordWrap);

  return `
    ${color}
    ${fontFamily}
    ${fontSize}
    ${fontWeight}
    ${letterSpacing}
    ${lineSpacing}
    ${textAlign}
    ${textDecoration}
    ${textOverflow}
    ${whiteSpace}
    ${wordBreak}
    ${wordWrap}
  `;
};

const setInteraction = (props: Readonly<IInteractionModifiable>): string => {
  const cursor = formatPropIf("cursor", props.cursor);
  const borderColorHover = formatPropIf("border-color", props.borderColorHover);
  const bgColorHover = formatPropIf("background-color", props.bgColorHover);
  const shadowHover = formatPropIf(
    "box-shadow",
    props.shadowHover === undefined
      ? undefined
      : theme.shadows[props.shadowHover],
  );
  const transition = formatPropIf("transition", props.transition);

  return `
    ${cursor}

    &:hover {
      ${transition || "transition: all 0.2s;"}
      ${borderColorHover}
      ${bgColorHover}
      ${shadowHover}
    }
  `;
};

const getStyledConfig: StyledOptions<"web", object> = {
  shouldForwardProp: (prop: string): boolean => !hiddenProps.includes(prop),
};

export {
  setPadding,
  setText,
  setMargin,
  setInteraction,
  setPosition,
  setDisplay,
  setBorder,
  getStyledConfig,
};
