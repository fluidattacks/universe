import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { Property } from "csstype";

type ColorGradient = "01" | "02";
type ColorPalette =
  | "25"
  | "50"
  | "100"
  | "200"
  | "300"
  | "400"
  | "500"
  | "600"
  | "700"
  | "800"
  | "900";

/**
 * Fluidattacks Default theme design
 */
interface DefaultTheme {
  breakpoints: {
    mobile: string;
    tablet: string;
    sm: string;
    md: string;
    lg: string;
  };
  spacing: {
    0: "0rem";
    0.125: "0.125rem";
    0.188: "0.188rem";
    0.25: "0.25rem";
    0.5: "0.5rem";
    0.625: "0.625rem";
    0.75: "0.75rem";
    1: "1rem";
    1.25: "1.25rem";
    1.5: "1.5rem";
    1.75: "1.75rem";
    2: "2rem";
    2.25: "2.25rem";
    2.5: "2.5rem";
    3: "3rem";
    3.5: "3.5rem";
    4: "4rem";
    4.5: "4.5rem";
    5: "5rem";
    6: "6rem";
  };
  typography: {
    type: {
      primary: string;
      poppins: string;
      mono: string;
    };
    heading: {
      xxl: string;
      xl: string;
      lg: string;
      md: string;
      sm: string;
      xs: string;
    };
    text: {
      xl: string;
      lg: string;
      md: string;
      sm: string;
      xs: string;
    };
    weight: {
      bold: string;
      semibold: string;
      regular: string;
    };
  };
  shadows: {
    none: string;
    sm: string;
    md: string;
    lg: string;
  };
  palette: {
    primary: Record<ColorPalette, string>;
    complementary: Record<ColorPalette, string>;
    error: Record<ColorPalette, string>;
    info: Record<ColorPalette, string>;
    warning: Record<ColorPalette, string>;
    success: Record<ColorPalette, string>;
    gray: Record<ColorPalette, string>;
    gradients: Record<ColorGradient, string>;
    black: string;
    white: string;
  };
}

type TSize = keyof DefaultTheme["typography"]["heading"];
type TSpacing = keyof DefaultTheme["spacing"];
type TShadows = keyof DefaultTheme["shadows"];
type TIconSize = "xl" | "lg" | "md" | "sm" | "xs" | "xxs" | "xxss";
type TIconType =
  | "fa-brands"
  | "fa-kit"
  | "fa-light"
  | "fa-regular"
  | "fa-solid";
type TMode = "dark" | "light";

interface IPaddingModifiable {
  padding?: [TSpacing, TSpacing, TSpacing, TSpacing];
  px?: TSpacing;
  py?: TSpacing;
  pl?: TSpacing;
  pr?: TSpacing;
  pt?: TSpacing;
  pb?: TSpacing;
}

interface IMarginModifiable {
  margin?: [TSpacing, TSpacing, TSpacing, TSpacing];
  mx?: TSpacing;
  my?: TSpacing;
  ml?: TSpacing;
  mr?: TSpacing;
  mt?: TSpacing;
  mb?: TSpacing;
}

interface IPositionModifiable {
  zIndex?: number;
  position?: Property.Position;
  top?: string;
  right?: string;
  bottom?: string;
  left?: string;
}

interface IBorderModifiable {
  border?: string;
  borderTop?: string;
  borderRight?: string;
  borderBottom?: string;
  borderLeft?: string;
  borderColor?: string;
  borderRadius?: string;
}

interface IIconModifiable {
  icon:
    | IconName
    | "priority-bars-high"
    | "priority-bars-low"
    | "priority-bars-medium";
  iconColor?: string;
  iconSize: TIconSize;
  iconType?: TIconType;
  iconTransform?: string;
}

interface ISizeModifiable {
  size: TSize;
  sizeMd?: TSize;
  sizeSm?: TSize;
}

interface IDisplayModifiable {
  scroll?: "none" | "x" | "xy" | "y";
  visibility?: Property.Visibility;
  display?: Property.Display;
  height?: string;
  width?: string;
  maxHeight?: string;
  maxWidth?: string;
  minHeight?: string;
  minWidth?: string;
  shadow?: TShadows;
  bgColor?: string;
  bgGradient?: string;
  gap?: TSpacing;
  flexDirection?: Property.FlexDirection;
  flexGrow?: Property.FlexGrow;
  justify?: Property.JustifyContent;
  justifySelf?: Property.JustifyContent;
  alignItems?: Property.AlignItems;
  alignSelf?: Property.AlignItems;
  wrap?: Property.FlexWrap;
}

interface ITextModifiable {
  color?: string;
  fontFamily?: Property.FontFamily;
  fontSize?: Property.FontSize<number | string>;
  fontWeight?: Property.FontWeight;
  letterSpacing?: Property.LetterSpacing;
  lineSpacing?: Property.LineHeight<number | string>;
  textAlign?: Property.TextAlign;
  textDecoration?: Property.TextDecoration;
  textOverflow?: Property.Overflow;
  whiteSpace?: Property.WhiteSpace;
  wordBreak?: Property.WordBreak;
  wordWrap?: Property.WordWrap;
}

interface IInteractionModifiable {
  cursor?: Property.Cursor;
  transition?: string;
  borderColorHover?: string;
  bgColorHover?: string;
  shadowHover?: TShadows;
}

type TModifiable = IBorderModifiable &
  IDisplayModifiable &
  IInteractionModifiable &
  IMarginModifiable &
  IPaddingModifiable &
  IPositionModifiable &
  ITextModifiable;

export type {
  DefaultTheme as ThemeType,
  ColorPalette,
  TSpacing,
  IPaddingModifiable,
  IMarginModifiable,
  IDisplayModifiable,
  IIconModifiable,
  ITextModifiable,
  IInteractionModifiable,
  IPositionModifiable,
  IBorderModifiable,
  ISizeModifiable,
  TIconSize,
  TIconType,
  TModifiable,
  TMode,
  TSize,
};
