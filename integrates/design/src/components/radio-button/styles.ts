import { styled } from "styled-components";

const LabelContainer = styled.label`
  ${({ theme }): string => `
    align-items: center;
    display: flex;
    color: ${theme.palette.gray[600]};
    cursor: pointer;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    font-weight: ${theme.typography.weight.regular};
    gap: ${theme.spacing[0.5]};
    line-height: ${theme.typography.text.lg};
    text-align: left;
    position: relative;

    &[aria-disabled="true"] {
      color: ${theme.palette.gray[300]};
      cursor: not-allowed;
    }

    input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      width: 0;
    }

    .checkmark {
      position: relative;
      top: 0;
      left: 0;
      height: 16px;
      width: 16px;
      background-color: ${theme.palette.white};
      border-radius: 50%;
      border: 1px solid ${theme.palette.gray[600]};
    }

    input:disabled ~ .checkmark {
      border: 1px solid ${theme.palette.gray[300]};
      background-color: ${theme.palette.white};
      cursor: not-allowed;
    }

    input:not(:disabled):checked ~ .checkmark {
      background-color: ${theme.palette.white};
      border: 1px solid ${theme.palette.primary[500]};
    }

    input:not(:disabled):hover ~ .checkmark {
      background-color: ${theme.palette.white};
      box-shadow: 0 0 0 4px ${theme.palette.gray[100]};
    }

    input:not(:disabled):hover:checked ~ .checkmark {
      background-color: ${theme.palette.white};
      box-shadow: 0 0 0 4px ${theme.palette.primary[50]};
    }

    .checkmark::after {
      content: "";
      position: absolute;
      display: none;
      top: 3px;
      left: 3px;
      width: 8px;
      height: 8px;
      border-radius: 50%;
      background: ${theme.palette.primary[500]};
    }

    input:checked ~ .checkmark::after {
      display: block;
    }

    input:disabled:checked ~ .checkmark::after {
      background-color: ${theme.palette.gray[300]};
    }

  `}
`;

const CheckMark = styled.span.attrs({ className: "checkmark" })``;

export { CheckMark, LabelContainer };
