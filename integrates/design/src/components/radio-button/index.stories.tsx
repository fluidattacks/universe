import type { Meta, StoryObj } from "@storybook/react";
import { expect, within } from "@storybook/test";

import type { IRadioButtonProps } from "./types";

import { RadioButton } from ".";
import { Container } from "../container";

const config: Meta = {
  component: RadioButton,
  tags: ["autodocs"],
  title: "Components/RadioButton",
};

const RadioButtonTemplate: StoryObj<{
  itemsProps: IRadioButtonProps[];
}> = {
  render: ({ itemsProps }): JSX.Element => (
    <Container display={"flex"} flexDirection={"column"} gap={0.5}>
      {itemsProps.map(
        (props): JSX.Element => (
          <RadioButton
            defaultChecked={props.defaultChecked}
            disabled={props.disabled}
            key={`$cardWithInput-${props.value}`}
            label={props.label}
            name={props.name}
            value={"three"}
          />
        ),
      )}
    </Container>
  ),
};

const Default = {
  ...RadioButtonTemplate,
  args: {
    itemsProps: [
      {
        defaultChecked: true,
        name: "radio",
        value: "button 1",
      },
      {
        label: "label here",
        name: "radio",
        value: "button 2",
      },
    ],
  },
};

Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render radio", async () => {
    expect(await canvas.getAllByRole("radio", { name: "radio" })).toHaveLength(
      2,
    );

    expect(
      await canvas.getAllByRole("radio", { name: "radio" })[0],
    ).toBeChecked();
    expect(
      await canvas.getAllByRole("radio", { name: "radio" })[1],
    ).not.toBeChecked();
  });
};

const Disabled = {
  ...RadioButtonTemplate,
  args: {
    itemsProps: [
      {
        disabled: true,
        name: "radioButtonDisabled",
        value: "button 3",
      },
      {
        defaultChecked: true,
        disabled: true,
        label: "label here",
        name: "radioButtonDisabled",
        value: "button 4",
      },
    ],
  },
};

Disabled.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render radio", async () => {
    expect(
      await canvas.getAllByRole("radio", { name: "radioButtonDisabled" }),
    ).toHaveLength(2);

    expect(
      await canvas.getAllByRole("radio", { name: "radioButtonDisabled" })[0],
    ).not.toBeChecked();
    expect(
      await canvas.getAllByRole("radio", { name: "radioButtonDisabled" })[0],
    ).toBeDisabled();
    expect(
      await canvas.getAllByRole("radio", { name: "radioButtonDisabled" })[1],
    ).toBeChecked();
    expect(
      await canvas.getAllByRole("radio", { name: "radioButtonDisabled" })[1],
    ).toBeDisabled();
  });
};

export { Default, Disabled };
export default config;
