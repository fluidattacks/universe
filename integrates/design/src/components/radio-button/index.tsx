import { Ref, forwardRef } from "react";

import { CheckMark, LabelContainer } from "./styles";
import type { IRadioButtonProps } from "./types";

const RadioButton = forwardRef(function RadioButton(
  {
    defaultChecked = false,
    disabled = false,
    label,
    name,
    onChange,
    onClick,
    value,
    ...props
  }: Readonly<IRadioButtonProps>,
  ref: Ref<HTMLInputElement>,
): JSX.Element {
  return (
    <LabelContainer
      aria-disabled={disabled}
      aria-label={label ?? name}
      role={"radiogroup"}
    >
      <input
        aria-checked={defaultChecked}
        aria-disabled={disabled}
        aria-label={label ?? name}
        defaultChecked={defaultChecked}
        disabled={disabled}
        name={name}
        onChange={onChange}
        onClick={onClick}
        ref={ref}
        type={"radio"}
        value={value}
        {...props}
      />
      <CheckMark />
      {label ?? null}
    </LabelContainer>
  );
});

export { RadioButton };
