import type { HTMLAttributes, HTMLProps } from "react";

/**
 * Radio button props.
 * @interface IRadioButtonProps
 * @extends HTMLAttributes<HTMLInputElement>
 * @extends HTMLProps<HTMLInputElement>
 * @property {string} name - Input name.
 * @property {string} value - Radio button value.
 */
interface IRadioButtonProps
  extends HTMLAttributes<HTMLInputElement>,
    HTMLProps<HTMLInputElement> {
  name: string;
  value?: string;
}

export type { IRadioButtonProps };
