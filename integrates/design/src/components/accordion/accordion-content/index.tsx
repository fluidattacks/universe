import { IAccordionContentProps } from "../types";
import { Container } from "components/container";
import { Text } from "components/typography";

const AccordionContent = ({
  item,
}: Readonly<IAccordionContentProps>): JSX.Element => {
  const descriptionContent =
    item.description && typeof item.description === "string" ? (
      <Text mb={item.extraElement ? 0.5 : undefined} size={"sm"}>
        {item.description}
      </Text>
    ) : (
      (item.description ?? undefined)
    );
  return (
    <Container
      display={"block"}
      id={`panel-${item.title}`}
      padding={[0.75, 0.75, 0.75, 0.75]}
    >
      {descriptionContent}
      {item.extraElement ? (
        <Container>{item.extraElement}</Container>
      ) : undefined}
    </Container>
  );
};

export { AccordionContent };
