import { AdvancedImage, lazyload, placeholder } from "@cloudinary/react";
import React from "react";

import { ICloudImageProps } from "./types";

import { useCloudinaryImage } from "hooks";

const CloudImage = ({
  alt = "img",
  height = "",
  width = "",
  plugins = [lazyload(), placeholder()],
  publicId,
}: Readonly<ICloudImageProps>): JSX.Element => {
  const image = useCloudinaryImage({ publicId });

  return (
    <AdvancedImage
      alt={alt}
      cldImg={image}
      height={height}
      plugins={plugins}
      width={width}
    />
  );
};

const MemoizedImage = React.memo(CloudImage);

export type { ICloudImageProps };
export { MemoizedImage as CloudImage };
