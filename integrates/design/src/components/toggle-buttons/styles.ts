import { styled } from "styled-components";

const ToggleContainer = styled.div`
  ${({ theme }): string => `
    align-items: center;
    display: flex;
    position: relative;
    width: 352px;

    > button:first-child {
      border-radius: ${theme.spacing[2]} 0 0 ${theme.spacing[2]};
    }
    > button:last-child {
      --angle-grad: -45deg;
      border-radius: 0 ${theme.spacing[2]} ${theme.spacing[2]} 0;
    }

    &::before {
      background: ${theme.palette.gradients["01"]} border-box;
      border-radius: ${theme.spacing[2]};
      content: "";
      position: absolute;
      inset: 0;
      border: 2px solid transparent;
      mask: linear-gradient(#fff 0 0) padding-box, linear-gradient(#fff 0 0);
      mask-composite: exclude;
    }

    @media screen and (max-width: ${theme.breakpoints.mobile}) {
      width: 320px;
    }
  `}
`;

const StyledToggleButton = styled.button`
  ${({ theme }): string => `
    align-items: center;
    background: linear-gradient(
      var(--angle-grad),
      rgba(243, 38, 55, var(--opacity-grad)),
      rgba(184, 7, 93, var(--opacity-grad))
    );
    color: ${theme.palette.primary[500]};
    cursor: pointer;
    display: flex;
    flex: 1 0 0;
    justify-content: center;
    gap: ${theme.spacing[0.625]};
    height: ${theme.spacing[4]};
    padding: ${theme.spacing[1.25]};
    position: relative;
    transition: color, --opacity-grad 0.5s ease-in-out;

    &.selected {
      --opacity-grad: 1;
      background: linear-gradient(
        var(--angle-grad),
        rgba(243, 38, 55, var(--opacity-grad)),
        rgba(184, 7, 93, var(--opacity-grad))
      );
      color: ${theme.palette.white};
    }
  `}
`;

export { StyledToggleButton, ToggleContainer };
