import type { Meta, StoryFn } from "@storybook/react";
import { expect, userEvent, within } from "@storybook/test";
import { useCallback } from "react";

import type { IToggleButtonProps } from "./types";

import { ToggleButton } from ".";

const config: Meta = {
  component: ToggleButton,
  tags: ["autodocs"],
  title: "Components/ToggleButton",
};

const Template: StoryFn<IToggleButtonProps> = (props): JSX.Element => {
  const mockClick = useCallback((): void => {}, []);

  return <ToggleButton {...props} handleClick={mockClick} />;
};

const Default = Template.bind({});
Default.args = {
  options: ["Label here", "Click me"],
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const buttonLeft = canvas.getByRole("button", { name: "Label here" });
  const buttonRight = canvas.getByRole("button", { name: "Click me" });

  await step("should render a Toggle button", async (): Promise<void> => {
    await expect(buttonLeft).toBeInTheDocument();
    await expect(buttonRight).toBeInTheDocument();
  });

  await step("should toggle on click", async (): Promise<void> => {
    await userEvent.click(buttonRight);
    await expect(buttonRight).toHaveClass("selected");
    await expect(buttonLeft).not.toHaveClass("selected");
  });
};

export { Default };
export default config;
