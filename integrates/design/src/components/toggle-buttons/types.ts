/**
 * Toggle button component props.
 * @interface IToggleButtonProps
 * @property {string} [defaultSelection] - The option selected by default.
 * @property {string[]} options - The list options label.
 * @property {Function} handleClick - The function to handle the click event.
 */
interface IToggleButtonProps {
  defaultSelection?: string;
  options: string[];
  handleClick: (selection: string) => void;
}

export type { IToggleButtonProps };
