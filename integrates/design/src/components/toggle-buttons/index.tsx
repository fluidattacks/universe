import { useCallback, useState } from "react";

import { StyledToggleButton, ToggleContainer } from "./styles";
import { IToggleButtonProps } from "./types";

import { Text } from "components/typography";

const ToggleButton = ({
  defaultSelection,
  options,
  handleClick,
}: Readonly<IToggleButtonProps>): JSX.Element => {
  const [currentSelection, setCurrentSelection] = useState(
    defaultSelection ?? options[0],
  );
  const handleOnClick = useCallback(
    (option: string): VoidFunction =>
      // eslint-disable-next-line functional/functional-parameters
      (): void => {
        setCurrentSelection(option);
        handleClick(option);
      },
    [handleClick],
  );

  return (
    <ToggleContainer>
      {options.map(
        (option, key): JSX.Element => (
          <StyledToggleButton
            className={`${option.includes(currentSelection) && "selected"}`}
            key={`${option}-${key}`}
            onClick={handleOnClick(option)}
          >
            <Text
              color={"inherit"}
              fontWeight={"bold"}
              size={"md"}
              sizeSm={"sm"}
              textAlign={"center"}
            >
              {option}
            </Text>
          </StyledToggleButton>
        ),
      )}
    </ToggleContainer>
  );
};

export { ToggleButton };
