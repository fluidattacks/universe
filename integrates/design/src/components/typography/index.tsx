import { Heading } from "./heading";
import { Span } from "./span";
import { Text } from "./text";

export { Heading, Text, Span };
