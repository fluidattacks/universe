import type { PropsWithChildren, Ref } from "react";
import { forwardRef } from "react";

import { StyledHeading } from "../styles";
import type { THeadingProps } from "../types";

const Heading = forwardRef(function Heading(
  {
    bgGradient,
    children,
    className,
    color,
    display,
    fontFamily,
    fontWeight,
    letterSpacing,
    lineSpacing,
    lineSpacingSm,
    size,
    sizeMd,
    sizeSm,
    textFill,
    whiteSpace,
    wordBreak,
    ...props
  }: Readonly<PropsWithChildren<THeadingProps>>,
  ref: Ref<HTMLParagraphElement>,
): JSX.Element {
  return (
    <StyledHeading
      $bgGradient={bgGradient}
      $color={color}
      $display={display}
      $fontFamily={fontFamily}
      $fontWeight={fontWeight}
      $letterSpacing={letterSpacing}
      $lineSpacing={lineSpacing}
      $lineSpacingSm={lineSpacingSm}
      $size={size}
      $sizeMd={sizeMd}
      $sizeSm={sizeSm}
      $textFill={textFill}
      $whiteSpace={whiteSpace}
      $wordBreak={wordBreak}
      className={className}
      ref={ref}
      {...props}
    >
      {children}
    </StyledHeading>
  );
});

export { Heading };
