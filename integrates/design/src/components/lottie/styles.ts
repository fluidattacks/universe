import Lottie from "lottie-light-react";
import type { LottieComponentProps } from "lottie-light-react";
import { styled } from "styled-components";

interface IStyledLottieProps
  extends Partial<
    Pick<LottieComponentProps, "animationData" | "autoplay" | "className">
  > {
  $size?: number;
}

const StyledLottie = styled(Lottie).attrs(
  ({ autoplay = true }: Readonly<IStyledLottieProps>): IStyledLottieProps => ({
    autoplay,
    className: "comp-lottie",
  }),
)<IStyledLottieProps>`
  ${({ $size = 16 }): string => `
    height: ${$size}px;
    width: ${$size}px;
  `}
`;

export type { IStyledLottieProps };
export { StyledLottie };
