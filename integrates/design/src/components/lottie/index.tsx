import { StyledLottie } from "./styles";
import { ILottieProps } from "./types";

const Lottie = (props: Readonly<ILottieProps>): JSX.Element => {
  const { size } = props;

  return <StyledLottie $size={size} {...props} />;
};

export { Lottie };
