import { isUndefined } from "lodash";
import { useCallback, useState } from "react";
import Joyride, { ACTIONS, EVENTS, STATUS } from "react-joyride";
import type { CallBackProps, Step } from "react-joyride";

import { ITourProps } from "./types";

const baseStep: Step = {
  content: "",
  disableBeacon: true,
  hideCloseButton: false,
  locale: {
    back: "Back",
    close: "Close",
    last: "Close",
    next: "Next",
    open: "Open the dialog",
    skip: "Skip",
  },
  showSkipButton: true,
  styles: {
    buttonBack: {
      fontFamily: "roboto",
    },
    buttonNext: {
      fontFamily: "roboto",
    },
    buttonSkip: {
      fontFamily: "roboto",
    },
    options: {
      zIndex: 9999,
    },
    tooltipContainer: {
      fontFamily: "roboto",
      textAlign: "left",
    },
  },
  target: "",
};

const Tour = ({ run, steps, onFinish }: Readonly<ITourProps>): JSX.Element => {
  const [runTour, setRunTour] = useState(run);
  const [tourStep, setTourStep] = useState(0);

  const handleJoyrideCallback = useCallback(
    (tourState: Readonly<CallBackProps>): void => {
      const { action, index, status, type } = tourState;

      if (
        ([EVENTS.STEP_AFTER, EVENTS.TARGET_NOT_FOUND] as string[]).includes(
          type,
        )
      ) {
        setTourStep(index + (action === ACTIONS.PREV ? -1 : 1));
      } else if (
        ([STATUS.FINISHED, STATUS.SKIPPED] as string[]).includes(status) ||
        action === "close"
      ) {
        setRunTour(false);
        if (!isUndefined(onFinish)) {
          onFinish();
        }
      }
    },
    [onFinish],
  );

  return (
    <Joyride
      callback={handleJoyrideCallback}
      continuous={true}
      disableOverlayClose={true}
      disableScrollParentFix={true}
      run={runTour}
      spotlightClicks={true}
      stepIndex={tourStep}
      steps={steps}
      styles={{
        buttonNext: {
          display: tourStep === steps.length - 1 ? "none" : "block",
        },
        options: { zIndex: 9999 },
      }}
    />
  );
};

export { baseStep, Tour };
export type { Step };
