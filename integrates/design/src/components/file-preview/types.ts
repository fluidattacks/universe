type TFileType = "image" | "video";

/**
 * File preview component props.
 * @interface IFilePreviewProps
 * @property { string } alt - The alt attribute for the image.
 * @property { TFileType } fileType - The type of the file (image or video).
 * @property { string } [height] - The height of the image or video.
 * @property { number } [opacity] - The opacity of the file preview.
 * @property { string } src - The source URL of the image or video.
 * @property { string } [width] - The width of the image or video.
 */
interface IFilePreviewProps {
  alt: string;
  fileType: TFileType;
  height?: string;
  opacity?: number;
  src: string;
  width?: string;
  videoViewStatus?: (stage: "start" | "half" | "end") => void;
}

export type { IFilePreviewProps, TFileType };
