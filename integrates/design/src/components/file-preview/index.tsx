import { useCallback, useState } from "react";

import { PreviewContainer } from "./styles";
import type { IFilePreviewProps } from "./types";
import { hasExtension } from "./utils";

import { Link } from "components/link";

const FilePreview = ({
  alt,
  fileType,
  width = "100%",
  opacity = 0,
  src,
  height = "auto",
  videoViewStatus,
}: Readonly<IFilePreviewProps>): JSX.Element => {
  const [viewed, setViewed] = useState({
    start: false,
    half: false,
    end: false,
  });

  const sendViewStatus = useCallback(
    (e: Readonly<React.SyntheticEvent<HTMLVideoElement, Event>>): void => {
      if (!videoViewStatus) return;
      const video = e.target as HTMLVideoElement;
      const percentageViewed = (video.currentTime / video.duration) * 100;
      if (percentageViewed > 5 && percentageViewed < 50) {
        if (!viewed.start) videoViewStatus("start");
        setViewed({ start: true, half: false, end: false });
      } else if (percentageViewed >= 50 && percentageViewed < 99) {
        if (!viewed.half) videoViewStatus("half");
        setViewed({ start: true, half: true, end: false });
      } else if (percentageViewed >= 99) {
        if (!viewed.end) videoViewStatus("end");
        setViewed({ start: true, half: true, end: true });
      } else setViewed({ start: false, half: false, end: false });
    },
    [videoViewStatus, viewed],
  );

  if (fileType === "video" || hasExtension(["webm"], src)) {
    return (
      <PreviewContainer $opacity={opacity}>
        <Link href={src} iconPosition={"hidden"}>
          <video
            controls={true}
            muted={true}
            onTimeUpdate={sendViewStatus}
            style={{ height, width }}
          >
            <source src={src} type={"video/webm"} />
            {src}
          </video>
        </Link>
      </PreviewContainer>
    );
  }

  return (
    <PreviewContainer $opacity={opacity}>
      <img alt={alt} src={src} style={{ height, width }} />
    </PreviewContainer>
  );
};

export type { IFilePreviewProps };
export { FilePreview };
