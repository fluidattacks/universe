import { styled } from "styled-components";
import type { ExecutionProps } from "styled-components";

import { Container } from "components/container";
import { IContainerProps } from "components/container/types";

interface IPreviewContainer extends IContainerProps {
  $opacity: number;
}

const PreviewContainer = styled(Container).attrs<IPreviewContainer>(
  ({ ...props }): ExecutionProps & Partial<IPreviewContainer> => ({
    display: "inline-block",
    position: "relative",
    ...props,
  }),
)`
  & img {
    display: block;
  }

  &::after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    pointer-events: none;
    ${({ $opacity = 0 }): string =>
      `background-color: rgba(0, 0, 0, ${$opacity});`}
  }
`;

export { PreviewContainer };
