import { IIconModifiable } from "components/@core";
import { ITabProps } from "components/tabs/types";

/**
 * Button empty state props.
 * @interface IButtonProps
 * @extends IIconModifiable
 * @property { string } [id] - Button id.
 * @property { string } [text] - Button text.
 * @property { Function } [onClick] - Button click handler.
 */
interface IButtonProps extends Partial<IIconModifiable> {
  id?: string;
  onClick?: () => void;
  text?: string;
}

/**
 * Info sidebar component props.
 * @interface IInfoSidebarProps
 * @property { IButtonProps[] } [actions] - Sidebar actions.
 * @property { React.ReactNode } [header] - Sidebar header.
 * @property { string } title - Sidebar title.
 * @property { ITabProps[] } tabs - Sidebar tabs.
 * @property { IButtonProps[] } [footer] - Sidebar footer.
 */
interface IInfoSidebarProps {
  actions?: IButtonProps[];
  header?: React.ReactNode;
  title: string;
  tabs: ITabProps[];
  footer?: IButtonProps[];
}

export type { IInfoSidebarProps };
