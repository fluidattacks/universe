import { InputHTMLAttributes } from "react";

/**
 *  Number input component props.
 * @interface INumberInputProps
 * @extends InputHTMLAttributes<HTMLInputElement>
 * @property { boolean } [autoUpdate] - Whether the input value should be updated automatically.
 * @property { number } [decimalPlaces] - Number of decimal places to display.
 * @property { Function } [onEnter] - Function to be called when the enter key is pressed.
 * @property { string } [tooltipMessage] - Tooltip message to display when hovering over the input.
 */
interface INumberInputProps extends InputHTMLAttributes<HTMLInputElement> {
  autoUpdate?: boolean;
  decimalPlaces?: number;
  onEnter: (value?: number) => void;
  tooltipMessage?: string;
}

export type { INumberInputProps };
