import type { Meta, StoryFn } from "@storybook/react";

import type { INumberInputProps } from ".";
import { NumberInput } from ".";

const config: Meta = {
  component: NumberInput,
  tags: ["autodocs"],
  title: "Components/NumberInput",
};

const Template: StoryFn<INumberInputProps> = (props): JSX.Element => (
  <NumberInput {...props} />
);

const Default = Template.bind({});
Default.args = {
  defaultValue: 1,
  max: 7,
  min: 0,
  name: "number-input",
  tooltipMessage: "Enter to save",
};

export { Default };
export default config;
