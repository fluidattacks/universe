import React, { PropsWithChildren, useCallback, useRef, useState } from "react";
import { useTheme } from "styled-components";

import { OptionContainer } from "./option-container";
import type { IOAuthSelectorProps, TOAuthProvider } from "./types";

import { Button } from "components/button";
import { Container } from "components/container";
import { useClickOutside } from "hooks/use-click-outside";

const OAuthSelector = ({
  align = "end",
  buttonLabel = "Add Credential",
  children,
  id,
  manualOption,
  providers,
}: Readonly<PropsWithChildren<IOAuthSelectorProps>>): JSX.Element => {
  const theme = useTheme();
  const [showOptions, setShowOptions] = useState<boolean>(false);
  const containerRef = useRef<HTMLDivElement>(null);

  useClickOutside(
    containerRef.current,
    (): void => {
      setShowOptions(false);
    },
    true,
  );

  const handleClickManual = useCallback(
    (event: Readonly<React.MouseEvent<HTMLOrSVGElement>>): void => {
      manualOption?.onClick(event);
      setShowOptions(false);
    },
    [manualOption],
  );
  const toggleShowOptions = useCallback((): void => {
    setShowOptions((previousState): boolean => !previousState);
  }, [setShowOptions]);

  const handleOptionClick = useCallback(
    (
      provider: TOAuthProvider,
    ): ((event: Readonly<React.MouseEvent<HTMLOrSVGElement>>) => void) =>
      (event: Readonly<React.MouseEvent<HTMLOrSVGElement>>): void => {
        providers[provider]?.onClick(event);
        setShowOptions(false);
      },
    [providers],
  );

  return (
    <Container
      alignItems={align}
      display={"flex"}
      flexDirection={"column"}
      position={"relative"}
      ref={containerRef}
      width={"max-content"}
      zIndex={10}
    >
      <Button icon={"plus"} onClick={toggleShowOptions} variant={"primary"}>
        {buttonLabel}
      </Button>
      <Container
        alignItems={"center"}
        bgColor={"#fff"}
        border={`1px solid ${theme.palette.gray[200]}`}
        borderRadius={"4px"}
        display={"flex"}
        gap={0.5}
        height={"78px"}
        id={id}
        pb={0.5}
        pl={0.75}
        position={"absolute"}
        pr={0.75}
        pt={0.5}
        shadow={"md"}
        top={"40px"}
        visibility={showOptions ? "visible" : "hidden"}
        zIndex={10}
      >
        {(Object.keys(providers) as TOAuthProvider[]).map(
          (provider): JSX.Element => {
            return (
              <OptionContainer
                key={provider}
                onClick={handleOptionClick(provider)}
                provider={provider}
              />
            );
          },
        )}
        {manualOption === undefined ? undefined : (
          <OptionContainer
            icon={"square-plus"}
            label={manualOption.label ?? "Add manually"}
            onClick={handleClickManual}
          />
        )}
        {children}
      </Container>
    </Container>
  );
};

export { OAuthSelector, OptionContainer };
