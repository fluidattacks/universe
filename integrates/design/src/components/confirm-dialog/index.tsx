import React, { useCallback, useEffect, useRef, useState } from "react";

import type { IConfirmDialogProps } from "./types";

import { Modal } from "components/modal";
import { useModal } from "hooks";

// eslint-disable-next-line functional/functional-parameters
const useConfirmDialog = (): {
  confirm: (props: Readonly<IConfirmDialogProps>) => Promise<boolean>;
  ConfirmDialog: React.FC<
    Readonly<{ id?: string; children?: React.ReactNode }>
  >;
} => {
  const [open, setOpen] = useState(false);
  const [dialogProps, setDialogProps] = useState<IConfirmDialogProps>();
  const resolveConfirm = useRef<((result: boolean) => void) | null>(null);
  const modal = useModal("confirm-dialog-modal");

  const confirm = useCallback(
    async (props: Readonly<IConfirmDialogProps>): Promise<boolean> => {
      setDialogProps(props);
      setOpen(true);

      return new Promise<boolean>((resolve): void => {
        // Needed to implement the deferred promise pattern
        // eslint-disable-next-line functional/immutable-data
        resolveConfirm.current = resolve;
      });
    },
    [],
  );

  const handleConfirm = useCallback((): void => {
    setOpen(false);
    setDialogProps(undefined);
    resolveConfirm.current?.(true);
  }, []);

  const handleCancel = useCallback((): void => {
    setOpen(false);
    setDialogProps(undefined);
    resolveConfirm.current?.(false);
  }, []);

  useEffect((): void => {
    modal.setIsOpen(open);
  }, [open, modal]);

  const ConfirmDialog: React.FC<
    Readonly<{ id?: string; children?: React.ReactNode }>
  > = ({ id = "confirm-dialog-modal", children }): React.ReactElement => {
    return (
      <Modal
        cancelButton={{
          onClick: handleCancel,
          text: "Cancel",
        }}
        confirmButton={{
          id: dialogProps?.id ?? "modal-confirm",
          onClick: handleConfirm,
          text: "Confirm",
        }}
        description={dialogProps?.message}
        id={id}
        modalRef={{ ...modal, close: handleCancel }}
        size={"sm"}
        title={dialogProps?.title as string}
      >
        {dialogProps?.content}
        {children}
      </Modal>
    );
  };

  return { ConfirmDialog, confirm };
};

export type { IConfirmDialogProps };
export { useConfirmDialog };
