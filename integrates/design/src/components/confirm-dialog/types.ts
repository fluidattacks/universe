/**
 * Confirm dialog component props.
 * @interface IConfirmDialogProps
 * @property { string } [id] - Id of the confirm dialog.
 * @property { React.ReactNode } [message] - Message to display in the confirm dialog.
 * @property { React.ReactNode } [content] - Content to display in the confirm dialog.
 * @property { string } title - Title of the confirm dialog.
 */
interface IConfirmDialogProps {
  id?: string;
  message?: React.ReactNode;
  content?: React.ReactNode;
  title: string;
}

export type { IConfirmDialogProps };
