import { styled } from "styled-components";

const StyledInput = styled.input`
  ${({ theme }): string => `
    background: none;
    border: none !important;
    box-shadow: none;
    box-sizing: border-box;
    color: ${theme.palette.gray[800]};
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    outline: none;
    width: 100%;

    &:-webkit-autofill {
      -webkit-background-clip: text;
    }

    &::placeholder {
      color: ${theme.palette.gray[400]};
    }

    &:is(:hover, :focus)::-webkit-search-cancel-button {
      display:none;
    }

    ::-webkit-search-decoration,
    ::-webkit-search-results-button,
    ::-webkit-search-results-decoration {
      display:none;
    }
  `}
`;

const OutlineContainer = styled.div`
  ${({ theme }): string => `
    align-items: center;
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[300]};
    border-radius: ${theme.spacing[0.5]};
    display: flex;
    height: 40px;
    gap: ${theme.spacing[0.5]};
    padding: ${theme.spacing[0.5]};
    width: 100%;

    &:hover {
      border-color: ${theme.palette.gray[600]};
    }

    &:focus-within {
      border: 2px solid ${theme.palette.black};
    }
  `}
`;

const OutlineDropdown = styled.div`
  ${({ theme }): string => `
    background-color: ${theme.palette.white};
    border-radius: ${theme.spacing[0.5]};
    width: 250px;
  `}
`;

export { StyledInput, OutlineContainer, OutlineDropdown };
