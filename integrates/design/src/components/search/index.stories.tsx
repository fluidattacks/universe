import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { ISearchProps } from "./types";

import { Search } from ".";

const config: Meta = {
  component: Search,
  tags: ["autodocs"],
  title: "Components/Search",
};

const Template: StoryFn<React.PropsWithChildren<ISearchProps>> = (
  props,
): JSX.Element => <Search {...props} />;

const Default = Template.bind({});

Default.args = {
  placeholder: "Search",
};

export { Default };
export default config;
