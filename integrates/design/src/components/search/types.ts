import { InputHTMLAttributes } from "react";

/**
 * Search component props interface.
 * @interface ISearchProps
 * @property { boolean } [smallSearch] - Use smaller search bar size.
 * @property { Function } [onClose] - Callback to reset search bar.
 */
interface ISearchProps extends InputHTMLAttributes<HTMLInputElement> {
  smallSearch?: boolean;
  onClear?: () => void;
}

export type { ISearchProps };
