import type { Meta, StoryFn } from "@storybook/react";

import type { INotificationProps } from "./types";

import { Notification } from ".";

const config: Meta = {
  argTypes: {
    id: {
      control: {
        disable: true,
      },
    },
  },
  component: Notification,
  tags: ["autodocs"],
  title: "Components/Notification",
};

const title = "Tittle informational notification";
const description =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.";

const Template: StoryFn<Readonly<INotificationProps>> = (
  props,
): JSX.Element => (
  <Notification {...props} description={description} title={title} />
);

const ErrorState = Template.bind({});

ErrorState.args = {
  variant: "error",
};

const Success = Template.bind({});
Success.args = {
  variant: "success",
};

const Warning = Template.bind({});
Warning.args = {
  variant: "warning",
};

const Info = Template.bind({});
Info.args = {
  variant: "info",
};

export { ErrorState, Success, Warning, Info };
export default config;
