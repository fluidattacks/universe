import { styled } from "styled-components";

import { TNotificationVariant } from "./types";

interface IAlertAndNotifyBoxProps {
  $variant: TNotificationVariant;
}

const NotificationContainer = styled.div<IAlertAndNotifyBoxProps>`
  ${({ theme, $variant }): string => `
    background: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    border-left: 4px solid ${theme.palette[$variant][500]};
    box-shadow: ${theme.shadows.md};
    position: relative;
    display: flex;
    width: 360px;
    padding: ${theme.spacing[1.25]} ${theme.spacing[1]} ${theme.spacing[1.25]} ${theme.spacing[0.75]};
    gap: ${theme.spacing[0.5]};

    button {
      position: absolute;
      top: 20px;
      right: 16px;
    }
  `}
`;

const IconsContainer = styled.div<IAlertAndNotifyBoxProps>`
  ${({ theme, $variant }): string => `
    height: ${theme.spacing[2.5]};
    width: ${theme.spacing[2.5]};
    position: relative;
    flex-shrink: 0;

    & .ellipse-1,
    & .ellipse-2,
    & .icon {
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      position: absolute;
    }

    & .ellipse-1 {
      transform: scale(2.5) translate(-20%, -20%);
      color: ${theme.palette[$variant][50]};
      position: absolute;
    }

    & .ellipse-2 {
      transform: scale(2) translate(-25%, -25%);
      color: ${theme.palette[$variant][200]};
    }

    & .icon {
      color: ${theme.palette[$variant][500]};
    }
  `}
`;

export { IconsContainer, NotificationContainer };
