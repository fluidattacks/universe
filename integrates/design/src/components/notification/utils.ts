import type { IconName } from "@fortawesome/free-solid-svg-icons";

import { TNotificationVariant } from "./types";

const getIcon = (variant: TNotificationVariant): IconName => {
  if (variant === "warning") {
    return "triangle-exclamation";
  } else if (variant === "info") {
    return "circle-info";
  } else if (variant === "success") {
    return "circle-check";
  }

  return "circle-exclamation";
};

export { getIcon };
