import type { Ref } from "react";
import { forwardRef, useCallback, useEffect, useState } from "react";

import { AlertContainer, MessageContainer, getIcon } from "./styles";
import type { IAlertProps } from "./types";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";

const Alert = forwardRef(function Alert(
  {
    autoHide = false,
    children,
    closable = false,
    onTimeOut: timer,
    show = true,
    time = 8,
    variant = "error",
    id,
    ...props
  }: Readonly<IAlertProps>,
  ref: Ref<HTMLDivElement>,
): JSX.Element {
  const [showBox, setShowBox] = useState(show);
  const handleClose = useCallback((): void => {
    setShowBox(false);
  }, []);

  useEffect((): VoidFunction => {
    const timeout = setTimeout((): void => {
      timer?.(true);
      if (autoHide) {
        handleClose();
      }
    }, time * 1000);

    // eslint-disable-next-line functional/functional-parameters
    return (): void => {
      clearTimeout(timeout);
    };
  }, [autoHide, handleClose, time, timer]);
  useEffect((): void => {
    setShowBox(show);
  }, [show, setShowBox]);

  return (
    <Container
      display={showBox ? "block" : "none"}
      ref={ref}
      role={"alert"}
      {...props}
    >
      <AlertContainer $variant={variant} id={id}>
        <Icon
          clickable={false}
          icon={getIcon(variant)}
          iconSize={"xs"}
          iconType={"fa-light"}
        />

        <MessageContainer>{children}</MessageContainer>
        {closable ? (
          <IconButton
            height={"fit-content"}
            icon={"close"}
            iconSize={"xs"}
            iconType={"fa-light"}
            onClick={handleClose}
            px={0.125}
            py={0.125}
            variant={"ghost"}
          />
        ) : undefined}
      </AlertContainer>
    </Container>
  );
});

export type { IAlertProps };
export { Alert };
