import { IconName } from "@fortawesome/free-solid-svg-icons";

type TPlan = "advanced" | "essential";
type TVersion = "full" | "overview";

/**
 * Plan component props.
 * @interface IPlanProps
 * @property { string } benefits - Benefits of the plan.
 * @property { string } buttonText - Text for the plan's button.
 * @property { string } description - Description of the plan.
 * @property { string } [priceAuthor] - Plan's price per group.
 * @property { string } [priceGroup] - Plan's price per author.
 */
interface IPlanProps {
  benefits: string;
  buttonText: string;
  description: string;
  priceAuthor?: string;
  priceGroup?: string;
  title: string;
}

/**
 * Plan features component props.
 * @interface IPlanFeaturesProps
 * @property { string } description - Description of the plan feature.
 * @property { IconName } icon - Icon for the plan feature.
 * @property { string } title - Title of the plan feature.
 */
interface IPlanFeatureProps {
  description: string;
  icon: IconName;
  title: string;
}

/**
 * Plan card component props.
 * @interface IPlanCardProps
 * @property { string } [description] - Description of the plan.
 * @property { Function } onClick - Function to call when button is clicked.
 * @property { TPlan } plan - Type of plan (advanced or essential).
 * @property { TVersion } [version] - Version of the card (full or overview).
 */
interface IPlanCardProps {
  description?: string;
  onClick: () => void;
  plan: TPlan;
  version?: TVersion;
}

export type { IPlanCardProps, IPlanFeatureProps, IPlanProps, TPlan };
