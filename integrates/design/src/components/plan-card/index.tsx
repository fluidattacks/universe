import { RecommendedTag } from "./recommended-tag";
import { OverviewCardContainer } from "./styles";
import { IPlanCardProps } from "./types";
import { plans } from "./utils";

import { Button } from "components/button";
import { theme } from "components/colors";
import { Container } from "components/container";
import { Heading, Text } from "components/typography";

const PlanCard = ({
  description: customDescription,
  onClick,
  plan,
}: Readonly<IPlanCardProps>): JSX.Element => {
  const {
    title,
    description: defaultDescription,
    benefits,
    buttonText,
  } = plans[plan];
  const description = customDescription ?? defaultDescription;

  return (
    <OverviewCardContainer className={plan}>
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={0.25}
        pt={plan === "essential" ? 1.75 : 0}
      >
        {plan === "advanced" && RecommendedTag}
        <Heading
          color={theme.palette.gray[900]}
          fontWeight={"semibold"}
          size={"sm"}
          sizeSm={"xs"}
        >
          {title}
        </Heading>
        <Text
          color={theme.palette.gray[400]}
          fontWeight={"semibold"}
          pb={plan === "essential" ? 1.25 : 0}
          size={"sm"}
          sizeSm={"xs"}
        >
          {benefits}
        </Text>
      </Container>
      <Text
        lineSpacing={1.5}
        lineSpacingSm={1.25}
        pb={plan === "advanced" ? 1.5 : 0}
        size={"md"}
        sizeSm={"sm"}
      >
        {description}
      </Text>
      <Button onClick={onClick} variant={"primary"}>
        {buttonText}
      </Button>
    </OverviewCardContainer>
  );
};

export { PlanCard };
