import type { Meta, StoryFn } from "@storybook/react";
import { useCallback } from "react";

import { PlanCard } from ".";
import { Container } from "../container";

const config: Meta = {
  component: PlanCard,
  tags: ["autodocs"],
  title: "Components/Cards/PlanCard",
};

const Template: StoryFn = (): JSX.Element => {
  const handleClick = useCallback(() => {}, []);

  return (
    <Container display={"flex"} gap={2} styleSm={"flex-direction: column"}>
      <PlanCard onClick={handleClick} plan={"essential"} version={"overview"} />
      <PlanCard onClick={handleClick} plan={"advanced"} version={"overview"} />
    </Container>
  );
};

const Default = Template.bind({});

export { Default };
export default config;
