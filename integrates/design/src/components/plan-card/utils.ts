import { IPlanFeatureProps, IPlanProps, TPlan } from "./types";

const plans: Record<TPlan, IPlanProps> = {
  advanced: {
    title: "Advanced plan",
    benefits: "Leverage vulnerability scanning + AI + expert intelligence",
    buttonText: "Contact sales",
    description:
      "For teams that want to discover risk exposure with speed and accuracy.",
    priceGroup: "$1,579",
  },
  essential: {
    title: "Essential plan",
    benefits: "Leverage vulnerability scanning",
    buttonText: "Start a 21-day free trial",
    description: `For teams that want to discover risk exposure quickly
        through our scanning tool.`,
    priceAuthor: "$499",
    priceGroup: "$1,579",
  },
};

const advancedPlanFeatures: IPlanFeatureProps[] = [
  {
    title: "All features of the Essential plan",
    icon: "circle-check",
    description: `Benefit from our tool, platform, integrations with IDEs
      and bug-tracking systems, GenAI and CI Agent.`,
  },
  {
    title: "A fusion of technology and human know-how",
    icon: "bullseye-pointer",
    description: `Get a more accurate knowledge of the security status of your
      apps by adding manual testing techniques.`,
  },
  {
    title: "Vulnerabilities of higher severity",
    icon: "sensor-triangle-exclamation",
    description: `Discover vulnerabilities whose exploitation could have more
      damaging impacts on your applications.`,
  },
  {
    title: "Unlimited expert support",
    icon: "message-question",
    description: `Talk to our hackers to solve your doubts about the complex
      vulnerabilities reported.`,
  },
];

const essentialPlanFeatures: IPlanFeatureProps[] = [
  {
    title: "Multiple techniques in a single solution",
    icon: "grid-2-plus",
    description: "Perform automated SAST, DAST, SCA and CSPM continuously.",
  },
  {
    title: "Vulnerability management",
    icon: "scanner-touchscreen",
    description: `Understand security issues and assign treatments from our
      platform and integrations.`,
  },
  {
    title: "Generative AI support",
    icon: "message-code",
    description: `Get vulnerability remediation advice automatically from our
      IDE extensions and platform.`,
  },
  {
    title: "CI Agent",
    icon: "rectangle-terminal",
    description: `Ensure that your applications are free of vulnerabilities
      before going into production.`,
  },
];

export { advancedPlanFeatures, essentialPlanFeatures, plans };
