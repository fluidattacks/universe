import { theme } from "components/colors";
import { Container } from "components/container";
import { Text } from "components/typography";

const RecommendedTag: JSX.Element = (
  <Container
    alignItems={"center"}
    bgColor={theme.palette.primary[25]}
    borderRadius={"8px"}
    gap={0.5}
    justify={"center"}
    padding={[0.125, 0.25, 0.125, 0.25]}
    width={"max-content"}
  >
    <Text
      bgGradient={theme.palette.gradients["01"]}
      fontWeight={"semibold"}
      size={"xs"}
      textFill={"transparent"}
    >
      {"Recommended"}
    </Text>
  </Container>
);

export { RecommendedTag };
