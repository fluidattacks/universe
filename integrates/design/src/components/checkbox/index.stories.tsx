import type { Meta, StoryFn } from "@storybook/react";
import { expect, within } from "@storybook/test";
import { useCallback, useState } from "react";

import type { ICheckboxProps } from "./types";

import { Checkbox } from ".";
import { Container } from "../container";

const config: Meta = {
  component: Checkbox,
  tags: ["autodocs"],
  title: "Components/Checkbox",
};

const CheckboxTemplate: StoryFn<ICheckboxProps> = (
  props: Readonly<ICheckboxProps>,
) => {
  const [checked, setChecked] = useState<boolean>(
    props.defaultChecked ?? false,
  );
  const handleCheck = useCallback((): void => setChecked(!checked), [checked]);
  return (
    <Container className={"flex flex-col gap-2"}>
      <Checkbox
        defaultChecked={checked}
        disabled={props.disabled}
        key={`checkbox-${props.name}`}
        label={props.label}
        name={props.name}
        onChange={handleCheck}
        value={props.value}
      />
    </Container>
  );
};

const Default = CheckboxTemplate.bind({});
Default.args = {
  defaultChecked: false,
  label: "Server into making requests to an arbitrary domain.",
  name: "checkbox1",
};

Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render default checkboxes", async () => {
    await expect(
      canvas.getByRole("checkbox", { name: "checkbox1" }),
    ).not.toBeChecked();
  });
};

const Checked = CheckboxTemplate.bind({});
Checked.args = {
  defaultChecked: true,
  label: "Server into making requests to an arbitrary domain.",
  name: "checkbox1-checked",
};

Checked.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render checked checkboxes", async () => {
    await expect(
      canvas.getByRole("checkbox", { name: "checkbox1-checked" }),
    ).toBeChecked();
  });
};

const Disabled = CheckboxTemplate.bind({});
Disabled.args = {
  defaultChecked: true,
  disabled: true,
  label: "Server into making requests to an arbitrary domain.",
  name: "checkbox1-disabled",
};

Disabled.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render disabled checkboxes", async () => {
    await expect(
      canvas.getByRole("checkbox", { name: "checkbox1-disabled" }),
    ).toBeChecked();
    await expect(
      canvas.getByRole("checkbox", { name: "checkbox1-disabled" }),
    ).toBeDisabled();
  });
};

export { Default, Checked, Disabled };
export default config;
