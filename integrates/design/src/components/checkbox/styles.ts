import { styled } from "styled-components";

const InputContainer = styled.label`
  ${({ theme }): string => `
    align-items: center;
    color: ${theme.palette.gray[600]};
    display: flex;
    cursor: pointer;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    font-weight: ${theme.typography.weight.regular};
    line-height: ${theme.spacing[1]};
    letter-spacing: 0;
    position: relative;
    text-align: left;

    &[aria-disabled="true"] {
      color: ${theme.palette.gray[300]};
      cursor: not-allowed;
    }

    input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }

    svg {
      background-color: ${theme.palette.white};
      border-radius: ${theme.spacing[0.25]};
      border: 1px solid ${theme.palette.gray[600]};
      top: 0;
      left: 0;
      height: ${theme.spacing[0.5]};
      margin-right: ${theme.spacing[0.5]};
      width: ${theme.spacing[0.5]};
      padding: ${theme.spacing[0.25]};
    }

    svg > path {
      fill: ${theme.palette.white};
    }

    input:checked ~ span > svg {
      background-color: ${theme.palette.primary[500]};
      border: 1px solid ${theme.palette.primary[500]};
    }

    input:disabled ~ span > svg {
      border: 1px solid ${theme.palette.gray[300]};
      cursor: not-allowed;
    }

    input:not(:disabled):hover ~ span > svg {
      box-shadow: 0 0 0 4px ${theme.palette.gray[100]};
    }

    input:checked:disabled ~ span > svg {
      background-color: ${theme.palette.gray[300]};
      border: 1px solid ${theme.palette.gray[300]};
    }

    input:not(:disabled):hover:checked ~ span > svg {
      box-shadow: 0 0 0 4px ${theme.palette.primary[50]};
    }

    input:checked ~ span > svg > path {
      fill: ${theme.palette.white};
    }

    input:checked:disabled ~ span > svg > path {
      fill: ${theme.palette.white};
    }
  `}
`;

export { InputContainer };
