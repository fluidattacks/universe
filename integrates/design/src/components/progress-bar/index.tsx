import { Bar, StyledProgressBar } from "./styles";
import type { IProgressBarProps } from "./types";

import { Container } from "components/container";
import { Text } from "components/typography";

const ProgressBar = ({
  minWidth = 100,
  percentage,
  percentageLocation = "right",
  showPercentage = false,
  orientation = "horizontal",
  rounded = true,
  variant = "default",
}: Readonly<IProgressBarProps>): JSX.Element => {
  const showLeftPercentage = showPercentage && percentageLocation === "left";

  const text = showPercentage ? (
    <Container
      alignItems={"center"}
      display={"flex"}
      justify={showLeftPercentage ? "flex-end" : "flex-start"}
      width={"40px"}
    >
      <Text ml={0.5} mr={showLeftPercentage ? 0.5 : 0} size={"sm"}>
        {`${percentage}%`}
      </Text>
    </Container>
  ) : null;
  const bar = (
    <Bar
      $minWidth={minWidth}
      $orientation={orientation}
      $rounded={rounded}
      $variant={variant}
      className={orientation}
    >
      <StyledProgressBar
        $percentage={percentage}
        $rounded={rounded}
        $variant={variant}
        className={"progress-bar"}
      />
    </Bar>
  );

  return (
    <Container
      alignItems={"center"}
      display={"inline-flex"}
      flexDirection={showLeftPercentage ? "row-reverse" : "row"}
      height={orientation === "horizontal" ? "auto" : "100%"}
      maxWidth={showLeftPercentage ? `${minWidth + 43}px` : undefined}
      minWidth={showLeftPercentage ? `${minWidth + 43}px` : undefined}
      textAlign={"end"}
    >
      {bar}
      {text}
    </Container>
  );
};

export { ProgressBar };
