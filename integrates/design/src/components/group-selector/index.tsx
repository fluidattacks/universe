import { KeyboardEvent, useCallback, useEffect, useState } from "react";
import { createPortal } from "react-dom";
import { useTheme } from "styled-components";

import { OptionBoxContainer, OptionContainer } from "./option-container";
import { StyledGroupContainer } from "./styles";
import type { IGroupSelectorProps } from "./types";

import { Container } from "components/container";
import { Divider } from "components/divider";
import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";
import { Search } from "components/search";
import { Text } from "components/typography";

const GroupSelector = ({
  isOpen = true,
  items,
  handleNewOrganization,
  onSelect,
  onClose,
  selectedItem,
  title,
  variant,
}: Readonly<IGroupSelectorProps>): JSX.Element => {
  const theme = useTheme();
  const [filteredItems, setFilteredItems] = useState(items);
  const [searchedValue, setSearchedValue] = useState("");

  const updateFilterState = useCallback(
    (value: string): void => {
      setFilteredItems((previousProps): { name: string }[] =>
        previousProps.filter((item): boolean =>
          String(item.name).toLowerCase().includes(value.toLocaleLowerCase()),
        ),
      );
    },
    [setFilteredItems],
  );

  const onSearch = useCallback(
    (event: Readonly<KeyboardEvent<HTMLInputElement>>): void => {
      if (event.currentTarget) {
        setSearchedValue(event.currentTarget.value);
        updateFilterState(event.currentTarget.value);
      }
    },
    [setSearchedValue, updateFilterState],
  );

  useEffect((): void => {
    setFilteredItems(items);
    updateFilterState(searchedValue);
  }, [items, updateFilterState, searchedValue]);

  const selector: JSX.Element = (
    <Container
      aria-modal={"true"}
      className={"comp-modal fixed inset-0 overflow-auto"}
      zIndex={99999}
    >
      <Container
        alignItems={"center"}
        bgColor={"rgb(52 64 84 / 70%)"}
        display={"flex"}
        height={"100vh"}
        justify={"center"}
        left={"0"}
        top={"0"}
        width={"100%"}
      >
        <StyledGroupContainer $itemsLength={filteredItems.length}>
          <Container
            alignItems={"center"}
            display={"flex"}
            justify={"space-between"}
          >
            <Text
              color={theme.palette.gray[800]}
              fontWeight={"bold"}
              size={"lg"}
            >
              {title}
            </Text>
            <IconButton
              icon={"xmark"}
              iconColor={theme.palette.gray[400]}
              iconSize={"xs"}
              iconType={"fa-light"}
              onClick={onClose}
              variant={"ghost"}
            />
          </Container>
          <Container mt={1}>
            <Search onInput={onSearch} placeholder={"Search"} />
          </Container>
          <Divider mb={1} mt={1} />
          <Container
            display={"inline-flex"}
            gap={1}
            maxHeight={"332px"}
            scroll={"y"}
            wrap={"wrap"}
          >
            {variant === "organization-selector" ? (
              <OptionBoxContainer onClick={handleNewOrganization}>
                <Container alignItems={"center"} display={"flex"}>
                  <Icon
                    icon={"square-plus"}
                    iconColor={theme.palette.gray[400]}
                    iconSize={"xs"}
                    iconType={"fa-light"}
                    mr={0.5}
                  />
                  <Text color={theme.palette.gray[400]} size={"sm"}>
                    {"New organization"}
                  </Text>
                </Container>
              </OptionBoxContainer>
            ) : undefined}
            {filteredItems.map(
              (item): JSX.Element => (
                <OptionContainer
                  isSelected={selectedItem.toLowerCase() === item.name}
                  key={item.name}
                  label={item.name.charAt(0).toUpperCase() + item.name.slice(1)}
                  onSelect={onSelect}
                />
              ),
            )}
          </Container>
        </StyledGroupContainer>
      </Container>
    </Container>
  );

  return isOpen ? createPortal(selector, document.body) : <div />;
};

export { GroupSelector };
