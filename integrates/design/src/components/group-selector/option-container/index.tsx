import { useCallback } from "react";
import { useTheme } from "styled-components";

import { OptionBoxContainer } from "../styles";
import type { IOptionProps } from "../types";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const OptionContainer = ({
  isSelected = false,
  label,
  onSelect,
}: Readonly<IOptionProps>): JSX.Element => {
  const theme = useTheme();
  const handleClick = useCallback((): void => {
    if (onSelect) {
      onSelect(label);
    }
  }, [onSelect, label]);

  return (
    <OptionBoxContainer $isSelected={isSelected} onClick={handleClick}>
      {isSelected ? (
        <Icon
          icon={"check"}
          iconColor={theme.palette.gray[800]}
          iconSize={"xs"}
        />
      ) : null}
      <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"sm"}>
        {label}
      </Text>
    </OptionBoxContainer>
  );
};

export { OptionContainer, OptionBoxContainer };
