import { styled } from "styled-components";

const StyledGroupContainer = styled.div<{ $itemsLength: number }>`
  ${({ theme, $itemsLength }): string => `
    display: flex;
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    box-shadow: ${theme.shadows.lg};
    flex-direction: column;
    min-width: 507px;
    max-width: ${$itemsLength <= 4 ? "507px" : "980px"};
    padding: ${theme.spacing[1.5]};

    @media screen and (max-width: 980px) and (min-width: 742px) {
      width: 742px;
    }
    @media screen and (max-width: 742px) {
      width: 507px;
    }
  `}
`;

const OptionBoxContainer = styled.div<{ $isSelected?: boolean }>`
  ${({ theme, $isSelected }): string => `
    align-items: center;
    background-color: ${
      $isSelected ? theme.palette.gray[100] : theme.palette.white
    };
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    cursor: pointer;
    display: flex;
    flex-direction: ${$isSelected ? "row-reverse" : "row"};
    min-height: 50px;
    padding: ${theme.spacing[1]} ${theme.spacing[0.625]};
    width: 215px;

    &:hover {
      background-color: ${theme.palette.gray[100]};
    }
  `}
`;

export { StyledGroupContainer, OptionBoxContainer };
