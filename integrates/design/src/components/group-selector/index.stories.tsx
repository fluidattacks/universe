import type { Meta, StoryFn } from "@storybook/react";
import React, { useCallback, useState } from "react";

import type { IGroupSelectorProps } from "./types";

import { GroupSelector } from ".";
import { useModal } from "../../hooks";
import { Button } from "../button";

const config: Meta = {
  component: GroupSelector,
  parameters: {
    controls: {
      exclude: ["isOpen", "onClickOpen", "onClose"],
    },
  },
  tags: ["autodocs"],
  title: "Components/GroupSelector",
};

const Template: StoryFn<IGroupSelectorProps> = ({
  items,
  selectedItem,
  title,
  variant,
}: Readonly<IGroupSelectorProps>): JSX.Element => {
  const [selected, setSelected] = useState<string>(selectedItem);
  const { close, isOpen, open } = useModal("item-selector");
  const handleClick = useCallback(
    (group: string): void => {
      setSelected(group);
    },
    [setSelected],
  );

  return (
    <React.Fragment>
      <Button onClick={open}>{"Click to select"}</Button>
      <GroupSelector
        isOpen={isOpen}
        items={items}
        onClose={close}
        onSelect={handleClick}
        selectedItem={selected}
        title={title}
        variant={variant}
      />
    </React.Fragment>
  );
};

const groupShort = [
  { name: "Group 1" },
  { name: "Group 2" },
  { name: "Group 3" },
  { name: "Group 4" },
];

const groups = [
  { name: "Group 1" },
  { name: "Group 2" },
  { name: "Group 3" },
  { name: "Group 4" },
  { name: "Group 5" },
  { name: "Group 6" },
  { name: "Group 7" },
  { name: "Group 8" },
  { name: "Group 9" },
  { name: "Group 10" },
  { name: "Group 11" },
];

const organizations = [
  { name: "Organization 1" },
  { name: "Organization 2" },
  { name: "Organization 3" },
  { name: "Organization 4" },
  { name: "Organization 5" },
  { name: "Organization 6" },
  { name: "Organization 7" },
  { name: "Organization 8" },
  { name: "Organization 9" },
  { name: "Organization 10" },
  { name: "Organization 11" },
  { name: "Organization 12" },
  { name: "Organization 13" },
  { name: "Organization 14" },
  { name: "Organization 15" },
  { name: "Organization 16" },
  { name: "Organization 17" },
  { name: "Organization 18" },
  { name: "Organization 19" },
  { name: "Organization 20" },
  { name: "Organization 21" },
  { name: "Organization 22" },
  { name: "Organization 23" },
  { name: "Organization 24" },
  { name: "Organization 25" },
  { name: "Organization 26" },
  { name: "Organization 27" },
];

const Default = Template.bind({});
Default.args = {
  items: groups,
  selectedItem: "Group 3",
  title: "Choose a group of the Organization",
};

const Short = Template.bind({});
Short.args = {
  items: groupShort,
  selectedItem: "Group 1",
  title: "Choose a group of the Organization",
};

const OrganizationVariant = Template.bind({});
OrganizationVariant.args = {
  items: organizations,
  selectedItem: "Organization 1",
  title: "Choose an organization",
  variant: "organization-selector",
};

export default config;
export { Short, Default, OrganizationVariant };
