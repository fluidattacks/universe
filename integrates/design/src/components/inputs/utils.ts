/* eslint-disable functional/immutable-data */
const createEvent = (type: string, name: string, value: unknown): Event => {
  const event = new Event(type);
  Object.defineProperty(event, "target", {
    value: { name, value },
  });

  return event;
};

const getFileName = (value: unknown): string => {
  const files = value as FileList | undefined;

  if (files) {
    return Array.from(files)
      .map((file): string => file.name)
      .join(", ");
  }

  return "";
};

export { createEvent, getFileName };
