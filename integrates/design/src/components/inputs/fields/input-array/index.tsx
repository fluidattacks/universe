/* eslint-disable react/jsx-no-bind */
import { Fragment } from "react";
import { Controller } from "react-hook-form";

import { ArrayField } from "./field";

import type { IInputArrayProps } from "../../types";

const InputArray = ({
  addText,
  append,
  control,
  disabled,
  fields,
  initValue = "",
  required,
  label,
  max = undefined,
  name = "array-field",
  onChange,
  placeholder,
  remove,
  tooltip,
}: Readonly<IInputArrayProps>): JSX.Element => {
  if (control && fields) {
    return (
      <Fragment>
        {fields.map((fieldProp, index) => (
          <Controller
            control={control}
            key={fieldProp.id}
            name={`${name}.${index}`}
            render={({ field, fieldState }) => (
              <ArrayField
                addText={addText}
                append={append}
                disabled={disabled}
                fieldState={fieldState}
                fields={fields}
                index={index}
                initValue={initValue}
                label={label}
                max={max}
                name={field.name}
                onBlur={field.onBlur}
                onChange={onChange ?? field.onChange}
                placeholder={placeholder}
                remove={remove}
                required={required}
                tooltip={tooltip}
                value={field.value.value}
              />
            )}
            {...{ index, fieldProp }}
          />
        ))}
      </Fragment>
    );
  }

  return (
    <ArrayField
      addText={addText}
      disabled={disabled}
      initValue={initValue}
      label={label}
      max={max}
      name={name}
      onChange={onChange}
      placeholder={placeholder}
      required={required}
      tooltip={tooltip}
    />
  );
};

export { InputArray };
