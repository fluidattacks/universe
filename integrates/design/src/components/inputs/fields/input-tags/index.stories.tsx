import type { Meta, StoryFn } from "@storybook/react";

import { InputTags } from ".";
import { Form } from "../../../form";
import type { IInputTagProps } from "../../types";

const config: Meta = {
  component: InputTags,
  tags: ["autodocs"],
  title: "Components/Inputs/InputTags",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<IInputTagProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ textinputtag: "" }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <InputTags {...props} />
  </Form>
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Tag input label",
  name: "textinputtag",
  placeholder: "Write a tag",
  required: false,
  tooltip: "",
};

const Help = Template.bind({});
Help.args = {
  disabled: false,
  helpText: "This is an example help text",
  label: "Tag input label",
  name: "textinputtaglink",
  placeholder: "Write a tag",
  required: false,
  tooltip: "",
};

export { Default, Help };
export default config;
