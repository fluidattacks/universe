/* eslint-disable @typescript-eslint/no-explicit-any */
import type { ChangeEvent, FocusEvent, KeyboardEvent } from "react";
import { useCallback, useMemo, useState } from "react";
import type { ControllerRenderProps } from "react-hook-form";
import { useTheme } from "styled-components";

import { OutlineContainer } from "../../../outline-container";
import { StyledTextInputContainer } from "../../../styles";
import type { IInputTagProps } from "../../../types";
import { createEvent } from "../../../utils";
import { TagInput } from "../styles";
import { Icon } from "components/icon";
import { Tag } from "components/tag";

const TagsField = ({
  disabled = false,
  error,
  field,
  helpLink,
  helpLinkText,
  helpText,
  label,
  name = "input-tag",
  onChange,
  onRemove,
  placeholder,
  required,
  tooltip,
  value: valueProp,
  weight,
}: Readonly<
  IInputTagProps & { field?: ControllerRenderProps<any> }
>): JSX.Element => {
  const theme = useTheme();
  const errorMsg = disabled ? undefined : error;
  const [inputValue, setInputValue] = useState("");
  const value = onChange ? valueProp : field?.value;

  const tags = useMemo((): string[] => {
    return value && typeof value === "string"
      ? value.split(",").filter(Boolean)
      : [];
  }, [value]);

  const setTags = useCallback(
    (values: Readonly<string[]>): void => {
      const newTags = new Set(values.filter(Boolean));
      const newValue = [...newTags].join(",");

      const changeEvent = createEvent("change", name, newValue);
      if (field) field.onChange(changeEvent);
      if (onChange)
        onChange(changeEvent as unknown as ChangeEvent<HTMLInputElement>);
    },
    [field, name, onChange],
  );

  const handleOnChange = useCallback(
    (event: Readonly<ChangeEvent<HTMLInputElement>>): void => {
      setInputValue(event.target.value);
    },
    [],
  );

  const handleOnBlur = useCallback(
    (event: Readonly<FocusEvent<HTMLInputElement>>): void => {
      const trimmedValue = inputValue.trim();
      if (trimmedValue.length && !tags.includes(trimmedValue)) {
        event.preventDefault();

        setTags([...tags, trimmedValue]);
        setInputValue("");
      }
      if (field) field.onBlur();
    },
    [inputValue, field, setTags, tags],
  );

  const handleOnKeyDown = useCallback(
    (event: Readonly<KeyboardEvent<HTMLInputElement>>): void => {
      const trimmedValue = inputValue.trim();

      if (event.key === "Enter" && trimmedValue.length === 0)
        event.preventDefault();
      if (
        ["Enter", ","].includes(event.key) &&
        trimmedValue.length > 0 &&
        !tags.includes(trimmedValue)
      ) {
        event.preventDefault();

        setTags([...tags, trimmedValue]);
        setInputValue("");
      } else if (
        event.key === "Backspace" &&
        !trimmedValue.length &&
        tags.length
      ) {
        event.preventDefault();
        onRemove?.(tags[tags.length - 1]);
        setTags(tags.slice(0, -1));
      }
    },
    [inputValue, onRemove, setTags, tags],
  );

  const removeTag = useCallback(
    (tag: string): VoidFunction => {
      // eslint-disable-next-line functional/functional-parameters
      return (): void => {
        onRemove?.(tag);
        setTags(tags.filter((currentValue): boolean => currentValue !== tag));
      };
    },
    [onRemove, setTags, tags],
  );

  return (
    <OutlineContainer
      error={errorMsg}
      helpLink={helpLink}
      helpLinkText={helpLinkText}
      helpText={helpText}
      htmlFor={name}
      label={label}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <StyledTextInputContainer
        $overflowX={"auto"}
        className={`${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""}`}
      >
        {tags.map((tag): JSX.Element => {
          return (
            <Tag
              disabled={disabled}
              key={tag}
              onClose={removeTag(tag)}
              tagLabel={tag}
            />
          );
        })}
        <TagInput
          aria-hidden={false}
          aria-invalid={errorMsg ? "true" : "false"}
          aria-label={name}
          aria-required={required}
          autoComplete={"off"}
          id={name}
          onBlur={handleOnBlur}
          onChange={handleOnChange}
          onKeyDown={handleOnKeyDown}
          placeholder={tags.length === 0 ? placeholder : undefined}
          ref={field?.ref}
          type={"text"}
          value={inputValue}
        />
        {errorMsg ? (
          <Icon
            icon={"exclamation-circle"}
            iconClass={"error-icon"}
            iconColor={theme.palette.error[500]}
            iconSize={"xs"}
          />
        ) : undefined}
      </StyledTextInputContainer>
    </OutlineContainer>
  );
};

export { TagsField };
