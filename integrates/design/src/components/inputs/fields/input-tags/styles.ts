import { styled } from "styled-components";

const TagInput = styled.input`
  border: none;
  background: transparent;
  min-width: 15rem;
  padding: 0;
  flex: 1;
`;

export { TagInput };
