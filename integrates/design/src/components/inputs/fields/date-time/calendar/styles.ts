import { styled } from "styled-components";

const TimeOutlineContainer = styled.div`
  ${({ theme }): string => `
    align-items: center;
    border-top: 1px solid ${theme.palette.gray[200]};
    display: flex;
    gap: ${theme.spacing[1.5]};
    justify-content: center;
    padding: ${theme.spacing[0.5]} ${theme.spacing[1.25]};
  `}
`;

const OutlineContainer = styled.div`
  align-items: center;
  display: flex;
  gap: ${({ theme }): string => theme.spacing[0.5]};
  justify-content: flex-end;
  flex: 1 0 0;
`;

export { OutlineContainer, TimeOutlineContainer };
