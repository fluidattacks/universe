import { parseDateTime } from "@internationalized/date";
import _ from "lodash";
import { useCallback, useRef } from "react";
import { DateValue, TimeValue, useDatePicker } from "react-aria";
import { useDatePickerState } from "react-stately";

import { Calendar } from "./calendar";

import { OutlineContainer } from "../../outline-container";
import { DateSelector } from "../../utils/date-selector";
import { Dialog } from "../../utils/dialog";
import { Popover } from "../../utils/popover";
import type { TInputDateProps } from "../date/types";
import { formatDateTimeInInput } from "utils/date";

const InputDateTime = (props: Readonly<TInputDateProps>): JSX.Element => {
  const {
    disabled = false,
    error,
    required,
    label,
    name,
    tooltip,
    register,
    setValue,
    watch,
  } = props;
  const value = watch?.(name);
  const datePickerRef = useRef(null);

  const state = useDatePickerState({
    ...props,
    granularity: "minute",
    hourCycle: 12,
    isDisabled: disabled,
    isRequired: required,
    shouldCloseOnSelect: false,
    value:
      _.isNil(value) || _.isEmpty(value) || value === "-"
        ? null
        : parseDateTime((value as string).replace(/\//gu, "-")),
  });
  const { close } = state;
  const { groupProps, fieldProps, buttonProps, dialogProps, calendarProps } =
    useDatePicker(props, state, datePickerRef);

  const handleCalendarChange = useCallback(
    (selectedValue: DateValue): void => {
      if (calendarProps.onChange) {
        calendarProps.onChange(selectedValue);
      }
      setValue?.(
        name,
        formatDateTimeInInput(String(selectedValue).replace(/-/gu, "/")),
      );
    },
    [calendarProps, name, setValue],
  );

  const handleTimeChange = useCallback(
    (selectedValue: TimeValue | null): void => {
      if (selectedValue) state.setTimeValue(selectedValue);
    },
    [state],
  );

  return (
    <OutlineContainer
      error={error}
      htmlFor={name}
      label={label}
      required={required}
      tooltip={tooltip}
    >
      <DateSelector
        buttonProps={buttonProps}
        datePickerRef={datePickerRef}
        disabled={disabled}
        error={error}
        fieldProps={fieldProps}
        granularity={"minute"}
        groupProps={groupProps}
        name={name}
        register={register}
        setValue={setValue}
        value={value}
      />
      {state.isOpen && !disabled ? (
        <Popover
          offset={8}
          placement={"bottom start"}
          state={state}
          triggerRef={datePickerRef}
        >
          <Dialog {...dialogProps}>
            <Calendar
              handleTimeChange={handleTimeChange}
              onClose={close}
              props={{ ...calendarProps, onChange: handleCalendarChange }}
              timeState={state}
            />
          </Dialog>
        </Popover>
      ) : null}
    </OutlineContainer>
  );
};

export type { TInputDateProps };
export { InputDateTime };
