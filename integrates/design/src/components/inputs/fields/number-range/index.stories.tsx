import type { Meta, StoryFn } from "@storybook/react";

import { InputNumberRange } from ".";
import type { IInputNumberRangeProps } from "../../types";

const config: Meta = {
  component: InputNumberRange,
  tags: ["autodocs"],
  title: "Components/Inputs/InputNumberRange",
};

const Template: StoryFn<IInputNumberRangeProps> = (props): JSX.Element => (
  <InputNumberRange {...props} />
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Label test date range",
  name: "inputNumberRange",
  required: false,
};

const ErrorState = Template.bind({});
ErrorState.args = {
  disabled: false,
  label: "Label test date range",
  name: "numberRange",
  required: false,
};

export { Default, ErrorState };
export default config;
