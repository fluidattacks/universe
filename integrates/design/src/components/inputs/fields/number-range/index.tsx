import { OutlineContainer } from "../../outline-container";
import type { IInputNumberRangeProps } from "../../types";
import { NumberField } from "../../utils/number-field";
import { Container } from "components/container";

const InputNumberRange = ({
  disabled = false,
  error,
  name,
  label,
  propsMax,
  propsMin,
  required,
  tooltip,
  variant = "default",
}: Readonly<IInputNumberRangeProps>): JSX.Element => {
  const nameMax = `${name}Max`;
  const nameMin = `${name}Min`;

  return (
    <OutlineContainer
      error={error}
      htmlFor={name}
      label={label}
      required={required}
      tooltip={tooltip}
    >
      <Container
        display={"flex"}
        flexDirection={variant === "filters" ? "column" : "row"}
        gap={0.25}
        width={"100%"}
      >
        <NumberField
          {...propsMin}
          disabled={disabled}
          error={error}
          name={propsMin.name ?? nameMin}
        />
        <NumberField
          {...propsMax}
          disabled={disabled}
          error={error}
          name={propsMax.name ?? nameMax}
        />
      </Container>
    </OutlineContainer>
  );
};

export { InputNumberRange };
