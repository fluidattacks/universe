import { autoUpdate, useFloating } from "@floating-ui/react-dom";
import { useCallback, useRef, useState } from "react";
import { createPortal } from "react-dom";
import {
  FlagImage,
  defaultCountries,
  parseCountry,
  usePhoneInput,
} from "react-international-phone";
import { useTheme } from "styled-components";

import {
  StyledCountryItem,
  StyledCountryName,
  StyledCountrySelector,
  StyledDropdown,
  StyledPhoneInputContainer,
} from "./styles";

import { OutlineContainer } from "../../outline-container";
import { StyledTextInputContainer } from "../../styles";
import { IInputProps } from "../../types";
import { Icon } from "components/icon";
import { Text } from "components/typography";
import { useClickOutside } from "hooks/use-click-outside";

const PhoneInput = ({
  disabled,
  error,
  label,
  name,
  placeholder,
  required,
  register,
  tooltip,
  value: valueProp,
  watch,
  weight,
}: Readonly<IInputProps>): JSX.Element => {
  const theme = useTheme();
  const containerRef = useRef<HTMLDivElement>(null);
  const errorMsg = disabled ? undefined : error;
  const value = watch?.(name) ?? valueProp;
  const [isOpen, setIsOpen] = useState(false);
  const [search, setSearch] = useState("");

  const { refs, floatingStyles } = useFloating({
    placement: "bottom-start",
    whileElementsMounted: autoUpdate,
  });
  const { inputRef, inputValue, handlePhoneValueChange, country, setCountry } =
    usePhoneInput({
      countries: defaultCountries,
      defaultCountry: "co",
      forceDialCode: true,
      preferredCountries: ["co", "us", "ar", "ec", "pa"],
      value: value ? String(value) : "",
    });

  const onKeyDown = useCallback(
    (event: Readonly<React.KeyboardEvent<HTMLInputElement>>): void => {
      if (event.key === "Enter") {
        event.preventDefault();
      }
    },
    [],
  );
  const handleDropdown = useCallback(
    (event: Readonly<React.MouseEvent<HTMLButtonElement>>): void => {
      event.preventDefault();
      setIsOpen((previous): boolean => !previous);
      if (!isOpen) {
        setSearch("");
      }
    },
    [isOpen],
  );
  const handleCountrySelect = useCallback(
    (selectedCountry: string): VoidFunction => {
      // eslint-disable-next-line functional/functional-parameters
      return (): void => {
        setCountry(selectedCountry);
        setIsOpen(false);
        setSearch("");
      };
    },
    [setCountry, setIsOpen],
  );
  const handleKeyDown = useCallback(
    (event: Readonly<React.KeyboardEvent<HTMLButtonElement>>): void => {
      if (event.key.length === 1) {
        setSearch((prev) => prev + event.key);
      } else if (event.key === "Backspace") {
        setSearch((prev) => prev.slice(0, -1));
      }
    },
    [],
  );
  const filteredCountries = defaultCountries.filter((item) => {
    const { name } = parseCountry(item);
    return name.toLowerCase().includes(search.toLowerCase());
  });

  useClickOutside(
    refs.floating.current as HTMLElement,
    (): void => {
      setIsOpen(false);
      setSearch("");
    },
    true,
  );

  return (
    <OutlineContainer
      error={errorMsg}
      htmlFor={name}
      label={label}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <StyledPhoneInputContainer
        className={`${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""}`}
        ref={containerRef}
      >
        <StyledCountrySelector
          aria-label={"country-selector"}
          data-testid={"country-selector"}
          disabled={disabled}
          id={"country-selector"}
          onClick={handleDropdown}
          onKeyDown={handleKeyDown}
          ref={refs.setReference}
        >
          <FlagImage iso2={country.iso2} size={"16px"} />
          <Icon
            icon={"angle-down"}
            iconColor={theme.palette.gray[400]}
            iconSize={"xs"}
            iconType={"fa-light"}
          />
        </StyledCountrySelector>
        <StyledTextInputContainer
          className={`${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""}`}
        >
          <input
            aria-hidden={false}
            aria-invalid={errorMsg ? "true" : "false"}
            aria-label={name}
            aria-required={required}
            autoComplete={"off"}
            data-testid={`${name}-input`}
            disabled={disabled}
            id={name}
            name={name}
            onKeyDown={onKeyDown}
            placeholder={placeholder}
            ref={inputRef}
            type={"tel"}
            value={inputValue}
            {...register?.(name, {
              required,
              onChange: handlePhoneValueChange,
            })}
          />
          {errorMsg ? (
            <Icon
              icon={"exclamation-circle"}
              iconClass={"error-icon"}
              iconColor={theme.palette.error[500]}
              iconSize={"xs"}
            />
          ) : undefined}
        </StyledTextInputContainer>
        {isOpen &&
          createPortal(
            <StyledDropdown ref={refs.setFloating} style={floatingStyles}>
              {filteredCountries.map((item) => {
                const { dialCode, iso2, name } = parseCountry(item);
                return (
                  <StyledCountryItem
                    key={iso2}
                    onClick={handleCountrySelect(iso2)}
                    value={iso2}
                  >
                    <FlagImage iso2={iso2} size={"16px"} />
                    <StyledCountryName>{name}</StyledCountryName>
                    <Text
                      color={theme.palette.gray[400]}
                      size={"sm"}
                      sizeSm={"xs"}
                      textAlign={"end"}
                    >
                      {"+" + dialCode}
                    </Text>
                  </StyledCountryItem>
                );
              })}
            </StyledDropdown>,
            document.getElementById("modal-wrapper") ??
              document.getElementById("root") ??
              document.body,
          )}
      </StyledPhoneInputContainer>
    </OutlineContainer>
  );
};

export { PhoneInput };
