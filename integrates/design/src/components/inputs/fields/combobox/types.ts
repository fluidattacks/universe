/* eslint-disable @typescript-eslint/no-explicit-any */
import type { Node } from "@react-types/shared";
import { InputHTMLAttributes } from "react";
import type { Control } from "react-hook-form";
import type { ComboBoxState } from "react-stately";

import type { IOutlineContainerProps } from "../../types";

/**
 * Props for the item component.
 * @interface IItem
 * @property { string } name - The name of the item.
 * @property { string } value - The value of the item.
 */
interface IItem {
  name: string;
  value: string;
}

/**
 * Props for the option component.
 * @interface IOptionProps
 * @property { IItem } item - The item to be rendered as an option.
 * @property { ComboBoxState<T> } state - The state of the ComboBox.
 */
interface IOptionProps<T> {
  item: Node<IItem>;
  state: ComboBoxState<T>;
}

/**
 * Props for the Select component.
 * @interface IComboBoxProps
 * @property { boolean } [disabled] - Whether the Select is disabled.
 * @property { string } name - The name of the Select.
 * @property { IItem[] } items - The items to be rendered as options.
 * @property { boolean } [loadingItems] - Whether the Select items are loading.
 * @property { Function } [onBlur] - The callback to be called on input blur.
 * @property { Function } [onChange] - The callback to be called on input change.
 * @property { string } [placeholder] - The placeholder for the input.
 */
interface IComboBoxProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, "onChange" | "onBlur">,
    Partial<Omit<IOutlineContainerProps, "maxLength" | "value">> {
  control?: Control<any>;
  disabled?: boolean;
  items: IItem[];
  loadingItems?: boolean;
  name: string;
  onBlur?: () => void;
  onChange?: (value: string) => void;
  placeholder?: string;
}

export type { IItem, IOptionProps, IComboBoxProps };
