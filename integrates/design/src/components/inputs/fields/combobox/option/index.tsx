import { useRef } from "react";
import { useOption } from "react-aria";
import { useTheme } from "styled-components";

import { StyledOption } from "../styles";
import { IOptionProps } from "../types";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const Option = <T extends object>({
  item,
  state,
}: Readonly<IOptionProps<T>>): JSX.Element => {
  const theme = useTheme();
  const ref = useRef(null);
  const { optionProps, isSelected, isFocused, isDisabled } = useOption(
    { key: item.key },
    state,
    ref,
  );

  const classes = [
    isSelected ? "selected" : "",
    isFocused ? "focused" : "",
    isDisabled ? "disabled" : "",
  ].join(" ");

  return (
    <StyledOption {...optionProps} className={classes} ref={ref}>
      <Text
        color={theme.palette.gray[800]}
        size={"sm"}
        textOverflow={"ellipsis"}
        whiteSpace={"nowrap"}
      >
        {item.rendered}
      </Text>
      {isSelected ? (
        <Icon icon={"check"} iconSize={"xxs"} iconType={"fa-light"} />
      ) : undefined}
    </StyledOption>
  );
};

export { Option };
