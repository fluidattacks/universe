import { styled } from "styled-components";

const StyledDropdown = styled.ul`
  ${({ theme }): string => `
    align-self: center;
    background: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    box-shadow: ${theme.shadows.md};
    list-style: none;
    max-height: 200px;
    min-width: 240px;
    overflow: hidden auto;
    scrollbar-color: ${theme.palette.gray[600]} ${theme.palette.gray[100]};
    scroll-padding: ${theme.spacing[0.5]};
    width: 95%;
  `}
`;

const StyledOption = styled.li`
  ${({ theme }): string => `
    align-items: center;
    color: ${theme.palette.gray[800]};
    cursor: pointer;
    display: flex;
    gap: ${theme.spacing[0.5]};
    justify-content: space-between;
    line-height: ${theme.spacing[1.25]};
    padding: ${theme.spacing[0.25]} ${theme.spacing[1]};

    p {
      overflow: hidden;
    }

    span {
      color: ${theme.palette.gray[700]};
    }

    &:hover {
      background: ${theme.palette.gray[100]};
    }

    &.selected {
      background: ${theme.palette.gray[200]};
    }

    &.disabled {
      cursor: not-allowed;

      p {
        color: ${theme.palette.gray[400]};
      }
    }
  `}
`;

export { StyledDropdown, StyledOption };
