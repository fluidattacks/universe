/* eslint-disable @typescript-eslint/no-explicit-any */
import { CollectionChildren, CollectionElement } from "@react-types/shared";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Key, useButton, useComboBox, useFilter, useListBox } from "react-aria";
import { type ControllerRenderProps } from "react-hook-form";
import { Item, useComboBoxState } from "react-stately";
import { useTheme } from "styled-components";

import { OutlineContainer } from "../../../outline-container";
import { StyledTextInputContainer } from "../../../styles";
import { Popover } from "../../../utils/popover";
import { Option } from "../option";
import { StyledDropdown } from "../styles";
import { IComboBoxProps, IItem } from "../types";
import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";

const ComboBoxField = ({
  field,
  disabled = false,
  error,
  helpLink,
  helpLinkText,
  helpText,
  label,
  loadingItems,
  name,
  items,
  onBlur: onBlurProp,
  onChange: onChangeProp,
  placeholder,
  required,
  tooltip,
  value,
  weight,
}: Readonly<
  IComboBoxProps & { field?: ControllerRenderProps<any> }
>): JSX.Element => {
  const theme = useTheme();
  const errorMsg = disabled ? undefined : error;
  const fieldValue = field?.value ?? value;

  const [inputValue, setInputValue] = useState(
    items.find((item): boolean => item.value === fieldValue)?.name ?? "",
  );
  const [selectedKey, setSelectedKey] = useState(fieldValue);
  const [filteredItems, setFilteredItems] = useState(items);
  const [showAll, setShowAll] = useState(false);
  const { contains } = useFilter({ sensitivity: "base" });

  const inputRef = useRef<HTMLInputElement>(null);
  const buttonRef = useRef(null);
  const listBoxRef = useRef(null);
  const comboboxRef = useRef(null);
  const popoverRef = useRef(null);

  const childrenCombobox = useMemo((): CollectionChildren<IItem> => {
    return items.map(
      (item): CollectionElement<IItem> =>
        contains(item.name, inputValue) || showAll ? (
          <Item key={item.value}>{item.name}</Item>
        ) : null,
    );
  }, [contains, inputValue, items, showAll]);

  const onInputChange = useCallback(
    (value: string): void => {
      setInputValue(value);
      if (value === "") setShowAll(true);
      else {
        setShowAll(false);
        setSelectedKey((prev: string): string => {
          const newValue = value === "" ? "" : prev;
          if (field) field.onChange(newValue);
          if (onChangeProp) onChangeProp(value);
          return newValue;
        });
        setFilteredItems(items.filter((item) => contains(item.name, value)));
      }
    },
    [contains, items, field, onChangeProp],
  );

  const onSelectionChange = useCallback(
    (key: Key | null): void => {
      setInputValue(items.find((item) => item.value === key)?.name ?? "");
      setSelectedKey(key ? String(key) : "");
      if (field) field.onChange(key);
      if (onChangeProp) onChangeProp(key ? String(key) : "");
      setFilteredItems(items);
    },
    [items, field, onChangeProp],
  );

  const onKeyDown = useCallback(
    (event: Readonly<React.KeyboardEvent<HTMLInputElement>>): void => {
      if (event.key === "Enter") event.preventDefault();
      setShowAll(false);
    },
    [],
  );

  const handleBlur = useCallback((): void => {
    if (onBlurProp) onBlurProp();
    if (field) field.onBlur();
  }, [field, onBlurProp]);

  const state = useComboBoxState({
    allowsEmptyCollection: true,
    inputValue,
    items: filteredItems,
    menuTrigger: "focus",
    shouldCloseOnBlur: true,
    selectedKey,
    onBlur: handleBlur,
    onInputChange,
    onKeyDown,
    // eslint-disable-next-line functional/functional-parameters
    onKeyUp: () => {
      setShowAll(true);
    },
    onOpenChange(isOpen, menuTrigger) {
      setShowAll(
        isOpen && (menuTrigger === "manual" || menuTrigger === "focus"),
      );
    },
    onSelectionChange,
    children: childrenCombobox,
  });

  const {
    buttonProps: comboBoxButtonProps,
    inputProps,
    listBoxProps: comboBoxListBoxProps,
  } = useComboBox(
    {
      inputValue,
      buttonRef,
      id: name,
      inputRef,
      label: label ?? name,
      listBoxRef,
      name,
      placeholder,
      popoverRef,
    },
    state,
  );

  const { listBoxProps } = useListBox(comboBoxListBoxProps, state, listBoxRef);
  const { buttonProps } = useButton(comboBoxButtonProps, buttonRef);

  useEffect(() => {
    if (loadingItems === false || !fieldValue) {
      setInputValue(
        items.find((item): boolean => item.value === fieldValue)?.name ?? "",
      );
    }
  }, [fieldValue, items, loadingItems]);

  return (
    <OutlineContainer
      error={errorMsg}
      helpLink={helpLink}
      helpLinkText={helpLinkText}
      helpText={helpText}
      htmlFor={name}
      label={label}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <StyledTextInputContainer
        className={`${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""}`}
        ref={comboboxRef}
      >
        <input
          {...inputProps}
          aria-invalid={errorMsg ? "true" : "false"}
          aria-label={name}
          onKeyDown={onKeyDown}
          ref={inputRef}
        />
        {errorMsg ? (
          <Icon
            icon={"exclamation-circle"}
            iconClass={"error-icon"}
            iconColor={theme.palette.error[500]}
            iconSize={"xs"}
          />
        ) : undefined}
        <IconButton
          {...buttonProps}
          className={"action"}
          icon={state.isOpen ? "chevron-up" : "chevron-down"}
          iconColor={theme.palette.gray[400]}
          iconSize={"xxs"}
          iconType={"fa-light"}
          px={0.125}
          py={0.125}
          ref={buttonRef}
          type={"button"}
          variant={"ghost"}
        />
      </StyledTextInputContainer>
      {state.isOpen && state.collection.size > 0 && !disabled ? (
        <Popover
          className={"dropdown"}
          offset={4}
          placement={"bottom start"}
          popoverRef={popoverRef}
          state={state}
          triggerRef={comboboxRef}
        >
          <StyledDropdown {...listBoxProps} ref={listBoxRef}>
            {[...state.collection].map((item) => (
              <Option item={item} key={item.key} state={state} />
            ))}
          </StyledDropdown>
        </Popover>
      ) : null}
    </OutlineContainer>
  );
};

export { ComboBoxField };
