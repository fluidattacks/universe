import type { Meta, StoryFn } from "@storybook/react";
import { expect, userEvent, within } from "@storybook/test";
import React from "react";

import type { IComboBoxProps } from ".";
import { ComboBox } from ".";
import { Form } from "../../../form";

const config: Meta = {
  component: ComboBox,
  tags: ["autodocs"],
  title: "Components/Inputs/ComboBox",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<React.FC<IComboBoxProps>> = (props): JSX.Element => {
  return (
    <Form
      defaultValues={{ combobox1: "", combobox2: "" }}
      onSubmit={mockSubmit}
      showButtons={false}
    >
      <ComboBox {...props} />
    </Form>
  );
};

const Default = Template.bind({});
Default.args = {
  label: "Select text label",
  name: "combobox1",
  items: [
    { name: "Cat", value: "cat" },
    { name: "Dog", value: "dog" },
    { name: "Cow", value: "cow" },
  ],
  placeholder: "Choose an option",
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole("combobox", { name: "combobox1" });

  await step("should render a combobox component", async (): Promise<void> => {
    await expect(input).toBeInTheDocument();
    await expect(
      canvas.getByPlaceholderText("Choose an option"),
    ).toBeInTheDocument();
    await expect(input).not.toBeDisabled();
    await expect(input).not.toBeInvalid();
  });
  await step("should allow typing", async (): Promise<void> => {
    await userEvent.type(input, "Cat");
    await expect(input).toHaveValue("Cat");
  });
};

const Large = Template.bind({});
Large.args = {
  label: "Select text label",
  name: "combobox2",
  items: [
    { name: "Cat", value: "cat" },
    { name: "Dog", value: "dog" },
    { name: "Cow", value: "cow" },
    { name: "Camaleon", value: "cameleon" },
    { name: "Elephant", value: "elephant" },
    { name: "Giraffe", value: "giraffe" },
    { name: "Lion", value: "lion" },
    { name: "Tiger", value: "tiger" },
    { name: "Bear", value: "bear" },
    { name: "Zebra", value: "zebra" },
    { name: "Kangaroo", value: "kangaroo" },
    { name: "Panda", value: "panda" },
    { name: "Koala", value: "koala" },
    { name: "Leopard", value: "leopard" },
    { name: "Cheetah", value: "cheetah" },
    { name: "Wolf", value: "wolf" },
    { name: "Fox", value: "fox" },
    { name: "Rabbit", value: "rabbit" },
    { name: "Deer", value: "deer" },
    { name: "Moose", value: "moose" },
    { name: "Bison", value: "bison" },
  ],
  placeholder: "Choose an option",
};

export { Default, Large };
export default config;
