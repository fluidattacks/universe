/**
 * Editable component props.
 * @interface IEditableProps
 * @property {string} [currentValue] - The current value of the editable field.
 * @property {string} [externalLink] - The external link associated with the editable field.
 * @property {boolean} isEditing - Indicates whether the editable field is being edited.
 * @property {string} label - The label for the editable field.
 * @property {string} name - The name of the editable field.
 * @property {string} [tooltip] - The tooltip text for the editable field.
 */
interface IEditableProps {
  currentValue?: string;
  externalLink?: string;
  isEditing: boolean;
  label: string;
  name: string;
  tooltip?: string;
}

export type { IEditableProps };
