import { isNil } from "lodash";
import { PropsWithChildren } from "react";

import type { IEditableProps } from "./types";

import { Label } from "../../label";
import { Container } from "components/container";
import { Link } from "components/link";
import { Text } from "components/typography";

const Editable = ({
  children,
  currentValue,
  externalLink,
  isEditing,
  label,
  name,
  tooltip,
}: Readonly<PropsWithChildren<IEditableProps>>): JSX.Element => {
  if (isEditing) {
    return <div>{children}</div>;
  }
  const value = isNil(currentValue) ? "" : currentValue;
  const href = String(externalLink ?? value);

  return (
    <Container>
      <Label htmlFor={name} label={label} tooltip={tooltip} weight={"bold"} />
      {href.startsWith("https://") ? (
        <Link href={href}>{value}</Link>
      ) : (
        <Text size={"md"}>{value}</Text>
      )}
    </Container>
  );
};

export type { IEditableProps };
export { Editable };
