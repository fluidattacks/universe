import type { Meta, StoryFn } from "@storybook/react";

import { TextArea } from ".";
import { Form } from "../../../form";
import type { ITextAreaProps } from "../../types";

const config: Meta = {
  component: TextArea,
  tags: ["autodocs"],
  title: "Components/Inputs/TextArea",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<ITextAreaProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ textareainput: "" }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <TextArea {...props} />
  </Form>
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Label",
  name: "textareainput",
  maxLength: 1000,
  placeholder: "Placeholder text",
  required: true,
  rows: 3,
};

const HelpLink = Template.bind({});
HelpLink.args = {
  disabled: false,
  helpText: "Help text here",
  helpLink: "https://test.com",
  label: "Label",
  name: "textareainputlink",
  placeholder: "Placeholder text",
  required: true,
  rows: 3,
};

export { Default, HelpLink };
export default config;
