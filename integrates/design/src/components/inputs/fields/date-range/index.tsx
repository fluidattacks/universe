import type { RangeValue, ValidationError } from "@react-types/shared";
import { useCallback, useRef } from "react";
import { DateValue, useDateRangePicker } from "react-aria";
import { useDateRangePickerState } from "react-stately";

import { Calendar } from "./calendar";
import { DateSelectorOutline } from "./styles";

import { OutlineContainer } from "../../outline-container";
import { DateSelector } from "../../utils/date-selector";
import { Dialog } from "../../utils/dialog";
import { Popover } from "../../utils/popover";
import type { TInputDateRangeProps } from "../date/types";
import { formatDate } from "utils/date";

const InputDateRange = ({
  disabled = false,
  error,
  label,
  name,
  required,
  tooltip,
  register,
  setValue,
  propsMax,
  propsMin,
  variant = "default",
  ...props
}: Readonly<TInputDateRangeProps>): JSX.Element => {
  const nameMax = `${name}Max`;
  const nameMin = `${name}Min`;
  const datePickerRef = useRef(null);

  const handleCalendarChange = useCallback(
    (selectedValue: Readonly<RangeValue<DateValue> | null>): void => {
      if (setValue && selectedValue) {
        setValue(
          propsMax?.name ?? nameMax,
          formatDate(String(selectedValue.end)),
        );
        setValue(
          propsMin?.name ?? nameMin,
          formatDate(String(selectedValue.start)),
        );
      }
    },
    [nameMax, nameMin, setValue, propsMax, propsMin],
  );

  const state = useDateRangePickerState({
    ...props,
    endName: propsMax?.name ?? nameMax,
    startName: propsMin?.name ?? nameMin,
    onChange(value) {
      handleCalendarChange(value);
    },
  });
  const { close } = state;

  const {
    groupProps,
    startFieldProps,
    endFieldProps,
    buttonProps,
    dialogProps,
    calendarProps,
  } = useDateRangePicker(
    { ...props, "aria-label": label ?? name },
    state,
    datePickerRef,
  );

  return (
    <OutlineContainer
      error={error}
      htmlFor={name}
      label={label}
      required={required}
      tooltip={tooltip}
    >
      <DateSelectorOutline
        $disabled={disabled}
        $show={error !== undefined}
        ref={datePickerRef}
        style={{
          flexDirection: variant === "filters" ? "column" : "row",
        }}
      >
        <DateSelector
          buttonProps={buttonProps}
          datePickerRef={datePickerRef}
          disabled={disabled}
          error={error}
          fieldProps={{
            ...startFieldProps,
            "aria-label": `${startFieldProps["aria-label"]} ${name}`,
            validate: (date): ValidationError | null =>
              endFieldProps.value && date > endFieldProps.value
                ? "Start date must be less than or equal to end date"
                : null,
          }}
          groupProps={groupProps}
          name={propsMin?.name ?? nameMin}
          register={register}
          setValue={setValue}
          value={propsMin?.value}
        />
        <DateSelector
          buttonProps={buttonProps}
          datePickerRef={datePickerRef}
          disabled={disabled}
          error={error}
          fieldProps={{
            ...endFieldProps,
            "aria-label": `${endFieldProps["aria-label"]} ${name}`,
            validate: (date): ValidationError | null =>
              startFieldProps.value && date < startFieldProps.value
                ? "End date must be greater than or equal to start date"
                : null,
          }}
          groupProps={groupProps}
          name={propsMax?.name ?? nameMax}
          register={register}
          setValue={setValue}
          value={propsMax?.value}
        />
      </DateSelectorOutline>
      {state.isOpen && !disabled ? (
        <Popover
          offset={8}
          placement={"bottom start"}
          state={state}
          triggerRef={datePickerRef}
        >
          <Dialog {...dialogProps}>
            <Calendar onClose={close} props={calendarProps} />
          </Dialog>
        </Popover>
      ) : null}
    </OutlineContainer>
  );
};

export type { TInputDateRangeProps };
export { InputDateRange };
