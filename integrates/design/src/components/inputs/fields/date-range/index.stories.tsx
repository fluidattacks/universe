import type { Meta, StoryFn } from "@storybook/react";

import { InputDateRange } from ".";
import type { TInputDateRangeProps } from ".";

const config: Meta = {
  component: InputDateRange,
  tags: ["autodocs"],
  title: "Components/Inputs/InputDateRange",
};

const Template: StoryFn<TInputDateRangeProps> = (props): JSX.Element => (
  <InputDateRange {...props} />
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Label test date range",
  name: "dateRange",
  required: false,
};

export { Default };
export default config;
