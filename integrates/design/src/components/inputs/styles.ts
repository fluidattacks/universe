import { styled } from "styled-components";

const StyledOutlineContainer = styled.div`
  ${({ theme }): string => `
    align-items: flex-start;
    display: inline-flex;
    flex-direction: column;
    gap: ${theme.spacing[0.25]};
    position: relative;
    width: 100%;

    label {
      color: ${theme.palette.gray[800]};
    }

    &:has(input.date-input) {
      div {
        padding-right: 0;
      }
    }

    &:has(input[type="file"]) {
      div {
        border-right: unset;
        padding-right: 0;

        &:hover, &:focus-within {
          border-right: unset;
        }
      }
    }

    > *:not(.error-msg) {
      font-family: ${theme.typography.type.primary};
      font-size: ${theme.typography.text.sm};
      font-style: normal;
      font-weight: ${theme.typography.weight.regular};
      line-height: ${theme.spacing[1.25]};
    }
  `}
`;

const StyledTextInputContainer = styled.div<{ $overflowX?: string }>`
  ${({ theme, $overflowX }): string => `
    align-items: center;
    display: flex;
    border-radius: ${theme.spacing[0.5]};
    border: 1px solid;
    border-color: ${theme.palette.gray[300]};
    color: ${theme.palette.gray[600]};
    gap: ${theme.spacing[0.625]};
    justify-content: space-between;
    height: 40px;
    overflow-x: ${$overflowX ?? "unset"};
    padding: ${
      $overflowX === undefined
        ? `${theme.spacing[0.625]} ${theme.spacing[0.75]}`
        : `${theme.spacing[0.25]} ${theme.spacing[0.75]}`
    };
    width: 100%;
    white-space: nowrap;

    input, textarea {
      background: inherit;
      color: ${theme.palette.gray[800]};
      cursor: inherit;
      outline: none;
      resize: none;
      width: 100%;

      &::placeholder {
        color: ${theme.palette.gray[400]};
      }

      &:focus-visible {
        outline: none;
      }

      &[type="number"] {
        appearance: textfield;
      }

      &::-webkit-outer-spin-button,
      &::-webkit-inner-spin-button {
        appearance: none;
        margin: 0;
      }

      &::-webkit-calendar-picker-indicator {
        display: none !important;
      }
    }

    input.date-input {
      display: none;
      width: 0;
    }

    &:has(textarea) {
      align-items: start;
      height: auto;
    }

    &:has(input[type="number"]) {
      padding-right: 0;
    }

    textarea {
      min-height: ${theme.spacing[6]};
    }

    span:not(.action) {
      font-size: 14px;
    }

    &.disabled {
      background: ${theme.palette.gray[100]};
      cursor: not-allowed;

      > * {
        color: ${theme.palette.gray[400]};
      }
    }

    &.error:not(.disabled) {
      border: 1px solid ${theme.palette.error[500]};
    }

    &.success:not(.disabled) {
      border: 1px solid ${theme.palette.success[500]};
    }

    &:focus-within:not(.disabled, .error) {
      background: ${theme.palette.white};
      border: 2px solid ${theme.palette.black};

      button.action-btn {
        height: 36px;
        width: 40px;
      }

      * > .success-icon {
        display: none;
      }
    }

    &:hover:not(:focus-within, .disabled, .error, .success) {
      background: ${theme.palette.white};
      border: 1px solid ${theme.palette.gray[600]};
    }
  `}
`;

export { StyledOutlineContainer, StyledTextInputContainer };
