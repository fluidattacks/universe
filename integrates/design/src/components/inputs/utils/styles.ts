import { RuleSet, css, styled } from "styled-components";

import { IActionButtonProps } from "./types";

import { BaseButtonComponent } from "components/@core";

const StyledButtonAction = styled(BaseButtonComponent)<{
  $margin: IActionButtonProps["margin"];
}>`
  align-items: center;
  background-color: inherit;
  border-color: inherit;
  display: flex;
  height: 38px;
  justify-content: center;
  width: 40px;
  margin: ${({ $margin = "0" }): RuleSet<object> => css`
    ${$margin}
  `};

  &:disabled {
    cursor: not-allowed;
  }

  &:hover:not(:disabled) {
    background-color: ${({ theme }): string => theme.palette.gray[100]};
    cursor: pointer;
  }
`;

const DialogContainer = styled.div`
  ${({ theme }): string => `
    align-items: flex-start;
    background: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.5]};
    box-shadow: ${theme.shadows.md};
    display: inline-flex;
    flex-direction: column;
    gap: ${theme.spacing[0.25]};
    height: auto;
    padding: ${theme.spacing[0.25]};
    width: 292px;
  `}
`;

const CalendarButtonAction = styled.button<{
  $disabled: boolean;
  $focus: boolean;
  $header: boolean;
}>`
  ${({ theme, $disabled, $focus, $header }): string => {
    const focusHeight = $focus ? "34px" : "40px";

    return `
      align-items: center;
      border-radius: ${
        $header ? `0 ${theme.spacing[0.5]} ${theme.spacing[0.5]} 0` : "4px"
      };
      border-left: inherit;
      display: flex;
      height: ${$header ? "38px" : focusHeight};
      justify-content: center;
      outline: none;
      width: ${$header ? "38px" : "40px"};

      &:hover {
        background-color: ${theme.palette.gray[$disabled ? 500 : 100]};
      }
    `;
  }}
`;

const InputDateBox = styled.div`
  ${({ theme }): string => `
    align-items: center;
    background: transparent;
    border-radius: ${theme.spacing[0.5]} 0 0 ${theme.spacing[0.5]};
    border-color: inherit;
    display: flex;
    height: 100%;
    justify-content: space-between;
    width: calc(100% - 40px);
  `}
`;

const TimeOutlineContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  gap: ${({ theme }): string => theme.spacing[0.5]};
  flex: 1 0 0;
`;

const TimePickerContainer = styled.div`
  ${({ theme }): string => `
    align-items: center;
    align-self: stretch;
    background-color: ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    display: flex;
    font-family: ${theme.typography.type.primary};
    gap: ${theme.spacing[0.25]};
    line-height: ${theme.spacing[1.5]};
    padding: 2px ${theme.spacing[0.25]};
  `}
`;

const DayPeriodButton = styled.button`
  ${({ theme }): string => `
    align-items: center;
    background-color: inherit;
    border: 0.5px solid ${theme.palette.gray[200]};
    border-radius: 7px;
    color: ${theme.palette.gray[800]};
    cursor: pointer;
    display: flex;
    font-weight: ${theme.typography.weight.regular};
    justify-content: center;
    gap: 10px;
    padding: 2px 10px;

    &#active {
      background-color: ${theme.palette.white};
      box-shadow: ${theme.shadows.md};
      font-weight: ${theme.typography.weight.bold};
    }
  `}
`;

export {
  StyledButtonAction,
  CalendarButtonAction,
  DayPeriodButton,
  DialogContainer,
  InputDateBox,
  TimeOutlineContainer,
  TimePickerContainer,
};
