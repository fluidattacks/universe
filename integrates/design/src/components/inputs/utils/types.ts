/* eslint-disable @typescript-eslint/no-explicit-any */
import type { Property } from "csstype";
import type {
  AriaDatePickerProps,
  AriaPopoverProps,
  DatePickerAria,
  DateValue,
} from "react-aria";
import { UseFormRegister, UseFormSetValue } from "react-hook-form";
import type {
  DateFieldState,
  DateFieldStateOptions,
  DateSegment,
  OverlayTriggerState,
  TimeFieldStateOptions,
} from "react-stately";

import {
  IBorderModifiable,
  IIconModifiable,
  IPaddingModifiable,
} from "components/@core";

/**
 * Action button component props.
 * @interface IActionButtonProps
 * @extends IIconModifiable
 * @extends IBorderModifiable
 * @extends IPaddingModifiable
 * @property {boolean} disabled - Indicates if the button is disabled.
 * @property {Function} [onClick] - The function to be called when the button is clicked.
 * @property {Property.Margin} [margin] - The button margin.
 */
interface IActionButtonProps
  extends Pick<IIconModifiable, "icon">,
    IBorderModifiable,
    IPaddingModifiable {
  disabled: boolean;
  onClick?: () => void;
  margin?: Property.Margin;
}

/**
 * Popover component props.
 * @interface IPopoverProps
 * @extends AriaPopoverProps
 * @property {string} [className] - The class name of the popover.
 * @property {boolean} [isFilter] - Indicates if the popover is a filter.
 * @property {OverlayTriggerState} state - The state of the overlay trigger.
 * @property {RefObject} [popoverRef] - The ref of the popover.
 */
interface IPopoverProps extends Omit<AriaPopoverProps, "popoverRef"> {
  className?: string;
  isFilter?: boolean;
  popoverRef?: React.MutableRefObject<HTMLDivElement | null>;
  state: OverlayTriggerState;
}

/**
 * Date selector component props.
 * @interface IDateSelectorProps
 * @extends DatePickerAria
 * @extends AriaDatePickerProps<DateValue>
 * @property {string} [error] - The error message.
 * @property {React.MutableRefObject<null>} datePickerRef - The ref of the date picker.
 * @property {boolean} [disabled] - Indicates if the date selector is disabled.
 * @property {RefCallBack} formRef - The ref of the input.
 * @property {string} name - The name of the date selector.
 * @property {Function} onBlur - The callback function on blur event.
 * @property {Function} onChange - The callback function on change event.
 * @property {boolean} [required] - Indicates if the date selector is required.
 */
interface IDateSelectorProps
  extends Pick<DatePickerAria, "buttonProps" | "fieldProps" | "groupProps">,
    Pick<AriaDatePickerProps<DateValue>, "granularity"> {
  error?: string;
  datePickerRef: React.MutableRefObject<null>;
  disabled?: boolean;
  name: string;
  required?: boolean;
  register?: UseFormRegister<any>;
  setValue?: UseFormSetValue<any>;
  value?: string;
}

/**
 * Date segment component props.
 * @interface IDateSegmentProps
 * @property {DateSegment} segment - The date segment.
 * @property {DateFieldState} state - The date field state.
 */
interface IDateSegmentProps {
  segment: DateSegment;
  state: DateFieldState;
}

/**
 * Date field component props.
 * @interface IDateFieldProps
 * @property {boolean} disabled - Indicates if the date field is disabled.
 * @property {boolean} error - Indicates if the date field has an error.
 */
interface IDateFieldProps {
  disabled: boolean;
  error: boolean;
}

type TTimeFieldProps = Omit<TimeFieldStateOptions, "createCalendar" | "locale">;
type TDateFieldProps = IDateFieldProps & {
  props: Omit<DateFieldStateOptions, "createCalendar" | "locale">;
};

export type {
  IActionButtonProps,
  IPopoverProps,
  IDateSelectorProps,
  IDateSegmentProps,
  TDateFieldProps,
  TTimeFieldProps,
};
