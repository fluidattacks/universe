import { PropsWithChildren, useRef } from "react";
import { useDialog } from "react-aria";
import type { AriaDialogProps } from "react-aria";

import { DialogContainer } from "../styles";

const Dialog = ({
  children,
  ...props
}: Readonly<PropsWithChildren<AriaDialogProps>>): JSX.Element => {
  const dialogRef = useRef(null);
  const { dialogProps } = useDialog(props, dialogRef);

  return (
    <DialogContainer {...dialogProps} ref={dialogRef}>
      {children}
    </DialogContainer>
  );
};

export { Dialog };
