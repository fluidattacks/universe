import { styled } from "styled-components";

import { TTimelineContainerSize } from "./types";

const TimelineStyledCard = styled.div<{ $size: TTimelineContainerSize }>`
  ${({ theme, $size }): string => `
    align-items: center;
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    display: flex;
    flex-direction: column;
    gap: ${theme.spacing[0.5]};
    min-height: 180px;
    padding: ${theme.spacing[1.5]};
    width: 510px;

    p {
      font-style: normal;
      line-height: ${theme.spacing[1.25]};
      text-align: ${$size === "long" ? "" : "center"};
    }

    &:hover {
      border: 1px solid ${theme.palette.gray[600]};
    }
  `}
`;

const TimelineContainer = styled.div<{ $size: TTimelineContainerSize }>`
  ${({ theme, $size }): string => `
    display: ${$size === "long" ? "flex" : "block"};
    flex-direction: column;
    gap: ${theme.spacing[1.5]};
    position: relative;
    max-width: 1100px;
    margin: 0 auto 0 auto;

    div.timeline-card {
      width: ${$size === "long" ? "510px" : "auto"};
      margin-left: ${$size === "long" ? "0px" : "30px"};
      margin-bottom: ${$size === "long" ? "0px" : theme.spacing[0.5]};
    }

    &::before {
      background-color: ${theme.palette.gray[200]};
      border-radius: 5px;
      height: 100%;
      width: 4px;
      transform: translate(-50%, 0);
    }

    > * {
      &:nth-child(even) {
        align-self: end;
      }

      &:nth-child(odd) {
        align-self: start;
      }

      &::before {
        position: absolute;
        background-color: ${theme.palette.primary[500]};
        border-radius: 50%;
        height: 14px;
        width: 14px;
        z-index: 1;
      }
    }

    &::before,
    > *::before {
      content: "";
      left: ${$size === "long" ? "50%" : "0px"};
      position: absolute;
      transform: translateX(-50%);
    }
  `}
`;

export { TimelineStyledCard, TimelineContainer };
