import { useTheme } from "styled-components";

import { TimelineStyledCard } from "../styles";
import { ITimelineCardProps } from "../types";
import { Text } from "components/typography";

const TimeLineCard = ({
  date,
  description,
  title,
  size = "long",
}: Readonly<ITimelineCardProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <TimelineStyledCard $size={size} className={"timeline-card"}>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        lineSpacing={1.75}
        size={"xl"}
      >
        {date}
      </Text>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        lineSpacing={1.5}
        mb={0.5}
        size={"md"}
        textAlign={"center"}
      >
        {title}
      </Text>
      <div>
        {description.map((text, index): JSX.Element => {
          const keyProp = `${text}#${index}`;

          return (
            <Text
              key={keyProp}
              size={"sm"}
              textAlign={"center"}
              wordBreak={"break-word"}
            >
              {text}
            </Text>
          );
        })}
      </div>
    </TimelineStyledCard>
  );
};

export { TimeLineCard };
