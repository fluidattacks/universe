import type { FC } from "react";
import { useCallback } from "react";

import {
  CarouselContainer,
  CarouselSlide,
  CarouselWrapper,
  NavButton,
  NavContainer,
} from "./styles";
import type { ICarouselProps, ISlide } from "./types";

import { useCarousel } from "hooks";

const Carousel: FC<ICarouselProps> = ({
  slides,
  interval = 300,
}): JSX.Element => {
  const { cycle, setCycle, setProgress } = useCarousel(interval, slides.length);

  const handleNavClick = useCallback(
    (index: number): (() => void) =>
      // eslint-disable-next-line functional/functional-parameters
      (): void => {
        setCycle(index);
        setProgress(0);
      },
    [setCycle, setProgress],
  );

  return (
    <CarouselContainer aria-roledescription={"carousel"}>
      <CarouselWrapper $currentIndex={cycle}>
        {slides.map((slide, index): JSX.Element => {
          const key = `slide-${index}`;

          return (
            <CarouselSlide
              aria-label={key}
              aria-roledescription={"slide"}
              key={key}
            >
              {slide.content}
            </CarouselSlide>
          );
        })}
      </CarouselWrapper>
      <NavContainer>
        {slides.map((_, index): JSX.Element => {
          const key = `nav-button-${index}`;

          return (
            <NavButton
              $isActive={index === cycle}
              aria-label={`Go to slide ${index + 1}`}
              data-testid={index === cycle ? "active" : "inactive"}
              key={key}
              onClick={handleNavClick(index)}
            />
          );
        })}
      </NavContainer>
    </CarouselContainer>
  );
};

export type { ISlide };
export { Carousel };
