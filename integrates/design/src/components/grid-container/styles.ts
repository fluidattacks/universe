import { styled } from "styled-components";

import type { IGridContainerProps } from "./types";

import { BaseComponent } from "components/@core";

interface IStyledGridProps {
  $xl?: IGridContainerProps["xl"];
  $lg?: IGridContainerProps["lg"];
  $md?: IGridContainerProps["md"];
  $sm?: IGridContainerProps["sm"];
  $gap?: IGridContainerProps["gap"];
}

const GridContainer = styled(BaseComponent)<IStyledGridProps>`
  display: grid;
  gap: ${({ $gap = 1.25 }): string => `${$gap}rem`};
  grid-template-columns: ${({ $sm }): string => `repeat(${$sm}, 1fr)`};

  ${({ theme, $xl, $lg, $md }): string => `
    @media (width >= ${theme.breakpoints.sm}) {
      grid-template-columns: repeat(${$md}, 1fr);
    }

    @media (width >= ${theme.breakpoints.md}) {
      grid-template-columns: repeat(${$lg}, 1fr);
    }

    @media (width >= ${theme.breakpoints.lg}) {
      grid-template-columns: repeat(${$xl}, 1fr);
    }
  `}
`;

export { GridContainer };
