import type { TModifiable } from "components/@core";

/**
 * Grid container component props.
 * @interface IGridContainerProps
 * @extends TModifiable
 * @property { number } xl - Grid container xl breakpoint.
 * @property { number } lg - Grid container lg breakpoint.
 * @property { number } md - Grid container md breakpoint.
 * @property { number } sm - Grid container sm breakpoint.
 */
interface IGridContainerProps extends TModifiable {
  xl: number;
  lg: number;
  md: number;
  sm: number;
}

export type { IGridContainerProps };
