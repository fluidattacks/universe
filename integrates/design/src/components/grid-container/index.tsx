import { PropsWithChildren, Ref, forwardRef } from "react";

import { GridContainer as StyledGridContainer } from "./styles";
import type { IGridContainerProps } from "./types";

const GridContainer = forwardRef(function GridContainer(
  {
    children,
    md,
    sm,
    lg,
    xl,
    gap,
    ...props
  }: Readonly<PropsWithChildren<IGridContainerProps>>,
  ref: Ref<HTMLDivElement>,
): JSX.Element {
  return (
    <StyledGridContainer
      $gap={gap}
      $lg={lg}
      $md={md}
      $sm={sm}
      $xl={xl}
      ref={ref}
      {...props}
    >
      {children}
    </StyledGridContainer>
  );
});
export { GridContainer };
