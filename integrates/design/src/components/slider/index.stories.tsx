import type { Meta, StoryFn } from "@storybook/react";

import type { TSliderProps } from "./types";

import { Slider } from ".";
import { Form } from "../form";

const config: Meta = {
  component: Slider,
  tags: ["autodocs"],
  title: "Components/Slider",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<TSliderProps> = (props): JSX.Element => {
  return (
    <Form
      defaultValues={{ sliderInput: 0, sliderInputBi: 0 }}
      onSubmit={mockSubmit}
      showButtons={false}
    >
      <Slider {...props} />
    </Form>
  );
};

const Default = Template.bind({});
Default.args = {
  defaultValue: 0,
  minValue: 0,
  maxValue: 1000,
  label: "Priority score",
  name: "sliderInput",
};

const Bidirectional = Template.bind({});
Bidirectional.args = {
  defaultValue: 0,
  minValue: -1000,
  maxValue: 1000,
  name: "sliderInputBi",
};

export { Default, Bidirectional };
export default config;
