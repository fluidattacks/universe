import { useRef } from "react";
import { VisuallyHidden, useSliderThumb } from "react-aria";

import { ISliderThumbProps } from "../types";

const Thumb = ({
  state,
  trackRef,
  index,
  name,
}: Readonly<ISliderThumbProps>): JSX.Element => {
  const inputRef = useRef(null);
  const { thumbProps, inputProps, isDragging } = useSliderThumb(
    { index, trackRef, inputRef },
    state,
  );

  return (
    <div {...thumbProps} className={`thumb ${isDragging ? "dragging" : ""}`}>
      <VisuallyHidden>
        <input {...inputProps} aria-label={name} ref={inputRef} />
      </VisuallyHidden>
    </div>
  );
};

export { Thumb };
