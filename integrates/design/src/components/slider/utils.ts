const normalizeValue = (min: number, max: number, value: number): number => {
  return Math.round(((value - min) / (max - min)) * 100);
};

export { normalizeValue };
