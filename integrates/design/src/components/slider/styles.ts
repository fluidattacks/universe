import { DefaultTheme, styled } from "styled-components";

const bidirectionalBg = (theme: DefaultTheme, $value: number): string =>
  $value >= 50
    ? `
      ${theme.palette.gray[200]} 50%,
      ${theme.palette.primary[500]} 50.05%,
      ${theme.palette.primary[500]} ${$value}%,
      ${theme.palette.gray[200]} ${$value}%
    `
    : `
      ${theme.palette.gray[200]} ${$value}%,
      ${theme.palette.primary[500]} ${$value}%,
      ${theme.palette.primary[500]} 50%,
      ${theme.palette.gray[200]} 50.05%,
      ${theme.palette.gray[200]} 100%
    `;

const StyledSlider = styled.div<{ $min: number; $value: number }>`
  ${({ theme, $min, $value }): string => `
    align-items: center;
    align-self: stretch;
    display: flex;
    flex-direction: column;
    justify-content: end;
    gap: ${theme.spacing[0.75]};
    width: 100%;

    .track {
      background: linear-gradient(
        to right,
        ${
          $min >= 0
            ? `
              ${theme.palette.primary[500]} ${$value}%,
              ${theme.palette.gray[200]} ${$value}%
            `
            : bidirectionalBg(theme, $value)
        }
      );
      border-radius: ${theme.spacing[0.125]};
      height: ${theme.spacing[0.25]};
      width: 100%;
    }

    .track.disabled {
      opacity: 0.4;
    }

    .thumb {
      background: ${theme.palette.white};
      border-radius: ${theme.spacing[1.25]};
      box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.30);
      top: 50%;
      height: 20px;
      width: 20px;
    }

    .thumb.dragging {
      border: 1px solid ${theme.palette.primary[500]};
    }
  `}
`;

const LimitValues = styled.div`
  ${({ theme }): string => `
    align-items: center;
    display: flex;
    justify-content: space-between;
    height: ${theme.spacing[0.625]};
    width: 100%;

    p {
      color: ${theme.palette.gray[400]};
      font-family: ${theme.typography.type.primary};
      font-size: ${theme.spacing[0.5]};
    }
  `}
`;

const Output = styled.output`
  ${({ theme }): string => `
    align-items: flex-end;
    border-radius: ${theme.spacing[0.5]};
    border: 1px solid ${theme.palette.gray[300]};
    background: ${theme.palette.white};
    display: flex;
    gap: ${theme.spacing[0.625]};
    padding: ${theme.spacing[0.625]} ${theme.spacing[0.75]};
    width: 59px;
  `}
`;

export { StyledSlider, LimitValues, Output };
