import type { Meta, StoryFn } from "@storybook/react";
import { expect, within } from "@storybook/test";

import type { INotificationSignProps } from "./types";

import { NotificationSign } from ".";

const config: Meta = {
  argTypes: {
    id: {
      control: {
        disable: true,
      },
    },
  },
  component: NotificationSign,
  tags: ["autodocs"],
  title: "Components/NotificationSign",
};

const Template: StoryFn<Readonly<INotificationSignProps>> = (
  props,
): JSX.Element => <NotificationSign {...props} />;

const Default = Template.bind({});
Default.args = {
  numberIndicator: 7,
  show: true,
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render all fixed tabs", async (): Promise<void> => {
    await expect(canvas.getByTestId("notification-sign")).toBeInTheDocument();
    await expect(canvas.getByText("7")).toBeInTheDocument();
  });
};

export { Default };
export default config;
