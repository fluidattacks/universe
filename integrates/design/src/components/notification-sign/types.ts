import { IPositionModifiable } from "components/@core";

type TVariant = "error" | "warning";

/**
 * Notification sign component props.
 * @interface INotificationSignProps
 * @extends IPositionModifiable
 * @property {number} [numberIndicator] - The number of notifications.
 * @property {boolean} [show] - The visibility of notification sign.
 * @property {TVariant} [variant] - The notification custom variant.
 */
interface INotificationSignProps
  extends Pick<IPositionModifiable, "left" | "top"> {
  numberIndicator?: number;
  show?: boolean;
  variant?: TVariant;
}

export type { INotificationSignProps };
