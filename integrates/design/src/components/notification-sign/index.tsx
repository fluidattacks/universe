import type { Ref } from "react";
import { forwardRef } from "react";

import { NotificationSignButton, NumberIndicator } from "./styles";
import type { INotificationSignProps } from "./types";

const NotificationSign = forwardRef(function NotificationSign(
  {
    numberIndicator,
    show,
    variant = "error",
    ...props
  }: Readonly<INotificationSignProps>,
  ref: Ref<HTMLDivElement>,
): JSX.Element {
  return (
    <NotificationSignButton
      $variant={variant}
      className={show ? "flex" : "hidden"}
      data-testid={"notification-sign"}
      ref={ref}
      {...props}
    >
      {numberIndicator && (
        <NumberIndicator>
          {numberIndicator <= 9 ? numberIndicator : 9}
        </NumberIndicator>
      )}
    </NotificationSignButton>
  );
});

export { NotificationSign };
