import type { Meta, StoryFn } from "@storybook/react";
import { expect, within } from "@storybook/test";
import React from "react";

import type { IContainerProps } from "./types";

import { Container } from ".";
import { theme } from "../colors";

const config: Meta = {
  component: Container,
  tags: ["autodocs"],
  title: "Components/Container",
};

const Template: StoryFn<React.PropsWithChildren<IContainerProps>> = (
  props,
): JSX.Element => <Container {...props}> {"Content example"} </Container>;

const Default = Template.bind({});
Default.args = {
  alignItems: "center",
  border: "5px solid",
  borderColor: theme.palette.primary[500],
  borderRadius: theme.spacing[0.5],
  center: true,
  display: "flex",
  fontSize: theme.typography.text.xl,
  fontWeight: theme.typography.weight.semibold,
  height: "350px",
  justify: "center",
  width: "350px",
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render a Container component", async (): Promise<void> => {
    await expect(canvas.getByText("Content example")).toBeInTheDocument();
  });
};

const FlexBox: StoryFn = (): JSX.Element => (
  <Container
    display={"flex"}
    flexDirection={"row"}
    justify={"center"}
    wrap={"wrap"}
  >
    {[...Array(15).keys()].map(
      (el): JSX.Element => (
        <Container
          key={el}
          mb={0.25}
          mt={0.25}
          pl={0.25}
          pr={0.25}
          width={"25%"}
        >{`Content ${el}`}</Container>
      ),
    )}
  </Container>
);

export { Default, FlexBox };
export default config;
