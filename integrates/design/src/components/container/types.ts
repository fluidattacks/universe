import type { HTMLAttributes, ReactHTML } from "react";

import type { TModifiable } from "components/@core";

type THtmlTag = keyof ReactHTML;

/**
 * Container component props.
 * @interface IContainerProps
 * @extends TModifiable
 * @extends HTMLAttributes<HTMLDivElement>
 * @property {THtmlTag} [as] - The react html tag to represent.
 * @property {boolean} [center] - Whether to center the content.
 * @property {Function} [onHover] - Function handler for hover event.
 * @property {Function} [onLeave] - Function handler for leave event.
 */
interface IContainerProps extends TModifiable, HTMLAttributes<HTMLDivElement> {
  as?: THtmlTag;
  center?: boolean;
  onHover?: () => void;
  onLeave?: () => void;
  styleMd?: string;
  styleSm?: string;
}

export type { IContainerProps };
