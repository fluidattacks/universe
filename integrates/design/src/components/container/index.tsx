import type { PropsWithChildren, Ref } from "react";
import { forwardRef } from "react";

import { StyledContainer } from "./styles";
import type { IContainerProps } from "./types";

const Container = forwardRef(function Container(
  {
    as = "div",
    center,
    children,
    id,
    onHover,
    onLeave,
    styleMd,
    styleSm,
    ...props
  }: Readonly<PropsWithChildren<IContainerProps>>,
  ref: Ref<HTMLDivElement>,
): JSX.Element {
  return (
    <StyledContainer
      $center={center}
      $styleMd={styleMd}
      $styleSm={styleSm}
      as={as}
      id={id}
      onMouseLeave={onLeave}
      onMouseOver={onHover}
      ref={ref}
      {...props}
    >
      {children}
    </StyledContainer>
  );
});

export { Container };
