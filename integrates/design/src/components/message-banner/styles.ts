import { styled } from "styled-components";

import { theme } from "components/colors";

const Button = styled.button`
  align-items: center;
  background-color: transparent;
  border: unset;
  cursor: pointer;
  display: flex;
  justify-content: center;
  gap: 5px;
  padding: 0;

  &:hover p {
    color: ${theme.palette.primary[200]};
  }

  &:hover span {
    color: ${theme.palette.primary[200]};
  }
`;

export { Button };
