import type { ButtonHTMLAttributes } from "react";

import type {
  IBorderModifiable,
  IDisplayModifiable,
  IIconModifiable,
  IPaddingModifiable,
} from "components/@core/types";
import type { TVariant } from "components/button/styles";
import type { TPlace } from "components/tooltip/types";

/**
 * IconButton component props.
 * @interface IIconButtonProps
 * @extends ButtonHTMLAttributes<HTMLButtonElement>
 * @extends IBorderModifiable
 * @extends IDisplayModifiable
 * @extends IIconModifiable
 * @extends IPaddingModifiable
 * @property {string} [tooltip] - Tooltip text.
 * @property {string} [tooltipPlace] - Tooltip placement.
 * @property {TVariant} [variant] - Variant
 */
interface IIconButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    Pick<IDisplayModifiable, "height" | "justify" | "width">,
    Pick<IPaddingModifiable, "px" | "py">,
    IBorderModifiable,
    IIconModifiable {
  tooltip?: string;
  tooltipPlace?: TPlace;
  variant?: TVariant;
}

export type { IIconButtonProps };
