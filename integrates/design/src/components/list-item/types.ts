import type { HTMLAttributes, HTMLProps } from "react";

import { IIconModifiable } from "components/@core";
import { IIconProps } from "components/icon/types";

/**
 * Label component props.
 * @interface ILabelProps
 * @property { string } color - The color for the label.
 * @property { string } [href] - The URL for the label.
 */
interface ILabelProps {
  color: string;
  href?: string;
}

/**
 * List item component props.
 * @interface IListItemProps
 * @extends IIconModifiable
 * @extends HTMLAttributes<HTMLLIElement>
 * @property { string } [header] - The header text for the list item.
 * @property { string } [href] - The URL for the list item.
 * @property { boolean } [selected] - Whether the list item is selected.
 * @property { string } [value] - The value for the list item.
 * @property { IIconModifiable["icon"] } [rightIcon] - The icon for the right side of the list item.
 * @property { IIconProps["onClick"] } [onClickIcon] - The function to call when the icon on the left side is clicked.
 */
interface IListItemProps
  extends Partial<Pick<IIconModifiable, "icon" | "iconType">>,
    HTMLAttributes<HTMLLIElement>,
    HTMLProps<HTMLLIElement> {
  href?: string;
  selected?: boolean;
  value?: string;
  rightIcon?: IIconModifiable["icon"];
  onClickIcon?: IIconProps["onClick"];
}

export type { IListItemProps, ILabelProps };
