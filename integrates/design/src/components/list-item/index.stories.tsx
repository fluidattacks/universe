import type { Meta, StoryFn } from "@storybook/react";

import type { IListItemProps } from "./types";

import { ListItem, ListItemsWrapper } from ".";
import { Checkbox } from "../checkbox";
import { RadioButton } from "../radio-button";

const config: Meta = {
  component: ListItem,
  tags: ["autodocs"],
  title: "Components/ListItem",
};

const defaultValue = "List item";
const args = [{ checked: true }, { checked: false }, { checked: false }];

const ListItemTemplate: StoryFn<Readonly<IListItemProps>> = (
  props,
): JSX.Element => (
  <ListItemsWrapper>
    <ListItem {...props}>{defaultValue}</ListItem>
  </ListItemsWrapper>
);

const CheckboxTemplate: StoryFn<Readonly<IListItemProps>> = (): JSX.Element => (
  <ListItemsWrapper>
    {args.map((arg, index): JSX.Element => {
      const key = `li-checkbox${index}`;

      return (
        <ListItem disabled={index === 2} key={key} value={key}>
          <Checkbox
            defaultChecked={arg.checked}
            name={key}
            value={index === 2 ? "Item disabled" : defaultValue}
          />
        </ListItem>
      );
    })}
  </ListItemsWrapper>
);

const RadioBtnTemplate: StoryFn<Readonly<IListItemProps>> = (): JSX.Element => (
  <ListItemsWrapper>
    {args.map((arg, index): JSX.Element => {
      const key = `li-radioButton${index}`;

      return (
        <ListItem disabled={index === 2} key={key} value={key}>
          <RadioButton
            defaultChecked={arg.checked}
            label={index === 2 ? "Item disabled" : defaultValue}
            name={key}
            value={defaultValue}
          />
        </ListItem>
      );
    })}
  </ListItemsWrapper>
);

const Default = ListItemTemplate.bind({});
Default.args = {
  disabled: false,
  icon: "gear",
};

const WithCheckbox = CheckboxTemplate.bind({});
WithCheckbox.args = {
  disabled: false,
  icon: "gear",
};

const WithRadioButton = RadioBtnTemplate.bind({});
WithRadioButton.args = {
  disabled: false,
  icon: "gear",
};

export default config;
export { Default, WithCheckbox, WithRadioButton };
