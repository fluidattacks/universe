import { StyledGap } from "./styles";

import { IGapProps } from "../../types";

const Gap = ({ children, disp, mh, mv }: Readonly<IGapProps>): JSX.Element => (
  <StyledGap $disp={disp} $mh={mh} $mv={mv}>
    {children}
  </StyledGap>
);

export { Gap };
