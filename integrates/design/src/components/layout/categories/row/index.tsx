import { StyledRow } from "./styles";

import { IRowProps } from "../../types";

const Row = ({
  align,
  children,
  cols,
  colsPadding,
  justify,
}: Readonly<IRowProps>): JSX.Element => (
  <StyledRow
    $align={align}
    $cols={cols}
    $colsPadding={colsPadding}
    $justify={justify}
  >
    {children}
  </StyledRow>
);

export { Row };
