import { styled } from "styled-components";

const InitialsCircle = styled.div`
  ${({ theme }): string => `
    align-items: center;
    background-color: ${theme.palette.gray[200]};
    border-radius: 100%;
    color: ${theme.palette.gray[800]};
    display: flex;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.xs};
    font-weight: 500;
    line-height: 18px;
    justify-content: center;
    text-align: center;
    width: ${theme.spacing[1.25]};
    height: ${theme.spacing[1.25]};
  `}
`;

const ChildrenContainer = styled.div`
  ul,
  ol {
    padding-top: 6px;
    padding-bottom: 6px;
    background-color: #fff;
    position: absolute;
    top: 40px;
    right: 1px;
    z-index: 1;
  }

  .no-hover,
  .user-info {
    text-decoration: none;
    display: flex;
    justify-content: space-between;
    height: auto;
  }

  .user-info,
  .commit-info {
    padding-left: 16px;
    padding-right: 16px;
  }

  .user-info {
    padding-top: 10px;
    padding-bottom: 1px;
  }

  .commit-info {
    padding-top: 1px;
    padding-bottom: 10px;
  }

  li {
    list-style-type: none;
  }

  span {
    white-space: nowrap;
  }

  a {
    color: ${({ theme }): string => theme.palette.gray[800]};
    text-decoration: none;
    padding: unset;
  }

  a:hover {
    text-decoration: none;
    color: ${({ theme }): string => theme.palette.gray[800]};
  }

  #menu-profile-container ul {
    min-width: unset;
    width: auto;
    z-index: 10;
  }

  #menu-profile-container li div {
    min-width: unset;
  }
`;

export { ChildrenContainer, InitialsCircle };
