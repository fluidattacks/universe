import { IMarginModifiable } from "components/@core";

/**
 * Premium feature component props.
 * @interface IPremiumFeatureProps
 * @property { string } [margin] - The margin for the premium feature.
 * @property { Function } [onClick] - Event handler for the premium feature click.
 * @property { string } [text] - Text for the premium feature.
 */
interface IPremiumFeatureProps {
  margin?: IMarginModifiable["mr"];
  onClick?: () => void;
  text?: string;
}

export type { IPremiumFeatureProps };
