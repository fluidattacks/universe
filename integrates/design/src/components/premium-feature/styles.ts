import { styled } from "styled-components";

const IconsContainer = styled.div`
  ${({ theme }): string => `
    height: ${theme.spacing[1]};
    width: ${theme.spacing[1]};
    position: relative;
    flex-shrink: 0;

    & .ellipse-1,
    & .ellipse-2,
    & .icon {
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      position: absolute;
    }

    & .ellipse-1 {
      color: ${theme.palette.warning[200]};
      position: absolute;
    }

    & .ellipse-2 {
      transform: scale(0.75) translate(-67%, -66%);
      color: ${theme.palette.warning[400]};
    }

    & .icon {
      color: ${theme.palette.warning[600]};
      font-size: ${theme.spacing[0.5]};
    }
  `}
`;

export { IconsContainer };
