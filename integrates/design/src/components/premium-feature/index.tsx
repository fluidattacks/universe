import { useTheme } from "styled-components";

import { IconsContainer } from "./styles";
import type { IPremiumFeatureProps } from "./types";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const PremiumFeature = ({
  margin,
  onClick,
  text,
}: Readonly<IPremiumFeatureProps>): JSX.Element => {
  const theme = useTheme();

  const icon = (
    <IconsContainer>
      <Icon icon={"circle"} iconClass={"ellipse-1"} iconSize={"xs"} />
      <Icon icon={"circle"} iconClass={"ellipse-2"} iconSize={"xs"} />
      <Icon icon={"crown"} iconClass={"icon"} iconSize={"xs"} />
    </IconsContainer>
  );

  if (text !== undefined) {
    return (
      <Container
        cursor={"pointer"}
        display={"inline-block"}
        mr={margin}
        onClick={onClick}
      >
        <Container
          alignItems={"center"}
          bgColor={theme.palette.warning[50]}
          borderRadius={"4px"}
          display={"flex"}
          padding={[0.25, 0.25, 0.25, 0.25]}
          width={"fit-content"}
        >
          {icon}
          <Text
            color={theme.palette.warning[500]}
            display={"inline"}
            ml={0.25}
            size={"xs"}
          >
            {text}
          </Text>
        </Container>
      </Container>
    );
  }

  return (
    <Container display={"inline-block"} mr={margin} onClick={onClick}>
      {icon}
    </Container>
  );
};

export { PremiumFeature };
