import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { IPremiumFeatureProps } from "./types";

import { PremiumFeature } from ".";
import { Button } from "../button";

const config: Meta = {
  component: PremiumFeature,
  tags: ["autodocs"],
  title: "Components/PremiumFeature",
};

const Template: StoryFn<React.PropsWithChildren<IPremiumFeatureProps>> = (
  props,
): JSX.Element => <PremiumFeature {...props} />;

const Default = Template.bind({});

const Badge: StoryFn = (): JSX.Element => <PremiumFeature text={"Upgrade"} />;

const PremiumButton: StoryFn = (): JSX.Element => (
  <Button variant={"primary"}>
    <PremiumFeature margin={0.25} />
    {"Talk to a hacker"}
  </Button>
);

export { Default, Badge, PremiumButton };
export default config;
