import { StyledFixedTab, StyledFixedTabs } from "../styles";
import { IFixedTabsProps } from "../types";
import { Heading } from "components/typography";

const FixedTabs = ({ items }: Readonly<IFixedTabsProps>): JSX.Element => {
  return (
    <StyledFixedTabs>
      {items.map(
        ({ isActive, label, onClick }, index): JSX.Element => (
          <StyledFixedTab
            className={isActive ? "active" : ""}
            key={`${label}-${index}`}
            onClick={onClick}
            type={"button"}
          >
            <Heading
              color={"inherit"}
              fontWeight={"semibold"}
              size={"sm"}
              sizeSm={"xs"}
            >
              {label}
            </Heading>
          </StyledFixedTab>
        ),
      )}
    </StyledFixedTabs>
  );
};

export { FixedTabs };
