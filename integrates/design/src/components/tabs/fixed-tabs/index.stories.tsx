import type { Meta, StoryFn } from "@storybook/react";
import { expect, userEvent, within } from "@storybook/test";
import { useCallback, useState } from "react";

import { FixedTabs } from ".";
import type { IFixedTabProps, IFixedTabsProps } from "../types";

const config: Meta = {
  component: FixedTabs,
  tags: ["autodocs"],
  title: "Components/FixedTabs",
};

const Template: StoryFn<IFixedTabsProps> = ({
  items,
}: Readonly<IFixedTabsProps>): JSX.Element => {
  const [selected, setSelected] = useState("Label 1");
  const mockClick = useCallback(
    (label: string): VoidFunction =>
      (): void => {
        setSelected(label);
      },
    [],
  );

  return (
    <FixedTabs
      items={items.map(
        ({ label }): IFixedTabProps => ({
          label,
          onClick: mockClick(String(label)),
          isActive: selected === label,
        }),
      )}
    />
  );
};

const Default = Template.bind({});
Default.args = {
  items: [
    { label: "Label 1" },
    { label: "Label 2" },
    { label: "Label 3" },
    { label: "Label 4" },
    { label: "Label 5" },
  ],
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const button1 = canvas.getByRole("button", { name: "Label 1" });
  const button2 = canvas.getByRole("button", { name: "Label 2" });
  const button3 = canvas.getByRole("button", { name: "Label 3" });
  const button4 = canvas.getByRole("button", { name: "Label 4" });
  const button5 = canvas.getByRole("button", { name: "Label 5" });

  await step("should render all fixed tabs", async (): Promise<void> => {
    await expect(button1).toBeInTheDocument();
    await expect(button2).toBeInTheDocument();
    await expect(button3).toBeInTheDocument();
    await expect(button4).toBeInTheDocument();
    await expect(button5).toBeInTheDocument();
  });

  await step("should change active tab on click", async (): Promise<void> => {
    await expect(button1).toHaveClass("active");
    await expect(button2).not.toHaveClass("active");
    await userEvent.click(button2);
    await expect(button1).not.toHaveClass("active");
    await expect(button2).toHaveClass("active");
    await userEvent.click(button3);
    await expect(button2).not.toHaveClass("active");
    await expect(button3).toHaveClass("active");
    await userEvent.click(button1);
  });
};

export { Default };
export default config;
