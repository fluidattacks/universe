import { useTheme } from "styled-components";

import { Tab } from "./tab";
import type { ITabProps, ITabsProps } from "./types";

import { Container } from "components/container";

const Tabs = ({
  items,
  borders = true,
  box = false,
  variant = "primary",
}: Readonly<ITabsProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      alignItems={"center"}
      bgColor={theme.palette.white}
      border={box ? "1px solid" : undefined}
      borderBottom={
        borders ? `1px solid ${theme.palette.gray[300]}` : undefined
      }
      borderColor={box ? theme.palette.gray[200] : undefined}
      borderRadius={box ? theme.spacing[0.25] : undefined}
      display={"inline-flex"}
      gap={0.25}
      id={"tablist"}
      padding={box ? [0, 0, 0, 0] : undefined}
      pl={borders ? 1.25 : undefined}
      pr={borders ? 1.25 : undefined}
      role={"tablist"}
      width={box ? "max-content" : "100%"}
    >
      {items.map(
        ({
          end,
          id,
          isActive,
          link,
          label,
          notificationSign,
          onClick,
          tag,
          tooltip,
        }): JSX.Element => (
          <Tab
            end={end}
            id={id}
            isActive={isActive}
            key={id}
            label={label}
            link={link}
            notificationSign={notificationSign}
            onClick={onClick}
            tag={tag}
            tooltip={tooltip}
            variant={variant}
          />
        ),
      )}
    </Container>
  );
};

export { Tabs };
export type { ITabProps };
