import { INotificationSignProps } from "components/notification-sign/types";

type TVariant = "primary" | "secondary";

/**
 * Individual tab props.
 * @property {boolean} [isActive] - Set the current tab active.
 * @property {Function} [onClick] - The handler event on tab click.
 * @property {string | JSX.Element} label - The tab title.
 */
interface IFixedTabProps {
  isActive?: boolean;
  onClick?: (event: Readonly<React.MouseEvent>) => void;
  label: string | JSX.Element;
}

/**
 * Fixed tabs component props.
 * ** Note: A maximum of 5 tabs is allowed. **
 * @property {ITabProps[]} items - The list of tabs to display.
 */
interface IFixedTabsProps {
  items: IFixedTabProps[];
}

/**
 * Tab component props.
 * @interface ITabProps
 * @extends IFixedTabProps
 * @property {boolean} [end] - Wheter to changes the matching logic for the active and pending states to only match to the "end" of the link prop.
 * @property {string} id - The id related to the tab.
 * @property {string} link - The link to redirect on tab click.
 * @property {INotificationSignProps} [notificationSign] - Notification tab details.
 * @property {string} [tooltip] - The tooltip text to add.
 * @property {TVariant} [variant] - The tab variant.
 * @property {number | string} [tag] - The content of the tag to display on tab.
 */
interface ITabProps extends IFixedTabProps {
  end?: boolean;
  id: string;
  link: string;
  notificationSign?: INotificationSignProps;
  tooltip?: string;
  variant?: TVariant;
  tag?: number | string;
}

/**
 * Tabs component props.
 * @interface ITabsProps
 * @property {ITabProps[]} items - The lists of tabs to display.
 * @property {boolean} [borders] - Whether to add borders to set of tabs.
 * @property {boolean} [box] - Whether to add box container to set of tabs.
 * @property {"primary" | "secondary"} [variant] - The variant of the tabs items.
 */
interface ITabsProps {
  items: ITabProps[];
  borders?: boolean;
  box?: boolean;
  variant?: ITabProps["variant"];
}

export type {
  IFixedTabsProps,
  IFixedTabProps,
  ITabProps,
  ITabsProps,
  TVariant,
};
