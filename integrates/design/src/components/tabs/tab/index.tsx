import { NavLink, useLocation } from "react-router-dom";

import { TabContainer } from "../styles";
import type { ITabProps } from "../types";
import { NotificationSign } from "components/notification-sign";
import { Tag } from "components/tag";
import { Tooltip } from "components/tooltip";

const Tab = ({
  end = false,
  label,
  id,
  isActive = false,
  link,
  notificationSign,
  onClick,
  tooltip = "",
  variant = "primary",
  tag,
}: Readonly<ITabProps>): JSX.Element | null => {
  const { pathname } = useLocation();
  const borderButton =
    isActive ||
    (variant === "primary" ? pathname.includes(link) : pathname.endsWith(link));

  return (
    <Tooltip display={"flex"} id={`${id}-tooltip`} tip={tooltip}>
      <TabContainer $borderButton={borderButton} $variant={variant}>
        <NavLink end={end} id={id} onClick={onClick} to={link}>
          {label}
          <NotificationSign
            left={notificationSign?.left}
            numberIndicator={notificationSign?.numberIndicator}
            show={notificationSign?.show}
            top={notificationSign?.top}
            variant={notificationSign?.variant}
          />
        </NavLink>
        {tag === undefined ? null : (
          <Tag tagLabel={`${tag}`} variant={"default"} />
        )}
      </TabContainer>
    </Tooltip>
  );
};

export type { ITabProps };
export { Tab };
