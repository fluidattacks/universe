import { styled } from "styled-components";
import type { DefaultTheme } from "styled-components";

import { TVariant } from "./types";

const StyledFixedTab = styled.button`
  ${({ theme }): string => `
    align-items: center;
    color: ${theme.palette.gray[500]};
    display: flex;
    gap: ${theme.spacing[0.625]};
    justify-content: center;
    padding: ${theme.spacing[0.75]};
    text-align: center;
    width: 151px;

    &.active {
      color: ${theme.palette.gray[800]};
      border-bottom: 3px solid ${theme.palette.primary[400]};
    }

    &:hover {
      color: ${theme.palette.gray[800]};
    }
  `}
`;

const StyledFixedTabs = styled.div`
  ${({ theme }): string => `
    align-items: center;
    border-bottom: 2px solid ${theme.palette.gray[200]};
    display: flex;
    gap: ${theme.spacing[3]};
    height: ${theme.spacing[3]};
    width: max-content;
  `}
`;

const getBorder = (
  theme: DefaultTheme,
  variant: TVariant,
  isActive: boolean,
): string => {
  if (isActive) {
    if (variant === "secondary") {
      return `3px solid ${theme.palette.primary[100]}`;
    }

    return `3px solid ${theme.palette.primary[500]}`;
  }

  return "unset";
};

const TabContainer = styled.div<{
  $variant: TVariant;
  $borderButton: boolean;
}>`
  display: flex;
  flex-direction: row;
  gap: ${({ theme }): string => theme.spacing[0.25]};
  border-bottom: ${({ theme, $borderButton, $variant }): string =>
    getBorder(theme, $variant, $borderButton)};
  align-items: center;
  padding: ${({ theme }): string => theme.spacing[0.625]}
    ${({ theme }): string => theme.spacing[0.5]};
  padding-bottom: ${({ theme, $borderButton }): string =>
    $borderButton
      ? `calc(${theme.spacing[0.625]} - 2px)`
      : theme.spacing[0.625]};

  &:hover:not([disabled]) {
    background-color: ${({ theme }): string => theme.palette.gray[100]};
    color: ${({ theme }): string => theme.palette.gray[800]};
  }

  a {
    align-items: center;
    background: transparent;
    color: ${({ theme }): string => theme.palette.gray[800]};
    font-family: ${({ theme }): string => theme.typography.type.primary};
    font-size: ${({ theme }): string => theme.typography.text.sm};
    display: flex;
    font-weight: ${({ theme }): string => theme.typography.weight.regular};
    position: relative;
    text-align: center;
    text-decoration: unset;
    width: auto;
    cursor: pointer;

    &:disabled {
      cursor: not-allowed;
      background-color: ${({ theme }): string => theme.palette.gray[300]};
      color: ${({ theme }): string => theme.palette.gray[300]};
    }

    &.active {
      background-color: transparent;

      &:hover {
        background-color: transparent;
      }
    }
  }
`;

export { TabContainer, StyledFixedTab, StyledFixedTabs };
