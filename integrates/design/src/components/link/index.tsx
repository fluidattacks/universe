import { PropsWithChildren } from "react";

import { StyledExternalLink, StyledInternalLink } from "./styles";
import type { ILinkProps } from "./types";

import { Icon } from "components/icon";

const Link = ({
  children,
  color,
  download,
  href,
  iconPosition = "right",
  onClick,
  target,
  variant = "lowRelevance",
}: Readonly<PropsWithChildren<ILinkProps>>): JSX.Element => {
  const isExternal = href.startsWith("https://");
  const icon = (
    <Icon
      icon={
        download === undefined
          ? "arrow-up-right-from-square"
          : "arrow-down-to-bracket"
      }
      iconSize={"xxs"}
      iconType={"fa-regular"}
      ml={0.25}
      mr={0.25}
    />
  );

  if (isExternal) {
    return (
      <StyledExternalLink
        $color={color}
        $variant={variant}
        download={download}
        href={href}
        onClick={onClick}
        rel={"nofollow noopener noreferrer"}
        target={target ?? "_blank"}
      >
        {iconPosition === "left" ? icon : undefined}
        {children}
        {iconPosition === "right" ? icon : undefined}
      </StyledExternalLink>
    );
  }

  return (
    <StyledInternalLink
      $variant={variant}
      download={download}
      onClick={onClick}
      rel={"nofollow noopener noreferrer"}
      target={target}
      to={href}
    >
      {target !== undefined && iconPosition === "left" ? icon : undefined}
      {children}
      {target !== undefined && iconPosition === "right" ? icon : undefined}
    </StyledInternalLink>
  );
};

export type { ILinkProps };
export { Link };
