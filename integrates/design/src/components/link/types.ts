type TVariant = "highRelevance" | "lowRelevance";
type TIconPosition = "hidden" | "left" | "right";

/**
 * Link component props.
 * @interface ILinkProps
 * @extends React.AnchorHTMLAttributes<HTMLAnchorElement>
 * @property { string } [color] - The color of the link.
 * @property { string } href - The URL of the link.
 * @property { TIconPosition } [iconPosition] - The position of the icon.
 * @property {Function} [onClick] - The callback function for when the link is clicked.
 * @property {React.HTMLAttributeAnchorTarget} [target] - The target attribute for the link.
 * @property { TVariant } [variant] - The variant of the link.
 */
interface ILinkProps extends Partial<Pick<HTMLAnchorElement, "download">> {
  color?: string;
  href: string;
  iconPosition?: TIconPosition;
  onClick?: () => void;
  target?: React.HTMLAttributeAnchorTarget;
  variant?: TVariant;
}

export type { TVariant, ILinkProps };
