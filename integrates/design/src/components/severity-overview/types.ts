type TSeverityOverviewBadgeVariant = "critical" | "high" | "low" | "medium";

/**
 * Severity overview badge component props.
 * @interface ISeverityOverviewBadgeProps
 * @property { TSeverityOverviewBadgeVariant } variant - Variant of the badge.
 * @property { number } value - Value of the badge.
 */
interface ISeverityOverviewBadgeProps {
  variant: TSeverityOverviewBadgeVariant;
  value: number;
}

/**
 * Variant of severity badge.
 * @interface IVariant
 * @property { string } iconColor - Color of the icon.
 * @property { string } iconText - Text of the icon.
 * @property { string } iconTextColor - Color of the icon text.
 * @property { string } textColor - Color of the text.
 */
interface IVariant {
  iconColor: string;
  iconText: string;
  iconTextColor: string;
  textColor: string;
}

/**
 * Severity overview component props.
 * @interface ISeverityOverviewProps
 * @property { number } critical - Number of critical vulnerabilities.
 * @property { number } high - Number of high vulnerabilities.
 * @property { number } medium - Number of medium vulnerabilities.
 * @property { number } low - Number of low vulnerabilities.
 */
interface ISeverityOverviewProps {
  critical: number;
  high: number;
  medium: number;
  low: number;
}

export type {
  ISeverityOverviewProps,
  ISeverityOverviewBadgeProps,
  IVariant,
  TSeverityOverviewBadgeVariant,
};
