import type { Meta, StoryFn } from "@storybook/react";

import type { ISeverityOverviewProps } from "./types";

import { SeverityOverview } from ".";

const config: Meta = {
  component: SeverityOverview,
  tags: ["autodocs"],
  title: "Components/SeverityOverview",
};

const Template: StoryFn<ISeverityOverviewProps> = (props): JSX.Element => (
  <SeverityOverview {...props} />
);

const Default = Template.bind({});

Default.args = {
  critical: 1,
  high: 2,
  low: 0,
  medium: 3,
};

export { Default };
export default config;
