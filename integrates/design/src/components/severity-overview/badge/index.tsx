import { capitalize } from "lodash";

import { ISeverityOverviewBadgeProps } from "../types";
import { getVariant } from "../utils";
import { Container } from "components/container";
import { Tooltip } from "components/tooltip";
import { Text } from "components/typography";

const SeverityOverviewBadge = ({
  variant,
  value,
}: Readonly<ISeverityOverviewBadgeProps>): JSX.Element => {
  const { iconColor, iconText, iconTextColor, textColor } = getVariant(
    variant,
    value,
  );
  const tooltipLabel = capitalize(variant.charAt(0)) + variant.slice(1);

  return (
    <Container
      alignItems={"center"}
      display={"inline-flex"}
      gap={0.5}
      minWidth={"43px"}
    >
      <Tooltip id={tooltipLabel} place={"top"} tip={tooltipLabel}>
        <Container
          alignItems={"center"}
          bgColor={iconColor}
          borderRadius={"4px"}
          cursor={"pointer"}
          display={"flex"}
          height={"22px"}
          maxWidth={"22px"}
          minWidth={"22px"}
        >
          <Text color={iconTextColor} size={"sm"} textAlign={"center"}>
            {iconText}
          </Text>
        </Container>
      </Tooltip>
      <Text color={textColor} size={"sm"}>
        {value}
      </Text>
    </Container>
  );
};

export { SeverityOverviewBadge };
