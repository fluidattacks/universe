import { SeverityOverviewBadge } from "./badge";
import { ISeverityOverviewProps } from "./types";

import { Container } from "components/container";

const SeverityOverview = ({
  critical,
  high,
  medium,
  low,
}: Readonly<ISeverityOverviewProps>): JSX.Element => {
  return (
    <Container alignItems={"center"} display={"inline-flex"} gap={1}>
      <SeverityOverviewBadge value={critical} variant={"critical"} />
      <SeverityOverviewBadge value={high} variant={"high"} />
      <SeverityOverviewBadge value={medium} variant={"medium"} />
      <SeverityOverviewBadge value={low} variant={"low"} />
    </Container>
  );
};

export { SeverityOverview };
