import type { ButtonHTMLAttributes } from "react";

import { IIconModifiable } from "components/@core";

type TVariant =
  | "add"
  | "approve"
  | "disabled"
  | "reject"
  | "submit"
  | "success";

/**
 * Table button component props.
 * @interface ITableButtonProps
 * @extends IIconModifiable
 * @extends ButtonHTMLAttributes<HTMLButtonElement>
 * @property { string } [label] - Button label.
 * @property { TVariant } variant - Button variant.
 */
interface ITableButtonProps
  extends Partial<Pick<IIconModifiable, "icon">>,
    ButtonHTMLAttributes<HTMLButtonElement> {
  label?: string;
  variant: TVariant;
}

export type { ITableButtonProps, TVariant };
