import React, { Ref, forwardRef, useCallback } from "react";

import { Ball, Slider, ToggleContainer } from "./styles";
import type { IToggleProps } from "./types";

import { Container } from "components/container";

const Toggle = forwardRef(function Toggle(
  {
    defaultChecked,
    disabled = false,
    justify,
    name,
    leftDescription,
    leftDescriptionMinWidth,
    rightDescription,
    ...props
  }: Readonly<IToggleProps>,
  ref: Ref<HTMLInputElement>,
): JSX.Element {
  const toggleValue = defaultChecked === true ? "on" : "off";

  const leftDescriptionText =
    typeof leftDescription === "object" &&
    leftDescription.on &&
    leftDescription.off
      ? leftDescription[toggleValue]
      : leftDescription;

  const handleClick = useCallback(
    (event: Readonly<React.MouseEvent<HTMLLabelElement>>): void => {
      event.stopPropagation();
    },
    [],
  );

  return (
    <Container
      alignItems={"center"}
      display={"flex"}
      fontSize={"14px"}
      gap={0.75}
      justify={justify}
    >
      <Container minWidth={leftDescriptionMinWidth}>
        {leftDescriptionText as string}
      </Container>
      <ToggleContainer onClick={handleClick} role={"switch"}>
        <input
          {...props}
          aria-disabled={disabled}
          aria-label={name}
          checked={defaultChecked === true}
          disabled={disabled}
          name={name}
          ref={ref}
          type={"checkbox"}
        />
        <Slider aria-label={"Toggle Switch"} id={`${name}Toggle`}>
          <Ball />
        </Slider>
      </ToggleContainer>

      {rightDescription}
    </Container>
  );
});

export { Toggle };
