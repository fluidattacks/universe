import { styled } from "styled-components";

const Slider = styled.div`
  ${({ theme }): string => `
    align-items: center;
    background-color: ${theme.palette.gray[200]};
    border-radius: 34px;
    cursor: pointer;
    display: flex;
    height: 20px;
    position: relative;
    transition: 0.2s;
    width: 36px;
  `}
`;

const Ball = styled.div`
  ${({ theme }): string => `
    background-color: ${theme.palette.white};
    border-radius: 50%;
    height: ${theme.spacing[1]};
    left: 2px;
    position: absolute;
    transition: 0.2s;
    width: ${theme.spacing[1]};
    z-index: 1;
  `}
`;

const ToggleContainer = styled.label`
  display: inline-block;

  input {
    position: absolute;
    height: 0;
    opacity: 0;
    width: 0;
  }

  input:checked + ${Slider} {
    background-color: ${({ theme }): string => theme.palette.primary[500]};
  }

  input:checked + ${Slider} > div {
    transform: translateX(100%);
  }

  input:disabled + ${Slider} {
    cursor: not-allowed;
    background-color: ${({ theme }): string => theme.palette.gray[100]};
  }

  input:disabled + ${Slider} > div {
    background-color: ${({ theme }): string => theme.palette.gray[200]};
    border: 1px solid ${({ theme }): string => theme.palette.gray[200]};
  }
`;

export { Slider, ToggleContainer, Ball };
