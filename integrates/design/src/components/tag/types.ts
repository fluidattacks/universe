import { IIconModifiable } from "components/@core";

type TTagVariant =
  | "default"
  | "error"
  | "inactive"
  | "info"
  | "reachable"
  | "remediation"
  | "role"
  | "success"
  | "technique"
  | "warning";

type TTagPriority = "default" | "high" | "low" | "medium";

/**
 * Tag component props.
 * @interface ITagProps
 * @extends IIconModifiable
 * @extends HTMLAttributes<HTMLSpanElement>
 * @property { boolean } [disabled] - Whether the tag is disabled.
 * @property { string } [filterValues] - The filter values for the tag.
 * @property { string } [fontSize] - The font size for the tag.
 * @property { string } [href] - The href for the tag.
 * @property { string } [linkLabel] - The link label for the tag.
 * @property { Function } [onClose] - Function handler for close button click.
 * @property { string } [tagTitle] - The title for the tag.
 * @property { string } [tagLabel] - The label for the tag.
 * @property { TTagPriority } [priority] - The priority of the tag.
 * @property { TTagVariant } [variant] - The variant of the tag.
 */
interface ITagProps
  extends Partial<Pick<IIconModifiable, "icon" | "iconColor" | "iconType">>,
    React.HTMLAttributes<HTMLSpanElement> {
  disabled?: boolean;
  filterValues?: string;
  fontSize?: string;
  href?: string;
  linkLabel?: string;
  onClose?: () => void;
  tagTitle?: string;
  tagLabel: string;
  priority?: TTagPriority;
  variant?: TTagVariant;
}

export type { ITagProps, TTagVariant };
