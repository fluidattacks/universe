import _ from "lodash";
import { useMemo } from "react";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TCallable = (...args: readonly any[]) => any;

// Delays and groups function calls
const useDebouncedCallback = <T extends TCallable>(
  callback: T,
  wait: number,
): _.DebouncedFunc<T> => {
  const debouncedCallback = useMemo(
    (): _.DebouncedFunc<typeof callback> => _.debounce(callback, wait),
    [callback, wait],
  );

  return debouncedCallback;
};

export { useDebouncedCallback };
