import { useCallback, useState } from "react";

interface IUseModal {
  open: () => void;
  close: () => void;
  toggle: () => void;
  isOpen: boolean;
  name: string;
  setIsOpen: (value: boolean) => void;
}

const useModal = (name: string): IUseModal => {
  const [isOpen, setIsOpen] = useState(false);

  const open = useCallback((): void => {
    setIsOpen(true);
  }, []);
  const close = useCallback((): void => {
    setIsOpen(false);
  }, []);
  const toggle = useCallback((): void => {
    setIsOpen((previousState): boolean => !previousState);
  }, []);

  return { close, isOpen, name, open, setIsOpen, toggle };
};

export type { IUseModal };
export { useModal };
