<br />
<p align="center">
  <a href="https://fluidattacks.com/" rel="noopener" target="_blank"><img
  width="460px" src="https://res.cloudinary.com/fluid-attacks/image/upload/v1728418266/airs/logo/logo_full.png" alt="Fluid Attacks logo"></a>
</p>

<h1 align="center">Fluid Attacks Design Components Library</h1>

Fluid Attacks Design is an open-source React component library that implements
the components used to develop Fluid Attacks' digital interfaces. It's comprehensive and can be used in production out of the box.

## Installation

Install the package in your project directory with:

```bash
npm install @fluidattacks/design
```

## Documentation

Visit [https://dev.fluidattacks.com/](https://dev.fluidattacks.com/components/integrates/design/) to view the full documentation.

## Components

All the components can be seen in the [Design Library](https://design.fluidattacks.com/).

## Contributing

Read the [contributing guide](https://dev.fluidattacks.com/getting-started/contributing/) to learn about our development process, and how to propose bug fixes and improvements.

## License

This project is licensed under the terms of the
[MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/).
