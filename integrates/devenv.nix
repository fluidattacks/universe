{ inputs, pkgs, ... }:
let utils = import inputs.utils { inherit pkgs; };
in {
  cachix = utils.cachix.config.default;

  packages = [ pkgs.devenv pkgs.ncurses utils.shell.aws ];

  processes = {
    integrates-back.exec = ''
      pushd back && devenv shell "integrates-back dev"
    '';
    integrates-front.exec = ''
      pushd front && devenv shell "integrates-front start";
    '';
  };
}
