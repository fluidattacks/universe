from dataclasses import (
    dataclass,
)
from unittest.mock import (
    Mock,
    call,
    patch,
)

import pytest

from streams.hooks.handler import (
    process_vulns,
)
from streams.types import (
    StreamEvent,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_issues")
@patch("requests.post")
def test_relevant_changes_are_notified(mock_post: Mock) -> None:
    changed_to_safe = {
        "dynamodb": {
            "Keys": {
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
            },
            "NewImage": {
                "bug_tracking_system_url": {"S": "https://site.atlassian.net/INTEGRATES-23"},
                "group_name": {"S": "test"},
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "state": {
                    "M": {
                        "specific": {"S": "specific1"},
                        "status": {"S": "SAFE"},
                        "where": {"S": "where1"},
                    },
                },
                "webhook_url": {"S": "https://id.hello.atlassian-dev.net/x1/id"},
            },
            "OldImage": {
                "bug_tracking_system_url": {"S": "https://site.atlassian.net/INTEGRATES-23"},
                "group_name": {"S": "test"},
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "state": {
                    "M": {
                        "specific": {"S": "specific1"},
                        "status": {"S": "VULNERABLE"},
                        "where": {"S": "where1"},
                    },
                },
                "webhook_url": {"S": "https://id.hello.atlassian-dev.net/x1/id"},
            },
            "SequenceNumber": "000000000000000000001",
        },
        "eventName": StreamEvent.MODIFY.value,
    }
    removed = {
        "dynamodb": {
            "Keys": {
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
            },
            "OldImage": {
                "bug_tracking_system_url": {"S": "https://site.atlassian.net/INTEGRATES-25"},
                "group_name": {"S": "test"},
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
                "state": {
                    "M": {
                        "reasons": {"L": [{"S": "FALSE_POSITIVE"}]},
                        "specific": {"S": "specific1"},
                        "status": {"S": "VULNERABLE"},
                        "where": {"S": "where1"},
                    },
                },
                "webhook_url": {"S": "https://id.hello.atlassian-dev.net/x1/id"},
            },
            "SequenceNumber": "000000000000000000003",
        },
        "eventName": StreamEvent.REMOVE.value,
    }
    process_vulns({"Records": [changed_to_safe, removed]}, LambdaContext())
    mock_post.assert_has_calls(
        [
            call(
                "https://id.hello.atlassian-dev.net/x1/id",
                json={
                    "issueURL": "https://site.atlassian.net/INTEGRATES-23",
                    "payload": {
                        "action": "CHANGED_TO_SAFE",
                        "specific": "specific1",
                        "where": "where1",
                    },
                },
                timeout=10,
            ),
            call(
                "https://id.hello.atlassian-dev.net/x1/id",
                json={
                    "issueURL": "https://site.atlassian.net/INTEGRATES-25",
                    "payload": {
                        "action": "WAS_REMOVED",
                        "specific": "specific1",
                        "reason": "FALSE_POSITIVE",
                        "where": "where1",
                    },
                },
                timeout=10,
            ),
        ],
        any_order=True,
    )


@pytest.mark.resolver_test_group("streams_issues")
@patch("requests.post")
def test_irrelevant_changes_arent_notified(mock_post: Mock) -> None:
    modified_same_status = {
        "dynamodb": {
            "Keys": {
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
            },
            "NewImage": {
                "group_name": {"S": "test"},
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "state": {
                    "M": {
                        "status": {"S": "VULNERABLE"},
                        "where": {"S": "where2"},
                        "specific": {"S": "where2"},
                    },
                },
            },
            "OldImage": {
                "group_name": {"S": "test"},
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "state": {
                    "M": {
                        "status": {"S": "VULNERABLE"},
                        "where": {"S": "where2"},
                        "specific": {"S": "where2"},
                    },
                },
            },
            "SequenceNumber": "000000000000000000001",
        },
        "eventName": StreamEvent.MODIFY.value,
    }
    removed_safe = {
        "dynamodb": {
            "Keys": {
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
            },
            "OldImage": {
                "group_name": {"S": "test"},
                "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
                "state": {"M": {"status": {"S": "SAFE"}}},
            },
            "SequenceNumber": "000000000000000000002",
        },
        "eventName": StreamEvent.REMOVE.value,
    }
    process_vulns({"Records": [modified_same_status, removed_safe]}, LambdaContext())
    mock_post.assert_not_called()
