import pytest

from streams.historic.handler import (
    batch_put_item,
)

MOCK_DATA: dict[str, list[dict]] = {
    "test_streams_process_remove_historic": [
        {
            "modified_by": "unittest@fluidattacks.com",
            "sk": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "pk": "EVENT#688900977#GROUP#group1",
            "sk_2": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-12-17T21:20:00+00:00",
            "status": "OPEN",
        },
        {
            "modified_by": "unittest@fluidattacks.com",
            "sk": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "pk": "EVENT#688900977#GROUP#group1",
            "sk_2": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-12-17T21:21:03+00:00",
            "status": "CREATED",
        },
        {
            "modified_by": "unittest@fluidattacks.com",
            "sk": "STATE#state#DATE#2018-06-30T19:40:05+00:00",
            "pk": "EVENT#688900977#GROUP#group1",
            "sk_2": "STATE#state#DATE#2018-06-30T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-30T19:40:05+00:00",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-12-26T18:37:00+00:00",
            "status": "SOLVED",
        },
        {
            "modified_by": "unittest@fluidattacks.com",
            "sk": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "pk": "EVENT#788900977#GROUP#group1",
            "sk_2": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-12-17T21:20:00+00:00",
            "status": "OPEN",
        },
        {
            "modified_by": "unittest@fluidattacks.com",
            "sk": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "pk": "EVENT#788900977#GROUP#group1",
            "sk_2": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-12-17T21:21:03+00:00",
            "status": "CREATED",
        },
        {
            "modified_by": "unittest@fluidattacks.com",
            "sk": "STATE#verification#DATE#2018-06-28T19:40:05+00:00",
            "pk": "EVENT#788900977#GROUP#group1",
            "sk_2": "STATE#verification#DATE#2018-06-28T19:40:05+00:00",
            "sk_3": "STATE#verification#DATE#2018-06-28T19:40:05+00:00",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-12-17T21:20:00+00:00",
            "status": "OPEN",
        },
        {
            "modified_by": "unittest@fluidattacks.com",
            "sk": "STATE#verification#DATE#2018-06-29T19:40:05+00:00",
            "pk": "EVENT#788900977#GROUP#group1",
            "sk_2": "STATE#verification#DATE#2018-06-29T19:40:05+00:0",
            "sk_3": "STATE#verification#DATE#2018-06-29T19:40:05+00:0",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-12-17T21:21:03+00:00",
            "status": "CREATED",
        },
    ],
}


@pytest.fixture(autouse=True)
def mock_data_for_module(
    request: pytest.FixtureRequest,
) -> None:
    db_data = MOCK_DATA.get(request.function.__name__)
    if db_data:
        batch_put_item(tuple(db_data))
