import time
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

import pytest

from streams.search.handler import (
    process_lines,
)
from test.functional.src.utils import (
    SearchParams,
    search,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_lines")
@pytest.mark.parametrize(
    ["input_data", "output_data"],
    [
        [
            [
                {
                    "eventID": "65062677-c35d-493b-b07c-dc9a57054f5e",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691613480000,
                        "Keys": {
                            "sk": {
                                "S": "LINES#ROOT#63298a73-9dff-46cf-b42d-"
                                + "9b2f01a56690#FILENAME#test/test1.py",
                            },
                            "pk": {"S": "GROUP#group1"},
                        },
                        "NewImage": {
                            "seen_first_time_by": {"S": "admin@fluidattacks.com"},
                            "filename": {"S": "test/test1.py"},
                            "group_name": {"S": "group1"},
                            "sk": {
                                "S": "LINES#ROOT#63298a73-9dff-46cf-b42d-"
                                + "9b2f01a56690#FILENAME#test/test1.py",
                            },
                            "root_id": {"S": "63298a73-9dff-46cf-b42d-9b2f01a56690"},
                            "pk_2": {"S": "GROUP#group1"},
                            "pk": {"S": "GROUP#group1"},
                            "state": {
                                "M": {
                                    "loc": {"N": "50"},
                                    "comments": {"S": ""},
                                    "sorts_risk_level": {"N": "-1"},
                                    "last_commit": {
                                        "S": "d9e4beba70c4f34d6117c3b0c23ebe6" + "b2bff66c4",
                                    },
                                    "last_author": {"S": "test@test.com"},
                                    "has_vulnerabilities": {"BOOL": False},
                                    "be_present": {"BOOL": False},
                                    "modified_date": {"S": "2023-08-09T20:38:42.798613+00:00"},
                                    "attacked_by": {"S": ""},
                                    "be_present_until": {"S": "2023-08-09T20:38:42.798598+00:00"},
                                    "attacked_lines": {"N": "0"},
                                    "modified_by": {"S": "admin@fluidattacks.com"},
                                    "last_commit_date": {"S": "2020-11-19T13:37:10+00:00"},
                                    "sorts_priority_factor": {"N": "-1"},
                                    "seen_at": {"S": "2023-08-09T20:38:42.798620+00:00"},
                                },
                            },
                            "sk_2": {
                                "S": "LINES#PRESENT#false#ROOT#63298a73-9dff-"
                                + "46cf-b42d-9b2f01a56690#FILENAME#test/"
                                + "test1.py",
                            },
                        },
                        "SequenceNumber": "000000000000000000277",
                        "SizeBytes": 813,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
            [
                {
                    "pk": "GROUP#group1",
                    "sk": "LINES#ROOT#63298a73-9dff-46cf-b42d-9b2f01a56690#"
                    + "FILENAME#test/test1.py",
                },
            ],
        ],
    ],
)
def test_streams_process_lines(
    input_data: list[dict[str, Any]],
    output_data: list[dict[str, Any]],
) -> None:
    search_result = search(SearchParams(index_value="lines_index", limit=10))

    assert search_result.total == 0

    process_lines({"Records": input_data}, LambdaContext())
    time.sleep(5)

    search_result = search(SearchParams(index_value="lines_index", limit=10))

    assert search_result.total == 1

    items = search_result.items

    for idx, item in enumerate(items):
        assert item["pk"] == output_data[idx]["pk"]
        assert item["sk"] == output_data[idx]["sk"]
