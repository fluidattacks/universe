# pylint: disable=too-few-public-methods
from decimal import (
    Decimal,
)
from typing import (
    Any,
)


class EventDataBuilder:
    @staticmethod
    def insert_record(
        *,
        vuln: dict[str, Any],
    ) -> dict[str, Any]:
        return {
            "eventName": "INSERT",
            "eventVersion": "1.1",
            "eventSource": "aws:dynamodb",
            "awsRegion": "ddblocal",
            "eventID": "33ae7b32-4918-4097-864b-9e84ccb2cd71",
            "dynamodb": {
                "StreamViewType": "NEW_AND_OLD_IMAGES",
                "SizeBytes": 1102,
                "ApproximateCreationDateTime": 1691616180000,
                "SequenceNumber": "000000000000000000001",
                "Keys": {
                    "sk": vuln["sk"],
                    "pk": vuln["pk"],
                },
                "NewImage": vuln,
            },
        }


class StreamsDataBuilder:
    @staticmethod
    def vuln_record(
        *,
        vuln_id: str = "559cb1d7-4b4f-4d30-be0e-648d42c1c0c5",
        fin_id: str = "563827909",
        group: str = "unittesting",
        org_name: str = "okada",
        created_by: str = "test1@gmail.com",
        vuln_type: str = "PORTS",
        **kwargs: Any,
    ) -> dict[str, Any]:
        return StreamsDataBuilder._cast_to_db(
            {
                "sk": f"FIN#{fin_id}",
                "pk": f"VULN#{vuln_id}",
                "pk_2": "ROOT",
                "pk_3": "USER",
                "pk_4": "EVENT",
                "pk_5": f"GROUP#{group}",
                "pk_6": f"FIN#{fin_id}",
                "pk_hash": "HASH",
                "sk_2": f"VULN#{vuln_id}",
                "sk_3": f"VULN#{vuln_id}",
                "sk_4": f"VULN#{vuln_id}",
                "sk_5": "VULN#ZR#false#STATE#vulnerable#TREAT#false",
                "sk_6": (
                    "VULN#DELETED#false#RELEASED#true#ZR#false#STATE#vulnerable#VERIF#requested"
                ),
                "sk_hash": "ROOT",
                "id": vuln_id,
                "type": vuln_type,
                "group_name": group,
                "finding_id": fin_id,
                "organization_name": org_name,
                "created_date": "2018-04-08T00:45:15+00:00",
                "created_by": created_by,
                "hacker_email": created_by,
                **kwargs,
            },
        )["M"]

    @staticmethod
    def _cast_to_db(value: str | int | float | dict) -> dict[str, Any]:
        if isinstance(value, (str)):
            return {"S": str(value)}
        if isinstance(value, (int, float)):
            return {"N": str(value)}
        if isinstance(value, (list)):
            return {"L": [StreamsDataBuilder._cast_to_db(v) for v in value]}
        if isinstance(value, (dict)):
            for key, val in value.items():
                value[key] = StreamsDataBuilder._cast_to_db(val)
        return {"M": value}


new_vuln: dict[str, Any] = StreamsDataBuilder.vuln_record(
    vuln_id="559cb1d7-4b4f-4d30-be0e-648d42c1c0c5",
    fin_id="563827909",
    created_by="test1@gmail.com",
    vuln_type="PORTS",
    state={
        "modified_by": "test1@gmail.com",
        "where": "192.168.1.20",
        "source": "ASM",
        "modified_date": "2018-04-08T00:45:15+00:00",
        "specific": "9999",
        "status": "VULNERABLE",
    },
    verification={
        "modified_date": "2018-04-09T00:45:11+00:00",
        "status": "REQUESTED",
    },
    treatment={
        "modified_date": "2018-04-08T00:45:11+00:00",
        "status": "UNTREATED",
    },
    unreliable_indicators={
        "unreliable_source": "ASM",
        "unreliable_treatment_changes": 0,
        "unreliable_reattack_cycles": 0,
        "unreliable_efficacy": 0,
        "unreliable_report_date": "2020-01-01T10:00:00+00:00",
    },
)

existing_vuln: dict[str, Any] = StreamsDataBuilder.vuln_record(
    vuln_id="80d6a69f-a376-46be-98cd-2fdedcffdcc0",
    fin_id="422286126",
    created_by="test@unittesting.com",
    vuln_type="INPUTS",
    state={
        "modified_by": "test@unittesting.com",
        "where": "https://example.com",
        "source": "ASM",
        "modified_date": "2020-09-09T21:01:26+00:00",
        "specific": "phone",
        "tool": {"name": "tool-2", "impact": "INDIRECT"},
        "status": "VULNERABLE",
    },
    treatment={
        "modified_by": "integratesuser2@gmail.com",
        "assigned": "integratesuser@gmail.com",
        "justification": "This is a treatment justification",
        "modified_date": "2020-11-23T17:46:10+00:00",
        "status": "IN_PROGRESS",
    },
    zero_risk={
        "modified_by": "test@gmail.com",
        "modified_date": "2020-09-09T21:01:26+00:00",
        "comment_id": "123456",
        "status": "CONFIRMED",
    },
    unreliable_indicators={
        "unreliable_reattack_cycles": 0,
        "unreliable_source": "ASM",
        "unreliable_efficacy": 0,
        "unreliable_report_date": "2020-09-09T21:01:26+00:00",
        "unreliable_treatment_changes": 1,
    },
    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
    technique="PTAAS",
    cwe_ids=["CWE-1035", "CWE-770", "CWE-937"],
    severity_score={
        "cvssf": 3.482,
        "base_score": 5.4,
        "temporal_score": 4.9,
        "cvss_v3": ("CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/E:P/RL:O/RC:C"),
        "threat_score": 4.9,
        "cvss_v4": ("CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:N/SC:N/SI:N/SA:N/E:P"),
        "cvssf_v4": 3.482,
    },
)

vuln_to_insert_1: dict[str, Any] = {
    "eventName": "INSERT",
    "eventVersion": "1.1",
    "eventSource": "aws:dynamodb",
    "awsRegion": "ddblocal",
    "eventID": "33ae7b32-4918-4097-864b-9e84ccb2cd71",
    "dynamodb": {
        "StreamViewType": "NEW_AND_OLD_IMAGES",
        "SizeBytes": 1102,
        "ApproximateCreationDateTime": 1691616180000,
        "SequenceNumber": "000000000000000000001",
        "Keys": {
            "sk": new_vuln["sk"],
            "pk": new_vuln["pk"],
        },
        "NewImage": new_vuln,
    },
}

vuln_to_modify_1: dict[str, Any] = {
    "eventName": "MODIFY",
    "eventVersion": "1.1",
    "eventSource": "aws:dynamodb",
    "awsRegion": "ddblocal",
    "eventID": "8807ece0-768e-4f01-b9dd-f2299b62c6ba",
    "dynamodb": {
        "StreamViewType": "NEW_AND_OLD_IMAGES",
        "SizeBytes": 2084,
        "ApproximateCreationDateTime": 1691616180000,
        "SequenceNumber": "000000000000000000002",
        "Keys": {
            "sk": existing_vuln["sk"],
            "pk": existing_vuln["pk"],
        },
        "NewImage": {
            **existing_vuln,
            "state": {
                "M": {
                    "modified_by": {"S": "test@unittesting.com"},
                    "where": {"S": "https://example.com"},
                    "source": {"S": "ASM"},
                    "modified_date": {"S": "2020-09-09T21:01:26+00:00"},
                    "specific": {"S": "phone"},
                    "tool": {
                        "M": {
                            "name": {"S": "tool-2"},
                            "impact": {"S": "INDIRECT"},
                        },
                    },
                    "status": {"S": "SAFE"},
                },
            },
            "unreliable_indicators": {
                "M": {
                    "unreliable_source": {"S": "ASM"},
                    "unreliable_treatment_changes": {"N": "0"},
                    "unreliable_reattack_cycles": {"N": "0"},
                    "unreliable_closing_date": {"S": "2020-01-01T10:00:00+00:00"},
                    "unreliable_efficacy": {"N": "0"},
                    "unreliable_report_date": {"S": "2020-01-02T11:00:00+00:00"},
                },
            },
        },
        "OldImage": existing_vuln,
    },
}

vulnerabilities_summary_indicators_1 = [
    [],
    {
        "pk": "FIN#563827909",
        "sk": "GROUP#group1",
        "severity_score": {
            "threat_score": "4.9",
        },
    },
]

vulnerabilities_summary_calculation_1 = [
    [
        {
            "pk": "VULN#e4a794bd-087d-400e-8792-1d7d10918ca4",
            "sk": "FIN#563827909",
            "created_date": "2024-01-01T10:00:00+00:00",
            "group_name": "unittesting",
            "state": {"status": "VULNERABLE"},
            "treatment": {"status": "UNTREATED"},
            "verification": None,
            "severity_score": {"threat_score": Decimal("1.0")},
            "unreliable_indicators": {},
        },
        {
            "pk": "VULN#87c9f4a0-3ea1-4cd1-9e11-03bd45de5a9c",
            "sk": "FIN#563827909",
            "created_date": "2024-01-01T10:00:00+00:00",
            "group_name": "unittesting",
            "state": {"status": "VULNERABLE"},
            "treatment": {"status": "UNTREATED"},
            "verification": None,
            "severity_score": {"threat_score": Decimal("4.0")},
            "unreliable_indicators": {},
        },
        {
            "pk": "VULN#0590954d-9993-4835-9753-dffea6edb40a",
            "sk": "FIN#563827909",
            "created_date": "2024-01-01T10:00:00+00:00",
            "group_name": "unittesting",
            "state": {"status": "VULNERABLE"},
            "treatment": {"status": "UNTREATED"},
            "verification": None,
            "severity_score": {"threat_score": Decimal("7.0")},
            "unreliable_indicators": {},
        },
        {
            "pk": "VULN#62130ad7-7911-497d-8b00-d6728c259c73",
            "sk": "FIN#563827909",
            "created_date": "2024-01-01T10:00:00+00:00",
            "group_name": "unittesting",
            "state": {"status": "VULNERABLE"},
            "treatment": {"status": "UNTREATED"},
            "verification": None,
            "severity_score": {"threat_score": Decimal("10.0")},
            "unreliable_indicators": {},
        },
        {
            "pk": "VULN#c8b74b10-e583-476f-8733-1c42f59f2287",
            "sk": "FIN#563827909",
            "created_date": "2024-01-01T10:00:00+00:00",
            "group_name": "unittesting",
            "state": {"status": "SAFE"},
            "treatment": {"status": "UNTREATED"},
            "verification": None,
            "severity_score": {"threat_score": Decimal("5.0")},
            "unreliable_indicators": {},
        },
    ],
    {
        "pk": "FIN#563827909",
        "sk": "GROUP#unittesting",
        "severity_score": {
            "threat_score": "4.9",
        },
    },
    {
        "submitted_vulnerabilities": 0,
        "rejected_vulnerabilities": 0,
        "vulnerabilities_summary": {
            "closed": 1,
            "open": 4,
            "submitted": 0,
            "rejected": 0,
            "open_critical": 1,
            "open_high": 1,
            "open_low": 1,
            "open_medium": 1,
            "open_critical_v3": 0,
            "open_high_v3": 0,
            "open_low_v3": 4,
            "open_medium_v3": 0,
        },
    },
]
