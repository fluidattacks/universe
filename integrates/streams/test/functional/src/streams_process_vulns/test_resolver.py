import time
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

import pytest

from streams.search.handler import (
    process_vulns,
)
from test.functional.src.utils import (
    SearchParams,
    search,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_vulns")
@pytest.mark.parametrize(
    ["input_data", "output_data"],
    [
        [
            [
                {
                    "eventID": "7c13d3d4-9568-4c21-af68-0dca56fa4e46",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691616180000,
                        "Keys": {
                            "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
                            "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-" + "bb7566c460bd"},
                        },
                        "NewImage": {
                            "treatment": {
                                "M": {
                                    "modified_date": {"S": "2018-04-08T00:45:11+00:00"},
                                    "status": {"S": "UNTREATED"},
                                },
                            },
                            "pk_4": {"S": "EVENT"},
                            "pk_5": {"S": "GROUP#group1"},
                            "pk_6": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
                            "type": {"S": "PORTS"},
                            "sk": {"S": "FIN#3c475384-834c-47b0-ac71-a41a022e401c"},
                            "pk_2": {"S": "ROOT"},
                            "pk_3": {"S": "USER"},
                            "id": {"S": "4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "test1@gmail.com"},
                                    "where": {"S": "192.168.1.20"},
                                    "source": {"S": "ASM"},
                                    "modified_date": {"S": "2018-04-08T00:45:15+00:00"},
                                    "specific": {"S": "9999"},
                                    "status": {"S": "VULNERABLE"},
                                },
                            },
                            "verification": {
                                "M": {
                                    "modified_date": {"S": "2018-04-09T00:45:11+00:00"},
                                    "status": {"S": "REQUESTED"},
                                },
                            },
                            "finding_id": {"S": "3c475384-834c-47b0-ac71-a41a022e401c"},
                            "hacker_email": {"S": "test1@gmail.com"},
                            "group_name": {"S": "group1"},
                            "pk_hash": {"S": "HASH"},
                            "organization_name": {"S": "orgtest"},
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_source": {"S": "ASM"},
                                    "unreliable_treatment_changes": {"N": "0"},
                                },
                            },
                            "created_by": {"S": "test1@gmail.com"},
                            "sk_4": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-" + "bb7566c460bd"},
                            "sk_3": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-" + "bb7566c460bd"},
                            "sk_6": {
                                "S": "VULN#DELETED#false#RELEASED#true#"
                                + "ZR#false#STATE#vulnerable#VERIF#requested",
                            },
                            "pk": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-" + "bb7566c460bd"},
                            "sk_5": {"S": "VULN#ZR#false#STATE#vulnerable#" + "TREAT#false"},
                            "sk_hash": {"S": "ROOT"},
                            "created_date": {"S": "2018-04-08T00:45:15+00:00"},
                            "sk_2": {"S": "VULN#4dbc03e0-4cfc-4b33-9b70-" + "bb7566c460bd"},
                        },
                        "SequenceNumber": "000000000000000000056",
                        "SizeBytes": 1102,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
                {
                    "eventID": "c420e556-fc43-4b5b-930e-17f7b13f73c3",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691616180000,
                        "Keys": {
                            "sk": {"S": "FIN#475041514"},
                            "pk": {"S": "VULN#c99e0bd7-23e0-47b7-801c-" + "50f9f8b585b0"},
                        },
                        "NewImage": {
                            "treatment": {
                                "M": {
                                    "modified_date": {"S": "2018-04-08T00:45:11+00:00"},
                                    "status": {"S": "UNTREATED"},
                                },
                            },
                            "pk_4": {"S": "EVENT"},
                            "pk_5": {"S": "GROUP#group1"},
                            "pk_6": {"S": "FIN#475041514"},
                            "type": {"S": "PORTS"},
                            "sk": {"S": "FIN#475041514"},
                            "pk_2": {"S": "ROOT"},
                            "pk_3": {"S": "USER"},
                            "id": {"S": "c99e0bd7-23e0-47b7-801c-50f9f8b585b0"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "test1@gmail.com"},
                                    "where": {"S": "192.168.1.20"},
                                    "source": {"S": "ASM"},
                                    "modified_date": {"S": "2018-04-08T00:45:15+00:00"},
                                    "specific": {"S": "9999"},
                                    "status": {"S": "VULNERABLE"},
                                },
                            },
                            "verification": {
                                "M": {
                                    "modified_date": {"S": "2018-04-09T00:45:11+00:00"},
                                    "status": {"S": "REQUESTED"},
                                },
                            },
                            "finding_id": {"S": "475041514"},
                            "hacker_email": {"S": "test1@gmail.com"},
                            "group_name": {"S": "group1"},
                            "pk_hash": {"S": "HASH"},
                            "organization_name": {"S": "orgtest"},
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_source": {"S": "ASM"},
                                    "unreliable_treatment_changes": {"N": "0"},
                                },
                            },
                            "created_by": {"S": "test1@gmail.com"},
                            "sk_4": {"S": "VULN#c99e0bd7-23e0-47b7-801c-" + "50f9f8b585b0"},
                            "sk_3": {"S": "VULN#c99e0bd7-23e0-47b7-801c-" + "50f9f8b585b0"},
                            "sk_6": {
                                "S": "VULN#DELETED#false#RELEASED#true#"
                                + "ZR#false#STATE#vulnerable#VERIF#requested",
                            },
                            "pk": {"S": "VULN#c99e0bd7-23e0-47b7-801c-" + "50f9f8b585b0"},
                            "sk_5": {"S": "VULN#ZR#false#STATE#vulnerable#" + "TREAT#false"},
                            "sk_hash": {"S": "ROOT"},
                            "created_date": {"S": "2018-04-08T00:45:15+00:00"},
                            "sk_2": {"S": "VULN#c99e0bd7-23e0-47b7-801c-" + "50f9f8b585b0"},
                        },
                        "SequenceNumber": "000000000000000000057",
                        "SizeBytes": 994,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
            [
                {
                    "id": "c99e0bd7-23e0-47b7-801c-50f9f8b585b0",
                    "pk": "VULN#c99e0bd7-23e0-47b7-801c-50f9f8b585b0",
                    "sk": "FIN#475041514",
                },
                {
                    "id": "4dbc03e0-4cfc-4b33-9b70-bb7566c460bd",
                    "pk": "VULN#4dbc03e0-4cfc-4b33-9b70-bb7566c460bd",
                    "sk": "FIN#3c475384-834c-47b0-ac71-a41a022e401c",
                },
            ],
        ],
    ],
)
def test_streams_process_vulns(
    input_data: list[dict[str, Any]],
    output_data: list[dict[str, Any]],
) -> None:
    search_result = search(SearchParams(index_value="vulns_index", limit=10))

    assert search_result.total == 0

    process_vulns({"Records": input_data}, LambdaContext())
    time.sleep(5)

    search_result = search(SearchParams(index_value="vulns_index", limit=10))

    assert search_result.total == 2

    items = search_result.items

    for idx, item in enumerate(items):
        assert item["id"] == output_data[idx]["id"]
        assert item["pk"] == output_data[idx]["pk"]
        assert item["sk"] == output_data[idx]["sk"]
