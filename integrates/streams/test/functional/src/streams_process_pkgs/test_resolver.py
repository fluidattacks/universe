import time
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

import pytest

from streams.search.handler import (
    process_pkgs,
)
from test.functional.src.utils import (
    SearchParams,
    search,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_pkgs")
@pytest.mark.parametrize(
    ["input_data", "output_data"],
    [
        [
            [
                {
                    "eventID": "75b75ead-98d5-4f13-ba88-4f37e6911dcb",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1713373500000,
                        "Keys": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk": {
                                "S": "PKGS#ROOT#4039d098-ffc5-4984-8ed3"
                                "-eb17bca98e19#PACKAGE#com.nimbusds:nimbus"
                                "-jose-jwt#VERSION#8.3",
                            },
                        },
                        "NewImage": {
                            "group_name": {"S": "unittesting"},
                            "name": {"S": "com.nimbusds:nimbus-jose-jwt"},
                            "sk": {
                                "S": "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17b"
                                "ca98e19#PACKAGE#com.nimbusds"
                                ":nimbus-jose-jwt#VERSION#8.3",
                            },
                            "pk_2": {"S": "GROUP#unittesting"},
                            "root_id": {"S": "4039d098-ffc5-4984-8ed3-eb17bca98e19"},
                            "pk": {"S": "GROUP#unittesting"},
                            "package_url": {"S": "pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3"},
                            "language": {"S": "java"},
                            "platform": {"S": "MAVEN"},
                            "state": {
                                "M": {
                                    "vulnerable": {"BOOL": False},
                                    "modified_by": {"S": "integrates@fluidattacks.com"},
                                    "be_present": {"BOOL": True},
                                    "id": {"S": "nb931d6e982c13ff"},
                                    "modified_date": {"S": "2024-01-05T14:38:51.241508+00:00"},
                                    "url": {
                                        "S": "https://repo1.maven.org/maven2"
                                        "/com/nimbusds/nimbus-jose-jwt/8.3/",
                                    },
                                    "locations": {
                                        "L": [
                                            {
                                                "M": {
                                                    "path": {"S": "build.gradle"},
                                                    "line": {"S": "85"},
                                                },
                                            },
                                            {
                                                "M": {
                                                    "path": {"S": "/lib/apk/db/installed"},
                                                    "layer": {
                                                        "S": "sha256:d4fc045c"
                                                        "9e3a848011de66f34b81f"
                                                        "052d4f2c15a17bb196d6"
                                                        "37e526349601820",
                                                    },
                                                },
                                            },
                                        ],
                                    },
                                    "licenses": {"L": [{"S": "MIT"}]},
                                    "health_metadata": {
                                        "M": {
                                            "latest_version": {"S": "15.0"},
                                            "author": {"S": "author"},
                                            "repo_url": {"S": ""},
                                        },
                                    },
                                },
                            },
                            "version": {"S": "8.3"},
                            "type_": {"S": "java-archive"},
                            "found_by": {"S": "java-parse-gradle-lock"},
                            "sk_2": {"S": "PKGS#PRESENT#false#VULNERABLE#false#"},
                        },
                        "SequenceNumber": "000000000000000001445",
                        "SizeBytes": 1795,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
                {
                    "eventID": "7e7a6a92-04e2-46fb-90c9-6a6a3368026b",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1713373740000,
                        "Keys": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk": {
                                "S": "PKGS#ROOT#4039d098-ffc5-4984-8ed3"
                                "-eb17bca98e19#PACKAGE#commons-io:commons"
                                "-io#VERSION#2.7",
                            },
                        },
                        "NewImage": {
                            "group_name": {"S": "unittesting"},
                            "name": {"S": "commons-io:commons-io"},
                            "sk": {
                                "S": "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bc"
                                "a98e19#PACKAGE#commons-io:commons"
                                "-io#VERSION#2.7",
                            },
                            "pk_2": {"S": "GROUP#unittesting"},
                            "root_id": {"S": "4039d098-ffc5-4984-8ed3-eb17bca98e19"},
                            "pk": {"S": "GROUP#unittesting"},
                            "package_url": {"S": "pkg:maven/commons-io/commons-io@2.7"},
                            "language": {"S": "java"},
                            "platform": {"S": "MAVEN"},
                            "state": {
                                "M": {
                                    "vulnerable": {"BOOL": False},
                                    "modified_by": {"S": "integrates@fluidattacks.com"},
                                    "be_present": {"BOOL": True},
                                    "id": {"S": "f75fd9178b7945bf"},
                                    "modified_date": {"S": "2024-01-05T14:38:51.241508+00:00"},
                                    "url": {
                                        "S": "https://repo1.maven.org/maven2"
                                        "/commons-io/commons-io/2.7/",
                                    },
                                    "locations": {
                                        "L": [
                                            {
                                                "M": {
                                                    "path": {"S": "build.gradle"},
                                                    "line": {"S": "88"},
                                                },
                                            },
                                        ],
                                    },
                                    "licenses": {"L": [{"S": "MIT"}]},
                                    "health_metadata": {
                                        "M": {
                                            "latest_version": {"S": "15.0"},
                                            "author": {"S": "author"},
                                            "repo_url": {"S": ""},
                                        },
                                    },
                                },
                            },
                            "version": {"S": "2.7"},
                            "type_": {"S": "java-archive"},
                            "found_by": {"S": "java-parse-gradle-lock"},
                            "sk_2": {"S": "PKGS#PRESENT#true#VULNERABLE#false#"},
                        },
                        "SequenceNumber": "000000000000000001516",
                        "SizeBytes": 1553,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
            [
                {
                    "sk": "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
                    "#PACKAGE#commons-io:commons-io#VERSION#2.7",
                    "pk": "GROUP#unittesting",
                },
                {
                    "sk": "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
                    "#PACKAGE#com.nimbusds:nimbus-jose-jwt#VERSION#8.3",
                    "pk": "GROUP#unittesting",
                },
            ],
        ],
    ],
)
def test_streams_process_pkgs(
    input_data: list[dict[str, Any]],
    output_data: list[dict[str, Any]],
) -> None:
    search_result = search(SearchParams(index_value="pkgs_index", limit=10))

    assert search_result.total == 0

    process_pkgs({"Records": input_data}, LambdaContext())
    time.sleep(5)

    search_result = search(SearchParams(index_value="pkgs_index", limit=10))

    assert search_result.total == 2

    items = search_result.items

    for idx, item in enumerate(items):
        assert item["pk"] == output_data[idx]["pk"]
        assert item["sk"] == output_data[idx]["sk"]
