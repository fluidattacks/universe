from collections.abc import Generator

import boto3
import fluidattacks_core.testing as ftest
from fluidattacks_core.testing.fakers.db import (
    add_finding_data,
)
from fluidattacks_core.testing.types import (
    DynamoDBResource,
    DynamoDBTable,
    FunctionFixture,
    Patch,
)

import streams.resource


@ftest.injectable
def prepare_data(
    patch_table: FunctionFixture[Patch],
) -> Generator[DynamoDBTable, None, None]:
    with patch_table(streams.resource, "TABLE_RESOURCE", "integrates_vms") as table:
        db_resource: DynamoDBResource = boto3.resource("dynamodb")
        add_finding_data(db_resource.Table("integrates_vms"))
        yield table
