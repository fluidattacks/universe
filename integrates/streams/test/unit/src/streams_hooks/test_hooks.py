import requests
from fluidattacks_core.testing import (
    tag,
)
from fluidattacks_core.testing.constants import (
    FINDING_ID,
    GROUP_NAME,
)
from fluidattacks_core.testing.fakers import (
    fake_vulnerability,
    get_streams_records,
)
from fluidattacks_core.testing.types import (
    DynamoDBTable,
    MockedInstance,
    MockingFunction,
)

from streams.hooks.notifier import (
    notify_poc,
)
from streams.hooks.types import (
    HookEvent,
)
from streams.hooks.vulnerabilities import (
    notify_created,
    notify_deleted,
    notify_updated,
)
from streams.utils import (
    format_record,
)

from .conftest import (
    format_expression_names,
    format_expression_values,
    format_set_expression,
)


@tag.streams_hooks
def test_notify_poc_acts_under_poc_groups(
    prepare_data: DynamoDBTable,
    mocking: MockingFunction,
) -> None:
    assert prepare_data
    mock_post = mocking(requests, "post", MockedInstance(status_code=200))

    notify_poc(
        event=HookEvent.REPORTED_VULNERABILITY,
        group_name="group1",
        vuln_id="1",
    )

    calls = mock_post.calls()
    assert len(calls) == 1
    assert calls[0][1]["json"]["group"] == "group1"


@tag.streams_hooks
def test_notify_poc_does_not_act_to_other_groups(
    prepare_data: DynamoDBTable,
    mocking: MockingFunction,
) -> None:
    assert prepare_data
    mock_post = mocking(requests, "post", MockedInstance(status_code=200))

    notify_poc(
        event=HookEvent.REPORTED_VULNERABILITY,
        group_name="group3",
        vuln_id="1",
    )

    calls = mock_post.calls()
    assert len(calls) == 0


@tag.streams_hooks
def test_notify_created_hook(prepare_data_2: DynamoDBTable, mocking: MockingFunction) -> None:
    table = prepare_data_2
    mock_post = mocking(requests, "post", MockedInstance(status_code=200))
    vuln = fake_vulnerability(
        vuln_id="fb454bdb-f339-4586-87bd-c63fe0a7ccf6",
        state="VULNERABLE",
        treatment_status="UNTREATED",
        bug_tracking_system_url="https://site.atlassian.net/IGTES-23",
        webhook_url="https://id.hello.atlassian-dev.net/x1/id",
    )
    vuln_url = f"https://app.fluidattacks.com/groups/{GROUP_NAME}/vulns/{FINDING_ID}/locations"
    table.put_item(Item=vuln)
    records = tuple(format_record(record) for record in get_streams_records())
    records = records[6:]

    notify_created(GROUP_NAME, records)

    post_calls = mock_post.calls()
    assert len(post_calls) == 1
    url = post_calls[0].args[0]
    text = post_calls[0].kwargs["json"]["text"]
    assert url == "test"  # google chat url
    assert vuln_url in text


@tag.streams_hooks
def test_notify_updated_hook(prepare_data_2: DynamoDBTable, mocking: MockingFunction) -> None:
    table = prepare_data_2
    mock_post = mocking(requests, "post", MockedInstance(status_code=200))
    # update VULNERABLE to SAFE
    to_update = {"state": {"status": "SAFE"}}
    table.update_item(
        Key={
            "pk": "VULN#fb454bdb-f339-4586-87bd-c63fe0a7ccf6",
            "sk": f"FIN#{FINDING_ID}",
        },
        ExpressionAttributeNames=format_expression_names(to_update),
        ExpressionAttributeValues=format_expression_values(to_update),
        UpdateExpression=format_set_expression(to_update),
    )
    records = tuple(format_record(record) for record in get_streams_records())
    records = records[6:]

    notify_updated(GROUP_NAME, records)

    post_calls = mock_post.calls()
    assert len(post_calls) == 1
    url = post_calls[0].args[0]
    action = post_calls[0].kwargs["json"]["payload"]["action"]
    assert url == "https://id.hello.atlassian-dev.net/x1/id"
    assert action == "CHANGED_TO_SAFE"


@tag.streams_hooks
def test_notify_deleted_hook(prepare_data_2: DynamoDBTable, mocking: MockingFunction) -> None:
    table = prepare_data_2
    mock_post = mocking(requests, "post", MockedInstance(status_code=200))
    # update VULNERABLE to SAFE
    table.delete_item(
        Key={
            "pk": "VULN#fb454bdb-f339-4586-87bd-c63fe0a7ccf6",
            "sk": f"FIN#{FINDING_ID}",
        },
    )
    records = tuple(format_record(record) for record in get_streams_records())
    records = records[6:]

    notify_deleted(GROUP_NAME, records)

    post_calls = mock_post.calls()
    assert len(post_calls) == 1
    url = post_calls[0].args[0]
    action = post_calls[0].kwargs["json"]["payload"]["action"]
    assert url == "https://id.hello.atlassian-dev.net/x1/id"
    assert action == "WAS_REMOVED"
