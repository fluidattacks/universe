{ makeScript, managePorts, outputs, projectPath, ... }:
makeScript {
  name = "streams-unit-test";
  entrypoint = ./entrypoint.sh;
  replace = { __argSecretsDev__ = projectPath "/integrates/secrets/dev.yaml"; };
  searchPaths = {
    source = [
      managePorts
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/integrates/streams/env"
    ];
  };
}
