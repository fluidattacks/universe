import re
from itertools import (
    chain,
)
from typing import (
    Any,
)

import bugsnag
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
)

from streams.logger import (
    StreamsLogger,
)
from streams.resource import (
    HISTORIC_TABLE,
)
from streams.types import (
    Record,
    StreamEvent,
)
from streams.utils import (
    format_record,
)

LOGGER = StreamsLogger("streams.historic")
LOGGER.set_handler("🔖")


def get_entity_historic(
    *,
    pk: str,
    sk: str,
    state_name: str,
) -> list[dict]:
    query_args = {
        "KeyConditionExpression": (
            Key("pk").eq(f"{pk}#{sk}") & Key("sk").begins_with(f"STATE#{state_name}#")
        ),
    }
    response = HISTORIC_TABLE.query(**query_args)  # type: ignore[arg-type]
    items: list[dict] = response.get("Items", [])
    while response.get("LastEvaluatedKey"):
        response = HISTORIC_TABLE.query(
            **query_args,  # type: ignore[arg-type]
            ExclusiveStartKey=response.get("LastEvaluatedKey"),  # type: ignore[arg-type]
        )
        items += response.get("Items", [])
    return items


def batch_put_item(items: tuple[dict, ...]) -> None:
    try:
        with HISTORIC_TABLE.batch_writer() as batch_writer:
            for item in items:
                batch_writer.put_item(Item=item)
    except ClientError as error:
        LOGGER.error(
            error,
            extra={
                "extra": {
                    "operation": "dynamodb_batch_put_item",
                    "item_ids": ["#".join([item["pk"], item["sk"]]) for item in items],
                },
            },
        )


def _get_group_name_from_key(key_parts: list[str]) -> str | None:
    try:
        group_prefix_index = key_parts.index("GROUP")
    except ValueError:
        return None

    try:
        return key_parts[group_prefix_index + 1]
    except IndexError:
        return None


def _get_pk2(key_parts: list[str], group_name: str) -> str:
    parts_with_upper_values = {"FILENAME", "PACKAGE_NAME", "ENTRYPOINT"}
    pk2: list[str] = []
    continue_next = False
    key_pattern = re.compile("^[A-Z_]+$")
    for part in key_parts:
        if continue_next:
            continue_next = False
            continue

        if part in parts_with_upper_values:
            continue_next = True

        if key_pattern.match(part):
            pk2.append(part)

    pk2.append(group_name)
    return "#".join(pk2)


def _get_pk3(group_name: str) -> str:
    return "#".join(["GROUP", group_name])


def _get_historic_items_to_put_by_record(record: Record) -> list[dict]:
    if (
        record.new_image is None
        or record.sk.startswith("STATE#")
        or record.sk.startswith("TREATMENT#")
        or record.event_name not in {StreamEvent.INSERT, StreamEvent.MODIFY}
    ):
        return []
    items: list[dict] = []
    for key, value in record.new_image.items():
        state_name = _get_state_name(key, value)
        if not state_name:
            continue
        if (
            record.old_image is not None
            and isinstance(record.old_image.get(state_name), dict)
            and record.old_image[state_name] == record.new_image[state_name]
        ):
            continue

        gsi_keys = {}
        item_pk = f"{record.pk}#{record.sk}"
        item_sk = f"STATE#{state_name}#DATE#{record.new_image[state_name]['modified_date']}"
        key_parts = item_pk.split("#")
        group_name_from_key = record.new_image.get("group_name") or _get_group_name_from_key(
            key_parts,
        )
        if group_name_from_key:
            gsi_keys = {
                "pk_2": _get_pk2(key_parts, group_name_from_key),
                "sk_2": item_sk,
                "pk_3": _get_pk3(group_name_from_key),
                "sk_3": item_sk,
            }
        items.append(
            {
                "pk": item_pk,
                "sk": item_sk,
                **gsi_keys,
                **record.new_image[state_name],
            },
        )

    return items


def _get_state_name(key: str, value: Any) -> str | None:
    state_name = key if isinstance(value, dict) and "modified_date" in value else None

    return state_name


def _deduplicate_items(items: tuple[dict, ...]) -> tuple[dict, ...]:
    return tuple({f"{item['pk']}#{item['sk']}": item for item in items}.values())


@bugsnag.aws_lambda_handler
def process_historic(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    items_to_put = _deduplicate_items(
        tuple(chain.from_iterable(map(_get_historic_items_to_put_by_record, records))),
    )
    batch_put_item(items_to_put)
