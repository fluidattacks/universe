import logging


class StreamsLogger(logging.Logger):
    name: str
    logger: logging.Logger

    def __init__(self, name: str, level: int = logging.INFO):
        super().__init__(name, level)

    def set_handler(self, identifier: str) -> None:
        if self.handlers:
            return

        handler = logging.StreamHandler()
        handler.name = self.name
        formatter = logging.Formatter(f"%(asctime)s %(levelname)s\t{identifier} - %(message)s")
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        self.addHandler(handler)
