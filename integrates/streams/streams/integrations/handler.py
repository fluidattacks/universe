import datetime
import json
import logging
from typing import (
    Any,
)

import bugsnag
import requests
from atlassian_jwt import (
    encode_token,
)
from boto3.dynamodb.conditions import (
    Key,
)

import streams.resource as resources
from streams.types import Record
from streams.utils import (
    format_record,
)

LOGGER = logging.getLogger()


@bugsnag.aws_lambda_handler
def process_vulns(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])

    for record in records:
        if record.new_image is None:
            return
        if record.old_image is not None and (
            record.old_image["state"]["status"] == "VULNERABLE"
            and record.new_image["state"]["status"] != "VULNERABLE"
        ):
            jira_delete_vulnerability(record)
        else:
            jira_update_vulnerability(record)


def jira_delete_vulnerability(record: Record) -> None:
    new_image = record.new_image
    if new_image is None:
        return
    group = _get_group(new_image["group_name"])
    org = _get_org(group["organization_id"])
    org_jira_id = org.get("jira_associated_id")
    if not org_jira_id:
        LOGGER.warning("Organization %s does not have an associated Jira ID", org["name"])
        return

    client_key, site = parse_jira_details(org_jira_id)

    jira_installation = _get_jira_installation(client_key, site)
    vuln_id = new_image["pk"].split("#")[1]

    if not _check_vuln_exists(vuln_id, jira_installation):
        return

    path = f"/rest/security/1.0/vulnerability/{vuln_id}"
    url = f"{jira_installation['base_url']}{path}"
    token: str = encode_token(
        http_method="DELETE",
        url=path,
        clientKey=jira_installation["key"],
        sharedSecret=jira_installation["shared_secret"],
    )

    response = requests.request(
        method="DELETE",
        url=url,
        headers={
            "Authorization": f"JWT {token}",
        },
        timeout=50,
    )
    LOGGER.info("Deleting vuln in jira, Response: %s", response.status_code)


def jira_update_vulnerability(record: Record) -> None:
    # pylint: disable-msg=too-many-locals
    new_image = record.new_image
    if new_image is None:
        return
    if new_image["state"]["status"] != "VULNERABLE":
        return
    group = _get_group(new_image["group_name"])
    org = _get_org(group["organization_id"])
    org_jira_id = org.get("jira_associated_id")
    if not org_jira_id:
        LOGGER.warning("Organization %s does not have an associated Jira ID", org["name"])
        return
    finding_id = new_image["sk"].split("#")[1]

    finding = _get_finding(finding_id=finding_id)

    client_key, site = parse_jira_details(org_jira_id)

    jira_installation = _get_jira_installation(client_key, site)

    vuln_id = new_image["pk"].split("#")[1]

    if not _check_vuln_exists(vuln_id, jira_installation):
        return

    path = "/rest/security/1.0/linkedWorkspaces/bulk"
    url = f"{jira_installation['base_url']}{path}"
    token: str = encode_token(
        http_method="POST",
        url=path,
        clientKey=jira_installation["key"],
        sharedSecret=jira_installation["shared_secret"],
    )

    payload = json.dumps(
        {
            "vulnerabilities": [
                {
                    "schemaVersion": "1.0",
                    "id": vuln_id,
                    "updateSequenceNumber": (int(datetime.datetime.now().timestamp())),
                    "containerId": new_image["group_name"],
                    "displayName": (f'{finding["title"]} - {new_image["state"]["where"]}'),
                    "description": (f"Where: {new_image['state']['where']}"),
                    "url": (
                        f'https://app.fluidattacks.com/orgs/{org["name"]}'
                        '/groups/'
                        f'{group["name"]}/vulns/{finding_id}/'
                        f'locations/{vuln_id}'
                    ),
                    "type": (get_type_from_technique(new_image["technique"])),
                    "introducedDate": format_date_string(new_image["created_date"]),
                    "lastUpdated": format_date_string(datetime.datetime.now().isoformat()),
                    "severity": {
                        "level": get_severity_text_value(finding["severity_score"]["threat_score"]),
                    },
                    "status": "open",
                    "additionalInfo": {
                        "content": new_image["state"]["where"],
                        "url": (
                            'https://app.fluidattacks.com/'
                            f'orgs/{org["name"]}/groups/'
                            f'{group["name"]}/vulns/{finding_id}/'
                            f'locations/{vuln_id}'
                        ),
                    },
                },
            ],
        },
    )
    response = requests.request(
        method="POST",
        url=url,
        data=payload,
        headers={
            "Authorization": f"JWT {token}",
            "Content-Type": "application/json",
        },
        timeout=50,
    )
    LOGGER.info("Updating vuln in jira, Response: %s", response.status_code)


def _check_vuln_exists(vuln_id: str, jira_installation: dict) -> bool:
    path = f"/rest/security/1.0/vulnerability/{vuln_id}"
    url = f"{jira_installation['base_url']}{path}"
    token: str = encode_token(
        http_method="GET",
        url=path,
        clientKey=jira_installation["key"],
        sharedSecret=jira_installation["shared_secret"],
    )

    response = requests.request(
        method="GET",
        url=url,
        headers={
            "Authorization": f"JWT {token}",
            "Accept": "application/json",
        },
        timeout=50,
    )
    return response.status_code == 200


def _get_group(group_name: str) -> dict:
    response = resources.TABLE_RESOURCE.query(
        KeyConditionExpression=(
            Key("pk").eq(f"GROUP#{group_name}") & Key("sk").begins_with("ORG#")
        ),
        Limit=1,
    )
    return response["Items"][0]


def _get_org(org_id: str) -> dict:
    response = resources.TABLE_RESOURCE.query(
        KeyConditionExpression=(Key("pk").eq(f"ORG#{org_id}") & Key("sk").begins_with("ORG#")),
        Limit=1,
    )
    return response["Items"][0]


def _get_jira_installation(client_key: str, site: str) -> dict:
    response = resources.TABLE_RESOURCE.query(
        KeyConditionExpression=(
            Key("pk").eq(f"CLIENT_KEY#{client_key}") & Key("sk").eq(f"SITE#{site}")
        ),
        Limit=1,
    )
    return response["Items"][0]


def _get_finding(finding_id: str) -> dict:
    response = resources.TABLE_RESOURCE.query(
        KeyConditionExpression=(
            Key("pk").eq(f"FIN#{finding_id}") & Key("sk").begins_with("GROUP#")
        ),
        Limit=1,
    )
    return response["Items"][0]


def parse_jira_details(org_jira_id: str) -> tuple[str, str]:
    parts = org_jira_id.split("#")
    return parts[1][:-4], parts[2]


def get_type_from_technique(technique: str) -> str:
    if technique == "SAST":
        return "sast"
    if technique == "DAST":
        return "dast"
    if technique == "SCA":
        return "sca"
    return "unknown"


def format_date_string(date_str: str) -> str:
    date_obj = datetime.datetime.fromisoformat(date_str)

    return date_obj.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"


def get_severity_text_value(score: int) -> str:
    if score >= 9:
        return "critical"
    if score > 6.9:
        return "high"
    if score > 3.9:
        return "medium"
    if score >= 0.1:
        return "low"
    return "unknown"
