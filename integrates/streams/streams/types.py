from enum import (
    Enum,
)
from typing import (
    Any,
    Literal,
    NamedTuple,
    NotRequired,
    TypedDict,
)

Item = dict[str, Any]

EventType = Literal["INSERT", "MODIFY", "REMOVE"]
RecordType = Literal["NEW_IMAGE", "OLD_IMAGE", "NEW_AND_OLD_IMAGES", "KEYS_ONLY"]
VulnerabilityStatusType = Literal["SAFE", "VULNERABLE", "SUBMITTED", "REJECTED"]
TreatmentStatusType = Literal["UNTREATED", "IN_PROGRESS", "ACCEPTED", "ACCEPTED_UNDEFINED"]
SeverityLevelType = Literal["low", "medium", "high", "critical"]


class StreamEvent(str, Enum):
    INSERT = "INSERT"
    MODIFY = "MODIFY"
    REMOVE = "REMOVE"


class Record(NamedTuple):
    event_name: StreamEvent
    new_image: dict[str, Any] | None
    old_image: dict[str, Any] | None
    pk: str
    sequence_number: str
    sk: str


class AttributeValue(TypedDict, total=False):
    S: str
    N: str
    B: bytes
    SS: list[str]
    NS: list[str]
    BS: list[bytes]
    M: dict[str, "AttributeValue"]
    L: list["AttributeValue"]
    NULL: bool
    BOOL: bool


class DDBKey(TypedDict):
    sk: AttributeValue
    pk: AttributeValue


class DDBRecordPayload(TypedDict):
    """Raw DynamoDB Record Payload"""

    StreamViewType: RecordType
    SizeBytes: int
    ApproximateCreationDateTime: int
    SequenceNumber: str
    Keys: DDBKey
    OldImage: NotRequired[dict[str, AttributeValue]]
    NewImage: NotRequired[dict[str, AttributeValue]]


class DDBRecord(TypedDict):
    """Raw DynamoDB Record"""

    eventID: str
    eventName: EventType
    eventVersion: str
    eventSource: str
    awsRegion: str
    dynamodb: DDBRecordPayload
