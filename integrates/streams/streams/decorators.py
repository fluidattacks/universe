import contextlib
import functools
import time
from collections.abc import (
    Callable,
)
from typing import (
    Any,
    TypeVar,
    cast,
)

T = TypeVar("T")


def retry_on_exceptions(
    *,
    exceptions: tuple[type[Exception], ...],
    max_attempts: int = 5,
    sleep_seconds: float = 0,
) -> Callable[[T], T]:
    def decorator(func: T) -> T:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            for _ in range(max_attempts - 1):
                with contextlib.suppress(*exceptions):
                    return _func(*args, **kwargs)

                time.sleep(sleep_seconds)

            return _func(*args, **kwargs)

        return cast(T, wrapper)

    return decorator
