from typing import Any

from boto3.dynamodb.conditions import Attr, AttributeBase, ConditionBase, Key
from botocore.exceptions import ClientError

import streams.resource as resources
from streams.indicators.custom_exceptions import FindingNotFound
from streams.types import Item

from .utils import (
    LOGGER,
    format_indicators,
    format_to_expression_attributes_names,
    format_to_expression_attributes_values,
    format_to_project_expression,
    format_to_update_expression,
)


def get_finding(*, pk: str, sk: str) -> Item:
    """Using exact pk and sk, returns the finding item from dynamodb."""
    args = set(
        [
            "pk",
            "sk",
            "state",
            "unreliable_indicators",
            "group_name",
            "severity_score",
        ],
    )
    query_args = {
        "ExpressionAttributeNames": format_to_expression_attributes_names(args),
        "Key": {
            "pk": pk,
            "sk": sk,
        },
        "ProjectionExpression": format_to_project_expression(args),
    }
    response = resources.TABLE_RESOURCE.get_item(**query_args)  # type: ignore[arg-type]
    item: Item = response.get("Item", {})
    return item


def get_root(*, pk: str, sk: str) -> Item:
    """Using exact pk and sk, returns the root item from dynamodb."""
    args = set(
        [
            "pk",
            "sk",
            "state",
            "unreliable_indicators",
            # Other columns from root...
        ],
    )
    query_args = {
        "ExpressionAttributeNames": format_to_expression_attributes_names(args),
        "Key": {
            "pk": pk,
            "sk": sk,
        },
        "ProjectionExpression": format_to_project_expression(args),
    }
    response = resources.TABLE_RESOURCE.get_item(**query_args)  # type: ignore[arg-type]
    item: Item = response.get("Item", {})
    return item


def get_vulnerabilities_by_finding(
    *,
    finding_id: str,
    state: str,
    zero_risk: bool = False,
) -> list[Item]:
    """
    using exact finding_id, returns vulnerabilities
    in finding from dynamodb.
    """
    args = set(
        [
            "pk",
            "sk",
            "created_date",
            "group_name",
            "state",
            "treatment",
            "verification",
            "zero_risk",
            "severity_score",
            "unreliable_indicators",
        ],
    )

    released_states = ["SAFE", "VULNERABLE"]
    released = str(state in released_states)
    haz_zr = str(zero_risk).lower()

    begin_sk = f"VULN#DELETED#false#RELEASED#{released.lower()}#ZR#{haz_zr}#STATE#{state.lower()}#"

    query_args = {
        "ExpressionAttributeNames": format_to_expression_attributes_names(args),
        "IndexName": "gsi_6",
        "KeyConditionExpression": (Key("pk_6").eq(finding_id) & Key("sk_6").begins_with(begin_sk)),
        "ProjectionExpression": format_to_project_expression(args),
        "Limit": 1000,
    }

    response = resources.TABLE_RESOURCE.query(**query_args)  # type: ignore[arg-type]
    items: list[Item] = response.get("Items", [])
    start_key: object | None = response.get("LastEvaluatedKey", None)

    while start_key is not None:
        response = resources.TABLE_RESOURCE.query(
            **query_args,  # type: ignore[arg-type]
            ExclusiveStartKey=start_key,  # type: ignore[arg-type]
        )
        items.extend(response.get("Items", []))
        start_key = response.get("LastEvaluatedKey", None)

    return items


def get_vulnerabilities_by_group(*, group_id: str, state: str) -> list[Item]:
    """
    Using exact group_id, returns vulnerabilities
    in group from dynamodb.
    """
    args = set(
        [
            "pk",
            "sk",
            "group_name",
            "state",
            "zero_risk",
            "unreliable_indicators",
        ],
    )

    begin_sk = f"VULN#ZR#false#STATE#{state.lower()}#"

    query_args = {
        "ExpressionAttributeNames": format_to_expression_attributes_names(args),
        "IndexName": "gsi_5",
        "KeyConditionExpression": (Key("pk_5").eq(group_id) & Key("sk_5").begins_with(begin_sk)),
        "ProjectionExpression": format_to_project_expression(args),
        "Limit": 1000,
    }

    response = resources.TABLE_RESOURCE.query(**query_args)  # type: ignore[arg-type]
    items: list[Item] = response.get("Items", [])
    start_key: object | None = response.get("LastEvaluatedKey", None)

    while start_key is not None:
        response = resources.TABLE_RESOURCE.query(
            **query_args,  # type: ignore[arg-type]
            ExclusiveStartKey=start_key,  # type: ignore[arg-type]
        )
        items.extend(response.get("Items", []))
        start_key = response.get("LastEvaluatedKey", None)

    return items


def get_released_nzr_vulns_by_finding(*, finding_id: str, zero_risk: bool = False) -> list[Item]:
    """
    Filter the released and non-zero risk vulnerabilities by `finding_id`.

    Released vulnerabilities have `SAFE` and `VULNERABLE` status
    (these are displayed to the user).

    Non-zero risk vulnerabilities are those that they are safe
    or they are vulnerable with zero risk status, but
    they are not `CONFIRMED` or `REQUESTED` for zero risk.
    """
    vulns = []
    vulns.extend(
        get_vulnerabilities_by_finding(
            finding_id=finding_id,
            state="VULNERABLE",
            zero_risk=zero_risk,
        ),
    )
    vulns.extend(
        get_vulnerabilities_by_finding(finding_id=finding_id, state="SAFE", zero_risk=zero_risk),
    )
    return vulns


def _print_indicators_summary(
    *,
    process_id: str,
    condition_expression: ConditionBase,
    pk: str,
    sk: str,
    db_indicators: Item,
    current_indicators: Item,
    new_indicators: Item,
) -> str:
    if not condition_expression:
        return ""
    key_t, value = _print_conditions(condition_expression)[-1]

    split_key = key_t.split(".")[1]
    data = (
        {
            "pk|sk": f"{pk}|{sk}",
            "db": (db_indicators[split_key] if split_key in db_indicators else None),
            "current_indicators": (
                current_indicators[key_t] if key_t in current_indicators else None
            ),
            "new_indicators": (new_indicators[key_t] if key_t in new_indicators else None),
        },
    )

    return f"[{process_id}] ConditionalCheckFailedException: {key_t} = {value} | {data}"


def _print_conditions(condition_expression: ConditionBase) -> list[Any]:
    return [
        value.name
        if isinstance(value, AttributeBase)
        else _print_condition_list(value)
        if isinstance(value, ConditionBase)
        else value
        for value in condition_expression.get_expression()["values"]
    ]


def _print_condition_list(conditions: ConditionBase) -> list[str]:
    return [
        value.name if isinstance(value, AttributeBase) else value
        for value in conditions.get_expression()["values"]
    ]


def was_updated_previously(db_indicators: Item, new_indicators: Item) -> bool:
    """Check if database indicators are the same as the new indicators."""
    return all(
        db_indicators[new_key] == new_indicators[new_key]
        for new_key in new_indicators
        if new_key in db_indicators
    )


def update_finding_indicators(
    *,
    process_id: str,
    finding: Item,
    current_indicators: Item,
    new_indicators: Item,
) -> None:
    pk_value = finding["pk"]
    sk_value = finding["sk"]
    current_indicators = format_indicators(current_indicators)
    new_indicators = format_indicators(new_indicators)

    ignored_fields = [
        "unreliable_indicators.zero_risk_summary",
    ]

    # Optimistic locking to prevent overwriting
    # if something else changed it first.
    condition_expression: ConditionBase = Attr("pk").exists()
    for key in current_indicators.keys():
        if key in ignored_fields:
            continue

        condition_expression &= Attr(key).eq(current_indicators[key])

    try:
        db_finding = get_finding(pk=pk_value, sk=sk_value)

        if not db_finding:
            raise FindingNotFound()

        resources.TABLE_RESOURCE.update_item(
            ConditionExpression=condition_expression,
            ExpressionAttributeValues=format_to_expression_attributes_values(new_indicators),
            Key={"pk": pk_value, "sk": sk_value},
            UpdateExpression=format_to_update_expression(new_indicators),
        )
        LOGGER.info("[%s] Finding updated: (%s, %s)", process_id, pk_value, sk_value)
    except FindingNotFound:
        LOGGER.info(
            "[%s] FindingNotFound: Fail trying to update %s",
            process_id,
            {
                "pk|sk": f"{pk_value}|{sk_value}",
            },
        )
    except ClientError as ex:
        if ex.response["Error"]["Code"] == "ConditionalCheckFailedException":
            db_indicators = db_finding.get("unreliable_indicators", {})

            if was_updated_previously(db_indicators, new_indicators):
                LOGGER.warning(
                    "[%s] FindingUpdatedPreviously: (%s, %s)",
                    process_id,
                    pk_value,
                    sk_value,
                )
                return

            raise ClientError(
                {
                    "Error": {
                        "Code": "ConditionalCheckFailedException",
                        "Message": _print_indicators_summary(
                            process_id=process_id,
                            condition_expression=condition_expression,
                            pk=pk_value,
                            sk=sk_value,
                            db_indicators=db_indicators,
                            current_indicators=current_indicators,
                            new_indicators=new_indicators,
                        ),
                    },
                },
                "Update indicators",
            ) from ex

        raise ex
