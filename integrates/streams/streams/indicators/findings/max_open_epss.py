from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


@handle_exceptions
def new_update(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
) -> None:
    """
    Updates max open EPSS (Exploit Prediction Scoring System)
    in `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.

    """
    released_vulns = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]
    scores = [
        int(x["state"]["advisories"]["epss"])
        if "advisories" in x["state"] and "epss" in x["state"]["advisories"]
        else 0
        for x in released_vulns
    ]

    new_indicators[FindingIndicators.EPSS] = max(scores) if len(scores) > 0 else 0
