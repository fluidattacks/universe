from collections import (
    Counter,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
)

from streams.indicators.operations import (
    get_vulnerabilities_by_finding,
)
from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    get_severity_level,
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


def get_vuln_severity(vuln: Any, finding: Any) -> str:
    score = (
        vuln["severity_score"].get("threat_score", "0")
        if "severity_score" in vuln
        else finding["severity_score"].get("threat_score", "0")
    )
    return get_severity_level(Decimal(score))


def get_vuln_severity_v3(vuln: Any, finding: Any) -> str:
    score = (
        vuln["severity_score"].get("temporal_score", "0")
        if "severity_score" in vuln
        else finding["severity_score"].get("temporal_score", "0")
    )
    return get_severity_level(Decimal(score))


@handle_exceptions
def update_all(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
    finding: Any,
) -> None:
    """
    Updates vulnerabilities summary in `new_indicators`.

    It updates the following indicators:
    - Total open vulnerabilities
    - Total closed vulnerabilities
    - Total submitted vulnerabilities
    - Total rejected vulnerabilities
    - Total open by severity level (critical, high, medium, low) in v3
    - Total open by severity level (critical, high, medium, low) in v4

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.
        finding: Finding metadata.

    """
    vulns_no_zrs = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]
    vulns_submitted = get_vulnerabilities_by_finding(finding_id=finding["pk"], state="SUBMITTED")
    vulns_rejected = get_vulnerabilities_by_finding(finding_id=finding["pk"], state="REJECTED")
    vulns_open = [
        vuln for vuln in vulns_no_zrs if "state" in vuln and vuln["state"]["status"] == "VULNERABLE"
    ]
    open_vulns_counter = Counter(get_vuln_severity(vuln, finding) for vuln in vulns_open)
    open_vulns_counter_v3 = Counter(get_vuln_severity_v3(vuln, finding) for vuln in vulns_open)
    vulns_counter = {
        **open_vulns_counter,
        "open": len(vulns_open),
        "closed": len(vulns_no_zrs) - len(vulns_open),
        "submitted": len(vulns_submitted),
        "rejected": len(vulns_rejected),
    }

    new_indicators[FindingIndicators.SUBMITTED_VULNERABILITIES] = vulns_counter["submitted"]
    new_indicators[FindingIndicators.REJECTED_VULNERABILITIES] = vulns_counter["rejected"]

    new_indicators[FindingIndicators.VULNERABILITIES_SUMMARY] = {
        "closed": vulns_counter["closed"],
        "open": vulns_counter["open"],
        "submitted": vulns_counter["submitted"],
        "rejected": vulns_counter["rejected"],
        "open_critical": vulns_counter.get("critical", 0),
        "open_high": vulns_counter.get("high", 0),
        "open_low": vulns_counter.get("low", 0),
        "open_medium": vulns_counter.get("medium", 0),
        "open_critical_v3": open_vulns_counter_v3.get("critical", 0),
        "open_high_v3": open_vulns_counter_v3.get("high", 0),
        "open_low_v3": open_vulns_counter_v3.get("low", 0),
        "open_medium_v3": open_vulns_counter_v3.get("medium", 0),
    }
