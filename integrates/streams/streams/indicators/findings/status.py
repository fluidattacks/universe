from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


@handle_exceptions
def new_update(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
) -> None:
    """
    Updates finding status in `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.

    """
    released_vulns = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]
    if len(released_vulns) == 0:
        new_indicators[FindingIndicators.STATUS] = "DRAFT"

    elif any(vuln for vuln in released_vulns if vuln["state"]["status"] == "VULNERABLE"):
        new_indicators[FindingIndicators.STATUS] = "VULNERABLE"

    else:
        new_indicators[FindingIndicators.STATUS] = "SAFE"
