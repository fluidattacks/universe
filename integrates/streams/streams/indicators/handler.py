import time
from typing import Any

import bugsnag
from botocore.exceptions import ClientError

from streams.utils import format_record

from .findings import max_open_epss as finding_epss
from .findings import max_open_root_criticality as finding_criticality
from .findings import max_open_severity_score as finding_severity
from .findings import release_date as finding_release_date
from .findings import status as finding_status
from .findings import total_open_cvssf as finding_open_cvssf
from .findings import total_open_priority as finding_open_priority
from .findings import treatment as finding_treatment
from .findings import verification as finding_verification
from .findings import vulnerabilities_summary as finding_vulnerabilities_summary
from .findings import zero_risk_summary as finding_zero_risk_summary
from .findings.types import FindingStore
from .operations import get_finding, get_root, update_finding_indicators
from .operations import get_released_nzr_vulns_by_finding as get_vulns
from .utils import LOGGER, get_process_id, log_record


def update_findings(store: FindingStore, process_id: str) -> None:
    for item in store.get_store():
        finding = item.finding
        new_indicators = item.indicators
        current_indicators = finding.get("unreliable_indicators", {})
        vulnerabilities = get_vulns(finding_id=finding["pk"]) if finding else []
        vulnerabilities_zr = get_vulns(finding_id=finding["pk"], zero_risk=True) if finding else []
        roots = item.roots

        if finding:
            finding_criticality.new_update(
                new_indicators=new_indicators,
                roots=roots,
            )
            finding_epss.new_update(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
            )
            finding_release_date.new_update(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
            )
            finding_severity.new_update(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
                finding=finding,
            )
            finding_status.new_update(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
            )

            finding_open_cvssf.new_update_all(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
                finding=finding,
            )
            finding_open_priority.new_update(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
            )
            finding_verification.new_update_all(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
            )
            finding_treatment.new_update_all(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
            )
            finding_vulnerabilities_summary.update_all(
                new_indicators=new_indicators,
                vulns=vulnerabilities,
                finding=finding,
            )
            finding_zero_risk_summary.new_update_all(
                new_indicators=new_indicators,
                vulns=vulnerabilities_zr,
            )

            store.update_by_key(f"{finding['pk']}|{finding['sk']}")

            update_finding_indicators(
                process_id=process_id,
                finding=finding,
                current_indicators=current_indicators,
                new_indicators=new_indicators,
            )


@bugsnag.aws_lambda_handler
def process(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    store = FindingStore(records, get_finding, get_root)
    process_id = get_process_id(records[0])
    start = time.time()

    log_record(records=records)

    try:
        update_findings(store, process_id)

        end = (time.time() - start) * 1000
        LOGGER.info("[%s] %.3f ms|Process finished", process_id, end)

    except ClientError as ex:
        if ex.response["Error"]["Code"] == "ConditionalCheckFailedException":
            LOGGER.warning("[%s] Retrying...", process_id)
            try:
                new_store = FindingStore(records, get_finding, get_root)
                update_findings(new_store, process_id)
            # pylint: disable-next=broad-except
            except Exception as exc:
                LOGGER.error(
                    "[%s] Update failed. Exception: %s",
                    process_id,
                    exc,
                )

        else:
            LOGGER.error(ex.response["Error"]["Message"])
