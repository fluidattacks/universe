from enum import (
    Enum,
)


class FindingIndicators(str, Enum):
    STATUS: str = "unreliable_status"
    EPSS: str = "max_open_epss"
    SEVERITY: str = "max_open_severity_score"
    SEVERITY_V4: str = "max_open_severity_score_v4"
    SUBMITTED_VULNERABILITIES: str = "submitted_vulnerabilities"
    REJECTED_VULNERABILITIES: str = "rejected_vulnerabilities"
    OLDEST_REPORT_DATE: str = "oldest_vulnerability_report_date"
    ROOT_CRITICALITY: str = "max_open_root_criticality"
    TREATMENT_SUMMARY: str = "treatment_summary"
    VERIFICATION_SUMMARY: str = "verification_summary"
    VULNERABILITIES_SUMMARY: str = "vulnerabilities_summary"
    ZERO_RISK_SUMMARY: str = "unreliable_zero_risk_summary"
    NEWEST_REPORT_DATE: str = "newest_vulnerability_report_date"
    TOTAL_OPEN_CVSSF: str = "total_open_cvssf"
    TOTAL_OPEN_CVSSF_V4: str = "total_open_cvssf_v4"
    TOTAL_OPEN_PRIORITY: str = "total_open_priority"

    def __str__(self) -> str:
        return self.value

    def __repr__(self) -> str:
        return self.value
