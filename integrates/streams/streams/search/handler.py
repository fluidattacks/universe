from datetime import (
    UTC,
    datetime,
)
from typing import (
    Any,
)

import bugsnag
from opensearchpy.helpers import (
    bulk,
)

from streams.opensearch import (
    CLIENT,
)
from streams.types import (
    Record,
    StreamEvent,
)
from streams.utils import (
    format_record,
)


def _process(records: tuple[Record, ...], index: str) -> None:
    """Replicates the item on AWS OpenSearch"""
    actions: list[dict[str, Any]] = []

    for record in records:
        action_name = "delete" if record.event_name == StreamEvent.REMOVE else "index"
        action = {
            # Max id length in OpenSearch is 512 (DocWriteRequest.java#L263)
            "_id": "#".join([record.pk, record.sk])[:512],
            "_index": index,
            "_op_type": action_name,
        }
        if action_name == "index" and record.new_image:
            actions.append({**action, "_source": record.new_image})
        else:
            actions.append(action)

    bulk(client=CLIENT, actions=actions, ignore_status=(404,))


def _format_vulns(records: tuple[Record, ...]) -> tuple[Record, ...]:
    formatted = []

    for record in records:
        if record.event_name in {StreamEvent.INSERT, StreamEvent.MODIFY} and record.new_image:
            # Needed as it doesn't fit in OpenSearch long data type (2^63)
            if "hash" in record.new_image:
                record.new_image["hash"] = str(record.new_image["hash"])
            # Needed as legacy ids were mapped to a numeric type, not uuids
            if "zero_risk" in record.new_image:
                record.new_image["zero_risk"]["comment_id"] = 0
        formatted.append(record)
    return tuple(formatted)


def _format_date(obj: dict, date_key: str) -> None:
    if date_key in obj and obj[date_key] is not None:
        obj[date_key] = datetime.fromisoformat(obj[date_key]).astimezone(tz=UTC).isoformat()


def _format_findings(records: tuple[Record, ...]) -> tuple[Record, ...]:
    formatted = []

    for record in records:
        ind_key = "unreliable_indicators"
        old_date_key = "oldest_vulnerability_report_date"
        new_date_key = "newest_vulnerability_report_date"

        if (
            record.event_name in {StreamEvent.INSERT, StreamEvent.MODIFY}
            and record.new_image
            and ind_key in record.new_image
        ):
            # Date must be in ISO format
            _format_date(record.new_image[ind_key], old_date_key)
            _format_date(record.new_image[ind_key], new_date_key)

        formatted.append(record)
    return tuple(formatted)


@bugsnag.aws_lambda_handler
def process_events(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    _process(records, "events_index")


@bugsnag.aws_lambda_handler
def process_executions(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    _process(records, "executions_index")


@bugsnag.aws_lambda_handler
def process_findings(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    formatted = _format_findings(records)
    _process(formatted, "findings_index")


@bugsnag.aws_lambda_handler
def process_inputs(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    _process(records, "inputs_index")


@bugsnag.aws_lambda_handler
def process_lines(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    _process(records, "lines_index")


@bugsnag.aws_lambda_handler
def process_packages(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    _process(records, "packages_index")


@bugsnag.aws_lambda_handler
def process_pkgs(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    _process(records, "pkgs_index")


@bugsnag.aws_lambda_handler
def process_roots(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    _process(records, "roots_index")


@bugsnag.aws_lambda_handler
def process_vulns(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])
    formatted = _format_vulns(records)
    _process(formatted, "vulns_index")
