from streams.hooks import (
    notifier,
)
from streams.logger import (
    StreamsLogger,
)
from streams.types import (
    Record,
    StreamEvent,
)

LOGGER = StreamsLogger("streams.hooks")
LOGGER.set_handler("🏹")


def _was_created(record: Record) -> bool:
    return bool(
        record.event_name == StreamEvent.INSERT
        and record.new_image
        and record.new_image.get("type", "") in ["Git", "IP", "URL"]
        and record.new_image["state"]["status"] == "ACTIVE",
    )


def notify_created(
    group_name: str,
    group_records: tuple[Record, ...],
) -> None:
    created_records = tuple(record for record in group_records if _was_created(record))
    if not created_records:
        return

    for record in created_records:
        root = record.new_image
        if not root:
            return
        notifier.notify_hooks(
            event="ROOT_CREATED",
            group_name=group_name,
            info={
                "root_id": root["pk"].lstrip("ROOT#"),
                "type": f'{root["type"]}Root',
            },
        )


def _was_disabled(record: Record) -> bool:
    return bool(
        record.old_image
        and "state" in record.old_image
        and record.old_image["state"]["status"] == "ACTIVE"
        and (
            record.event_name == StreamEvent.MODIFY
            and record.new_image
            and "state" in record.new_image
            and record.new_image["state"]["status"] == "INACTIVE"
        ),
    )


def notify_disabled(
    group_name: str,
    group_records: tuple[Record, ...],
) -> None:
    disabled_records = tuple(record for record in group_records if _was_disabled(record))
    if not disabled_records:
        return

    for record in disabled_records:
        root = record.new_image or record.old_image
        if not root:
            return
        notifier.notify_hooks(
            event="ROOT_DISABLED",
            group_name=group_name,
            info={
                "root_id": root["pk"].lstrip("ROOT#"),
            },
        )
