import logging
import uuid
from collections.abc import Callable
from concurrent.futures import (
    ThreadPoolExecutor,
)
from contextlib import (
    suppress,
)
from typing import (
    Any,
    ParamSpec,
    cast,
)

import requests
from azure.devops.connection import (
    Connection,
)
from azure.devops.credentials import (
    BasicTokenAuthentication,
)
from azure.devops.v7_1.work_item_tracking import (
    JsonPatchOperation,
    Wiql,
    WorkItem,
    WorkItemQueryResult,
    WorkItemReference,
    WorkItemTrackingClient,
)
from boto3.dynamodb.conditions import (
    Key,
)
from gitlab import (
    Gitlab,
    GitlabAuthenticationError,
)
from gitlab.v4.objects import (
    Project,
    ProjectIssue,
    ProjectLabel,
)

import streams.resource as resources
from streams.context import (
    FI_AZURE_OAUTH2_ISSUES_SECRET,
    FI_AZURE_OAUTH2_ISSUES_SECRET_OLD,
    FI_GITLAB_ISSUES_OAUTH2_APP_ID,
    FI_GITLAB_ISSUES_OAUTH2_SECRET,
    FI_GOOGLE_CHAT_WEBHOOK_URL,
    FI_WEBHOOK_POC_KEY,
    FI_WEBHOOK_POC_ORG,
    FI_WEBHOOK_POC_URL,
)
from streams.decorators import (
    retry_on_exceptions,
)
from streams.hooks.types import (
    HookEvent,
    IssueSubtask,
)

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

AZURE_MAX_TITLE_LENGTH = 256
GITLAB_MAX_TITLE_LENGTH = 255
NOTIFIER_TIMEOUT_SEC = 10

P = ParamSpec("P")


def resilient(func: Callable[P, None]) -> Callable[P, None]:
    def decorated(*args: P.args, **kwargs: P.kwargs) -> None:
        try:
            retry_on_exceptions(
                exceptions=(
                    GitlabAuthenticationError,
                    requests.exceptions.ReadTimeout,
                ),
                max_attempts=3,
                sleep_seconds=5,
            )(func)(*args, **kwargs)
        except Exception as exc:  # pylint:disable=broad-exception-caught
            LOGGER.exception(exc)

    return decorated


def _get_group(group_name: str) -> dict:
    response = resources.TABLE_RESOURCE.query(
        KeyConditionExpression=(
            Key("pk").eq(f"GROUP#{group_name}") & Key("sk").begins_with("ORG#")
        ),
        Limit=1,
    )
    return response["Items"][0]


def _get_gitlab_access_token(
    *,
    group_name: str,
    organization_id: str,
    redirect_uri: str,
    refresh_token: str,
) -> str:
    response: requests.Response = requests.post(
        "https://gitlab.com/oauth/token",
        json={
            "client_id": FI_GITLAB_ISSUES_OAUTH2_APP_ID,
            "client_secret": FI_GITLAB_ISSUES_OAUTH2_SECRET,
            "grant_type": "refresh_token",
            "redirect_uri": redirect_uri,
            "refresh_token": refresh_token,
        },
        timeout=NOTIFIER_TIMEOUT_SEC,
    )
    try:
        result = response.json()
        resources.TABLE_RESOURCE.update_item(
            ExpressionAttributeValues={":r": result["refresh_token"]},
            Key={"pk": f"GROUP#{group_name}", "sk": f"ORG#{organization_id}"},
            UpdateExpression="set gitlab_issues.refresh_token=:r",
        )
        return result["access_token"]
    except (KeyError, requests.exceptions.JSONDecodeError):
        LOGGER.info("_get_gitlab_access_token: %s", locals())
        raise


def _create_gitlab_task(
    *,
    assignee_ids: list[int],
    gitlab: Gitlab,
    label_ids: list[int],
    parent_issue: ProjectIssue,
    project: str,
    title: str,
) -> None:
    gitlab.http_post(
        "https://gitlab.com/api/graphql",
        post_data={
            "operationName": "createWorkItem",
            "query": """
                mutation createWorkItem($input: WorkItemCreateInput!) {
                    workItemCreate(input: $input) {
                        errors
                    }
                }
            """,
            "variables": {
                "input": {
                    "assigneesWidget": {
                        "assigneeIds": [
                            f"gid://gitlab/User/{assignee_id}" for assignee_id in assignee_ids
                        ],
                    },
                    "confidential": True,
                    "hierarchyWidget": {"parentId": f"gid://gitlab/WorkItem/{parent_issue.id}"},
                    "labelsWidget": {
                        "labelIds": [
                            f"gid://gitlab/ProjectLabel/{label_id}" for label_id in label_ids
                        ],
                    },
                    "namespacePath": project,
                    "title": title[:GITLAB_MAX_TITLE_LENGTH],
                    "workItemTypeId": "gid://gitlab/WorkItems::Type/5",
                },
            },
        },
    )


def _get_gitlab_matching_issue(
    project: Project,
    title: str,
) -> ProjectIssue | None:
    matching_issues = cast(
        list[ProjectIssue],
        project.issues.list(
            **{
                "in": "title",
                "issue_type": "issue",
                "scope": "all",
                "search": title,
                "state": "opened",
            },
        ),
    )
    return next(iter(matching_issues), None)


def _get_gitlab_label_ids(project: Project, labels: list[str]) -> list[int]:
    results = cast(list[ProjectLabel], project.labels.list(all=True))
    return [result.id for result in results if result.name in labels]


@resilient
def notify_gitlab(
    *,
    description: str,
    group_name: str,
    subtasks: tuple[IssueSubtask, ...],
    title: str,
) -> None:
    group = _get_group(group_name)
    settings = group.get("gitlab_issues")
    if (
        settings is None
        or settings.get("gitlab_project") is None
        or not settings.get("issue_automation_enabled")
    ):
        return
    LOGGER.info(
        "Notifying gitlab",
        extra={
            "extra": {
                "gitlab_project": settings["gitlab_project"],
                "group_name": group_name,
            },
        },
    )
    access_token = _get_gitlab_access_token(
        group_name=group["name"],
        organization_id=group["organization_id"],
        redirect_uri=settings["redirect_uri"],
        refresh_token=settings["refresh_token"],
    )
    with Gitlab(
        oauth_token=access_token,
        retry_transient_errors=True,
    ) as gitlab:
        project = gitlab.projects.get(settings["gitlab_project"])
        matching_issue = _get_gitlab_matching_issue(project, title)
        assignee_ids = [int(assignee_id) for assignee_id in settings.get("assignee_ids", [])]
        labels = settings.get("labels", [])
        parent_issue = matching_issue or cast(
            ProjectIssue,
            project.issues.create(
                {
                    "assignee_ids": assignee_ids,
                    "confidential": True,
                    "description": description,
                    "labels": ",".join(labels),
                    "title": title[:GITLAB_MAX_TITLE_LENGTH],
                },
            ),
        )
        LOGGER.info(
            "Using parent issue",
            extra={"id": parent_issue.iid, "url": parent_issue.web_url},
        )

        label_ids = _get_gitlab_label_ids(project, labels)
        for subtask in subtasks:
            _create_gitlab_task(
                assignee_ids=assignee_ids,
                gitlab=gitlab,
                label_ids=label_ids,
                parent_issue=parent_issue,
                project=settings["gitlab_project"],
                title=subtask["title"],
            )
            LOGGER.info("Created task")


def _request_azure_devops_token(redirect_uri: str, refresh_token: str, secret: str | None) -> str:
    data = {
        "client_assertion_type": ("urn:ietf:params:oauth:client-assertion-type:jwt-bearer"),
        "grant_type": "refresh_token",
        "client_assertion": secret,
        "assertion": refresh_token,
        "redirect_uri": redirect_uri,
    }
    response = requests.post(
        "https://app.vssps.visualstudio.com/oauth2/token",
        data=data,
        timeout=NOTIFIER_TIMEOUT_SEC,
    )
    if not response.ok:
        return ""
    result = response.json()
    return result["access_token"]


def _get_azure_devops_access_token(redirect_uri: str, refresh_token: str) -> str:
    new = _request_azure_devops_token(redirect_uri, refresh_token, FI_AZURE_OAUTH2_ISSUES_SECRET)
    if new:
        return new
    return _request_azure_devops_token(
        redirect_uri,
        refresh_token,
        FI_AZURE_OAUTH2_ISSUES_SECRET_OLD,
    )


def _get_azure_devops_matching_issue(
    client: WorkItemTrackingClient,
    azure_project: str,
    title: str,
) -> WorkItemReference | None:
    search_response: WorkItemQueryResult = client.query_by_wiql(
        wiql=Wiql(
            query=f"""
                SELECT [System.Id]
                FROM WorkItems
                WHERE [System.TeamProject] = '{azure_project}'
                AND [System.Title] CONTAINS '{title}'
            """,
        ),
        top=1,
    )
    matching_issues: list[WorkItemReference] = search_response.work_items
    return next(iter(matching_issues), None)


def _create_azure_devops_task(
    *,
    assigned_to: str,
    client: WorkItemTrackingClient,
    parent_issue: WorkItemReference | WorkItem,
    project: str,
    tags: str,
    title: str,
) -> WorkItem:
    return client.create_work_item(
        bypass_rules=True,
        document=[
            JsonPatchOperation(
                op="add",
                path="/fields/System.AssignedTo",
                value=assigned_to,
            ),
            JsonPatchOperation(
                op="add",
                path="/fields/System.Tags",
                value=tags,
            ),
            JsonPatchOperation(
                op="add",
                path="/fields/System.Title",
                value=title[:AZURE_MAX_TITLE_LENGTH],
            ),
            JsonPatchOperation(
                op="add",
                path="/relations/-",
                value={
                    "rel": "System.LinkTypes.Hierarchy-Reverse",
                    "url": parent_issue.url,
                },
            ),
        ],
        project=project,
        type="Task",
    )


@resilient
def notify_azure_devops(
    *,
    description: str,
    group_name: str,
    subtasks: tuple[IssueSubtask, ...],
    title: str,
) -> None:
    settings = _get_group(group_name).get("azure_issues")
    if (
        settings is None
        or settings.get("azure_project") is None
        or not settings.get("issue_automation_enabled")
    ):
        return
    access_token = _get_azure_devops_access_token(
        settings["redirect_uri"],
        settings["refresh_token"],
    )
    connection = Connection(
        base_url=f"https://dev.azure.com/{settings['azure_organization']}",
        creds=BasicTokenAuthentication({"access_token": access_token}),
    )
    client: WorkItemTrackingClient = connection.clients_v7_1.get_work_item_tracking_client()
    LOGGER.info(
        "Notifying azure",
        extra={
            "azure_organization": settings["azure_organization"],
            "azure_project": settings["azure_project"],
            "group_name": group_name,
            "title": title,
        },
    )
    matching_issue = _get_azure_devops_matching_issue(client, settings["azure_project"], title)
    assigned_to = settings.get("assigned_to", "")
    tags = "; ".join(settings.get("tags", []))
    parent_issue: WorkItemReference | WorkItem = matching_issue or client.create_work_item(
        bypass_rules=True,
        document=[
            JsonPatchOperation(
                op="add",
                path="/fields/System.AssignedTo",
                value=assigned_to,
            ),
            JsonPatchOperation(
                op="add",
                path="/fields/System.Description",
                value=description,
            ),
            JsonPatchOperation(
                op="add",
                path="/fields/System.Tags",
                value=tags,
            ),
            JsonPatchOperation(
                op="add",
                path="/fields/System.Title",
                value=title[:AZURE_MAX_TITLE_LENGTH],
            ),
        ],
        project=settings["azure_project"],
        type="Issue",
    )
    LOGGER.info(
        "Using parent issue",
        extra={"id": parent_issue.id, "url": parent_issue.url},
    )

    for subtask in subtasks:
        created_task = _create_azure_devops_task(
            assigned_to=assigned_to,
            client=client,
            parent_issue=parent_issue,
            project=settings["azure_project"],
            tags=tags,
            title=subtask["title"],
        )
        LOGGER.info(
            "Created task",
            extra={"id": created_task.id, "url": created_task.url},
        )


def is_under_review(group_name: str) -> bool:
    with suppress(IndexError):
        group = _get_group(group_name)
        return group.get("state", {}).get("managed", "") == "UNDER_REVIEW"

    return True


def _get_hooks(group_name: str) -> tuple[dict[str, Any], ...]:
    query_params = {
        "IndexName": "inverted_index",
        "KeyConditionExpression": (
            Key("sk").eq(f"GROUP#{group_name}") & Key("pk").begins_with("HOOK#")
        ),
    }
    response = resources.TABLE_RESOURCE.query(**query_params)  # type: ignore[arg-type]
    items = response.get("Items", [])
    while response.get("LastEvaluatedKey"):
        response = resources.TABLE_RESOURCE.query(
            **query_params,  # type: ignore[arg-type]
            ExclusiveStartKey=response.get("LastEvaluatedKey"),  # type: ignore[arg-type]
        )
        items += response.get("Items", [])
    return tuple(items)


def notify_hooks(*, event: str, group_name: str, info: dict[str, Any]) -> None:
    hooks = tuple(
        hook
        for hook in _get_hooks(group_name)
        if event in hook["hook_events"] and hook["state"]["status"] == "ACTIVE"
    )

    with ThreadPoolExecutor() as executor:

        @resilient
        def _notify(hook: dict[str, Any]) -> None:
            LOGGER.info(
                "Notifying hook",
                extra={
                    "entrypoint": hook["entry_point"],
                    "event": event,
                    "group_name": group_name,
                    "info": info,
                },
            )
            response = requests.post(
                hook["entry_point"],
                headers={hook["token_header"]: hook["token"]}
                if hook["token_header"]
                else {"x-api-key": hook["token"]},
                json={
                    "event": event,
                    "group": group_name,
                    "info": info,
                },
                timeout=NOTIFIER_TIMEOUT_SEC,
            )
            LOGGER.info(
                "Hook response",
                extra={
                    "group_name": group_name,
                    "entrypoint": hook["entry_point"],
                    "event": event,
                    "status_code": response.status_code,
                    "response_text": response.text,
                },
            )

        executor.map(_notify, hooks)


@resilient
def notify_google_chat(*, text: str) -> None:
    if FI_GOOGLE_CHAT_WEBHOOK_URL == "null" or not FI_GOOGLE_CHAT_WEBHOOK_URL:
        return

    requests.post(
        FI_GOOGLE_CHAT_WEBHOOK_URL,
        json={"text": text},
        timeout=NOTIFIER_TIMEOUT_SEC,
    )


@resilient
def notify_jira(
    *,
    issue_url: str,
    payload: dict[str, Any],
    webhook_url: str,
) -> None:
    requests.post(
        webhook_url,
        json={"issueURL": issue_url, "payload": payload},
        timeout=NOTIFIER_TIMEOUT_SEC,
    )


def _get_group_names(org_id: str) -> tuple[str, ...]:
    query_args = {
        "ExpressionAttributeNames": {"#name": "name"},
        "IndexName": "inverted_index",
        "KeyConditionExpression": (Key("sk").eq(org_id) & Key("pk").begins_with("GROUP#")),
        "ProjectionExpression": "#name",
    }
    response = resources.TABLE_RESOURCE.query(**query_args)  # type: ignore[arg-type]
    items = response.get("Items", [])
    while response.get("LastEvaluatedKey"):
        response = resources.TABLE_RESOURCE.query(
            **query_args,  # type: ignore[arg-type]
            ExclusiveStartKey=response.get("LastEvaluatedKey"),  # type: ignore[arg-type]
        )
        items += response.get("Items", [])
    return tuple(item["name"] for item in items)  # type: ignore[misc]


@resilient
def notify_poc(*, event: HookEvent, group_name: str, vuln_id: str) -> None:
    if (
        FI_WEBHOOK_POC_URL == "null"
        or not FI_WEBHOOK_POC_KEY
        or not FI_WEBHOOK_POC_URL
        or not FI_WEBHOOK_POC_ORG
    ):
        return

    poc_groups = _get_group_names(FI_WEBHOOK_POC_ORG)
    if group_name in poc_groups:
        request_id = str(uuid.uuid4())
        LOGGER.info(
            "Notifying event for vulnerability in the group",
            extra={
                "request_id": request_id,
                "event_name": event.name,
                "vulnerability_id": vuln_id,
                "group_name": group_name,
                "task_name": "notify_client",
            },
        )
        response = requests.post(
            FI_WEBHOOK_POC_URL,
            headers={"x-api-key": FI_WEBHOOK_POC_KEY},
            json={
                "group": group_name,
                "event": event.value,
                "id_vulnerability": vuln_id,
            },
            timeout=NOTIFIER_TIMEOUT_SEC,
        )
        LOGGER.info(
            "Notification request finished",
            extra={
                "request_id": request_id,
                "status_code": response.status_code,
                "task_name": "notify_client",
            },
        )
