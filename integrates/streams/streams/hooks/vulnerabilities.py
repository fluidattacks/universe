import itertools
from operator import (
    itemgetter,
)
from typing import (
    Any,
)

import streams.resource as resources
from streams.hooks import (
    notifier,
)
from streams.hooks.types import (
    HookEvent,
    IssueSubtask,
)
from streams.logger import (
    StreamsLogger,
)
from streams.types import (
    Record,
    StreamEvent,
)

LOGGER = StreamsLogger("streams.hooks")
LOGGER.set_handler("🏹")


def _get_finding(finding_id: str, group_name: str) -> dict[str, Any]:
    response = resources.TABLE_RESOURCE.get_item(
        Key={"pk": f"FIN#{finding_id}", "sk": f"GROUP#{group_name}"},
    )
    return response["Item"]


def _was_approved(record: Record) -> bool:
    return bool(
        record.event_name == StreamEvent.MODIFY
        and record.new_image
        and record.new_image["state"]["status"] == "VULNERABLE"
        and record.old_image
        and record.old_image["state"]["status"] == "SUBMITTED",
    )


def _was_created_vulnerable(record: Record) -> bool:
    """Usually machine, move roots or migration scripts"""
    return bool(
        record.event_name == StreamEvent.INSERT
        and record.new_image
        and record.new_image["state"]["status"] == "VULNERABLE",
    )


def _group_by_finding_id(records: tuple[Record, ...]) -> tuple[tuple[str, tuple[dict, ...]], ...]:
    get_finding_id = itemgetter("finding_id")
    sorted_items = sorted(
        (record.new_image for record in records if record.new_image),
        key=get_finding_id,
    )
    return tuple(
        (finding_id, tuple(vulnerabilities))
        for finding_id, vulnerabilities in itertools.groupby(sorted_items, key=get_finding_id)
    )


def notify_created(
    group_name: str,
    group_records: tuple[Record, ...],
) -> None:
    """
    Notifies whenever vulnerabilities transition to a state
    where they become visible to customers
    """
    created_records = tuple(
        record
        for record in group_records
        if _was_approved(record) or _was_created_vulnerable(record)
    )
    if not created_records:
        return

    for record in created_records:
        vulnerability = record.new_image
        if not vulnerability or "finding_id" not in vulnerability:
            return
        finding = _get_finding(vulnerability["finding_id"], group_name)

        notifier.notify_hooks(
            event="VULNERABILITY_CREATED",
            group_name=group_name,
            info={
                "finding_id": finding["id"],
                "finding_title": finding["title"],
                "group_name": group_name,
                "severity_score": float(
                    vulnerability["severity_score"]["temporal_score"]
                    if "severity_score" in vulnerability
                    else finding["severity_score"]["temporal_score"],
                ),
                "severity_score_v4": float(
                    vulnerability["severity_score"].get("threat_score", 0)
                    if "severity_score" in vulnerability
                    else finding["severity_score"].get("threat_score", 0),
                ),
                "vulnerability_id": vulnerability["pk"].lstrip("VULN#"),
                "vulnerability_specific": vulnerability["state"]["specific"],
                "vulnerability_where": vulnerability["state"]["where"],
            },
        )
        notifier.notify_poc(
            event=HookEvent.REPORTED_VULNERABILITY,
            group_name=group_name,
            vuln_id=vulnerability["pk"].lstrip("VULN#"),
        )

    items_by_finding = _group_by_finding_id(created_records)

    for finding_id, vulnerabilities in items_by_finding:
        finding = _get_finding(finding_id, group_name)
        url = f"https://app.fluidattacks.com/groups/{group_name}/vulns/{finding_id}"
        subtasks: tuple[IssueSubtask, ...] = tuple(
            {
                "title": (
                    f"Location: {vulnerability['state']['where']} - "
                    f"Specific: {vulnerability['state']['specific']}"
                ),
            }
            for vulnerability in vulnerabilities
        )
        notifier.notify_azure_devops(
            description=(
                f"{finding['description']}<br>"
                "<br>"
                f"<b>Threat:</b> {finding['threat']}<br>"
                f"<b>Recommendation:</b> {finding['recommendation']}<br>"
                "<br>"
                f"<a href='{url}'>{url}</a>"
            ),
            group_name=group_name,
            subtasks=subtasks,
            title=finding["title"],
        )
        notifier.notify_gitlab(
            description=(
                f"{finding['description']}\n"
                "\n"
                f"**Threat:** {finding['threat']}\n"
                "\n"
                f"**Recommendation:** {finding['recommendation']}\n"
                "\n"
                f"[{url}]({url})"
            ),
            group_name=group_name,
            subtasks=subtasks,
            title=finding["title"],
        )

    notifier.notify_google_chat(
        text="\n".join(
            [
                f"🏹 New vulnerabilities reported on group {group_name}:",
                *[
                    f"- https://app.fluidattacks.com/groups"
                    f"/{group_name}/vulns/{finding_id}/locations"
                    for finding_id, _ in items_by_finding
                ],
            ],
        ),
    )


def _linked_to_jira_issue(record: Record) -> bool:
    return bool(
        record.old_image
        and record.old_image.get("bug_tracking_system_url")
        and record.old_image.get("webhook_url"),
    )


def _was_deleted(record: Record) -> bool:
    return bool(
        record.old_image
        and record.old_image["state"]["status"] == "VULNERABLE"
        and (
            record.event_name == StreamEvent.REMOVE
            or (
                record.event_name == StreamEvent.MODIFY
                and record.new_image
                and record.new_image["state"]["status"] == "DELETED"
            )
        ),
    )


def notify_deleted(
    group_name: str,
    group_records: tuple[Record, ...],
) -> None:
    """Notifies whenever reported vulnerabilities transition to a deleted state"""
    deleted_records = tuple(record for record in group_records if _was_deleted(record))
    if not deleted_records:
        return

    for record in deleted_records:
        vulnerability = record.new_image or record.old_image
        if not vulnerability:
            return
        notifier.notify_hooks(
            event="VULNERABILITY_DELETED",
            group_name=group_name,
            info={
                "finding_id": vulnerability["sk"].lstrip("FIN#"),
            },
        )
        notifier.notify_poc(
            event=HookEvent.DELETED_VULNERABILITY,
            group_name=group_name,
            vuln_id=vulnerability["pk"].lstrip("VULN#"),
        )
        if _linked_to_jira_issue(record) and record.old_image:
            reasons = record.old_image["state"].get("reasons", [])
            notifier.notify_jira(
                issue_url=record.old_image["bug_tracking_system_url"],
                payload={
                    "action": "WAS_REMOVED",
                    "reason": next(iter(reasons), "Not provided."),
                    "specific": record.old_image["state"]["specific"],
                    "where": record.old_image["state"]["where"],
                },
                webhook_url=record.old_image["webhook_url"],
            )


def _changed_to_safe(record: Record) -> bool:
    return bool(record.new_image and record.new_image["state"]["status"] == "SAFE")


def _was_updated(record: Record) -> bool:
    return bool(
        record.event_name == StreamEvent.MODIFY
        and record.old_image
        and record.old_image["state"]["status"] == "VULNERABLE"
        and record.new_image
        and record.new_image["state"]["status"] in ["SAFE", "VULNERABLE"],
    )


def _was_closed(record: Record) -> bool:
    return bool(
        record.event_name == StreamEvent.MODIFY
        and record.old_image
        and record.old_image["state"]["status"] == "VULNERABLE"
        and record.new_image
        and record.new_image["state"]["status"] == "SAFE",
    )


def notify_updated(
    group_name: str,
    group_records: tuple[Record, ...],
) -> None:
    """Notifies whenever reported vulnerabilities are updated"""
    updated_records = tuple(record for record in group_records if _was_updated(record))
    if not updated_records:
        return

    for record in updated_records:
        vulnerability = record.new_image
        if not vulnerability:
            return
        notifier.notify_poc(
            event=HookEvent.EDITED_VULNERABILITY,
            group_name=group_name,
            vuln_id=vulnerability["pk"].lstrip("VULN#"),
        )
        if _changed_to_safe(record) and _linked_to_jira_issue(record) and record.old_image:
            notifier.notify_jira(
                issue_url=record.old_image["bug_tracking_system_url"],
                payload={
                    "action": "CHANGED_TO_SAFE",
                    "specific": record.old_image["state"]["specific"],
                    "where": record.old_image["state"]["where"],
                },
                webhook_url=record.old_image["webhook_url"],
            )

        if _was_closed(record):
            notifier.notify_hooks(
                event="VULNERABILITY_CLOSED",
                group_name=group_name,
                info={"vulnerability_id": vulnerability["pk"].lstrip("VULN#")},
            )
