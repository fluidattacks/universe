import os
from typing import (
    Any,
)

from boto3.dynamodb.types import (
    TypeDeserializer,
)

from streams.types import (
    Record,
    StreamEvent,
)


def _deserialize_dynamodb_json(item: dict[str, Any]) -> dict[str, Any]:
    """Deserializes a DynamoDB JSON into a python dictionary"""
    deserializer = TypeDeserializer()

    return {key: deserializer.deserialize(value) for key, value in item.items()}


def format_record(record: dict[str, Any]) -> Record:
    """
    Formats the record into a NamedTuple

    https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_streams_Record.html
    """
    return Record(
        event_name=StreamEvent[record["eventName"]],
        new_image=(
            _deserialize_dynamodb_json(record["dynamodb"]["NewImage"])
            if "NewImage" in record["dynamodb"]
            else None
        ),
        old_image=(
            _deserialize_dynamodb_json(record["dynamodb"]["OldImage"])
            if "OldImage" in record["dynamodb"]
            else None
        ),
        pk=record["dynamodb"]["Keys"]["pk"]["S"],
        sequence_number=record["dynamodb"]["SequenceNumber"],
        sk=record["dynamodb"]["Keys"]["sk"]["S"],
    )


def _get_secret(var_name: str) -> str | None:
    value = os.getenv(var_name)
    if value is None or value == "null":
        return None

    return value


def def_secret(prod: str, dev: str) -> str | None:
    secret_prod = _get_secret(prod)
    secret_dev = _get_secret(dev)
    if secret_prod is None and secret_dev is None:
        raise ValueError(f"{prod} or {dev} is missing")

    return secret_prod or secret_dev
