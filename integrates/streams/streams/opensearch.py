from typing import Any, Literal

from opensearchpy import (
    AWSV4SignerAuth,
    JSONSerializer,
    OpenSearch,
    RequestsHttpConnection,
)

from streams.context import (
    FI_AWS_OPENSEARCH_HOST,
)
from streams.resource import (
    SESSION,
)


class SetEncoder(JSONSerializer):
    def default(self, data: Any) -> Any:
        if isinstance(data, set):
            return list(data)
        return JSONSerializer.default(self, data)


CREDENTIALS = SESSION.get_credentials()


def _get_client(host: str, service: Literal["aoss", "es"]) -> OpenSearch:
    is_https = host.startswith("https://")
    return OpenSearch(
        connection_class=RequestsHttpConnection,
        hosts=[host],
        http_auth=AWSV4SignerAuth(CREDENTIALS, SESSION.region_name, service),
        http_compress=True,
        max_retries=10,
        retry_on_status=(429, 502, 503, 504),
        retry_on_timeout=True,
        serializer=SetEncoder(),
        use_ssl=is_https,
        verify_certs=is_https,
    )


CLIENT = _get_client(FI_AWS_OPENSEARCH_HOST or "", "es")
