{
  imports = [ ./dev/makes.nix ];
  lintWithAjv = {
    integratesStreamsTriggers = {
      schema = "/integrates/streams/triggers-schema.json";
      targets = [ "/integrates/streams/triggers.json" ];
    };
  };
}
