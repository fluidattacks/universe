{ makeScript, outputs, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "machine-serve-report";
  replace = {
    __argIntegratesBackEnv__ = outputs."/integrates/back/env";
    __argIntegratesSrc__ = projectPath "/integrates/back/integrates";
  };
  searchPaths.source =
    [ outputs."/common/utils/aws" outputs."/integrates/back/env/pypi" ];
}
