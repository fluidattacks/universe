{ makePythonEnvironment, makeSearchPaths, projectPath, ... }:
let
  src = projectPath "/integrates/jobs/execute_sbom";
  pythonDeps = makePythonEnvironment {
    pythonProjectDir = src;
    pythonVersion = "3.11";
  };
in makeSearchPaths {
  pythonPackage = [ src ];
  source = [ pythonDeps ];
}
