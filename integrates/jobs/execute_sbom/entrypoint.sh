# shellcheck shell=bash

# Function to retrieve the file extension for a given SBOM format
function get_file_extension {
  local sbom_format="${1}"
  local file_extension

  if ! file_extension="$(execution-helper get-file-extension \
    --sbom-format "${sbom_format}")"; then
    error "Failed to retrieve the file format for ${sbom_format}"
    exit 1
  fi

  echo "${file_extension}"
}

# Common function to set up directories and download configuration files
function prepare_execution_environment {
  local group_dir="${1}"
  local config_file="${2}"

  if [ ! -d "${group_dir}" ]; then
    error "Directory '${group_dir}' not found"
    return 1
  fi

  local execution_configs_dir="${group_dir}/execution_configs"
  mkdir -p "${execution_configs_dir}"
  aws s3 cp "s3://machine.data/sbom_configs/${config_file}" \
    "${execution_configs_dir}/" || {
    error "Failed to download configuration: ${config_file}"
    return 1
  }

  local file_config="${execution_configs_dir}/${config_file}"
  if [ ! -f "${file_config}" ]; then
    error "SBOM configuration not found: ${file_config}"
    return 1
  fi

  echo "${file_config}"
}

# Function to process individual SBOM configurations and execute tasks
function process_sbom_execution {
  local config_file="${1}"
  local execution_id="${2}"
  local s3_suffix="${3}"
  local requester_email="${4}"
  local job_type="${5}"
  local sbom_format="${6}"
  local file_extension="${7}"
  local notification_id="${8}"
  local image_ref="${9:-}"

  sbom "execution_configs/${config_file}" --config
  local execution_result="execution_results/${execution_id}.${file_extension}"

  if [[ -f ${execution_result} ]]; then
    aws s3 cp "${execution_result}" \
      "s3://machine.data/sbom_results/${s3_suffix}.${file_extension}" || {
      error "Failed to upload result for ${execution_id}"
      return 1
    }

    case "${job_type}" in
      scheduler)
        execution-helper submit-task \
          --execution-id "${s3_suffix}" \
          --image-ref "${image_ref}" \
          || error "Processing Task submission failed"
        ;;
      request)
        execution-helper submit-mail-task \
          --execution-id "${s3_suffix}" \
          --requester-email "${requester_email}" \
          --sbom-format "${sbom_format}" \
          --notification-id "${notification_id}" \
          || error "Mail task submission failed"
        ;;
      *)
        error "Unknown job type: ${job_type}"
        ;;
    esac
  else
    error "Execution result not found: ${execution_result}"
  fi
}

# Function to handle images
function execute_on_images {
  local additional_info="${1}"
  local requester_email="${2}"
  local group_name="${3}"

  local sbom_format file_extension images_raw config_files
  job_type="$(echo "${additional_info}" | jq -c -r '.job_type')"
  notification_map="$(echo "${additional_info}" | jq -c -r '.notification_id_map // empty')"
  sbom_format="$(echo "${additional_info}" | jq -c -r '.sbom_format')"
  file_extension="$(get_file_extension "${sbom_format}")"
  images_raw="$(echo "${additional_info}" | jq -c '.images')"
  config_files="$(echo "${additional_info}" | jq -c '.images_config_files')"

  local images_array=() configs_array=()
  mapfile -t images_array < <(echo "${images_raw}" | jq -r -c '.[]')
  mapfile -t configs_array < <(echo "${config_files}" | jq -r -c '.[]')

  mkdir -p "groups/${group_name}"

  for i in "${!images_array[@]}"; do
    info "Processing image $((i + 1)) out of ${#images_array[@]}"

    local current_image="${images_array[i]}"
    local config_file="${configs_array[i]}"

    local image_ref root_nickname execution_id s3_suffix notification_id
    root_nickname="$(echo "${current_image}" | jq -r '.root_nickname')"
    image_ref="$(echo "${current_image}" | jq -r '.image_ref')"

    notification_id="$(echo "${notification_map}" \
      | jq -r --arg key "${image_ref}" \
        '.[$key] // empty')"

    local group_dir="groups/${group_name}"

    local file_config
    if ! file_config=$(prepare_execution_environment \
      "${group_dir}" "${config_file}"); then
      continue
    fi

    config_filename="$(basename "${file_config}")"
    pushd "${group_dir}" || continue
    mkdir -p "execution_results"

    execution_id="${config_file%_config.*}"
    s3_suffix="${group_name}/${root_nickname}/images/${execution_id}"

    process_sbom_execution "${config_filename}" "${execution_id}" "${s3_suffix}" \
      "${requester_email}" "${job_type}" "${sbom_format}" "${file_extension}" \
      "${notification_id}" "${image_ref}"

    popd || continue
  done
}

# Function to handle git roots
function execute_on_git_roots {
  local additional_info="${1}"
  local requester_email="${2}"
  local group_name="${3}"

  local sbom_format file_extension roots config_files
  job_type="$(echo "${additional_info}" | jq -c -r '.job_type')"
  notification_map="$(echo "${additional_info}" | jq -c -r '.notification_id_map // empty')"
  sbom_format="$(echo "${additional_info}" | jq -c -r '.sbom_format')"
  file_extension="$(get_file_extension "${sbom_format}")"
  roots="$(echo "${additional_info}" | jq -c '.root_nicknames')"
  config_files="$(echo "${additional_info}" | jq -c '.roots_config_files')"

  local roots_array=() configs_array=()
  mapfile -t roots_array < <(echo "${roots}" | jq -r '.[]')
  mapfile -t configs_array < <(echo "${config_files}" | jq -r '.[]')

  for i in "${!roots_array[@]}"; do
    info "Processing root $((i + 1)) out of ${#roots_array[@]}"

    local root_nickname="${roots_array[i]}"
    local config_file="${configs_array[i]}"
    local execution_id s3_suffix notification_id

    notification_id="$(echo "${notification_map}" \
      | jq -r --arg key "${root_nickname}" \
        '.[$key] // empty')"

    if ! melts --init pull-repos \
      --group "${group_name}" \
      --root "${root_nickname}"; then
      error "Failed root cloning for ${root_nickname}"
      continue
    fi

    local group_dir="groups/${group_name}"

    local file_config
    if ! file_config=$(prepare_execution_environment \
      "${group_dir}" "${config_file}"); then
      continue
    fi

    config_filename="$(basename "${file_config}")"
    pushd "${group_dir}" || continue
    mkdir -p "execution_results"

    execution_id="${config_file%_config.*}"
    s3_suffix="${group_name}/${root_nickname}/simple/${execution_id}"

    process_sbom_execution "${config_filename}" "${execution_id}" "${s3_suffix}" \
      "${requester_email}" "${job_type}" "${sbom_format}" "${file_extension}" \
      "${notification_id}"

    popd || continue
  done
}

# Main execution function
function config_and_execute_sbom {
  local dynamo_item="${1}"
  local requester_email group_name additional_info

  requester_email="$(echo "${dynamo_item}" | jq -r '.Item.subject.S')"
  group_name="$(echo "${dynamo_item}" | jq -r '.Item.entity.S')"
  additional_info="$(echo "${dynamo_item}" | jq -r '.Item.additional_info.S')"

  if echo "${additional_info}" | jq -e '.images' > /dev/null; then
    execute_on_images "${additional_info}" "${requester_email}" "${group_name}"
  fi

  if echo "${additional_info}" | jq -e '.root_nicknames' > /dev/null; then
    execute_on_git_roots "${additional_info}" "${requester_email}" \
      "${group_name}"
  fi
}

# Main entry point
function main {
  local dynamo_item
  local dynamo_key

  aws_login "dev" "3600"
  sops_export_vars "common/secrets/dev.yaml" \
    CACHIX_AUTH_TOKEN \
    INTEGRATES_API_TOKEN
  aws_login "prod_integrates" "3600"

  dynamo_key="{\"pk\":{\"S\": \"${2}\"}}"
  dynamo_item="$(aws dynamodb get-item \
    --table-name "fi_async_processing" \
    --key "${dynamo_key}")"

  [[ -z ${dynamo_item} ]] && {
    error "Dynamo item is null or empty"
    exit 1
  }

  chown -R "${USER}" "$(pwd)"

  config_and_execute_sbom "${dynamo_item}"
  aws dynamodb delete-item \
    --table-name "fi_async_processing" \
    --key "${dynamo_key}"
}

main "${@}"
