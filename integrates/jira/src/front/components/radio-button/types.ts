import type { HTMLAttributes, HTMLProps } from "react";

type TFieldVariant = "formikField" | "input" | "tableCheckbox";
interface IRadioButtonProps
  extends HTMLAttributes<HTMLInputElement>,
    HTMLProps<HTMLInputElement> {
  name: string;
  value: string;
  variant?: TFieldVariant;
}

export type { IRadioButtonProps, TFieldVariant };
