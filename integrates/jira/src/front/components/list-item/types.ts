import type { HTMLAttributes, HTMLProps } from "react";

interface IListItemProps
  extends HTMLAttributes<HTMLLIElement>,
    HTMLProps<HTMLLIElement> {
  header?: string;
  value?: string;
}

export type { IListItemProps };
