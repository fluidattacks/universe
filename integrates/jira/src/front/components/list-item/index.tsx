import { Li } from "./styles";
import type { IListItemProps } from "./types";

import { Container } from "../container";
import { Text } from "../typography";

function ListItem({
  children,
  header,
  onClick,
  value,
}: Readonly<IListItemProps>): Readonly<React.JSX.Element> {
  return (
    <Li onClick={onClick} value={value}>
      <div className={"main-container"}>
        {value === undefined ? undefined : (
          <Container alignItems={"center"} display={"flex"} gap={0.5}>
            <Text size={"sm"}>{header ?? value}</Text>
          </Container>
        )}
        {children}
      </div>
    </Li>
  );
}

export { ListItem };
