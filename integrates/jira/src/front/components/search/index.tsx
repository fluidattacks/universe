import { Form, Formik } from "formik";
import { useCallback } from "react";

import { SearchBar } from "./search-bar";
import type { ISearchProps } from "./types";

function Search({
  enableSearchBar = true,
  handleOnSubmit,
  handleOnKeyPressed,
  placeHolder,
}: Readonly<ISearchProps>): Readonly<React.JSX.Element> | null {
  const handleSubmit = useCallback(
    ({ search }: Readonly<{ search: string }>): void => {
      handleOnSubmit?.(search);
      handleOnKeyPressed?.(search);
    },
    [handleOnKeyPressed, handleOnSubmit],
  );

  if (!enableSearchBar) {
    return null;
  }

  return (
    <Formik initialValues={{ search: "" }} onSubmit={handleSubmit}>
      <Form>
        <SearchBar
          handleOnKeyPressed={handleOnKeyPressed}
          handleOnSubmit={handleOnSubmit}
          name={"search"}
          placeHolder={placeHolder}
        />
      </Form>
    </Formik>
  );
}

export { Search };
