/* eslint-disable react/jsx-props-no-spreading */
import { useField, useFormikContext } from "formik";
import _ from "lodash";
import { useCallback } from "react";

import { Container } from "../../container";
import { Icon } from "../../icon";
import { theme } from "../../theme";
import { StyledInput } from "../styles";
import type { TInputProps } from "../types";

function SearchBar({
  name,
  handleOnKeyPressed,
  handleOnSubmit,
  placeHolder,
}: Readonly<TInputProps>): Readonly<React.JSX.Element> {
  const [field] = useField(name);
  const { setFieldValue, values } = useFormikContext<{ search: string }>();

  const handleSearch = useCallback((): void => {
    handleOnSubmit?.(values.search);
  }, [handleOnSubmit, values.search]);

  const handleSearchByKey = useCallback((): void => {
    handleOnKeyPressed?.(values.search);
  }, [handleOnKeyPressed, values.search]);

  const clearField = useCallback((): void => {
    void setFieldValue("search", "");
    handleOnKeyPressed?.("");
  }, [handleOnKeyPressed, setFieldValue]);

  return (
    <Container
      alignItems={"center"}
      border={"1px solid"}
      borderColor={theme.palette.gray["300"]}
      borderRadius={"4px"}
      display={"flex"}
      pb={0.5}
      pl={0.5}
      pr={0.5}
      pt={0.5}
    >
      <Icon
        color={theme.palette.gray["700"]}
        icon={"magnifying-glass"}
        iconType={"fa-light"}
        mr={0.5}
        onClick={handleSearch}
        size={"lg"}
      />
      <StyledInput
        {...field}
        $isEmpty={_.isEmpty(values.search)}
        autoComplete={"off"}
        name={name}
        onKeyDown={handleSearchByKey}
        onKeyUp={handleSearchByKey}
        onSubmit={handleSearch}
        placeholder={placeHolder}
      />
      {_.isEmpty(values.search) ? undefined : (
        <Icon
          color={theme.palette.gray["300"]}
          icon={"xmark"}
          iconType={"fa-light"}
          mr={0.5}
          onClick={clearField}
          size={"lg"}
        />
      )}
    </Container>
  );
}

export { SearchBar };
