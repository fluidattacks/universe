import type { PaginationState, RowData, Table } from "@tanstack/react-table";

import type { ITableProps } from "../types";

interface IPaginationProps<TData extends RowData>
  extends Pick<ITableProps<TData>, "onNextPage"> {
  hasNextPage?: boolean;
  size: number;
  table: Table<TData>;
}

interface IPaginationSizeProps<TData extends RowData>
  extends IPaginationProps<TData> {
  color: string;
  pagination: PaginationState;
}

interface IPaginationControlProps<TData extends RowData>
  extends Omit<IPaginationSizeProps<TData>, "size"> {
  pageCount: number;
}

export type { IPaginationProps, IPaginationSizeProps, IPaginationControlProps };
