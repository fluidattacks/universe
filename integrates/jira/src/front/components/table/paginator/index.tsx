/* eslint-disable @typescript-eslint/no-magic-numbers */
import type { RowData } from "@tanstack/react-table";
import { useEffect } from "react";

import { PaginationControl } from "./pagination-control";
import { PaginationSize } from "./pagination-size";
import type { IPaginationProps } from "./types";

import { Container } from "../../container";
import { theme } from "../../theme";

function Paginator<TData extends RowData>({
  hasNextPage = false,
  onNextPage = undefined,
  size,
  table,
}: Readonly<IPaginationProps<TData>>): Readonly<React.JSX.Element> {
  const pageCount = table.getPageCount();
  const { pagination } = table.getState();
  const textColor = theme.palette.gray["25"];

  useEffect((): void => {
    if (pagination.pageIndex + 1 > pageCount) {
      table.setPageIndex(0);
    }
  }, [pageCount, pagination, table]);

  return (
    <Container
      alignItems={"center"}
      bgColor={theme.palette.gray[800]}
      borderRadius={"4px"}
      display={"flex"}
      flexDirection={"row"}
      height={"56px"}
      justify={"space-between"}
      pb={0.5}
      pl={1.5}
      pr={1.5}
      pt={0.5}
    >
      <PaginationSize
        color={textColor}
        pagination={pagination}
        size={size}
        table={table}
      />
      <PaginationControl
        color={textColor}
        hasNextPage={hasNextPage}
        onNextPage={onNextPage}
        pageCount={pageCount}
        pagination={pagination}
        table={table}
      />
    </Container>
  );
}

export { Paginator };
