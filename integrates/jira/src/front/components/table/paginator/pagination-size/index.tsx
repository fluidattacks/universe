/* eslint-disable @typescript-eslint/no-magic-numbers */
import type { RowData } from "@tanstack/react-table";
import { useCallback } from "react";

import { getCurrentRange } from "./utils";

import { Container } from "../../../container";
import { Text } from "../../../typography";
import { Button } from "../styles";
import type { IPaginationSizeProps } from "../types";

function PaginationSize<TData extends RowData>({
  color,
  pagination,
  size,
  table,
}: Readonly<IPaginationSizeProps<TData>>): Readonly<React.JSX.Element> {
  const { pageIndex, pageSize } = pagination;
  const lastPage = Math.min(100, size);
  const itemRange = getCurrentRange(pageIndex, pageSize, size);
  const listSize = [...new Set([10, 20, 50, lastPage])].filter(
    (viewSize) => viewSize <= size,
  );

  const handleClickSize = useCallback(
    (viewSize: number) => () => {
      table.setPageSize(viewSize);
    },
    [table],
  );

  return (
    <Container alignItems={"center"} display={"flex"} gap={4}>
      <Container alignItems={"flex-start"} display={"flex"} gap={0.25}>
        {listSize.map((viewSize) => (
          <Button
            $variant={viewSize === pageSize ? "selected" : "secondary"}
            key={viewSize}
            onClick={handleClickSize(viewSize)}
          >
            <Text color={color} size={"sm"}>
              {viewSize}
            </Text>
          </Button>
        ))}
      </Container>
      <Text color={color} size={"sm"}>
        {itemRange}
      </Text>
    </Container>
  );
}

export { PaginationSize };
