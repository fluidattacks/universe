function getCurrentRange(
  pageIndex: number,
  pageSize: number,
  size: number,
): string {
  const fromRange = pageSize * pageIndex + 1;
  const toRange = Math.min(pageSize * (pageIndex + 1), size);

  return `${fromRange} - ${toRange} of ${size}`;
}

export { getCurrentRange };
