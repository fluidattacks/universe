/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable functional/prefer-immutable-types */
import type { ButtonHTMLAttributes } from "react";
import { css, styled } from "styled-components";

import { theme } from "../../theme";
import type { TCssString } from "../../utils";

type TVariant = "secondary" | "selected";

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  $variant: TVariant;
}

const getVariant: Record<TVariant, TCssString> = {
  secondary: css`
    background-color: ${theme.palette.gray[800]};

    &:hover {
      background-color: ${theme.palette.gray[600]};
    }
  `,
  selected: css`
    background-color: ${theme.palette.gray[700]};

    &:hover {
      background-color: ${theme.palette.gray[600]};
    }
  `,
};

const Button = styled.button<IButtonProps>`
  border-radius: 4px;
  display: inline-flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 12px;
  height: 40px;
  width: 40px;
  transition: all 0.25s ease;
  border: none;
  cursor: pointer;

  ${({ $variant }): TCssString => getVariant[$variant]}
`;

export type { IButtonProps, TVariant };
export { Button };
