/* eslint-disable functional/immutable-data */
/* eslint-disable max-statements */
import type { RowData } from "@tanstack/react-table";
import { useCallback, useRef } from "react";

import { Container } from "../../../container";
import { Icon } from "../../../icon";
import { theme } from "../../../theme";
import { Text } from "../../../typography";
import { Button } from "../styles";
import type { IPaginationControlProps } from "../types";

function getRange(start: number, end: number): Readonly<number[]> {
  return Array.from({ length: end - start }, (_value, index) => start + index);
}

function PaginationControl<TData extends RowData>({
  color,
  hasNextPage = false,
  onNextPage = undefined,
  pageCount,
  pagination,
  table,
}: Readonly<IPaginationControlProps<TData>>): Readonly<React.JSX.Element> {
  const { pageIndex } = pagination;
  const isInLast = pageCount - pageIndex <= 2;
  const needLoadMore = hasNextPage && isInLast;
  const loading = useRef<boolean>(false);

  const goToNext = useCallback(() => {
    if (loading.current) return;

    if (isInLast && onNextPage) {
      loading.current = true;
      void onNextPage().finally((): void => {
        table.setPageIndex(pageIndex + 1);
        loading.current = false;
      });
    } else {
      table.setPageIndex(pageIndex + 1);
    }
  }, [isInLast, onNextPage, pageIndex, table]);

  const indexes = getRange(
    Math.max(pageIndex - 2, 0),
    Math.min(pageIndex + 2, pageCount - 1) + 1,
  );

  const handlePreviousPage = useCallback(() => {
    table.previousPage();
  }, [table]);

  const handleClickIndexes = useCallback(
    (index: number) => () => {
      if (index === pageCount - 1 && onNextPage) {
        void onNextPage().finally((): void => {
          table.setPageIndex(index);
        });
      } else {
        table.setPageIndex(index);
      }
    },
    [onNextPage, pageCount, table],
  );

  const handleFirstPage = useCallback((): void => {
    table.setPageIndex(0);
  }, [table]);

  const handleLastPage = useCallback((): void => {
    if (loading.current) return;

    if (needLoadMore && onNextPage) {
      loading.current = true;
      void onNextPage().finally((): void => {
        const lastIndex = indexes[indexes.length - 1];
        table.setPageIndex(Math.max(lastIndex, pageCount - 1));
        loading.current = false;
      });
    } else {
      table.setPageIndex(pageCount - 1);
    }
  }, [indexes, needLoadMore, onNextPage, pageCount, table]);

  return (
    <Container alignItems={"center"} display={"flex"} gap={1}>
      <Icon
        color={color}
        disabled={!table.getCanPreviousPage()}
        icon={"chevrons-left"}
        onClick={handleFirstPage}
        size={"md"}
      />
      <Icon
        color={color}
        disabled={!table.getCanPreviousPage()}
        icon={"chevron-left"}
        onClick={handlePreviousPage}
        size={"md"}
      />
      <Container alignItems={"flex-start"} display={"flex"} gap={0.25}>
        {indexes.map((index: number) => (
          <Button
            $variant={index === pageIndex ? "selected" : "secondary"}
            key={index}
            onClick={handleClickIndexes(index)}
          >
            <Text color={theme.palette.gray["25"]} size={"sm"}>
              {index + 1}
            </Text>
          </Button>
        ))}
      </Container>
      <Icon
        color={color}
        disabled={!table.getCanNextPage() && !needLoadMore}
        icon={"chevron-right"}
        onClick={goToNext}
        size={"md"}
      />
      <Icon
        color={color}
        disabled={!table.getCanNextPage() && !needLoadMore}
        icon={"chevrons-right"}
        onClick={handleLastPage}
        size={"md"}
      />
    </Container>
  );
}

export { PaginationControl };
