import type { ColumnDef, RowData } from "@tanstack/react-table";

interface ICsvConfig {
  export?: boolean;
  name?: string;
}

interface ITableOptions {
  enableSearchBar?: boolean;
  hasGlobalFilter?: boolean;
  hasNextPage?: boolean;
  searchPlaceholder?: string;
  size?: number;
}

interface ITableProps<TData extends RowData> {
  data: TData[];
  columns: Readonly<ColumnDef<TData>[]>;
  csvConfig?: ICsvConfig;
  loadingData?: boolean;
  onNextPage?: () => Promise<void>;
  onSearch?: (search: string) => void;
  options?: ITableOptions;
}

export type { ITableProps, ITableOptions };
