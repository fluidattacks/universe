/* eslint-disable @typescript-eslint/no-magic-numbers */
import { styled } from "styled-components";

import { theme } from "../theme";

const TableContainer = styled.div`
  background-color: ${theme.palette.white};
  border: solid 1px ${theme.palette.gray[200]};
  border-radius: 4px;
  color: ${theme.palette.gray[800]};
  font-family: Roboto, sans-serif;
  font-size: 14px;
  margin-bottom: 8px;
  overflow: auto;
  padding: 20px;
  text-align: left;

  table {
    border-collapse: collapse;
    table-layout: auto;
    width: 100%;
  }

  td,
  th {
    border-bottom: solid 1px ${theme.palette.gray[200]};
    height: 40px;
    white-space: pre-line;
  }

  td {
    padding: 5px;
    padding-left: unset;
  }

  table tbody tr:not(.no-data-row):hover {
    background-color: ${theme.palette.gray[50]};
  }

  th svg {
    font-size: 12px;
  }

  th svg[class*="angle"] {
    font-size: 12px;
  }

  label {
    display: flex;
    align-items: center;
    margin: unset;
  }

  th {
    font-weight: 700;
    color: ${theme.palette.gray[400]};
    padding: unset;
  }
`;

export { TableContainer };
