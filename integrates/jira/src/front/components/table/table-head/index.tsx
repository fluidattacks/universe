/* eslint-disable functional/prefer-immutable-types */
import {
  faSort,
  faSortDown,
  faSortUp,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import type { RowData, Table } from "@tanstack/react-table";
import { flexRender } from "@tanstack/react-table";

interface IHeadProps<TData extends RowData> {
  readonly table: Table<TData>;
}

function Head<TData extends RowData>({
  table,
}: IHeadProps<TData>): Readonly<React.JSX.Element> {
  return (
    <thead>
      {table.getHeaderGroups().map((headerGroup): JSX.Element => {
        return (
          <tr key={headerGroup.id}>
            {headerGroup.headers.map((header) => (
              <th
                colSpan={header.colSpan}
                key={header.id}
                onClick={header.column.getToggleSortingHandler()}
                style={{
                  cursor: header.column.getCanSort() ? "pointer" : "default",
                }}
              >
                {flexRender(
                  header.column.columnDef.header,
                  header.getContext(),
                )}
                &nbsp;
                <FontAwesomeIcon
                  color={"#b0b0bf"}
                  icon={
                    {
                      asc: faSortUp,
                      desc: faSortDown,
                    }[header.column.getIsSorted() as string] ?? faSort
                  }
                />
              </th>
            ))}
          </tr>
        );
      })}
    </thead>
  );
}

export { Head };
