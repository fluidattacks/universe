/* eslint-disable react/forbid-component-props, react/jsx-props-no-spreading */
import { useField } from "formik";

import { InputContainer } from "../../input-container";
import { InputWrapper, StyledTextArea } from "../../styles";
import type { TTextAreaProps } from "../../types";

function TextArea(
  props: Readonly<TTextAreaProps>,
): Readonly<React.JSX.Element> {
  const { rows, disabled } = props;
  const [field, { error, touched }] = useField(props);
  const { value } = field;

  const alert = touched ? error : undefined;

  return (
    <InputContainer alert={alert} {...props}>
      <InputWrapper
        $show={alert !== undefined}
        className={disabled === true ? "disabled" : ""}
        style={{ height: "auto" }}
      >
        <StyledTextArea
          {...field}
          {...props}
          $show={alert !== undefined}
          rows={rows}
          type={"text"}
          value={value}
        />
      </InputWrapper>
    </InputContainer>
  );
}

export { TextArea };
