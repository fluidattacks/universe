/* eslint-disable functional/prefer-immutable-types */
/* eslint-disable react/jsx-props-no-spreading */
import { useField } from "formik";

import { InputContainer } from "../../input-container";
import { StyledInput } from "../../styles";
import type { TInputProps } from "../../types";

function Input(props: TInputProps): JSX.Element {
  const { name, suggestions } = props;
  const [field, { error, touched }] = useField(props);
  const { value } = field;
  const alert = touched ? error : undefined;

  return (
    <InputContainer alert={alert} {...props}>
      <StyledInput
        {...field}
        {...props}
        $show={alert !== undefined}
        autoComplete={"off"}
        list={`list_${name}`}
        type={"text"}
        value={value}
      />
      <datalist id={`list_${name}`}>
        {suggestions?.map((suggestion): JSX.Element => {
          return <option key={suggestion} value={suggestion} />;
        })}
      </datalist>
    </InputContainer>
  );
}

export { Input };
