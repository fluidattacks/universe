/* eslint-disable @typescript-eslint/no-magic-numbers */
import { css, styled } from "styled-components";

import { theme } from "../theme";

const sharedStyles = css`
  align-items: center;
  background: ${theme.palette.white};
  border-radius: ${theme.spacing[0.25]};
  box-sizing: border-box;
  color: ${theme.palette.gray["700"]};
  display: flex;
  font-family: ${theme.typography.type.primary};
  font-size: ${theme.typography.text.sm};
  font-weight: ${theme.typography.weight.regular};
  gap: 10px;
  height: 36px;
  line-height: ${theme.spacing[1.25]};
  outline: none;
  padding: 10px 16px;
  width: 100%;

  &::placeholder {
    color: ${theme.palette.gray["400"]};
  }

  &:is(.disabled, :disabled) {
    background: ${theme.palette.gray["100"]};
    cursor: not-allowed;

    &::placeholder {
      color: ${theme.palette.gray["300"]};
    }
  }
`;

const InputBox = styled.div<{ $variant: string }>`
  align-items: flex-start;
  border: ${({ $variant }): string =>
    $variant === "box-inputs" ? `1px solid ${theme.palette.gray["200"]}` : ""};
  border-radius: ${({ $variant }): string =>
    $variant === "box-inputs" ? theme.spacing[0.25] : ""};
  display: flex;
  flex-direction: column;
  gap: ${({ $variant }): string =>
    $variant === "box-inputs" ? "10px" : "4px"};
  padding: ${({ $variant }): string =>
    $variant === "box-inputs" ? "10px 16px" : "0"};
  position: relative;
  margin: 0;
  width: ${({ $variant }): string =>
    $variant === "box-inputs" ? "637px" : "100%"};
`;

const ErrorMessage = styled.div<{ $show: boolean }>`
  align-items: center;
  align-self: stretch;
  color: ${theme.palette.error["500"]};
  display: ${({ $show }): string => ($show ? "flex" : "none")};
  font-family: ${theme.typography.type.primary};
  font-size: ${theme.typography.text.xs};
  font-weight: ${theme.typography.weight.regular};
  gap: 4px;
  line-height: 18px;
  text-align: left;
`;

const InputWrapper = styled.div<{ $show: boolean }>`
  ${sharedStyles}
  align-self: stretch;
  border: ${({ $show }): string =>
    $show
      ? `1px solid ${theme.palette.error["500"]}`
      : `1px solid ${theme.palette.gray["200"]}`};
  gap: 4px;
  position: relative;
  width: 100%;
  z-index: 1;
`;

const SelectDropdownContainer = styled.ul<{ $isOpen: boolean }>`
  align-items: flex-start;
  align-self: stretch;
  background: ${theme.palette.white};
  border: 1px solid ${theme.palette.gray["100"]};
  border-radius: 4px;
  box-shadow: ${theme.shadows.md};
  display: ${({ $isOpen }): string => ($isOpen ? "flex" : "none")};
  flex-direction: column;
  padding: 4px 0;
  position: absolute;
  top: calc(100% - 5px);
  z-index: 999;
  width: 100%;

  > li {
    align-items: center;
    color: ${theme.palette.gray["700"]};
    height: auto;
    padding: 4px 16px;
    width: 100%;
  }
`;

const StyledInput = styled.input<{ $show: boolean }>`
  ${sharedStyles}
  border: ${({ $show }): string =>
    $show
      ? `1px solid ${theme.palette.error["500"]}`
      : `1px solid ${theme.palette.gray["200"]}`};

  &::-webkit-calendar-picker-indicator {
    display: none !important;
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    appearance: none;
    margin: 0;
  }

  &[type="number"] {
    appearance: textfield;
  }
`;

const StyledTextArea = styled.textarea<{ $show: boolean }>`
  ${sharedStyles}
  border: ${({ $show }): string =>
    $show
      ? `1px solid ${theme.palette.error["500"]}`
      : `1px solid ${theme.palette.gray["200"]}`};
  border: none;
  height: auto;
  min-width: 140px;
  resize: none;
  padding: 0;
`;

const ActionButtons = styled.div`
  align-items: center;
  display: inline-flex;
  gap: 8px;
  justify-content: center;
  position: absolute;
  right: 0;
  top: 0;
  width: auto;
  z-index: 11;

  &.input-select {
    margin: 0 16px 3px 0;
  }
`;

export {
  ActionButtons,
  ErrorMessage,
  InputBox,
  InputWrapper,
  SelectDropdownContainer,
  StyledInput,
  StyledTextArea,
};
