/* eslint-disable @typescript-eslint/no-magic-numbers */
import { styled } from "styled-components";

import type { TTextProps } from "./types";

import { theme } from "../theme";

interface IStyledTextProps {
  $color?: TTextProps["color"];
  $display?: TTextProps["display"];
  $ml?: TTextProps["ml"];
  $mr?: TTextProps["mr"];
  $mt?: TTextProps["mr"];
  $size: TTextProps["size"];
  $textAlign?: TTextProps["textAlign"];
  $weight?: TTextProps["weight"];
}

const StyledText = styled.p<IStyledTextProps>`
  ${({
    $color = theme.palette.gray[600],
    $display = "block",
    $ml = 0,
    $mr = 0,
    $mt = 0,
    $size,
    $textAlign = "",
  }): string => `
    color: ${$color};
    display: ${$display};
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text[$size]};
    font-weight: ${theme.typography.weight.regular};
    line-height: ${theme.spacing[1.25]};
    margin: ${theme.spacing[$mt]} ${theme.spacing[$mr]} ${theme.spacing[0]} ${theme.spacing[$ml]};
    padding: ${theme.spacing[0]} ${theme.spacing[0]} ${theme.spacing[0]} ${theme.spacing[0]};
    text-align: ${$textAlign};
    text-decoration: unset;
    width: 100%;
    white-space: pre-line;
  `}
`;

const StyledHeading = styled.p<IStyledTextProps>`
  ${({ $size }): string => `
    color: ${theme.palette.gray[800]};
    display: block;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.heading[$size]};
    font-weight: ${theme.typography.weight.regular};
    line-height: ${theme.spacing[1.25]};
    margin: ${theme.spacing[0]} ${theme.spacing[0]} ${theme.spacing[0]} ${theme.spacing[0]};
    padding: ${theme.spacing[0]} ${theme.spacing[0]} ${theme.spacing[0]} ${theme.spacing[0]};
    width: "100%";
  `}
`;

export { StyledHeading, StyledText };
