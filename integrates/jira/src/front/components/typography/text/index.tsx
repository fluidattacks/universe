/* eslint-disable functional/prefer-immutable-types */
import { StyledText } from "../styles";
import type { TTextProps } from "../types";

function Text({
  children,
  color,
  display,
  ml,
  mr,
  mt,
  size,
  textAlign,
}: TTextProps): Readonly<React.JSX.Element> {
  return (
    <StyledText
      $color={color}
      $display={display}
      $ml={ml}
      $mr={mr}
      $mt={mt}
      $size={size}
      $textAlign={textAlign}
    >
      {children}
    </StyledText>
  );
}

export { Text };
