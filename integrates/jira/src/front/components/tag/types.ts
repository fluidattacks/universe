type TTagVariant = "green" | "orange" | "red";

interface ITagProps extends React.HTMLAttributes<HTMLSpanElement> {
  variant: TTagVariant;
  fontSize?: string;
}

export type { ITagProps, TTagVariant };
