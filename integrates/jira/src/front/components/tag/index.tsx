import { StyledTag } from "./styles";
import type { ITagProps, TTagVariant } from "./types";

function Tag({
  children,
  fontSize,
  variant,
}: Readonly<ITagProps>): Readonly<React.JSX.Element> {
  return (
    <StyledTag $fontSize={fontSize} $variant={variant}>
      {children}
    </StyledTag>
  );
}

export type { ITagProps, TTagVariant };
export { Tag };
