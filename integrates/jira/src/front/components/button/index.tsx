import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { ButtonHTMLAttributes, MouseEventHandler, ReactNode } from "react";

import type { IStyledButtonProps } from "./styles";
import { StyledButton } from "./styles";

import { Container } from "../container";
import { Icon } from "../icon";

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children?: ReactNode;
  display?: IStyledButtonProps["$display"];
  icon?: IconName;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  pb?: IStyledButtonProps["$pb"];
  pl?: IStyledButtonProps["$pl"];
  pr?: IStyledButtonProps["$pr"];
  pt?: IStyledButtonProps["$pt"];
  variant?: IStyledButtonProps["$variant"];
}

function Button({
  children,
  disabled,
  display,
  icon,
  onClick,
  pb,
  pl,
  pr,
  pt,
  type = "button",
  variant,
}: Readonly<IButtonProps>): Readonly<React.JSX.Element> {
  return (
    <StyledButton
      $display={display}
      $pb={pb}
      $pl={pl}
      $pr={pr}
      $pt={pt}
      $variant={variant}
      disabled={disabled}
      onClick={onClick}
      type={type}
    >
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"unset"}
        whiteSpace={"nowrap"}
      >
        {icon ? (
          <div
            style={{
              display: "inline-block",
              marginRight: children === undefined ? "" : "4px",
            }}
          >
            <Icon icon={icon} iconType={"fa-light"} size={"md"} />
          </div>
        ) : null}
        {children}
      </Container>
    </StyledButton>
  );
}

export type { IButtonProps };
export { Button };
