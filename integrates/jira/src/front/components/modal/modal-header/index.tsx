/* eslint-disable @typescript-eslint/no-magic-numbers */
import type React from "react";
import { useCallback } from "react";

import { Icon } from "../../icon";
import { theme } from "../../theme";
import { Text } from "../../typography";
import { CloseContainer, Header, Title } from "../styles";
import type { IModalProps } from "../types";

interface IModalHeaderProps {
  title: string;
  description: React.ReactNode | string;
  modalRef: IModalProps["modalRef"];
  onCloseModal?: () => void;
}

function ModalHeader({
  title,
  description,
  modalRef,
  onCloseModal,
}: Readonly<IModalHeaderProps>): Readonly<React.JSX.Element> {
  const handleClose = useCallback((): void => {
    onCloseModal?.();
    modalRef.close();
  }, [modalRef, onCloseModal]);

  return (
    <Header>
      <Title>
        <Text color={theme.palette.gray[800]} size={"lg"} weight={"bold"}>
          {title}
        </Text>
        <CloseContainer>
          <Icon
            color={theme.palette.gray[300]}
            icon={"close"}
            iconType={"fa-light"}
            onClick={handleClose}
            size={"md"}
          />
        </CloseContainer>
      </Title>
      <Text mt={0.5} size={"sm"}>
        {description}
      </Text>
    </Header>
  );
}

export { ModalHeader };
