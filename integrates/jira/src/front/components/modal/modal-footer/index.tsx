import type React from "react";
import { useCallback } from "react";

import { Button } from "../../button";
import { Footer } from "../styles";
import type { IModalProps } from "../types";

interface IModalFooterProps {
  modalRef: IModalProps["modalRef"];
  cancelButton: IModalProps["cancelButton"];
  confirmButton: IModalProps["confirmButton"];
}

function ModalFooter({
  modalRef,
  confirmButton = undefined,
  cancelButton = undefined,
}: Readonly<IModalFooterProps>): Readonly<React.JSX.Element> | null {
  const handleCancel = useCallback((): void => {
    cancelButton?.onClick();
    modalRef.close();
  }, [cancelButton, modalRef]);

  const handleConfirm = useCallback((): void => {
    confirmButton?.onClick();
    modalRef.close();
  }, [confirmButton, modalRef]);

  const isThereExtraInfo = [confirmButton, cancelButton].some(
    (item): boolean => item !== undefined,
  );

  const fullInfo = [confirmButton, cancelButton].every((item): boolean =>
    Boolean(item),
  );

  if (isThereExtraInfo) {
    return (
      <Footer $fullInfo={fullInfo}>
        {cancelButton ? (
          <Button display={"block"} onClick={handleCancel} variant={"tertiary"}>
            {cancelButton.text}
          </Button>
        ) : undefined}
        {confirmButton ? (
          <Button display={"block"} onClick={handleConfirm} variant={"primary"}>
            {confirmButton.text}
          </Button>
        ) : undefined}
      </Footer>
    );
  }

  return null;
}

export { ModalFooter };
