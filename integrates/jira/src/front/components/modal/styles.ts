/* eslint-disable @typescript-eslint/no-magic-numbers */
import { styled } from "styled-components";

import type { IStyledFooterProps } from "./types";

const ModalWrapper = styled.div`
  align-items: center;
  background-color: rgb(52 64 84 / 70%);
  bottom: 0;
  display: flex;
  justify-content: center;
  left: 0;
  overflow: auto;
  position: fixed;
  right: 0;
  top: 0;
  z-index: 999;
`;

const ModalContainer = styled.div`
  background-color: ${({ theme }): string => theme.palette.white};
  border-radius: 4px;
  border: 1px solid ${({ theme }): string => theme.palette.gray[200]};
  box-shadow: ${({ theme }): string => theme.shadows.lg};
  color: ${({ theme }): string => theme.palette.gray[800]};
  display: flex;
  flex-direction: column;
  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.sm};
  margin: auto;
  max-height: 90%;
  max-width: 90%;
  min-width: 440px;
  padding: ${({ theme }): string => theme.spacing[1.5]};
  white-space: pre-line;

  > *:not(img) {
    margin-bottom: ${({ theme }): string => theme.spacing[1.5]};
  }

  > *:last-child {
    margin-bottom: 0;
  }
`;

const Header = styled.div`
  display: block;
  height: max-content;
  max-height: 100%;
  max-width: 100%;
  overflow: unset;
  position: static;
`;

const Title = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const CloseContainer = styled.div`
  > span {
    line-height: 1;
    font-size: 24px;
  }
`;

const Footer = styled.div<IStyledFooterProps>`
  display: flex;
  flex-direction: row;
  gap: ${({ $gap = "0" }): string => $gap};
  justify-content: ${({ $justifyContent = "space-between" }): string =>
    $justifyContent};
  margin: 0;

  > label {
    margin: 0;
    flex: ${({ $fullInfo = false }): string =>
      $fullInfo ? "8 2 auto" : "1 1 auto"};
  }

  > div {
    flex: 1 1 auto;
    margin-right: ${({ theme }): string => theme.spacing[0.75]};
  }

  > div:last-child {
    margin-right: 0;
  }
`;

export { Header, ModalContainer, Footer, ModalWrapper, CloseContainer, Title };
