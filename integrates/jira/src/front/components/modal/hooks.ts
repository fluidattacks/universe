import { useCallback, useState } from "react";

interface IUseModal {
  open: () => void;
  close: () => void;
  isOpen: boolean;
  setIsOpen: (value: boolean) => void;
}

function useModal(): Readonly<IUseModal> {
  const [isOpen, setIsOpen] = useState(false);

  const open = useCallback((): void => {
    setIsOpen(true);
  }, []);
  const close = useCallback((): void => {
    setIsOpen(false);
  }, []);

  return { close, isOpen, open, setIsOpen };
}

export type { IUseModal };
export { useModal };
