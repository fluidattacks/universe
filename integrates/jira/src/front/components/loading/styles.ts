/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable functional/prefer-immutable-types */
import { keyframes, styled } from "styled-components";

import type { IStyledLoadingProps, TColor, TSize } from "./types";

import { theme } from "../theme";

const sizes: Record<TSize, string> = {
  lg: "56",
  sm: "12",
};

const colors: Record<TColor, string> = {
  red: theme.palette.primary[500],
  white: theme.palette.white,
};

const svgAnim = keyframes`
  0% {
    transform: rotateZ(0deg);
  }
  100% {
    transform: rotateZ(360deg)
  }
`;

const dash = keyframes`
  0%,
  25% {
    stroke-dashoffset: 280;
    transform: rotate(0);
  }

  50%,
  75% {
    stroke-dashoffset: 75;
    transform: rotate(45deg);
  }

  100% {
    stroke-dashoffset: 280;
    transform: rotate(360deg);
  }
`;

const LoadingContainer = styled.svg<IStyledLoadingProps>`
  animation: ${svgAnim} 2s linear infinite;
  ${({ $size = "lg" }): string => `
  animation-timing-function: linear;
  display: inline-block;
  height: ${sizes[$size]}px;
  width: ${sizes[$size]}px;
  `}
`;

const Circle = styled.circle<IStyledLoadingProps>`
  animation: 1.4s ease-in-out infinite both ${dash};
  display: block;
  fill: transparent;
  stroke: ${({ $color = "red" }): string => colors[$color]};
  stroke-linecap: round;
  stroke-dasharray: 283;
  stroke-dashoffset: 280;
  stroke-width: 10px;
  transform-origin: 50% 50%;
`;

const CircleBg = styled.circle<IStyledLoadingProps>`
  display: block;
  fill: transparent;
  stroke: ${theme.palette.gray[200]};
  stroke-linecap: round;
  stroke-width: 10px;
  transform-origin: 50% 50%;
  opacity: ${({ $color }): string => ($color === "white" ? "20%" : "unset")};
`;

export { Circle, CircleBg, LoadingContainer };
