type TSize = "lg" | "sm";
type TColor = "red" | "white";

interface IStyledLoadingProps {
  $color?: TColor;
  $size?: TSize;
}

interface ILoadingProps {
  color?: TColor;
  label?: string;
  size?: TSize;
}

export type { IStyledLoadingProps, ILoadingProps, TColor, TSize };
