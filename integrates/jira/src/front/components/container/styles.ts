import { styled } from "styled-components";

import type { IContainerProps } from "./types";

import { theme } from "../theme";

interface IStyledContainerProps {
  $alignItems?: IContainerProps["alignItems"];
  $bgColor: string | undefined;
  $border: string | undefined;
  $borderColor: string | undefined;
  $borderRadius: string | undefined;
  $display?: IContainerProps["display"];
  $flexDirection?: IContainerProps["flexDirection"];
  $gap?: IContainerProps["gap"];
  $height?: string;
  $justify?: IContainerProps["justify"];
  $mb?: IContainerProps["mb"];
  $mt?: IContainerProps["mb"];
  $pb?: IContainerProps["pb"];
  $pl?: IContainerProps["pl"];
  $pr?: IContainerProps["pr"];
  $pt?: IContainerProps["pt"];
  $whiteSpace?: IContainerProps["whiteSpace"];
  $width?: string;
  $wrap?: IContainerProps["wrap"];
}

const StyledContainer = styled.div<IStyledContainerProps>`
  ${({
    $alignItems = "unset",
    $bgColor = "transparent",
    $border = "",
    $borderColor = "",
    $borderRadius = "",
    $display = "block",
    $flexDirection = "unset",
    $gap = 0,
    $height = "auto",
    $justify = "unset",
    $mb = 0,
    $mt = 0,
    $pb = 0,
    $pl = 0,
    $pr = 0,
    $pt = 0,
    $whiteSpace = "unset",
    $width = "auto",
    $wrap = "unset",
  }): string => `
  align-items: ${$alignItems};
  background-color: ${$bgColor};
  border: ${$border};
  border-color: ${$borderColor};
  border-radius: ${$borderRadius};
  display: ${$display};
  flex-direction: ${$flexDirection};
  flex-wrap: ${$wrap};
  gap: ${theme.spacing[$gap]};
  height: ${$height};
  justify-content: ${$justify};
  margin: ${theme.spacing[$mt]} ${theme.spacing[0]} ${theme.spacing[$mb]} ${theme.spacing[0]};
  padding: ${theme.spacing[$pt]} ${theme.spacing[$pr]} ${theme.spacing[$pb]} ${theme.spacing[$pl]};
  white-space: ${$whiteSpace};
  width: ${$width};

  &:hover {
    box-shadow: ${theme.shadows.none};
  }
`}
`;

export { StyledContainer };
