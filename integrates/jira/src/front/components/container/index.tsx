/* eslint-disable functional/prefer-immutable-types */
import type { Ref } from "react";
import { forwardRef } from "react";

import { StyledContainer } from "./styles";
import type { IContainerProps } from "./types";

const Container = forwardRef(function Container(
  {
    alignItems,
    bgColor,
    border,
    borderColor,
    borderRadius,
    children,
    display,
    flexDirection,
    gap,
    height,
    justify,
    mb,
    mt,
    pb,
    pl,
    pr,
    pt,
    whiteSpace,
    width,
    wrap,
  }: IContainerProps,
  ref: Ref<HTMLDivElement>,
): JSX.Element {
  return (
    <StyledContainer
      $alignItems={alignItems}
      $bgColor={bgColor}
      $border={border}
      $borderColor={borderColor}
      $borderRadius={borderRadius}
      $display={display}
      $flexDirection={flexDirection}
      $gap={gap}
      $height={height}
      $justify={justify}
      $mb={mb}
      $mt={mt}
      $pb={pb}
      $pl={pl}
      $pr={pr}
      $pt={pt}
      $whiteSpace={whiteSpace}
      $width={width}
      $wrap={wrap}
      ref={ref}
    >
      {children}
    </StyledContainer>
  );
});

export { Container };
