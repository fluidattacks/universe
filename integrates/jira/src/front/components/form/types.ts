import type { FormikConfig, FormikValues } from "formik";
import type { MouseEventHandler, ReactNode } from "react";

interface IFormProps {
  cancelButtonLabel?: string;
  children: ReactNode;
  onClickCancel?: MouseEventHandler<HTMLButtonElement>;
  submitButtonLabel?: string;
  title?: string;
}

type TFormProps<Values extends FormikValues> = FormikConfig<Values> &
  IFormProps;

export type { IFormProps, TFormProps };
