/* eslint-disable sort-keys */
/* eslint-disable functional/prefer-immutable-types */
import type { DefaultTheme } from "styled-components";
import { ThemeProvider } from "styled-components";

const gray = {
  "25": "#fcfcfd",
  "50": "#f9fafb",
  "100": "#f2f4f7",
  "200": "#eaecf0",
  "300": "#d0d5dd",
  "400": "#98a2b3",
  "500": "#667085",
  "600": "#475467",
  "700": "#344054",
  "800": "#1d2939",
  "900": "#101828",
};

const primary = {
  "25": "#fef2f3",
  "50": "#fddfe2",
  "100": "#fbbac0",
  "200": "#f9959e",
  "300": "#f65e6a",
  "400": "#f32637",
  "500": "#bf0b1a",
  "600": "#ac0a17",
  "700": "#9a0915",
  "800": "#870812",
  "900": "#750710",
};

const error = {
  "25": "",
  "50": "#fef3f2",
  "100": "",
  "200": "#fecdca",
  "300": "",
  "400": "",
  "500": "#f04438",
  "600": "",
  "700": "#b42318",
  "800": "",
  "900": "",
};

const warning = {
  "25": "",
  "50": "#fffaeb",
  "100": "",
  "200": "#fef0c7",
  "300": "",
  "400": "#ffd562",
  "500": "#fdb022",
  "600": "#dc6803",
  "700": "#b54708",
  "800": "",
  "900": "",
};

const success = {
  "25": "",
  "50": "#f6fef9",
  "100": "",
  "200": "#a6f4c5",
  "300": "",
  "400": "",
  "500": "#12b76a",
  "600": "",
  "700": "#027a48",
  "800": "",
  "900": "",
};

const info = {
  "25": "",
  "50": "#eff8ff",
  "100": "",
  "200": "#b2ddff",
  "300": "",
  "400": "",
  "500": "#2e90fa",
  "600": "",
  "700": "#175cd3",
  "800": "",
  "900": "",
};

const typography: DefaultTheme["typography"] = {
  type: {
    primary: "Roboto, sans-serif",
    mono: "'Space Mono', monospace",
  },
  heading: {
    lg: "48px",
    md: "32px",
    sm: "24px",
    xs: "16px",
  },
  text: {
    lg: "20px",
    md: "16px",
    sm: "14px",
    xs: "12px",
  },
  weight: {
    bold: "700",
    regular: "400",
  },
};

const shadows: DefaultTheme["shadows"] = {
  none: "none",
  sm: "0px 1px 2px 0px rgba(16, 24, 40, 0.15)",
  md: "0px 4px 6px 0px rgba(16, 24, 40, 0.15)",
  lg: "0px 8px 24px 0px rgba(16, 24, 40, 0.15)",
};

const spacing: DefaultTheme["spacing"] = {
  0: "0rem",
  0.25: "0.25rem",
  0.5: "0.5rem",
  0.75: "0.75rem",
  1: "1rem",
  1.25: "1.25rem",
  1.5: "1.5rem",
  1.75: "1.75rem",
  2: "2rem",
  2.25: "2.25rem",
  2.5: "2.5rem",
  3: "3rem",
  3.5: "3.5rem",
  4: "4rem",
  5: "5rem",
  6: "6rem",
};

const theme: DefaultTheme = {
  typography,
  shadows,
  spacing,
  palette: {
    primary,
    error,
    info,
    warning,
    success,
    black: "#0c111d",
    white: "#ffffff",
    gray,
  },
};

function CustomThemeProvider({
  children,
}: Readonly<{ children: React.ReactNode }>): Readonly<React.JSX.Element> {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}

export { CustomThemeProvider, theme };
