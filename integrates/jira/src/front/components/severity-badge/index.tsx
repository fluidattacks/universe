/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable functional/prefer-immutable-types */
import { Text } from "./styles";

import { Container } from "../container";
import { theme } from "../theme";

type TSeverityBadgeVariant = "critical" | "disable" | "high" | "low" | "medium";

interface ISeverityBadgeProps {
  readonly textL: string;
  readonly textR: string;
  readonly variant: TSeverityBadgeVariant;
}

interface IVariant {
  bgColor: string;
  colorL: string;
  colorR: string;
}

const variants: Record<TSeverityBadgeVariant, IVariant> = {
  critical: {
    bgColor: theme.palette.primary[500],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.white,
  },
  disable: {
    bgColor: theme.palette.gray[200],
    colorL: theme.palette.gray[400],
    colorR: theme.palette.gray[400],
  },
  high: {
    bgColor: theme.palette.warning[600],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.white,
  },
  low: {
    bgColor: theme.palette.warning[200],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.gray[800],
  },
  medium: {
    bgColor: theme.palette.warning[500],
    colorL: theme.palette.gray[800],
    colorR: theme.palette.gray[800],
  },
};

function SeverityBadge({
  textL,
  textR,
  variant,
}: ISeverityBadgeProps): Readonly<React.JSX.Element> {
  const { bgColor, colorL, colorR } = variants[variant];

  return (
    <Container alignItems={"flex-start"} display={"inline-flex"}>
      <Container
        alignItems={"center"}
        bgColor={theme.palette.white}
        border={`1px solid ${bgColor}`}
        borderRadius={"4px 0px 0px 4px"}
        display={"flex"}
        justify={"center"}
        pb={0.25}
        pl={0.25}
        pr={0.25}
        pt={0.25}
      >
        <Text $color={colorL}>{textL}</Text>
      </Container>
      <Container
        alignItems={"center"}
        bgColor={bgColor}
        border={`1px solid ${bgColor}`}
        borderRadius={"0px 4px 4px 0px"}
        display={"flex"}
        pb={0.25}
        pl={0.25}
        pr={0.25}
        pt={0.25}
      >
        <Text $color={colorR}>{textR}</Text>
      </Container>
    </Container>
  );
}

export type { ISeverityBadgeProps, TSeverityBadgeVariant };
export { SeverityBadge };
