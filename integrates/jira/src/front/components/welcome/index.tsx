import { Alert } from "../alert";

function Welcome(): Readonly<React.JSX.Element> {
  return (
    <Alert variant={"info"}>
      {"Welcome,"}
      <br />
      {"Head over to ⚙️ Project settings > Apps > Fluid Attacks"}
      <br />
      {"or ask your Jira Administrator to set it up"}
    </Alert>
  );
}

export { Welcome };
