import { styled } from "styled-components";

import { StyledButton } from "../button/styles";

const StyledIconButton = styled(StyledButton)<{ $actionButton: boolean }>`
  line-height: 0;
  border-radius: ${({ $actionButton }): string =>
    $actionButton ? "2px" : "4px"};
  padding: ${({ $actionButton }): string => ($actionButton ? "2px" : "8px")};
  max-width: 28px;
  max-height: 28px;
  text-align: center;
`;

export { StyledIconButton };
