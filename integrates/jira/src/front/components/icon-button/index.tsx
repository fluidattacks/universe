import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { ButtonHTMLAttributes, MouseEventHandler, ReactNode } from "react";

import { StyledIconButton } from "./styles";

import type { TVariant } from "../button/styles";
import { Icon } from "../icon";
import type { TIconType } from "../icon/types";

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  actionButton?: boolean;
  children?: ReactNode;
  color?: string;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  icon: IconName;
  iconType?: TIconType;
  variant?: TVariant;
}

function IconButton({
  actionButton = false,
  color,
  disabled,
  onClick,
  type = "button",
  value,
  icon,
  iconType,
  variant,
}: Readonly<IButtonProps>): Readonly<React.JSX.Element> {
  return (
    <div>
      <StyledIconButton
        $actionButton={actionButton}
        $variant={variant}
        disabled={disabled}
        onClick={onClick}
        type={type}
        value={value}
      >
        <Icon color={color} icon={icon} iconType={iconType} size={"sm"} />
      </StyledIconButton>
    </div>
  );
}

export type { IButtonProps };
export { IconButton };
