import { view } from "@forge/bridge";
import type { FullContext } from "@forge/bridge/out/types";
import { createContext, useContext, useEffect, useState } from "react";

const appContext = createContext<FullContext | undefined>(undefined);

interface IAppContext {
  readonly children: React.JSX.Element;
}

function AppContextProvider({
  children,
}: IAppContext): Readonly<React.JSX.Element> {
  const [context, setContext] = useState<FullContext>();

  useEffect(() => {
    void view.getContext().then(setContext);
  }, []);

  return <appContext.Provider value={context}>{children}</appContext.Provider>;
}

function useAppContext(): Readonly<FullContext> | undefined {
  return useContext(appContext);
}

export { AppContextProvider, useAppContext };
