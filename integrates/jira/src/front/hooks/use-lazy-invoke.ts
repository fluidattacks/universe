import { invoke } from "@forge/bridge";
import type { InvokePayload } from "@forge/bridge/out/types";
import { useMutation } from "@tanstack/react-query";

interface ILazyInvokeOptions<T> {
  readonly onCompleted: (result: T) => void;
}

interface ILazyInvokeResult<T> {
  readonly loading: boolean;
  readonly result: T | undefined;
}

type TLazyInvokeTuple<T> = readonly [
  (payload?: InvokePayload) => Promise<T>,
  ILazyInvokeResult<T>,
];

function useLazyInvoke<T = Record<string, unknown>>(
  resolverName: string,
  options?: ILazyInvokeOptions<T>,
): TLazyInvokeTuple<T> {
  const { data, isPending, mutateAsync } = useMutation({
    mutationFn: async (payload?: InvokePayload) => {
      return invoke<T>(resolverName, payload);
    },
    onSuccess: options?.onCompleted,
  });

  return [mutateAsync, { loading: isPending, result: data }];
}

export { useLazyInvoke };
