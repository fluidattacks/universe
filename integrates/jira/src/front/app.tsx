import mixpanel from "mixpanel-browser";

import { useAppContext } from "./context/app-context";
import { Main } from "./pages/main";
import { Reattack } from "./pages/reattack";
import { Settings } from "./pages/settings";

const modules: Record<string, React.JSX.Element> = {
  main: <Main />,
  reattack: <Reattack />,
  settings: <Settings />,
};

function App(): Readonly<React.JSX.Element> {
  const appContext = useAppContext();

  return <div>{appContext ? modules[appContext.moduleKey] : undefined}</div>;
}

mixpanel.init("7a7ceb75ff1eed29f976310933d1cc3e");

export { App };
