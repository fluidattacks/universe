import type { ColumnDef } from "@tanstack/react-table";
import { Fragment, useMemo } from "react";

import { CreateIssue } from "./create-issue";
import type {
  IUseVulnerabilities,
  TVulnerability,
} from "./hooks/use-vulnerabilities";
import { useVulnerabilities } from "./hooks/use-vulnerabilities";
import { LinkIssue } from "./link-issue";
import { Severity } from "./severity";
import { UnlinkIssue } from "./unlink-issue";
import { ViewIssue } from "./view-issue";

import { Table } from "../../components/table";
import { Heading } from "../../components/typography";
import { Welcome } from "../../components/welcome";
import { hasConfigurationErrors } from "../../utils";

function formatSlashes(value: string): Readonly<React.JSX.Element[]> {
  return value.split("/").map((part) => (
    <Fragment key={part}>
      {`${part}/`}
      <wbr />
    </Fragment>
  ));
}

function getColumns(
  refetch: IUseVulnerabilities["refetch"],
): Readonly<ColumnDef<TVulnerability>[]> {
  return [
    { accessorKey: "finding.title", header: "Type" },
    {
      accessorKey: "where",
      cell: ({ getValue }) => formatSlashes(getValue<string>()),
      header: "Location",
    },
    {
      accessorKey: "specific",
      cell: ({ getValue }) => formatSlashes(getValue<string>()),
      header: "Specific",
    },
    {
      accessorKey: "severityThreatScore",
      cell: ({ getValue }): Readonly<React.JSX.Element> => {
        return <Severity score={getValue() as number} />;
      },
      header: "Severity",
    },
    { accessorKey: "reportDate", header: "Report date" },
    {
      accessorKey: "externalBugTrackingSystem",
      cell: ({ getValue, row }): Readonly<React.JSX.Element> => {
        const value = getValue<string | null>();

        if (value === null) {
          return (
            <>
              <CreateIssue refetch={refetch} vulnerability={row.original} />
              <LinkIssue refetch={refetch} vulnerability={row.original} />
            </>
          );
        }

        return (
          <>
            <ViewIssue url={value} />
            <UnlinkIssue refetch={refetch} vulnerability={row.original} />
          </>
        );
      },
      header: "Issue",
    },
  ];
}

function Main(): Readonly<React.JSX.Element> {
  const {
    errors,
    fetchNextPage,
    hasNextPage,
    isLoading,
    refetch,
    setSearch,
    vulnerabilities,
  } = useVulnerabilities();
  const columns = useMemo(() => getColumns(refetch), [refetch]);

  if (hasConfigurationErrors(errors)) {
    return <Welcome />;
  }

  return (
    <>
      <Heading size={"sm"}>{"Vulnerabilities"}</Heading>
      <br />
      <Table
        columns={columns}
        data={vulnerabilities}
        loadingData={isLoading}
        onNextPage={fetchNextPage}
        onSearch={setSearch}
        options={{
          hasNextPage,
          searchPlaceholder: "Search by location or specific",
        }}
      />
    </>
  );
}

export { Main };
