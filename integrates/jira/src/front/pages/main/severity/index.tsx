import {
  SeverityBadge,
  type TSeverityBadgeVariant,
} from "../../../components/severity-badge";

const SEVERITY_THRESHOLD_CRITICAL = 9;
const SEVERITY_THRESHOLD_HIGH = 6.9;
const SEVERITY_THRESHOLD_MED = 3.9;
const SEVERITY_THRESHOLD_LOW = 0.1;

type TSeverity = "CRITICAL" | "HIGH" | "LOW" | "MED" | "NONE";

function getSeverityLevel(score: number): TSeverity {
  if (score >= SEVERITY_THRESHOLD_CRITICAL) {
    return "CRITICAL";
  }
  if (score > SEVERITY_THRESHOLD_HIGH) {
    return "HIGH";
  }
  if (score > SEVERITY_THRESHOLD_MED) {
    return "MED";
  }
  if (score >= SEVERITY_THRESHOLD_LOW) {
    return "LOW";
  }

  return "NONE";
}

interface ISeverityConfig {
  variant: TSeverityBadgeVariant;
  text: string;
}

const severityConfig: Readonly<Record<TSeverity, ISeverityConfig>> = {
  CRITICAL: { text: "Critical", variant: "critical" },
  HIGH: { text: "High", variant: "high" },
  LOW: { text: "Low", variant: "low" },
  MED: { text: "Medium", variant: "medium" },
  NONE: { text: "None", variant: "disable" },
};

interface ISeverityProps {
  readonly score: number;
}

function Severity({ score }: ISeverityProps): Readonly<React.JSX.Element> {
  const level = getSeverityLevel(score);
  const { variant, text } = severityConfig[level];

  return (
    <SeverityBadge textL={score.toFixed(1)} textR={text} variant={variant} />
  );
}

export { Severity };
