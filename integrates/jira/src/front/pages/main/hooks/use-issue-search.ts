import { useState } from "react";

import { SEARCH_ISSUES_RESOLVER } from "../../../../back/constants";
import type { ISearchResponse } from "../../../../back/model/issues";
import { useDebouncedCallback } from "../../../hooks/use-debounced-callback";
import { useInvoke } from "../../../hooks/use-invoke";

type TIssues = Readonly<ISearchResponse["sections"][0]["issues"]>;

const DEBOUNCE_WAIT_MS = 300;

interface IUseIssueSearch {
  readonly issues: TIssues;
  readonly setQuery: (query: string) => void;
}

function useIssueSearch(): IUseIssueSearch {
  const [query, setQuery] = useState("");
  const { result } = useInvoke<ISearchResponse>(SEARCH_ISSUES_RESOLVER, {
    payload: { query },
  });
  const issues = result?.sections[0].issues ?? [];

  const debouncedSetQuery = useDebouncedCallback((value: string): void => {
    setQuery(value);
  }, DEBOUNCE_WAIT_MS);

  return { issues, setQuery: debouncedSetQuery };
}

export { useIssueSearch };
