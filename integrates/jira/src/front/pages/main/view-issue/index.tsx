import { router } from "@forge/bridge";
import { useCallback } from "react";

import { Button } from "../../../components/button";

interface IViewIssueProps {
  readonly url: string;
}

function ViewIssue({ url }: IViewIssueProps): Readonly<React.JSX.Element> {
  const handleClick = useCallback(() => {
    void router.open(url);
  }, [url]);

  return (
    <Button
      icon={"arrow-up-right-from-square"}
      onClick={handleClick}
      variant={"ghost"}
    >
      {"View"}
    </Button>
  );
}

export { ViewIssue };
