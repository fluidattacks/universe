import type { ApolloQueryResult } from "@apollo/client";
import type { QueryObserverResult } from "@tanstack/react-query";

import { GET_LINKED_VULNERABILITIES } from "../../../../back/constants";
import type { GetLinkedVulnerabilitiesQuery } from "../../../../back/gql/graphql";
import { useInvoke } from "../../../hooks/use-invoke";

type TVulnerability = NonNullable<
  GetLinkedVulnerabilitiesQuery["group"]["vulnerabilities"]["edges"][0]
>["node"];

interface IUseLinkedVulnerability {
  readonly errors?: ApolloQueryResult<unknown>["errors"];
  readonly loading: boolean;
  readonly refetch: () => Promise<
    QueryObserverResult<
      ApolloQueryResult<GetLinkedVulnerabilitiesQuery | undefined>
    >
  >;
  readonly vulnerabilities: TVulnerability[];
}

function useLinkedVulnerabilities(issueURL: string): IUseLinkedVulnerability {
  const { refetch, result, loading } = useInvoke<
    ApolloQueryResult<GetLinkedVulnerabilitiesQuery | undefined>
  >(GET_LINKED_VULNERABILITIES, { payload: { issueURL } });

  const edges = result?.data?.group.vulnerabilities.edges ?? [];
  const vulnerabilities = edges
    .map((edge) => edge?.node)
    .filter((node): node is TVulnerability => Boolean(node));

  return {
    errors: result?.errors,
    loading,
    refetch,
    vulnerabilities,
  };
}

export type { TVulnerability, IUseLinkedVulnerability };
export { useLinkedVulnerabilities };
