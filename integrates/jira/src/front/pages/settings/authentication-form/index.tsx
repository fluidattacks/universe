import type { ApolloQueryResult } from "@apollo/client";
import type { QueryObserverResult } from "@tanstack/react-query";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import { object, string } from "yup";

import { UPDATE_TOKEN_RESOLVER } from "../../../../back/constants";
import type { GetMeQuery } from "../../../../back/gql/graphql";
import { Button } from "../../../components/button";
import { Form } from "../../../components/form";
import { Input } from "../../../components/input";
import { Text } from "../../../components/typography";
import { useLazyInvoke } from "../../../hooks/use-lazy-invoke";
import { openLink } from "../../../utils";

const DOCS_TOKEN_LINK =
  "https://help.fluidattacks.com/portal/en/kb/articles/learn-the-basics-of-the-fluid-attacks-api#Authentication_with_Fluid_Attacks__platform_API_Token";

const validationSchema = object({
  token: string().required(),
});

interface IAuthenticationFormProps {
  readonly loading: boolean;
  readonly refetch: () => Promise<
    QueryObserverResult<ApolloQueryResult<GetMeQuery>>
  >;
  readonly result: ApolloQueryResult<GetMeQuery> | undefined;
}

function AuthenticationForm({
  loading,
  refetch,
  result,
}: IAuthenticationFormProps): Readonly<React.JSX.Element> {
  const [invokeUpdate, { loading: updating }] = useLazyInvoke(
    UPDATE_TOKEN_RESOLVER,
  );

  const handleSubmit = useCallback(
    async ({ token }: Readonly<{ token: string | undefined }>) => {
      await invokeUpdate({ token });
      mixpanel.track("UpdateJiraToken");
      await refetch();
    },
    [invokeUpdate, refetch],
  );

  const disconnect = useCallback(() => {
    void handleSubmit({ token: undefined });
  }, [handleSubmit]);

  if (result === undefined) {
    return <div />;
  }

  if (result.errors === undefined) {
    return (
      <Button disabled={loading || updating} onClick={disconnect}>
        {"Change token"}
      </Button>
    );
  }

  return (
    <Form
      initialValues={{ token: "" }}
      onSubmit={handleSubmit}
      submitButtonLabel={"Connect"}
      title={"Authentication"}
      validationSchema={validationSchema}
    >
      <Input inputRequired label={"API Token"} name={"token"} type={"text"} />
      <Text size={"sm"}>
        <Button
          onClick={openLink(DOCS_TOKEN_LINK)}
          pl={0.25}
          pr={0.25}
          variant={"ghost"}
        >
          {"Check our docs"}
        </Button>
        &nbsp;{"for more information on how to generate one"}
      </Text>
    </Form>
  );
}

export { AuthenticationForm };
