import type { ApolloQueryResult } from "@apollo/client";
import mixpanel from "mixpanel-browser";

import { AuthenticationForm } from "./authentication-form";
import { AuthenticationStatus } from "./authentication-status";
import { SettingsForm } from "./settings-form";

import { GET_ME_RESOLVER } from "../../../back/constants";
import type { GetMeQuery } from "../../../back/gql/graphql";
import { useInvoke } from "../../hooks/use-invoke";

function Settings(): Readonly<React.JSX.Element> {
  const { loading, refetch, result } =
    useInvoke<ApolloQueryResult<GetMeQuery>>(GET_ME_RESOLVER);
  if (result?.data) {
    mixpanel.identify(result.data.me.userEmail);
  }

  return (
    <>
      <AuthenticationStatus loading={loading} result={result} />
      <br />
      <AuthenticationForm loading={loading} refetch={refetch} result={result} />
      <br />
      {loading || !result?.data ? undefined : (
        <>
          <br />
          <SettingsForm data={result.data} />
        </>
      )}
    </>
  );
}

export { Settings };
