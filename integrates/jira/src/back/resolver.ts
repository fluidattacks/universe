import Resolver from "@forge/resolver";
import { GraphQLError } from "graphql";

import { isAdmin } from "./authz";
import {
  GET_GROUP_VULNERABILITIES_RESOLVER,
  GET_LINKED_VULNERABILITIES,
  GET_ME_RESOLVER,
  GET_SETTINGS_RESOLVER,
  REQUEST_REATTACK_RESOLVER,
  SEARCH_ISSUES_RESOLVER,
  UPDATE_SETTINGS_RESOLVER,
  UPDATE_TOKEN_RESOLVER,
  UPDATE_VULNERABILITY_ISSUE_RESOLVER,
} from "./constants";
import { addComment, searchIssues } from "./model/issues";
import { getMe } from "./model/me";
import { getSettings, updateSettings } from "./model/settings";
import type { ISettings } from "./model/settings";
import { getToken, updateToken } from "./model/token";
import {
  getLinkedVulnerabilities,
  getVulnerabilities,
  requestReattack,
  updateVulnerabilityIssue,
} from "./model/vulnerabilities";
import { getIssueId } from "./utils";

interface IRequestContext {
  extension: {
    project: {
      id: string;
    };
  };
}

const resolver = new Resolver();

resolver.define(GET_GROUP_VULNERABILITIES_RESOLVER, async (request) => {
  const { project } = (request.context as IRequestContext).extension;

  if (!(await isAdmin(project.id))) {
    return { errors: [new GraphQLError("Access denied")] };
  }

  const [token, settings] = await Promise.all([
    getToken(project.id),
    getSettings(project.id),
  ]);
  const { after, search } = request.payload as {
    after: string;
    search: string;
  };

  if (token === undefined || settings.groupName === undefined) {
    return { errors: [new GraphQLError("Missing configuration")] };
  }

  return getVulnerabilities({
    after,
    apiToken: token,
    groupName: settings.groupName,
    search,
  });
});

resolver.define(GET_LINKED_VULNERABILITIES, async (request) => {
  const { project } = (request.context as IRequestContext).extension;
  const [token, settings] = await Promise.all([
    getToken(project.id),
    getSettings(project.id),
  ]);
  const { issueURL } = request.payload as { issueURL: string };

  if (token === undefined || settings.groupName === undefined) {
    return { errors: [new GraphQLError("Missing configuration")] };
  }

  return getLinkedVulnerabilities(token, settings.groupName, issueURL);
});

resolver.define(GET_ME_RESOLVER, async (request) => {
  const { project } = (request.context as IRequestContext).extension;

  if (!(await isAdmin(project.id))) {
    return { errors: [new GraphQLError("Access denied")] };
  }

  const token = await getToken(project.id);

  if (token === undefined) {
    return { errors: [new GraphQLError("Missing configuration")] };
  }

  return getMe(token);
});

resolver.define(GET_SETTINGS_RESOLVER, async (request): Promise<ISettings> => {
  const { project } = (request.context as IRequestContext).extension;

  return getSettings(project.id);
});

resolver.define(REQUEST_REATTACK_RESOLVER, async (request) => {
  const { project } = (request.context as IRequestContext).extension;
  const { findingId, justification, vulnerabilities } = request.payload as {
    findingId: string;
    justification: string;
    vulnerabilities: string[];
  };

  const token = await getToken(project.id);

  if (token === undefined) {
    return { errors: [new GraphQLError("Missing configuration")] };
  }

  return requestReattack({
    apiToken: token,
    findingId,
    justification,
    vulnerabilities,
  });
});

resolver.define(SEARCH_ISSUES_RESOLVER, async (request) => {
  const { project } = (request.context as IRequestContext).extension;
  const { query } = request.payload as { query: string };

  return searchIssues(project.id, query);
});

resolver.define(UPDATE_SETTINGS_RESOLVER, async (request): Promise<void> => {
  const { project } = (request.context as IRequestContext).extension;

  if (!(await isAdmin(project.id))) {
    return;
  }

  const settings = request.payload as ISettings;

  await updateSettings(project.id, settings);
});

resolver.define(UPDATE_TOKEN_RESOLVER, async (request): Promise<void> => {
  const { project } = (request.context as IRequestContext).extension;

  if (!(await isAdmin(project.id))) {
    return;
  }

  const { token } = request.payload as { token: string };

  await updateToken(project.id, token);
});

resolver.define(UPDATE_VULNERABILITY_ISSUE_RESOLVER, async (request) => {
  const { project } = (request.context as IRequestContext).extension;

  if (!(await isAdmin(project.id))) {
    return { errors: [new GraphQLError("Access denied")] };
  }

  const token = await getToken(project.id);
  const { issueURL, vulnerabilityId } = request.payload as {
    issueURL: string;
    vulnerabilityId: string;
  };

  if (token === undefined) {
    return { errors: [new GraphQLError("Missing configuration")] };
  }

  const response = await updateVulnerabilityIssue(
    token,
    issueURL,
    vulnerabilityId,
  );
  await addComment(
    getIssueId(issueURL),
    `Issue ${issueURL ? "linked to" : "unlinked from"} a vulnerability.`,
  );

  return response;
});

const handler = resolver.getDefinitions();

export { handler };
