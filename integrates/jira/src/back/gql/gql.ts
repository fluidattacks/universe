/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
    "\n    query GetGroupFindings($groupName: String!) {\n      group(groupName: $groupName) {\n        findings {\n          description\n          id\n          lastVulnerability\n          maxOpenSeverityScore\n          maxOpenSeverityScoreV4\n          status\n          title\n          totalOpenCVSSF\n          totalOpenCVSSFV4\n        }\n      }\n    }\n  ": types.GetGroupFindingsDocument,
    "\n    query GetMe {\n      me {\n        organizations {\n          groups {\n            name\n          }\n          name\n        }\n        userEmail\n        userName\n      }\n    }\n  ": types.GetMeDocument,
    "\n    query GetLinkedVulnerabilities($groupName: String!, $issueURL: String!) {\n      group(groupName: $groupName) {\n        vulnerabilities(externalBugTrackingSystem: $issueURL) {\n          edges {\n            node {\n              finding {\n                groupName\n                id\n                title\n              }\n              id\n              specific\n              state\n              verification\n              where\n            }\n          }\n        }\n      }\n    }\n  ": types.GetLinkedVulnerabilitiesDocument,
    "\n    query GetGroupVulnerabilities(\n      $after: String\n      $groupName: String!\n      $search: String\n    ) {\n      group(groupName: $groupName) {\n        vulnerabilities(\n          after: $after\n          first: 30\n          search: $search\n          state: VULNERABLE\n        ) {\n          edges {\n            node {\n              externalBugTrackingSystem\n              finding {\n                description\n                groupName\n                id\n                title\n                totalOpenCVSSF\n                totalOpenCVSSFV4\n              }\n              id\n              reportDate\n              severityTemporalScore\n              severityThreatScore\n              specific\n              where\n            }\n          }\n          pageInfo {\n            endCursor\n            hasNextPage\n          }\n        }\n      }\n    }\n  ": types.GetGroupVulnerabilitiesDocument,
    "\n    mutation RequestReattack(\n      $findingId: String!\n      $justification: String!\n      $vulnerabilities: [String]!\n    ) {\n      requestVulnerabilitiesVerification(\n        findingId: $findingId\n        justification: $justification\n        vulnerabilities: $vulnerabilities\n      ) {\n        success\n      }\n    }\n  ": types.RequestReattackDocument,
    "\n    mutation UpdateVulnerabilityIssue(\n      $issueURL: String!\n      $vulnerabilityId: ID!\n      $webhookURL: String\n    ) {\n      updateVulnerabilityIssue(\n        issueURL: $issueURL\n        vulnerabilityId: $vulnerabilityId\n        webhookURL: $webhookURL\n      ) {\n        success\n      }\n    }\n  ": types.UpdateVulnerabilityIssueDocument,
};

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = graphql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function graphql(source: string): unknown;

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n    query GetGroupFindings($groupName: String!) {\n      group(groupName: $groupName) {\n        findings {\n          description\n          id\n          lastVulnerability\n          maxOpenSeverityScore\n          maxOpenSeverityScoreV4\n          status\n          title\n          totalOpenCVSSF\n          totalOpenCVSSFV4\n        }\n      }\n    }\n  "): (typeof documents)["\n    query GetGroupFindings($groupName: String!) {\n      group(groupName: $groupName) {\n        findings {\n          description\n          id\n          lastVulnerability\n          maxOpenSeverityScore\n          maxOpenSeverityScoreV4\n          status\n          title\n          totalOpenCVSSF\n          totalOpenCVSSFV4\n        }\n      }\n    }\n  "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n    query GetMe {\n      me {\n        organizations {\n          groups {\n            name\n          }\n          name\n        }\n        userEmail\n        userName\n      }\n    }\n  "): (typeof documents)["\n    query GetMe {\n      me {\n        organizations {\n          groups {\n            name\n          }\n          name\n        }\n        userEmail\n        userName\n      }\n    }\n  "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n    query GetLinkedVulnerabilities($groupName: String!, $issueURL: String!) {\n      group(groupName: $groupName) {\n        vulnerabilities(externalBugTrackingSystem: $issueURL) {\n          edges {\n            node {\n              finding {\n                groupName\n                id\n                title\n              }\n              id\n              specific\n              state\n              verification\n              where\n            }\n          }\n        }\n      }\n    }\n  "): (typeof documents)["\n    query GetLinkedVulnerabilities($groupName: String!, $issueURL: String!) {\n      group(groupName: $groupName) {\n        vulnerabilities(externalBugTrackingSystem: $issueURL) {\n          edges {\n            node {\n              finding {\n                groupName\n                id\n                title\n              }\n              id\n              specific\n              state\n              verification\n              where\n            }\n          }\n        }\n      }\n    }\n  "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n    query GetGroupVulnerabilities(\n      $after: String\n      $groupName: String!\n      $search: String\n    ) {\n      group(groupName: $groupName) {\n        vulnerabilities(\n          after: $after\n          first: 30\n          search: $search\n          state: VULNERABLE\n        ) {\n          edges {\n            node {\n              externalBugTrackingSystem\n              finding {\n                description\n                groupName\n                id\n                title\n                totalOpenCVSSF\n                totalOpenCVSSFV4\n              }\n              id\n              reportDate\n              severityTemporalScore\n              severityThreatScore\n              specific\n              where\n            }\n          }\n          pageInfo {\n            endCursor\n            hasNextPage\n          }\n        }\n      }\n    }\n  "): (typeof documents)["\n    query GetGroupVulnerabilities(\n      $after: String\n      $groupName: String!\n      $search: String\n    ) {\n      group(groupName: $groupName) {\n        vulnerabilities(\n          after: $after\n          first: 30\n          search: $search\n          state: VULNERABLE\n        ) {\n          edges {\n            node {\n              externalBugTrackingSystem\n              finding {\n                description\n                groupName\n                id\n                title\n                totalOpenCVSSF\n                totalOpenCVSSFV4\n              }\n              id\n              reportDate\n              severityTemporalScore\n              severityThreatScore\n              specific\n              where\n            }\n          }\n          pageInfo {\n            endCursor\n            hasNextPage\n          }\n        }\n      }\n    }\n  "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n    mutation RequestReattack(\n      $findingId: String!\n      $justification: String!\n      $vulnerabilities: [String]!\n    ) {\n      requestVulnerabilitiesVerification(\n        findingId: $findingId\n        justification: $justification\n        vulnerabilities: $vulnerabilities\n      ) {\n        success\n      }\n    }\n  "): (typeof documents)["\n    mutation RequestReattack(\n      $findingId: String!\n      $justification: String!\n      $vulnerabilities: [String]!\n    ) {\n      requestVulnerabilitiesVerification(\n        findingId: $findingId\n        justification: $justification\n        vulnerabilities: $vulnerabilities\n      ) {\n        success\n      }\n    }\n  "];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n    mutation UpdateVulnerabilityIssue(\n      $issueURL: String!\n      $vulnerabilityId: ID!\n      $webhookURL: String\n    ) {\n      updateVulnerabilityIssue(\n        issueURL: $issueURL\n        vulnerabilityId: $vulnerabilityId\n        webhookURL: $webhookURL\n      ) {\n        success\n      }\n    }\n  "): (typeof documents)["\n    mutation UpdateVulnerabilityIssue(\n      $issueURL: String!\n      $vulnerabilityId: ID!\n      $webhookURL: String\n    ) {\n      updateVulnerabilityIssue(\n        issueURL: $issueURL\n        vulnerabilityId: $vulnerabilityId\n        webhookURL: $webhookURL\n      ) {\n        success\n      }\n    }\n  "];

export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;