import { storage } from "@forge/api";

async function getToken(projectId: string): Promise<string | undefined> {
  const token = (await storage.getSecret(`${projectId}#TOKEN`)) as
    | string
    | undefined;

  return token;
}

async function updateToken(
  projectId: string,
  token: string | undefined,
): Promise<void> {
  if (token === undefined) {
    await storage.deleteSecret(`${projectId}#TOKEN`);
  } else {
    await storage.setSecret(`${projectId}#TOKEN`, token);
  }
}

export { getToken, updateToken };
