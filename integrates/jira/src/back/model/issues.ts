import { doc, paragraph } from "@atlaskit/adf-utils/builders";
import api, { route } from "@forge/api";

async function addComment(issueId: string, comment: string): Promise<void> {
  await api.asApp().requestJira(route`/rest/api/3/issue/${issueId}/comment`, {
    body: JSON.stringify({
      body: doc(paragraph(comment)),
    }),
    headers: { "Content-Type": "application/json" },
    method: "POST",
  });
}

interface ISearchResponse {
  sections: {
    issues: {
      id: number;
      key: string;
      summaryText: string;
    }[];
  }[];
}

async function searchIssues(
  projectId: string,
  query: string,
): Promise<ISearchResponse> {
  const params = new URLSearchParams({
    currentProjectId: projectId,
    query,
  });
  const response = await api
    .asUser()
    .requestJira(route`/rest/api/3/issue/picker?${params}`);

  return response.json() as Promise<ISearchResponse>;
}

export type { ISearchResponse };
export { addComment, searchIssues };
