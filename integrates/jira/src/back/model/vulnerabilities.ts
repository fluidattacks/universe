import type { ApolloQueryResult, FetchResult } from "@apollo/client";
import { webTrigger } from "@forge/api";

import { getClient } from "../api/client";
import { graphql } from "../gql";
import type {
  GetGroupVulnerabilitiesQuery,
  GetLinkedVulnerabilitiesQuery,
  RequestReattackMutation,
  UpdateVulnerabilityIssueMutation,
} from "../gql/graphql";

async function getLinkedVulnerabilities(
  apiToken: string,
  groupName: string,
  issueURL: string,
): Promise<ApolloQueryResult<GetLinkedVulnerabilitiesQuery>> {
  const client = getClient(apiToken);
  const query = graphql(`
    query GetLinkedVulnerabilities($groupName: String!, $issueURL: String!) {
      group(groupName: $groupName) {
        vulnerabilities(externalBugTrackingSystem: $issueURL) {
          edges {
            node {
              finding {
                groupName
                id
                title
              }
              id
              specific
              state
              verification
              where
            }
          }
        }
      }
    }
  `);

  return client.query({ query, variables: { groupName, issueURL } });
}

interface IGetVulnerabilitiesArgs {
  readonly after?: string;
  readonly apiToken: string;
  readonly groupName: string;
  readonly search?: string;
}

async function getVulnerabilities({
  after,
  apiToken,
  groupName,
  search,
}: IGetVulnerabilitiesArgs): Promise<
  ApolloQueryResult<GetGroupVulnerabilitiesQuery>
> {
  const client = getClient(apiToken);
  const query = graphql(`
    query GetGroupVulnerabilities(
      $after: String
      $groupName: String!
      $search: String
    ) {
      group(groupName: $groupName) {
        vulnerabilities(
          after: $after
          first: 30
          search: $search
          state: VULNERABLE
        ) {
          edges {
            node {
              externalBugTrackingSystem
              finding {
                description
                groupName
                id
                title
                totalOpenCVSSF
                totalOpenCVSSFV4
              }
              id
              reportDate
              severityTemporalScore
              severityThreatScore
              specific
              where
            }
          }
          pageInfo {
            endCursor
            hasNextPage
          }
        }
      }
    }
  `);

  return client.query({ query, variables: { after, groupName, search } });
}

interface IRequestReattackArgs {
  readonly apiToken: string;
  readonly findingId: string;
  readonly justification: string;
  readonly vulnerabilities: string[];
}

async function requestReattack({
  apiToken,
  findingId,
  justification,
  vulnerabilities,
}: IRequestReattackArgs): Promise<FetchResult<RequestReattackMutation>> {
  const client = getClient(apiToken);
  const mutation = graphql(`
    mutation RequestReattack(
      $findingId: String!
      $justification: String!
      $vulnerabilities: [String]!
    ) {
      requestVulnerabilitiesVerification(
        findingId: $findingId
        justification: $justification
        vulnerabilities: $vulnerabilities
      ) {
        success
      }
    }
  `);

  return client.mutate({
    mutation,
    variables: { findingId, justification, vulnerabilities },
  });
}

async function updateVulnerabilityIssue(
  apiToken: string,
  issueURL: string,
  vulnerabilityId: string,
): Promise<FetchResult<UpdateVulnerabilityIssueMutation>> {
  const webhookURL = issueURL
    ? await webTrigger.getUrl("updateIssueTrigger")
    : undefined;
  const client = getClient(apiToken);
  const mutation = graphql(`
    mutation UpdateVulnerabilityIssue(
      $issueURL: String!
      $vulnerabilityId: ID!
      $webhookURL: String
    ) {
      updateVulnerabilityIssue(
        issueURL: $issueURL
        vulnerabilityId: $vulnerabilityId
        webhookURL: $webhookURL
      ) {
        success
      }
    }
  `);

  return client.mutate({
    mutation,
    variables: { issueURL, vulnerabilityId, webhookURL },
  });
}

export {
  getLinkedVulnerabilities,
  getVulnerabilities,
  requestReattack,
  updateVulnerabilityIssue,
};
