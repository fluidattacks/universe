import type { ApolloQueryResult } from "@apollo/client";

import { getClient } from "../api/client";
import { graphql } from "../gql";
import type { GetMeQuery } from "../gql/graphql";

async function getMe(apiToken: string): Promise<ApolloQueryResult<GetMeQuery>> {
  const client = getClient(apiToken);
  const query = graphql(`
    query GetMe {
      me {
        organizations {
          groups {
            name
          }
          name
        }
        userEmail
        userName
      }
    }
  `);

  return client.query({ query });
}

export { getMe };
