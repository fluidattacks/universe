import { addComment } from "./model/issues";
import { getIssueId } from "./utils";

interface IRequest {
  readonly body: string;
}

interface IResponse {
  readonly statusCode: number;
}

enum Action {
  CHANGED_TO_SAFE = "CHANGED_TO_SAFE",
  WAS_REMOVED = "WAS_REMOVED",
}

interface IPayload {
  action: Action;
  reason?: string;
  specific: string;
  where: string;
}

interface IBody {
  issueURL: string;
  payload: IPayload;
}

async function updateIssue(request: IRequest): Promise<IResponse> {
  const body = JSON.parse(request.body) as IBody;
  const { issueURL, payload } = body;
  const { reason, specific, where } = payload;
  const issueId = getIssueId(issueURL);
  const location = `Location ${where} with specific ${specific}`;

  switch (payload.action) {
    case Action.CHANGED_TO_SAFE: {
      await addComment(issueId, `${location} is now Safe.`);
      break;
    }
    case Action.WAS_REMOVED:
      await addComment(issueId, `${location} was removed due to: ${reason}`);
      break;
    default:
      console.error("Received unknown payload", payload);
  }

  return { statusCode: 200 };
}

export { updateIssue };
