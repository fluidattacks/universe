/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */
/* eslint-disable @typescript-eslint/no-unused-vars */

declare namespace Cypress {
  interface Chainable<Subject> {
    loginToJira: () => Readonly<Chainable<Subject>>;
    dismissMfaIfAsked: () => Readonly<Chainable<Subject>>;
  }
}
