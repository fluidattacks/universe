import "cypress-iframe";

const CLOSE_MFA_DIALOG_DEFAULT_WAIT = 8000;

function configureCustomCommands(): void {
  Cypress.Commands.add("dismissMfaIfAsked", () => {
    const acceptButtonId = "#mfa-promote-dismiss";
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(CLOSE_MFA_DIALOG_DEFAULT_WAIT);
    cy.get("button").then(() => {
      if (Cypress.$(acceptButtonId).length) {
        cy.get(acceptButtonId).click();
        return;
      }
      cy.log("Mfa accept dialog not displayed. Skipping...");
    });
  });

  Cypress.Commands.add("loginToJira", () => {
    // Strategy to make cypress pick the correct top origin
    cy.visit("/404", { failOnStatusCode: false });
    cy.visit("/");

    cy.origin("https://id.atlassian.com", () => {
      cy.on("uncaught:exception", () => false);
      cy.get("#username").type(Cypress.env("ATLASSIAN_USERNAME") as string);
      cy.get("button[type='submit']").click();
      cy.get("#password").type(Cypress.env("ATLASSIAN_PASSWORD") as string, {
        log: false,
      });
      cy.get("button[type='submit']").click();
    });
    cy.dismissMfaIfAsked();

    cy.url().should(
      "equal",
      "https://fluidattacks-dev.atlassian.net/jira/your-work",
    );
  });
}

function overrideCommands(): void {
  Cypress.Commands.overwrite("log", (log, ...args) => {
    if (Cypress.browser.isHeadless) {
      return cy.task("log", args, { log: false }).then(() => {
        return log(...args);
      });
    }
    console.log(...args);
    return log(...args);
  });
}

export { configureCustomCommands, overrideCommands };
