function manageExceptions(): void {
  Cypress.on("uncaught:exception", () => {
    return false;
  });
}

export { manageExceptions };
