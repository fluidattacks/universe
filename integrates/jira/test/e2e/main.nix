{ inputs, isDarwin, isLinux, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-jira-test-e2e";
  searchPaths = {
    bin = [ inputs.nixpkgs.kubectl inputs.nixpkgs.git ]
      ++ inputs.nixpkgs.lib.optionals isLinux [ inputs.nixpkgs.chromium ]
      ++ inputs.nixpkgs.lib.optionals isDarwin [ inputs.nixpkgs.electron ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/common/utils/cypress-fa"
    ];
  };
}
