/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable import/no-default-export */
import { defineConfig } from "cypress";

export default defineConfig({
  chromeWebSecurity: false,
  e2e: {
    baseUrl: "https://fluidattacks-dev.atlassian.net",
    defaultCommandTimeout: 60000,
    setupNodeEvents(on) {
      on("task", {
        log(args) {
          console.log(...args);
          return null;
        },
      });
    },
    specPattern: "./specs/**",
    supportFile: "./support/setup.ts",
    testIsolation: true,
  },
  viewportHeight: 768,
  viewportWidth: 1366,
});
