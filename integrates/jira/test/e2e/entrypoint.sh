# shellcheck shell=bash

function main {
  local cluster="common-k8s"
  local region="us-east-1"

  : \
    && aws_login "dev" "3600" \
    && sops_export_vars_cypress integrates/secrets/dev.yaml \
      ATLASSIAN_PASSWORD \
      ATLASSIAN_USERNAME \
      TEST_RETRIEVES_TOKEN \
    && export CYPRESS_CI_COMMIT_REF_NAME="${CI_COMMIT_REF_NAME}" \
    && if test -n "${CI-}"; then
      : && export KUBECONFIG="./.kube/config" \
        && aws_eks_update_kubeconfig "${cluster}" "${region}" \
        && kubectl rollout status \
          "deploy/integrates-${CI_COMMIT_REF_NAME}" \
          -n "dev" \
          --timeout=0
    fi \
    && pushd integrates/jira \
    && npm install \
    && pushd test/e2e \
    && run_cypress_fa "chromium" "${@}" \
    && popd \
    || return 1
}

main "${@}"
