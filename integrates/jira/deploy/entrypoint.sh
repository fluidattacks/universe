# shellcheck shell=bash

function export_secrets {
  local secrets=(
    FORGE_API_TOKEN
    FORGE_EMAIL
  )

  : && aws_login "dev" "3600" \
    && sops_export_vars integrates/secrets/dev.yaml "${secrets[@]}" \
    || return 1

}

function await_rollout {
  local cluster="common-k8s"
  local region="us-east-1"
  local env="${1}"
  local role="${2}"

  if [[ ${env} == "dev" ]]; then
    : && aws_login "${role}" "3600" \
      && if test -n "${CI-}"; then
        : && aws_eks_update_kubeconfig "${cluster}" "${region}" \
          && kubectl rollout status \
            "deploy/integrates-${CI_COMMIT_REF_NAME}" \
            -n "${env}" \
            --timeout=0
      fi \
      || return 1
  fi
}

function install_dependencies {
  : && npm clean-install \
    && npx forge settings set usage-analytics false \
    || return 1
}

function get_installation_id {
  local forge_environment="${1}"
  local forge_list
  forge_list=$(npx forge install list --json 2>&1)

  : && if ! echo "$forge_list" | jq -e 'type == "array"' > /dev/null; then
    : && echo "$forge_list" >&2 \
      && return 1
  fi \
    && echo "$forge_list" \
    | jq \
      --arg environment "${forge_environment}" \
      --raw-output '.[] | select(.environment == $environment) | .id' \
    || return 1
}

function remove_previous_deployment {
  local forge_environment="${1}"
  local environments
  local installation_id

  : && installation_id=$(get_installation_id "${forge_environment}") \
    && if [[ -n ${installation_id} ]]; then
      : && npx forge uninstall "${installation_id}" \
        || return 1
    fi \
    && environments=$(npx forge environments list) \
    && if [[ ${environments} =~ ${forge_environment} ]]; then
      : && npx forge environments delete \
        --environment "${forge_environment}" \
        --non-interactive \
        || return 1
    fi \
    || return 1
}

function build {
  : && npx vite build \
    || return 1
}

function deploy {
  local forge_environment="${1}"
  export FORGE_FLUID_HOST="https://${CI_COMMIT_REF_NAME}.app.fluidattacks.com"
  export FORGE_APP_KEY="com.fluidattacks.integrates-jira-${CI_COMMIT_REF_NAME}"

  : && if [[ ${forge_environment} != "production" ]]; then
    : && remove_previous_deployment "${forge_environment}" \
      && npx forge environments create \
        --environment "${forge_environment}" \
        --non-interactive \
      || return 1
  else
    : && FORGE_FLUID_HOST="https://app.fluidattacks.com" \
      && FORGE_APP_KEY="com.fluidattacks.integrates-jira" \
      || return 1
  fi \
    && npx forge variables set \
      --environment "${forge_environment}" \
      CI_COMMIT_REF_NAME "${CI_COMMIT_REF_NAME}" \
    && npx forge deploy \
      --environment "${forge_environment}" \
      --non-interactive \
    || return 1
}

function install {
  local forge_environment="${1}"
  local forge_site="${2}"
  local common_args=(
    --environment "${forge_environment}"
    --non-interactive
    --product "Jira"
    --site "${forge_site}"
  )

  : && if [[ ${forge_environment} == "production" ]]; then
    npx forge install "${common_args[@]}" --upgrade
  else
    npx forge install "${common_args[@]}"
  fi \
    || return 1
}

function main {
  local forge_environment
  local forge_site
  local env
  local role

  : && case "${1:-}" in
    dev)
      forge_environment="${CI_COMMIT_REF_NAME}"
      forge_site="fluidattacks-dev.atlassian.net"
      ;;
    prod)
      forge_environment="production"
      forge_site="fluidattacks.atlassian.net"
      ;;
    *)
      abort "[ERROR] Second argument must be one of: dev, prod"
      ;;
  esac \
    && if [ "${1}" == "prod" ]; then
      env="prod-integrates"
      role="prod_integrates"
    else
      env="dev"
      role="dev"
    fi \
    && export_secrets \
    && await_rollout "${env}" "${role}" \
    && pushd integrates/jira \
    && install_dependencies \
    && build \
    && deploy "${forge_environment}" \
    && install "${forge_environment}" "${forge_site}" \
    && popd \
    || return 1
}

main "$@"
