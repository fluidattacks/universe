# shellcheck shell=bash

function main {
  local return_value=1

  : \
    && pushd integrates/jira \
    && npm install \
    && if npx graphql-codegen --config codegen.ts --check; then
      info 'Generated types are up-to-date!' \
        && return_value=0
    else
      info 'Generated types are not up-to-date.' \
        && info 'we will generate the types,' \
        && info 'but the job will fail.' \
        && npx graphql-codegen --config codegen.ts \
        && return_value=1
    fi \
    && popd \
    && return "${return_value}"
}

main "${@}"
