# shellcheck shell=bash

function main {
  : && pushd integrates/jira \
    && npm clean-install \
    && npx tsc -p tsconfig.json --noEmit \
    && npx eslint . \
    && popd \
    || return 1
}

main "$@"
