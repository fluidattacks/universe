# shellcheck shell=bash

function export_secrets {
  local secrets=(
    FORGE_API_TOKEN
    FORGE_EMAIL
  )

  : && aws_login "dev" "3600" \
    && sops_export_vars integrates/secrets/dev.yaml "${secrets[@]}" \
    || return 1
}

function clean {
  export FORGE_FLUID_HOST="https://test.app.fluidattacks.com"
  export FORGE_APP_KEY="com.fluidattacks.integrates-jira"

  install_list=$(npx forge install list --json)

  echo "$install_list"

  filtered_list=$(echo "$install_list" | jq --raw-output '.[] | select(.environment != "production") | "\(.id) \(.environment)"')

  if [ -z "$filtered_list" ]; then
    echo "No non-production installations found."
    return 0
  fi

  while read -r installation_id environment_name; do
    echo "Processing: ${installation_id} ${environment_name}"
    npx forge uninstall "${installation_id}" \
      && npx forge environments delete --environment "${environment_name}" --non-interactive
  done <<< "$filtered_list" || return 1
}

function main {
  : && export_secrets \
    && pushd integrates/jira \
    && npm clean-install \
    && npx forge settings set usage-analytics false \
    && sleep 1 \
    && clean \
    && popd \
    || return 1
}

main "$@"
