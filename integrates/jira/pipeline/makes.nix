{ projectPath, ... }:
let
  arch = let
    commit = "3c8778c732597a2fdee42d7c20d1ab5d948e13d4";
    sha256 = "sha256:0ii4qam4ykq9gbi2lmkk9p9gixmrypmz54jgcji9hpypk4azxy3w";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    all = {
      default = arch.core.rules.titleRule {
        products = [ "all" "integrates-jira" "integrates[^-]" ];
        types = [ ];
      };
      deployRule = arch.core.rules.titleRule {
        products = [ "all" "integrates-jira" ];
        types = [ "feat" "fix" "refac" ];
      };
      noRotate = arch.core.rules.titleRule {
        products = [ "all" "integrates-jira" "integrates" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
  };
in {
  pipelines = {
    integratesJiraDefault = {
      gitlabPath = "/integrates/jira/pipeline/default.yaml";
      jobs = [
        {
          output = "/pipelineOnGitlab/integratesJiraDefault";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/jira/deploy dev";
          gitlabExtra = arch.extras.default // {
            allow_failure = true;
            environment = {
              name = "integrates-jira/development/$CI_COMMIT_REF_SLUG";
              url = "https://fluidattacks-dev.atlassian.net/jira";
            };
            needs = [ "/integrates/back/deploy/dev" ];
            rules = arch.rules.dev ++ [ rules.all.deployRule ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/jira/deploy prod";
          gitlabExtra = arch.extras.default // {
            environment = {
              name = "integrates-jira/production";
              url = "https://fluidattacks.atlassian.net/jira";
            };
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.all.deployRule ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/jira/lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/jira/test/e2e run";
          gitlabExtra = arch.extras.default // {
            allow_failure = true;
            artifacts = {
              name = "jira_test_e2e_$CI_COMMIT_REF_NAME_$CI_COMMIT_SHA";
              paths = [ "integrates/jira/test/e2e/cypress/**" ];
              expire_in = "1 day";
              when = "always";
            };
            needs =
              [ "/integrates/back/deploy/dev" "/integrates/jira/deploy dev" ];
            rules = arch.rules.dev ++ [ rules.all.deployRule ];
            stage = arch.stages.post-deploy;
            tags = [ arch.tags.common-x86 ];
          };
        }
        {
          output = "/integrates/jira/types";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
      ];
    };
  };
}
