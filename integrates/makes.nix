# https://github.com/fluidattacks/makes
{
  imports = [
    ./back/makes.nix
    ./design/build/makes.nix
    ./design/makes.nix
    ./front/makes.nix
    ./infra/makes.nix
    ./jira/makes.nix
    ./pipeline/makes.nix
    ./retrieves/makes.nix
    ./streams/makes.nix
  ];
  secretsForAwsFromGitlab = {
    prodIntegrates = {
      roleArn = "arn:aws:iam::205810638802:role/prod_integrates";
      duration = 3600;
    };
  };
}
