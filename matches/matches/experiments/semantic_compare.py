import asyncio
import json
import logging

from matches.embed.criteria import (
    EMBEDDING_COLLECTION,
)
from matches.embed.database import get_db_client
from matches.embed.embedder import Embedder

LOGGER = logging.getLogger()


async def run_comparison() -> None:
    concepts = [
        "SQL Injection",
        "Cross-Site Scripting",
        "Cross-Site Request Forgery",
        "Escalada de privilegios",
        "Negación de servicio",
        "Información sensible en el código fuente",
    ]
    embedder = Embedder()
    db_client = await get_db_client()
    for concept in concepts:
        embedded = await embedder.embed([concept])
        results = await db_client.search(
            collection_name=EMBEDDING_COLLECTION,
            data=embedded,
            limit=2,
            output_fields=["title", "category"],
        )
        LOGGER.info(
            "\n\nResults for '%s': %s", concept, json.dumps(results, indent=2, ensure_ascii=False)
        )


def run() -> None:
    logging.basicConfig(level=logging.INFO)
    asyncio.run(run_comparison())
