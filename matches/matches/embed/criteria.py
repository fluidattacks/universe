import asyncio
import logging
from typing import Any, TypedDict

import yaml
from httpx import AsyncClient as HttpxAsyncClient
from pymilvus.exceptions import MilvusException
from pymilvus.milvus_client import AsyncMilvusClient

from .database import get_db_client, post_db_checkpoint
from .embedder import Embedder

LOGGER = logging.getLogger()

EMBEDDING_MODEL = "voyage-3"
EMBEDDING_COLLECTION = "fa_criteria_collection"

_COMMIT_SHA = "83655cf104b52f13b42d4dc750b6afac4ffb3b67"
_YAML_PATH = "common/criteria/src/vulnerabilities/data.yaml"
_CRITERIA_VULNS_YAML = f"https://gitlab.com/fluidattacks/universe/-/raw/{_COMMIT_SHA}/{_YAML_PATH}"

_EMBEDDING_DIMENSION = 1024
_DEFAULT_METRIC_TYPE = "COSINE"


class CriteriaTransformed(TypedDict):
    category: str
    examples: str
    remediation_time: str
    score: str
    score_v4: str
    requirements: str
    metadata: str
    last_update_time: str
    title: str
    description: str
    impact: str
    recommendation: str
    threat: str


async def _load_fa_criteria() -> dict[str, CriteriaTransformed]:
    http_client = HttpxAsyncClient()
    response = await http_client.get(_CRITERIA_VULNS_YAML)

    criteria_vulns: dict[str, Any] = yaml.safe_load(response.text)
    for key in criteria_vulns:
        criteria_vulns[key] = {**criteria_vulns[key], **criteria_vulns[key]["en"]}
        del criteria_vulns[key]["en"]
        del criteria_vulns[key]["es"]
    return criteria_vulns


async def _clean_up_collection(
    client: AsyncMilvusClient, collection_name: str, dimension: int
) -> None:
    try:
        await client.load_collection(collection_name)
        await client.drop_collection(collection_name)
        LOGGER.info("Successfully dropped existing collection: %s.", collection_name)
    except MilvusException:
        LOGGER.info("Collection %s not found. Creating a new one.", collection_name)

    await client.create_collection(
        collection_name=collection_name,
        dimension=dimension,
        metric_type=_DEFAULT_METRIC_TYPE,
    )


async def _update_criteria_embeddings(
    client: AsyncMilvusClient, criteria_vulns: dict[str, CriteriaTransformed]
) -> None:
    await _clean_up_collection(client, EMBEDDING_COLLECTION, _EMBEDDING_DIMENSION)
    embedder = Embedder()
    embedded = await embedder.embed([str(value) for value in criteria_vulns.values()])
    data = [
        {
            **value,
            "id": int(key),
            "vector": vector,
            "text": str(value),
        }
        for i, (vector, (key, value)) in enumerate(
            zip(embedded, criteria_vulns.items(), strict=True)
        )
    ]

    await client.insert(collection_name=EMBEDDING_COLLECTION, data=data)


async def process_fa_criteria() -> None:
    criteria_vulns = await _load_fa_criteria()
    client = await get_db_client()
    await _update_criteria_embeddings(client, criteria_vulns)
    await post_db_checkpoint()
    LOGGER.info("Successfully update criteria embeddings")


def run() -> None:
    logging.basicConfig(level=logging.INFO)
    asyncio.run(process_fa_criteria())
