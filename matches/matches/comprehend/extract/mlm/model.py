import zipfile
from pathlib import Path
from typing import cast

from transformers.modeling_utils import PreTrainedModel
from transformers.models.auto.modeling_auto import (
    AutoModelForSequenceClassification,
)

from matches.context import MATCHES_BUCKET, MODELS_REMOTE_PATH
from matches.utils.aws import download_from_s3

TARGET_MODEL = "column-classification-28-02-2025-v1"
DEFAULT_EXTENSION = ".tar.gz"


async def fetch_model() -> None:
    bucket_name = MATCHES_BUCKET
    object_key = f"{MODELS_REMOTE_PATH}/{TARGET_MODEL}.{DEFAULT_EXTENSION}"
    download_path = f"models/{TARGET_MODEL}.{DEFAULT_EXTENSION}"
    extract_path = download_path.replace(DEFAULT_EXTENSION, "")

    await download_from_s3(bucket_name, object_key, Path(download_path))

    with zipfile.ZipFile(download_path, "r") as zip_ref:
        zip_ref.extractall(extract_path)


async def load_model() -> PreTrainedModel:
    model_local_path = f"models/{TARGET_MODEL}"
    if not Path().exists():
        await fetch_model()

    return cast(
        PreTrainedModel,
        AutoModelForSequenceClassification.from_pretrained(model_local_path),
    )
