import torch
from transformers.models.auto.tokenization_auto import (
    AutoTokenizer,
)

from matches.comprehend.constants import LABEL_MAPPINGS

from .model import load_model


async def predict_column(text: str) -> tuple[str, torch.Tensor]:
    device = torch.device("mps")
    model = await load_model()
    model.to(device)  # type: ignore[arg-type]
    tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")
    inputs = tokenizer(text, return_tensors="pt", padding="max_length", truncation=True).to(device)
    with torch.no_grad():
        outputs = model(**inputs)
        probs = torch.nn.functional.softmax(outputs.logits, dim=-1)
        predicted = list(LABEL_MAPPINGS.keys())[int(probs.argmax().item())]
        return predicted, probs
