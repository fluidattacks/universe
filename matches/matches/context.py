import os

REGION = "us-east-1"
EXTRACTION_MODEL_ID = "us.anthropic.claude-3-7-sonnet-20250219-v1:0"
EXTRACTION_SYSTEM_PROMPT_PATH = os.environ["EXTRACTION_SYSTEM_PROMPT_PATH"]
ENVIRONMENT = os.environ["ENVIRONMENT"]
MATCHES_BUCKET = "integrates" if ENVIRONMENT == "production" else "integrates.dev"
MODELS_REMOTE_PATH = "matches/models"
VOYAGE_API_KEY = os.environ["VOYAGE_API_KEY"]
