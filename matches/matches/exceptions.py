class InvalidResponseLengthError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


class UndefinedExtractionPromptPathError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


class FileDownloadError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


class FileUploadError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)
