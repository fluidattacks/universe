{ fromJsonFile, inputs, projectPath, ... }: {
  inputs = {
    skimsTestsMap = fromJsonFile (projectPath "/skims/test/test_groups.json");
    skimsTestPythonCategories = builtins.attrNames inputs.skimsTestsMap;
    skimsCICategories = builtins.filter (key: key != "_" && key != "all")
      inputs.skimsTestPythonCategories;
    skimsBenchmarkOwasp = builtins.fetchTarball {
      url =
        "https://github.com/OWASP-Benchmark/BenchmarkJava/archive/cbf5bb523ba57545aa3302acec5385467854d557.tar.gz";
      sha256 = "WpWRL3g9nYbPce6+nqs+UZSvJvHnE45Cwu5tChat+C8=";
    };
    skimsNistTestSuites = builtins.fetchTarball {
      url =
        "https://github.com/fluidattacks/NIST-SARD-Test-Suites/archive/7189c65ab6e398180e3f2aa2de683466b4412821.tar.gz";
      sha256 = "CDLX3Xa7nCmzdJdAjlSzdlFIaUx3cg7GPiqc5c8Dj6Q=";
    };
    skimsVulnerableApp = builtins.fetchTarball {
      url =
        "https://github.com/SasanLabs/VulnerableApp/archive/ecfe0a01f661a68b6c88cd7e5baac127df5d7da0.tar.gz";
      sha256 = "ojKTqTAtaEJZ1bU+QAMKXcb1PIFNWP5r78+fYDo41dg=";
    };
    skimsVulnerableJsApp = builtins.fetchTarball {
      url =
        "https://github.com/fluidattacks/vulnerable_js_app/archive/1282fbde196abb5a77235ba4dd5a64f46dac6e52.tar.gz";
      sha256 = "0sc6zx9zf2v5m2a0hfccynyn93mcx97ks6ksdf9larha6l5r977f";
    };
    skimsTreeSitterCSharp = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-c-sharp/archive/362a8a41b265056592a0c3771664a21d23a71392.tar.gz";
      sha256 = "sha256:0w6xdb8m38brhin0bmqsdqggdl95xqs3lbwq7azm5gg94agz9qf1";
    };
    skimsTreeSitterGo = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-go/archive/3c3775faa968158a8b4ac190a7fda867fd5fb748.tar.gz";
      sha256 = "sha256:0yi8if9mqzzcs4qflflz90hhaxkzlq54wia3s0iiqzfqxk24a61g";
    };
    skimsTreeSitterJava = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-java/archive/a1bbe92a6370bb4c15386735fbda12f2b812a923.tar.gz";
      sha256 = "sha256:009nhnn4dv1p8yvhamhr8xh6q0mqj844725l9k81rkzzxqlv4q82";
    };
    skimsTreeSitterJavaScript = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-javascript/archive/3a837b6f3658ca3618f2022f8707e29739c91364.tar.gz";
      sha256 = "sha256:03v1gpr5lnifrk4lns690fviid8p02wn7hfdwp3ynp7lh1cid63a";
    };
    skimsTreeSitterJson = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-json/archive/ee35a6ebefcef0c5c416c0d1ccec7370cfca5a24.tar.gz";
      sha256 = "sha256:0p0fiqi5imxm13s1fs6bhqw6v11n79ri1af3d072zm7jqkcl5mhc";
    };
    skimsTreeSitterPhp = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-php/archive/43aad2b9a98aa8e603ea0cf5bb630728a5591ad8.tar.gz";
      sha256 = "sha256:0qkjp5n3ys0jckg67zcdf4mkb01ilnkqx61wga13ys2infgd8agq";
    };
    skimsTreeSitterPython = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-python/archive/6d14e44ea217bc5bb1c1804180e9818d7d1b8d91.tar.gz";
      sha256 = "sha256:0dzy60m16qb0i9xrwi3vnii3gy1k50prbbgq3362gcm4dj5hcdpy";
    };
    skimsTreeSitterRuby = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-ruby/archive/71bd32fb7607035768799732addba884a37a6210.tar.gz";
      sha256 = "sha256:0c1vs63ydcb3q2wnif18l5irbj6chkcvd3p6dg0vyhklk5acrvca";
    };
    skimsTreeSitterTypeScript = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-typescript/archive/f975a621f4e7f532fe322e13c4f79495e0a7b2e7.tar.gz";
      sha256 = "sha256:0rlhhqp9dv6y0iljb4bf90d89f07zkfnsrxjb6rvw985ibwpjkh9";
    };
    skimsTreeSitterHcl = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter-grammars/tree-sitter-hcl/archive/9e3ec9848f28d26845ba300fd73c740459b83e9b.tar.gz";
      sha256 = "sha256:08sjjmfkkpm2crpdp2gy86daap1fbnj4l1vwvn3zz05gfq2zpkhw";
    };
    skimsTreeSitterKotlin = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter-grammars/tree-sitter-kotlin/archive/e1444cfff56ed9c3e6bccee861a888147415b3a7.tar.gz";
      sha256 = "sha256:09f4q07g3pswmvsa7r6aa9xksw2azdkc4mxf6icb2m0hflbmjykx";
    };
    skimsTreeSitterScala = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter/tree-sitter-scala/archive/d3b9553b47d7ab413a146c4b6498e527265b966e.tar.gz";
      sha256 = "sha256:02awiraj5mmgyi31yzxyxarkkl41qksm3lm41pq9c6bmyqpir2y6";
    };
    skimsTreeSitterYaml = builtins.fetchTarball {
      url =
        "https://github.com/tree-sitter-grammars/tree-sitter-yaml/archive/3975596d84650e8d8ab8ef3128b1b7f0fec324f8.tar.gz";
      sha256 = "1ydh8wjnc6qm22bg5l1bprgqbw17wp8xfk1j6dhb4rlqmais2fm3";
    };
    skimsTreeSitterSwift = builtins.fetchTarball {
      url =
        "https://github.com/alex-pinkus/tree-sitter-swift/archive/57c1c6d6ffa1c44b330182d41717e6fe37430704.tar.gz";
      sha256 = "sha256:18z9rlycls841qvjgs376kqnwds71sm6lbafgjk8pxfqa7w6gwqn";
    };
    skimsTreeSitterDart = builtins.fetchTarball {
      url =
        "https://github.com/UserNobody14/tree-sitter-dart/archive/e81af6ab94a728ed99c30083be72d88e6d56cf9e.tar.gz";
      sha256 = "sha256:0zl46vkm4p1jmivmnpyyzc58fwhx5frfgi0rfxna43h0qxdv62wy";
    };
    sbomTreeSitterGemFileLock = builtins.fetchTarball {
      url =
        "https://github.com/fluidattacks/tree-sitter-gemfilelock/archive/a0c57ba1edbe39ec0327838995d104aab2ed16c3.tar.gz";
      sha256 = "sha256-MBMJX9mi2fj6wfZqYw5VwHhbRZakSRBuqQ4wsIhUnlQ=";
    };
    sbomTreeSitterMixLock = builtins.fetchTarball {
      url =
        "https://github.com/fluidattacks/tree-sitter-mix_lock/archive/8ef8b09bf9b11369ce1d83478af3971e6dfe21b9.tar.gz";
      sha256 = "sha256:1fxj1s6p18qqfb876yxzcy0n3rmp6hipxym3hjnacbaa0a42s0p5";
    };
  };
}
