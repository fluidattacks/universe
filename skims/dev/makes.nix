{ makeSearchPaths, outputs, inputs, ... }: {
  dev = {
    skims = {
      source = [
        outputs."/skims/config/runtime"
        outputs."/common/dev/global_deps"
        (makeSearchPaths { pythonPackage = [ "$PWD/skims/skims" ]; })
      ];
      bin = [
        inputs.nixpkgs.findutils
        inputs.nixpkgs.gnugrep
        inputs.nixpkgs.nodejs_20
        outputs."/common/utils/wait"
        outputs."/common/utils/kill/port"
        outputs."/common/utils/kill/tree"
        outputs."/skims/test/mocks/http"
        outputs."/skims/test/mocks/ssl/safe"
        outputs."/skims/test/mocks/ssl/unsafe"
      ];
    };
  };
}
