# https://github.com/fluidattacks/makes
{ outputs, ... }: {
  deployTerraform = {
    modules = {
      skims = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodSkims"
          outputs."/secretsForEnvFromSops/skims"
          outputs."/secretsForTerraformFromEnv/skims"
        ];
        src = "/skims/infra/src";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    skims = {
      vars = [
        "CACHE_USER_DEFAULT_PASSWORD"
        "CACHE_USER_READ_PASSWORD"
        "CACHE_USER_WRITE_PASSWORD"
      ];
      manifest = "/skims/secrets/dev.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    skims = {
      cache_user_read_password = "CACHE_USER_READ_PASSWORD";
      cache_user_write_password = "CACHE_USER_WRITE_PASSWORD";
      cache_user_default_password = "CACHE_USER_DEFAULT_PASSWORD";
    };
  };
  lintTerraform = {
    modules = {
      skims = {
        setup = [ outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/skims/infra/src";
        version = "1.0";
      };
    };
  };
  testTerraform = {
    modules = {
      skims = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/skims"
          outputs."/secretsForTerraformFromEnv/skims"
        ];
        src = "/skims/infra/src";
        version = "1.0";
      };
    };
  };
}
