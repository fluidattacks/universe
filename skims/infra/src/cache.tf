resource "aws_elasticache_subnet_group" "packages_metadata" {
  name       = "sbom-packages-metadata"
  subnet_ids = [for subnet in data.aws_subnet.main : subnet.id]

  tags = {
    "Name"              = "sbom-packages-metadata"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
    "Access"            = "private"
  }
}

resource "aws_elasticache_replication_group" "packages_metadata" {
  replication_group_id       = "sbom-packages-metadata"
  description                = "Replication group for sbom-packages-metadata"
  engine                     = "redis"
  engine_version             = "7.1"
  node_type                  = "cache.t4g.medium"
  num_cache_clusters         = 1
  subnet_group_name          = aws_elasticache_subnet_group.packages_metadata.name
  security_group_ids         = [aws_security_group.sbom_public_access.id]
  user_group_ids             = [aws_elasticache_user_group.sbom.user_group_id]
  automatic_failover_enabled = false
  port                       = 6379
  network_type               = "ipv4"
  transit_encryption_enabled = true
  at_rest_encryption_enabled = true
  snapshot_retention_limit   = 7

  tags = {
    "Name"              = "sbom-packages-metadata"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
    "Access"            = "private"
  }
}

output "redis_endpoint" {
  value = aws_elasticache_replication_group.packages_metadata.primary_endpoint_address
}

resource "aws_elasticache_user" "default" {
  user_id       = "restricted-default-user"
  user_name     = "default"
  engine        = "REDIS"
  access_string = "off -@all"
  authentication_mode {
    type = "no-password-required"
  }

  timeouts {
    create = "10m"
    update = "10m"
  }
  tags = {
    "Name"              = "aws_elasticache_user_default"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
  }
}

resource "aws_elasticache_user" "readonly" {
  user_id       = "readonly"
  user_name     = "readonly"
  engine        = "REDIS"
  access_string = "on +get ~*"
  authentication_mode {
    type      = "password"
    passwords = [var.cache_user_read_password]
  }

  timeouts {
    create = "15m"
    update = "15m"
  }
  tags = {
    "Name"              = "aws_elasticache_user_readonly"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
  }
}

resource "aws_elasticache_user" "readwrite" {
  user_id       = "readwrite"
  user_name     = "readwrite"
  engine        = "REDIS"
  access_string = "on +@all ~*"
  authentication_mode {
    type      = "password"
    passwords = [var.cache_user_write_password]
  }

  timeouts {
    create = "15m"
    update = "15m"
  }
  tags = {
    "Name"              = "aws_elasticache_user_readwrite"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
  }
}

resource "aws_elasticache_user_group" "sbom" {
  user_group_id = "sbom-packages-metadata-group"
  engine        = "REDIS"
  user_ids = [
    aws_elasticache_user.default.user_id,
    aws_elasticache_user.readonly.user_id,
    aws_elasticache_user.readwrite.user_id
  ]
  tags = {
    "Name"              = "aws_elasticache_user_group_sbom"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
  }
}

resource "aws_security_group" "sbom_public_access" {
  name        = "sbom-cache-sg"
  description = "Allow Redis access"
  vpc_id      = data.aws_vpc.main.id

  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = [for subnet in data.aws_subnet.main : subnet.cidr_block]
  }

  tags = {
    "Name"              = "sbom-cache-sg"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
    "Access"            = "private"
    "NOFLUID"           = "f024_Allow_outbound_traffic_to_any_destination"
  }
}
