{ inputs, makeScript, outputs, ... }: {
  jobs."/skims/coverage" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "skims-coverage";
    searchPaths = {
      bin = [
        inputs.codecov-cli
        inputs.nixpkgs.git
        inputs.nixpkgs.gnugrep
        inputs.nixpkgs.python311Packages.coverage
      ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
