# shellcheck shell=bash

function main {
  local commit_msg
  local module_name
  local report_args=(
    --fail-under='90.00'
    --precision=2
  )

  pushd skims || return 1
  commit_msg="$(git --no-pager log --pretty=format:%B HEAD^..HEAD)"
  module_name=$(echo "$commit_msg" | grep -oP '(?<=-)[^\\]+(?=\\)' || true)

  if [[ -n $module_name ]]; then
    report_args+=(--include="skims/lib_${module_name}/*")
  fi

  coverage combine
  coverage report "${report_args[@]}"
  coverage xml
  popd || return 1

  if [[ -n $module_name ]]; then
    return 0
  fi

  aws_login dev 3600
  sops_export_vars common/secrets/dev.yaml CODECOV_TOKEN

  codecov-cli upload-process --flag skims --dir skims
}

main "${@}"
