# shellcheck shell=bash

function upload {
  echo '[INFO] Running tap'
  tap-json infer-and-adjust \
    < 'benchmark.json' \
    > '.singer'
  echo '[INFO] Running target'
  target-warehouse destroy-and-upload \
    --schema-name "skims_benchmark" \
    --truncate \
    < .singer
}

function main {
  local category="${1-}"
  local extra_flags=("${@:2}")

  skims-benchmark-owasp "${category}" "${extra_flags[@]}"
  if test -z "${category}"; then
    upload
  fi
}

main "${@}"
