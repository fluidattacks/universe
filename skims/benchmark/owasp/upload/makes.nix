{ inputs, makeScript, outputs, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [ tap.json.root target.warehouse.root ];
  env = builtins.map inputs.getRuntime deps;
in {
  jobs."/skims/benchmark/owasp/upload" = makeScript {
    searchPaths = {
      bin = env ++ [ outputs."/skims/benchmark/owasp" ];
      source =
        [ outputs."/common/utils/aws" outputs."/observes/common/db-creds" ];
    };
    name = "skims-benchmark-owasp-upload";
    entrypoint = ./entrypoint.sh;
  };
}
