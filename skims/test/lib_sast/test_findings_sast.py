# noqa: INP001
import tempfile
from pathlib import Path
from unittest.mock import (
    patch,
)

import pytest

from test.lib_cspm.data.azure.side_effects import (
    AzureMockCredentials,
)
from test.utils import (
    check_that_csv_results_match,
    create_config,
    skims,
)


def run_finding(finding: str) -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        path = Path(tmp_dir) / f"{finding}.yaml"
        with path.open("w", encoding="utf-8") as tmpfile:
            template = "skims/test/lib_sast/test_configs/template.yaml"
            tmpfile.write(create_config(finding, template))

        with (
            patch("lib_cspm.aws.utils.run_boto3_fun", return_value={}),
            patch("lib_cspm.gcp.analyze.get_regions", return_value=["asia-east1"]),
            patch("lib_cspm.gcp.utils.run_gcp_client", return_value={}),
            patch(
                "lib_cspm.azure.analyze.ClientSecretCredential",
                AzureMockCredentials,
            ),
            patch(
                "lib_cspm.azure.azure_clients.common.run_azure_client",
                return_value={},
            ),
        ):
            code, stdout, _ = skims("scan", str(path))

        assert code == 0, stdout
        assert "[INFO] Startup work dir is:" in stdout
        assert "[INFO] An output file has been written:" in stdout
        check_that_csv_results_match(finding, "skims/test/lib_sast/results")


def run_multiple_findings_test(findings: list[str]) -> None:
    for finding_code in findings:
        run_finding(finding_code)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_001_025")
def test_sast_findings_001_025() -> None:
    findings = [
        "F001",
        "F002",
        "F004",
        "F005",
        "F007",
        "F008",
        "F009",
        "F010",
        "F012",
        "F015",
        "F016",
        "F017",
        "F020",
        "F021",
        "F024",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_026_050")
def test_sast_findings_026_050() -> None:
    findings = [
        "F027",
        "F029",
        "F031",
        "F034",
        "F035",
        "F037",
        "F042",
        "F043",
        "F044",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_051_075")
def test_sast_findings_051_075() -> None:
    findings = [
        "F052",
        "F055",
        "F056",
        "F058",
        "F060",
        "F062",
        "F063",
        "F065",
        "F067",
        "F068",
        "F071",
        "F073",
        "F075",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_076_099")
def test_sast_findings_076_099() -> None:
    findings = [
        "F078",
        "F079",
        "F081",
        "F083",
        "F085",
        "F086",
        "F089",
        "F091",
        "F096",
        "F097",
        "F098",
        "F099",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_100_125")
def test_sast_findings_100_125() -> None:
    findings = [
        "F100",
        "F101",
        "F106",
        "F107",
        "F108",
        "F109",
        "F111",
        "F112",
        "F115",
        "F117",
        "F123",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_126_150")
def test_sast_findings_126_150() -> None:
    findings = [
        "F128",
        "F130",
        "F134",
        "F135",
        "F136",
        "F143",
        "F146",
        "F148",
        "F149",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_151_175")
def test_sast_findings_151_175() -> None:
    findings = [
        "F151",
        "F152",
        "F153",
        "F156",
        "F157",
        "F159",
        "F160",
        "F164",
        "F165",
        "F168",
        "F169",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_176_199")
def test_sast_findings_176_199() -> None:
    findings = [
        "F177",
        "F183",
        "F188",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_200_225")
def test_sast_findings_200_225() -> None:
    findings = [
        "F203",
        "F211",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_226_250")
def test_sast_findings_226_250() -> None:
    findings = [
        "F234",
        "F235",
        "F236",
        "F239",
        "F246",
        "F247",
        "F249",
        "F250",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_251_275")
def test_sast_findings_251_275() -> None:
    findings = [
        "F256",
        "F258",
        "F259",
        "F262",
        "F265",
        "F266",
        "F267",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_276_299")
def test_sast_findings_276_299() -> None:
    findings = [
        "F280",
        "F281",
        "F282",
        "F297",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_300_325")
def test_sast_findings_300_325() -> None:
    findings = [
        "F300",
        "F309",
        "F313",
        "F320",
        "F325",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_326_350")
def test_sast_findings_326_350() -> None:
    findings = [
        "F332",
        "F333",
        "F335",
        "F338",
        "F343",
        "F344",
        "F346",
        "F350",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_351_375")
def test_sast_findings_351_375() -> None:
    findings = [
        "F353",
        "F354",
        "F358",
        "F359",
        "F362",
        "F363",
        "F368",
        "F371",
        "F372",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_376_399")
def test_sast_findings_376_399() -> None:
    findings = [
        "F380",
        "F385",
        "F390",
        "F394",
        "F395",
        "F396",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_400_425")
def test_sast_findings_400_425() -> None:
    findings = [
        "F400",
        "F401",
        "F402",
        "F403",
        "F404",
        "F405",
        "F406",
        "F407",
        "F408",
        "F412",
        "F413",
        "F414",
        "F416",
        "F418",
        "F421",
        "F423",
        "F425",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("convert_yaml_test_data_to_json")
@pytest.mark.skims_test_group("sast_findings_426_450")
def test_sast_findings_426_450() -> None:
    findings = [
        "F426",
        "F433",
        "F437",
    ]
    run_multiple_findings_test(findings)
