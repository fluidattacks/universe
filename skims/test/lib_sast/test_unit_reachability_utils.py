# noqa: INP001
from typing import TYPE_CHECKING
from unittest.mock import (
    Mock,
    patch,
)

import pytest
from lib_sast.reachability.common import (
    determine_queries_to_execute,
    find_dependency_file,
    get_filtered_queries,
)
from lib_sast.sast_model import (
    GraphShardMetadataLanguage,
)

if TYPE_CHECKING:
    from lib_sast.reachability.types import (
        Query,
    )


@pytest.mark.skims_test_group("all_unittesting")
def test_find_dependency_file() -> None:
    with patch(
        "lib_sast.reachability.common.aux_find_dependency_file",
        return_value="/start/dependency_file",
    ):
        result = find_dependency_file("/start", GraphShardMetadataLanguage.PYTHON)
        assert result == "/start/dependency_file"


@pytest.mark.skims_test_group("all_unittesting")
def test_find_dependency_file_not_found() -> None:
    with patch(
        "lib_sast.reachability.common.aux_find_dependency_file",
        return_value="",
    ):
        result = find_dependency_file("/start", GraphShardMetadataLanguage.PYTHON)
        assert result == ""


@pytest.mark.skims_test_group("all_unittesting")
def test_get_filtered_queries() -> None:
    queries = (Mock(dependency=("package1",), cve="CVE-1234"),)
    packages = {"package1": {"version": "1.0", "id": "pkg1"}}
    adv_db = {"package1": {"CVE-1234": "1.0"}}
    result = get_filtered_queries(queries, packages, adv_db)
    assert len(result) == 1


@pytest.mark.skims_test_group("all_unittesting")
def test_determine_queries_to_execute() -> None:
    with patch(
        "lib_sast.reachability.common.get_filtered_queries",
        return_value=[],
    ):
        queries: dict[GraphShardMetadataLanguage, tuple[Query, ...]] = {
            GraphShardMetadataLanguage.PYTHON: (),
        }
        deps = {"items": {"/path/to/file": {"package1": {"version": "1.0"}}}}
        vuln_ranges = {"package1": {"CVE-1234": "1.0"}}
        result = determine_queries_to_execute(queries, deps, vuln_ranges)
        assert "/path/to/file" in result
        assert GraphShardMetadataLanguage.PYTHON in result["/path/to/file"]
