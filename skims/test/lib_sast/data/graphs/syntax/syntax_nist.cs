
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using Newtonsoft.Json;
using SampleWebApi.Models;
using SampleWebApi.Repositories;
using SampleWebApi.Services;
using System.Data.SqlClient;
using System.Data;
using System.Web;


namespace testcases.CWE89_SQL_Injection{
	class Test_Case : AbstractTestCaseWeb {
    public override void Bad(HttpRequest req, HttpResponse resp)
    {
        string data;
        /* POTENTIAL FLAW: Read data from a querystring using Params.Get */
        data = req.Params.Get("name");
        if (data != null)
        {
            string[] names = data.Split('-');
            int successCount = 0;
            SqlCommand badSqlCommand = null;
            try
            {
                using (SqlConnection dbConnection = IO.GetDBConnection())
                {
                    badSqlCommand.Connection = dbConnection;
                    dbConnection.Open();
                    for (int i = 0; i < names.Length; i++)
                    {
                        /* POTENTIAL FLAW: data concatenated into SQL statement used in CommandText, which could result in SQL Injection */
                        badSqlCommand.CommandText += "update users set hitcount=hitcount+1 where name='" + names[i] + "';";
                    }
                    var affectedRows = badSqlCommand.ExecuteNonQuery();
                    successCount += affectedRows;
                    IO.WriteLine("Succeeded in " + successCount + " out of " + names.Length + " queries.");
                }
            }
            catch (SqlException exceptSql)
            {
                IO.Logger.Log(NLog.LogLevel.Warn, "Error getting database connection", exceptSql);
            }
            finally
            {
                try
                {
                    if (badSqlCommand != null)
                    {
                        badSqlCommand.Dispose();
                    }
                }
                catch (SqlException exceptSql)
                {
                    IO.Logger.Log(NLog.LogLevel.Warn, "Error disposing SqlCommand", exceptSql);
                }
            }
        }
    }
	}

	class cipher{
  	public void Encrypt(byte[] key, byte[] data, MemoryStream target) {
    	byte[] initializationVector = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

		using var aes = new AesCryptoServiceProvider();
		var encryptor = aes.CreateEncryptor(key, initializationVector);

		using var aes2 = new AesCryptoServiceProvider();
    	var encryptor = aes2.CreateEncryptor(key, aes2.IV);

		using var cryptoStream = new CryptoStream(target, encryptor, CryptoStreamMode.Write);
		cryptoStream.Write(data);
		}
	}

	[RoutePrefix("api/house")]
    public class HouseController : ApiController
    {
        private readonly IHouseRepository _houseRepository;
        const int MaxPageSize = 10;
        private readonly IHouseMapper _houseMapper;

        public HouseController(IHouseRepository houseRepository, IHouseMapper houseMapper)
        {
            _houseRepository = houseRepository;
            _houseMapper = houseMapper;
        }

        [HttpGet]
        [EnableQuery(PageSize = MaxPageSize)]
        [Route("")]
        public IHttpActionResult Get(int page = 1, int pageSize = MaxPageSize)
        {
            if (pageSize > MaxPageSize)
            {
                pageSize = MaxPageSize;
            }

            var paginationHeader = new
            {
                totalCount = _houseRepository.GetAll().Count
                // Add more headers here if you want...
            };

            List<HouseEntity> result = _houseRepository.GetAll()
                    .Skip(pageSize * (page - 1))
                    .Take(pageSize)
                    .ToList();

            HttpContext.Current.Response.AppendHeader("X-Pagination", JsonConvert.SerializeObject(paginationHeader));

            return Ok(result.Select(x => _houseMapper.MapToDto(x)));
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Create([FromBody] HouseDto houseDto)
        {
            if (houseDto == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            HouseEntity houseEntity = _houseMapper.MapToEntity(houseDto);

            _houseRepository.Add(houseEntity);

            return CreatedAtRoute("GetSingleHouse", new { id = houseEntity.Id }, _houseMapper.MapToDto(houseEntity));
        }

        [HttpDelete]
        [Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            HouseEntity houseEntityToDelete = _houseRepository.GetSingle(id);

            if (houseEntityToDelete == null)
            {
                return NotFound();
            }

            _houseRepository.Delete(id);

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
