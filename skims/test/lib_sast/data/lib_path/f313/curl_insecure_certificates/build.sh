#!/bin/bash
URL="https://somedangerouswebsite.com"

# unsafe cases
curl --insecure -O "$URL"
curl -v -k "$URL"

# compliant
curl -s "$URL"
