#!/bin/bash
echo "adding host to known host"
mkdir -p ~/.ssh
touch ~/.ssh/known_hosts
ssh-keyscan sftp >> ~/.ssh/known_hosts
echo "run command on remote server"

# Unsafe case hardcoded password
sshpass -p pass sftp foo@sftp

# Safe cases comments or variables
# sshpass -p pass
sshpass -p $"1"
EOF
