import crypto from 'crypto';

require('dotenv').config();

const hcKey: string = "SAFE_PRIVATE_KEY"
var safeKey: string = process.env.HMAC_SECRET_KEY;;

// Following lines must fail
export const hmac = (data: string) => crypto.createHmac('sha256', 'pa4qacea4VK9t9nGv7yZtwmj').update(data).digest('hex');
let hmac2 = crypto.createHmac('sha256', hcKey);

// Safe case:
export const hmac = (data: string) => crypto.createHmac('sha256', safeKey).update(data).digest('hex');
let hmac2 = crypto.createHmac('sha256', safeKey);
