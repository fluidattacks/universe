using System;

class ExampleClass{

    public void VulnerableCases(){
        XmlSchemaCollection xsc = new XmlSchemaCollection();
        xsc.Add("urn: bookstore - schema", "books.xsd");
    }

    public void SafeCases(String command){
        XmlSchemaCollection xsc2 = new XmlSchemaCollection();
        xsc2.Add(command, "books.xsd");

        my_obj = new Collection();
        my_obj.Add("urn: bookstores", "books.xsd");

    }
}
