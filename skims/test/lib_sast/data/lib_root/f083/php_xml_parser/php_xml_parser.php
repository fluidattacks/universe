<?php

function insecure_xml_processing()
{
    $file = $_GET['file'];
    $xml = file_get_contents($file);

    $insecure_xml_1 = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOENT);
    $insecure_xml_2 = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOENT);

    $insecure_doc = new DOMDocument();
    $insecure_doc->load($file, LIBXML_NOENT);
    $insecure_doc->loadXML($xml, LIBXML_NOENT);

    $insecure_reader = new XMLReader();
    $insecure_reader->open($file);
    $insecure_reader->setParserProperty(XMLReader::SUBST_ENTITIES, true);

    echo "Insecure XML processed. Be cautious!";
}

function secure_xml_processing()
{
    $file = $_GET['file'];
    $xml = file_get_contents($file);
    $sanitized = sanitize($xml);

    $secure_xml_1 = simplexml_load_string($xml, 'SimpleXMLElement');
    $secure_xml_2 = simplexml_load_file("no_xxe.xml", 'SimpleXMLElement');
    $secure_xml_3 = simplexml_load_file($sanitized->data, 'SimpleXMLElement', LIBXML_NOENT);

    $secure_doc = new DOMDocument();
    $secure_doc->load($file);
    $secure_doc->loadXML($xml);

    $secure_reader = new XMLReader();
    $secure_reader->open($file);
    $secure_reader->setParserProperty(XMLReader::SUBST_ENTITIES, false);

    echo "Secure XML processed successfully.";
}

?>
