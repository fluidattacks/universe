import { type NextFunction, type Request, type Response } from 'express'

const libxml = require('libxmljs2')
const vm = require('vm')


function handleXmlUpload({ file }: Request, res: Response, next: NextFunction) {
  const safeVar = "1"
  const sandbox = { libxml, file }
  const safeSandbox = {
    libxml, safeVar
  }
  vm.createContext(sandbox)
  vm.createContext(safeSandbox)
  const vuln = vm.runInContext('libxml.parseXml(file, { noblanks: true, noent: true, nocdata: true })', sandbox, { timeout: 2000 })
  const safe = vm.runInContext('libxml.parseXml(file, { noblanks: true, noent: true, nocdata: true })', safeSandbox, { timeout: 2000 })
  const safe2 = vm.runInContext('libxml.parseXml(file, { noblanks: true, noent: true, nocdata: true })', {libxml, safeVar}, { timeout: 2000 })
}
