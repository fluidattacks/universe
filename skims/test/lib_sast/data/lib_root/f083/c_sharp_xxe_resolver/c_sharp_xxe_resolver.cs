using System.Xml;

public static void insecureDecode()
{
    XmlDocument parser = new XmlDocument();
    parser.XmlResolver = new XmlUrlResolver(); // Noncompliant
    parser.LoadXml("xxe.xml");
}

public static void secureDecode()
{
    XmlDocument parser = new XmlDocument();
    parser.XmlResolver = null;
    parser.LoadXml("xxe.xml");
}

public static void secureDecode2()
{
    somethingUnrelated = new XmlUrlResolver();

    XmlDocument parser = new XmlDocument();
    parser.XmlResolver = null;
    parser.LoadXml("xxe.xml");
}
