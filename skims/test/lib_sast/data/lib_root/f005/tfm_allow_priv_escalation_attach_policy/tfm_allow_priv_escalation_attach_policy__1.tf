resource "aws_iam_policy" "example" {
  name        = "example_policy"
  path        = "/"
  description = "A example policy for demonstration"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = "iam:AttachUserPolicy"
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role" "example" {
  name = "example_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

data "aws_iam_policy_document" "vuln_policy_doc" {
  statement {
    effect = "Allow"

    actions = [
      "iam:AttachUserPolicy"
    ]

    resources = [
      "arn:aws:iam:::role/test_role",
    ]
  }
}
resource "aws_iam_policy" "test_policy" {
  name = "test_policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Effect": "Allow",
        "Action": [
            "iam:AttachUserPolicy",
            "iam:Create*"
        ],
        "Resource": [
            "arn:aws:iam:::role/test_role"
        ]
    }
  ]
}
EOF
}
