resource "aws_iam_policy" "unsafe-policy" {
  name        = "example_policy"
  description = "Policy that allows creating and setting policy versions"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "iam:CreatePolicyVersion",
          "iam:SetDefaultPolicyVersion",
        ],
        Resource = "*",
      },
    ],
  })
}


data "aws_iam_policy_document" "unsafe-policy-2" {
  statement {
    effect = "Allow"

    actions = [
      "iam:CreatePolicyVersion",
      "iam:SetDefaultPolicyVersion",
    ]

    resources = [
      "arn:aws:iam:::role/test_role",
    ]
  }
}
resource "aws_iam_policy" "unsafe-policy-3" {
  name = "test_policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Effect": "Allow",
        "Action": [
            "iam:CreatePolicyVersion",
            "iam:SetDefaultPolicyVersion",
        ],
        "Resource": [
            "arn:aws:iam:::role/test_role"
        ]
    }
  ]
}
EOF
}


resource "aws_iam_role" "safe_policy" {
  name = "example_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com",
        },
      },
    ],
  })
}
