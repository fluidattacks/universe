// Source: https://semgrep.dev/r?q=problem-based-packs.insecure-transport.java-stdlib.telnet-request.telnet-request
import org.apache.commons.net.telnet.TelnetClient;

class Bad {
    public void badtelnet1() {
        TelnetClient telnet = new TelnetClient(); // -> Vulnerable
        telnet.connect("rainmaker.wunderground.com");
    }

    public void badtelnet2() {
        TelnetClient telnet = null;
        telnet = new TelnetClient(); // -> Vulnerable
        telnet.connect("rainmaker.wunderground.com");
    }
}

class Ok {
    public void oktelnet1() {
        NotTelnetClient telnet = new NotTelnetClient(); // -> Safe
        telnet.connect("rainmaker.wunderground.com");
    }

    public void oktelnet2() {
        TelnetClientOther telnet = new TelnetClientOther(); // -> Safe
        telnet.connect("rainmaker.wunderground.com");
    }
}
