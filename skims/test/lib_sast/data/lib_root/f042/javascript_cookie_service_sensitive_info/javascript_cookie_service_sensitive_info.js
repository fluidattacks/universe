import { OnInit} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

export const SESSION_ID = 'SessionId';
export const SESSION_TIMESTAMP = 'SessionTime';

export class AsistenciaComponent {
  constructor(route, cookieService){
    return
  }

  ngOnInit(){
    this.route.params.subscribe(params => {
      this.cookieService.set(SESSION_ID, params.sessionid);
      // Safe case
      this.cookieService.set(SESSION_ID, params.timestamp);
    });
  }

}
