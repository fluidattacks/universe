import { OnInit} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

export const SESSION_ID = 'SessionId';

export class AsistenciaComponent implements OnInit {
  public token: string;

  constructor(
    private route: ActivatedRoute,
    private cookieService: CookieService,
  ){}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.cookieService.set(SESSION_ID, params.sessionid);
    });
  }

}
