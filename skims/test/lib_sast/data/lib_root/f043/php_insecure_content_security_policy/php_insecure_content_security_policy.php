<?php

/* ------------------------------- VULNERABLE CASES ------------------------------- */
// Vulnerable cases source: https://csp-evaluator.withgoogle.com/

// NONCOMPLIANT CASES #1:
// 'unsafe-inline' allows the execution of unsafe in-page scripts and event handlers.
header("Content-Security-Policy: script-src 'self' 'unsafe-inline';"); // -> Line must be marked
$cspHeader1 = "Content-Security-Policy: script-src 'self' 'unsafe-inline';";
header($cspHeader1); // -> Line must be marked

// NONCOMPLIANT CASES #2:
// 'unsafe-eval' allows the execution of code injected into DOM APIs such as eval().
header("Content-Security-Policy: script-src 'self' 'unsafe-eval';"); // -> Line must be marked
$cspHeader2 = "Content-Security-Policy: script-src 'self' 'unsafe-eval';";
header($cspHeader2); // -> Line must be marked

// NONCOMPLIANT CASES #3:
// 'data:' URI in script-src allows the execution of unsafe scripts.
header("Content-Security-Policy: script-src 'self' data:;"); // -> Line must be marked
$cspHeader3 = "Content-Security-Policy: script-src 'self' data:;";
header($cspHeader3); // -> Line must be marked

// NONCOMPLIANT CASES #4:
// default-src should not allow '*' as source.
header("Content-Security-Policy: default-src 'self' *;"); // -> Line must be marked
$cspHeader4 = "Content-Security-Policy: default-src 'self' *;";
header($cspHeader4); // -> Line must be marked

// NONCOMPLIANT CASES #5:
// Multiple unsafe sources for script-src.
header("Content-Security-Policy: script-src 'unsafe-inline' 'unsafe-eval' 'self' data: https://www.google.com http://www.google-analytics.com/gtm/js https://*.gstatic.com/feedback/ https://ajax.googleapis.com;"); // -> Line must be marked
$cspHeader5 = "Content-Security-Policy: script-src 'unsafe-inline' 'unsafe-eval' 'self' data: https://www.google.com http://www.google-analytics.com/gtm/js https://*.gstatic.com/feedback/ https://ajax.googleapis.com;";
header($cspHeader5); // -> Line must be marked

// NONCOMPLIANT CASES #6:
// default-src should not allow '*' as source
header("Content-Security-Policy: default-src 'self' *;"); // -> Line must be marked
$cspHeader6 = "Content-Security-Policy: default-src 'self' *;";
header($cspHeader6); // -> Line must be marked

// NONCOMPLIANT CASES #7:
// Endpoints which allow to bypass this CSP.
header("Content-Security-Policy: script-src 'self' https://www.google.com http://www.google-analytics.com/gtm/js https://*.gstatic.com/feedback/ https://ajax.googleapis.com;"); // -> Line must be marked
$cspHeader7 = "Content-Security-Policy: script-src 'self' https://www.google.com http://www.google-analytics.com/gtm/js https://*.gstatic.com/feedback/ https://ajax.googleapis.com;";
header($cspHeader7); // -> Line must be marked

// NONCOMPLIANT CASES #8:
// Combination of the previous vulnerable configurations
header("Content-Security-Policy: script-src 'unsafe-inline' 'unsafe-eval' 'self' data: https://www.google.com http://www.google-analytics.com/gtm/js https://*.gstatic.com/feedback/ https://ajax.googleapis.com; default-src 'self' * 127.0.0.1 https://[2a00:79e0:1b:2:b466:5fd9:dc72:f00e]/foobar;"); // -> Line must be marked
$cspHeader8 = "Content-Security-Policy: script-src 'unsafe-inline' 'unsafe-eval' 'self' data: https://www.google.com http://www.google-analytics.com/gtm/js https://*.gstatic.com/feedback/ https://ajax.googleapis.com; default-src 'self' * 127.0.0.1 https://[2a00:79e0:1b:2:b466:5fd9:dc72:f00e]/foobar;";
header($cspHeader8); // -> Line must be marked


/* ------------------------------- SAFE CASES ------------------------------- */
// Secure case #1. Restricts script sources to self and trusted domains.
header("Content-Security-Policy: script-src 'self' https://trusted-cdn.com;"); // -> Safe line

// Secure case #2. Disallows unsafe-inline styles and restricts sources to self and trusted domains.
header("Content-Security-Policy: style-src 'self' https://trusted-cdn.com;"); // -> Safe line

// Secure case #3. Restricts image sources to self and trusted domains, disallows data URIs.
header("Content-Security-Policy: img-src 'self' https://trusted-cdn.com;"); // -> Safe line

// Secure case #4. Restricts font sources to self and trusted domains.
header("Content-Security-Policy: font-src 'self' https://trusted-cdn.com;"); // -> Safe line

// Secure case #5. Restricts media sources to self and trusted domains.
header("Content-Security-Policy: media-src 'self' https://trusted-cdn.com;"); // -> Safe line

// Secure case #6. Restricts frame sources to self and trusted domains.
header("Content-Security-Policy: frame-src 'self' https://trusted-cdn.com;"); // -> Safe line

// Secure case #7. Disallows all object-src to prevent loading of Flash or Java objects.
header("Content-Security-Policy: object-src 'none';"); // -> Safe line

// Secure case #8. Disallows unsafe-eval in script sources.
header("Content-Security-Policy: script-src 'self';"); // -> Safe line

// Secure case #9. Restricts connect-src to self and trusted domains.
header("Content-Security-Policy: connect-src 'self' https://trusted-api.com;"); // -> Safe line

// Secure case #10. Restricts sources to self and trusted domains, disallows unsafe-inline and unsafe-eval.
header("Content-Security-Policy: default-src 'self'; script-src 'self' https://trusted-cdn.com; img-src 'self' https://trusted-cdn.com; style-src 'self' https://trusted-cdn.com;"); // -> Safe line

?>
