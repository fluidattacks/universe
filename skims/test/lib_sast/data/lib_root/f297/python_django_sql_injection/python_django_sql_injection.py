# type: ignore

from myapp.models import (
    MyModel,
)


# Reference: https://docs.djangoproject.com/en/5.1/ref/models/expressions/
# Vulnerable SQL Query using RawSQL
def get_data_1(request):
    user_input = request.GET.get("user_input")
    return MyModel.objects.filter(
        RawSQL(f"column_name = {user_input}", [])  # -> Vulnerable
    )


# Safe version SQL Query using RawSQL
def get_data_2(request):
    user_input = request.POST.get("user_input")
    return MyModel.objects.filter(
        RawSQL("column_name = %s", [user_input])  # -> Safe
    )


# Vulnerable SQL Query using extra with where param
def get_data_3(request):
    user_input = request.COOKIES.get("user_input")
    return MyModel.objects.extra(
        where=[f"column_name = {user_input}"]  # -> Vulnerable
    )


# Vulnerable SQL Query using extra with select param
def get_data_4(request):
    user_input = request.headers.get("X-User-Input")
    return MyModel.objects.extra(  # -> Vulnerable
        select={"custom_column": f"column_name = {user_input}"}
    )


# Safe version SQL Query using extra
def get_data_5(request):
    user_input = request.data.get("user_input")
    return MyModel.objects.extra(  # -> Safe
        select={"custom_column": "column_name = %s"},
        select_params=[user_input],
    )


# Vulnerable SQL Query using raw
def get_data_6(request):
    user_input = request.GET.get("user_input")
    return MyModel.objects.raw(
        f"SELECT * FROM myapp_mymodel WHERE column_name = {user_input}"
    )  # -> Vulnerable


# Safe version SQL Query using raw
def get_data_7(request):
    user_input = request.POST.get("user_input")
    return MyModel.objects.raw(
        "SELECT * FROM myapp_mymodel WHERE column_name = %s", [user_input]
    )  # -> Safe


# Possibly sanitized case. If the danger symbol comes from an invocation


def get_data_8(request):
    user_input = request.GET.get("user_input")
    approved_input = sanitize(user_input)
    return MyModel.objects.filter(
        RawSQL(f"column_name = {approved_input}", [])  # -> Safe
    )
