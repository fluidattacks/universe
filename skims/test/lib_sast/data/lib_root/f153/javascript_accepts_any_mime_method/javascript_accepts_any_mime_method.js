
function vuln1() {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "https://jsonplaceholder.typicode.com/posts");
  xhr.setRequestHeader("Accept", "*/*");
}

function vuln2() {
  const headers = new Headers();
  headers.append("Accept-Language", "en-US");
  headers.append("Accept", "*/*");
}

function safe1() {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "https://jsonplaceholder.typicode.com/posts");
  xhr.setRequestHeader("Accept", "any/*");
}

const safe2 = () => {
  const instance = new XMLHttpRequest();
  instance.open("POST", "https://jsonplaceholder.typicode.com/posts");
  instance.setRequestHeader("Content-Type", "application/json");;
};

function safe3() {
  const example = new AnyClass();
  example.open("PUT", "https://jsonplaceholder.typicode.com/posts/1");
  example.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  example.set("Accept", "*/*");
}
