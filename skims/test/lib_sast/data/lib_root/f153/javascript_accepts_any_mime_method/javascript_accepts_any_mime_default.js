const axiosAlias = require("axios");
import { $ } from 'jquery';
const ky = require("ky");

async function vulnerableReports() {
  const dangHeaders = {Accept: "*/*",};

  axiosAlias.defaults.baseURL = "https://api.example.com";
  axiosAlias.defaults.headers.post["Accept"] = "*/*";

  let dang_value = "*/*";
  const instance = axiosAlias.create({baseURL: "https://api.example.com"});
  instance.defaults.headers.common["Accept"] = dang_value;


  axiosAlias.get("https://example.com", { headers: dangHeaders, });

  axiosAlias.get("https://example.com", {
    headers: {
      Connection: "keep-alive",
      Accept: "*/*",
    },
  });


  $.ajax({
    url: "https://example.com/api",
    method: "GET",
    headers: {
      Authorization: "Bearer YOUR_ACCESS_TOKEN_HERE",
      Accept: "*/*",
    }
  });

  $.ajax({
    url: "https://example.com/api",
    method: "GET",
    headers: dangHeaders,
  });



  const vuln1 = await fetch("https://example.com", {
    headers: new Headers(dangHeaders),
  });

  const vuln2 = await ky("https://jsonplaceholder.typicode.com/posts", {
    headers: {
      Authorization: "Bearer my-token",
      Accept: "*/*",
      "Content-Type": "application/json",
    },
  });

}


async function safeReports() {
  $.ajax({
    url: "https://example.com/api",
    method: "GET",
    headers: {
      Authorization: "Bearer YOUR_ACCESS_TOKEN_HERE",
    }
  });

  axiosAlias.get("https://example.com", {
    headers: {
      Connection: "keep-alive",
      Accept: "text/html",
    },
  });

  const safe1 = await ky("https://jsonplaceholder.typicode.com/posts", {
    headers: {
      Authorization: "Bearer my-token",
      "Content-Type": "application/json",
    },
  });

}
