function vuln01(): void {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "https://jsonplaceholder.typicode.com/posts");
  xhr.setRequestHeader("Accept", "*/*");
}

function safe01(): void {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "https://jsonplaceholder.typicode.com/posts");
  xhr.setRequestHeader("Accept", "any/*");
}
