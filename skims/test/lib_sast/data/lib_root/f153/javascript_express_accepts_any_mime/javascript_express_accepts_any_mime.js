const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.text({ type: '*/*' }))

app.use(bodyParser.text({ type: 'json' }))
