resource "azurerm_app_service" "safe" {
  name                = "safe"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  app_service_plan_id = azurerm_app_service_plan.example.id
  client_cert_enabled = true

  site_config {
    always_on = true
  }

  auth_settings {
    enabled = true
  }
}

resource "azurerm_app_service" "vulnerable" {
  name                = "vulnerable"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  app_service_plan_id = azurerm_app_service_plan.example.id
  client_cert_enabled = true

  site_config {
    always_on = true
  }
}

resource "azurerm_app_service" "vulnerable2" {
  name                = "vulnerable2"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  app_service_plan_id = azurerm_app_service_plan.example.id
  client_cert_enabled = true

  site_config {
    always_on = true
  }

  auth_settings {
    enabled = false
  }
}
