resource "azurerm_linux_web_app" "vulnerable" {
  name                = "example-app-service"
  service_plan_id     = "plan_id"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  https_only          = true

  site_config {
    always_on = true
  }
  auth_settings {
    enabled = true
  }

}

resource "azurerm_linux_web_app" "vulnerable" {
  name                       = "example-app-service"
  service_plan_id            = "plan_id"
  location                   = azurerm_resource_group.example.location
  resource_group_name        = azurerm_resource_group.example.name
  https_only                 = true
  client_certificate_enabled = false
  site_config {
    always_on = true
  }
  auth_settings {
    enabled = true
  }

}
resource "azurerm_linux_web_app" "vulnerable" {
  name                       = "example-app-service"
  service_plan_id            = "plan_id"
  location                   = azurerm_resource_group.example.location
  resource_group_name        = azurerm_resource_group.example.name
  https_only                 = true
  client_certificate_enabled = true
  site_config {
    always_on = true
  }
  auth_settings {
    enabled = true
  }

}

resource "azurerm_app_service" "safe" {
  name                = "example-app-service"
  app_service_plan_id = "plan_id"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  https_only          = true
  client_cert_enabled = true
  auth_settings {
    enabled = true
  }
}
