resource "azurerm_redis_cache" "safe1" {
  name                = "safe1"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  capacity            = 2
  family              = "C"
  sku_name            = "Standard"
  minimum_tls_version = "1.2"
  redis_configuration {
    authentication_enabled = true
  }
}

resource "azurerm_redis_cache" "safe2" {
  name                = "safe2"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  capacity            = 2
  family              = "C"
  sku_name            = "Standard"
  minimum_tls_version = "1.2"
}

resource "azurerm_redis_cache" "vulnerable" {
  name                = "safe2"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  capacity            = 2
  family              = "C"
  sku_name            = "Standard"
  minimum_tls_version = "1.2"
  redis_configuration {
    authentication_enabled = false
  }
}
