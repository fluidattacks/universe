const axiosAlias = require("axios");
import kyAlias from "ky";

const dangerousCases = async () => {
  const axiosResponse = await axiosAlias.get(`https://example.com`);
  localStorage.setItem("axiosKey", axiosResponse);

  const fetchResponse = await fetch("/movies");
  localStorage.setItem("fetchKey", fetchResponse);

  const kyResponse = await kyAlias("/movies");
  localStorage.setItem("fetchKey", kyResponse);

};


const safeCases = async () => {
  const safeVar = await otherFunction("/example");
  localStorage.setItem("safe", safeVar);
};
