namespace Controllers
{
    public class Calculate
    {
        public static void ProcessRequest(HttpRequest req, HttpResponse res)
        {
            string name = req.QueryString["name"];
            res.Write("Hello " + name);

            string value = req.QueryString["value"];
            res.AddHeader("X-Header", value);
        }
    }

    public class BadRequest1
    {
        public override void Bad(HttpRequest req, HttpResponse resp)
        {
            string data;
            using (TcpClient tcpConn = new TcpClient("host.example.org", 39544))
            {
                using (StreamReader sr = new StreamReader(tcpConn.GetStream()))
                {
                    data = sr.ReadLine();
                }
            }

            resp.StatusCode = 404;
            resp.StatusDescription = "<br>Bad() - Parameter name has value " + data;
        }
    }
}
