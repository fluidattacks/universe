using System;

namespace TestCases
{

    public partial class UnsafeCases : System.Web.UI.Page{

        protected void Page_Load(object sender, EventArgs e){
            // Unsafe case
            string input = Request.Form["in"];
            Response.Write("<HTML>" + input + "</HTML>");
        }
    }

    public class SafeCases
    {
        public static void ProcessRequest2(object sender, EventArgs e)
        {
            Reader customReader = clientCustomReader();
            string name = customReader.ReadLine["name"];
            Response.Write("Hello " + name);
        }
    }
}
