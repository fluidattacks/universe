/*
 * Decompiled with CFR 0.152.
 *
 * Could not load the following classes:
 *  javax.servlet.ServletContext
 *  javax.servlet.ServletException
 *  javax.servlet.http.HttpServlet
 *  javax.servlet.http.HttpServletRequest
 *  javax.servlet.http.HttpServletResponse
 *  org.eclipse.help.internal.search.SearchHit
 *  org.eclipse.help.internal.search.SearchProgressMonitor
 */
package org.eclipse.help.internal.webapp.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.help.internal.search.SearchHit;
import org.eclipse.help.internal.search.SearchProgressMonitor;
import org.eclipse.help.internal.webapp.data.SearchData;
import org.eclipse.help.internal.webapp.data.UrlUtil;
import org.eclipse.help.internal.webapp.parser.SearchParser;
import org.eclipse.help.internal.webapp.utils.SearchXMLGenerator;

public class AdvancedSearchService
extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static final String XID = "xid";

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean boolIsJSON;
        req.setCharacterEncoding("UTF-8");
        resp.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        resp.setContentType("application/xml; charset=UTF-8");
        ServletContext context = req.getSession().getServletContext();
        SearchData searchData = new SearchData(context, req, resp);
        String noCat = req.getParameter("noCategory");
        boolean boolIsCategory = noCat == null || !noCat.equalsIgnoreCase("true");
        String locale = UrlUtil.getLocale(req, resp);
        SearchProgressMonitor pm = SearchProgressMonitor.getProgressMonitor((String)locale);
        while (!pm.isDone()) {
            try {
                Thread.sleep(500L);
            }
            catch (InterruptedException interruptedException) {
                // empty catch block
            }
        }
        searchData.readSearchResults();
        SearchHit[] hits = searchData.getResults();
        String response = SearchXMLGenerator.serialize(hits, boolIsCategory);
        String returnType = req.getParameter("returnType");
        boolean bl = boolIsJSON = returnType != null && returnType.equalsIgnoreCase("json");
        if (boolIsJSON) {
            resp.setContentType("text/plain");
            response = this.getJSONResponse(response);
        }
        PrintWriter writer = resp.getWriter();
        writer.write(response);
    }

}
