// Source: https://semgrep.dev/r?q=gitlab.find_sec_bugs.SAML_IGNORE_COMMENTS-1
import org.opensaml.xml.parse.BasicParserPool;

public class SAMLVulnerableExample {
    // Unsafe configuration allowing XML comments
    public void vulnerableSetup() {
        BasicParserPool parserPool = new BasicParserPool();
        parserPool.setIgnoreComments(false); // -> Vulnerable: Allows XML comments

        // Potential attack payload could include XML comments to manipulate SAML assertion
        String maliciousAssertion =
            "<saml:Assertion>" +
            "  <saml:Subject>legituser<!-- Malicious comment -->attackuser</saml:Subject>" +
            "</saml:Assertion>";
    }
}

public class SAMLVulnerableExample {
    // Unsafe configuration allowing XML comments
    public void vulnerableSetup() {
        BasicParserPool parserPool = new BasicParserPool();
        Boolean ignoreComments = false;
        parserPool.setIgnoreComments(ignoreComments); // -> Vulnerable: Allows XML comments

        // Potential attack payload could include XML comments to manipulate SAML assertion
        String maliciousAssertion =
            "<saml:Assertion>" +
            "  <saml:Subject>legituser<!-- Malicious comment -->attackuser</saml:Subject>" +
            "</saml:Assertion>";
    }
}

public class SAMLSecureExample {
    // Secure configuration (default behavior)
    public void secureSetup() {
        BasicParserPool parserPool = new BasicParserPool();
        // No explicit setIgnoreComments() call - uses default safe configuration

        parserPool.setIgnoreComments(true); // -> Safe

        // Clean SAML assertion without manipulative comments
        String cleanAssertion =
            "<saml:Assertion>" +
            "  <saml:Subject>legituser</saml:Subject>" +
            "</saml:Assertion>";
    }
}

public class SAMLSecureExample {
    // Secure configuration (default behavior)
    public void secureSetup() {
        BasicParserPool parserPool = new BasicParserPool();
        // No explicit setIgnoreComments() call - uses default safe configuration
        Boolean ignoreComments = true;
        parserPool.setIgnoreComments(ignoreComments); // -> Safe

        // Clean SAML assertion without manipulative comments
        String cleanAssertion =
            "<saml:Assertion>" +
            "  <saml:Subject>legituser</saml:Subject>" +
            "</saml:Assertion>";
    }
}

public class SAMLConfig {

    public static void configureParser() {
        BasicParserPool parserPool = new BasicParserPool();
        parserPool.setIgnoreComments(false);  // -> Vulnerable
        parserPool.initialize();
    }
}

public class SAMLConfig {

    public static void configureParser() {
        BasicParserPool parserPool = new BasicParserPool(); // -> Safe
        parserPool.initialize();
    }
}

public class SAMLConfig {

    public static void configureParser() {
        BasicParserPool parserPool = new BasicParserPool();
        parserPool.setIgnoreComments(true);  // -> Safe
        parserPool.initialize();
    }
}

public class OpenSAML2AuthenticationBypassCheckSample {

  public void foo() {
    new StaticBasicParserPool().setIgnoreComments(false); // -> Vulnerable Change "setIgnoreComments" to "true" or remove the call to "setIgnoreComments" to prevent the authentication bypass.
    new StaticBasicParserPool().setIgnoreComments(true); // -> Safe

    new BasicParserPool().setIgnoreComments(false); // -> Vulnerable Change "setIgnoreComments" to "true" or remove the call to "setIgnoreComments" to prevent the authentication bypass.
    new BasicParserPool().setIgnoreComments(true); // -> Safe
  }
}
