import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebPage;

public class WicketXssTestPage extends WebPage {

    private void vulnerableMethod() {
        Component component = new Component("vulnerableComponent") {
            @Override
            protected void onRender() {
                // Custom rendering logic
            }
        };
        component.setEscapeModelStrings(false); // -> Vulnerable
        add(component);
    }

    private void nonVulnerableMethod() {
        Component component = new Component("safeComponent") {
            @Override
            protected void onRender() {
                // Custom rendering logic
            }
        };
        component.setEscapeModelStrings(true); // -> Safe
        add(component);
    }

    private void defaultBehaviorMethod() {
        Component component = new Component("defaultComponent") {
            @Override
            protected void onRender() {
                // Custom rendering logic
            }
        };
        // No explicit call to setEscapeModelStrings, uses default (true)
        add(component);
    }

    private void toggleVulnerabilityMethod() {
        Component component = new Component("toggleComponent") {
            @Override
            protected void onRender() {
                // Custom rendering logic
            }
        };
        component.setEscapeModelStrings(true); // Initially safe
        component.setEscapeModelStrings(false); // -> Vulnerable
        add(component);
    }

    // Source: https://github.com/peicheng/Jetwick/blob/d48622b8becd3b2ae83ae92bf16972a8a29159a8/src/main/java/de/jetwick/wikipedia/WikipediaPanel.java#L57
    @Override
    public void populateItem(final ListItem item) {
        final WikiEntry url = (WikiEntry) item.getModelObject();

        String str = url.getTitle();
        if (str.length() > 40)
            str = str.substring(0, 37).trim() + "..";

        ExternalLink link = new ExternalLink("urlLink", url.getUrl(), str);
        item.add(link);

        Label label = new Label("urlLabel", url.getText());
        label.setEscapeModelStrings(false); // -> Vulnerable
        item.add(label);
    }

    // Source: https://github.com/mallowlabs/gitblit/blob/13a3f5bc3e2d25fc76850f86070dc34efe60d77a/src/com/gitblit/wicket/pages/RepositoryPage.java#L323
    protected void addFullText(String wicketId, String text, boolean substituteRegex) {
      String html = StringUtils.escapeForHtml(text, true);
      if (substituteRegex) {
        html = GitBlit.self().processCommitMessage(repositoryName, text);
      } else {
        html = StringUtils.breakLinesForHtml(html);
      }
      add(new Label(wicketId, html).setEscapeModelStrings(false)); // -> Vulnerable
    }
}
