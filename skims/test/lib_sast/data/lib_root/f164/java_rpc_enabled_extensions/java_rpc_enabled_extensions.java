// Source: https://semgrep.dev/r?q=gitlab.find_sec_bugs.RPC_ENABLED_EXTENSIONS-1
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;

public class VulnerableServerConfig {
    public static void main(String[] args) {
        XmlRpcServerConfigImpl serverConfig = new XmlRpcServerConfigImpl();
        serverConfig.setEnabledForExtensions(true); // -> Vulnerable
    }
}

public class VulnerableServerConfig2 {
    public static void main(String[] args) {
        XmlRpcServerConfigImpl serverConfig = new XmlRpcServerConfigImpl();
        Boolean isEnabled = true;
        serverConfig.setEnabledForExtensions(isEnabled); // -> Vulnerable
    }
}

public class VulnerableClientConfig {
    public static void main(String[] args) {
        XmlRpcClientConfigImpl clientConfig = new XmlRpcClientConfigImpl();
        clientConfig.setEnabledForExtensions(true); // -> Vulnerable
    }
}

public class VulnerableClientConfig2 {
    public static void main(String[] args) {
        XmlRpcClientConfigImpl clientConfig = new XmlRpcClientConfigImpl();
        Boolean isEnabled = true;
        clientConfig.setEnabledForExtensions(isEnabled); // -> Vulnerable
    }
}

public class SecureServerConfig {
    public static void main(String[] args) {
        XmlRpcServerConfigImpl serverConfig = new XmlRpcServerConfigImpl();
        serverConfig.setEnabledForExtensions(false); // -> Safe
    }
}

public class SecureServerConfig2 {
    public static void main(String[] args) {
        XmlRpcServerConfigImpl serverConfig = new XmlRpcServerConfigImpl();
        Boolean isEnabled = false;
        serverConfig.setEnabledForExtensions(isEnabled); // -> Safe
    }
}

public class SecureClientConfig {
    public static void main(String[] args) {
        XmlRpcClientConfigImpl clientConfig = new XmlRpcClientConfigImpl();
        clientConfig.setEnabledForExtensions(false); // -> Safe
    }
}

public class SecureClientConfig {
    public static void main(String[] args) {
        XmlRpcClientConfigImpl clientConfig = new XmlRpcClientConfigImpl();
        Boolean isEnabled = false;
        clientConfig.setEnabledForExtensions(isEnabled); // -> Safe
    }
}

public class DefaultServerConfig {
    public static void main(String[] args) {
        XmlRpcServerConfigImpl serverConfig = new XmlRpcServerConfigImpl(); // -> Safe
    }
}
