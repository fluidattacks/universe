using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text.Encodings.Web;


// Non Compliant
public class LogController : Controller
{
    [HttpGet]
    public IActionResult LogDataInsecure(HttpRequest request)
    {
        string logData = request.QueryString["logData"];
        StreamWriter sw = new StreamWriter("log.txt", true);
        sw.WriteLine(logData);
    }

    [HttpGet]
    public IActionResult LogDataInsecure2(HttpRequest request)
    {
        string logData = request.Headers["logData"];
        string sanitizedLogData = _htmlEncoder.Encode(logData);
        StreamWriter sw = new StreamWriter("log.txt", true);
        sw.WriteLine(sanitizedLogData);
        return Ok();
    }

    [HttpGet]
    public IActionResult LogDataInsecure2(HttpRequest request)
    {
        string logData = request.Headers["logData"];
        string data = logData.Replace("n", "").Replace("r", "");
        StreamWriter sw = new StreamWriter("log.txt", true);
        sw.WriteLine(data);
        return Ok();
    }


}
