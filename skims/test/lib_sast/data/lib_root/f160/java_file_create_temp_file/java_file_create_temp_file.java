import java.io.File;
import java.nio.file.Files.*;

public class Test {
    public static void main(String directoryPath){
        // Unsafe
        File.createTempFile(directoryPath, null);
        // Safe
        File outputFile = java.nio.file.Files.createTempFile(directoryPath, null);
        File.createTempFile(directoryPath, "xxx");
        createTempFile(directoryPath, "xxx");

    }
}
