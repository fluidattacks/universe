resource "azurerm_network_security_group" "example1" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS TCP allowed from any source
  security_rule {
    name                       = "NetBIOS-TCP-Unsafe"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "139"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  # Safe: NetBIOS TCP allowed only from internal source
  security_rule {
    name                       = "NetBIOS-TCP-Safe"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "139"
    source_address_prefix      = "10.0.0.0/24"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "example2" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS UDP allowed from any source
  security_rule {
    name                       = "NetBIOS-UDP-Unsafe"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "137-138"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  # Safe: NetBIOS UDP denied explicitly
  security_rule {
    name                       = "NetBIOS-UDP-Denied"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "137-138"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "azurerm_network_security_group" "example3" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Safe: NetBIOS UDP allowed but only from internal network
  security_rule {
    name                       = "NetBIOS-UDP-Safe"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "137-138"
    source_address_prefix      = "192.168.1.0/24"
    destination_address_prefix = "*"
  }

  # Safe: Non-NetBIOS TCP allowed
  security_rule {
    name                       = "Non-NetBIOS-TCP"
    priority                   = 105
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "azurerm_network_security_group" "example4" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS allowed explicitly from Internet
  security_rule {
    name                       = "NetBIOS-Explicit-Internet"
    priority                   = 106
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "139"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }

  # Safe: SSH on port 22 (Non-NetBIOS)
  security_rule {
    name                       = "SSH-Allowed"
    priority                   = 107
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "azurerm_network_security_group" "example5" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS rule but in an open port range
  security_rule {
    name                       = "NetBIOS-In-Range"
    priority                   = 108
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "135-139"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  # Safe: Port range not including NetBIOS
  security_rule {
    name                       = "Non-NetBIOS-Range"
    priority                   = 109
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "140-150"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "example6" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS TCP allowed from any source
  security_rule {
    name                       = "NetBIOS-TCP-Unsafe"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["139", "445"]
    source_address_prefixes    = ["*"]
    destination_address_prefix = "*"
  }

  # Safe: NetBIOS TCP allowed only from internal source
  security_rule {
    name                       = "NetBIOS-TCP-Safe"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["135-139", "445", "1024-1030"]
    source_address_prefixes    = ["203.0.113.10", "198.51.100.25", "192.0.2.30"]
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "example7" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS UDP allowed from any source
  security_rule {
    name                       = "NetBIOS-UDP-Unsafe"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["137-138", "4500-4600"]
    source_address_prefixes    = ["*", "Internet"]
    destination_address_prefix = "*"
  }

  # Safe: NetBIOS UDP denied explicitly
  security_rule {
    name                       = "NetBIOS-UDP-Denied"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["137-138", "1500-1510"]
    source_address_prefixes    = ["*", "203.0.113.50"]
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "example8" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Safe: NetBIOS UDP allowed but only from internal network
  security_rule {
    name                       = "NetBIOS-UDP-Safe"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["137-138", "2000-2010"]
    source_address_prefixes    = ["192.168.1.0/24", "10.0.0.0/16"]
    destination_address_prefix = "*"
  }

  # Safe: Non-NetBIOS TCP allowed
  security_rule {
    name                       = "Non-NetBIOS-TCP"
    priority                   = 105
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["8080", "9090"]
    source_address_prefixes    = ["*", "172.16.0.0/12"]
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "example9" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS allowed explicitly from Internet
  security_rule {
    name                       = "NetBIOS-Explicit-Internet"
    priority                   = 106
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["139", "1500-1510"]
    source_address_prefixes    = ["Internet", "203.0.113.1"]
    destination_address_prefix = "*"
  }

  # Safe: SSH on port 22 (Non-NetBIOS)
  security_rule {
    name                       = "SSH-Allowed"
    priority                   = 107
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22", "2222"]
    source_address_prefixes    = ["*", "192.168.100.1"]
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "example10" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  # Unsafe: NetBIOS rule but in an open port range
  security_rule {
    name                       = "NetBIOS-In-Range"
    priority                   = 108
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["135-139", "1000-1050"]
    source_address_prefixes    = ["*", "198.51.100.5"]
    destination_address_prefix = "*"
  }

  # Safe: Port range not including NetBIOS
  security_rule {
    name                       = "Non-NetBIOS-Range"
    priority                   = 109
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["140-150", "5000-5050"]
    source_address_prefixes    = ["*", "10.10.10.10"]
    destination_address_prefix = "*"
  }
}
