// VULNERABLE CASES
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (!env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage(); // -> Vulnerable
    }
    else
    {
        app.UseExceptionHandler("/Error");
    }
    app.UseHsts();
}


public void Configure1(IApplicationBuilder app, IWebHostEnvironment env)
{
    var dev = env.IsDevelopment();
    if (!dev)
    {
        app.UseDeveloperExceptionPage(); // -> Vulnerable
    }
    else
    {
        app.UseExceptionHandler("/Error");
    }
    app.UseHsts();
}


public void Configure2(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (env.IsDevelopment())
    {
        app.UseExceptionHandler("/Error");
    }
    else
    {
        app.UseDeveloperExceptionPage(); // -> Vulnerable
    }
    app.UseHsts();
}


public void Configure3(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (true)
    {
        app.UseDeveloperExceptionPage(); // -> Vulnerable
    }
    app.UseHsts();
}


public void Configure4(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (test_var)
    {
        app.UseDeveloperExceptionPage(); // -> Vulnerable
    }
    else
    {
        app.UseExceptionHandler("/Error");
    }

    app.UseHsts();
}


public void Configure5(IApplicationBuilder app, IHostingEnvironment env)
{
    app.UseDeveloperExceptionPage(); // -> Vulnerable
    app.UseHsts();
    app.UseHttpsRedirection();
    app.UseStaticFiles();

    app.UseRouting();

    app.UseAuthentication();
    app.UseAuthorization();

    app.UseSession();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapRazorPages();
    });

}


// SAFE CASES
public void Configure6(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage(); // -> Safe
    }
    else
    {
        app.UseExceptionHandler("/Error");
    }
    app.UseHsts();
}


public void Configure7(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (!env.IsDevelopment())
    {
        app.UseExceptionHandler("/Error");
    }
    else
    {
        app.UseDeveloperExceptionPage(); // -> Safe
    }
    app.UseHsts();
}


public void Configure8(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (!env.IsDevelopment())
    {
        app.UseHsts();
    }
    else
    {
        app.UseDeveloperExceptionPage(); // -> Safe
    }
    app.UseHttpsRedirection();
    app.UseStaticFiles();
    app.UseRouting();
    app.UseAuthentication();
    app.UseAuthorization();
    app.UseSession();
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapRazorPages();
    });

}


public void Configure9(IApplicationBuilder app, IWebHostEnvironment env)
{
    var dev = env.IsDevelopment();
    if (dev)
    {
        app.UseDeveloperExceptionPage(); // -> Safe
    }
    else
    {
        app.UseExceptionHandler("/Error");
    }
    app.UseHsts();
}


public void Configure9(IApplicationBuilder app, IWebHostEnvironment env)
{
    var dev = env.IsDevelopment();
    if (!dev)
    {
        app.UseExceptionHandler("/Error");
    }
    else
    {
        app.UseDeveloperExceptionPage(); // -> Safe
    }
    app.UseHsts();
}
