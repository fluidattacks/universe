using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class viewuser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyExpConnectionString"].ToString());
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter();
        string userId = Request.QueryString["userid"];
        string sqlQuery = string.Format("SELECT name FROM user_info WHERE userID={0}", userId);
        SqlCommand cmd = new SqlCommand(sqlQuery, conn);

    }

    protected void Safe_Page_Load(object sender, EventArgs e)
    {
        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyExpConnectionString"].ToString());
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter();
        string userId = Request.QueryString["userid"];
        string sqlQuery = "SELECT name FROM user_info WHERE userID = @UserID";
        SqlCommand cmd = new SqlCommand(sqlQuery, conn);

    }
}
