const express = require('express');
const axios = require('axios');
const validUrl = require('valid-url');

const app = express();

app.get('/profile', (req, res) => {
  console.log('Received request for /profile');

  // Simulated profile data
  const profileData = {
    name: 'John Doe',
    role: 'Developer'
  };

  res.json(profileData);
  console.log('Sent profile data response');
});

app.get('/fetch-data', async (req, res) => {
  const url = req.query.url;
  console.log(`Received request for /fetch-data with URL: ${url}`);

  try {
    const response = await axios.get(url); // -> Vulnerable
    res.send(response.data);
    console.log(`Data fetched and sent for URL: ${url}`);
  } catch (error) {
    console.error(`Error fetching data from URL: ${url}`, error);
    res.status(500).send('Error fetching data');
  }
});


function isValidUrl(userUrl) {
  try {
    const parsedUrl = new URL(userUrl);
    const valid_domains = ['example.com', 'example.org', 'example.net'];
    return ['http:', 'https:'].includes(parsedUrl.protocol) && valid_domains.includes(parsedUrl.hostname);
  } catch {
    return false;
  }
}


app.get('/fetch-data-safe', async (req, res) => {
  const url = req.query.url;
  console.log(`Received request for /fetch-data with URL: ${url}`);

  if (!isValidUrl(url)) {
    console.error(`Invalid URL provided: ${url}`);
    return res.status(400).send('Invalid URL');
  }

  try {
    const response = await axios.get(url); // -> Safe
    res.send(response.data);
    console.log(`Data fetched and sent for URL: ${url}`);
  } catch (error) {
    console.error(`Error fetching data from URL: ${url}`, error.message);
    res.status(500).send('Error fetching data');
  }
});

app.post('/proxy-post', async (req, res) => {
  const { url, data } = req.body;
  console.log(`Received POST request for /proxy-post with URL: ${url}`);

  try {
    const response = await axios.post(url, data); // -> Vulnerable
    res.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error posting data to URL: ${url}`, error);
    res.status(500).send('Error posting data');
  }
});

app.post('/proxy-post-safe', async (req, res) => {
  const { url, data } = req.body;
  console.log(`Received POST request for /proxy-post with URL: ${url}`);

  if (!isValidUrl(url)) {
    console.error(`Invalid or untrusted URL provided: ${url}`);
    return res.status(400).send('Invalid or untrusted URL');
  }

  try {
    const response = await axios.post(url, data); // -> Safe
    response.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error posting data to URL: ${url}`, error.message);
    res.status(500).send('Error posting data');
  }
});

app.post('/proxy-request', async (request, response) => {
  const { url, method, data } = request.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  try {
    const response = await axios.request({ // -> Vulnerable
      url: url,
      method: method || 'POST',
      data: data,
    });
    response.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error making request to URL: ${url}`, error);
    response.status(500).send('Error making request');
  }
});


app.post('/proxy-request-safe', async (request, response) => {
  const { url, method, data } = request.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  if (!isValidUrl(url)) {
    console.error(`Invalid or untrusted URL provided: ${url}`);
    return response.status(400).send('Invalid or untrusted URL');
  }

  const allowedMethods = ['POST', 'GET'];
  if (!allowedMethods.includes(method)) {
    console.error(`Method not allowed: ${method}`);
    return response.status(405).send('Method not allowed');
  }

  try {
    const response = await axios.request({ // -> Safe
      url: url,
      method: method || 'POST',
      data: data,
    });
    response.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error making request to URL: ${url}`, error.message);
    response.status(500).send('Error making request');
  }
});

app.post('/proxy-request-vulnerable', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  try {
    const response = await axios({ // -> Vulnerable
      url: url,
      method: method || 'POST',
      data: data,
    });
    res.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error making request to URL: ${url}`, error);
    res.status(500).send('Error making request');
  }
});

app.post('/proxy-request-safe', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  if (!isValidUrl(url)) {
    console.error(`Invalid or untrusted URL provided: ${url}`);
    return res.status(400).send('Invalid or untrusted URL');
  }

  if (!allowedMethods.includes(method)) {
    console.error(`Method not allowed: ${method}`);
    return res.status(405).send('Method not allowed');
  }

  try {
    const response = await axios({ //  -> Safe
      url: url,
      method: method || 'POST',
      data: data,
    });
    res.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error making request to URL: ${url}`, error.message);
    res.status(500).send('Error making request');
  }
});

const ALLOWED_DOMAINS = ['example.com', 'mytrusteddomain.com'];

app.post('/proxy-request', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  if (validUrl.isUri(url) && ALLOWED_DOMAINS.some(domain => url.includes(domain))) {
    try {
      const response = await axios({ // -> Safe
        url: url,
        method: method || 'POST',
        data: data,
      });
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  } else {
    console.error(`Invalid or unauthorized URL: ${url}`);
    res.status(400).send('Invalid or unauthorized URL');
  }
});

app.post('/proxy-request', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  if (!validUrl.isUri(url) || !ALLOWED_DOMAINS.some(domain => url.includes(domain))) {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  } else {
    try {
      const response = await axios({ // -> Safe
        url: url,
        method: method || 'POST',
        data: data,
      });
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  }
});

app.post('/proxy-request', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  if (!validUrl.isUri(url) || !ALLOWED_DOMAINS.some(domain => url.includes(domain))) {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }

  try {
    const response = await axios({ // -> Safe
      url: url,
      method: method || 'POST',
      data: data,
    });
    res.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error making request to URL: ${url}`, error);
    res.status(500).send('Error making request');
  }

});

app.post('/proxy-request', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  if (!validUrl.isUri(url) || !ALLOWED_DOMAINS.some(domain => url.includes(domain))) {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }

  try {
    const response = await axios({ // -> Safe
      url: url,
      method: method || 'POST',
      data: data,
    });
    res.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error making request to URL: ${url}`, error);
    res.status(500).send('Error making request');
  }

});

app.post('/proxy-request-method-safe', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  const isAllowedUrl = () => {
    return validUrl.isUri(url) && ALLOWED_DOMAINS.some(domain => url.includes(domain));
  };

  if (isAllowedUrl()) {
    try {
      const response = await axios({ // -> Safe
        url: url,
        method: method || 'POST',
        data: data,
      });
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  } else {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }

});


app.post('/proxy-request-method-vuln', async (req, res) => {
  const { url, method, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  const isAllowedUrl = () => {
    // Other logic not related to URL variable
    1 + 1;
    return true;
  };

  if (isAllowedUrl()) {
    try {
      const response = await axios({ // -> Vulnerable
        url: url,
        method: method || 'POST',
        data: data,
      });
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  } else {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }

});

app.post('/proxy-request-safe-includes', async (req, res) => {
  const { url, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url}`);


  if (url.includes('http://localhost:3000') || url.includes('http://localhost:3001')) {
    try {
      const response = await axios.post(url, data); // -> Safe
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  } else {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }
});

app.post('/proxy-request-method-vulnerable', async (req, res) => {
  const { url, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url}`);

  if (!url.includes('http://localhost')) {
    try {
      const response = await axios.post(url, data); // -> Safe (to avoid FP)
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  } else {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }
});

app.get('/fetch-data', async (req, res) => {
  console.log(`Received request for /fetch-data with URL: ${req.query.url}`);

  try {
    const response = await axios.get(req.query.url); // -> Vulnerable
    res.send(response.data);
    console.log(`Data fetched and sent for URL: ${req.query.url}`);
  } catch (error) {
    console.error(`Error fetching data from URL: ${req.query.url}`, error);
    res.status(500).send('Error fetching data');
  }
});

app.post('/proxy-request-method-vulnerable', async (req, res) => {
  const { url, data } = notARequest.body;
  console.log(`Received request for /proxy-request with URL: ${url}`);

  if (!url.includes('http://localhost')) {
    try {
      const response = await axios.put(url, data); // -> Safe
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  } else {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }
});

app.post('/proxy-request-method-vulnerable', async (req, res) => {
  const { url, data } = req.body;
  console.log(`Received request for /proxy-request with URL: ${url}`);

  if (!url.includes('http://localhost')) {
    try {
      const response = await axios.put(url, data); // -> Safe (to avoid FP)
      res.send(response.data);
      console.log(`Data sent to URL: ${url}`);
    } catch (error) {
      console.error(`Error making request to URL: ${url}`, error);
      res.status(500).send('Error making request');
    }
  } else {
    console.error(`Invalid or unauthorized URL: ${url}`);
    return res.status(400).send('Invalid or unauthorized URL');
  }
});

app.post('/proxy-request', async (request, response) => {
  const { url, method, data } = request.body;
  console.log(`Received request for /proxy-request with URL: ${url} and method: ${method}`);

  try {
    const response = await axios.request({ // -> Safe
      noturl: url,
      method: method || 'POST',
      data: data,
    });
    response.send(response.data);
    console.log(`Data sent to URL: ${url}`);
  } catch (error) {
    console.error(`Error making request to URL: ${url}`, error);
    response.status(500).send('Error making request');
  }
});

app.listen(3000, () => {
  console.log('Server running on port 3000');
});
