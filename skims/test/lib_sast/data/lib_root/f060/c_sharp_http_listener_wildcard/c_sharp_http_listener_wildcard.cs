using System;
using System.Net;

// Source: https://semgrep.dev/r?q=csharp.lang.security.http.http-listener-wildcard-bindings.http-listener-wildcard-bindings
namespace HttpListenerWildcard {
    class MyBadHttpListener {
        public static void HttpListenerWildcard() {
            HttpListener listener = new HttpListener();

            listener.Prefixes.Add("http://*:8080"); // -> Vulnerable

            listener.Prefixes.Add("http://+:8080"); // -> Vulnerable

            listener.Prefixes.Add("https://*:8080"); // -> Vulnerable

            listener.Prefixes.Add("https://+:8080"); // -> Vulnerable

            listener.Prefixes.Add("https://*.com:8080"); // -> Vulnerable

            listener.Prefixes.Add("https://0.0.0.0:8080"); // -> Safe

            listener.Prefixes.Add("http://www.contoso.com:8080/"); // -> Safe

            listener.Prefixes.Add("http://*.test.com:8080"); // -> Safe

            listener.Start();
        }
    }
}
