<?php

use Illuminate\Support\Facades\Http;

$opt = false;

# No compliant
$vuln_response_1 = Http::withOptions([
    'verify' => false,
])->get('https://insecure-server.com');


$vuln_response_2 = Http::withOptions([
    'verify' => $opt,
])->post('https://insecure-server.com');


# Compliant

$secure_response_1 = Http::withOptions([
    'verify' => true,
])->get('https://secure-server.com');

$undefined_context = stream_context_create($options);

?>
