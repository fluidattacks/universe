// Source: https://semgrep.dev/r?q=java.lang.security.audit.crypto.ssl.insecure-hostname-verifier.insecure-hostname-verifier
package verify;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;


public class LocalHost {
    public boolean verify(final String hostname, final SSLSession session) {
        return hostname.equals("localhost");
    }
}
