// Source: https://semgrep.dev/r?q=java.lang.security.audit.crypto.ssl.insecure-hostname-verifier.insecure-hostname-verifier
package verify;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

public class AllHosts implements HostnameVerifier {
    public boolean verify(final String hostname, final SSLSession session) { // -> Vulnerable
        return true;
    }
}

public class AllHosts implements HostnameVerifier {
    public boolean verify(final String hostname, final SSLSession session) { // -> Vulnerable
        // Just a comment
        return true;
    }
}

public class LocalHost implements HostnameVerifier {
    public boolean verify(final String hostname, final SSLSession session) { // -> Safe
        return hostname.equals("localhost");
    }
}

// cf. https://stackoverflow.com/questions/2642777/trusting-all-certificates-using-httpclient-over-https
public class InlineVerifier {
    public InlineVerifier() {
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
            public boolean verify(String hostname, SSLSession session) {  // -> Vulnerable
                return true;
            }
        });
    }
}

public class SpecificHosts implements HostnameVerifier {
    public boolean verify(final String hostname, final SSLSession session) { // -> Safe
        return hostname.equals("example.com") || hostname.equals("api.example.com");
    }
}

public class DefaultHostVerifier implements HostnameVerifier {
    private final HostnameVerifier defaultVerifier = HttpsURLConnection.getDefaultHostnameVerifier();

    public boolean verify(final String hostname, final SSLSession session) { // -> Safe
        return defaultVerifier.verify(hostname, session);
    }
}
