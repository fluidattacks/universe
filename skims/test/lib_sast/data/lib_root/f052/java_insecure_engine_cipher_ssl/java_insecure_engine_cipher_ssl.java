import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

public class Unsafe {
	private static final String SSL="TLSv1";

	public void createSSLEngine() {

		serverEngine = serverSslContext.createSSLEngine();
		serverEngine.setEnabledProtocols(new String[] { SSL });

	}
}

public class Safe {

	public void createSSLEngine() {

		serverEngine = serverSslContext.createSSLEngine();
		serverEngine.setEnabledProtocols(new String[] { "TLSv1.2" });

	}
}
