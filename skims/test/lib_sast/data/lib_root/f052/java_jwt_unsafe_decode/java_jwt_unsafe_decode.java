import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtDecodingTest {

	public void mustFailDecode() {

		// Chained method unsafe implementation
		// Using .parse() is insecure because it doesn't throw
		// Exceptions for invalid or missing signatures.
		Jwts.parser()
    	.setSigningKey(SECRET_KEY)
      .parse(token)
      .getBody();
	}

	public String mustFailPlainDecode() {

		// Plain unsafe implementation
		JwtParserBuilder parseBuilder = Jwts.parserBuilder();
		JwtParserBuilder parseBuilderSign = parseBuilder.setSigningKey(KEY);

		JwtParser parser = parseBuilderSign.build();
		JwtParser parsed = parser.parse(TOKEN);

		return parser.getBody();
	}

	public void mustPassDecode() {

		// Chained method safe implementation
		Jwts.parser()
			.setSigningKey(SECRET_KEY)
			.parseClaimsJws(TOKEN)
			.getBody();
	}

	public String mustPassPlainDecode(String privateKey) {

		// Plain safe implementation
		JwtParserBuilder parseBuilder = Jwts.parserBuilder();
		JwtParserBuilder parseBuilderSign = parseBuilder.setSigningKey(privateKey);

		JwtParser parser = parseBuilderSign.build();

		return parser.parseClaimsJws(TOKEN);
	}
}
