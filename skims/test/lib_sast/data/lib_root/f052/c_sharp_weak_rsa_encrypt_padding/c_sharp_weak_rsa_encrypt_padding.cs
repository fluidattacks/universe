using System;
using System.Security.Cryptography;

// Source: https://semgrep.dev/r?q=csharp.dotnet.security.use_weak_rsa_encryption_padding.use_weak_rsa_encryption_padding
public class RSAEncryption
{
	public static void EncryptWithBadPadding1()
	{
		RSA key = RSA.Create();
		byte[] msg = new byte[16];
		Type t = typeof(byte[]);
		RSAPKCS1KeyExchangeFormatter formatter = new RSAPKCS1KeyExchangeFormatter(key);
		byte[] cipherText = formatter.CreateKeyExchange(msg, t); // -> Vulnerable
	}

	public static void DecryptWithBadPadding()
	{
		RSA key = RSA.Create();
		byte[] ciphertext = new byte[16];
		var deformatter = new RSAPKCS1KeyExchangeDeformatter(key);
		var plaintext = deformatter.DecryptKeyExchange(ciphertext); // -> Vulnerable
	}

	public static void EncryptWithBadPadding2()
	{
		RSA key = RSA.Create();
		byte[] msg = new byte[16];
		var formatter = new RSAPKCS1KeyExchangeFormatter(key);
		byte[] cipherText = formatter.CreateKeyExchange(msg); // -> Vulnerable
	}

	public static void EncryptWithGoodPadding1()
	{
		RSA key = RSA.Create();
		byte[] msg = new byte[16];
		Type t = typeof(byte[]);
		AsymmetricKeyExchangeFormatter formatter = new RSAOAEPKeyExchangeFormatter(key);
		byte[] cipherText = formatter.CreateKeyExchange(msg, t); // -> Safe
	}

	public static void EncryptWithGoodPadding2()
	{
		RSA key = RSA.Create();
		byte[] msg = new byte[16];
		AsymmetricKeyExchangeFormatter formatter = new RSAOAEPKeyExchangeFormatter(key);
		byte[] cipherText = formatter.CreateKeyExchange(msg); // -> Safe
	}

	public static void DecryptWithGoodPadding()
	{
		RSA key = RSA.Create();
		byte[] ciphertext = new byte[16];
		var deformatter = new RSAOAEPKeyExchangeDeformatter(key);
		var plaintext = deformatter.DecryptKeyExchange(ciphertext); // -> Safe
	}

  public static void DecryptWithGoodPadding4()
	{
		RSA key = RSA.Create();
		byte[] ciphertext = new byte[16];
		RSAPKCS1KeyExchangeFormatter deformatter = new RSAOAEPKeyExchangeDeformatter(key);
		var plaintext = deformatter.DecryptKeyExchange(ciphertext); // -> Safe
	}

	public static void Main(string[] args){
	}
}
