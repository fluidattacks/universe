import jwt
import os

payload = {"user_id": 123}
secret_key = "my_hardcoded_key"
safe_key = os.environ.get("MY_APP_SECRET_KEY")

# Must fail secret key comes from hardcoded str
jwt_token = jwt.encode(payload, secret_key, algorithm="HS256")

# Must fail key is a literal str
jwt_token_ii = jwt.encode(payload, "HC_KEY")


# Safe usage: NO ONE OF FOLLOWING SHOULD BE MARKED
safe_jwt_token = jwt.encode(
    payload, safe_key, algorithm="HS512"  # type: ignore
)
safe_jwt_token_i = jwt.encode(payload, safe_key)  # type: ignore
safe_jwt_token_ii = jwt.encode(
    payload, os.environ.get("MY_APP_SECRET_KEY")  # type: ignore
)
