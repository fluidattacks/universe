import static org.junit.Assert.assertTrue;
import java.util.Date;
import org.junit.Test;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class JwtTest {

	@SuppressWarnings("deprecation")
	private String mustFail() {
		String token = Jwts.builder().setSubject("adam")
			.setExpiration(new Date(2018, 1, 1))
			.setIssuer("info@wstutorial.com")
			.claim("groups", new String[] { "user", "admin" })
			.signWith("MTIzNDU2Nzg=", SignatureAlgorithm.HS256).compact();
		return token;
	}

	@SuppressWarnings("deprecation")
	private String mustFailVar() {
		String secretKey = "YourSecretKeyHere";

		String token = Jwts.builder().setSubject("adam")
			.setExpiration(new Date(2018, 1, 1))
			.setIssuer("info@wstutorial.com")
			.claim("groups", new String[] { "user", "admin" })
			.signWith(secretKey, SignatureAlgorithm.HS256).compact();
		return token;
	}

	@SuppressWarnings("deprecation")
	private String mustFailStringMethod() {

		String token = Jwts.builder().setSubject("adam")
			.setExpiration(new Date(2018, 1, 1))
			.setIssuer("info@wstutorial.com")
			.claim("groups", new String[] { "user", "admin" })
			.signWith("secretKey".getBytes(), SignatureAlgorithm.HS256).compact();
		return token;
	}
	@SuppressWarnings("deprecation")
	public static String createJWTSafe(String id, String issuer, String subject, long ttlMillis) {

    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

    long nowMillis = System.currentTimeMillis();
    Date now = new Date(nowMillis);

    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

    JwtBuilder builder = Jwts.builder().setId(id)
			.setIssuedAt(now)
			.setSubject(subject)
			.setIssuer(issuer)
			.signWith(signingKey, signatureAlgorithm);

    return builder.compact();
	}
	@SuppressWarnings("deprecation")
	private String mustPassStringParam() {

		String secretKey = "YourSecretKeyHere";

		String token = Jwts.builder().setSubject("adam")
			.setExpiration(new Date(2018, 1, 1))
			.setIssuer("info@wstutorial.com")
			.claim("groups", new String[] { "user", "admin" })
			.signWith(anyMethod.getSecret(secretKey), SignatureAlgorithm.HS256).compact();

		return token;

	}
}
