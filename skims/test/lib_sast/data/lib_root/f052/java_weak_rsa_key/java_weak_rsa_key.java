import java.security.KeyPairGenerator;

public class WeakRSA {

  static void rsaWeak() {
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(512); // -> Vulnerable
  }

  static void rsaOK() {
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(2048); // -> Safe
  }

  static void rsaWeak1024() {
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(1024); // -> Vulnerable
  }

  static void rsaStrong3072() {
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(3072); // -> Safe
  }

  static void rsaWeak768() {
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(768); // -> Vulnerable
  }

  static void rsaStrong4096() {
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(4096); // -> Safe
  }

  static void rsaUserDefinedWeak() {
    int keySize = 600;
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(keySize); // -> Vulnerable
  }

  static void rsaUserDefinedStrong() {
    int keySize = 2048;
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(keySize); // -> Safe
  }
}
