<?php

$data = "Sensitive Information!";
$key = "supersecretkey";
$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method));

// Noncompliant
$method_1 = "aes-256-cbc";

$encrypted_1 = openssl_encrypt($data, $method_1, $key, OPENSSL_RAW_DATA, $iv);
$encrypted_2 = openssl_encrypt($data, "bf-cfb", $key, OPENSSL_RAW_DATA, $iv);
$decrypted_1 = openssl_decrypt($encrypted_1, $method_1, $key, OPENSSL_RAW_DATA, $iv);

// Compliant
$method = "camellia-256-ofb";
$secure_1 = openssl_encrypt($data, "aria-256-ofb", $key, OPENSSL_RAW_DATA, $iv);
$secure_2 = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
$decrypted_secure_1 = openssl_decrypt($secure_1, "camellia-256-ofb", $key, OPENSSL_RAW_DATA, $iv);

?>
