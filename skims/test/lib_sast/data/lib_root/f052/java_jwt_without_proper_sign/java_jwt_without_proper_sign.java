import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtTest {

	public void mustFail() {

		// Chained method unsafe implementation
        Jwts.builder()
            .setSubject(USER_LOGIN)
            .compact();
	}

	public String mustFailPlain() {

		// Plain unsafe implementation
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		JwtBuilder builder = Jwts.builder();

		builder.setIssuedAt(now);

        return builder.compact();
	}

	public void mustPass() {

		// Chained method safe implementation

	    Jwts.builder()
        .setSubject(USER_LOGIN)
        .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
        .compact();
	}

	public String mustPassPlain(String privateKey) {

		// Plain safe implementation
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		String signingKey = getPrivateKeyFromString(privateKey);

		JwtBuilder builder = Jwts.builder();

		builder.setIssuedAt(now);
		builder.signWith(signatureAlgorithm, signingKey);

        return builder.compact();
	}
}
