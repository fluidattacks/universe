import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class BrokenKeyDerivationActivity extends Activity {

    private static SecretKey deriveKeyInsecurely(String password, int keySizeInBytes) {
        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
				String key = "ThisIs128bitSize";
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        return skeySpec;
    }

    private SecretKey deriveKeySecurely(String password, int keySizeInBytes) {

        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), retrieveSalt());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
        return new SecretKeySpec(keyBytes, "AES");
    }
}
