import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.ImageHtmlEmail;

public class InsecureSMTPTest {

    public static void main(String[] args) throws Exception {
        // VULNERABLE: SimpleEmail without SSL configuration
        Email simpleEmailVulnerable = new SimpleEmail();
        simpleEmailVulnerable.setHostName("smtp.mail.example.com");
        simpleEmailVulnerable.setSmtpPort(465);
        simpleEmailVulnerable.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        simpleEmailVulnerable.setFrom("x@example.com");
        simpleEmailVulnerable.setSubject("TestMail");
        simpleEmailVulnerable.setMsg("This is a test mail ... :-)");
        simpleEmailVulnerable.addTo("y@example.com");
        simpleEmailVulnerable.send(); // -> Vulnerable
        // Missing setSSLOnConnect and setSSLCheckServerIdentity

        // VULNERABLE: MultiPartEmail without SSL configuration
        Email multiPartEmailVulnerable = new MultiPartEmail();
        multiPartEmailVulnerable.setHostName("smtp.mail.example.com");
        multiPartEmailVulnerable.setSmtpPort(465);
        multiPartEmailVulnerable.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        multiPartEmailVulnerable.setFrom("x@example.com");
        multiPartEmailVulnerable.setSubject("TestMail");
        multiPartEmailVulnerable.setMsg("This is a test mail with attachment.");
        multiPartEmailVulnerable.addTo("y@example.com");
        multiPartEmailVulnerable.send(); // -> Vulnerable
        // Missing setSSLOnConnect and setSSLCheckServerIdentity

        // VULNERABLE: HtmlEmail without SSL configuration
        Email htmlEmailVulnerable = new HtmlEmail();
        htmlEmailVulnerable.setHostName("smtp.mail.example.com");
        htmlEmailVulnerable.setSmtpPort(465);
        htmlEmailVulnerable.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        htmlEmailVulnerable.setFrom("x@example.com");
        htmlEmailVulnerable.setSubject("HTML TestMail");
        htmlEmailVulnerable.setMsg("<html><body><h1>This is a test mail</h1></body></html>");
        htmlEmailVulnerable.addTo("y@example.com");
        htmlEmailVulnerable.send(); // -> Vulnerable
        // Missing setSSLOnConnect and setSSLCheckServerIdentity

        // VULNERABLE: ImageHtmlEmail without SSL configuration
        Email imageHtmlEmailVulnerable = new ImageHtmlEmail();
        imageHtmlEmailVulnerable.setHostName("smtp.mail.example.com");
        imageHtmlEmailVulnerable.setSmtpPort(465);
        imageHtmlEmailVulnerable.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        imageHtmlEmailVulnerable.setFrom("x@example.com");
        imageHtmlEmailVulnerable.setSubject("TestMail");
        imageHtmlEmailVulnerable.setMsg("<html><body><h1>This is a test mail</h1></body></html>");
        imageHtmlEmailVulnerable.addTo("y@example.com");
        imageHtmlEmailVulnerable.send(); // -> Vulnerable

        // SAFE: SimpleEmail with SSL configuration
        Email simpleEmailSafe = new SimpleEmail();
        simpleEmailSafe.setHostName("smtp.mail.example.com");
        simpleEmailSafe.setSmtpPort(465);
        simpleEmailSafe.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        simpleEmailSafe.setSSLOnConnect(true);
        simpleEmailSafe.setSSLCheckServerIdentity(true);
        simpleEmailSafe.setFrom("x@example.com");
        simpleEmailSafe.setSubject("Secure TestMail");
        simpleEmailSafe.setMsg("This is a secure test mail ... :-)");
        simpleEmailSafe.addTo("y@example.com");
        simpleEmailSafe.send(); // -> Safe

        // SAFE: ImageHtmlEmail with SSL configuration
        Email imageHtmlEmailSafe = new ImageHtmlEmail();
        imageHtmlEmailSafe.setHostName("smtp.mail.example.com");
        imageHtmlEmailSafe.setSmtpPort(465);
        imageHtmlEmailSafe.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        imageHtmlEmailSafe.setSSLOnConnect(true);
        imageHtmlEmailSafe.setSSLCheckServerIdentity(true);
        imageHtmlEmailSafe.setFrom("x@example.com");
        imageHtmlEmailSafe.setSubject("Secure HTML Image TestMail");
        imageHtmlEmailSafe.setMsg("<html><body><h1>This is a secure HTML email with an image.</h1></body></html>");
        imageHtmlEmailSafe.addTo("y@example.com");
        imageHtmlEmailSafe.send(); // -> Safe

        // SAFE: MultiPartEmail with complete SSL configuration
        Email multiPartEmailSafe = new MultiPartEmail();
        multiPartEmailSafe.setHostName("smtp.mail.example.com");
        multiPartEmailSafe.setSmtpPort(465);
        multiPartEmailSafe.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        multiPartEmailSafe.setSSLOnConnect(true);
        multiPartEmailSafe.setSSLCheckServerIdentity(true);
        multiPartEmailSafe.setFrom("x@example.com");
        multiPartEmailSafe.setSubject("Secure TestMail");
        multiPartEmailSafe.setMsg("This is a secure test mail ... :-)");
        multiPartEmailSafe.addTo("y@example.com");
        multiPartEmailSafe.send(); // -> Safe

        // SAFE: HtmlEmail with complete SSL configuration
        Email htmlEmailSafe = new HtmlEmail();
        htmlEmailSafe.setHostName("smtp.mail.example.com");
        htmlEmailSafe.setSmtpPort(465);
        htmlEmailSafe.setAuthenticator(new DefaultAuthenticator("user@example.com", "password"));
        htmlEmailSafe.setStartTLSRequired(true);
        htmlEmailSafe.setSSLCheckServerIdentity(true);
        htmlEmailSafe.setFrom("x@example.com");
        htmlEmailSafe.setSubject("Secure HTML TestMail");
        htmlEmailSafe.setMsg("<html><body><h1>This is a secure HTML test mail</h1></body></html>");
        htmlEmailSafe.addTo("y@example.com");
        htmlEmailSafe.send(); // -> Safe
    }
}
