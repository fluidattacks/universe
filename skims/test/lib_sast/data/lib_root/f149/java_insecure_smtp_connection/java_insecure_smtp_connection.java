import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.*;

// Source: https://semgrep.dev/r?q=java.lang.security.audit.insecure-smtp-connection.insecure-smtp-connection
public class Cls {

    // cf. https://find-sec-bugs.github.io/bugs.htm#INSECURE_SMTP_SSL
    public void sendEmail(String username, String password) {
        Email email = new SimpleEmail();
        email.setHostName("smtp.servermail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(username, password));
        email.setSSLOnConnect(true);
        email.setFrom("user@gmail.com");
        email.setSubject("TestMail");
        email.setMsg("This is a test mail ... :-)");
        email.addTo("foo@bar.com");
        email.send(); // -> Vulnerable
    }

    public void sendEmailSafe(String username, String password) {
        Email email = new SimpleEmail();
        email.setHostName("smtp.servermail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(username, password));
        email.setSSLOnConnect(true);
        email.setSSLCheckServerIdentity(true);
        email.setFrom("user@gmail.com");
        email.setSubject("TestMail");
        email.setMsg("This is a test mail ... :-)");
        email.addTo("foo@bar.com");
        email.send(); // -> Safe
    }
}
