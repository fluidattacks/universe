using System;
using System.Security.Cryptography;

public class VulnerableClass
{

    private Aes CreateAesInstance(byte[] key)
    {
        var staticIV = new byte[16] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        Aes aes = Aes.Create();
        aes.Key = key;
        aes.IV = staticIV;
        return aes;
    }
}

public class SecureClass
{
    private Aes CreateAesInstance2(byte[] key, byte[] iv)
    {
        Aes aes = Aes.Create();
        aes.Key = key;
        aes.IV = iv;
        return aes;
    }

    private Aes CreateAesInstance2(byte[] key)
    {
        var staticIV = new byte[16];

        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(staticIV);
        }

        Aes aes = Aes.Create();
        aes.Key = key;
        aes.IV = staticIV;
        return aes;
    }
}
