import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.RuntimePermission;
import java.lang.reflect.ReflectPermission;

public class PermissionTest {

    public static void main(String[] args) {
        PermissionCollection permissions1 = new Permissions();
        RuntimePermission createClassLoaderPermission = new RuntimePermission("createClassLoader"); // -> Vulnerable
        permissions1.add(createClassLoaderPermission);

        PermissionCollection permissions2 = new Permissions();
        ReflectPermission suppressAccessChecksPermission = new ReflectPermission("suppressAccessChecks"); // -> Vulnerable
        permissions2.add(suppressAccessChecksPermission);

        PermissionCollection permissions3 = new Permissions();
        permissions3.add(new ReflectPermission("suppressAccessChecks")); // -> Vulnerable

        PermissionCollection permissions4 = new Permissions();
        permissions4.add(new RuntimePermission("createClassLoader")); // -> Vulnerable

        PermissionCollection permissions11 = new Permissions();
        String createClassLoader = "createClassLoader";
        RuntimePermission createClassLoaderPermission = new RuntimePermission(createClassLoader); // -> Vulnerable
        permissions1.add(createClassLoaderPermission);

        PermissionCollection permissions12 = new Permissions();
        String suppressAccessChecks = "suppressAccessChecks";
        ReflectPermission suppressAccessChecksPermission = new ReflectPermission(suppressAccessChecks); // -> Vulnerable
        permissions2.add(suppressAccessChecksPermission);

        PermissionCollection safePermissions1 = new Permissions();
        safePermissions1.add(new RuntimePermission("accessDeclaredMembers")); // -> Safe

        PermissionCollection safePermissions2 = new Permissions();
        safePermissions2.add(new RuntimePermission("readFileDescriptor")); // -> Safe
        safePermissions2.add(new RuntimePermission("writeFileDescriptor")); // -> Safe

        PermissionCollection safePermissions1 = new Permissions();
        String accessDeclaredMembers = "accessDeclaredMembers";
        safePermissions1.add(new RuntimePermission(accessDeclaredMembers)); // -> Safe

        PermissionCollection safePermissions2 = new Permissions();
        String readFileDescriptor = "readFileDescriptor";
        String writeFileDescriptor = "writeFileDescriptor";
        safePermissions2.add(new RuntimePermission(readFileDescriptor)); // -> Safe
        safePermissions2.add(new RuntimePermission(writeFileDescriptor)); // -> Safe


        System.out.println("Permission tests executed.");
    }
}

// Source: https://github.com/zxiaofan/JDK/blob/19a6c71e52f3ecd74e4a66be5d0d552ce7175531/JDK1.6-Java%20SE%20Development%20Kit%206u45/src/java/lang/reflect/AccessibleObject.java#L71
public class AccessibleObject implements AnnotatedElement {

    static final private java.security.Permission ACCESS_PERMISSION = new ReflectPermission( // -> Vulnerable
      "suppressAccessChecks"
    );

    public static void setAccessible(AccessibleObject[] array, boolean flag) throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) sm.checkPermission(ACCESS_PERMISSION);
        for (int i = 0; i < array.length; i++) {
            setAccessible0(array[i], flag);
        }

    }
}

// Source: https://github.com/rapidminer/rapidminer-studio/blob/7124551801923decfe2c0e077e329a1686087c12/src/main/java/com/rapidminer/security/PluginSandboxPolicy.java#L266
private static PermissionCollection createUnsignedPermissions(final PluginClassLoader loader) {
		final Permissions permissions = new Permissions();

		if (ProductConstraintManager.INSTANCE.isInitialized()) {

			boolean developmentIsEnabled = Boolean.parseBoolean(
					ParameterService.getParameterValue(RapidMiner.PROPERTY_RAPIDMINER_UPDATE_DEVELOPMENT_PERMISSIONS));
			boolean isDevelopmentAllowed = ProductConstraintManager.INSTANCE.getActiveLicense()
					.getPrecedence() >= StudioLicenseConstants.DEVELOPER_LICENSE_PRECEDENCE;
			if (developmentIsEnabled && isDevelopmentAllowed) {
				// aid extension development by granting all permissions
				return createAllPermissions();
			}


			boolean isAllowed = ProductConstraintManager.INSTANCE.getActiveLicense()
					.getPrecedence() >= StudioLicenseConstants.UNLIMITED_LICENSE_PRECEDENCE
					|| ProductConstraintManager.INSTANCE.isTrialLicense();
			boolean isEnabled = Boolean.parseBoolean(
					ParameterService.getParameterValue(RapidMiner.PROPERTY_RAPIDMINER_UPDATE_ADDITIONAL_PERMISSIONS));
			if (isAllowed && isEnabled) {
				permissions.add(new ReflectPermission("suppressAccessChecks")); // -> Vulnerable
				permissions.add(new ReflectPermission("newProxyInPackage.*"));
				permissions.add(new AWTPermission("accessClipboard"));
				permissions.add(new RuntimePermission("createClassLoader")); // -> Vulnerable
				permissions.add(new RuntimePermission("getClassLoader"));
				permissions.add(new RuntimePermission("setContextClassLoader"));
				permissions.add(new RuntimePermission("enableContextClassLoaderOverride"));
				permissions.add(new RuntimePermission("closeClassLoader"));
				permissions.add(new RuntimePermission("modifyThread"));
				permissions.add(new RuntimePermission("stopThread"));
				permissions.add(new RuntimePermission("modifyThreadGroup"));
				permissions.add(new RuntimePermission("loadLibrary.*"));
				permissions.add(new RuntimePermission("getStackTrace"));
				permissions.add(new RuntimePermission("setDefaultUncaughtExceptionHandler"));
				permissions.add(new RuntimePermission("preferences"));
				permissions.add(new RuntimePermission("setFactory"));
				permissions.add(new PropertyPermission("*", "write"));
				permissions.add(new NetPermission("*"));
			}
		}

    // ...
}

// Source: https://github.com/rapidminer/rapidminer-studio/blob/7124551801923decfe2c0e077e329a1686087c12/src/main/java/com/rapidminer/tools/plugin/PluginClassLoader.java#L183
public class PluginClassLoader extends URLClassLoader {
  // ...
	public void setPluginKey(String pluginKey) {
		if (System.getSecurityManager() != null) {
			AccessController.checkPermission(new RuntimePermission("createClassLoader")); // -> Vulnerable
		}
		this.pluginKey = pluginKey;
	}

  // ...
}

// Source: https://github.com/gngrOrg/gngr/blob/a0df8a7a8ced1d4ceae98edf933a4c52baf36e51/src/Platform_Core/org/lobobrowser/security/LocalSecurityPolicy.java#L59
public class LocalSecurityPolicy extends Policy {
  // ...

  private static void initCorePermissions() {
    CORE_PERMISSIONS.add(new SocketPermission("*", "connect,resolve,listen,accept"));
    CORE_PERMISSIONS.add(new RuntimePermission("createClassLoader")); // -> Vulnerable
    CORE_PERMISSIONS.add(new RuntimePermission("getClassLoader"));
    CORE_PERMISSIONS.add(new RuntimePermission("exitVM"));
    CORE_PERMISSIONS.add(new RuntimePermission("setIO"));
    CORE_PERMISSIONS.add(new RuntimePermission("setContextClassLoader"));
    CORE_PERMISSIONS.add(new RuntimePermission("enableContextClassLoaderOverride"));
    CORE_PERMISSIONS.add(new RuntimePermission("setFactory"));
    CORE_PERMISSIONS.add(new RuntimePermission("accessClassInPackage.*"));
    CORE_PERMISSIONS.add(new RuntimePermission("defineClassInPackage.*"));
    CORE_PERMISSIONS.add(new RuntimePermission("accessDeclaredMembers"));
    CORE_PERMISSIONS.add(new RuntimePermission("getStackTrace"));
    CORE_PERMISSIONS.add(new RuntimePermission("preferences"));
    CORE_PERMISSIONS.add(new RuntimePermission("modifyThreadGroup"));
    CORE_PERMISSIONS.add(new RuntimePermission("getProtectionDomain"));
    CORE_PERMISSIONS.add(new RuntimePermission("shutdownHooks"));
    CORE_PERMISSIONS.add(new RuntimePermission("modifyThread"));
    CORE_PERMISSIONS.add(new RuntimePermission("com.sun.media.jmc.accessMedia"));
    // loadLibrary necessary in Java 6, in particular loadLibrary.sunmscapi.
    CORE_PERMISSIONS.add(new RuntimePermission("loadLibrary.*"));
    CORE_PERMISSIONS.add(new NetPermission("setDefaultAuthenticator"));
    CORE_PERMISSIONS.add(new NetPermission("setCookieHandler"));
    CORE_PERMISSIONS.add(new NetPermission("specifyStreamHandler"));
    CORE_PERMISSIONS.add(new SSLPermission("setHostnameVerifier"));
    CORE_PERMISSIONS.add(new SSLPermission("getSSLSessionContext"));
    CORE_PERMISSIONS.add(new SecurityPermission("putProviderProperty.*"));
    CORE_PERMISSIONS.add(new SecurityPermission("insertProvider.*"));
    CORE_PERMISSIONS.add(new SecurityPermission("removeProvider.*"));
    CORE_PERMISSIONS.add(new java.util.logging.LoggingPermission("control", null));
    CORE_PERMISSIONS.add(GenericLocalPermission.EXT_GENERIC);
  }

  // ...
}
