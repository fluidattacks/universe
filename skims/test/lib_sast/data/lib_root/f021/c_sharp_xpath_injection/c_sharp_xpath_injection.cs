using System;
using System.Xml.XPath;

public partial class Test
{
    protected void Unsafe_Path()
    {
        TcpClient tcpConn = new TcpClient("host.example.org", 39544);
        StreamReader sr = new StreamReader(tcpConn.GetStream());
        data = sr.ReadLine();
        xmlFile = "SomeFile.xml";
        string[] tokens = data.Split("||".ToCharArray());
        string username = tokens[0];
        XPathDocument inputXml = new XPathDocument(xmlFile);
        XPathNavigator xPath = inputXml.CreateNavigator();
        string query = "//users/user[name/text()='" + username;
        string secret = (string)xPath.Evaluate(query);
    }
}
