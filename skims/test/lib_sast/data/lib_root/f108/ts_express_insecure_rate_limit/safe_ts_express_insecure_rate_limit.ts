const RateLimit = require('express-rate-limit')
const express = require('express')

const app = express()

app.use('/rest/user/reset-password', new RateLimit({
  windowMs: 5 * 60 * 1000,
  max: 100,
  keyGenerator (req) {
    const clientIp = req.ip
    const forwardedFor = req.headers['x-forwarded-for']

    if (trustedProxies.includes(clientIp) && forwardedFor) {
      const ipAddresses = forwardedFor.split(',').map(ip => ip.trim())
      return ipAddresses[0] // Use the first IP in the X-Forwarded-For chain
    }

    return clientIp
  }
}))
