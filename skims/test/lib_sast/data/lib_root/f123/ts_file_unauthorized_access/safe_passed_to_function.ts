import path = require('path');
import { type Request, type Response, type NextFunction } from 'express';

const allowedExtensions = ['.txt', '.json'];

module.exports = function serveKeyFiles() {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file;
    const fileExtension = path.extname(file);

    if (allowedExtensions.includes(fileExtension) && !file.includes('/')) {
      res.sendFile(path.resolve('encryptionkeys/', file));
    } else {
      res.status(403).send('Forbidden: Invalid file or path.');
    }
  }
};
