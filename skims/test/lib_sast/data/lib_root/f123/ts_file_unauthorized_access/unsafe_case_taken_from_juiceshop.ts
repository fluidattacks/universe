import path = require('path')
import { type Request, type Response, type NextFunction } from 'express'

module.exports = function serveKeyFiles () {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file

    if (!file.includes('/')) {
      res.sendFile(path.resolve('encryptionkeys/', file)) // -> Safe (to avoid FP)
    } else {
      res.status(403)
      next(new Error('File names cannot contain forward slashes!'))
    }
  }
}

module.exports = function serveKeyFiles() {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file;

    res.sendFile(path.resolve('encryptionkeys/', file)); // -> Vulnerable
  };
};

module.exports = function serveKeyFiles() {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file;

    const validFilePattern = /^[a-zA-Z0-9_-]+\.(txt|key)$/;
    const not_the_file = "key123.txt";

    if (validFilePattern.test(not_the_file)) {
      res.sendFile(path.resolve('encryptionkeys/', file)); // -> Vulnerable
    } else {
      res.status(403);
      next(new Error('Invalid file name!'));
    }
  };
};

module.exports = function serveKeyFiles() {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file;

    if (!file.includes('/') && !file.includes('..')) {
      res.sendFile(path.resolve('encryptionkeys/', file)); // -> Safe
    } else {
      res.status(403);
      next(new Error('Invalid file path!'));
    }
  };
};

module.exports = function serveKeyFiles() {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file;

    const safeFile = path.basename(file);
    res.sendFile(path.resolve('encryptionkeys/', safeFile)); // -> Safe
  };
};

module.exports = function serveKeyFiles() {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const safeFile = path.basename(params.file);

    res.sendFile(path.resolve('encryptionkeys/', safeFile)); // -> Safe
  };
};

module.exports = function serveKeyFiles() {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file;

    const validFilePattern = /^[a-zA-Z0-9_-]+\.(txt|key)$/;

    if (validFilePattern.test(file)) {
      res.sendFile(path.resolve('encryptionkeys/', file)); // -> Safe
    } else {
      res.status(403);
      next(new Error('Invalid file name!'));
    }
  };
};
