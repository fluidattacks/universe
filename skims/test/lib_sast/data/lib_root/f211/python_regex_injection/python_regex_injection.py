# type: ignore

from flask import (
    request,
)
import re


def unsafe_regex() -> None:
    # Noncompliant: pattern in vulnerable to DoS
    pattern = "^(([a-z])+.)+[A-Z]([a-z])+$"
    filename = request.GET.get["attachment"]
    re.search(pattern, filename)

    usermail = request.args["useremail"]
    re.match(pattern, usermail)

    user_id = request.args["userId"]
    re.findall(pattern, user_id)


def safe_regex() -> None:
    # Regex pattern does not use backtracking
    safe_pat = "^([a-z])+$"
    filename = request.files["attachment"]
    re.search(safe_pat, filename)

    mail = request.args["useremail"]
    re.match(safe_pat, mail)

    re.findall("[A-Z]*", filename)
