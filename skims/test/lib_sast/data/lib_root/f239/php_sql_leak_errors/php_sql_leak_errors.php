<?php
class Users
{
  function vulnerable_example($userid)
  {
    $mysqli = new mysqli("localhost", "username", "password", "database_name");
    $query = sprintf("SELECT * from users where id = '%d'",
                    mysql_real_escape_string($userid));
    $res = mysql_query($query);
    if ($res)
    {
      return mysql_fetch_assoc($res);
    }
    else
    {
      $error = mysqli_error();
      die($error);
    }
  }

  function vulnerable_example2($userid)
  {
    $mysqli = new mysql("localhost", "username", "password", "database_name");
    $query = sprintf("SELECT * from users where id = '%d'",
                    mysql_real_escape_string($userid));
    $res = mysql_query($query);
    exit(mysql_error());
  }


  function not_deterministic($userid)
  {
    $myobject = new myownobject("something_safe");
    die("error" . $myobject->error);
  }
}
?>
