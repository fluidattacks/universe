<?php

$dang_key = "display_errors";
$dang_val = 1;

$safe_key = "other_setting";
$dang_val = 0;

// Dang cases:

ini_set("display_errors", 1);
ini_set("display_errors", "1");
ini_set("display_errors", "ON");
ini_set($dang_key, "ON");
ini_set("display_errors", $dang_val);
ini_set($dang_key, $dang_val);


// Safe cases

ini_set("display_errors", 0);
ini_set("display_errors", "0");
ini_set("display_errors", "OFF");
ini_set($safe_key, "ON");
ini_set("display_errors", $safe_val);
ini_set($safe_key, $safe_val);

switch ($input) {
  case 'all':
    ini_set("display_errors", 1);
    ini_set("display_errors", "1");
    ini_set("display_errors", "ON");
    ini_set($dang_key, "ON");
    ini_set("display_errors", $dang_val);
    ini_set($dang_key, $dang_val);
    break;
  default:
    ini_set("display_errors", 1);
    ini_set("display_errors", "1");
    ini_set("display_errors", "ON");
    ini_set($dang_key, "ON");
    ini_set("display_errors", $dang_val);
    ini_set($dang_key, $dang_val);
    break;
}



if ($token != "") {
  ini_set("display_errors", 1);
  ini_set("display_errors", "1");
  ini_set("display_errors", "ON");
  ini_set($dang_key, "ON");
  ini_set("display_errors", $dang_val);
  ini_set($dang_key, $dang_val);
} elseif ($token != "token") {
  ini_set("display_errors", 1);
  ini_set("display_errors", "1");
  ini_set("display_errors", "ON");
  ini_set($dang_key, "ON");
  ini_set("display_errors", $dang_val);
  ini_set($dang_key, $dang_val);

} else {
  ini_set("display_errors", 1);
  ini_set("display_errors", "1");
  ini_set("display_errors", "ON");
  ini_set($dang_key, "ON");
  ini_set("display_errors", $dang_val);
  ini_set($dang_key, $dang_val);
}
