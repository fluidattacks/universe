package jwt_test.jwt_test_1;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

public class App
{

    static String secret = "secret";

    private static void bad1() {
        try {
            Algorithm algorithm = Algorithm.HMAC256("secret");
            String token = JWT.create()
                .withIssuer("auth0")
                .sign(algorithm);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    private static void ok1(String secretKey) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            String token = JWT.create()
                .withIssuer("auth0")
                .sign(algorithm);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    public static void main( String[] args )
    {
        bad1();
        ok1(args[0]);
    }
}

abstract class App2
{
    static String secret = "secret";

    public void bad2() {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret.getBytes());
            String token = JWT.create()
                .withIssuer("auth0")
                .sign(algorithm);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    public void bad3() {
        try {
            Algorithm algorithm = Algorithm.HMAC384("secret");
            String token = JWT.create()
                .withIssuer("auth0")
                .sign(algorithm);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    public void bad4() {
        try {
            String token = JWT.create()
                .withIssuer("auth0")
                .sign(Algorithm.HMAC256(secret));
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    public void bad5() {
        try {
            Algorithm algorithm = Algorithm.none();
            String token = JWT.create() // VULN
                .withIssuer("auth0")
                .sign(algorithm);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    public void ok2(String secretKey) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(System.getenv("SECRET_KEY"));
            String token = JWT.create()
                .withIssuer("auth0")
                .sign(algorithm);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    public void ok3(String secretKey) {
        try {
            String token = JWT.create()
                .withIssuer("auth0")
                .sign(Algorithm.HMAC256(System.getenv("SECRET_KEY")));
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }
}


public static String vulnerableCreate(String id, String audience, String subject, Map<String, Object> claims, long timeExp) {
  String apiKeySecret = "the_super_secret_key";
  Algorithm signatureAlgorithm = Algorithm.HMAC256(apiKeySecret);
  long nowMillis = System.currentTimeMillis();
  Date now = new Date(nowMillis);
  JWTCreator.Builder builder = JWT.create().withJWTId(id).withIssuer("TEST").withAudience(new String[]{audience}).withSubject(subject).withIssuedAt(now);
  if (timeExp > 0L) {
    Date exp = new Date(nowMillis + timeExp);
    builder.withExpiresAt(exp);
  }
  return builder.sign(signatureAlgorithm); // -> Vulnerable
}


public static String safeCreate(String id, String audience, String subject, Map<String, Object> claims, long timeExp, String key) {
  Algorithm signatureAlgorithm = Algorithm.HMAC256(key);
  long nowMillis = System.currentTimeMillis();
  Date now = new Date(nowMillis);
  JWTCreator.Builder builder = JWT.create().withJWTId(id).withIssuer("TEST").withAudience(new String[]{audience}).withSubject(subject).withIssuedAt(now);
  if (timeExp > 0L) {
    Date exp = new Date(nowMillis + timeExp);
    builder.withExpiresAt(exp);
  }
  return builder.sign(signatureAlgorithm); // -> Safe
}
