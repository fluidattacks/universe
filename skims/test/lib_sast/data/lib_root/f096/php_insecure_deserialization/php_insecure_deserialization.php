<?php

class User
{
    public $username;
    public $isAdmin;
}

function vulnerable_deserialization_example()
{
    $user_data = $_GET['data'];
    $unserializedUserData = unserialize($user_data);
    $unserializedPostData = unserialize($_POST['data']);

}

function secure_deserialization_example()
{
    $user_data = $_GET['data'];
    $sanitized_data = sanitize_text_field($user_data);
    $decoded_data = unserialize($sanitized_data);
    $id     = absint( $_REQUEST["id"] );
    $result = $wpdb->get_row( "SELECT * FROM $table WHERE id = $id" );
    if ( $result ) {
      $settings['param'] = unserialize( $result->param );
    }
}

?>
