# type: ignore
# settings.py

ALLOWED_HOSTS = ["www.example.com"]
DEBUG = False  # -> Safe
DEFAULT_FROM_EMAIL = "webmaster@example.com"
DEBUG = True  # -> Vulnerable
