const express = require('express');
const errorhandler_module = require('errorhandler');
const custom_errorhandler = require('./errorhandler');
const errorhandler = require('./another_custom_errorhandler');

let app = express();
let app_no_express = no_express();

app.use(errorhandler_module()); // Vulnerable

if (process.env.NODE_ENV === 'development') {
  app.use(errorhandler_module()); // Safe
}

app.use(custom_errorhandler()); // Safe
app.use(errorhandler()); // Safe
app_no_express.use(errorhandler_module()); // Safe
