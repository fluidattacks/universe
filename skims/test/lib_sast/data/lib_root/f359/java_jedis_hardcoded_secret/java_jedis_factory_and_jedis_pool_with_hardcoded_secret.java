import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisFactory;

// Source: https://semgrep.dev/r?q=java.jedis.secrets.jedis-jedisfactory-hardcoded-password.jedis-jedisfactory-hardcoded-password
@Service
public class JedisService implements IJedisService {
    @Test
    public void hardcoded() {
        JedisFactory jedisFactory = new JedisFactory();
      jedisFactory.setHostName(hostName);
      jedisFactory.setPort(port);
      jedisFactory.setPassword("asdf"); // -> Vulnerable
      jedisFactory.setDatabase(database);
    }

    public void notHardcoded(String password) {
        JedisFactory jedisFactory = new JedisFactory();
        jedisFactory.setHostName(hostName);
        jedisFactory.setPort(port);
        jedisFactory.setPassword(password); // -> Safe
        jedisFactory.setDatabase(database);
    }
}


// Source: https://semgrep.dev/r?q=java.jedis.secrets.jedispool-constructor-hardcoded-secret.jedispool-constructor-hardcoded-secret
public class RedisTools {
    private static final Logger log = LoggerFactory.getLogger(RedisTools.class);
    private static ReentrantLock lockPool = new ReentrantLock();
    private static RedisTools instance;
    private static JedisPool jedisPool;
    private static ReentrantLock lock = new ReentrantLock();

    private static void initJedisPool() throws Exception{
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(300);
        jedisPoolConfig.setMaxIdle(5);
        jedisPoolConfig.setMinIdle(5);
        jedisPoolConfig.setMaxWaitMillis(3 * 1000);
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setTestOnReturn(true);
        jedisPoolConfig.setTestWhileIdle(true);
        jedisPoolConfig.setMinEvictableIdleTimeMillis(500);
        jedisPoolConfig.setSoftMinEvictableIdleTimeMillis(1000);
        jedisPoolConfig.setTimeBetweenEvictionRunsMillis(1000);
        jedisPoolConfig.setNumTestsPerEvictionRun(100);
        Redis redis = Config.cache().getRedis();
        String user = redis.getUser();
        String password = "asdf";
        if(StringUtils.isBlank(redis.getUser())){
            user = null;
        }
        if(StringUtils.isBlank(redis.getPassword())){
            password = null;
        }
        jedisPool = new JedisPool( // -> Vulnerable
          jedisPoolConfig,
          redis.getHost(),
          redis.getPort(),
          redis.getSocketTimeout(),
          user,
          password,
          redis.getIndex(),
          redis.getSslEnable()
        );
    }

    public RedisCacheStorager(String host, int port, int timeout, String password, JedisPoolConfig poolConfig) {
        String password = "asdf";
        new JedisPool(poolConfig, host, port, timeout, password); // -> Vulnerable

        String password2 = instance.getCfgPassword();
        new JedisPool(poolConfig, host, port, timeout, password2); // -> Safe
    }

    private void open() {
        if (config.get().enabled) {
            log.info("Initalizing Commune connection...");

            RedisServer server = config.get().server;

            GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
            poolConfig.setJmxEnabled(false);
            poolConfig.setMinIdle(0);

            String host = server.host;
            int port = server.port;
            int cTimeout = server.cTimeout;
            int sTimeout = server.sTimeout;
            String password = "uh oh!";
            int defaultDatabase = Protocol.DEFAULT_DATABASE;
            JedisPool pool = new JedisPool( // -> Vulnerable
                    poolConfig,
                    host,
                    port,
                    cTimeout,
                    sTimeout,
                    password,
                    defaultDatabase,
                    "plume_commune");

            client = new CommuneClient(pool, receiveQueue, sendQueue, getSource());
        }
    }

	@Bean
	@ConditionalOnMissingBean(JedisPool.class)
	public JedisPool jedisPool() {

		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxIdle(jedisProperties.getPool().getMaxIdle());
		poolConfig.setMaxTotal(jedisProperties.getPool().getMaxTotal());
		poolConfig.setMaxWaitMillis(jedisProperties.getPool().getMaxWaitMillis() * 1000);

        String host = jedisProperties.getHost();
        int port = jedisProperties.getPort();
        int timeout = jedisProperties.getTimeout() * 1000;
        String password = jedisProperties.getPassword();

		JedisPool jedisPool = new JedisPool(poolConfig, host, port, timeout, "asdf", 0); // -> Vulnerable

		JedisPool jedisPool = new JedisPool(poolConfig, host, port, timeout, password, 0); // -> Safe
		return jedisPool;
	}
}


// Source: https://semgrep.dev/r?q=java.jedis.secrets.jedissentinelpool-constructor-hardcoded-secret.jedissentinelpool-constructor-hardcoded-secret
public class RedisTools {
    protected static final Logger LOGGER = LoggerFactory.getLogger(JedisPooledFactory.class);

    private static volatile JedisPoolAbstract jedisPool = null;

    private static final String HOST = "127.0.0.1";

    private static final int PORT = 6379;
    private static final int DATABASE = 0;

    private static final int SENTINEL_HOST_NUMBER = 3;

    private static final Configuration CONFIGURATION = ConfigurationFactory.getInstance();

    public static JedisPoolAbstract getJedisPoolInstance(JedisPoolAbstract... jedisPools) {
        JedisPoolAbstract tempJedisPool = null;
        JedisPoolConfig poolConfig = new JedisPoolConfig();

        String masterName = CONFIGURATION.getConfig(ConfigurationKeys.STORE_REDIS_SENTINEL_MASTERNAME);

        Set<String> sentinels = new HashSet<>(SENTINEL_HOST_NUMBER);
        int db = CONFIGURATION.getInt(ConfigurationKeys.STORE_REDIS_DATABASE, DATABASE);

        String password = CONFIGURATION.getConfig(ConfigurationKeys.STORE_REDIS_PASSWORD);
        tempJedisPool = new JedisSentinelPool(masterName, sentinels, poolConfig, 60000, password, db); // -> Safe

        String password2 = "uh oh!";
        tempJedisPool = new JedisSentinelPool(masterName, sentinels, poolConfig, 60000, password2, db); // -> Vulnerable
    }
}
