// Source: https://semgrep.dev/r?q=java.jedis.secrets.jedis-jedisclientconfig-hardcoded-password.jedis-jedisclientconfig-hardcoded-password
import redis.clients.jedis.JedisClientConfig;
import redis.clients.jedis.DefaultJedisClientConfig;

public class JedisTest {
    void run() {
        new DefaultJedisClientConfig(connectionTimeoutMillis, socketTimeoutMillis, // -> Vulnerable
            blockingSocketTimeoutMillis, user, "asdf", database, clientName, ssl, sslSocketFactory,
            sslParameters, hostnameVerifier, hostAndPortMapper);

        DefaultJedisClientConfig.create(connectionTimeoutMillis, socketTimeoutMillis, // ->  Vulnerable
            blockingSocketTimeoutMillis, user, "asdf", database, clientName, ssl, sslSocketFactory,
            sslParameters, hostnameVerifier, hostAndPortMapper);

        new DefaultJedisClientConfig(connectionTimeoutMillis, socketTimeoutMillis, // -> Safe
            blockingSocketTimeoutMillis, user, "", database, clientName, ssl, sslSocketFactory,
            sslParameters, hostnameVerifier, hostAndPortMapper);

        new DefaultJedisClientConfig(connectionTimeoutMillis, socketTimeoutMillis, // -> Safe
            blockingSocketTimeoutMillis, user, config.getAuthPassword(), database, clientName, ssl, sslSocketFactory,
            sslParameters, hostnameVerifier, hostAndPortMapper);

        new DefaultJedisClientConfig(connectionTimeoutMillis, socketTimeoutMillis, // -> Safe
            blockingSocketTimeoutMillis, user, config.getAuthPassword("john"), database, clientName, ssl, sslSocketFactory,
            sslParameters, hostnameVerifier, hostAndPortMapper);

        new DefaultJedisClientConfig(connectionTimeoutMillis, socketTimeoutMillis, // -> Safe
            blockingSocketTimeoutMillis, user, config.getAuthPassword["john"], database, clientName, ssl, sslSocketFactory,
            sslParameters, hostnameVerifier, hostAndPortMapper);

        JedisClientConfig cc = DefaultJedisClientConfig.builder()
            .password("asdf") // -> Vulnerable
            .ssl(useSsl)
            .build();

        // Connect to the Azure Cache for Redis over the TLS/SSL port using the key.
        Jedis jedis = new Jedis(cacheHostname, 6380, cc);

        JedisClientConfig cc2 = DefaultJedisClientConfig.builder()
            .password("") // -> Safe
            .ssl(useSsl)
            .build();

        cc.updatePassword("hello"); // -> Vulnerable

        cc.updatePassword(""); // -> Safe

        DefaultJedisClientConfig.Builder builder = DefaultJedisClientConfig.builder();

        builder.password("asdf"); // -> Vulnerable

        builder.password(""); // -> Safe
    }
}
