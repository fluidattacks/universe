using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

// Source: https://semgrep.dev/r?q=csharp.lang.security.system.passwordauthenticationmethod-hardcoded-secret.passwordauthenticationmethod-hardcoded-secret
namespace SendMailTest
{
    class Program
    {
        static void Main(string[] args)
        {
                string password = "superP4ssword";
                new PasswordAuthenticationMethod("aa", "reee"); // -> Vulnerable
                new PasswordAuthenticationMethod("aa", password); // -> Vulnerable
                new PasswordAuthenticationMethod("aa", args[1]); // -> Safe

        }
    }
}

// Source: https://semgrep.dev/r?q=csharp.lang.security.system.networkcredential-hardcoded-secret.networkcredential-hardcoded-secret
private A GetConnection(args)
{
    NetworkCredential cre = new NetworkCredential();
    string password = "aaa";

    // Vulnerable cases:
    new NetworkCredential("username", "password"); // -> Vulnerable
    cre.Password = "aaaa"; // -> Vulnerable
    new NetworkCredential("username", password); // -> Vulnerable
    cre.Password = password; // -> Vulnerable
    cre["password"] = "aaaa"; // -> Vulnerable
    cre["password"] = password; // -> Vulnerable

    // Safe cases:
    new NetworkCredential("username", args[1]); // -> Safe
    cre.Password = args[1]; // -> Safe
    cre["password"] = args[1]; // -> Safe

}

// Source: https://semgrep.dev/r?q=csharp.lang.security.system.oracleconnectionstringbuilder-hardcoded-secret.oracleconnectionstringbuilder-hardcoded-secret
private OracleConnectionStringBuilder GetConnection(args)
{
    OracleConnectionStringBuilder builder = new OracleConnectionStringBuilder();
    builder.Password = "reee!"; // -> Vulnerable
    builder["Password"] = "reee!"; // -> Vulnerable

    var cb = new OracleConnectionStringBuilder();
    cb["Password"] = "reee!"; // -> Vulnerable
    cb.Password = "reee!"; // -> Vulnerable

    builder.Password = args[1]; // -> Safe
    builder["Password"] = args[1]; // -> Safe
    cb["Password"] = args[1]; // -> Safe
    cb.Password = args[1]; // -> Safe

}

// Source: https://semgrep.dev/r?q=csharp.lang.security.system.sqlconnectionstringbuilder-hardcoded-secret.sqlconnectionstringbuilder-hardcoded-secret
private SqlConnectionStringBuilder GetConnection(args)
{
    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
    string password = "aaaa";

    builder.Password = "reee!"; // -> Vulnerable
    builder["Password"] = "reee!"; // -> Vulnerable

    var cb = new SqlConnectionStringBuilder();
    cb["Password"] = password; // -> Vulnerable
    cb.Password = "reee!"; // -> Vulnerable

    builder.Password = args[1]; // -> Safe
    builder["Password"] = args[1]; // -> Safe
    cb["Password"] = args[1]; // -> Safe
    cb.Password = args[1]; // -> Safe
}
