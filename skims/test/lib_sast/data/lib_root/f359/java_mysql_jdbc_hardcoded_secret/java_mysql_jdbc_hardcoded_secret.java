// Source: https://semgrep.dev/r?q=java.mysql.mysql-jdbc-hardcoded-secret.mysql-jdbc-hardcoded-secret
import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;

public class DataSrc {
    public static DataSource getMysqlDataSource(){
         String password = "password";
        MysqlDataSource src = new MysqlDataSource();

        src.setPassword("aaaa"); // -> Vulnerable

        src.setPassword(config); // -> Safe

        src.setPassword(password); // -> Vulnerable
        return src;
    }
}
