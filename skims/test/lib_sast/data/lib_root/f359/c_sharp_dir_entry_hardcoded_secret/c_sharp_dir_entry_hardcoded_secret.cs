using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;
using System.Threading.Tasks;


namespace adduser
{
    class Program
    {
        static void Main(string[] args)
        {
                string emptyPassword = "";
                string password = "123";
                var net = new DirectoryEntry();
                var users = net.Children.Add("test", "User");

                users.Invoke("SetPassword", ""); // -> Safe
                users.Invoke("SetPassword", "123!"); // -> Vulnerable
                users.Invoke("SetPassword", new object[] { "" }); // -> Safe
                users.Invoke("SetPassword", new object[] { "123" }); // -> Vulnerable
                users.Invoke("SetPassword", args[1]); // -> Safe

                var users2 = net.Children.Add("test", User);
                users2.Invoke("SetPassword", ""); // -> Safe
                users2.Invoke("SetPassword", "123!"); // -> Vulnerable

                DirectoryEntry AD = new DirectoryEntry();

                    DirectoryEntry Username = AD.Children.Find();

                    Username.Invoke("SetPassword", emptyPassword); // -> Safe
                    Username.Invoke("SetPassword", password);  // -> Vulnerable
                    Username.Invoke("SetPassword", config); // -> Safe


                DirectoryEntry testusername = new DirectoryEntry().Children.Find();
                testusername.Invoke("SetPassword", emptyPassword); // -> Safe
                testusername.Invoke("SetPassword", password);  // -> Vulnerable
                testusername.Invoke("SetPassword", "password123");  // -> Vulnerable
                testusername.Invoke("SetPassword", config); // -> Safe


                new DirectoryEntry().Children().Find().Invoke("SetPassword", emptyPassword); // -> Safe
                new DirectoryEntry().Children().Find().Invoke("SetPassword", password);  // -> Vulnerable
                new DirectoryEntry().Children.Find().Invoke("SetPassword", "password101"); // -> Vulnerable
                new DirectoryEntry().Children.Find().Invoke("SetPassword", config); // -> Safe


                var net3 = new DirectoryNoEntry(); // No DirectoryEntry
                var users3 = net3.Children.Add("test", "User");

                users3.Invoke("SetPassword", ""); // -> Safe
                users3.Invoke("SetPassword", "123!"); // -> Safe
        }
    }
}
