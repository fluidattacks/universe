// Source: https://semgrep.dev/r?q=java.lang.security.system.system-setproperty-hardcoded-secret.system-setproperty-hardcoded-secret
public class Client {
	public void connect() {

        System.setProperty("javax.net.ssl.keyStorePassword", "password"); // -> Vulnerable

        System.setProperty("javax.net.ssl.trustStorePassword", "password");  // -> Vulnerable

        System.setProperty("javax.net.ssl.keyStorePassword", ""); // -> Safe

        System.setProperty("javax.net.ssl.trustStorePassword", config); // -> Safe

        System.setProperty("javax.net.ssl.keyStorePassword", config); // -> Safe
	}

}
