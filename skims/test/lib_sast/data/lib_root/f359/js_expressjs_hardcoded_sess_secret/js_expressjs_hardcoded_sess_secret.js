const express = require('express');
const session = require('express-session');
require('dotenv').config(); // Load environment variables from a .env file

const app = express();

// Vulnerable: The secret key is hardcoded directly in the code
const vulnerable_secret_key = 'hardcoded_secret_key'

app.use(session({ // -> Vulnerable
  secret: 'hardcoded_secret_key',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 3600000,
    httpOnly: true,
    secure: true,
  }
}));


app.use(session({ // -> Vulnerable
  secret: vulnerable_secret_key,
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 3600000,
    httpOnly: true,
    secure: true,
  }
}));


// Secure: The secret key is loaded from an environment variable
const secure_secret_key = process.env.SESSION_SECRET

app.use(session({ // -> Secure
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 3600000,
    httpOnly: true,
    secure: true,
  }
}));


app.use(session({ // -> Secure
  secret: secure_secret_key,
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 3600000,
    httpOnly: true,
    secure: true,
  }
}));


app.get('/', (req, res) => {
  res.send('Hello world!');
});

app.listen(3000, () => {
  console.log('Server running on http://localhost:3000');
});
