package pe.interbank.mmp.user.domain.aggregates.user.security;

import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;

class JwtTokenUtilTest {


    @InjectMocks
    private JwtTokenUtil jwtTokenUtil;

    @Mock
    private JwtAuthenticationRequest authenticationRequest;

    // Despite secret is hardcoded this is a test, so it is safe
    @Test
    void testGetClaimsFromToken() {
        String token = Jwts.builder()
                           .setSubject("testUser")
                           .setExpiration(new Date(System.currentTimeMillis() + 3600 * 1000))
                           .signWith(SignatureAlgorithm.HS512, "mySecret")
                           .compact();

        Claims claims = jwtTokenUtil.getClaimsFromToken(token);

        assertNotNull(claims);
        assertEquals("testUser", claims.getSubject());
    }

}
