import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtService {

    private String vulnerableSecret = "super_secret_1";

    // Generate a token
    public String generateTokenVulnerable(String username) {
        return Jwts.builder() // -> Vulnerable
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10)) // 10 hours expiration
                .signWith(SignatureAlgorithm.HS256, vulnerableSecret)
                .compact();
    }

    // Validate a token
    public Claims extractClaimsVulnerable(String token) {
        return Jwts.parser() // -> Vulnerable
                .setSigningKey(vulnerableSecret)
                .parseClaimsJws(token)
                .getBody();
    }

    @Value("${jwt.secret}")
    private String secureSecret;

    // Generate a token
    public String generateTokenSecure(String username) {
        return Jwts.builder() // -> Secure
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10)) // 10 hours expiration
                .signWith(SignatureAlgorithm.HS256, secureSecret)
                .compact();
    }

    // Validate a token
    public Claims extractClaimsSecure(String token) {
        return Jwts.parser() // -> Secure
                .setSigningKey(secureSecret)
                .parseClaimsJws(token)
                .getBody();
    }

}
