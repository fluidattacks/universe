// Source: https://semgrep.dev/r?q=gitlab.find_sec_bugs.HARD_CODE_PASSWORD-1
import javax.security.auth.kerberos.KerberosKey;
import javax.security.auth.kerberos.KerberosPrincipal;
import javax.crypto.spec.PBEKeySpec;
// Case 6. PBEKeySpec Pattern
PBEKeySpec vulnerableSpec = new PBEKeySpec("hardcodedPass123", salt, iterations); // -> Vulnerable

String pbePasswordStr = "hardcodedPass123";
PBEKeySpec vulnerableSpec = new PBEKeySpec(pbePasswordStr, salt, iterations); // -> Vulnerable

String pbePassword = SecureConfigLoader.getPBEPassword();
PBEKeySpec secureSpec = new PBEKeySpec(pbePassword, salt, iterations); // -> Safe

PBEKeySpec keySpec = new PBEKeySpec("hardcodedPassword".toCharArray(), salt, iterationCount); // -> Vulnerable

String pbePasswordStrToCharArr = "hardcodedPassword".toCharArray();
PBEKeySpec keySpec = new PBEKeySpec(pbePasswordStrToCharArr, salt, iterationCount); // -> Vulnerable

String password = System.getenv("PBE_PASSWORD");
PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, iterationCount); // -> Safe


// Case 9. KerberosKey Pattern
KerberosKey vulnerableKey = new KerberosKey(principal, "hardcodedPass123", algorithm); // -> Vulnerable

String pbePasswordStr = "hardcodedPass123";
KerberosKey vulnerableKey = new KerberosKey(principal, pbePasswordStr, algorithm); // -> Vulnerable

String kerberosPassword = SecureConfigLoader.getKerberosPassword();
KerberosKey secureKey = new KerberosKey(principal, kerberosPassword, algorithm); // -> Safe

KerberosKey key = new KerberosKey(principal, "hardcodedPassword".toCharArray(), null, KerberosKey.DES3_CBC_SHA1_KD); // -> Vulnerable

String pbePasswordStrToCharArr = "hardcodedPassword".toCharArray();
KerberosKey key = new KerberosKey(principal, pbePasswordStrToCharArr, null, KerberosKey.DES3_CBC_SHA1_KD); // -> Vulnerable

String password = System.getenv("KERBEROS_PASSWORD");
KerberosKey key = new KerberosKey(principal, password.toCharArray(), null, KerberosKey.DES3_CBC_SHA1_KD); // -> Safe
