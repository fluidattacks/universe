using System.Net.Http;

class ExampleClass{

    void VulnCases(){

        WinHttpHandler winHttpHandler = new WinHttpHandler();
        winHttpHandler.CheckCertificateRevocationList = false;

        bool enabledCertification = false;
        WinHttpHandler winHttpHandler2 = new WinHttpHandler();
        winHttpHandler2.CheckCertificateRevocationList = enabledCertification;

    }

    void SafeCases(){

        WinHttpHandler winHttpHandler3 = new WinHttpHandler();
        winHttpHandler3.CheckCertificateRevocationList = true;

        customHttpHandler = new CustomHttpHandler();
        customHttpHandler.CheckCertificateRevocationList = false;
    }

}
