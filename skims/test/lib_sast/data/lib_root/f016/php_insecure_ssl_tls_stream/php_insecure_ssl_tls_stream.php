<?php

use Illuminate\Support\Facades\Http;

$encrypt = STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT;

# No compliant

$vuln_context_1 = stream_context_create([
  'ssl' => [
      'crypto_method' => STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT
  ],
]);

$vuln_context_2 = stream_context_create([
  'ssl' => [
      'crypto_method' => $encrypt
  ],
]);


$vuln_context_3 = stream_socket_enable_crypto($socket, true, STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT);

$vuln_context_4 = stream_socket_enable_crypto($socket, true, $encrypt);

# Compliant

$secure_context_1 = stream_context_create([
  'ssl' => [
      'crypto_method' => STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT
  ],
]);

$secure_context_2 = stream_socket_enable_crypto($socket, true, STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);

?>
