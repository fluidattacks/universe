resource "azurerm_redis_cache" "vulnerable1" {
  name                = "vulnerable1"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  capacity            = 2
  family              = "C"
  sku_name            = "Standard"
  minimum_tls_version = "1.0"
  redis_configuration {
    authentication_enabled = true
  }
}
resource "azurerm_redis_cache" "vulnerable2" {
  name                = "vulnerable2"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  capacity            = 2
  family              = "C"
  sku_name            = "Standard"
  redis_configuration {
    authentication_enabled = true
  }
}
resource "azurerm_redis_cache" "safe" {
  name                = "safe"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  capacity            = 2
  family              = "C"
  sku_name            = "Standard"
  minimum_tls_version = "1.2"
  redis_configuration {
    authentication_enabled = true
  }
}
