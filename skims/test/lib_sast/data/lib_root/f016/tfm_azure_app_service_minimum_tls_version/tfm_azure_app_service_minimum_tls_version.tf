resource "azurerm_linux_web_app" "vulnerable" {
  name                = "example-app-service"
  service_plan_id     = "plan_id"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  https_only          = true

  site_config {
    always_on           = true
    minimum_tls_version = "1.0"
  }
  auth_settings {
    enabled = true
  }
}
resource "azurerm_linux_web_app" "safe1" {
  name                = "example-app-service"
  service_plan_id     = "plan_id"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  https_only          = true

  site_config {
    always_on           = true
    minimum_tls_version = "1.3"
  }
  auth_settings {
    enabled = true
  }
}
resource "azurerm_linux_web_app" "safe2" {
  name                = "example-app-service"
  service_plan_id     = "plan_id"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  https_only          = true

  site_config {
    always_on = true
  }
  auth_settings {
    enabled = true
  }
}
