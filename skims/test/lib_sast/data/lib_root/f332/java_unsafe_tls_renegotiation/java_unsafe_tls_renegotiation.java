// Source: https://semgrep.dev/r?q=problem-based-packs.insecure-transport.java-stdlib.tls-renegotiation.tls-renegotiation
class Bad {
    public void bad1() {
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", true); // -> Vulnerable
    }

    public void bad2() {
        String propertyName = "sun.security.ssl.allowUnsafeRenegotiation";
        boolean allowUnsafe = true;
        java.lang.System.setProperty(propertyName, allowUnsafe); // -> Vulnerable
    }

    public void bad3() {
        boolean allowUnsafe = true;
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", allowUnsafe); // -> Vulnerable
    }
}

class Ok {
    public void ok1() {
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", false); // -> Safe
    }

    public void ok2() {
        java.lang.System.setProperty("sun.security.ssl.otherProperty", true); // -> Safe
    }

    public void ok3() {
        String propertyName = "sun.security.ssl.allowUnsafeRenegotiation";
        boolean allowUnsafe = false;
        java.lang.System.setProperty(propertyName, allowUnsafe); // -> Safe
    }

    public void ok4() {
        boolean allowUnsafe = false;
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", allowUnsafe); // -> Safe
    }

    public void ok5(boolean allowUnsafe) {
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", allowUnsafe); // -> Safe
    }
}
