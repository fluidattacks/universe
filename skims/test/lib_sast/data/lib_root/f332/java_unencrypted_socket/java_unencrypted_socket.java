// Source: https://semgrep.dev/playground/r/java.lang.security.audit.crypto.unencrypted-socket.unencrypted-socket?editorMode=advanced
package testcode.crypto;

import javax.net.ssl.SSLServerSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.ServerSocket;

public class UnencryptedSocket {

    static void sslSocket() throws IOException {
        Socket soc = SSLSocketFactory.getDefault().createSocket("www.google.com", 443); // -> Safe
        doGetRequest(soc);
    }

    static void plainSocket() throws IOException {
        Socket soc = new Socket("www.google.com", 80); // -> Vulnerable
        doGetRequest(soc);
    }

    static void otherConstructors() throws IOException {
        Socket soc1 = new Socket("www.google.com", 80, true); // -> Vulnerable
        doGetRequest(soc1);
        byte[] address = {127, 0, 0, 1};
        Socket soc2 = new Socket("www.google.com", 80, InetAddress.getByAddress(address), 13337); // -> Vulnerable
        doGetRequest(soc2);
        byte[] remoteAddress = {74, 125, (byte) 226, (byte) 193};
        Socket soc3 = new Socket(InetAddress.getByAddress(remoteAddress), 80); // -> Vulnerable
        doGetRequest(soc2);
    }

    static void doGetRequest(Socket soc) throws IOException {
        System.out.println("");
        soc.close();
    }
}

public class UnencryptedServerSocket {

    static void sslServerSocket() throws IOException {
        ServerSocket ssoc = SSLServerSocketFactory.getDefault().createServerSocket(1234); // -> Safe
        ssoc.close();
    }

    static void plainServerSocket() throws IOException {
        ServerSocket ssoc = new ServerSocket(1234); // -> Vulnerable
        ssoc.close();
    }

    static void otherConstructors() throws IOException {
        ServerSocket ssoc1 = new ServerSocket(); // -> Vulnerable
        ssoc1.close();
        ServerSocket ssoc2 = new ServerSocket(1234, 10); // -> Vulnerable
        ssoc2.close();
        byte[] address = {127, 0, 0, 1};
        ServerSocket ssoc3 = new ServerSocket(1234, 10, InetAddress.getByAddress(address)); // -> Vulnerable
        ssoc3.close();
    }

}
