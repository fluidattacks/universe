resource "kubernetes_service" "my_service" {
  metadata {
    name = "my-service"
  }
  spec {
    selector = {
      "app.kubernetes.io/name" = "MyApp"
    }
    port {
      name        = "http"
      protocol    = "TCP"
      port        = 80
      target_port = 80
    }
    port {
      name        = "https"
      protocol    = "TCP"
      port        = 443
      target_port = 443
    }
  }
}
