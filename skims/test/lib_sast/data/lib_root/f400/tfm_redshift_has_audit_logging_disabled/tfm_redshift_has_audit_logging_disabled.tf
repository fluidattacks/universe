resource "aws_redshift_cluster" "vulnexample" {
  cluster_identifier = "tf-redshift-cluster"

}

resource "aws_redshift_cluster" "vulnexample2" {
  cluster_identifier = "tf-redshift-cluster"

  logging {
    enable        = false
    bucket_name   = "my-s3-audit-log-bucket"
    s3_key_prefix = "redshift-logs/"
  }
}

resource "aws_redshift_cluster" "safeexample" {
  cluster_identifier = "tf-redshift-cluster"

  logging {
    enable        = true
    bucket_name   = "my-s3-audit-log-bucket"
    s3_key_prefix = "redshift-logs/"
  }
}
