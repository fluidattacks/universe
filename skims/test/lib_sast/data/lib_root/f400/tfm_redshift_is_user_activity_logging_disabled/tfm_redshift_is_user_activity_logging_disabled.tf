resource "aws_redshift_parameter_group" "vulnexample" {
  name        = "my-parameter-group"
  family      = "redshift-1.0"
  description = "My Redshift cluster parameter group"

  parameter {
    name  = "enable_user_activity_logging"
    value = "false"
  }
  parameter {
    name  = "query_group"
    value = "my_query_group"
  }
}

resource "aws_redshift_parameter_group" "vulnexample2" {
  name        = "my-parameter-group"
  family      = "redshift-1.0"
  description = "My Redshift cluster parameter group"

  parameter {
    name  = "query_group"
    value = "my_query_group"
  }
}

resource "aws_redshift_parameter_group" "safeexample" {
  name        = "my-parameter-group"
  family      = "redshift-1.0"
  description = "My Redshift cluster parameter group"

  parameter {
    name  = "query_group"
    value = "my_query_group"
  }

  parameter {
    name  = "enable_user_activity_logging"
    value = "true"
  }

}
