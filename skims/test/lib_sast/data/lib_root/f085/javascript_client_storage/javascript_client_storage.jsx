// Get is not sensitive
localStorage.getItem('non-sensitive');
localStorage.getItem(non.sensitive);
localStorage.getItem('is_authenticated');
localStorage.getItem('_isAuthorized');

// setItem tests
function tryLogin(data, sessionID, authentication) {
  try {
    if (data['access_token']) {
      // Dangerous tokens
      sessionStorage.setItem('token', data.totp_token);
      sessionStorage.setItem('password', sessionID);
      sessionStorage.setItem('password', authentication.token);

      const decodedToken = parseJwt(data['access_token']);
      const timeToExpire = (decodedToken.exp * 1000 - 45 * 60 * 1000) / 1000;
      const encryptTime = encodeAndObfuscate(timeToExpire);
      // Session expiration time is not a dangerous value
      sessionStorage.setItem('ext', encryptTime);
      // Boolean values
      sessionStorage.setItem('has_password', data.has_password);
      // Undeterministic
      sessionStorage.setItem('token_exp', getExpirationDate(data.totp_token));
      sessionStorage.setItem('password', encryptPassword(data.password));
    }
  } catch (e) {
    throw new Error(`Error on token ${e}`);
  }
}
