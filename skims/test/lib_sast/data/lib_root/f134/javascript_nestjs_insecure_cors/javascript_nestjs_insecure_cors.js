import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors(); // -> Vulnerable

  const appVulnerable = await NestFactory.create(AppModule, { cors: true }); // -> Vulnerable
  await appVulnerable.listen(3001);

  const appVulnerable2 = await NestFactory.create(AppModule);
  appVulnerable2.enableCors({ // -> Vulnerable
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
  });
  await appVulnerable2.listen(3002);

  const appVulnerable4 = await NestFactory.create(AppModule, { // -> Vulnerable
    cors: {
      origin: ['https://trusteddomain.com', '*'],
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type', 'Authorization'],
      credentials: true
    }
  });
  await appVulnerable4.listen(3001);

  const appVulnerable5 = await NestFactory.create(AppModule, { // -> Vulnerable
    cors: {
      origin: ['*'],
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type', 'Authorization'],
      credentials: true
    }
  });
  await appVulnerable5.listen(3001);

  const appVulnerable6 = await NestFactory.create(AppModule, { // -> Vulnerable
    cors: {
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type', 'Authorization'],
      credentials: true,
      origin: '*',
    }
  });
  await appVulnerable6.listen(3001);

  const appVulnerable7 = await NestFactory.create(AppModule, { // -> Vulnerable
    cors: {
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type', 'Authorization'],
      credentials: true,
      origin: true,
    }
  });
  await appVulnerable7.listen(3001);

  const origins = ['https://trusteddomain.com', 'https://anothertrusted.com', '*'];
  const appVulnerable8 = await NestFactory.create(AppModule, { // -> Vulnerable
    cors: {
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type', 'Authorization'],
      credentials: true,
      origin: origins,
    }
  });
  await appVulnerable8.listen(3001);

  const origins2 = true;
  const appVulnerable9 = await NestFactory.create(AppModule, { // -> Vulnerable
    cors: {
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type', 'Authorization'],
      credentials: true,
      origin: origins2,
    }
  });
  await appVulnerable9.listen(3001);

  const originsConfig = '*';
  const appVulnerable10 = await NestFactory.create(AppModule);
  appVulnerable10.enableCors({ // -> Vulnerable
    origin: originsConfig,
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
  });
  await appVulnerable10.listen(3002);

  const appVulnerable11 = await NotNestFactory.create(AppModule);
  appVulnerable11.enableCors({ // -> Safe
    origin: originsConfig,
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
  });
  await appVulnerable11.listen(3002);


  await appSecure2.listen(3004);

  const appSecure = await NestFactory.create(AppModule);
  appSecure.enableCors({ // -> Safe
    origin: ['https://trusteddomain.com'],
    methods: ['GET', 'POST'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,
  });
  await appSecure.listen(3003);

  const appSecure2 = await NestFactory.create(AppModule);
  appSecure2.enableCors({ // -> Safe
    origin: (origin, callback) => {
      const allowedOrigins = ['https://trusteddomain.com', 'https://anothertrusted.com'];
      if (!origin || allowedOrigins.includes(origin)) {
        callback(null, true);
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    },
    methods: ['GET', 'POST'],
    credentials: true,
  });
  await appSecure2.listen(3004);

  console.log(`Servers running on ports 3001, 3002, 3003, and 3004`);
}

const appSecure = await NestFactory.create(AppModule, { // -> Safe
  cors: {
    origin: ['https://trusteddomain.com'],
    methods: ['GET', 'POST'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true
  }
});


bootstrap();
