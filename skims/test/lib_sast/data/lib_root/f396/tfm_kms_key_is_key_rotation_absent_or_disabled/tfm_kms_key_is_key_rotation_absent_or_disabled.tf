resource "aws_kms_key" "a" {
  description              = "Unsafe Key"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = 10
}

resource "aws_kms_key" "a" {
  description              = "Unsafe Key 2"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = 10
  enable_key_rotation      = false
}

resource "aws_kms_key" "a" {
  description              = "Safe Key"
  customer_master_key_spec = "RSA_2048"
  enable_key_rotation      = false
  deletion_window_in_days  = 10
}

resource "aws_kms_key" "a" {
  description              = "Sake key 2"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  enable_key_rotation      = true
  deletion_window_in_days  = 10
}
