using System;
using System.Security.Cryptography;

// Source: https://semgrep.dev/r?q=csharp.dotnet.security.use_ecb_mode.use_ecb_mode
public class Encryption
{
	public void EncryptWithAesEcb() {
		Aes key = Aes.Create();
		key.Mode = CipherMode.ECB; // -> Vulnerable
		using var encryptor = key.CreateEncryptor();
		byte[] msg = new byte[32];
		var cipherText = encryptor.TransformFinalBlock(msg, 0, msg.Length);
	}

	public void EncryptWithAesEcb2() {
		Aes key = Aes.Create();
		byte[] msg = new byte[32];
		var cipherText = key.EncryptEcb(msg, PaddingMode.PKCS7); // -> Vulnerable
	}

	public void DecryptWithAesEcb(byte[] cipherText) {
		Aes key = Aes.Create();
		key.Mode = CipherMode.ECB; // -> Vulnerable
		using var decryptor = key.CreateDecryptor();
		var msg = decryptor.TransformFinalBlock(cipherText, 0, cipherText.Length);
	}

	public void DecryptWithAesEcb2(byte[] cipherText) {
		Aes key = Aes.Create();
		var msgText = key.DecryptEcb(cipherText, PaddingMode.PKCS7); // -> Vulnerable
	}

	public void EncryptWith3DESEcb() {
		TripleDES key = TripleDES.Create();
		key.Mode = CipherMode.ECB; // -> Vulnerable
		using var encryptor = key.CreateEncryptor();
		byte[] msg = new byte[32];
		var cipherText = encryptor.TransformFinalBlock(msg, 0, msg.Length);
	}

	public void EncryptWith3DESEcb2() {
		TripleDES key = TripleDES.Create();
		byte[] msg = new byte[32];
		var cipherText = key.EncryptEcb(msg, PaddingMode.PKCS7); // -> Vulnerable
	}

	public void DecryptWith3DESEcb(byte[] cipherText) {
		TripleDES key = TripleDES.Create();
		key.Mode = CipherMode.ECB; // -> Vulnerable
		using var decryptor = key.CreateDecryptor();
		var msg = decryptor.TransformFinalBlock(cipherText, 0, cipherText.Length);
	}

	public void DecryptWith3DESEcb2(byte[] cipherText) {
		TripleDES key = TripleDES.Create();
		var msgText = key.DecryptEcb(cipherText, PaddingMode.PKCS7); // -> Vulnerable
	}

	public void EncryptWithEcb(SymmetricAlgorithm key) {
		key.Mode = CipherMode.ECB; // -> Vulnerable
		using var encryptor = key.CreateEncryptor();
		byte[] msg = new byte[32];
		var cipherText = encryptor.TransformFinalBlock(msg, 0, msg.Length);
	}

	public void EncryptWithEcb2(SymmetricAlgorithm key) {
		byte[] msg = new byte[32];
		var cipherText = key.EncryptEcb(msg, PaddingMode.PKCS7); // -> Vulnerable
	}

	public void DecryptWithEcb(SymmetricAlgorithm key, byte[] cipherText) {
		key.Mode = CipherMode.ECB; // -> Vulnerable
		using var decryptor = key.CreateDecryptor();
		var msg = decryptor.TransformFinalBlock(cipherText, 0, cipherText.Length);
	}

	public void DecryptWithEcb2(SymmetricAlgorithm key, byte[] cipherText) {
		var msgText = key.DecryptEcb(cipherText, PaddingMode.PKCS7); // -> Vulnerable
	}

	public void EncryptWithAesCbc() {
		Aes key = Aes.Create();
		key.Mode = CipherMode.CBC; // -> Safe for f282
		using var encryptor = key.CreateEncryptor();
		byte[] msg = new byte[32];
		var cipherText = encryptor.TransformFinalBlock(msg, 0, msg.Length);
	}

	public void EncryptWithAesCbc2() {
		Aes key = Aes.Create();
		byte[] msg = new byte[32];
		byte[] iv = new byte[16];
		var cipherText = key.EncryptCbc(msg, iv, PaddingMode.PKCS7); // -> Safe for f282
	}

	public void DecryptWithAesCbc(byte[] cipherText) {
		Aes key = Aes.Create();
		key.Mode = CipherMode.CBC; // -> Safe for f282
		using var decryptor = key.CreateDecryptor();
		var msg = decryptor.TransformFinalBlock(cipherText, 0, cipherText.Length);
	}

	public void DecryptWithAesCbc2(byte[] cipherText, byte[] iv) {
		Aes key = Aes.Create();
		var msgText = key.DecryptCbc(cipherText, iv, PaddingMode.PKCS7);  // -> Safe for f282
	}

	public static void Main()
	{
		Console.WriteLine("Hello World");
	}
}
