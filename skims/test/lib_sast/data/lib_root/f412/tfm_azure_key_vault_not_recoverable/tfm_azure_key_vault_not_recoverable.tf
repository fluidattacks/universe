resource "azurerm_key_vault" "vulnerable" {
  name                       = "examplekeyvault"
  sku_name                   = "fluid"
  location                   = azurerm_resource_group.example.location
  resource_group_name        = azurerm_resource_group.example.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days = 7
}

resource "azurerm_key_vault" "vulnerable2" {
  name                     = "examplekeyvault"
  sku_name                 = "fluid"
  location                 = azurerm_resource_group.example.location
  resource_group_name      = azurerm_resource_group.example.name
  tenant_id                = data.azurerm_client_config.current.tenant_id
  purge_protection_enabled = false
}

resource "azurerm_key_vault" "vulnerable3" {
  name                       = "examplekeyvault"
  sku_name                   = "fluid"
  location                   = azurerm_resource_group.example.location
  resource_group_name        = azurerm_resource_group.example.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  purge_protection_enabled   = true
  soft_delete_retention_days = 7
}

resource "azurerm_key_vault" "safe" {
  name                       = "examplekeyvault"
  sku_name                   = "fluid"
  location                   = azurerm_resource_group.example.location
  resource_group_name        = azurerm_resource_group.example.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  purge_protection_enabled   = true
  soft_delete_retention_days = 90
}

resource "azurerm_key_vault" "safe" {
  name                     = "examplekeyvault"
  sku_name                 = "fluid"
  location                 = azurerm_resource_group.example.location
  resource_group_name      = azurerm_resource_group.example.name
  tenant_id                = data.azurerm_client_config.current.tenant_id
  purge_protection_enabled = true
}
