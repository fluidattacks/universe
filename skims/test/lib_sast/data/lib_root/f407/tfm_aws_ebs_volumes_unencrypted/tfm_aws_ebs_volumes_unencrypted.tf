resource "aws_launch_configuration" "vuln" {
  instance_type = "t2.micro"
  image_id      = "fluid"
  root_block_device {
    encrypted = false
  }

  ebs_block_device {
    device_name = "fluid_test"
  }
}


resource "aws_launch_configuration" "safe" {
  instance_type = "t2.micro"
  image_id      = "fluid_image"
  root_block_device {
    encrypted = true
  }
}
