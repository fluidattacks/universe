resource "aws_db_instance" "example" {
  allocated_storage                   = 20
  engine                              = "mysql"
  engine_version                      = "5.7"
  instance_class                      = "db.t2.micro"
  name                                = "example"
  username                            = "admin"
  password                            = "password"
  parameter_group_name                = "default.mysql5.7"
  iam_database_authentication_enabled = false
}

resource "aws_db_instance" "example2" {
  allocated_storage    = 20
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "example"
  username             = "admin"
  password             = "password"
  parameter_group_name = "default.mysql5.7"
}