resource "aws_redshift_cluster" "my_redshift_cluster" {
  cluster_identifier  = "my-redshift-cluster"
  database_name       = "mydb"
  master_username     = "myusername"
  master_password     = "mypassword"
  node_type           = "ds2.xlarge"
  cluster_type        = "multi-node"
  number_of_nodes     = 2
  publicly_accessible = true
}
