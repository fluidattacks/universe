resource "aws_elasticache_cluster" "example" {
  cluster_id                 = "cache-cluster-example"
  engine                     = "redis"
  node_type                  = "cache.m4.large"
  num_cache_nodes            = 1
  parameter_group_name       = "default.redis3.2"
  transit_encryption_enabled = false
}
