resource "aws_cognito_user_pool" "my_pool" {
  name              = "my-user-pool"
  mfa_configuration = "OFF"
}
