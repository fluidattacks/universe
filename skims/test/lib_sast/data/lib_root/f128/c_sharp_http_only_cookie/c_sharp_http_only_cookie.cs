using System.Web;

// Non compliant
class HttpOnlyGenerator1
{
    public HttpCookie InsecureCase1(HttpCookie httpCookie)
    {
        httpCookie.HttpOnly = false;
        return httpCookie;
    }
}

class HttpOnlyGenerator2
{
    public HttpCookie InsecureCase2(HttpCookie httpCookie)
    {
        bool httpOnly = false;
        httpCookie.HttpOnly = httpOnly;
        return httpCookie;
    }
}


// Compliant
class HttpOnlyGenerator3
{
    public HttpCookie SecureCase1(HttpCookie httpCookie)
    {
        httpCookie.HttpOnly = true;
        return httpCookie;
    }
}

class HttpOnlyGenerator5
{
    public HttpCookie SecureCase2(HttpCookie httpCookie)
    {
        bool httpOnly = true;
        httpCookie.HttpOnly = httpOnly;
        return httpCookie;
    }
}
