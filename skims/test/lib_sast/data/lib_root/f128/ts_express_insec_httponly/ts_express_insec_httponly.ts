import express from 'express';
import session from 'express-session';

const app = express();

const vuln_config: session.SessionOptions = {
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: false, // Explicitly allows JavaScript access to the cookie
    maxAge: 24 * 60 * 60 * 1000 // 1 day
  }
};

const safe_cookie_config: session.SessionOptions = {
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    secure: false,
    maxAge: 24 * 60 * 60 * 1000 // 1 day
  }
};

// Default values of both cookie and httpOnly are safe
// https://www.npmjs.com/package/express-session

const other_safe_cookie_config: session.SessionOptions = {
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: false,
    maxAge: 24 * 60 * 60 * 1000 // 1 day
  }
};

const another_safe_cookie_config: session.SessionOptions = {
  resave: false,
  saveUninitialized: true
};

// vuln cases
app.use(session({ // -> Vulnerable
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: false, // Explicitly allows JavaScript access to the cookie
    maxAge: 24 * 60 * 60 * 1000 // 1 day
  }
}));

app.use(session(vuln_config));

// safe cases
app.use(session()); // -> Default safe values

app.use(session({ // -> Safe
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    maxAge: 24 * 60 * 60 * 1000 // 1 day
  }
}));

app.use(session(safe_cookie_config));
app.use(session(other_safe_cookie_config));
app.use(session(another_safe_cookie_config));
