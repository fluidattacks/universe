<?php

class Admins
{
   function unsafe_cookie($adminid)
   {
      $query = sprintf(
         "INSERT into `admon_table` (`id`, `admin_id`, `created_on`) VALUES (NULL, '%s', NOW());",
		   mysql_real_escape_string($adminid)
      );
      if ($res = mysql_query($query))
      {
         $id = mysql_insert_id();
         setcookie("session", $id);
         return $id;
      }
   }

   function undeterministic_cookie($adminid)
   {
      $query = sprintf(
         "INSERT into `admon_table` (`id`, `admin_id`, `created_on`) VALUES (NULL, '%s', NOW());",
		   mysql_real_escape_string($adminid)
      );
      $res = mysql_query($query);
      $id2 = mysql_insert_id();
      $safe_id = $id2 + randomized_value();
      setcookie("session", $safe_id);
      return $safe_id;
   }
}

?>
