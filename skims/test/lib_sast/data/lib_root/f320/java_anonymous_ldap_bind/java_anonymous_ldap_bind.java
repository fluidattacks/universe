public class Cls {

    public void ldapBind(Environment env) {
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env); // -> Vulnerable
    }

    DirContext authenticate(String dn, String password) throws AccountException {
        final Properties env = createContextProperties();
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.REFERRAL, referral);
        try {
            return new InitialDirContext(env); // -> Vulnerable
        } catch (NamingException e) {
            throw new AuthenticationFailedException("Incorrect username or password", e);
        }
    }

    public void ldapBindSafe(Environment env) {
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        DirContext ctx = new InitialDirContext(env); // -> Safe
    }

    DirContext authenticate2(String dn, String password) throws AccountException {
        final Properties env = createContextProperties();
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.REFERRAL, referral);
        try {
            return new InitialDirContext(env); // -> Safe
        } catch (NamingException e) {
            throw new AuthenticationFailedException("Incorrect username or password", e);
        }
    }

    DirContext authenticate2(String dn, String password) throws AccountException {
        final Properties env = createContextProperties();
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.REFERRAL, referral);
        try {
            return new InitialDirContext(env); // -> Safe
        } catch (NamingException e) {
            throw new AuthenticationFailedException("Incorrect username or password", e);
        }
    }

    public void ldapBindSafe2(Environment env) {
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(); // -> Safe
    }
}
