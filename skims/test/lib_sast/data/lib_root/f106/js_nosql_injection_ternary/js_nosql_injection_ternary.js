const db = require('../data/mongodb');
const customSanitizer = require('../utils/customSanitizer');
const database = require('../data/mariadb');
const BasketModel = require('../models/BasketModel'); // Adjust paths as needed
const ProductModel = require('../models/ProductModel'); // Adjust paths as needed
const WalletModel = require('../models/WalletModel'); // Adjust paths as needed

module.exports = function unsafeCase() {
  return async (req, res, next) => {
    try {
      const id = req.params.id;

      // Find the basket including products
      const basket = await BasketModel.findOne({
        where: { id },
        include: [{ model: ProductModel, paranoid: false, as: 'Products' }]
      });

      if (!basket) {
        return res.status(404).json({ message: 'Basket not found' });
      }

      // Find the wallet
      const wallet = await WalletModel.findOne({
        where: { UserId: req.body.UserId }
      });

      if (!wallet) {
        return res.status(404).json({ message: 'Wallet not found' });
      }

      // Insert order into database
      await db.orders.insert({
        promotionalAmount: discountAmount,
        paymentId: req.body.orderDetails ? req.body.orderDetails.paymentId : null,
        addressId: req.body.orderDetails ? req.body.orderDetails.addressId : null,
        eta: req.body.deliveryMethod.eta.toString()
      });

      res.status(200).json({ message: 'Order placed successfully' });

    } catch (error) {
      next(error); // Pass errors to the error-handling middleware
    }
  };
};

module.exports = function safeCase() {
  return async (req, res, next) => {
    try {
      const id = req.params.id;

      // Find the basket including products
      const basket = await BasketModel.findOne({
        where: { id },
        include: [{ model: ProductModel, paranoid: false, as: 'Products' }]
      });

      if (!basket) {
        return res.status(404).json({ message: 'Basket not found' });
      }

      // Find the wallet
      const wallet = await WalletModel.findOne({
        where: { UserId: req.body.UserId }
      });

      if (!wallet) {
        return res.status(404).json({ message: 'Wallet not found' });
      }

      // Insert order into database with sanitized inputs
      await db.orders.order({
        promotionalAmount: discountAmount,
        paymentId: req.body.orderDetails ? customSanitizer(req.body.orderDetails.paymentId) : null,
        addressId: req.body.orderDetails ? customSanitizer(req.body.orderDetails.addressId) : null,
        eta: req.body.deliveryMethod.eta.toString()
      });

      res.status(200).json({ message: 'Order placed successfully' });

    } catch (error) {
      next(error); // Pass errors to the error-handling middleware
    }
  };
};
