declare var module: any;

function ts_vuln (req, res) {
    const value = req.query.value;

    res.setHeader("Set-Cookie", value);  // Noncompliant
    res.cookie("connect.sid", value);  // Noncompliant

    res.setHeader("X-Data", value); // Compliant
    res.cookie("data", value); // Compliant
};

import fs from 'fs'
import { type Request, type Response, type NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import * as utils from './utils'


export const publicKey = fs ? fs.readFileSync('encryptionkeys/jwt.pub', 'utf8') : 'placeholder-public-key'

export const updateAuthenticatedUsers = () => (req: Request, res: Response, next: NextFunction) => {
  const token = req.cookies.token || utils.jwtFrom(req)
  if (token) {
    jwt.verify(token, publicKey, (err: Error | null, decoded: any) => {
      if (err === null) {
        if (authenticatedUsers.get(token) === undefined) {
          authenticatedUsers.put(token, decoded)
          res.cookie('token', token) // Non-compliant
        }
      }
    })
  }
  next()
}
