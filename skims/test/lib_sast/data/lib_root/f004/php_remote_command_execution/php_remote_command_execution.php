<?php

$safe_command = 'ls -l';
$user_input =  $_REQUEST['input'];

$output = [];

// Vulnerable cases

// Executing command from $_GET
exec($_GET['var']);
shell_exec($_POST['val']);
system($_REQUEST['cmd']);
passthru($_REQUEST['cmd']);

exec("ls -l" . $_GET['cmd'], $output, 0);
shell_exec($_POST['command'] . "&& ls -l");
system("ls -l" . $_REQUEST['var'], 0);
passthru("ls -l" . $_REQUEST['var'], 0);


// Executing command from other user inputs
exec($user_input);
shell_exec($user_input);
system($user_input, 137);
passthru($user_input, 137);

exec("ls -l" . $user_input);
shell_exec("ls -l" . $user_input);
system("ls -l" . $user_input);
passthru("ls -l" . $user_input);

// Safe cases
// Executing a safe command
$safe_command = 'ls -l';
exec($safe_command);
shell_exec($safe_command);
system($safe_command);
passthru($safe_command);

// Executing command using parameterized command
$param_command = "ls -l " . escapeshellarg($_GET['param']);
exec($param_command);
shell_exec($param_command);
system($param_command);
passthru($param_command);

exec("ls -l" . escapeshellcmd($user_input), $output, 137);
shell_exec("ls -l" . escapeshellcmd($_POST["param"]));
system(escapeshellarg($user_input));
passthru(escapeshellarg($user_input));

?>
