import jwt, { VerifyErrors, JwtPayload } from 'jsonwebtoken';

const secretKey = 'my_secret_key';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImpvaG4uZG9lIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTE2MjM5MDIyfQ.kbYbLxObQKgIZ9W9vVhhH-lYLe7JhKMcINIRaZ89zV8';

// Verify the JWT token
jwt.verify(token, secretKey, (err: VerifyErrors | null, decoded: JwtPayload | undefined) => {
  if (err) {
    console.error('Token verification failed:', err.message);
  } else {
    console.log('Token verified successfully:');
    console.log(decoded);
  }
});
