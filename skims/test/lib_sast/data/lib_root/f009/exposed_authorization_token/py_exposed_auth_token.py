# type: ignore

import aiohttp
import http
from typing import (
    Any,
)

danger_headers = {
    "Accept": "text/html",
    "authorization": "Bearer YWUzYjQwODAwODA2Y2E5ZjdjNjkzOTFh"
    + "OWM5ZWZiMjQ6",
}


async def danger_func() -> Any:
    data = None
    async with aiohttp.ClientSession() as session:
        async with session.get(
            headers=danger_headers, url="test.com"
        ) as response:
            if response.status == 200:
                data = await response.text()
    return data


unsafe_1 = http.client.HTTPSConnection("www.example.com")
unsafe_1.request("GET", "/", headers=danger_headers)

safe_headers = {
    "Accept": "text/html",
    "authorization": "Bearer some_clearly_fake_token",
}

safe_1 = http.client.HTTPSConnection("www.example.com")
safe_1.request("GET", "/", headers=safe_headers)
