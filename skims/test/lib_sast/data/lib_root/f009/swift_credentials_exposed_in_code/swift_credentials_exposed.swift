import JWTDecode

let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImpvaG4uZG9lIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTE2MjM5MDIyfQ.kbYbLxObQKgIZ9W9vVhhH-lYLe7JhKMcINIRaZ89zV8"

do {
    let jwt = try decode(jwt: token)
    let claims = jwt.claims
    // Verify the JWT token
    let isVerified = try jwt.verifySignatureWith(Data(secret.utf8))

    if isVerified {
        print("Token is verified")
    } else {
        print("Token verification failed")
    }
} catch {
    print("Error decoding/verifying token: \(error)")
}
