import java.lang.Runtime;
import javax.crypto.NullCipher;

class Cls {

    public Cls() {
        System.out.println("Hello");
    }

    public byte[] test1(String plainText) {
        javax.crypto.NullCipher nullCipher = new javax.crypto.NullCipher(); // -> Vulnerable

        Cipher doNothingCipher = new NullCipher(); // -> Vulnerable

        byte[] cipherText = doNothingCipher.doFinal(plainText);
        return cipherText;
    }

    public void test2(String plainText) {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // -> Vulnerable
        byte[] cipherText = cipher.doFinal(plainText);
        return cipherText;
    }

    public void test3(String plainText) {
        useCipher(new NullCipher()); // -> Vulnerable
    }

    private static void useCipher(Cipher cipher, String secret) throws Exception {
       SecretKey key = new SecretKeySpec(secret.getBytes("UTF-8"), "AES");
       cipher.init(Cipher.ENCRYPT_MODE, key);
       byte[] plainText  = "aeiou".getBytes("UTF-8");
       byte[] cipherText = cipher.doFinal(plainText);
       System.out.println(new String(cipherText));
    }
}
