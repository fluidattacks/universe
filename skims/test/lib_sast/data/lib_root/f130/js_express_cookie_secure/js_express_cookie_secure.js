const express = require('express');
const session_alias = require('express-session');


const app = express();

const vuln_config = {
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: false, // Explicitly Allows JavaScript access to the cookie
    maxAge: 24 * 60 * 60 * 1000
  }
}

// Default values of both cookie and httpOnly are dangerous
//https://www.npmjs.com/package/express-session

const other_vuln_config =  {
  resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 24 * 60 * 60 * 1000
    }
};

const another_vuln_config =  {
  resave: false,
    saveUninitialized: true,
};


const safe_cookie_config = {
  resave: false,
    saveUninitialized: true,
    cookie: {
      httpOnly: true,
      secure: true,
      maxAge: 24 * 60 * 60 * 1000
    }
};


// vuln cases

app.use(session_alias({ // -> Vulnerable
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    secure: false,
    maxAge: 24 * 60 * 60 * 1000
  }
}));
app.use(session_alias());

app.use(session_alias(vuln_config));
app.use(session_alias(other_vuln_config));
app.use(session_alias(another_vuln_config));


// safe cases

app.use(session_alias({ // -> safe
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    secure: true,
    maxAge: 24 * 60 * 60 * 1000
  }
}));
