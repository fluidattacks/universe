// Source: https://semgrep.dev/r?q=java.micronaut.audit.cookies.missing-secure.missing-secure
package com.example;

import io.micronaut.http.*;
import io.micronaut.http.annotation.*;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.cookie.Cookie;
import io.micronaut.http.cookie.SameSite;
import io.micronaut.http.netty.cookies.NettyCookie;
import io.micronaut.http.server.cors.CrossOrigin;
import io.micronaut.http.simple.cookies.SimpleCookie;
import io.micronaut.http.simple.cookies.SimpleCookieFactory;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.serde.annotation.Serdeable;
import jakarta.inject.Inject;
import org.reactivestreams.Publisher;
import io.micronaut.http.HttpRequest;

import java.io.*;


@Controller("/hello")
public class HelloController {

  @Post("/test1")
  public MutableHttpMessage<Object> postTest1() throws FileNotFoundException {

    SimpleCookie s = new SimpleCookie("foo", "bar"); // -> Vulnerable

    Cookie c1 = getCookieSomewhere(); // -> Safe

    return HttpResponse.ok().cookie(Cookie.of("foo", "bar").secure(true)); // -> Safe
  }

  @Post("/test2")
  public MutableHttpMessage<Object> postTest2(HttpRequest<?> request) throws FileNotFoundException {

    Cookie cookie = request.getCookies()
            .findCookie( "foobar" )
            .orElse( new NettyCookie( "foo", "bar" ) ); // -> Vulnerable

    if ( someVar != null )
    {
      cookie.value( someVar );
    }

    Cookie c = new NettyCookie("foo", "bar"); // -> Safe
    c.secure(true);

    NettyCookie r = new NettyCookie("foo", "bar").secure(true); // -> Safe

    Cookie z = new NettyCookie("foo", "bar"); // -> Vulnerable

    return HttpResponse.ok().cookie(Cookie.of("zzz", "ddd")); // -> Vulnerable
  }
}

@Controller("/hello")
public class HelloController {

    @Post("/test1")
    public MutableHttpMessage<Object> postTest1() {
        SimpleCookie s = new SimpleCookie("foo", "bar"); // -> Vulnerable
        return HttpResponse.ok().cookie(s);
    }

    @Post("/test2")
    public MutableHttpMessage<Object> postTest2() {
        Cookie secureCookie = new NettyCookie("foo", "bar").secure(true); // -> Safe
        return HttpResponse.ok().cookie(secureCookie);
    }

    @Post("/test3")
    public MutableHttpMessage<Object> postTest3() {
        Cookie c = Cookie.of("foo", "bar"); // -> Vulnerable
        return HttpResponse.ok().cookie(c);
    }

    @Post("/test4")
    public MutableHttpMessage<Object> postTest4() {
        Cookie c = Cookie.of("foo", "bar").secure(true); // -> Safe
        return HttpResponse.ok().cookie(c);
    }

    @Post("/test5")
    public MutableHttpMessage<Object> postTest5() {
        NettyCookie nc = new NettyCookie("test", "1234"); // -> Vulnerable
        return HttpResponse.ok().cookie(nc);
    }

    @Post("/test6")
    public MutableHttpMessage<Object> postTest6() {
        NettyCookie nc = new NettyCookie("test", "1234");
        nc.secure(true); // -> Safe
        return HttpResponse.ok().cookie(nc);
    }

    @Post("/test7")
    public MutableHttpMessage<Object> postTest7(HttpRequest<?> request) {
        Cookie cookie = request.getCookies()
                .findCookie("sessionId")
                .orElse(new SimpleCookie("sessionId", "abc123")); // -> Vulnerable

        return HttpResponse.ok().cookie(cookie);
    }

    @Post("/test8")
    public MutableHttpMessage<Object> postTest8(HttpRequest<?> request) {
        Cookie cookie = request.getCookies()
                .findCookie("sessionId")
                .orElse(new SimpleCookie("sessionId", "abc123").secure(true)); // -> Safe

        return HttpResponse.ok().cookie(cookie);
    }

    @Post("/test9")
    public MutableHttpMessage<Object> postTest9() {
        NettyCookie cookie = new NettyCookie("insecure", "noSecureFlag"); // -> Vulnerable
        return HttpResponse.ok().cookie(cookie);
    }

    @Post("/test10")
    public MutableHttpMessage<Object> postTest10() {
        Cookie c = Cookie.of("safe", "secureCookie").secure(true); // -> Safe
        return HttpResponse.ok().cookie(c);
    }
}
