import express, { Application } from 'express';
import session from 'express-session';

const app: Application = express();

// Vulnerable configurations

const vulnConfig: session.SessionOptions = {
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: false,
    maxAge: 24 * 60 * 60 * 1000
  }
};

// Default values of both cookie and secure are dangerous
// https://www.npmjs.com/package/express-session

const otherVulnConfig: session.SessionOptions = {
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 24 * 60 * 60 * 1000 // No `secure` or `httpOnly` set, which can be dangerous
  }
};

const anotherVulnConfig: session.SessionOptions = {
  resave: false,
  saveUninitialized: true // No cookie options set, could be dangerous
};

// Safe cookie configuration

const safeCookieConfig: session.SessionOptions = {
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,  // Prevents JavaScript access to the cookie
    secure: true,    // Ensures cookie is only sent over HTTPS
    maxAge: 24 * 60 * 60 * 1000 // 1 day in milliseconds
  }
};

// Vulnerable cases

app.use(session({ // Vulnerable because `secure` is false
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    secure: false,  // This allows cookies over non-HTTPS connections
    maxAge: 24 * 60 * 60 * 1000
  }
}));

app.use(session());

app.use(session(vulnConfig));
app.use(session(otherVulnConfig));
app.use(session(anotherVulnConfig));

// Safe cases

app.use(session(safeCookieConfig));

app.use(session({ // Safe because `httpOnly` and `secure` are true
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    secure: true,
    maxAge: 24 * 60 * 60 * 1000
  }
}));
