
resource "aws_security_group" "vulnerable1" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "someid"

  egress {
    from_port = 443
    to_port   = 443
    protocol  = "-1"
  }
}

resource "aws_security_group_rule" "vulnerable2" {
  security_group_id = "sg-123456"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = -1
}

resource "aws_security_group" "not_vulnerable1" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "someid"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
  }

  egress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "udp"
    cidr_blocks = ["127.0.0.1/32"]
  }
}

resource "aws_security_group_rule" "not_vulnerable2" {
  security_group_id = "sg-123456"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = "127.0.0.1/32"
}
