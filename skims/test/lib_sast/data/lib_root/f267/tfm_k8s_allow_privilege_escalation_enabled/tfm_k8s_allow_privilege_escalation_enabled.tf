resource "kubernetes_pod_v1" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    security_context {
      seccomp_profile {
        type = "localhost"
      }
    }
    container {
      name  = "unsafe_privileges1"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = true
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
    container {
      name  = "unsafe_privileges2"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root           = true
        read_only_root_filesystem = true
        capabilities {
          drop = ["all"]
        }
      }
    }
    container {
      name  = "safe_privileges"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
  }
}

resource "kubernetes_deployment_v1" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "web"
      }
    }
    template {
      metadata {
        labels = {
          app = "web"
        }
      }
      spec {
        automount_service_account_token = false
        security_context {
          run_as_non_root = false
          seccomp_profile {
            type = "Localhost"
          }
        }
        container {
          name  = "unsafe_root"
          image = "nginx"
          port {
            container_port = 80
          }
          security_context {
            run_as_non_root            = true
            allow_privilege_escalation = true
            read_only_root_filesystem  = true
            capabilities {
              drop = ["all"]
            }
          }
        }
      }
    }
  }
}
