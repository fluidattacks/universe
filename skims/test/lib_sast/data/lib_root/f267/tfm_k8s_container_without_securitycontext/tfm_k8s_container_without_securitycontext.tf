resource "kubernetes_pod" "example" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    container {
      name  = "front-end"
      image = "nginx"
      port {
        container_port = 80
      }
    }
    container {
      name  = "safe-context"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
        seccomp_profile {
          type = "Localhost"
        }
      }
    }
  }
}
