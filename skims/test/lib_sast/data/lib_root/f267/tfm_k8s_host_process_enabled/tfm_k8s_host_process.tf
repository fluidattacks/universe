resource "kubernetes_pod" "frontend" {
  metadata {
    name = "frontend"
    labels = {
      name = "podName"
    }
  }
  spec {
    automount_service_account_token = false
    security_context {
      windows_options {
        host_process = true
      }
    }
    container {
      name  = "container-with-cpu-limit"
      image = "images.my-company.example/app:v4"
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
      }
      resources {
        limits = {
          cpu    = "500m"
          memory = "128Mi"
        }
        requests = {
          cpu    = "250m"
          memory = "64Mi"
        }
      }
    }
  }
}
