<?php

/* ------------------------------- VULNERABLE CASES ------------------------------- */

// NONCOMPLIANT CASES #1:
// 'no-referrer-when-downgrade' allows the full URL to be sent unless navigating from HTTPS to HTTP.
header("Referrer-Policy: no-referrer-when-downgrade"); // -> Line must be marked
$insecureReferrerPolicy1 = "Referrer-Policy: no-referrer-when-downgrade";
header($insecureReferrerPolicy1); // -> Line must be marked

// NONCOMPLIANT CASES #2:
// 'origin' sends only the origin (scheme, host, port) in all requests, including potentially unsafe contexts.
header("Referrer-Policy: origin"); // -> Line must be marked
$insecureReferrerPolicy2 = "Referrer-Policy: origin";
header($insecureReferrerPolicy2); // -> Line must be marked

// NONCOMPLIANT CASES #3:
// 'origin-when-cross-origin' sends the full URL for same-origin requests and only the origin for cross-origin requests.
header("Referrer-Policy: origin-when-cross-origin"); // -> Line must be marked
$insecureReferrerPolicy3 = "Referrer-Policy:  origin-when-cross-origin";
header($insecureReferrerPolicy3); // -> Line must be marked

// NONCOMPLIANT CASES #4:
// 'unsafe-url' sends the full URL in the Referer header for all requests.
header("Referrer-Policy: unsafe-url"); // -> Line must be marked
$insecureReferrerPolicy4 = "Referrer-Policy:  unsafe-url";
header($insecureReferrerPolicy4); // -> Line must be marked

/* ------------------------------- SAFE CASES ------------------------------- */

// SECURE CASE #1:
// 'no-referrer' ensures the Referer header is never sent.
header("Referrer-Policy: no-referrer"); // -> Safe line
$safeReferrerPolicy1 = "Referrer-Policy: no-referrer";
header($safeReferrerPolicy1); // -> Safe line

// SECURE CASE #2:
// 'same-origin' only sends the Referer header for same-origin requests.
header("Referrer-Policy: same-origin"); // -> Safe line
$safeReferrerPolicy2 = "Referrer-Policy: same-origin";
header($safeReferrerPolicy2); // -> Safe line

// SECURE CASE #3:
// 'strict-origin' sends only the origin for cross-origin requests and omits the header for HTTPS to HTTP requests.
header("Referrer-Policy: strict-origin"); // -> Safe line
$safeReferrerPolicy3 = "Referrer-Policy: strict-origin";
header($safeReferrerPolicy3); // -> Safe line

// SECURE CASE #4:
// 'strict-origin-when-cross-origin' sends the full URL for same-origin requests and only the origin for cross-origin requests, omitting the header for HTTPS to HTTP requests.
header("Referrer-Policy: strict-origin-when-cross-origin"); // -> Safe line
$safeReferrerPolicy4 = "Referrer-Policy: strict-origin-when-cross-origin";
header($safeReferrerPolicy4); // -> Safe line

?>
