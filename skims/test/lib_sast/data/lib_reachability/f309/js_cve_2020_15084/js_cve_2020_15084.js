import jwt from 'express-jwt'
const jwksRsa = require('jwks-rsa');

const compliantJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://localhost/.well-known/jwks.json`
  }),
  // Validate the audience and the issuer.
  audience: "audience",
  issuer: `https://localhost/`,
  // restrict allowed algorithms
  algorithms: ['RS256']
});

const vulnkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://localhost/`
  }),
});
