using Avro.IO;

namespace Avro.Test
{
    using Decoder = Avro.IO.Decoder;
    public class BinaryCodecTests
    {
        public void TestInvalidInputWithMaxArrayLengthAsStringLength()
        {
            using (MemoryStream iostr = new MemoryStream())
            {
                Encoder e = new BinaryEncoder(iostr);

                #if NETCOREAPP3_1_OR_GREATER
                const int maximumArrayLength = 0x7FFFFFC7;
                #else
                const int maximumArrayLength = 0x7FFFFFFF / 2;
                #endif

                e.WriteLong(maximumArrayLength);
                e.WriteBytes(Encoding.UTF8.GetBytes("SomeSmallString"));

                iostr.Flush();
                iostr.Position = 0;
                Decoder d = new BinaryDecoder(iostr);

                var exception = Assert.Throws<AvroException>(() => d.ReadString());

                Assert.NotNull(exception);
                Assert.AreEqual("Could not read as many bytes from stream as expected!", exception.Message);
                iostr.Close();
            }
        }

    }
}
