using Newtonsoft.Json;
namespace Controllers
{
    public class Vuln
    {
        public static void Vuln(HttpRequest req, HttpResponse res)
        {
            var parsedJson = JObject.Parse(req.Params.Get("name"));
            using (var ms = new MemoryStream())
            using (var sWriter = new StreamWriter(ms))
            using (var jWriter = new JsonTextWriter(sWriter))
            {
                //Trying to serialize the object will result in StackOverflowException !!!
                parsedJson.WriteTo(jWriter);
            }

            //ToString throws StackOverflowException as well  (ToString is very unefficient - even for smaller payloads, it will occupy a lot of CPU & Memory)
            parsedJson.ToString();

            //JsonConvert.SerializeObject throws StackOverflowException as well
            string a = JsonConvert.SerializeObject(parsedJson);
        }
    }

    public class Safe
    {
        public override void Safe(HttpRequest req, HttpResponse resp)
        {
            string json = "internal value";
            var parsedJson = JObject.Parse(json);
            using (var ms = new MemoryStream())
            using (var sWriter = new StreamWriter(ms))
            using (var jWriter = new JsonTextWriter(sWriter))
            {
                //Trying to serialize the object will result in StackOverflowException !!!
                parsedJson.WriteTo(jWriter);
            }

            //ToString throws StackOverflowException as well  (ToString is very unefficient - even for smaller payloads, it will occupy a lot of CPU & Memory)
            parsedJson.ToString();

            //JsonConvert.SerializeObject throws StackOverflowException as well
            string a = JsonConvert.SerializeObject(parsedJson);
        }
    }

    public class Safe2
    {
        public static void Safe2(HttpRequest req, HttpResponse res)
        {
            var parsedJson = JObject.Parse(req.Params.Get("name"));
            var options = new JsonSerializerOptions{ MaxDepth = 64 };
            using (var ms = new MemoryStream())
            using (var sWriter = new StreamWriter(ms))
            using (var jWriter = new JsonTextWriter(sWriter))

            {
                //Trying to serialize the object will result in StackOverflowException !!!
                parsedJson.WriteTo(jWriter, options);
            }

            //ToString throws StackOverflowException as well  (ToString is very unefficient - even for smaller payloads, it will occupy a lot of CPU & Memory)
            parsedJson.ToString(options);

            //JsonConvert.SerializeObject throws StackOverflowException as well
            string a = JsonConvert.SerializeObject(parsedJson, options);
        }
    }
}
