package com.example.vulnerabletjws;

import Acme.Serve.Serve;

import java.util.HashMap;

public class VulnerableServer {
    public static void main(String[] args) {
        // Configure the server settings
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("port", 8080); // Server will listen on port 8080

        // Start the TJWS server
        Serve server = new Serve();
        server.arguments = properties;
        server.addDefaultServlets(null);
        server.serve();

        System.out.println("TJWS Server started on http://localhost:8080");

        // The server will serve files from the default directory or respond with a 404 page
        // If a client requests a nonexistent page, the server's vulnerable 404 page will display
    }
}
