import sanitizeHtml from 'sanitize-html';

module.exports = function vulnFunc () {
  return (req, res, next) => {
    res.redirect(sanitizeHtml(req.params.text, {
      allowedTags: [ 'textarea' ]
    }))
  }
}


module.exports = function safeFunc () {
  return (req, res, next) => {
    let text = "some internal text";
    res.redirect(sanitizeHtml(text, {
      allowedTags: [ 'textarea' ]
    }))
  }
}
