const utils = require("axios/lib/utils");
const { trim } = require("axios/lib/utils");

module.exports = function vulnFunc() {
  return (req, res, next) => {
    let body = req.params.body;
    let vulnerableLine = trim(body);
    res.redirect(utils.trim(body));
  };
};

module.exports = function safeFunc() {
  return (req, res, next) => {
    let body = "https://example.com";
    let safeLine = trim(body);
    res.redirect(utils.trim(body));
  };
};
