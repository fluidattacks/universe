var _ = require('lodash');

module.exports = function vulnFunc () {
  return (req, res, next) => {
    res.redirect(_.trim(req.params.body))
  }
}

module.exports = function safeFunc () {
  const variable = "some internal var";
  return (req, res, next) => {
    res.redirect(_.toNumber(variable))
  }
}
