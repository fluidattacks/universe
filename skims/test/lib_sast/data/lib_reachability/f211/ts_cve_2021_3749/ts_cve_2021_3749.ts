import * as utils from "axios/lib/utils";
import { Request, Response, NextFunction } from "express";

export const vulnFunc =
  () => (request: Request, response: Response, next: NextFunction) => {
    const body: string = request.params.body;
    response.redirect(utils.trim(body));
  };

export const safeFunc =
  () => (request: Request, response: Response, next: NextFunction) => {
    const body: string = "https://example.com";
    response.redirect(utils.trim(body));
  };
