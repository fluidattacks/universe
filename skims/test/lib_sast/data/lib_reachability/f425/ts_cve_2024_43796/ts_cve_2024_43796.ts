import express, { Request, Response, NextFunction } from "express";

const app = express();

const vulnFunc = (req: Request, res: Response, next: NextFunction) => {
  if (req.headers["x-forwarded-proto"] !== "https") {
    res.header("Strict-Transport-Security", "max-age=31536000");
    const url = req.url;
    return res.redirect(["https://", req.get("Host"), url].join(""));
  }
  return next();
};

const safeFunc = (req: Request, res: Response, next: NextFunction) => {
  if (req.headers["x-forwarded-proto"] === "https") {
    return next();
  }
  return res.redirect("/");
};

app.set("port", process.env.PORT || 5000);

app.use(vulnFunc);
app.get("/", safeFunc);
