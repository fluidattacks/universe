const express = require("express");
const app = express();

const vulnFunc = (request, response, next) => {
  if (request.headers["x-forwarded-proto"] !== "https") {
    response.header("Strict-Transport-Security", "max-age=31536000");
    return response.redirect(["https://", request.get("Host"), request.url].join(""));
  }
  return next();
};

const safeFunc = (request, response) => {
  if (request.headers["x-forwarded-proto"] === "https") {
    response.header("Strict-Transport-Security", "max-age=31536000");
    return next();
  }

  response.redirect("/");
};

app.set("port", process.env.PORT || 5000);

app.use(vulnFunc);
