import { type NextFunction, type Request, type Response } from 'express'
const {VM} = require("vm2");
const vm = new VM();


function vulnFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = content.code;
  console.log(vm.run(code));
}


function safeFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = "some internal code";
  console.log(vm.run(code));
}
