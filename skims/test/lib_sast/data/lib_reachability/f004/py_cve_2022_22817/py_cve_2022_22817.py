from flask import request
from PIL import ImageMath

def vuln_eval_expr() -> None:
    expr = request.args.get("expr")
    result = ImageMath.eval(expr)

def safe_eval_expr() -> None:
    expr = "a + b"
    result = ImageMath.eval(expr, a=10, b=5)
