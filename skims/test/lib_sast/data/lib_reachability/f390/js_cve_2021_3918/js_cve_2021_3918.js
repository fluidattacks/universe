var jsonschema = require('json-schema');

module.exports = function vulnFunc () {
  return (req, res, next) => {
    res.redirect(jsonschema.validate(req.body.data, req.body.schema))
  }
}


module.exports = function safeFunc () {
  const data = "some internal var";
  const schema = "some internal var";
  return (req, res, next) => {
    res.redirect(jsonschema.validate(data, schema))
  }
}

module.exports = function sanitizedFunc () {
  return (req, res, next) => {
    const schema = "some internal var";
    const fileExtension = sanitize(req.params.body);
    if (fileExtension) {
      res.redirect(jsonschema.validate(req.params.body, schema))
    }
  }
}
