const safeEval = require('notevil')

function vulnFunc (req, res) {
  const code = req.params.code;
  console.log(safeEval(code));
}


function safeFunc (req, res) {
  const code = "some internal code";
  console.log(safeEval(code));
}
