import { type NextFunction, type Request, type Response } from 'express'
const safeEval = require('notevil')


function vulnFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = content.code;
  console.log(safeEval(code));
}


function safeFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = "some internal code";
  console.log(safeEval(code));
}
