import { type NextFunction, type Request, type Response } from 'express'
const nanoid = require('nanoid')


function vulnFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = content.code;
  console.log(nanoid.nanoid(code));
}


function safeFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = "some internal code";
  console.log(nanoid.nanoid(code));
}
