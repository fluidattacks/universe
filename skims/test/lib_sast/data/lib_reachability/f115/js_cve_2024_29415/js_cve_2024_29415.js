
const ip = require('ip');

export default class RemoteAddressStrategy extends Strategy {
  constructor() {
    super('remoteAddress');
  }
  isEnabled(req, parameters, context) {
    return parameters.IPs.split(/\s*,\s*/).some((range) => {
      if (!ip.isPublic(req.params.range)) {
        try {
          return ip.cidrSubnet(range).contains(context.remoteAddress);
        } catch (err) {
          return false;
        }
      }
    })
  }
}
