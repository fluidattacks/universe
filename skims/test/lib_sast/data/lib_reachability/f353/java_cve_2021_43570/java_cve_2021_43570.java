import com.starkbank.ellipticcurve.PrivateKey;
import com.starkbank.ellipticcurve.PublicKey;
import com.starkbank.ellipticcurve.Signature;
import com.starkbank.ellipticcurve.Ecdsa;


public class GenerateKeys{

    public void runUnsafe(HttpServletRequest request) {
      String input = request.getParameter("input");
      PrivateKey privateKey = new PrivateKey();
      PublicKey publicKey = privateKey.publicKey();

      // Generate Signature
      Signature signature = Ecdsa.sign(input, privateKey);

      // Verify if signature is valid
      boolean verified = Ecdsa.verify(input, signature, publicKey) ; // Vuln

      // Return the signature verification status
      System.out.println("Verified: " + verified);


    }

    public static void main(String[] args){
        // Generate Keys
        PrivateKey privateKey = new PrivateKey();
        PublicKey publicKey = privateKey.publicKey();

        String message = "Testing message";
        // Generate Signature
        Signature signature = Ecdsa.sign(message, privateKey);

        // Verify if signature is valid
        boolean verified = Ecdsa.verify(message, signature, publicKey) ; // Safe

        // Return the signature verification status
        System.out.println("Verified: " + verified);

    }
}
