# noqa: INP001
import pytest
import yaml
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.advisories_community import (
    RE_RANGES,
    URL_GITLAB_ADVISORY_DATABASE,
    fix_operators,
    fix_pip_composer_range,
    format_platform_ranges,
    format_ranges,
    get_platform_advisories,
)
from lib_sca.schedulers.repositories.common import (
    resolve_source,
)
from pytest_mock import (
    MockerFixture,
)

ADVS_COMM_MOD_STR = "lib_sca.schedulers.repositories.advisories_community"


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("ranges", "expected"),
    [
        ("[5.0.0,6.0.0)[6.0.0,6.0.4]", ["[5.0.0,6.0.0)", "[6.0.0,6.0.4]"]),
        ("(,0.3.13]", ["(,0.3.13]"]),
        ("[1.1.4]", ["[1.1.4]"]),
        ("12345]", []),
    ],
)
def test_re_ranges_pattern(ranges: str, expected: list[str]) -> None:
    match_ranges = RE_RANGES.findall(ranges)
    assert match_ranges == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("range_str", "expected"),
    [
        ("[5.0.0,6.0.0)", ">=5.0.0 <6.0.0"),
        ("(,0.3.13]", ">=0 <=0.3.13"),
        ("[1.1.4]", "=1.1.4"),
    ],
)
def test_advs_format_range(range_str: str, expected: str) -> None:
    formated_range = fix_operators(range_str)
    assert formated_range == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("range_str", "expected"),
    [
        (">=4.0,<4.3||>=5.0,<5.2", ">=4.0 <4.3||>=5.0 <5.2"),
        ("==3.1||>=4.0.0,<=4.0.2", "=3.1||>=4.0.0 <=4.0.2"),
        (">=1.0,<=1.0.1", ">=1.0 <=1.0.1"),
    ],
)
def test_fix_pip_composer_range(range_str: str, expected: str) -> None:
    formated_range = fix_pip_composer_range(range_str)
    assert formated_range == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("platform", "range_str", "expected"),
    [
        ("maven", "[1.1.4]", "=1.1.4"),
        ("pypi", ">=1.0,<=1.0.1", ">=1.0 <=1.0.1"),
        (
            "npm",
            "<5.2.4.4||>=6.0.0.0 <6.0.3.3",
            "<5.2.4.4||>=6.0.0.0 <6.0.3.3",
        ),
    ],
)
def test_format_platform_ranges_internal(platform: str, range_str: str, expected: str) -> None:
    formated_range = format_platform_ranges(platform, range_str)
    assert formated_range == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("platform", "range_str", "expected"),
    [
        ("pypi", "<1.0||>=2.4.1 <3.2.1", ">=0 <1.0 || >=2.4.1 <3.2.1"),
        (
            "gem",
            "<5.2.4.4||>=6.0.0.0 <6.0.3.3",
            ">=0 <5.2.4.4 || >=6.0.0.0 <6.0.3.3",
        ),
        ("nuget", "[12.3.5]", "=12.3.5"),
    ],
)
def test_format_platform_ranges(platform: str, range_str: str, expected: str) -> None:
    formated_range = format_ranges(platform, range_str, "", "")
    assert formated_range == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_platform_advisories() -> None:
    tmp_dirname = "skims/test/lib_sca/data/scheduler_mocks/test_sca_adv"
    platform = "npm"
    affected_range = ">=1.0.0 <=1.6.3"
    filename = "tmp/tmp3x0835a8/npm/cve-test-example.yml"
    expected: list[Advisory] = [
        Advisory(
            id="CVE-2021-25943",
            package_manager=platform,
            package_name="101",
            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
            severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/UI:N/AT:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
            source=resolve_source(URL_GITLAB_ADVISORY_DATABASE, filename, platform),
            vulnerable_version=affected_range,
            cwe_ids=["CWE-1035", "CWE-937"],
            created_at="2021-05-14",
            modified_at="2021-05-24",
            details="Object Prototype Pollution\n",
        ),
    ]
    advisories = get_platform_advisories(set(), tmp_dirname, platform)
    assert advisories == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_platform_advisories_error(mocker: MockerFixture) -> None:
    tmp_dirname: str = "skims/test/lib_sca/data/scheduler_mocks/test_sca_adv"
    platform: str = "npm"
    glob_mock = mocker.patch(
        f"{ADVS_COMM_MOD_STR}.yaml.safe_load",
        side_effect=yaml.YAMLError(),
    )
    print_mock = mocker.patch(f"{ADVS_COMM_MOD_STR}.log_blocking")
    get_platform_advisories(set(), tmp_dirname, platform)
    assert glob_mock.call_count == 1
    assert isinstance(print_mock.call_args.args[1], str)
