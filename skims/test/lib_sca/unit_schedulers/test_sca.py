# noqa: INP001

from importlib import (
    import_module,
)
from types import (
    ModuleType,  # noqa: TC003
)

import pytest
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.common import (
    _check_vulnerable_version,
    coerce_range,
    get_clean_severity,
    get_clean_severity_v4,
    get_clean_vulnerable_version,
)
from lib_sca.schedulers.refresh_advisories import (
    create_advisories_dict,
    get_clean_advisories,
    get_raw_advisories,
    patch_advisories,
    update_sca,
)
from pytest_mock import (
    MockerFixture,
)

UPD_SCA_TABLE_STR = "lib_sca.schedulers.refresh_advisories"


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("version_range", "expected"),
    [
        ("=1.2.3", "=1.2.3-"),
        (">1.2.3", ">1.2.3-"),
        ("<=1.2.3", ">=0.0.0- <=1.2.3-"),
        (">=1.2.3 <2.0.0", ">=1.2.3- <2.0.0-"),
        ("<2.0.0-beta", ">=0.0.0- <2.0.0-beta"),
        ("v1.2.3", "1.2.3-"),
        ("=v1.2.3-alpha", "=1.2.3-alpha"),
        (">=1.2", ">=1.2.0-"),
        ("=1.2.03", "=1.2.3-"),
        ("<1.2.3+build123", ">=0.0.0- <1.2.3+build123"),
        ("<=1.2.3-rc.1", ">=0.0.0- <=1.2.3-rc.1"),
        (">=1.0.0 <1.5.0 || >=2.0.0 <2.5.0", ">=1.0.0- <1.5.0-||>=2.0.0- <2.5.0-"),
        ("~1.2", "~1.2.*-"),
    ],
)
def test_coerce_range(*, version_range: str, expected: str) -> None:
    result = coerce_range(version_range)
    assert result == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("versions", "expected"),
    [
        ("=1.0.0", True),
        (">=0 <1.2.14", True),
        (">=2.3.0-rc1 <2.7.7", True),
        (">=0 <2.7.9.3 || >=2.8.0 <2.8.11.1 || >=2.9.0 <2.9.5", True),
        ("<=1.0.0 || >=2.0.0", True),
        ("<=1.0.0we", True),
        ("=1.2.3-alpha.1+build.20230524", True),
        ("=2.0.0 || >=1.2wer.4 <1.3.5", False),
        ("invalid || =2.0.0", False),
        ("", False),
    ],
)
def test_check_versions(*, versions: str, expected: bool) -> None:
    result = _check_vulnerable_version(versions, "npm")
    assert result == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("severity", "expected_result"),
    [
        (
            "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N",
            "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N",
        ),
        ("CVSS:2.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N", None),
        ("CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N", None),
        ("CVSS:3.1/AV:N/AC:L/BAD:N/UI:N/S:C/C:L/I:N/A:N", None),
    ],
)
def test_check_severity(*, severity: str, expected_result: str | None) -> None:
    result = get_clean_severity(severity)
    assert result == expected_result


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("severity", "expected_result"),
    [
        (
            "CVSS:4.0/AV:N/AC:L/AT:P/PR:N/UI:P/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U",
            "CVSS:4.0/AV:N/AC:L/AT:P/PR:N/UI:P/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U",
        ),
        ("CVSS:2.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N", None),
        (
            "CVSS:4.0/AV:N/AC:L/AT:P/S:U/PR:N/UI:P/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U",
            None,
        ),
    ],
)
def test_check_severity_v4(*, severity: str, expected_result: str | None) -> None:
    result = get_clean_severity_v4(severity)
    assert result == expected_result


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("advisory", "expected"),
    [
        (
            Advisory(
                id="CVE-ADVISORY-1",
                package_manager="pypi",
                package_name="package_name_1",
                severity="CVSS:2.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                source="https://github.com/github/advisory-database.git",
                vulnerable_version=">=0 || >=2.4.0 <2.12.2",
                created_at="test_date",
                modified_at="test_date",
            ),
            ">=2.4.0 <2.12.2",
        ),
        (
            Advisory(
                id="CVE-ADVISORY-1",
                package_manager="gem",
                package_name="package_name_1",
                severity="CVSS:2.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                source="https://github.com/github/advisory-database.git",
                vulnerable_version=">v2.3.0-rc1 <2.7.7",
                created_at="test_date",
                modified_at="test_date",
            ),
            ">2.3.0-rc1 <2.7.7",
        ),
        (
            Advisory(
                id="CVE-ADVISORY-1",
                package_manager="npm",
                package_name="package_name_1",
                severity="CVSS:2.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                source="https://github.com/github/advisory-database.git",
                vulnerable_version="=2.0.0 || >=1.2wer.4 <1.3.5",
                created_at="test_date",
                modified_at="test_date",
            ),
            None,
        ),
    ],
)
def test_get_clean_vulnerable_version(*, advisory: Advisory, expected: str | None) -> None:
    result = get_clean_vulnerable_version(advisory)
    assert result == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_clean_advisory() -> None:
    raw_advisories = [
        Advisory(
            id="CVE-ADVISORY-1",
            package_manager="gem",
            package_name="package_name_1",
            severity="CVSS:2.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            source="https://github.com/github/advisory-database.git",
            vulnerable_version=">=0 || >=2.4.0 <2.12.2",
            created_at="test_date",
            modified_at="test_date",
        ),
        Advisory(
            id="CVE-ADVISORY-2",
            package_manager="go",
            package_name="package_name_2",
            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
            source="MANUAL",
            vulnerable_version=">=0 <2.4.0 || >2.5.4 <=2.6.3",
            created_at="test_date",
            modified_at="test_date",
        ),
    ]
    clean_advisories = get_clean_advisories(raw_advisories)
    assert clean_advisories == [
        Advisory(
            id="CVE-ADVISORY-1",
            package_manager="gem",
            package_name="package_name_1",
            severity=None,
            source="https://github.com/github/advisory-database.git",
            vulnerable_version=">=2.4.0 <2.12.2",
            created_at="test_date",
            modified_at="test_date",
        ),
        Advisory(
            id="CVE-ADVISORY-2",
            package_manager="go",
            package_name="package_name_2",
            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
            source="MANUAL",
            vulnerable_version=">=0 <2.4.0 || >2.5.4 <=2.6.3",
            created_at="test_date",
            modified_at="test_date",
        ),
    ]


@pytest.mark.skims_test_group("sca_unittesting")
def test_create_advisories_dict() -> None:
    clean_advisories = [
        Advisory(
            id="CVE-ADVISORY-1",
            package_manager="gem",
            package_name="package_name_1",
            severity=None,
            source="https://github.com/github/advisory-database.git",
            vulnerable_version=">=2.4.0 <2.12.2",
            created_at="test_date",
            modified_at="test_date",
        ),
        Advisory(
            id="CVE-ADVISORY-2",
            package_manager="go",
            package_name="package_name_2",
            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
            source="https://gitlab.com/gitlab/advisory-database.git",
            vulnerable_version=">=0 <2.4.0 || >2.5.4 <=2.6.3",
            created_at="test_date",
            modified_at="test_date",
        ),
        Advisory(
            id="GEMSEC-123",
            package_manager="gem",
            package_name="package_name_1",
            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
            source="https://gitlab.com/gitlab/advisory-database.git",
            vulnerable_version=">=0",
            alternative_id="CVE-ADVISORY-1",
            created_at="test_date",
            modified_at="test_date",
        ),
    ]
    advisories_dict = create_advisories_dict(clean_advisories)
    assert advisories_dict == {
        "gem": {
            "package_name_1": {
                "CVE-ADVISORY-1": Advisory(
                    id="CVE-ADVISORY-1",
                    package_name="package_name_1",
                    package_manager="gem",
                    vulnerable_version=">=2.4.0 <2.12.2",
                    source="https://github.com/github/advisory-database.git",
                    cwe_ids=None,
                    created_at="test_date",
                    modified_at="test_date",
                ),
            },
        },
        "go": {
            "package_name_2": {
                "CVE-ADVISORY-2": Advisory(
                    id="CVE-ADVISORY-2",
                    package_name="package_name_2",
                    package_manager="go",
                    vulnerable_version=">=0 <2.4.0 || >2.5.4 <=2.6.3",
                    source="https://gitlab.com/gitlab/advisory-database.git",
                    cwe_ids=None,
                    created_at="test_date",
                    modified_at="test_date",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                ),
            },
        },
    }


@pytest.mark.skims_test_group("sca_unittesting")
def test_patch_advisories() -> None:
    advisories = {
        "npm": {
            "test_pkg_1": {
                "CVE-2024-31": Advisory(
                    id="CVE-2024-31",
                    package_name="test_pkg_1",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    package_manager="npm",
                    vulnerable_version="=2.0.0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22", "CWE-345"],
                    alternative_id="GHSA-4gj",
                ),
                "CVE-2023": Advisory(
                    id="CVE-2023",
                    package_name="test_pkg_1",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    package_manager="npm",
                    vulnerable_version="<1.0.0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22", "CWE-345"],
                    alternative_id="GHSA-6tgv",
                ),
            },
        },
    }
    adv_to_patch = {
        "NEW": [
            Advisory(
                id="NEW_ADV_1",
                package_name="test_pkg_1",
                severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                package_manager="npm",
                vulnerable_version=">1.0.0",
                source="MANUAL",
                created_at="test_date",
                modified_at="test_date",
                cwe_ids=["CWE-22", "CWE-345"],
            ),
        ],
        "PATCH": [
            Advisory(
                id="CVE-2023",
                package_name="test_pkg_1",
                package_manager="npm",
                vulnerable_version="<0",
                source="MANUAL",
            ),
            Advisory(
                id="GHSA-4gj",
                package_name="test_pkg_1",
                package_manager="npm",
                vulnerable_version=">2.0.0",
                source="MANUAL",
            ),
        ],
    }
    patch_advisories(advisories, adv_to_patch)
    assert advisories == {
        "npm": {
            "test_pkg_1": {
                "CVE-2024-31": Advisory(
                    id="CVE-2024-31",
                    package_name="test_pkg_1",
                    package_manager="npm",
                    vulnerable_version=">2.0.0",
                    source="MANUAL",
                    cwe_ids=["CWE-22", "CWE-345"],
                    created_at="test_date",
                    modified_at="test_date",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    alternative_id="GHSA-4gj",
                ),
                "CVE-2023": Advisory(
                    id="CVE-2023",
                    package_name="test_pkg_1",
                    package_manager="npm",
                    vulnerable_version="<0",
                    source="MANUAL",
                    cwe_ids=["CWE-22", "CWE-345"],
                    created_at="test_date",
                    modified_at="test_date",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    alternative_id="GHSA-6tgv",
                ),
                "NEW_ADV_1": Advisory(
                    id="NEW_ADV_1",
                    package_name="test_pkg_1",
                    package_manager="npm",
                    vulnerable_version=">1.0.0",
                    source="MANUAL",
                    cwe_ids=["CWE-22", "CWE-345"],
                    created_at="test_date",
                    modified_at="test_date",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                ),
            },
        },
    }


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("sources_advs", "expected"),
    [
        (
            [
                Advisory(
                    id="CVE-002",
                    package_name="test_pkg_2",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                    package_manager="npm",
                    vulnerable_version=">=1.0.0 <=2.0.0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22", "CWE-345"],
                ),
            ],
            {
                "npm": {
                    "test_pkg_2": {
                        "CVE-002": Advisory(
                            id="CVE-002",
                            package_name="test_pkg_2",
                            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                            severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/"
                            "VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                            package_manager="npm",
                            vulnerable_version=">=1.0.0 <=2.0.0",
                            source="test_source",
                            created_at="test_date",
                            modified_at="test_date",
                            cwe_ids=["CWE-22", "CWE-345"],
                        ),
                    },
                },
            },
        ),
        (
            [
                Advisory(
                    id="GHSA-4gj",
                    package_name="test_pkg_2",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                    package_manager="npm",
                    vulnerable_version="=2.0.0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22", "CWE-345"],
                    alternative_id="CVE-002",
                ),
            ],
            {
                "npm": {
                    "test_pkg_2": {
                        "GHSA-4gj": Advisory(
                            id="GHSA-4gj",
                            package_name="test_pkg_2",
                            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                            severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/"
                            "VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                            package_manager="npm",
                            vulnerable_version="=2.0.0",
                            source="test_source",
                            created_at="test_date",
                            modified_at="test_date",
                            cwe_ids=["CWE-22", "CWE-345"],
                            alternative_id="CVE-002",
                        ),
                    },
                },
            },
        ),
    ],
)
async def test_add(sources_advs: list[Advisory], expected: dict) -> None:
    s3_advisories = create_advisories_dict(sources_advs)
    assert s3_advisories == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_raw_advisories(mocker: MockerFixture) -> None:
    advisories = [
        Advisory(
            id="CVE-ADVISORY-1",
            package_manager="gem",
            package_name="package_name_1",
            severity=None,
            source="https://github.com/github/advisory-database.git",
            vulnerable_version=">=2.4.0 <2.12.2",
            cwe_ids=["CWE-20"],
            created_at="test_date",
            modified_at="test_date",
        ),
    ]
    mocker.patch(f"{UPD_SCA_TABLE_STR}.Repo.clone_from")
    mocker.patch(
        f"{UPD_SCA_TABLE_STR}.get_osv_advisories",
        return_value=[
            Advisory(
                id="PYSEC-2014",
                package_manager="pypi",
                package_name="package_name_2",
                severity=None,
                source="OSV",
                vulnerable_version="<1.0.0",
                created_at="test_date",
                modified_at="test_date",
            ),
        ],
    )
    module_test: ModuleType = import_module(UPD_SCA_TABLE_STR)
    get_repo_mock = mocker.Mock(return_value=advisories)
    mocker.patch.object(
        module_test,
        "ADVISORIES_GETTERS",
        {"url": get_repo_mock},
    )
    raw_advisories = get_raw_advisories()
    assert raw_advisories == [
        Advisory(
            id="CVE-ADVISORY-1",
            package_name="package_name_1",
            package_manager="gem",
            vulnerable_version=">=2.4.0 <2.12.2",
            source="https://github.com/github/advisory-database.git",
            cwe_ids=["CWE-20"],
            created_at="test_date",
            modified_at="test_date",
        ),
        Advisory(
            id="PYSEC-2014",
            package_name="package_name_2",
            package_manager="pypi",
            vulnerable_version="<1.0.0",
            source="OSV",
            created_at="test_date",
            modified_at="test_date",
        ),
    ]


@pytest.mark.skims_test_group("sca_unittesting")
def test_update_sca(mocker: MockerFixture) -> None:
    mocker.patch(f"{UPD_SCA_TABLE_STR}.get_raw_advisories")
    mocker.patch(f"{UPD_SCA_TABLE_STR}.get_clean_advisories")
    mocker.patch(f"{UPD_SCA_TABLE_STR}.create_advisory_db")
    mocker.patch(f"{UPD_SCA_TABLE_STR}.load_cve_findings")

    model_add_mock = mocker.patch(f"{UPD_SCA_TABLE_STR}.create_advisories_dict")
    update_sca()
    assert model_add_mock.call_count == 1
