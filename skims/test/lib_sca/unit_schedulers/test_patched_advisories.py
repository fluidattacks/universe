# noqa: INP001


import pytest
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.patched_advisories import (
    get_patched_advisories,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_patched_advisories() -> None:
    expected_advisories: dict[str, list[Advisory]] = {
        "PATCH": [
            Advisory(
                id="CVE-2021-43617",
                package_name="laravel/framework",
                package_manager="composer",
                vulnerable_version="<0",
                source="MANUAL",
            ),
        ],
        "NEW": [
            Advisory(
                id="GHSA-p8fm-w787-x6x3",
                package_name="coa",
                package_manager="npm",
                vulnerable_version=">2.0.2",
                source="MANUAL",
                cwe_ids=["CWE-79"],
                severity="CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L",
                details="Vulnerable to XSS",
            ),
        ],
    }

    advisories = get_patched_advisories(
        "skims/test/lib_sca/data/scheduler_mocks/patched_advisories/**/*.json",
    )

    assert advisories == expected_advisories
