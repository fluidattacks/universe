# noqa: INP001
import pytest
from lib_sca.sca_new.version.constraint_unit import (
    ConstraintUnit,
)
from lib_sca.sca_new.version.deb_constraint import (
    new_deb_comparator,
    new_deb_constraint,
)
from lib_sca.sca_new.version.deb_version import (
    DebVersionComparator,
)
from lib_sca.sca_new.version.deb_version.version import (
    new_deb_version,
)
from lib_sca.sca_new.version.version_operator import (
    Operator,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("version_str", "constraint", "satisfied"),
    [
        ("2.3.1", "", True),
        ("2.3.1", "> 1.0.0, < 2.0.0", False),
        ("1.3.1", "> 1.0.0, < 2.0.0", True),
        ("2.0.0", "> 1.0.0, <= 2.0.0", True),
        ("2.0.0", "> 1.0.0, < 2.0.0", False),
        ("1.0.0", ">= 1.0.0, < 2.0.0", True),
        ("1.0.0", "> 1.0.0, < 2.0.0", False),
        ("0.9.0", "> 1.0.0, < 2.0.0", False),
        ("1.5.0", "> 0.1.0, < 0.5.0 || > 1.0.0, < 2.0.0", True),
        ("0.2.0", "> 0.1.0, < 0.5.0 || > 1.0.0, < 2.0.0", True),
        ("0.0.1", "> 0.1.0, < 0.5.0 || > 1.0.0, < 2.0.0", False),
        ("0.6.0", "> 0.1.0, < 0.5.0 || > 1.0.0, < 2.0.0", False),
        ("2.5.0", "> 0.1.0, < 0.5.0 || > 1.0.0, < 2.0.0", False),
        ("2.3.1", "< 2.0.0", False),
        ("2.3.1", "< 2.0", False),
        ("2.3.1", "< 2", False),
        ("2.3.1", "< 2.3", False),
        ("2.3.1", "< 2.3.1", False),
        ("2.3.1", "< 2.3.2", True),
        ("2.3.1", "< 2.4", True),
        ("2.3.1", "< 3", True),
        ("2.3.1", "< 3.0", True),
        ("2.3.1", "< 3.0.0", True),
        ("2.3.1-1ubuntu0.14.04.1", " <2.0.0", False),
        ("2.3.1-1ubuntu0.14.04.1", " <2.0", False),
        ("2.3.1-1ubuntu0.14.04.1", " <2", False),
        ("2.3.1-1ubuntu0.14.04.1", " <2.3", False),
        ("2.3.1-1ubuntu0.14.04.1", " <2.3.1", False),
        ("2.3.1-1ubuntu0.14.04.1", " <2.3.2", True),
        ("2.3.1-1ubuntu0.14.04.1", " <2.4", True),
        ("2.3.1-1ubuntu0.14.04.1", " <3", True),
        ("2.3.1-1ubuntu0.14.04.1", " <3.0", True),
        ("2.3.1-1ubuntu0.14.04.1", " <3.0.0", True),
        (
            "7u151-2.6.11-2ubuntu0.14.04.1",
            " < 7u151-2.6.11-2ubuntu0.14.04.1",
            False,
        ),
        ("7u151-2.6.11-2ubuntu0.14.04.1", " < 7u151-2.6.11", False),
        ("7u151-2.6.11-2ubuntu0.14.04.1", " < 7u151-2.7", False),
        ("7u151-2.6.11-2ubuntu0.14.04.1", " < 7u151", False),
        ("7u151-2.6.11-2ubuntu0.14.04.1", " < 7u150", False),
        ("7u151-2.6.11-2ubuntu0.14.04.1", " < 7u152", True),
        (
            "7u151-2.6.11-2ubuntu0.14.04.1",
            " < 7u152-2.6.11-2ubuntu0.14.04.1",
            True,
        ),
        (
            "7u151-2.6.11-2ubuntu0.14.04.1",
            " < 8u1-2.6.11-2ubuntu0.14.04.1",
            True,
        ),
        ("43.0.2357.81-0ubuntu0.14.04.1.1089", "<43", False),
        ("43.0.2357.81-0ubuntu0.14.04.1.1089", "<43.0", False),
        ("43.0.2357.81-0ubuntu0.14.04.1.1089", "<43.0.2357", False),
        ("43.0.2357.81-0ubuntu0.14.04.1.1089", "<43.0.2357.81", False),
        (
            "43.0.2357.81-0ubuntu0.14.04.1.1089",
            "<43.0.2357.81-0ubuntu0.14.04.1.1089",
            False,
        ),
        (
            "43.0.2357.81-0ubuntu0.14.04.1.1089",
            "<43.0.2357.82-0ubuntu0.14.04.1.1089",
            True,
        ),
        (
            "43.0.2357.81-0ubuntu0.14.04.1.1089",
            "<43.0.2358-0ubuntu0.14.04.1.1089",
            True,
        ),
        (
            "43.0.2357.81-0ubuntu0.14.04.1.1089",
            "<43.1-0ubuntu0.14.04.1.1089",
            True,
        ),
        (
            "43.0.2357.81-0ubuntu0.14.04.1.1089",
            "<44-0ubuntu0.14.04.1.1089",
            True,
        ),
    ],
)
def test_new_deb_constraint(*, version_str: str, constraint: str, satisfied: bool) -> None:
    version = new_deb_version(version_str)
    result = new_deb_constraint(constraint)
    assert result is not None
    assert result.satisfied(version) == satisfied


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_deb_constraint_non_version() -> None:
    version = None
    result = new_deb_constraint("> 1.0.0, < 2.0.0")
    assert result is not None
    assert not result.satisfied(version)
    assert str(result) == "> 1.0.0, < 2.0.0 (deb)"


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_deb_constraint_non_version_no_constraint() -> None:
    version = None
    result = new_deb_constraint("")
    assert result is not None
    assert result.satisfied(version)
    assert str(result) == "node (deb)"


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_deb_constraint_not_expression() -> None:
    version = new_deb_version("2.3.1")
    result = new_deb_constraint("> 1.0.0, < 2.0.0")
    assert result is not None
    result.expression = None
    assert not result.satisfied(version)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("version_str", "expected_result"),
    [
        ("1.2.3", True),
        ("1.2.3-4", True),
        ("1.2.3-4ubuntu5", True),
        ("1:1.2.3-4", True),
        ("1:1.2.3-4ubuntu5", True),
        ("invalid_version", False),
        ("", False),
        ("1.2.3.4", True),
        ("1.2.3.4-5", True),
        ("1.2.3.4-5ubuntu6", True),
        ("1:1.2.3.4-5", True),
        ("1:1.2.3.4-5ubuntu6", True),
    ],
)
def test_new_deb_comparator(*, version_str: str, expected_result: bool) -> None:
    unit = ConstraintUnit(Operator.EQ, version=version_str)

    if expected_result:
        result = new_deb_comparator(unit)
        assert result is not None
        assert isinstance(result, DebVersionComparator)
    else:
        with pytest.raises(ValueError, match=f"unable to parse version: {version_str}"):
            new_deb_comparator(unit)
