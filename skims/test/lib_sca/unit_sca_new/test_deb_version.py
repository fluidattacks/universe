# noqa: INP001
import pytest
from lib_sca.sca_new.version import (
    Version,
)
from lib_sca.sca_new.version.deb_version.version import (
    DebVersion as DebianVersion,
)
from lib_sca.sca_new.version.deb_version.version import (
    DefaultNumList,
    DefaultStringList,
    compare,
    compare_string,
    extract,
    new_deb_version,
    valid,
    verify_debian_revision,
    verify_upstream_version,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_upstream_version_valid() -> None:
    try:
        verify_upstream_version("1.2.3")
        verify_upstream_version("1.0-alpha+beta")
        verify_upstream_version("2.0~rc1")
        verify_upstream_version("3.1.4:15")
    except ValueError as exc:
        pytest.fail(f"verify_upstream_version raises an exception: {exc}")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_upstream_version_empty() -> None:
    with pytest.raises(ValueError, match="upstream_version is empty"):
        verify_upstream_version("")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_upstream_version_non_digit_start() -> None:
    with pytest.raises(ValueError, match="upstream_version must start with digit"):
        verify_upstream_version("a1.2.3")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_upstream_version_invalid_characters() -> None:
    with pytest.raises(ValueError, match="upstream_version includes invalid character"):
        verify_upstream_version("1.2.3$")
    with pytest.raises(ValueError, match="upstream_version includes invalid character"):
        verify_upstream_version("1.2.3/4")
    with pytest.raises(ValueError, match="upstream_version includes invalid character"):
        verify_upstream_version("1.2.3 4")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_upstream_version_edge_cases() -> None:
    try:
        verify_upstream_version("0")
        verify_upstream_version("9999999999")
        verify_upstream_version("1.0+")
        verify_upstream_version("1.0-")
        verify_upstream_version("1_2_3")
    except ValueError as exc:
        pytest.fail(f"verify_upstream_version raises an exception: {exc}")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_debian_revision_valid() -> None:
    try:
        verify_debian_revision("1")
        verify_debian_revision("alpha1")
        verify_debian_revision("1.2.3+4~5_6")
    except ValueError as exc:
        pytest.fail(f"verify_debian_revision raises an exception: {exc}")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_debian_revision_empty() -> None:
    try:
        verify_debian_revision("")
    except ValueError as exc:
        pytest.fail(f"verify_debian_revision raises an exception: {exc}")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_debian_revision_invalid_characters() -> None:
    with pytest.raises(ValueError, match="debian_revision includes invalid character"):
        verify_debian_revision("1.2.3*")
    with pytest.raises(ValueError, match="debian_revision includes invalid character"):
        verify_debian_revision("1.2.3 4")
    with pytest.raises(ValueError, match="debian_revision includes invalid character"):
        verify_debian_revision("1.2.3/4")


@pytest.mark.skims_test_group("sca_unittesting")
def test_verify_debian_revision_edge_cases() -> None:
    try:
        verify_debian_revision("0")
        verify_debian_revision("9999999999")
        verify_debian_revision("+.~_")
        verify_debian_revision("a" * 100)
        verify_debian_revision("1" * 100)
    except ValueError as exc:
        pytest.fail(f"verify_debian_revision raises an exception: {exc}")


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_valid_version() -> None:
    result = extract("1.2.3-alpha4")
    assert result == (
        DefaultNumList([1, 2, 3, 4]),
        DefaultStringList([".", ".", "-alpha"]),
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_version_only_numbers() -> None:
    result = extract("123456")
    assert result == (DefaultNumList([123456]), DefaultStringList([]))


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_version_only_strings() -> None:
    result = extract("abc-def.ghi")
    assert result == (DefaultNumList([]), DefaultStringList(["abc-def.ghi"]))


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_version_mixed_content() -> None:
    result = extract("1a2b3c")
    assert result == (
        DefaultNumList([1, 2, 3]),
        DefaultStringList(["a", "b", "c"]),
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_version_with_special_characters() -> None:
    result = extract("1.0-rc1+build2")
    assert result == (
        DefaultNumList([1, 0, 1, 2]),
        DefaultStringList([".", "-rc", "+build"]),
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_empty_version() -> None:
    result = extract("")
    assert result == (DefaultNumList([]), DefaultStringList([]))


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_version_large_numbers() -> None:
    result = extract("9999999999.0.1")
    assert result == (
        DefaultNumList([9999999999, 0, 1]),
        DefaultStringList([".", "."]),
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_version_multiple_consecutive_non_digits() -> None:
    result = extract("1.0-alpha_beta~rc1")
    assert result == (
        DefaultNumList([1, 0, 1]),
        DefaultStringList([".", "-alpha_beta~rc"]),
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_version_init() -> None:
    version_1 = DebianVersion()
    assert version_1.epoch == 0
    assert version_1.upstream_version == ""
    assert version_1.debian_revision == ""

    version_2 = DebianVersion(epoch=1, upstream_version="2.0", debian_revision="3")
    assert version_2.epoch == 1
    assert version_2.upstream_version == "2.0"
    assert version_2.debian_revision == "3"


@pytest.mark.skims_test_group("sca_unittesting")
def test_version_comparison() -> None:
    version_1 = DebianVersion(epoch=1, upstream_version="1.0", debian_revision="1")
    version_2 = DebianVersion(epoch=1, upstream_version="1.0", debian_revision="2")
    version_3 = DebianVersion(epoch=2, upstream_version="1.0", debian_revision="1")

    assert version_1.less_than(version_2)
    assert version_2.greater_than(version_1)
    assert version_1.equal(version_1)
    assert version_3.greater_than(version_1)
    assert version_3.greater_than(version_2)


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_version_with_epoch() -> None:
    version = new_deb_version("2:1.2.3-4")
    assert version.epoch == 2
    assert version.upstream_version == "1.2.3"
    assert version.debian_revision == "4"


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_version_invalid_epoch() -> None:
    with pytest.raises(ValueError, match="epoch parse error"):
        new_deb_version("a:1.2.3-4")

    with pytest.raises(ValueError, match="epoch is negative"):
        new_deb_version("-1:1.2.3-4")


@pytest.mark.skims_test_group("sca_unittesting")
def test_compare_string_edge_cases() -> None:
    assert compare_string("", "") == 0
    assert compare_string("a", "b") < 0
    assert compare_string("b", "a") > 0
    assert compare_string("~", "a") < 0
    assert compare_string("z", "~") > 0


@pytest.mark.skims_test_group("sca_unittesting")
def test_compare_complex_versions() -> None:
    assert compare("1.0~rc1", "1.0") < 0
    assert compare("1.0+build1", "1.0+build2") < 0
    assert compare("1.0.1", "1.0+build1") > 0
    assert compare("1.0-alpha", "1.0-beta") < 0


@pytest.mark.skims_test_group("sca_unittesting")
def test_valid_function() -> None:
    assert valid("1.0") is True
    assert valid("1:2.0-3") is True
    assert valid("invalid version") is False
    assert valid("1.0a-") is True
    assert valid("1.0+") is True


@pytest.mark.skims_test_group("sca_unittesting")
def test_extract_complex_cases() -> None:
    nums, strs = extract("1.2.3a4b5c")
    assert nums == DefaultNumList([1, 2, 3, 4, 5])
    assert strs == DefaultStringList([".", ".", "a", "b", "c"])

    nums, strs = extract("1.0~rc1+build2")
    assert nums == DefaultNumList([1, 0, 1, 2])
    assert strs == DefaultStringList([".", "~rc", "+build"])


@pytest.mark.skims_test_group("sca_unittesting")
def test_default_list_behaviors() -> None:
    num_list = DefaultNumList([1, 2, 3])
    assert num_list.get(0) == 1
    assert num_list.get(5) == 0

    str_list = DefaultStringList(["a", "b", "c"])
    assert str_list.get(0) == "a"
    assert str_list.get(5) == ""


@pytest.mark.skims_test_group("sca_unittesting")
def test_deb_version_compare_greater() -> None:
    deb_version = Version("2.0.0", VersionFormat.DEB)
    deb_version.populate()
    other_version = Version("1.9.9", VersionFormat.DEB)
    other_version.populate()
    assert deb_version.version_comparator
    assert other_version.version_comparator
    assert deb_version.version_comparator.deb_ver
    assert other_version.version_comparator.deb_ver
    assert (
        deb_version.version_comparator.deb_ver.compare(other_version.version_comparator.deb_ver) > 0
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_deb_version_compare_less() -> None:
    deb_version = Version("1.0.0", VersionFormat.DEB)
    deb_version.populate()
    other_version = Version("1.0.1", VersionFormat.DEB)
    other_version.populate()
    assert deb_version.version_comparator
    assert other_version.version_comparator
    assert deb_version.version_comparator.deb_ver
    assert other_version.version_comparator.deb_ver
    assert (
        deb_version.version_comparator.deb_ver.compare(other_version.version_comparator.deb_ver) < 0
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_deb_version_compare_equal() -> None:
    deb_version = Version("1.2.3", VersionFormat.DEB)
    deb_version.populate()
    other_version = Version("1.2.3", VersionFormat.DEB)
    other_version.populate()
    assert deb_version.version_comparator
    assert other_version.version_comparator
    assert deb_version.version_comparator.deb_ver
    assert other_version.version_comparator.deb_ver
    assert not deb_version.version_comparator.deb_ver.compare(
        other_version.version_comparator.deb_ver,
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_deb_version_compare_with_epoch() -> None:
    deb_version = Version("1:1.0.0", VersionFormat.DEB)
    deb_version.populate()
    other_version = Version("2.0.0", VersionFormat.DEB)
    other_version.populate()
    assert deb_version.version_comparator
    assert other_version.version_comparator
    assert deb_version.version_comparator.deb_ver
    assert other_version.version_comparator.deb_ver
    assert (
        deb_version.version_comparator.deb_ver.compare(other_version.version_comparator.deb_ver) > 0
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_deb_version_compare_with_revision() -> None:
    deb_version = Version("1.0.0-1", VersionFormat.DEB)
    deb_version.populate()
    other_version = Version("1.0.0", VersionFormat.DEB)
    other_version.populate()
    assert deb_version.version_comparator
    assert other_version.version_comparator
    assert deb_version.version_comparator.deb_ver
    assert other_version.version_comparator.deb_ver
    assert (
        deb_version.version_comparator.deb_ver.compare(other_version.version_comparator.deb_ver) > 0
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_deb_version_compare_complex() -> None:
    deb_version = Version("2:1.0.5+dfsg-1ubuntu1", VersionFormat.DEB)
    deb_version.populate()
    other_version = Version("2:1.0.5+dfsg-1", VersionFormat.DEB)
    other_version.populate()
    assert deb_version.version_comparator
    assert other_version.version_comparator
    assert deb_version.version_comparator.deb_ver
    assert other_version.version_comparator.deb_ver
    assert (
        deb_version.version_comparator.deb_ver.compare(other_version.version_comparator.deb_ver) > 0
    )


@pytest.mark.skims_test_group("sca_unittesting")
def test_deb_version_compare_equal_1() -> None:
    deb_version = Version("1.0.2", VersionFormat.DEB)
    deb_version.populate()
    other_version = Version("1.0.0", VersionFormat.DEB)
    other_version.populate()
    assert deb_version.version_comparator
    assert other_version.version_comparator
    assert deb_version.version_comparator.deb_ver
    assert other_version.version_comparator.deb_ver
    assert (
        deb_version.version_comparator.deb_ver.compare(other_version.version_comparator.deb_ver)
        == 2
    )
