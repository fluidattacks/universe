# noqa: INP001
from typing import (
    cast,
)

import pytest
from lib_sca.sca_new.distro import (
    Distro,
    new_distro_from_release,
)
from lib_sca.sca_new.distro.type import (
    LinuxDistributionType,
)
from semantic_version import (
    Version,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("release_data", "expected"),
    [
        (
            {
                "id_": "ubuntu",
                "version_id": "20.04",
                "version": "20.04 LTS",
                "id_like": ["debian"],
            },
            Distro(
                type=LinuxDistributionType.UBUNTU,
                raw_version="20.04",
                id_like=["debian"],
                version=Version.coerce("20.04"),
            ),
        ),
        (
            {
                "id_": "debian",
                "version_id": "",
                "version": "",
                "id_like": ["debian"],
                "pretty_name": "Debian GNU/Linux sid",
            },
            Distro(
                type=LinuxDistributionType.DEBIAN,
                raw_version="unstable",
                id_like=["debian"],
                version=None,
            ),
        ),
        (
            {
                "id_": "centos",
                "version_id": "7",
                "version": "7 (Core)",
                "id_like": ["rhel fedora"],
            },
            Distro(
                type=LinuxDistributionType.CENTOS,
                raw_version="7",
                id_like=["rhel fedora"],
                version=Version.coerce("7"),
            ),
        ),
        (
            {
                "id_": "",
                "version_id": "",
                "version": "",
                "id_like": [""],
            },
            None,
        ),
    ],
)
def test_new_distro_from_release(release_data: dict[str, str], expected: Distro | None) -> None:
    result = new_distro_from_release(release_data)
    assert result == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_distro_from_release_invalid_version() -> None:
    release_data = {
        "id_": "ubuntu",
        "version_id": "invalid",
        "version": "invalid",
        "id_like": "debian",
    }
    result = new_distro_from_release(release_data)
    assert result is None


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_distro_from_release_version_fallback() -> None:
    release_data = {
        "id_": "fedora",
        "version_id": "",
        "version": "33",
        "id_like": ["fedora"],
    }
    result = new_distro_from_release(cast(dict[str, str], release_data))
    assert result == Distro(
        type=LinuxDistributionType.FEDORA,
        raw_version="33",
        id_like=["fedora"],
        version=Version.coerce("33"),
    )
