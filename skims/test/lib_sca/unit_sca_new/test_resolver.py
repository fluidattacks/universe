# noqa: INP001
import pytest
from cpe import (
    CPE,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.from_language import (
    from_language,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.python.resolver import (
    Resolver,
)
from lib_sca.sca_new.pkg.package import (
    Language,
    Location,
    Package,
    PackageType,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_normalize() -> None:
    assert Resolver.normalize("Flask-RESTful") == "flask-restful"
    assert Resolver.normalize("django_rest_framework") == "django-rest-framework"
    assert Resolver.normalize("scikit_learn") == "scikit-learn"
    assert Resolver.normalize("NUMPY") == "numpy"


@pytest.mark.skims_test_group("sca_unittesting")
def test_resolver_for_python() -> None:
    python_package = Package(
        id="2",
        name="sample-python-package",
        version="2.0.0",
        locations=[Location(path="/path/to/python/package")],
        language=Language.PYTHON,
        licenses=["Apache-2.0"],
        type=PackageType.PYTHON_PKG,
        purl="pkg:pypi/sample-python-package@2.0.0",
        cpes=[CPE("cpe:/a:sample:python-package:2.0.0")],
        metadata={"key": "value"},
    )
    resolver = from_language(python_package.language)
    assert isinstance(resolver, Resolver)
    resolved = resolver.resolve(python_package)
    assert resolved[0] == "sample-python-package"


@pytest.mark.skims_test_group("sca_unittesting")
def test_resolver_for_python_invalid_purl() -> None:
    python_package = Package(
        id="2",
        name="sample-python-package",
        version="2.0.0",
        locations=[Location(path="/path/to/python/package")],
        language=Language.PYTHON,
        licenses=["Apache-2.0"],
        type=PackageType.PYTHON_PKG,
        purl="invalid",
        cpes=[CPE("cpe:/a:sample:python-package:2.0.0")],
        metadata={"key": "value"},
    )
    resolver = from_language(python_package.language)
    assert isinstance(resolver, Resolver)
    resolver.resolve(python_package)
