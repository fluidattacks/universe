# noqa: INP001
import pytest
from lib_sca.sca_new.db.sqlite_vunnel.namespace.cpe.namespace import (
    Namespace as CPENamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.distro.namespace import (
    Namespace as DistroNamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.from_str import (
    from_str,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.language.namespace import (
    Namespace as LanguageNamespace,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_empty_namespace() -> None:
    with pytest.raises(ValueError, match="Empty namespace"):
        from_str("")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_invalid_components() -> None:
    with pytest.raises(ValueError, match="incorrect number of components"):
        from_str("single_component")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_cpe_namespace() -> None:
    result = from_str("provider:cpe")
    assert isinstance(result, CPENamespace)
    result.provider()
    result.resolver()
    result.string()


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_distro_namespace() -> None:
    result = from_str("pkg:distro:ubuntu:20.04")
    assert isinstance(result, DistroNamespace)
    result.provider()
    result.resolver()
    result.string()


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_language_namespace() -> None:
    result = from_str("provider:language:python:apk")
    assert isinstance(result, LanguageNamespace)
    result.provider()
    result.resolver()
    result.string()


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_invalid_type() -> None:
    with pytest.raises(ValueError, match="type invalid is incorrect"):
        from_str("pkg:invalid:some:components")
