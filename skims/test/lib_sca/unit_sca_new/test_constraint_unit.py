# noqa: INP001
import pytest
from lib_sca.sca_new.version.constraint_unit import (
    ConstraintUnit,
    parse_unit,
    trim_quotes,
)
from lib_sca.sca_new.version.version_operator import (
    Operator,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("operator", "version", "comparison", "expected"),
    [
        (Operator.EQ, "1.0.0", 0, True),
        (Operator.EQ, "1.0.0", 1, False),
        (Operator.GT, "1.0.0", 1, True),
        (Operator.GT, "1.0.0", 0, False),
        (Operator.GTE, "1.0.0", 0, True),
        (Operator.GTE, "1.0.0", -1, False),
        (Operator.LT, "1.0.0", -1, True),
        (Operator.LT, "1.0.0", 0, False),
        (Operator.LTE, "1.0.0", 0, True),
        (Operator.LTE, "1.0.0", 1, False),
    ],
)
def test_constraint_unit_satisfied(
    operator: Operator,
    version: str,
    comparison: int,
    expected: int,
) -> None:
    constraint = ConstraintUnit(range_operator=operator, version=version)
    assert constraint.satisfied(comparison) == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_constraint_unit_unknown_operator() -> None:
    constraint = ConstraintUnit(
        range_operator="UNKNOWN",  # type: ignore[arg-type]
        version="1.0.0",
    )
    with pytest.raises(ValueError, match="unknown operator: UNKNOWN"):
        constraint.satisfied(0)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("input_string", "expected_output"),
    [
        ('"hello"', "hello"),
        ("'world'", "world"),
        ('"quoted string with spaces"', "quoted string with spaces"),
        ("'single quoted string'", "single quoted string"),
        ('"empty"', "empty"),
        ("''", ""),
    ],
)
def test_trim_quotes_valid(input_string: str, expected_output: str) -> None:
    assert trim_quotes(input_string) == expected_output


@pytest.mark.skims_test_group("sca_unittesting")
def test_trim_quotes_empty_string() -> None:
    assert not trim_quotes("")


@pytest.mark.skims_test_group("sca_unittesting")
def test_trim_quotes_nested_quotes() -> None:
    assert trim_quotes("\"nested 'quotes'\"") == "nested 'quotes'"
    assert trim_quotes("'nested \"quotes\"'") == 'nested "quotes"'


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("phrase", "expected"),
    [
        (
            ">=1.0.0",
            ConstraintUnit(range_operator=Operator.GTE, version="1.0.0"),
        ),
        (
            "< 2.0.0",
            ConstraintUnit(range_operator=Operator.LT, version="2.0.0"),
        ),
        (
            "=3.1.4",
            ConstraintUnit(range_operator=Operator.EQ, version="3.1.4"),
        ),
        (">4.2", ConstraintUnit(range_operator=Operator.GT, version="4.2")),
        (
            "<=5.0-alpha",
            ConstraintUnit(range_operator=Operator.LTE, version="5.0-alpha"),
        ),
    ],
)
def test_parse_unit_valid(phrase: str, expected: ConstraintUnit) -> None:
    result = parse_unit(phrase)
    assert result is not None
    assert result.range_operator == expected.range_operator
    assert result.version == expected.version


@pytest.mark.skims_test_group("sca_unittesting")
def test_parse_unit_with_spaces() -> None:
    result = parse_unit("  >=  1.0.0  ")
    assert result is not None
    assert result.range_operator == Operator.GTE
    assert result.version == "1.0.0"


@pytest.mark.skims_test_group("sca_unittesting")
def test_parse_unit_with_quoted_version() -> None:
    result = parse_unit('="1.0.0"')
    assert result is not None
    assert result.range_operator == Operator.EQ
    assert result.version == "1.0.0"


@pytest.mark.skims_test_group("sca_unittesting")
def test_parse_unit_with_single_quoted_version() -> None:
    result = parse_unit("='1.0.0'")
    assert result is not None
    assert result.range_operator == Operator.EQ
    assert result.version == "1.0.0"


@pytest.mark.skims_test_group("sca_unittesting")
def test_parse_unit_with_complex_version() -> None:
    result = parse_unit(">2.1.0-alpha.1+build.2")
    assert result is not None
    assert result.range_operator == Operator.GT
    assert result.version == "2.1.0-alpha.1+build.2"


@pytest.mark.skims_test_group("sca_unittesting")
def test_parse_unit_not_match() -> None:
    result = parse_unit("abc123")
    assert result is None
