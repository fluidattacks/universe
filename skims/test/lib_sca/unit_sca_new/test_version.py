# noqa: INP001
import pytest
from cpe import (
    CPE,
)
from lib_sca.sca_new.pkg.package import (
    Language,
    Location,
    Package,
    PackageType,
)
from lib_sca.sca_new.version import (
    Version,
    new_version,
    new_version_from_package,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_version_deb() -> None:
    raw_version = "1.0.0"
    ver_format = VersionFormat.DEB
    version = new_version(raw_version, ver_format)

    assert version.raw == raw_version
    assert version.format == ver_format
    assert version.version_comparator is not None
    assert version.version_comparator.deb_ver is not None


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_version_unknown() -> None:
    raw_version = "1.0.0"
    ver_format = VersionFormat.UNKNOWN
    version = new_version(raw_version, ver_format)

    assert version.raw == raw_version
    assert version.format == ver_format
    assert version.version_comparator is not None
    assert version.version_comparator.deb_ver is None


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_version_from_package() -> None:
    package = Package(
        id="pkg-1",
        name="test-package",
        version="1.0.0",
        locations=[Location(path="/path/to/package")],
        language=Language.PYTHON,
        licenses=["MIT"],
        type=PackageType.DEB_PKG,
        purl="pkg:deb/test-package@1.0.0",
        cpes=[CPE("cpe:/a:test:test-package:1.0.0")],
        metadata={},
    )

    version = new_version_from_package(package)

    assert version is not None
    assert version.raw == package.version
    assert version.format == VersionFormat.DEB
    assert version.version_comparator is not None
    assert version.version_comparator.deb_ver is not None


@pytest.mark.skims_test_group("sca_unittesting")
def test_version_populate() -> None:
    raw_version = "1.0.0"
    ver_format = VersionFormat.DEB
    version = Version(raw=raw_version, format=ver_format)

    version.populate()

    assert version.version_comparator is not None
    assert version.version_comparator.deb_ver is not None


@pytest.mark.skims_test_group("sca_unittesting")
def test_version_populate_unknown_format() -> None:
    raw_version = "1.0.0"
    ver_format = VersionFormat.UNKNOWN
    version = Version(raw=raw_version, format=ver_format)

    version.populate()

    assert version.version_comparator is not None
    assert version.version_comparator.deb_ver is None
