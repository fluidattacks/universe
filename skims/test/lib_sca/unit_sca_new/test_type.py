# noqa: INP001
import pytest
from lib_sca.sca_new.distro.type import (
    LinuxDistributionType,
)


@pytest.mark.parametrize(
    ("key", "expected"),
    [
        ("ubuntu", LinuxDistributionType.UBUNTU),
        ("DEBIAN", LinuxDistributionType.DEBIAN),
        ("CentOS", LinuxDistributionType.CENTOS),
        ("fedora", LinuxDistributionType.FEDORA),
        ("alpine", LinuxDistributionType.ALPINE),
    ],
)
@pytest.mark.skims_test_group("sca_unittesting")
def test_get_instance(key: str, expected: LinuxDistributionType) -> None:
    assert LinuxDistributionType.get_instance(key) == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_instance_unknown() -> None:
    unknown_distro = "unknown_distro"
    with pytest.raises(ValueError):  # noqa: PT011
        LinuxDistributionType.get_instance(unknown_distro)


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_instance_empty_string() -> None:
    with pytest.raises(ValueError):  # noqa: PT011
        LinuxDistributionType.get_instance("")


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_instance_case_insensitive() -> None:
    assert LinuxDistributionType.get_instance("UBUNTU") == LinuxDistributionType.UBUNTU
    assert LinuxDistributionType.get_instance("debian") == LinuxDistributionType.DEBIAN
    assert LinuxDistributionType.get_instance("CeNtOs") == LinuxDistributionType.CENTOS


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_instance_whitespace() -> None:
    assert LinuxDistributionType.get_instance(" ubuntu ") == LinuxDistributionType.UBUNTU
    assert LinuxDistributionType.get_instance("\tdebian\n") == LinuxDistributionType.DEBIAN
