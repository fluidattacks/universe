# noqa: INP001
from collections.abc import AsyncGenerator
from typing import (
    Any,
)
from unittest.mock import (
    MagicMock,
    patch,
)

import pytest
from lib_cspm.aws.utils import (
    get_arns_that_are_excluded,
)
from model.core import (
    AwsCredentials,
)


class MockPaginator:
    def __init__(self, pages: list[dict]) -> None:
        self.pages = pages

    def paginate(
        self,
        *args: Any,  # noqa: ANN401 ARG002
        **kwargs: Any,  # noqa: ANN401 ARG002
    ) -> AsyncGenerator:
        async def async_paginator() -> AsyncGenerator:
            for page in self.pages:
                yield page

        return async_paginator()


class MockClient:
    def __init__(self, pages: list[dict]) -> None:
        self.pages = pages
        self.get_paginator = MagicMock(return_value=MockPaginator(pages))

    async def list_role_tags(self, **kwargs: Any) -> dict:  # noqa: ANN401
        if "RoleName" in kwargs and kwargs["RoleName"] == "Role1":
            return {"Tags": [{"Key": "NOFLUID", "Value": "f165_test"}]}
        return {}

    async def list_user_tags(self, **kwargs: Any) -> dict:  # noqa: ANN401
        if "UserName" in kwargs and kwargs["UserName"] == "User1":
            return {"Tags": [{"Key": "NOFLUID", "Value": "f031_test"}]}
        return {}

    async def __aenter__(self) -> "MockClient":
        return self

    async def __aexit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: object,
    ) -> None:
        # Needed to mock a context manager
        pass


class MockSession:
    def __init__(self) -> None:
        # Required to init the mock session
        pass

    def client(self, service_name: str, region_name: str | None = None) -> MockClient:
        if service_name == "resourcegroupstaggingapi" or region_name:
            return MockClient(
                [
                    {
                        "ResourceTagMappingList": [
                            {
                                "ResourceARN": "arn:aws:s3:::mybucket",
                                "Tags": [{"Key": "NOFLUID", "Value": "f165_test"}],
                            },
                            {
                                "ResourceARN": "arn:aws:s3:::mybucket2",
                                "Tags": [
                                    {
                                        "Key": "NOFLUID",
                                        "Value": "f031.f165_test",
                                    },
                                ],
                            },
                        ],
                    },
                ],
            )
        if service_name == "iam":
            return MockClient(
                [
                    {
                        "Roles": [
                            {
                                "RoleName": "Role1",
                                "Arn": ("arn:aws:iam::123456789012:role/Role1"),
                            },
                        ],
                        "Users": [
                            {
                                "UserName": "User1",
                                "Arn": ("arn:aws:iam::123456789012:user/User1"),
                            },
                        ],
                    },
                ],
            )
        return MockClient([])


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.asyncio
async def test_get_arns_that_are_excluded() -> None:
    with patch("aioboto3.Session", return_value=MockSession()):
        # Create a mock credentials object
        credentials = AwsCredentials(
            access_key_id="ASIA1",
            secret_access_key="foobar",  # noqa: S106
            session_token=None,
        )
        regions = ["us-east-1"]

        # Call the function under test
        result = await get_arns_that_are_excluded(credentials, regions)

        assert result == {
            "arn:aws:iam::123456789012:user/User1": ["f031"],
            "arn:aws:iam::123456789012:role/Role1": ["f165"],
            "arn:aws:s3:::mybucket": ["f165"],
            "arn:aws:s3:::mybucket2": ["f031", "f165"],
        }
