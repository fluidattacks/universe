from test.lib_cspm.data.gcp.f200 import (
    CopyableDict,
)


class MockIAMConfiguration:
    def __init__(self, *, bucket_policy_only_enabled: bool) -> None:
        self.bucket_policy_only_enabled = bucket_policy_only_enabled


class MockBucket:
    def __init__(
        self,
        *,
        path: str,
        bucket_policy_only_enabled: bool = True,
    ) -> None:
        self.path = path
        self.iam_configuration = MockIAMConfiguration(
            bucket_policy_only_enabled=bucket_policy_only_enabled,
        )

    @property
    def _properties(self) -> CopyableDict:
        return CopyableDict(
            {
                "path": self.path,
                "iamConfiguration": {
                    "uniformBucketLevelAccess": (self.iam_configuration.bucket_policy_only_enabled),
                },
            },
        )


def mock_data() -> tuple[list, str]:
    return [
        MockBucket(path="fluidbucket_safe"),
        MockBucket(
            path="fluidbucket_unsafe",
            bucket_policy_only_enabled=False,
        ),
    ], "fluid_test_project"
