class CopyableDict(dict):
    def copy(self) -> "CopyableDict":
        return CopyableDict(self)


class MockBucket:
    def __init__(
        self,
        *,
        path: str,
        versioning_enabled: bool = True,
        retention_policy_locked: bool = True,
        log_bucket: str | None = None,
        lifecycle_rules: list[str] | None = None,
    ) -> None:
        self.path = path
        self.versioning_enabled = versioning_enabled
        self.retention_policy_locked = retention_policy_locked
        self.log_bucket = log_bucket
        self.lifecycle_rules = lifecycle_rules if lifecycle_rules else []

    def get_logging(self) -> dict | None:
        if hasattr(self, "log_bucket") and self.log_bucket is not None:
            return {"logBucket": self.log_bucket}

        return None

    @property
    def _properties(self) -> CopyableDict:
        return CopyableDict(
            {
                "path": self.path,
                "versioning_enabled": self.versioning_enabled,
                "retention_policy_locked": self.retention_policy_locked,
                "logBucket": self.log_bucket,
            },
        )


def mock_data() -> tuple[list, str]:
    return [
        MockBucket(
            path="fluidbucket_safe",
            log_bucket="FluidLogBucket",
            lifecycle_rules=["Ok"],
        ),
        MockBucket(
            path="fluidbucket_unsafe",
            versioning_enabled=False,
            retention_policy_locked=False,
        ),
    ], "fluid_test_project"
