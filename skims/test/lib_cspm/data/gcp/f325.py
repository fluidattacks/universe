class MockIAMConfiguration:
    def __init__(self, public_access_prevention: str) -> None:
        self.public_access_prevention = public_access_prevention


class MockIAMPolicy:
    def __init__(self, bindings: list) -> None:
        self.bindings = bindings


class MockBucket:
    def __init__(
        self,
        *,
        path: str,
        public_access_prevention: str = "enforced",
        bindings_members: list[str] | None = None,
    ) -> None:
        self.path = path
        self.iam_configuration = MockIAMConfiguration(
            public_access_prevention=public_access_prevention,
        )
        self.bindings_members = (
            bindings_members if bindings_members else ["fluidUser1", "fluidUser2"]
        )

    def get_iam_policy(self) -> MockIAMPolicy:
        return MockIAMPolicy([{"members": self.bindings_members}])


def mock_data() -> tuple[list, str]:
    return [
        MockBucket(path="fluidbucket_safe"),
        MockBucket(
            path="fluidbucket_unsafe",
            public_access_prevention="free",
            bindings_members=["allUsers", "fluidUser3"],
        ),
    ], "fluid_test_project"
