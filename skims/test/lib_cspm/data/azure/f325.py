from typing import (
    Any,
    NamedTuple,
)


class Permissions(NamedTuple):
    actions: list


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "azure_role_actions_is_a_wildcard": [
            {
                "id": "vulnerable",
                "role_type": "CustomRole",
                "permissions": [{"actions": ["*"]}],
            },
            {
                "id": "vulnerable2",
                "role_type": "CustomRole",
                "permissions": [{"actions": ["action1", "*", "action3"]}],
            },
            {
                "id": "safe",
                "role_type": "CustomRole",
                "permissions": [{"actions": ["action1", "action2", "action3"]}],
            },
            {
                "id": "safe2",
                "role_type": "BuiltInRole",
                "permissions": [{"actions": ["*"]}],
            },
        ],
    }
