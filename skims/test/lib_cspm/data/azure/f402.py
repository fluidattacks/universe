from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "network_flow_log_insecure_retention_period": [
            {
                "id": "test",
                "name": "testing-flowlog",
                "type": "Microsoft.Network/networkWatchers/flowLogs",
                "location": "eastus",
                "tags": {},
                "storage_id": "test",
                "enabled": True,
                "retention_policy": {"days": 0, "enabled": False},
                "format": {"type": "JSON", "version": 2},
                "flow_analytics_configuration": {
                    "network_watcher_flow_analytics_configuration": {
                        "enabled": False,
                        "traffic_analytics_interval": 0,
                    },
                },
                "provisioning_state": "Succeeded",
            },
        ],
        "network_watcher_not_enabled": [],
        "azure_key_vault_soft_delete_retention": [
            {
                "id": "vulnerable",
                "properties": {
                    "enable_soft_delete": True,
                    "soft_delete_retention_in_days": 7,
                },
            },
            {
                "id": "safe1",
                "properties": {
                    "enable_soft_delete": False,
                    "soft_delete_retention_in_days": 7,
                },
            },
            {
                "id": "safe2",
                "properties": {
                    "enable_soft_delete": True,
                    "soft_delete_retention_in_days": 90,
                },
            },
        ],
    }
