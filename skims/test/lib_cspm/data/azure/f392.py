from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "azure_data_lake_allows_access_from_any_source": [
            {
                "id": "vulnerable1",
                "is_hns_enabled": True,
                "public_network_access": "Enabled",
                "network_rule_set": {
                    "ip_rules": [
                        {"ip_address_or_range": "0.0.0.0/0"},
                        {"ip_address_or_range": "201.49.130.76"},
                    ],
                    "virtual_network_rules": [],
                    "default_action": "Deny",
                },
            },
            {
                "id": "vulnerable2",
                "is_hns_enabled": True,
                "public_network_access": "Enabled",
                "network_rule_set": {
                    "ip_rules": [],
                    "virtual_network_rules": [],
                    "default_action": "Allow",
                },
            },
            {
                "id": "safe1",
                "is_hns_enabled": True,
                "public_network_access": "Disabled",
                "network_rule_set": {
                    "ip_rules": [
                        {"ip_address_or_range": "0.0.0.0/0"},
                        {"ip_address_or_range": "201.49.130.76"},
                    ],
                    "virtual_network_rules": [],
                    "default_action": "Deny",
                },
            },
            {
                "id": "safe2",
                "is_hns_enabled": True,
                "public_network_access": "Enabled",
                "network_rule_set": {
                    "ip_rules": [{"ip_address_or_range": "201.49.130.76"}],
                    "virtual_network_rules": [],
                    "default_action": "Deny",
                },
            },
            {
                "id": "safe3",
                "is_hns_enabled": True,
                "public_network_access": "Enabled",
                "network_rule_set": {
                    "ip_rules": [],
                    "virtual_network_rules": [],
                    "default_action": "Deny",
                },
            },
            {
                "id": "safe4",
                "is_hns_enabled": True,
                "public_network_access": "Enabled",
                "network_rule_set": {
                    "ip_rules": [{"ip_address_or_range": "0.0.0.0/0"}],
                    "virtual_network_rules": [{"virtual_network_resource_id": "safe"}],
                    "default_action": "Deny",
                },
            },
        ],
        "azure_synapse_firewall_allows_public_access": [
            {
                "id": "vulnerable 1/1",
                "public_network_access": "Enabled",
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
            {
                "id": "safe 1/3",
                "public_network_access": "Disabled",
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
            {
                "id": "safe 2/3",
                "public_network_access": "Enabled",
                "start_ip_address": "123.123.123.123",
                "end_ip_address": "129.13.123.42",
            },
            {
                "id": "safe 3/3",
                "public_network_access": "Enabled",
                "start_ip_address": None,
                "end_ip_address": None,
            },
        ],
        "az_db_psql_flex_server_firewall_allows_public_access": [
            {
                "id": "vulnerable 1/1",
                "network": {"public_network_access": "Enabled"},
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
            {
                "id": "safe 1/3",
                "network": {"public_network_access": "Disabled"},
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
            {
                "id": "safe 2/3",
                "network": {"public_network_access": "Enabled"},
                "start_ip_address": "123.123.123.123",
                "end_ip_address": "129.13.123.42",
            },
            {
                "id": "safe 3/3",
                "network": {"public_network_access": "Enabled"},
                "start_ip_address": None,
                "end_ip_address": None,
            },
        ],
    }
