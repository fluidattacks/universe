from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "has_public_cluster": {
            "DBClusters": [
                {
                    "DBClusterArn": "arn:aws:iam::123456789012:dbc/unsafe",
                    "PubliclyAccessible": True,
                },
                {
                    "DBClusterArn": "arn:aws:iam::123456789012:dbc/safe",
                    "PubliclyAccessible": False,
                },
            ],
        },
    }
