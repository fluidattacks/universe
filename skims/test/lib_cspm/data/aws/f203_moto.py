import json

import boto3
from mypy_boto3_s3 import (
    S3Client,
)


def acl_public_buckets(
    s3_service: S3Client,
) -> None:
    bucket_name = "vulnerable-bucket-example"
    s3_service.create_bucket(
        Bucket=bucket_name,
    )

    s3_service.put_bucket_acl(
        Bucket=bucket_name,
        AccessControlPolicy={
            "Grants": [
                {
                    "Grantee": {
                        "Type": "Group",
                        "URI": ("http://acs.amazonaws.com/groups/global/AllUsers"),
                    },
                    "Permission": "WRITE",
                },
            ],
            "Owner": s3_service.get_bucket_acl(Bucket=bucket_name)["Owner"],
        },
    )


def s3_buckets_allow_unauthorized_public_access(
    s3_service: S3Client,
) -> None:
    bucket_name = "public-bucket"
    s3_service.create_bucket(
        Bucket=bucket_name,
    )

    bucket_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "policy": "*",
                },
                "Resource": [
                    "*",
                ],
                "Action": [
                    "s3:*",
                ],
            },
        ],
    }

    s3_service.put_bucket_policy(
        Bucket=bucket_name,
        Policy=json.dumps(bucket_policy),
    )


def main() -> None:
    s3_service = boto3.client("s3")
    acl_public_buckets(s3_service)
    s3_buckets_allow_unauthorized_public_access(s3_service)
