import boto3
from mypy_boto3_rds import (
    RDSClient,
)


def rds_has_not_deletion_protection(
    rds_service: RDSClient,
) -> None:
    rds_service.create_db_cluster(
        DBClusterIdentifier="vulnerable-rds-cluster",
        Engine="aurora-mysql",
        MasterUsername="admin",
        MasterUserPassword="password",
        DeletionProtection=False,
    )

    rds_service.create_db_instance(
        DBInstanceIdentifier="vulnerable-rds-instance",
        DBInstanceClass="db.t3.micro",
        Engine="aurora-mysql",
        DBClusterIdentifier="vulnerable-rds-cluster",
        DeletionProtection=False,
        PubliclyAccessible=True,
    )


def rds_has_not_automated_backups(
    rds_service: RDSClient,
) -> None:
    rds_service.create_db_instance(
        DBInstanceIdentifier="vulnerable-rds-instance-no-backups",
        DBInstanceClass="db.t3.micro",
        Engine="mysql",
        MasterUsername="admin",
        MasterUserPassword="password",
        AllocatedStorage=20,
        BackupRetentionPeriod=0,
        PubliclyAccessible=True,
    )


def main() -> None:
    rds_service = boto3.client("rds", region_name="us-east-1")
    rds_has_not_deletion_protection(rds_service)
    rds_has_not_automated_backups(rds_service)
