import boto3
import requests
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_rds import (
    RDSClient,
)


def instances_is_not_inside_a_db_subnet_group(
    rds_service: RDSClient,
) -> None:
    rds_service.create_db_instance(
        DBInstanceIdentifier="vulnerable-rds-instance",
        AllocatedStorage=20,
        DBName="testdb",
        Engine="mysql",
        MasterUsername="admin",
        MasterUserPassword="TestPassword123!",
        DBInstanceClass="db.t3.micro",
        PubliclyAccessible=True,
    )


def rds_unrestricted_db_security_groups(
    ec2_service: EC2Client,
    rds_service: RDSClient,
) -> None:
    security_group = ec2_service.create_security_group(
        GroupName="vulnerable-rds-sg",
        Description="Security group with open access for RDS instance",
        VpcId="YOUR_VPC_ID",
    )
    security_group_id = security_group["GroupId"]

    ec2_service.authorize_security_group_ingress(
        GroupId=security_group_id,
        IpPermissions=[
            {
                "IpProtocol": "tcp",
                "FromPort": 3306,
                "ToPort": 3306,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
        ],
    )

    rds_service.create_db_instance(
        DBInstanceIdentifier="vulnerable-rds-instance",
        AllocatedStorage=20,
        DBName="testdb",
        Engine="mysql",
        MasterUsername="admin",
        MasterUserPassword="TestPassword123!",
        DBInstanceClass="db.t3.micro",
        VpcSecurityGroupIds=[security_group_id],
        PubliclyAccessible=True,
    )
    rds_service.create_db_cluster(
        DBClusterIdentifier="vulnerable-rds-instance",
        Engine="aurora-mysql",
        MasterUsername="admin",
        MasterUserPassword="TestPassword123!",
        VpcSecurityGroupIds=[security_group_id],
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    rds_service = boto3.client("rds", region_name="us-east-1")
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    instances_is_not_inside_a_db_subnet_group(rds_service)
    rds_unrestricted_db_security_groups(ec2_service, rds_service)
