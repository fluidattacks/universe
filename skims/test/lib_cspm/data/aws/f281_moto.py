import json

import boto3
from mypy_boto3_s3 import (
    S3Client,
)


def s3_has_insecure_transport(
    s3_service: S3Client,
) -> None:
    bucket_name = "vulnerable-bucket"
    s3_service.create_bucket(Bucket=bucket_name)

    bucket_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AllowInsecureTransport",
                "Effect": "Allow",
                "Principal": "*",
                "Action": "s3:GetObject",
                "Resource": f"arn:aws:s3:::{bucket_name}/*",
                "Condition": {"Bool": {"aws:SecureTransport": "false"}},
            },
        ],
    }

    s3_service.put_bucket_policy(Bucket=bucket_name, Policy=json.dumps(bucket_policy))


def main() -> None:
    s3_service = boto3.client("s3")
    s3_has_insecure_transport(s3_service)
