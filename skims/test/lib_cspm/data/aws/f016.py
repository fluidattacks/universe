from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "api_gateway_insecure_tls_version": {
            "items": [
                {
                    "id": "Vulnerable 1/1",
                    "domainName": "etobotestfluid.click",
                    "securityPolicy": "TLS_1_0",
                },
                {
                    "id": "Safe 1/1",
                    "domainName": "myawesomedomain.com",
                    "securityPolicy": "TLS_1_2",
                },
            ],
        },
        "aws_app_mesh_virtual_gateway_tls_disabled": {
            "meshes": [{"meshName": "my_app_mesh"}],
            "virtualGateways": [{"virtualGatewayName": "my_virtual_gateway"}],
            "virtualGateway": {
                "meshName": "my_app_mesh",
                "virtualGatewayName": "Vulnerable 1/1",
                "metadata": {"meshOwner": "2020202020"},
                "spec": {"backendDefaults": {"clientPolicy": {"tls": {"enforce": False}}}},
            },
        },
        "aws_rds_cluster_tls_disabled": {
            "DBClusters": [
                {
                    "DBClusterArn": "DBClusterArn",
                    "Engine": "aurora-mysql",
                    "DBClusterParameterGroup": "cluster_parameter_group",
                },
            ],
            "Parameters": [
                {
                    "ParameterName": "require_secure_transport",
                    "ParameterValue": "OFF",
                },
                {
                    "ParameterName": "rds.force_ssl",
                    "ParameterValue": "0",
                },
            ],
        },
        "aws_msk_client_broker_tls_disabled": {
            "ClusterInfoList": [
                {
                    "ClusterType": "Provisioned",
                    "ClusterArn": "arn:aws:kafka:us-east-1:123:cluster/abc",
                },
            ],
            "ClusterInfo": {
                "EncryptionInfo": {"EncryptionInTransit": {"ClientBroker": "PLAINTEXT"}},
            },
        },
        "aws_msk_broker_broker_tls_disabled": {
            "ClusterInfoList": [
                {
                    "ClusterType": "Provisioned",
                    "ClusterArn": "arn:aws:kafka:us-east-1:123:cluster/abc",
                },
            ],
            "ClusterInfo": {
                "EncryptionInfo": {
                    "EncryptionInTransit": {
                        "ClientBroker": "PLAINTEXT",
                        "InCluster": False,
                    },
                },
            },
        },
        "aws_document_db_cluster_tls_disabled": {
            "DBClusters": [
                {
                    "DBClusterArn": "arn:aws:rds:us-e1:123:cluster:vuln-db",
                    "DBClusterParameterGroup": "vulnerable-db-parameter-group",
                },
            ],
            "Parameters": [
                {
                    "Sid": "Disabled TLS",
                    "ParameterName": "tls",
                    "ParameterValue": "disabled",
                },
                {
                    "Sid": "Enabled TLS",
                    "ParameterName": "tls",
                    "ParameterValue": "enabled",
                },
            ],
        },
        "default": {},
    }
