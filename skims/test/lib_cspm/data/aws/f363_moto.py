import boto3
from mypy_boto3_iam import (
    IAMClient,
)


def password_expiration_unsafe(
    iam_service: IAMClient,
) -> None:
    """Report vulnerabilities for methods.

    > password_reuse_unsafe
    > min_password_len_unsafe
    > password_expiration_unsafe
    > not_requires_lowercase
    > not_requires_uppercase
    > not_requires_symbols
    > not_requires_numbers
    """
    iam_service.update_account_password_policy(
        MinimumPasswordLength=8,
        RequireSymbols=False,
        RequireNumbers=False,
        RequireUppercaseCharacters=False,
        RequireLowercaseCharacters=False,
        AllowUsersToChangePassword=False,
        MaxPasswordAge=120,
        PasswordReusePrevention=5,
        HardExpiry=False,
    )


def main() -> None:
    iam_service = boto3.client("iam")
    password_expiration_unsafe(iam_service)
