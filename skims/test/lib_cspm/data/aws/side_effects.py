import inspect
from collections.abc import (
    Collection,
)
from typing import (
    Any,
)

from test.lib_cspm.data.aws import (
    f016,
    f024,
    f073,
    f101,
    f109,
    f165,
    f177,
    f257,
    f277,
    f325,
    f333,
    f372,
    f392,
    f396,
    f400,
    f406,
)

MOCKERS: dict[str, dict[str, dict[str, Collection | bool]]] = {
    "F005": {"default": {}},
    "F016": f016.mock_data(),
    "F024": f024.mock_data(),
    "F031": {"default": {}},
    "F073": f073.mock_data(),
    "F081": {"default": {}},
    "F099": {"default": {}},
    "F101": f101.mock_data(),
    "F109": f109.mock_data(),
    "F165": f165.mock_data(),
    "F177": f177.mock_data(),
    "F203": {"default": {}},
    "F246": {"default": {}},
    "F256": {"default": {}},
    "F257": f257.mock_data(),
    "F258": {"default": {}},
    "F259": {"default": {}},
    "F277": f277.mock_data(),
    "F281": {"default": {}},
    "F325": f325.mock_data(),
    "F333": f333.mock_data(),
    "F335": {"default": {}},
    "F363": {"default": {}},
    "F372": f372.mock_data(),
    "F392": f392.mock_data(),
    "F394": {"default": {}},
    "F396": f396.mock_data(),
    "F400": f400.mock_data(),
    "F406": f406.mock_data(),
    "F407": {"default": {}},
    "F433": {"default": {}},
}


def get_mock_info(
    finding: str,
) -> dict[str, dict[str, Collection | bool]] | None:
    return MOCKERS.get(finding)


def make_side_effect(
    mock_data: dict[str, dict[str, Collection | bool]],
) -> dict[str, Collection | Any]:
    def side_effect(
        *args: Any,  # noqa: ARG001 ANN401
        **kwargs: Any,  # noqa: ARG001 ANN401
    ) -> dict[str, Collection | Any]:
        stack = inspect.stack()
        # We need to know the invoker function of run_boto3_fun
        # so we can use it as key to retrieve the specific
        # mock_data. Sometimes the invoker of run_boto3_fun
        # is an util function inside a vulnerability check method
        # thats why we check indexes 3 and 4 of the stack.
        invoker = stack[3].function
        upper_invoker = stack[4].function
        if mock_data.get(invoker):
            return mock_data[invoker]
        if mock_data.get(upper_invoker):
            return mock_data[upper_invoker]
        return {}

    return side_effect()
