# Attempt to import optional dependencies
import random
import string
from collections.abc import (
    Awaitable,
    Callable,
    Generator,
    Iterator,
)
from contextlib import (
    contextmanager,
)
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from typing import (
    Any,
    TypeVar,
)
from unittest.mock import (
    patch,
)

import aiobotocore
import aiobotocore.endpoint
import botocore
import botocore.retries.standard
from moto import (
    mock_aws,
)

T = TypeVar("T")
R = TypeVar("R")


class IDGenerator:
    def __init__(self, seed: int | None = None) -> None:
        if seed is not None:
            random.seed(seed)

    def generate_policy_id(self) -> str:
        return "A" + "".join(
            random.choices(string.ascii_uppercase + string.digits, k=20),  # noqa:S311
        )


@dataclass
class _PatchedAWSReponseContent:
    """Patched version of `botocore.awsrequest.AWSResponse.content` ."""

    content: bytes | Awaitable[bytes]

    def __await__(self) -> Iterator[bytes]:
        async def _generate_async() -> bytes:
            if isinstance(self.content, Awaitable):
                return await self.content
            return self.content

        return _generate_async().__await__()

    def decode(self, encoding: str) -> str:
        assert isinstance(self.content, bytes)
        return self.content.decode(encoding)


class PatchedAWSResponse:
    """Patched version of `botocore.awsrequest.AWSResponse`."""

    def __init__(self, response: botocore.awsrequest.AWSResponse) -> None:
        self._response = response
        self.status_code = response.status_code

        self.headers = response.headers
        self.headers["x-amz-crc32"] = None
        self.url = response.url
        self.content = _PatchedAWSReponseContent(response.content)
        self._content = self.content
        self.raw = response.raw
        self.text = response.text
        if not hasattr(self.raw, "raw_headers"):
            self.raw.raw_headers = {}


class PatchedRetryContext(botocore.retries.standard.RetryContext):
    """Patched version of `botocore.retries.standard.RetryContext`."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:  # noqa: ANN401
        if kwargs.get("http_response"):
            kwargs["http_response"] = PatchedAWSResponse(kwargs["http_response"])
        super().__init__(*args, **kwargs)


def _factory(
    original: Callable[[botocore.awsrequest.AWSResponse, T], Awaitable[R]],
) -> Callable[[botocore.awsrequest.AWSResponse, T], Awaitable[R]]:
    async def patched_convert_to_response_dict(
        http_response: botocore.awsrequest.AWSResponse,
        operation_model: T,
    ) -> R:
        return await original(
            PatchedAWSResponse(http_response),  # type: ignore[arg-type]
            operation_model,
        )

    return patched_convert_to_response_dict


@contextmanager
def mock_aio_aws() -> Generator[None, None, None]:
    """Patch for Moto library.

    Inspired by a GitHub discussion
    (https://github.com/aio-libs/aiobotocore/issues/755).
    """
    with (
        patch(
            "aiobotocore.endpoint.convert_to_response_dict",
            new=_factory(aiobotocore.endpoint.convert_to_response_dict),
        ),
        patch("botocore.retries.standard.RetryContext", new=PatchedRetryContext),
        patch(
            "moto.iam.models.random_policy_id",
            new=IDGenerator(seed=12345).generate_policy_id,
        ),
        patch(
            "moto.iam.models.utcnow",
            return_value=datetime(2024, 1, 1),  # noqa: DTZ001,
        ),
        patch(
            "moto.core.responses.gen_amzn_requestid_long",
            return_value=("92pp4YyTuo8owd8VEOq00lMMkGk9GYoGV5rfGymHIhz1A6CQ7o5v"),
        ),
        patch(
            "moto.eks.models.ENDPOINT_TEMPLATE",
            new=("https://uySzDTe9H6G5PivXZGHv.Lkg.us-east-1.eks.amazonaws.com/"),
        ),
        patch(
            "moto.rds.models.iso_8601_datetime_with_milliseconds",
            return_value=("2024-01-01 00:00:00+00:00"),
        ),
        patch(
            "moto.eks.models.iso_8601_datetime_without_milliseconds",
            return_value=("2024-01-01 00:00:00+00:00"),
        ),
        patch(
            "moto.ec2.models.instances.utc_date_and_time",
            return_value=("2024-01-01 00:00:00+00:00"),
        ),
        patch(
            "moto.ec2.models.elastic_block_store.utc_date_and_time",
            return_value=("2024-01-01 00:00:00+00:00"),
        ),
        patch(
            "moto.ec2.models.amis.utc_date_and_time",
            return_value=("2024-01-01 00:00:00+00:00"),
        ),
        patch(
            "moto.elbv2.models.id",
            return_value=5785188048,
        ),
        patch(
            "moto.elasticache.models.utcnow",
            return_value=datetime(2024, 1, 1),  # noqa: DTZ001,
        ),
        mock_aws(
            config={
                "iam": {"load_aws_managed_policies": True},
            },
        ),
    ):
        yield
