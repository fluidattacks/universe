import boto3
from mypy_boto3_rds import (
    RDSClient,
)


def has_public_instances(
    rds_service: RDSClient,
) -> None:
    cluster_identifier = "vulnerable-rds-cluster"

    rds_service.create_db_cluster(
        DBClusterIdentifier=cluster_identifier,
        Engine="aurora-mysql",
        MasterUsername="admin",
        MasterUserPassword="password123",
    )

    instance_identifier = "vulnerable-rds-instance"
    rds_service.create_db_instance(
        DBInstanceIdentifier=instance_identifier,
        DBInstanceClass="db.t2.small",
        Engine="aurora-mysql",
        DBClusterIdentifier=cluster_identifier,
        PubliclyAccessible=True,
    )


def main() -> None:
    rds_service = boto3.client("rds", region_name="us-east-1")
    has_public_instances(rds_service)
