from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "ec2_has_not_termination_protection": {
            "Reservations": [
                {
                    "Instances": [
                        {
                            "InstanceId": "123",
                        },
                    ],
                    "OwnerId": "fluidattacks",
                },
            ],
            "DisableApiTermination": {
                "Value": False,
            },
        },
        "default": {},
    }
