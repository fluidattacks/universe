from typing import TYPE_CHECKING

import boto3
import requests
from mypy_boto3_cloudfront import (
    CloudFrontClient,
)
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_elbv2 import (
    ElasticLoadBalancingv2Client,
)
from mypy_boto3_opensearch import (
    OpenSearchServiceClient,
)
from mypy_boto3_rds import (
    RDSClient,
)

if TYPE_CHECKING:
    from mypy_boto3_cloudfront.type_defs import (
        DistributionConfigTypeDef,
    )
    from mypy_boto3_rds.type_defs import (
        ParameterTypeDef,
    )


def serves_content_over_insecure_protocols(
    cloudfront_service: CloudFrontClient,
) -> None:
    distribution_config: DistributionConfigTypeDef = {
        "CallerReference": "my-distribution",
        "Origins": {
            "Quantity": 1,
            "Items": [
                {
                    "Id": "my-origin",
                    "DomainName": "example.com",
                    "CustomOriginConfig": {
                        "HTTPPort": 80,
                        "HTTPSPort": 443,
                        "OriginProtocolPolicy": "http-only",
                    },
                },
            ],
        },
        "DefaultCacheBehavior": {
            "TargetOriginId": "my-origin",
            "ForwardedValues": {
                "QueryString": False,
                "Cookies": {"Forward": "none"},
            },
            "TrustedSigners": {
                "Enabled": False,
                "Quantity": 0,
            },
            "ViewerProtocolPolicy": "allow-all",
            "MinTTL": 0,
        },
        "Comment": "My vulnerable distribution",
        "Enabled": True,
    }

    cloudfront_service.create_distribution(DistributionConfig=distribution_config)


def elbv2_uses_insecure_ssl_protocol(
    elbv2_service: ElasticLoadBalancingv2Client,
    ec2_service: EC2Client,
) -> None:
    """Report vulnerabilities for methods.

    > elbv2_uses_insecure_ssl_protocol,
    > elbv2_uses_insecure_ssl_cipher,
    > uses_insecure_security_policy,
    """
    vpc_response = ec2_service.create_vpc(
        CidrBlock="10.0.0.0/16",
        TagSpecifications=[
            {
                "ResourceType": "vpc",
                "Tags": [{"Key": "Name", "Value": "my-vpc"}],
            },
        ],
    )
    vpc_id = vpc_response["Vpc"]["VpcId"]

    subnet_response = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.1.0/24",
        AvailabilityZone="us-east-1a",
        TagSpecifications=[
            {
                "ResourceType": "subnet",
                "Tags": [{"Key": "Name", "Value": "my-subnet"}],
            },
        ],
    )
    subnet_id = subnet_response["Subnet"]["SubnetId"]

    sg_response = ec2_service.create_security_group(
        GroupName="my-sg",
        Description="My security group",
        VpcId=vpc_id,
        TagSpecifications=[
            {
                "ResourceType": "security-group",
                "Tags": [{"Key": "Name", "Value": "my-sg"}],
            },
        ],
    )
    sg_id = sg_response["GroupId"]

    ec2_service.authorize_security_group_ingress(
        GroupId=sg_id,
        IpPermissions=[
            {
                "IpProtocol": "tcp",
                "FromPort": 80,
                "ToPort": 80,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
            {
                "IpProtocol": "tcp",
                "FromPort": 443,
                "ToPort": 443,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
        ],
    )

    lb_response = elbv2_service.create_load_balancer(
        Name="my-insecure-alb",
        Subnets=[subnet_id],
        SecurityGroups=[sg_id],
        Scheme="internet-facing",
        Type="application",
        IpAddressType="ipv4",
    )
    lb_arn = lb_response["LoadBalancers"][0]["LoadBalancerArn"]

    elbv2_service.create_listener(
        LoadBalancerArn=lb_arn,
        Protocol="HTTPS",
        Port=443,
        SslPolicy="ELBSecurityPolicy-TLS-1-0-2015-04",
        Certificates=[
            {
                "CertificateArn": (
                    "arn:aws:acm:us-east-1:123456789012:"
                    "certificate/12345678-1234-1234-1234-123456789012"
                ),
            },
        ],
        DefaultActions=[
            {
                "Type": "fixed-response",
                "FixedResponseConfig": {
                    "MessageBody": "Insecure SSL Policy",
                    "StatusCode": "200",
                    "ContentType": "text/plain",
                },
            },
        ],
    )


def aws_rds_instance_tls_disabled(rds_service: RDSClient) -> None:
    test_cases = [
        {
            "instance_id": "vulnerable-instance",
            "param_group_name": "Vulnerable param group",
            "status": "Vulnerable",
            "value_1": "OFF",
            "value_2": "0",
        },
        {
            "instance_id": "safe-instance",
            "param_group_name": "Safe param group",
            "status": "Safe",
            "value_1": "ON",
            "value_2": "1",
        },
    ]
    for test_case in test_cases:
        param_group_name = test_case.get("param_group_name", "")
        status = test_case.get("status", "")
        value_1 = test_case.get("value_1", "")
        value_2 = test_case.get("value_2", "")

        rds_service.create_db_parameter_group(
            DBParameterGroupFamily="mysql5.6",
            DBParameterGroupName=param_group_name,
            Description="Parameter group description",
        )
        parameters: list[ParameterTypeDef] = [
            {
                "ParameterName": "require_secure_transport",
                "AllowedValues": "ON, OFF",
                "DataType": "string",
                "Description": f"{status} param 1/2",
                "ParameterValue": value_1,
                "Source": "user",
                "ApplyType": "dynamic",
                "IsModifiable": True,
                "ApplyMethod": "immediate",
                "SupportedEngineModes": ["provisioned"],
            },
            {
                "ParameterName": "rds.force_ssl",
                "AllowedValues": "0, 1",
                "DataType": "boolean",
                "Description": f"{status} param 2/2",
                "ParameterValue": value_2,
                "Source": "user",
                "ApplyType": "dynamic",
                "IsModifiable": True,
                "ApplyMethod": "immediate",
                "SupportedEngineModes": ["provisioned"],
            },
        ]
        rds_service.modify_db_parameter_group(
            DBParameterGroupName=param_group_name,
            Parameters=parameters,
        )
        rds_service.create_db_instance(
            DBInstanceIdentifier=test_case.get("instance_id", ""),
            DBParameterGroupName=param_group_name,
            Engine="mysql",
            DBInstanceClass="db.m7g",
            MasterUsername="admin",
            MasterUserPassword="password123",
        )


def aws_opensearch_domain_insecure_tls_version(
    opensearch_service: OpenSearchServiceClient,
) -> None:
    opensearch_service.create_domain(
        DomainName="vulnerable_domain_1_2",
        DomainEndpointOptions={
            "TLSSecurityPolicy": "Policy-Min-TLS-1-0-2019-07",
        },
    )
    opensearch_service.create_domain(
        DomainName="vulnerable_domain_2_2",
        DomainEndpointOptions={
            "TLSSecurityPolicy": "Policy-Min-TLS-1-2-2019-07",
        },
    )
    opensearch_service.create_domain(
        DomainName="safe_domain",
        DomainEndpointOptions={
            "TLSSecurityPolicy": "Policy-Min-TLS-1-2-PFS-2023-10",
        },
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    cloudfront_service = boto3.client("cloudfront")
    elbv2_service = boto3.client("elbv2", region_name="us-east-1")
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    rds_service = boto3.client("rds", region_name="us-east-1")
    opensearch_service = boto3.client("opensearch")
    serves_content_over_insecure_protocols(cloudfront_service)
    elbv2_uses_insecure_ssl_protocol(elbv2_service, ec2_service)
    aws_rds_instance_tls_disabled(rds_service)
    aws_opensearch_domain_insecure_tls_version(opensearch_service)
