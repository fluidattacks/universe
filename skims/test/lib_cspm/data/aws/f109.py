from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "clusters_is_not_inside_a_db_subnet_group": {
            "DBClusters": [
                {
                    "DBClusterArn": ("arn:aws:iam::123456789012:dbc/mydbcluster"),
                },
            ],
        },
    }
