from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "redshift_has_audit_logging_disabled": {
            "Account": "234234234234",
            "Clusters": [
                {
                    "ClusterIdentifier": "fluidRedShiftCluster",
                    "KmsKeyId": "fluidkey/8888",
                    "ClusterParameterGroups": [
                        {
                            "ParameterGroupName": "fluidParamGroup1",
                        },
                    ],
                },
            ],
            "LoggingEnabled": False,
        },
        "redshift_is_user_activity_logging_disabled": {
            "Account": "234234234234",
            "Clusters": [
                {
                    "ClusterIdentifier": "fluidRedShiftCluster",
                    "KmsKeyId": "fluidkey/8888",
                    "ClusterParameterGroups": [
                        {
                            "ParameterGroupName": "fluidParamGroup1",
                        },
                    ],
                },
            ],
            "Parameters": [
                {
                    "ParameterName": "enable_user_activity_logging",
                    "ParameterValue": "false",
                },
                {
                    "ParameterName": "enable_user_activity_logging",
                    "ParameterValue": "true",
                },
            ],
        },
        "aws_app_mesh_virtual_gateway_access_logging_disabled": {
            "meshes": [{"meshName": "my_app_mesh"}],
            "virtualGateways": [{"virtualGatewayName": "Vulnerable 1/1"}],
            "virtualGateway": {
                "meshName": "my_app_mesh",
                "virtualGatewayName": "Vulnerable 1/1",
                "metadata": {"meshOwner": "2020202020"},
                "spec": {"logging": {}},
            },
        },
        "aws_cloud_trail_delivery_failing": {
            "Trails": [
                {
                    "TrailARN": "arn:aws:cloudtrail:us-east-2:00:trail/abc",
                    "Name": "abc",
                },
            ],
            "LatestDeliveryError": "KMS.DisabledException",
        },
        "aws_beanstalk_persistent_logs": {
            "Environments": [
                {
                    "EnvironmentArn": "environment_arn",
                    "EnvironmentName": "vulnerable_env",
                    "ApplicationName": "vulnerable_app",
                },
            ],
            "ConfigurationSettings": [
                {
                    "OptionSettings": [
                        {
                            "OptionName": "LogPublicationControl",
                            "Value": "false",
                        },
                        {"OptionName": "StreamLogs", "Value": "false"},
                    ],
                },
            ],
        },
        "aws_document_db_without_audit_logs": {
            "DBClusters": [
                {
                    "DBClusterArn": "arn:aws:rds:us-e1:123:cluster:vuln-db",
                    "DBClusterParameterGroup": "vulnerable-db-parameter-group",
                },
            ],
            "Parameters": [
                {
                    "ParameterName": "audit_logs",
                    "ParameterValue": "disabled",
                },
            ],
        },
        "aws_global_accelerator_flow_logs_disabled": {
            "Accelerators": [{"AcceleratorArn": "arn:aws:globalaccelerator:*"}],
            "AcceleratorAttributes": {"FlowLogsEnabled": False},
        },
        "aws_msk_cluster_logging_disabled": {
            "ClusterInfoList": [
                {
                    "ClusterArn": (
                        "arn:aws:msk:us-east-1:123456789012:cluster/vulnerable-cluster",
                    ),
                    "Provisioned": {
                        "LoggingInfo": None,
                    },
                },
                {
                    "ClusterArn": ("arn:aws:msk:us-east-1:123456789012:cluster/safe-cluster",),
                    "Provisioned": {
                        "LoggingInfo": {"BrokerLogs": {"CloudWatchLogs": {"Enabled": False}}},
                    },
                },
            ],
        },
    }
