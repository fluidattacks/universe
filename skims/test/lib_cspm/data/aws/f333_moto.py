import json

import boto3
import requests
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_iam import (
    IAMClient,
)


def aws_ec2_instance_has_multiple_network_interfaces(
    ec2_service: EC2Client,
) -> None:
    """Report vulnerabilities for methods.

    > aws_ec2_instance_has_multiple_network_interfaces
    > ec2_iam_instances_without_profile
    """
    vpc_response = ec2_service.create_vpc(CidrBlock="10.0.0.0/16")
    vpc_id = vpc_response["Vpc"]["VpcId"]
    subnet = ec2_service.create_subnet(CidrBlock="10.0.0.0/24", VpcId=vpc_id)
    subnet_id = subnet["Subnet"]["SubnetId"]
    network_interface_1 = ec2_service.create_network_interface(SubnetId=subnet_id)
    network_interface_2 = ec2_service.create_network_interface(SubnetId=subnet_id)
    ec2_service.run_instances(
        ImageId="ami-0abcdef1234567890",
        MinCount=1,
        MaxCount=1,
        InstanceType="t2.micro",
        NetworkInterfaces=[
            {
                "NetworkInterfaceId": network_interface_1["NetworkInterface"]["NetworkInterfaceId"],
                "DeviceIndex": 0,
            },
            {
                "NetworkInterfaceId": network_interface_2["NetworkInterface"]["NetworkInterfaceId"],
                "DeviceIndex": 1,
            },
        ],
    )


def has_modify_instance_attribute(
    iam_service: IAMClient,
    ec2_service: EC2Client,
) -> None:
    role_name = "vulnerable_ec2_role"
    assume_role_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"Service": "ec2.amazonaws.com"},
                "Action": "sts:AssumeRole",
            },
        ],
    }
    iam_service.create_role(
        RoleName=role_name,
        AssumeRolePolicyDocument=json.dumps(assume_role_policy),
    )

    modify_instance_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": ["ec2:ModifyInstanceAttribute"],
                "Resource": "*",
            },
        ],
    }
    iam_service.put_role_policy(
        RoleName=role_name,
        PolicyName="ModifyInstancePolicy",
        PolicyDocument=json.dumps(modify_instance_policy),
    )

    instance_profile_name = "vulnerable_instance_profile"
    iam_service.create_instance_profile(InstanceProfileName=instance_profile_name)
    iam_service.add_role_to_instance_profile(
        InstanceProfileName=instance_profile_name,
        RoleName=role_name,
    )
    full_access_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": "ec2:ModifyInstanceAttribute",
                "Resource": "*",
            },
        ],
    }
    policy_response = iam_service.create_policy(
        PolicyName="CustomFullAccessPolicy",
        PolicyDocument=json.dumps(full_access_policy),
    )
    policy_arn = policy_response["Policy"]["Arn"]

    iam_service.attach_role_policy(RoleName=role_name, PolicyArn=policy_arn)
    vpc_response = ec2_service.create_vpc(CidrBlock="10.0.0.0/16")
    vpc_id = vpc_response["Vpc"]["VpcId"]

    subnet = ec2_service.create_subnet(CidrBlock="10.0.1.0/24", VpcId=vpc_id)
    subnet_id = subnet["Subnet"]["SubnetId"]

    ec2_service.run_instances(
        ImageId="ami-0abcdef1234567890",
        MinCount=1,
        MaxCount=1,
        InstanceType="t2.micro",
        NetworkInterfaces=[{"SubnetId": subnet_id, "DeviceIndex": 0}],
        IamInstanceProfile={"Name": instance_profile_name},
        InstanceInitiatedShutdownBehavior="stop",
    )


def ec2_has_terminate_shutdown_behavior(
    ec2_service: EC2Client,
) -> None:
    """Report vulnerabilities for methods.

    > ec2_has_terminate_shutdown_behavior
    > ec2_iam_instances_without_profile
    """
    ec2_service.run_instances(
        ImageId="ami-0abcdef1234567890",
        InstanceType="t2.micro",
        MinCount=1,
        MaxCount=1,
        InstanceInitiatedShutdownBehavior="terminate",
        TagSpecifications=[
            {
                "ResourceType": "instance",
                "Tags": [{"Key": "Name", "Value": "VulnerableInstance"}],
            },
        ],
    )


def has_unused_ec2_key_pairs(
    ec2_service: EC2Client,
) -> None:
    ec2_service.create_key_pair(KeyName="UnusedKeyPair")


def has_publicly_shared_amis(
    ec2_service: EC2Client,
) -> None:
    instance_response = ec2_service.run_instances(
        ImageId="ami-0abcdef1234567890",
        InstanceType="t2.micro",
        MinCount=1,
        MaxCount=1,
    )
    instance_id = instance_response["Instances"][0]["InstanceId"]

    ami_response = ec2_service.create_image(
        InstanceId=instance_id,
        Name="PubliclySharedAMI",
        NoReboot=True,
    )
    ami_id = ami_response["ImageId"]

    ec2_service.modify_image_attribute(ImageId=ami_id, LaunchPermission={"Add": [{"Group": "all"}]})


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    iam_service = boto3.client("iam")
    aws_ec2_instance_has_multiple_network_interfaces(ec2_service)
    has_modify_instance_attribute(iam_service, ec2_service)
    ec2_has_terminate_shutdown_behavior(ec2_service)
    has_unused_ec2_key_pairs(ec2_service)
    has_publicly_shared_amis(ec2_service)
