import json
from typing import (
    Any,
)

import boto3
from mypy_boto3_iam import (
    IAMClient,
)
from mypy_boto3_s3 import (
    S3Client,
)
from mypy_boto3_sqs import (
    SQSClient,
)


def private_buckets_not_blocking_public_acls(
    s3_service: S3Client,
) -> None:
    s3_service.create_bucket(Bucket="test_not_blocking_public_acls")
    s3_service.put_public_access_block(
        Bucket="test_not_blocking_public_acls",
        PublicAccessBlockConfiguration={
            "BlockPublicAcls": False,
            "IgnorePublicAcls": False,
            "BlockPublicPolicy": False,
            "RestrictPublicBuckets": False,
        },
    )


def public_buckets(s3_service: S3Client) -> None:
    s3_service.create_bucket(Bucket="test_public_bucket")
    s3_service.put_bucket_acl(
        Bucket="test_public_bucket",
        GrantFullControl=("uri=http://acs.amazonaws.com/groups/global/AuthenticatedUsers"),
    )


def iam_has_wildcard_resource_on_write_action(
    iam_service: IAMClient,
) -> None:
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Resource": "*",
                "Action": "*",
            },
            {
                "Effect": "Allow",
                "Resource": "*",
                "Action": "activate:CreateForm",
            },
            {
                "Effect": "Allow",
                "Resource": "*",
                "Action": "example:CreateForm",
            },
            {
                "Effect": "Allow",
                "Resource": "arn:aws:s3:::my_test_bucket/*",
                "Action": "s3:*",
            },
            {
                "Effect": "Allow",
                "Resource": "arn:aws:s3:::my_test_bucket/*",
                "Action": "*",
            },
            {
                "Effect": "Allow",
                "Resource": "*",
                "Action": "s3:PutBucketCORS",
            },
            {
                "Effect": "Allow",
                "Resource": "arn:aws:s3:::my_test_bucket/*",
                "Action": "s3:Put*",
            },
            {
                "Effect": "Allow",
                "Resource": "*",
                "Action": "s3:*Tags",
            },
        ],
    }
    policy_document_str = json.dumps(policy_document)
    policy_response = iam_service.create_policy(
        PolicyName="MyAccessPolicy",
        PolicyDocument=policy_document_str,
    )
    policy_arn = policy_response["Policy"]["Arn"]
    user_name = "example_user"

    iam_service.create_user(UserName=user_name)
    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy_arn)


def group_with_permissive_inline_policies(
    iam_service: IAMClient,
) -> None:
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {"Effect": "Allow", "Action": ["iam:*"], "Resource": "*"},
            {
                "Effect": "Allow",
                "Action": ["sqs:*"],
                "Resource": "arn:aws:s3:::my_test_bucket/*",
            },
        ],
    }
    policy_document_str = json.dumps(policy_document)
    group_name = "test_group"
    iam_service.create_group(GroupName=group_name)
    iam_service.put_group_policy(
        GroupName=group_name,
        PolicyName="test_policy",
        PolicyDocument=policy_document_str,
    )


def iam_is_policy_miss_configured(
    iam_service: IAMClient,
) -> None:
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "NotAction": [
                    "s3:Put",
                ],
                "NotResource": [
                    "arn:aws:s3:::my_test_bucket/test_file",
                ],
            },
        ],
    }
    policy_document_str = json.dumps(policy_document)
    policy_response = iam_service.create_policy(
        PolicyName="myPolicy",
        PolicyDocument=policy_document_str,
    )
    policy_arn = policy_response["Policy"]["Arn"]
    user_name = "example_user"
    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy_arn)


def permissive_policy(
    iam_service: IAMClient,
) -> None:
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "ssm:*",
                ],
                "Resource": "*",
            },
        ],
    }
    policy_document_str = json.dumps(policy_document)
    policy_response = iam_service.create_policy(
        PolicyName="permissivePolicy",
        PolicyDocument=policy_document_str,
    )
    policy_arn = policy_response["Policy"]["Arn"]
    user_name = "example_user"
    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy_arn)


def sqs_is_public(
    sqs_service: SQSClient,
) -> None:
    queue_policy_variations: list[dict[str, Any]] = [
        {"name": "vulnerable_queue1_of_2", "Principal": "*"},
        {"name": "vulnerable_queue2_of_2", "Principal": {"AWS": "*"}},
        {"name": "safe_queue", "Principal": {"AWS": "123456"}},
    ]
    for var in queue_policy_variations:
        name = var["name"]
        principal = var["Principal"]
        response = sqs_service.create_queue(QueueName=name)
        queue_url = response["QueueUrl"]
        attrs = sqs_service.get_queue_attributes(QueueUrl=queue_url, AttributeNames=["QueueArn"])
        queue_arn = attrs["Attributes"]["QueueArn"]
        sqs_service.set_queue_attributes(
            QueueUrl=queue_url,
            Attributes={
                "Policy": json.dumps(
                    {
                        "Version": "2012-10-17",
                        "Statement": [
                            {
                                "Effect": "Allow",
                                "Principal": principal,
                                "Action": "sqs:*",
                                "Resource": queue_arn,
                            },
                        ],
                    },
                ),
            },
        )


def main() -> None:
    s3_service = boto3.client("s3", region_name="us-east-1")
    iam_service = boto3.client("iam")
    sqs_service = boto3.client("sqs", region_name="us-east-1")
    public_buckets(s3_service)
    private_buckets_not_blocking_public_acls(s3_service)
    iam_has_wildcard_resource_on_write_action(iam_service)
    group_with_permissive_inline_policies(iam_service)
    iam_is_policy_miss_configured(iam_service)
    permissive_policy(iam_service)
    sqs_is_public(sqs_service)
