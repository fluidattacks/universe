import boto3
from mypy_boto3_rds import (
    RDSClient,
)


def rds_has_unencrypted_storage(
    rds_service: RDSClient,
) -> None:
    rds_service.create_db_instance(
        DBInstanceIdentifier="my-unencrypted-db",
        AllocatedStorage=20,
        DBInstanceClass="db.t3.micro",
        Engine="mysql",
        MasterUsername="admin",
        MasterUserPassword="password",
        BackupRetentionPeriod=7,
        StorageEncrypted=False,
    )


def main() -> None:
    rds_service = boto3.client("rds", region_name="us-east-1")
    rds_has_unencrypted_storage(rds_service)
