import boto3
from mypy_boto3_cognito_idp import (
    CognitoIdentityProviderClient,
)
from mypy_boto3_iam import (
    IAMClient,
)


def cognito_has_mfa_disabled(
    cognito_service: CognitoIdentityProviderClient,
) -> None:
    """Report vulnerabilities for methods.

    > cognito_has_mfa_disabled
    > root_without_mfa
    """
    cognito_service.create_user_pool(
        PoolName="test-user-pool",
        MfaConfiguration="OFF",
        AutoVerifiedAttributes=["email"],
    )


def cognito_mfa_disabled_external_idp(
    cognito_service: CognitoIdentityProviderClient,
) -> None:
    """Report vulnerabilities for no methods.

    This handles a third party IdPs that do not have MFA enabled.
    """
    user_pool = cognito_service.create_user_pool(
        PoolName="test-user-pool",
        MfaConfiguration="OFF",
        AutoVerifiedAttributes=["email"],
    )
    cognito_service.create_identity_provider(
        UserPoolId=user_pool["UserPool"]["Id"],
        ProviderName="Google",
        ProviderType="Google",
        ProviderDetails={
            "client_id": "test-client-id",
            "client_secret": "test-client-secret",
            "authorize_scopes": "email",
        },
        AttributeMapping={
            "email": "email",
        },
    )


def iam_has_mfa_disabled(
    iam_service: IAMClient,
) -> None:
    """Report vulnerabilities for methods.

    > mfa_disabled_for_users_with_console_password
    """
    username = "test-user-no-mfa"
    iam_service.create_user(UserName=username)

    iam_service.create_login_profile(
        UserName=username,
        Password="TestPassword123!",  # noqa: S106
        PasswordResetRequired=False,
    )

    iam_service.attach_user_policy(
        UserName=username,
        PolicyArn="arn:aws:iam::aws:policy/ReadOnlyAccess",
    )


def main() -> None:
    cognito_service = boto3.client("cognito-idp", region_name="us-east-1")
    iam_service = boto3.client("iam")
    cognito_has_mfa_disabled(cognito_service)
    cognito_mfa_disabled_external_idp(cognito_service)
    iam_has_mfa_disabled(iam_service)
