import boto3
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_eks import (
    EKSClient,
)
from mypy_boto3_elbv2 import (
    ElasticLoadBalancingv2Client,
)


def elb2_has_not_deletion_protection(
    ec2_service: EC2Client,
    elbv2_service: ElasticLoadBalancingv2Client,
    eks_service: EKSClient,
) -> None:
    vpc_response = ec2_service.create_vpc(
        CidrBlock="10.0.0.0/16",
    )
    vpc_id = vpc_response["Vpc"]["VpcId"]
    ec2_service.modify_vpc_attribute(VpcId=vpc_id, EnableDnsSupport={"Value": True})
    ec2_service.modify_vpc_attribute(VpcId=vpc_id, EnableDnsHostnames={"Value": True})

    subnet_response = ec2_service.create_subnet(
        CidrBlock="10.0.1.0/24",
        VpcId=vpc_id,
    )
    subnet_id = subnet_response["Subnet"]["SubnetId"]

    igw_response = ec2_service.create_internet_gateway()
    igw_id = igw_response["InternetGateway"]["InternetGatewayId"]
    ec2_service.attach_internet_gateway(
        InternetGatewayId=igw_id,
        VpcId=vpc_id,
    )

    route_table_response = ec2_service.create_route_table(VpcId=vpc_id)
    route_table_id = route_table_response["RouteTable"]["RouteTableId"]
    ec2_service.create_route(
        RouteTableId=route_table_id,
        DestinationCidrBlock="0.0.0.0/0",
        GatewayId=igw_id,
    )
    ec2_service.associate_route_table(
        SubnetId=subnet_id,
        RouteTableId=route_table_id,
    )

    elbv2_service.create_load_balancer(
        Name="vulnerable-alb",
        Subnets=[subnet_id],
        Scheme="internet-facing",
        Type="application",
        IpAddressType="ipv4",
    )
    eks_service.create_cluster(
        name="eks-cluster",
        roleArn="arn:aws:iam::123456789012:role/eks-cluster-role",
        resourcesVpcConfig={},
    )
    elbv2_service.create_load_balancer(
        Name="safe-alb",
        Subnets=[subnet_id],
        Scheme="internet-facing",
        Type="application",
        IpAddressType="ipv4",
        Tags=[
            {"Key": "elbv2.k8s.aws/cluster", "Value": "eks-cluster"},
        ],
    )


def main() -> None:
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    elbv2_service = boto3.client("elbv2", region_name="us-east-1")
    eks_service = boto3.client("eks")
    elb2_has_not_deletion_protection(ec2_service, elbv2_service, eks_service)
