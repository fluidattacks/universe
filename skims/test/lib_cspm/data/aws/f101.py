from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "aws_elasticache_replication_group_wo_auto_backups": {
            "ReplicationGroups": [
                {
                    "ARN": "Vulnerable 1/1",
                    "SnapshotRetentionLimit": 0,
                },
                {
                    "ARN": "Safe 1/1",
                    "SnapshotRetentionLimit": 5,
                },
            ],
        },
        "aws_elasticache_replication_backup_retention_period": {
            "ReplicationGroups": [
                {
                    "ARN": "Vulnerable 1/1",
                    "SnapshotRetentionLimit": 6,
                },
                {
                    "ARN": "Safe 1/1",
                    "SnapshotRetentionLimit": 7,
                },
            ],
        },
        "default": {},
    }
