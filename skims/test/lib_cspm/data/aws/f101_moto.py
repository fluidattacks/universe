import boto3
from mypy_boto3_rds import (
    RDSClient,
)


def aws_rds_backup_retention_period(
    rds_service: RDSClient,
) -> None:
    rds_service.create_db_instance(
        DBInstanceIdentifier="vulnerable-instance",
        Engine="mysql",
        DBInstanceClass="db.m7g",
        MasterUsername="admin",
        BackupRetentionPeriod=5,
    )
    rds_service.create_db_instance(
        DBInstanceIdentifier="safe-instance",
        Engine="mysql",
        DBInstanceClass="db.m7g",
        MasterUsername="admin",
        BackupRetentionPeriod=10,
    )
    rds_service.create_db_cluster(
        DBClusterIdentifier="vulnerable-cluster",
        Engine="aurora-mysql",
        MasterUsername="admin",
        MasterUserPassword="somepassword",
        BackupRetentionPeriod=5,
    )
    rds_service.create_db_cluster(
        DBClusterIdentifier="safe-cluster",
        Engine="aurora-mysql",
        MasterUsername="admin",
        MasterUserPassword="somapassword",
        BackupRetentionPeriod=10,
    )


def main() -> None:
    rds_service = boto3.client("rds", region_name="us-east-1")
    aws_rds_backup_retention_period(rds_service)
