import json

import boto3
import requests
from mypy_boto3_kms import (
    KMSClient,
)
from mypy_boto3_secretsmanager import (
    SecretsManagerClient,
)


def kms_key_is_key_rotation_absent_or_disabled(
    kms_service: KMSClient,
) -> None:
    response = kms_service.create_key(
        Description="Vulnerable KMS key without key rotation",
        KeyUsage="ENCRYPT_DECRYPT",
        Origin="AWS_KMS",
        BypassPolicyLockoutSafetyCheck=False,
    )

    key_id = response["KeyMetadata"]["KeyId"]
    kms_service.get_key_rotation_status(KeyId=key_id)


def secrets_manager_has_automatic_rotation_disabled(
    secretsmanager_service: SecretsManagerClient,
) -> None:
    secret_value = {"username": "myusername", "password": "mypassword"}

    response = secretsmanager_service.create_secret(
        Name="vulnerable-secret",
        Description="This is a vulnerable secret without automatic rotation",
        SecretString=json.dumps(secret_value),
        Tags=[
            {"Key": "Environment", "Value": "Production"},
        ],
    )

    secret_arn = response["ARN"]

    secretsmanager_service.describe_secret(SecretId=secret_arn)


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    kms_service = boto3.client("kms")
    secretsmanager_service = boto3.client("secretsmanager")
    kms_key_is_key_rotation_absent_or_disabled(kms_service)
    secrets_manager_has_automatic_rotation_disabled(secretsmanager_service)
