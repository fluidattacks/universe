import json

import boto3
from mypy_boto3_iam import (
    IAMClient,
)


def allows_priv_escalation_by_policies_versions(
    iam_service: IAMClient,
) -> None:
    """Report vulnerabilities for methods.

    > allows_priv_escalation_by_policies_versions
    > allows_priv_escalation_by_attach_policy
    """
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": ["iam:*"],
                "Effect": "Allow",
                "Resource": ["*"],
                "Sid": "allWriteTest1",
            },
            {
                "Action": [
                    "iam:CreatePolicyVersion",
                    "iam:SetDefaultPolicyVersion",
                    "iam:AttachUserPolicy",
                ],
                "Effect": "Allow",
                "Resource": ["*"],
                "Sid": "allWriteTest2",
            },
        ],
    }
    policy_document_str = json.dumps(policy_document)
    policy_response = iam_service.create_policy(
        PolicyName="MyAccessPolicy",
        PolicyDocument=policy_document_str,
    )
    policy_arn = policy_response["Policy"]["Arn"]
    user_name = "example_user"

    iam_service.create_user(UserName=user_name)
    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy_arn)


def main() -> None:
    iam_service = boto3.client("iam")
    allows_priv_escalation_by_policies_versions(iam_service)
