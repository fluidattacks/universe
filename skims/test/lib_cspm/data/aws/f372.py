from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "aws_cloudfront_traffic_allows_http": {
            "DistributionList": {
                "Items": [
                    {
                        "ARN": "arn:aws:cloudfront::000:distribution/abc",
                        "Origins": {
                            "Items": [
                                {
                                    "CustomOriginConfig": {
                                        "Name": "Vulnerable 1/2",
                                        "OriginProtocolPolicy": "http-only",
                                    },
                                },
                            ],
                        },
                    },
                    {
                        "ARN": "arn:aws:cloudfront::000:distribution/def",
                        "Origins": {
                            "Items": [
                                {
                                    "CustomOriginConfig": {
                                        "Name": "Vulnerable 2/2",
                                        "OriginProtocolPolicy": "match-viewer",
                                    },
                                },
                                {
                                    "CustomOriginConfig": {
                                        "Name": "Safe",
                                        "OriginProtocolPolicy": "https-only",
                                    },
                                },
                            ],
                        },
                    },
                ],
            },
        },
    }
