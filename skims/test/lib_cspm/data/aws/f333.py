from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "ec2_has_associate_public_ip_address": {
            "Account": "234234234234",
            "LaunchTemplates": [
                {
                    "LaunchTemplateId": "ltid1-018de572ae43404d8",
                },
                {
                    "LaunchTemplateId": "ltid2-018de572ae43404d8",
                },
            ],
            "LaunchTemplateVersions": [
                {
                    "LaunchTemplateId": "ltid1-018de572ae43404d8",
                    "LaunchTemplateName": "fluidtemplateunsafe",
                    "LaunchTemplateData": {
                        "NetworkInterfaces": [
                            {
                                "AssociatePublicIpAddress": True,
                            },
                        ],
                    },
                },
                {
                    "LaunchTemplateId": "ltid2-018de572ae43404d8",
                    "LaunchTemplateName": "fluidtemplateunsafe",
                    "LaunchTemplateData": {
                        "NetworkInterfaces": [
                            {
                                "AssociatePublicIpAddress": False,
                            },
                        ],
                    },
                },
            ],
        },
        "has_unencrypted_snapshots": {
            "Account": "123456789012",
            "Snapshots": [
                {
                    "Encrypted": False,
                    "SnapshotId": "snp-018de572ae43404d8",
                    "State": "pending",
                },
                {
                    "Encrypted": True,
                    "SnapshotId": "snp-018de572ae49046d8",
                    "State": "pending",
                },
            ],
        },
        "has_unencrypted_amis": {
            "Account": "234234234234",
            "Images": [
                {
                    "ImageId": "img-018de572ae43404d8",
                    "BlockDeviceMappings": [
                        {
                            "Ebs": {
                                "Encrypted": False,
                            },
                        },
                    ],
                },
            ],
        },
        "has_instances_using_unapproved_amis": {
            "Account": "234234234234",
            "Reservations": [
                {
                    "Groups": [
                        {
                            "GroupName": "mygroup",
                            "GroupId": "gr-018de572ae43404d8",
                        },
                    ],
                    "Instances": [
                        {
                            "AmiLaunchIndex": 1,
                            "InstanceId": "int-018de572ae43404d8",
                            "ImageId": "img-018de572ae43404d8",
                            "NetworkInterfaces": [
                                {
                                    "Association": {"PublicIp": "127.0.0.0"},
                                },
                            ],
                            "State": {
                                "Code": 1,
                                "Name": "pending",
                            },
                        },
                    ],
                    "OwnerId": "owner_123",
                },
            ],
            "Images": [
                {
                    "ImageOwnerAlias": "privateOwner",
                },
            ],
        },
        "aws_ec2_instance_using_imds_v1": {
            "Reservations": [
                {
                    "OwnerId": "owner_123",
                    "Instances": [
                        {
                            "InstanceId": "vulnerable 1/1",
                            "MetadataOptions": {"HttpTokens": "optional"},
                            "State": {
                                "Name": "running",
                            },
                        },
                        {
                            "InstanceId": "safe 1/1",
                            "MetadataOptions": {"HttpTokens": "required"},
                            "State": {
                                "Name": "running",
                            },
                        },
                    ],
                },
            ],
        },
    }
