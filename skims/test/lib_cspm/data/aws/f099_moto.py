import json

import boto3
from mypy_boto3_s3 import (
    S3Client,
)


def bucket_policy_has_server_side_encryption_disable(
    s3_service: S3Client,
) -> None:
    bucket_name = "vulnerable-bucket-no-sse"

    s3_service.create_bucket(
        Bucket=bucket_name,
    )

    bucket_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Condition": {
                    "Null": {
                        "s3:x-amz-server-side-encryption": "false",
                    },
                },
                "Effect": "Allow",
                "Principal": {"AWS": "111122223333"},
                "Action": [
                    "s3:GetBucketAcl",
                    "s3:GetObjectAcl",
                    "s3:PutObject",
                ],
                "Resource": [
                    "arn:aws:s3:::policytest1/*",
                ],
            },
        ],
    }

    s3_service.put_bucket_policy(Bucket=bucket_name, Policy=json.dumps(bucket_policy))


def main() -> None:
    s3_service = boto3.client("s3")
    bucket_policy_has_server_side_encryption_disable(s3_service)
