from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "aws_cloudfront_is_not_protected_with_waf": {
            "DistributionList": {
                "Items": [
                    {
                        "ARN": """
                            arn:aws:cloudfront::00:distribution/vulnerable 1/1
                        """,
                        "WebACLId": "",
                    },
                    {
                        "ARN": "arn:aws:cloudfront::00:distribution/safe 1/1",
                        "WebACLId": """
                            arn:aws:wafv2:us-east-1:0000:global/webacl/webaclID
                        """,
                    },
                ],
            },
        },
        "default": {},
    }
