import boto3
import requests
from mypy_boto3_ec2 import (
    EC2Client,
)


def use_default_security_group(
    ec2_service: EC2Client,
) -> None:
    key_name = "my-key-pair"
    response = ec2_service.describe_security_groups(
        Filters=[{"Name": "group-name", "Values": ["default"]}],
    )
    default_sg_id = response["SecurityGroups"][0]["GroupId"]

    ec2_service.run_instances(
        ImageId="ami-0abcdef1234567890",
        InstanceType="t2.micro",
        MinCount=1,
        MaxCount=1,
        KeyName=key_name,
        SecurityGroupIds=[default_sg_id],
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    use_default_security_group(ec2_service)
