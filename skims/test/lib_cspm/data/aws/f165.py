import json
from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    content = b"arn,access_key_1_active,access_key_2_active,cert_1_active\nmyUser,true,true,true"
    return {
        "has_not_support_role": {
            "Account": "234234234234",
            "PolicyGroups": [],
            "PolicyUsers": [],
            "PolicyRoles": [],
        },
        "has_root_active_signing_certificates": {
            "Content": content,
        },
        "dynamob_encrypted_with_aws_master_keys": {
            "TableNames": [
                "fluidTable",
            ],
            "Table": {
                "TableName": "fluidTable",
                "TableArn": "arn:aws:iam::aws:table/fluidTable",
                "SSEDescription": {
                    "Status": "ENABLED",
                    "SSEType": "AES256",
                    "KMSMasterKeyArn": "arn:aws:iam::aws:key/fuildKey",
                },
            },
        },
        "redshift_has_public_clusters": {
            "Account": "234234234234",
            "Clusters": [
                {
                    "Name": "fluidCluster",
                    "ClusterIdentifier": "fluidCluster123",
                    "ClusterNamespaceArn": ("arn:aws:iam::123456789012:Cluster/FC1"),
                    "PubliclyAccessible": True,
                    "ClusterParameterGroups": [
                        {
                            "ParameterGroupName": "fluidParamGroup1",
                        },
                    ],
                },
            ],
        },
        "redshift_not_requires_ssl": {
            "Account": "234234234234",
            "Clusters": [
                {
                    "Name": "fluidCluster",
                    "ClusterIdentifier": "fluidCluster123",
                    "ClusterNamespaceArn": ("arn:aws:iam::123456789012:Cluster/FC1"),
                    "PubliclyAccessible": True,
                    "ClusterParameterGroups": [
                        {
                            "ParameterGroupName": "fluidParamGroup1",
                        },
                    ],
                },
            ],
            "Parameters": [
                {
                    "ParameterName": "require_ssl",
                    "ParameterValue": "false",
                },
            ],
        },
        "elasticache_uses_default_port": {
            "CacheClusters": [
                {
                    "CacheClusterId": "fluidCacheCluster1",
                    "ARN": "arn:aws:fluidCacheCluster/1234",
                    "Engine": "memcached",
                    "ConfigurationEndpoint": {
                        "Address": "-1",
                        "Port": 11211,
                    },
                },
            ],
        },
        "acm_certificate_expired": {
            "CertificateSummaryList": [
                {
                    "CertificateArn": "arn:aws:acm:us-east-1:0:certificate/ab",
                    "Status": "EXPIRED",
                },
            ],
        },
        "api_gateway_cache_encryption_disabled": {
            "items": [
                {"id": "1234"},
            ],
            "item": [
                {
                    "stageName": "Vulnerable 1/1",
                    "cacheClusterEnabled": True,
                    "cacheClusterStatus": "AVAILABLE",
                    "methodSettings": {
                        "*/*": {
                            "cacheDataEncrypted": False,
                        },
                    },
                },
                {
                    "stageName": "Safe 1/3",
                    "cacheClusterEnabled": True,
                    "cacheClusterStatus": "AVAILABLE",
                    "methodSettings": {
                        "*/*": {
                            "cacheDataEncrypted": True,
                        },
                    },
                },
                {
                    "stageName": "Safe 2/3",
                    "cacheClusterEnabled": False,
                    "cacheClusterStatus": "AVAILABLE",
                    "methodSettings": {
                        "*/*": {
                            "cacheDataEncrypted": False,
                        },
                    },
                },
                {
                    "stageName": "Safe 3/3",
                    "cacheClusterEnabled": True,
                    "cacheClusterStatus": "NOT_AVAILABLE",
                    "methodSettings": {
                        "*/*": {
                            "cacheDataEncrypted": False,
                        },
                    },
                },
            ],
        },
        "backup_vault_policy_allow_delete_recovery_points": {
            "BackupVaultList": [
                {
                    "BackupVaultName": "vulnerable_1_of_1",
                    "BackupVaultArn": "arn:...:vulnerable_1_of_1",
                },
            ],
            "Policy": json.dumps(
                {
                    "Statement": [
                        {
                            "Effect": "Deny",
                            "Principal": {"AWS": "*"},
                            "Action": ["backup:CopyIntoBackupVault"],
                            "Resource": "*",
                        },
                    ],
                },
            ),
        },
        "aws_bedrock_guardrails_no_sensitive_info_filter": {
            "guardrails": [
                {
                    "id": "vulnerable 1/1",
                    "arn": "arn:aws:bedrock:us-east-1:account-id:guardrail",
                },
            ],
        },
        "aws_event_bridge_default_event_bus_exposed": {
            "Arn": "arn:aws:events:us-east-2:0000:event-bus/default",
            "Policy": json.dumps(
                {
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": "*",
                            "Action": "*",
                            "Resource": "arn:aws:events:us-east-2:*",
                        },
                    ],
                },
            ),
        },
        "aws_lambda_url_without_authentication": {
            "Functions": [{"FunctionArn": "arn", "FunctionName": "Vulnerable 1/1"}],
            "FunctionUrlConfigs": [{"AuthType": "NONE"}],
        },
        "aws_lambda_function_exposed": {
            "Functions": [{"FunctionArn": "arn", "FunctionName": "Vulnerable 1/1"}],
            "Policy": json.dumps(
                {
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {"AWS": "*"},
                            "Action": "*",
                            "Resource": "arn",
                        },
                    ],
                },
            ),
        },
        "aws_comprehend_analysis_without_encryption": {
            "EntitiesDetectionJobPropertiesList": [{"OutputDataConfig": {}, "JobArn": "arn"}],
        },
        "aws_msk_cluster_is_publicly_accessible": {
            "ClusterInfoList": [
                {
                    "ClusterType": "provisioned",
                    "ClusterArn": "arn:aws:kafka:us-east-1:123:cluster/abc",
                },
            ],
            "ClusterInfo": {
                "BrokerNodeGroupInfo": {
                    "ConnectivityInfo": {"PublicAccess": {"Type": "SERVICE_PROVIDED_EIPS"}},
                },
            },
            "aws_route53_transfer_lock_disabled": {
                "Domains": [{"DomainName": "vulnerable.com"}],
                "StatusList": ["OK"],
            },
        },
        "aws_athena_workgroup_query_results_not_encrypted": {
            "WorkGroups": [
                {
                    "Name": "vulnerable-workgroup",
                },
            ],
            "QueryExecutionIds": ["132jldfadcal"],
            "WorkGroup": {
                "Name": "vulnerable-workgroup",
                "Arn": "arn:aws:athena:us-east-1:1:workgroup/vulnerable-wg",
                "State": "ENABLED",
                "Configuration": {
                    "ResultConfiguration": {},
                },
            },
        },
        "aws_unencrypted_ecr_repository": {
            "repositories": [
                {
                    "repositoryArn": ("arn:aws:ecr:us-east-1:1:repository/vulnerable-repo"),
                    "encryptionConfiguration": {},
                },
            ],
        },
        "aws_rds_unencrypted_db_cluster_snapshot": {
            "DBClusterSnapshots": [
                {
                    "DBClusterSnapshotArn": (
                        "arn:aws:rds:us-east-1:1:cluster-snapshot:vulnerabale-snapshot"
                    ),
                    "StorageEncrypted": False,
                },
                {
                    "DBClusterSnapshotArn": (
                        "arn:aws:rds:us-east-1:1:cluster-snapshot:safe-snapshot"
                    ),
                    "StorageEncrypted": True,
                },
            ],
        },
        "aws_public_accessible_dms_replication": {
            "ReplicationInstances": [
                {
                    "ReplicationInstanceArn": "arn:aws:rds:us-east-1:1:"
                    "vulnerable-dms-replication-instance",
                    "PubliclyAccessible": True,
                },
                {
                    "ReplicationInstanceArn": "arn:aws:rds:us-east-1:1:"
                    "safe-dms-replication-instance",
                    "PubliclyAccessible": False,
                },
            ],
        },
    }
