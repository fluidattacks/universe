import os
import time
from typing import TYPE_CHECKING

import boto3
import requests
from freezegun import (
    freeze_time,
)
from mypy_boto3_cloudfront import (
    CloudFrontClient,
)
from mypy_boto3_cloudtrail import (
    CloudTrailClient,
)
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_eks import (
    EKSClient,
)
from mypy_boto3_elbv2 import (
    ElasticLoadBalancingv2Client,
)
from mypy_boto3_mq import (
    MQClient,
)
from mypy_boto3_mq.type_defs import (
    LogsTypeDef,
)
from mypy_boto3_opensearch import (
    OpenSearchServiceClient,
)
from mypy_boto3_rds import (
    RDSClient,
)
from mypy_boto3_route53 import (
    Route53Client,
)
from mypy_boto3_s3 import (
    S3Client,
)

if TYPE_CHECKING:
    from mypy_boto3_cloudfront.type_defs import (
        DistributionConfigTypeDef,
    )

os.environ["TZ"] = "UTC"
time.tzset()


def is_trail_bucket_logging_disabled(
    cloudtrail_service: CloudTrailClient,
    s3_service: S3Client,
) -> None:
    """Report vulnerabilities for methods.

    > is_trail_bucket_logging_disabled
    > s3_has_server_access_logging_disabled,
    """
    bucket_name = "vulnerable-cloudtrail-bucket"
    s3_service.create_bucket(
        Bucket=bucket_name,
    )
    cloudtrail_service.create_trail(
        Name="vulnerable-cloudtrail-trail",
        S3BucketName=bucket_name,
        IsMultiRegionTrail=True,
        IncludeGlobalServiceEvents=True,
        EnableLogFileValidation=True,
    )

    cloudtrail_service.start_logging(Name="vulnerable-cloudtrail-trail")


def elbv2_has_access_logging_disabled(
    elbv2_service: ElasticLoadBalancingv2Client,
    ec2_service: EC2Client,
) -> None:
    """Report vulnerabilities for methods.

    > elbv2_has_access_logging_disabled
    > vpcs_without_flowlog,
    """
    response_vpc = ec2_service.create_vpc(CidrBlock="10.0.0.0/16")
    vpc_id = response_vpc["Vpc"]["VpcId"]

    response_subnet_1 = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.1.0/24",
    )
    subnet_1_id = response_subnet_1["Subnet"]["SubnetId"]

    response_subnet_2 = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.2.0/24",
    )
    subnet_2_id = response_subnet_2["Subnet"]["SubnetId"]

    response_sg = ec2_service.create_security_group(
        GroupName="vulnerable-alb-sg",
        Description="Security group for vulnerable ALB",
        VpcId=vpc_id,
    )
    security_group_id = response_sg["GroupId"]

    ec2_service.authorize_security_group_ingress(
        GroupId=security_group_id,
        IpPermissions=[
            {
                "IpProtocol": "tcp",
                "FromPort": 80,
                "ToPort": 80,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
            {
                "IpProtocol": "tcp",
                "FromPort": 443,
                "ToPort": 443,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
        ],
    )
    elbv2_service.create_load_balancer(
        Name="vulnerable-alb",
        Subnets=[subnet_1_id, subnet_2_id],
        SecurityGroups=[security_group_id],
        Scheme="internet-facing",
        Tags=[
            {"Key": "Name", "Value": "vulnerable-alb"},
            {"Key": "Environment", "Value": "Production"},
        ],
        Type="application",
        IpAddressType="ipv4",
    )


def cloudfront_has_logging_disabled(
    cloudfront_service: CloudFrontClient,
) -> None:
    distribution_config: DistributionConfigTypeDef = {
        "CallerReference": "unique-string",
        "Comment": "CloudFront distribution without logging",
        "Enabled": True,
        "Origins": {
            "Quantity": 1,
            "Items": [
                {
                    "Id": "1",
                    "DomainName": ("vulnerable-cloudfront-origin-bucket.s3.amazonaws.com"),
                    "S3OriginConfig": {"OriginAccessIdentity": ""},
                },
            ],
        },
        "DefaultCacheBehavior": {
            "TargetOriginId": "1",
            "ViewerProtocolPolicy": "allow-all",
            "TrustedSigners": {"Enabled": False, "Quantity": 0},
            "ForwardedValues": {
                "QueryString": False,
                "Cookies": {"Forward": "none"},
            },
            "MinTTL": 3600,
            "AllowedMethods": {
                "Quantity": 2,
                "Items": ["HEAD", "GET"],
                "CachedMethods": {"Quantity": 2, "Items": ["HEAD", "GET"]},
            },
        },
        "ViewerCertificate": {"CloudFrontDefaultCertificate": True},
        "Logging": {
            "Enabled": False,
            "Bucket": "",
            "Prefix": "",
            "IncludeCookies": False,
        },
    }

    cloudfront_service.create_distribution(DistributionConfig=distribution_config)


def cloudtrail_trails_not_multiregion(
    cloudtrail_service: CloudTrailClient,
) -> None:
    bucket_name = "vulnerable-cloudtrail-bucket"
    cloudtrail_service.create_trail(
        Name="vulnerable-cloudtrail-trail",
        S3BucketName=bucket_name,
        IsMultiRegionTrail=False,
        IncludeGlobalServiceEvents=True,
        EnableLogFileValidation=True,
    )

    cloudtrail_service.stop_logging(Name="vulnerable-cloudtrail-trail")


def cloudtrail_logging_disabled(
    s3_service: S3Client,
    cloudtrail_service: CloudTrailClient,
) -> None:
    bucket_name = "vulnerable-cloudtrail-logs-bucket"
    s3_service.create_bucket(
        Bucket=bucket_name,
    )
    cloudtrail_service.create_trail(
        Name="vulnerable-cloudtrail-trail",
        S3BucketName=bucket_name,
        IncludeGlobalServiceEvents=True,
        IsMultiRegionTrail=False,
        EnableLogFileValidation=True,
        KmsKeyId="",
    )

    cloudtrail_service.stop_logging(Name="vulnerable-cloudtrail-trail")


def aws_config_service(s3_service: S3Client) -> None:
    """Report vulnerabilities for methods.

    > aws_config_referencing_missing_s3_bucket,
    """
    # Safe config
    s3_service.create_bucket(Bucket="config_service_bucket")
    ca_central_1_config_svc = boto3.client("config", region_name="us-east-1")
    ca_central_1_config_svc.put_configuration_recorder(
        ConfigurationRecorder={
            "name": "configuration-recorder",
            "roleARN": "arn:aws:iam::111122223333:role/role",
        },
    )
    ca_central_1_config_svc.put_delivery_channel(
        DeliveryChannel={
            "name": "vulnerable-delivery-channel",
            "s3BucketName": "config_service_bucket",
        },
    )
    # Vulnerable config
    us_east_1_config_svc = boto3.client("config", region_name="us-east-1")
    us_east_1_config_svc.put_configuration_recorder(
        ConfigurationRecorder={
            "name": "configuration-recorder",
            "roleARN": "arn:aws:iam::111122223333:role/role",
        },
    )
    us_east_1_config_svc.put_delivery_channel(
        DeliveryChannel={
            "name": "vulnerable-delivery-channel",
            "s3BucketName": "some-missing-bucket",
        },
    )


def eks_mock(
    eks_service: EKSClient,
) -> None:
    """Report vulnerabilities for methods.

    > aws_eks_cluster_logging_disabled
    """
    eks_service.create_cluster(
        name="vulnerable-cluster",
        version="1.21",
        roleArn="arn:aws:iam::123456789012:role/eks-service-role",
        resourcesVpcConfig={
            "subnetIds": ["subnet-12345678", "subnet-87654321"],
            "endpointPublicAccess": False,
            "endpointPrivateAccess": False,
        },
        logging={
            "clusterLogging": [
                {
                    "types": [
                        "api",
                        "audit",
                        "authenticator",
                    ],
                    "enabled": False,
                },
                {
                    "types": [
                        "controllerManager",
                        "scheduler",
                    ],
                    "enabled": True,
                },
            ],
        },
    )
    eks_service.create_cluster(
        name="safe-cluster",
        version="1.21",
        roleArn="arn:aws:iam::123456789012:role/eks-service-role",
        resourcesVpcConfig={
            "subnetIds": ["subnet-12345678", "subnet-87654321"],
            "endpointPublicAccess": True,
            "endpointPrivateAccess": False,
        },
        logging={
            "clusterLogging": [
                {
                    "types": [
                        "api",
                        "audit",
                        "authenticator",
                        "controllerManager",
                        "scheduler",
                    ],
                    "enabled": True,
                },
            ],
        },
    )


def opensearch_mock(opensearch_service: OpenSearchServiceClient) -> None:
    """Report vulnerabilities for methods.

    > aws_opensearch_without_audit_logs
    """
    opensearch_service.create_domain(DomainName="vulnerable_domain")
    opensearch_service.create_domain(
        DomainName="safe_domain",
        LogPublishingOptions={"AUDIT_LOGS": {"Enabled": True}},
    )


@freeze_time("2024-10-03 00:00:00-00:00")
def aws_mq_broker_logs_disabled(
    mq_service: MQClient,
) -> None:
    def create_broker(broker_name: str, logs: LogsTypeDef) -> None:
        mq_service.create_broker(
            BrokerName=broker_name,
            PubliclyAccessible=True,
            EngineType="ACTIVEMQ",
            EngineVersion="5.18.4",
            HostInstanceType="mq.t3.micro",
            AutoMinorVersionUpgrade=True,
            DeploymentMode="ACTIVE_STANDBY_MULTI_AZ",
            Logs=logs,
            Users=[
                {
                    "ConsoleAccess": True,
                    "Username": "user",
                    "Password": "password",
                },
            ],
        )

    create_broker(
        broker_name="vulnerable_broker_1_3",
        logs={"Audit": False, "General": False},
    )
    create_broker(
        broker_name="vulnerable_broker_2_3",
        logs={"Audit": True, "General": False},
    )
    create_broker(
        broker_name="vulnerable_broker_3_3",
        logs={"Audit": False, "General": True},
    )
    create_broker(broker_name="safe_broker_1_1", logs={"Audit": True, "General": True})


def aws_route53_dns_query_logging_disabled(
    route53_service: Route53Client,
) -> None:
    route53_service.create_hosted_zone(
        Name="vulnerable-hosted-zone",
        CallerReference="vulnerable-hosted-zone",
        HostedZoneConfig={
            "Comment": "Comment",
            "PrivateZone": False,
        },
    )


def aws_rds_db_cluster_logs_disabled(rds_service: RDSClient) -> None:
    rds_service.create_db_cluster(
        DBClusterIdentifier="vulnerable-rds-cluster",
        Engine="aurora-mysql",
        MasterUsername="admin",
        MasterUserPassword="password123",
    )
    rds_service.create_db_cluster(
        DBClusterIdentifier="safe-rds-cluster",
        Engine="aurora-mysql",
        MasterUsername="admin",
        MasterUserPassword="password123",
        EnableCloudwatchLogsExports=["postgresql"],
    )


def rds_and_neptune_mock(rds_service: RDSClient) -> None:
    """Report vulnerabilities for methods.

    > aws_rds_db_instance_logs_disabled(rds_service
    > aws_neptune_db_instance_logs_disabled
    """
    rds_service.create_db_instance(
        DBInstanceIdentifier="vulnerable-rds-cluster",
        DBInstanceClass="db.m5.large",
        Engine="mysql",
        MasterUsername="admin",
        MasterUserPassword="password123",
    )
    rds_service.create_db_instance(
        DBInstanceIdentifier="safe-rds-cluster",
        DBInstanceClass="db.m5.large",
        Engine="mysql",
        MasterUsername="admin",
        MasterUserPassword="password123",
        EnableCloudwatchLogsExports=["audit"],
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    cloudtrail_service = boto3.client("cloudtrail", region_name="us-east-2")
    s3_service = boto3.client("s3")
    elbv2_service = boto3.client("elbv2", region_name="us-east-1")
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    cloudfront_service = boto3.client("cloudfront")
    eks_service = boto3.client("eks")
    opensearch_service = boto3.client("opensearch")
    mq_service = boto3.client("mq")
    route53_service = boto3.client("route53")
    rds_service = boto3.client("rds")
    is_trail_bucket_logging_disabled(cloudtrail_service, s3_service)
    elbv2_has_access_logging_disabled(elbv2_service, ec2_service)
    cloudfront_has_logging_disabled(cloudfront_service)
    cloudtrail_trails_not_multiregion(cloudtrail_service)
    cloudtrail_logging_disabled(s3_service, cloudtrail_service)
    aws_config_service(s3_service=s3_service)
    eks_mock(eks_service)
    opensearch_mock(opensearch_service)
    aws_mq_broker_logs_disabled(mq_service)
    aws_route53_dns_query_logging_disabled(route53_service)
    aws_rds_db_cluster_logs_disabled(rds_service)
    rds_and_neptune_mock(rds_service)
