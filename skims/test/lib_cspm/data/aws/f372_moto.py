from typing import TYPE_CHECKING

import boto3
import requests
from mypy_boto3_cloudfront import (
    CloudFrontClient,
)
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_elbv2 import (
    ElasticLoadBalancingv2Client,
)
from mypy_boto3_opensearch import (
    OpenSearchServiceClient,
)

if TYPE_CHECKING:
    from mypy_boto3_cloudfront.type_defs import (
        DistributionConfigTypeDef,
    )


def cloudfront_mocks(
    cloudfront_service: CloudFrontClient,
) -> None:
    """Report vulnerabilities for methods.

    > cft_serves_content_over_http
    > aws_cloudfront_distribution_viewer_policy_allows_http
    """
    distribution_config: DistributionConfigTypeDef = {
        "CallerReference": "unique-reference-12345",
        "Comment": "Distribution vulnerable to serving content over HTTP",
        "Origins": {
            "Quantity": 1,
            "Items": [
                {
                    "Id": "1",
                    "DomainName": "example.com",
                    "CustomOriginConfig": {
                        "HTTPPort": 80,
                        "HTTPSPort": 443,
                        "OriginProtocolPolicy": "match-viewer",
                    },
                },
            ],
        },
        "DefaultCacheBehavior": {
            "TargetOriginId": "1",
            "ViewerProtocolPolicy": "allow-all",
            "ForwardedValues": {
                "QueryString": False,
                "Cookies": {"Forward": "none"},
            },
            "TrustedSigners": {"Enabled": False, "Quantity": 0},
            "MinTTL": 0,
        },
        "Enabled": True,
    }

    cloudfront_service.create_distribution(DistributionConfig=distribution_config)


def mock_alb(
    elbv2_service: ElasticLoadBalancingv2Client,
    ec2_service: EC2Client,
) -> None:
    """Report vulnerabilities for methods.

    > aws_alb_http_not_redirected_to_https
    > elbv2_listeners_not_using_https
    """
    response_vpc = ec2_service.create_vpc(CidrBlock="10.0.0.0/16")
    vpc_id = response_vpc["Vpc"]["VpcId"]

    response_subnet_1 = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.1.0/24",
    )
    subnet_1_id = response_subnet_1["Subnet"]["SubnetId"]

    response_subnet_2 = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.2.0/24",
    )
    subnet_2_id = response_subnet_2["Subnet"]["SubnetId"]

    response_sg = ec2_service.create_security_group(
        GroupName="vulnerable-alb-sg",
        Description="Security group for vulnerable ALB",
        VpcId=vpc_id,
    )
    security_group_id = response_sg["GroupId"]

    ec2_service.authorize_security_group_ingress(
        GroupId=security_group_id,
        IpPermissions=[
            {
                "IpProtocol": "tcp",
                "FromPort": 80,
                "ToPort": 80,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
            {
                "IpProtocol": "tcp",
                "FromPort": 22,
                "ToPort": 22,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
        ],
    )

    response = elbv2_service.create_load_balancer(
        Name="vulnerable-alb",
        Subnets=[
            subnet_1_id,
            subnet_2_id,
        ],
        SecurityGroups=[security_group_id],
        Scheme="internet-facing",
        Tags=[
            {"Key": "Name", "Value": "vulnerable-alb"},
        ],
        Type="application",
        IpAddressType="ipv4",
    )

    load_balancer_arn = response["LoadBalancers"][0]["LoadBalancerArn"]

    elbv2_service.create_listener(
        LoadBalancerArn=load_balancer_arn,
        Protocol="HTTP",
        Port=80,
        DefaultActions=[
            {
                "Type": "fixed-response",
                "FixedResponseConfig": {
                    "MessageBody": "This is a vulnerable listener.",
                    "StatusCode": "200",
                    "ContentType": "text/plain",
                },
            },
        ],
    )


def mock_opensearch(opensearch_service: OpenSearchServiceClient) -> None:
    """Report vulnerabilities for methods.

    > aws_opensearch_domain_allows_http
    """
    opensearch_service.create_domain(DomainName="vulnerable_domain")
    opensearch_service.create_domain(
        DomainName="safe_domain",
        DomainEndpointOptions={"EnforceHTTPS": True},
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    cloudfront_service = boto3.client("cloudfront", region_name="us-east-1")
    elbv2_service = boto3.client("elbv2", region_name="us-east-1")
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    opensearch_service = boto3.client("opensearch")
    cloudfront_mocks(cloudfront_service)
    mock_alb(elbv2_service, ec2_service)
    mock_opensearch(opensearch_service)
