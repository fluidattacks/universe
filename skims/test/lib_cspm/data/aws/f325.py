from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "kms_has_master_keys_exposed_to_everyone": {
            "Account": "234234234234",
            "Aliases": [
                {
                    "AliasName": "myAlias",
                    "AliasArn": "arn:aws:iam::123456789012:alias/",
                    "TargetKeyId": "mykeyId",
                },
            ],
            "PolicyNames": [
                "myPolicy",
            ],
            "Policy": "{'Statement': [{'Principal': {'AWS': '*'},},],}",
        },
    }
