{ inputs, makeSearchPaths, makeScript, managePorts, outputs, ... }:
let
  httpMockEnv = makeSearchPaths {
    bin = [ inputs.nixpkgs.python311Packages.flask ];
    source = [ managePorts ];
    pythonPackage = [ inputs.nixpkgs.python311Packages.flask ];
  };
in {
  jobs."/skims/test/mocks/http" = makeScript {
    name = "skims-test-mocks-http";
    replace = { __argApp__ = ./src; };
    entrypoint = ./entrypoint.sh;
    searchPaths.source = [ httpMockEnv ];
  };
}
