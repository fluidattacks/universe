# noqa: INP001
from collections.abc import (
    Callable,
)
from unittest.mock import (
    patch,
)

import pytest

from test.utils import (
    _default_snippet_filter,
    check_that_csv_results_match,
    get_suite_config,
    skims,
)

CONFIG_PATH = "skims/test/lib_dast/test_configs"
RESULTS_PATH = "skims/test/lib_dast/results"


@pytest.mark.skims_test_group("dast_http_findings")
@pytest.mark.usefixtures("test_mocks_http")
def test_dast_http_findings() -> None:
    def snippet_filter(snippet: str | None) -> str:
        if not snippet:
            snippet = ""
        return "\n".join(line for line in snippet.splitlines() if snippet and "< Date:" not in line)

    run_dast_group("lib_http", CONFIG_PATH, RESULTS_PATH, snippet_filter)


@pytest.mark.skims_test_group("dast_ssl_findings")
@pytest.mark.usefixtures("test_mocks_ssl_safe", "test_mocks_ssl_unsafe")
def test_dast_ssl_findings() -> None:
    run_dast_group("lib_ssl", CONFIG_PATH, RESULTS_PATH)


def run_dast_group(
    suite: str,
    config_path: str,
    results_path: str,
    snippet_filter: Callable[[str], str] = _default_snippet_filter,
) -> None:
    with patch("lib_cspm.aws.analyze.run_boto3_fun", return_value={}):
        code, stdout, _ = skims("scan", get_suite_config(suite, config_path))
        assert code == 0, stdout
        assert "[INFO] Startup work dir is:" in stdout
        assert "[INFO] An output file has been written:" in stdout
        check_that_csv_results_match(suite, results_path, snippet_filter=snippet_filter)
