# noqa: INP001
from datetime import UTC, datetime, timedelta
from typing import (
    Any,
    NamedTuple,
)
from unittest.mock import (
    MagicMock,
    patch,
)

import pytest
from cryptography import (
    x509,
)
from freezegun import (
    freeze_time,
)
from lib_dast.methods_enum_ssl import (
    MethodsEnumSSL,
)
from lib_dast.ssl.analyze_certificate import (
    _get_certificate_supported_names,
    _is_wildcard_certificate,
    ssl_certificate_vulns,
)
from lib_dast.ssl.model import (
    SSLCtxt,
)
from model.core import (
    Vulnerabilities,  # noqa: TC002
)


class MockExtensionValue(NamedTuple):
    values: list[str]

    def __str__(self) -> str:
        return self.values[0]

    def get_values_for_type(self, *_: Any) -> list[str]:  # noqa: ANN401
        return self.values


class MockExtension(NamedTuple):
    value: MockExtensionValue


class MockCertificate(NamedTuple):
    alternative_names: str
    subject: str
    issuer: str
    not_valid_after_utc: datetime = datetime.now(tz=UTC) + timedelta(hours=2)

    def __str__(self) -> str:
        return self.alternative_names


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.parametrize(
    ("host", "cert_name", "expected"),
    [
        ("example.com", "*.hello*.com", False),
        ("example.com", "example.*.com", False),
        ("docs.example.com", "example1.com", False),
        ("internal.docs.example.com", "*.example.com", False),
        ("docs.example.com", "*.example.com", True),
    ],
)
def test_wildcard_with_vuln(*, host: str, cert_name: str, expected: bool) -> None:
    result = _is_wildcard_certificate(host, cert_name)
    assert result == expected


@pytest.mark.skims_test_group("all_unittesting")
def test_get_domain_names() -> None:
    mock_x509 = MagicMock(spec=x509.Certificate)
    mock_x509.subject.get_attributes_for_oid.return_value = [
        MockExtension(
            value=MockExtensionValue(values=["test.com"]),
        ),
    ]
    mock_x509.extensions.get_extension_for_oid.return_value = MockExtension(
        value=MockExtensionValue(values=["*.test.com", "help.test.com", "www.test.org"]),
    )
    result = _get_certificate_supported_names(mock_x509)

    assert result == ["test.com", "*.test.com", "help.test.com", "www.test.org"]


@pytest.mark.skims_test_group("all_unittesting")
@freeze_time("2024-10-01 12:00:00")
def test_ssl_expired_certificate() -> None:
    mock_ssl_ctx = SSLCtxt(
        host="help.fluidattacks.com",
        port=443,
    )
    mock_certificate = MockCertificate(
        alternative_names="DNS:localhost.test.com",
        subject="help.fluidattacks.com",
        issuer="ssllabs",
        not_valid_after_utc=datetime.now(tz=UTC) - timedelta(hours=2),
    )
    with (
        patch(
            "lib_dast.ssl.analyze_certificate._get_server_certificate",
            return_value=mock_certificate,
        ),
        patch(
            "lib_dast.ssl.analyze_certificate._get_certificate_supported_names",
            return_value=["help.fluidattacks.com"],
        ),
    ):
        vulns = ssl_certificate_vulns(mock_ssl_ctx)

        assert len(vulns) == 1
        ssl_vuln = vulns[0]
        assert (
            ssl_vuln.skims_metadata.source_method
            == MethodsEnumSSL.SSL_CERTIFICATE_EXPIRED.value.get_name()
        )
        assert ssl_vuln.extract_path == "https://help.fluidattacks.com"
        assert isinstance(ssl_vuln.skims_metadata.snippet, str)
        assert "Certificate validation" in ssl_vuln.skims_metadata.snippet
        assert "Preparing connection with domain:" in ssl_vuln.skims_metadata.snippet
        assert "Checking X509 certificate for vulnerabilities" in ssl_vuln.skims_metadata.snippet


@pytest.mark.skims_test_group("all_unittesting")
def test_ssl_not_expired_certificate() -> None:
    mock_ssl_ctx = SSLCtxt(
        host="help.fluidattacks.com",
        port=443,
    )

    mock_alternative_names = MockCertificate(
        alternative_names="DNS:localhost.test.com",
        subject="help.fluidattacks.com",
        issuer="ssllabs",
    )
    with (
        patch(
            "lib_dast.ssl.analyze_certificate._get_server_certificate",
            return_value=mock_alternative_names,
        ),
        patch(
            "lib_dast.ssl.analyze_certificate._get_certificate_supported_names",
            return_value=["help.fluidattacks.com"],
        ),
    ):
        vulns = ssl_certificate_vulns(mock_ssl_ctx)

        assert len(vulns) == 0


@pytest.mark.skims_test_group("all_unittesting")
def test_ssl_self_signed_certificate() -> None:
    mock_ssl_ctx = SSLCtxt(
        host="help.fluidattacks.com",
        port=443,
    )

    mock_alternative_names = MockCertificate(
        alternative_names="DNS:localhost.test.com",
        subject="help.fluidattacks.com",
        issuer="help.fluidattacks.com",
    )
    with (
        patch(
            "lib_dast.ssl.analyze_certificate._get_server_certificate",
            return_value=mock_alternative_names,
        ),
        patch(
            "lib_dast.ssl.analyze_certificate._get_certificate_supported_names",
            return_value=["help.fluidattacks.com"],
        ),
    ):
        vulns = ssl_certificate_vulns(mock_ssl_ctx)

    assert len(vulns) == 1


@pytest.mark.skims_test_group("all_unittesting")
@patch("lib_dast.ssl.analyze_certificate._get_server_certificate")
@patch("lib_dast.ssl.analyze_certificate._get_certificate_supported_names")
def test_ssl_no_vuln(
    mock_alternative_names: MagicMock,
    mock_server_certificate: MagicMock,
) -> None:
    ssl_ctx = SSLCtxt(
        host="test.help.fluidattacks.com",
        port=443,
    )
    mock_alternative_names.return_value = ["test.com", "example.com"]
    mock_certificate = MockCertificate(
        alternative_names="DNS:localhost.test.com",
        subject="help.fluidattacks.com",
        issuer="ssllabs",
    )
    mock_server_certificate.return_value = mock_certificate
    vulns: Vulnerabilities = ssl_certificate_vulns(ssl_ctx)
    assert len(vulns) == 1
    assert vulns[0].skims_metadata.source_method == MethodsEnumSSL.SSL_WRONG_CN.value.get_name()

    mock_alternative_names.return_value = [
        "fluidattacks.com",
        "*.fluidattacks.com",
        "*.test.fluidattacks.com",
    ]
    vulns_2: Vulnerabilities = ssl_certificate_vulns(ssl_ctx)
    assert len(vulns_2) == 1
    assert vulns_2[0].skims_metadata.source_method == MethodsEnumSSL.SSL_WRONG_CN.value.get_name()

    mock_alternative_names.return_value = [
        "fluidattacks.com",
        "*.help.fluidattacks.com",
    ]
    vulns_3: Vulnerabilities = ssl_certificate_vulns(ssl_ctx)
    assert len(vulns_3) == 1
    assert (
        vulns_3[0].skims_metadata.source_method
        == MethodsEnumSSL.SSL_WILDCARD_CERTIFICATE.value.get_name()
    )

    mock_alternative_names.return_value = [
        "fluidattacks.com",
        "test.help.fluidattacks.com",
    ]
    vulns_4: Vulnerabilities = ssl_certificate_vulns(ssl_ctx)
    assert len(vulns_4) == 0
