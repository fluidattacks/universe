# noqa: INP001
from datetime import (
    datetime,
)
from urllib.parse import (
    urlparse,
)

import pytest
from bs4 import (
    BeautifulSoup,
)
from lib_dast.http.analyze import (
    _check_url_components,
    _remove_duplicate_redirected_urls,
)
from lib_dast.http.model import (
    URLContext,
)


@pytest.mark.skims_test_group("all_unittesting")
def test_check_url_components() -> None:
    original_urls = {"https://localhost:4000", "https://originalurl.com"}
    # Check url to same url
    assert (
        _check_url_components("https://localhost:4000", "https://localhost:4000", original_urls)
        is not None
    )
    # Check http redirected to https
    assert (
        _check_url_components(
            "http://localhost:4000",
            "https://localhost:4000/",
            original_urls,
        )
        is None
    )
    # Check redirect to another domain
    assert (
        _check_url_components(
            "https://originalurl.com",
            "http://cloudflareblock.com",
            original_urls,
        )
        is None
    )


@pytest.mark.skims_test_group("all_unittesting")
def test_remove_duplicate_redirected_urls() -> None:
    content = "<h1> Hello </h1>"
    soup = BeautifulSoup(content, features="html.parser")
    url_contexts = {
        URLContext(
            components=urlparse("https://originalurl.com/"),
            content=content,
            custom_f023=None,
            has_redirect=False,
            headers_raw={},
            is_html=True,
            original_url="https://originalurl.com/",
            soup=soup,
            timestamp_ntp=datetime.now().timestamp(),  # noqa: DTZ005
            url="https://originalurl.com/",
            response_status=200,
        ),
        URLContext(
            components=urlparse("https://redirectedurl.com/"),
            content=content,
            custom_f023=None,
            has_redirect=False,
            headers_raw={},
            is_html=True,
            original_url="https://originalurl2.com/",
            soup=soup,
            timestamp_ntp=datetime.now().timestamp(),  # noqa: DTZ005
            url="https://redirectedurl.com/",
            response_status=200,
        ),
        URLContext(
            components=urlparse("https://redirectedurl.com/"),
            content=content,
            custom_f023=None,
            has_redirect=False,
            headers_raw={},
            is_html=True,
            original_url="http://originalurl2.com/",
            soup=soup,
            timestamp_ntp=datetime.now().timestamp(),  # noqa: DTZ005
            url="https://redirectedurl.com",
            response_status=200,
        ),
    }
    original_urls = {"https://originalurl.com", "https://originalurl2.com"}
    clean_url_contexts = _remove_duplicate_redirected_urls(url_contexts, original_urls)

    urls_to_analyze = {url_ctx.url for url_ctx in clean_url_contexts}

    assert urls_to_analyze == {
        "https://originalurl.com/",
        "https://redirectedurl.com/",
    }
