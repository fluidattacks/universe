# shellcheck shell=bash

readonly FINDINGS_MANIFEST_PATH="skims/manifests/finding_codes.json"

function main {
  : && commit_msg="$(git --no-pager log HEAD^..HEAD)" \
    && if echo "${commit_msg}" | grep -q -- "- migration-aware"; then
      info "migration-aware flag detected. Be sure of running the migration to prevent" \
        "any vulnerabilities associated with this Finding from remaining open in the platform."
      return 0
    fi \
    && info "Checking if any findings were deprecated in this commit." \
    && if git diff HEAD^ HEAD "${FINDINGS_MANIFEST_PATH}" | grep -q '\-  "F'; then
      error \
        "This commit is deprecating a Finding." \
        "Please add the flag '- migration-aware' to your commit" \
        "body to indicate acknowledgment." \
        "Additionally, ensure you run the migration to prevent any" \
        "vulnerabilities associated with this Finding from remaining open in the platform."
    fi \
    && info "The commit does not deprecate any findings."

}

main "${@}"
