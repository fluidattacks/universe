# noqa: INP001
import pytest
from lib_dast.utils import (
    is_html,
)


@pytest.mark.skims_test_group("all_unittesting")
def test_is_html() -> None:
    assert not is_html("test")
    assert not is_html("{}")
    assert not is_html('{"json": true}')
    assert not is_html('\n\n\n\n\n {"json": "<html>test</html>"}')
    assert is_html("<html><title>asdf</title></html>")
    assert is_html("<html><title>asdf")
    assert is_html("<!DOCTYPE html><html><title>asdf")
