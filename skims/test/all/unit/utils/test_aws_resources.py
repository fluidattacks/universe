# noqa: INP001
import json
import shutil
import subprocess
from datetime import (
    UTC,
    datetime,
    timedelta,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
)

import boto3
import botocore
import pytest
import requests
from bs4 import (
    BeautifulSoup,
)
from dateutil.parser import (
    parse,
)

from test.utils import (
    load_json_results,
)


def get_credentials(
    session: boto3.Session,
) -> botocore.credentials.Credentials:
    return session.get_credentials()  # type: ignore[return-value]


def get_signin_token(
    credentials: botocore.credentials.Credentials,
) -> tuple[str, requests.Session]:
    rsession = requests.Session()
    response = rsession.get(
        "https://signin.aws.amazon.com/federation",
        params={
            "Action": "getSigninToken",
            "Session": json.dumps(
                {
                    "sessionId": credentials.access_key,
                    "sessionKey": credentials.secret_key,
                    "sessionToken": credentials.token,
                },
            ),
        },
    )
    token = json.loads(response.text)["SigninToken"]
    return token, rsession


def signin(token: str, rsession: requests.Session) -> requests.Session:
    rsession.get(
        "https://signin.aws.amazon.com/federation",
        params={
            "Action": "login",
            "Issuer": None,
            "Destination": "https://console.aws.amazon.com/",
            "SigninToken": token,
        },
    )
    return rsession


def get_csrf_token(rsession: requests.Session) -> str | None:
    response = rsession.get(
        "https://us-east-1.console.aws.amazon.com/iamv2/home#",
        params={"region": "eu-central-1", "state": "hashArgs"},
    )
    soup = BeautifulSoup(response.text, "html.parser")
    for meta in soup.find_all("meta"):
        if meta.get("name") == "awsc-csrf-token":
            return meta["content"]
    return None


def get_api_result(
    path: str,
    param: dict[str, Any] | None,
    csrf_token: str,
    rsession: requests.Session,
    cache: dict[str, dict[str, Any]],
) -> Any:  # noqa: ANN401
    params = json.dumps(param)
    if path in cache and params in cache[path]:
        return cache[path][params]

    response = rsession.post(
        "https://us-east-1.console.aws.amazon.com/iamv2/api/iamv2",
        headers={
            "Content-Type": "application/json",
            "X-CSRF-Token": csrf_token,
        },
        data=json.dumps(
            {
                "headers": {"Content-Type": "application/json"},
                "path": f"/prod/{path}",
                "method": "POST",
                "region": "us-east-1",
                "params": {},
                **({"contentString": params} if params else {}),
            },
        ),
    )
    result = json.loads(response.text)
    cache[path][params] = result
    return result


def get_token(
    session: boto3.Session,
    cache: dict[str, dict[str, Any]],
) -> tuple[str | None, requests.Session, dict[str, dict[str, Any]]]:
    credentials = get_credentials(session)
    token, rsession = get_signin_token(credentials)
    rsession = signin(token, rsession)
    csrf_token = get_csrf_token(rsession)
    return csrf_token, rsession, cache


def fetch_resources(
    cache: dict[str, dict[str, Any]],
    csrf_token: str,
    rsession: requests.Session,
) -> Any:  # noqa: ANN401
    if csrf_token:
        return get_api_result("resources", None, csrf_token, rsession, cache)
    return None


def build_arn_dict(services_result: Any, filename: str) -> None:  # noqa: ANN401
    arn_dict: dict[str, dict[str, str]] = {}
    for service_attr in services_result:
        service = service_attr["arn"].split(":")[2]
        resource = service_attr["resourceName"]
        arn_dict.setdefault(service, {})[resource] = service_attr["arn"]
    save_arn_dict(arn_dict, filename)


def save_arn_dict(
    arn_dict: dict[str, dict[str, Any]],
    filename: str,
) -> None:
    with Path(filename).open("w", encoding="utf-8") as file:
        json.dump(arn_dict, file, indent=2)


def get_iam_info(
    rsession: requests.Session,
    csrf_token: str,
    cache: dict[str, dict[str, Any]],
) -> dict[str, dict[str, Any]]:
    awssvcs: dict[str, dict[str, Any]] = {}

    if csrf_token:
        services = get_api_result("services", None, csrf_token, rsession, cache)
        for service in services:
            name = service["serviceName"]
            if name not in awssvcs:
                awssvcs[name] = {"parts": []}
            awssvcs[name]["parts"].append(service)

    return get_all_service_actions(rsession, csrf_token, awssvcs, cache)


def build_actions_dict(awssvcs: dict[str, dict[str, Any]], filename: str) -> None:
    arn_dict: dict[str, dict[str, list[str]]] = {}
    for service_name, service_info in awssvcs.items():
        actions = service_info.get("Actions", [])
        if actions:
            sorted_actions = sorted(actions, key=lambda x: x["actionName"])
            for action in sorted_actions:
                action_group = action["actionGroups"][0].lower()
                action_name = action["actionName"]
                resource_wildcard = bool(action.get("requiredResources"))

                group_map = {
                    "readonly": "read",
                    "listonly": "list",
                    "readwrite": "write",
                }

                if action_group in group_map:
                    group = group_map[action_group]
                    arn_dict.setdefault(service_name, {}).setdefault(group, []).append(action_name)

                if not resource_wildcard:
                    arn_dict.setdefault(service_name, {}).setdefault(
                        "wildcard_resource",
                        [],
                    ).append(action_name)

    save_arn_dict(arn_dict, filename)


def get_all_service_actions(
    rsession: requests.Session,
    csrf_token: str,
    awssvcs: dict[str, dict[str, Any]],
    cache: dict[str, dict[str, Any]],
) -> dict[str, dict[str, Any]]:
    for service_name, service_items in awssvcs.items():
        if "Actions" not in service_items:
            awssvcs[service_name]["Actions"] = get_api_result(
                rsession=rsession,
                csrf_token=csrf_token,
                path="actions",
                param={"serviceName": awssvcs[service_name]["parts"][0]["serviceKeyName"]},
                cache=cache,
            )
    return awssvcs


def run_fetch_resources(*, filename: str, build_arn_file: bool) -> None:
    methods: dict[str, Any] = {
        "services": lambda _: None,
        "actions": lambda p: {"serviceName": p, "RegionName": "eu-central-1"},
        "resources": lambda _: None,
        "contextKeys": lambda _: None,
        "globalConditionKeys": lambda _: None,
        "getServiceLinkedRoleTemplate": lambda p: {"serviceName": p},
        "policySummary": lambda p: {"policyDocument": p},
        "validate": lambda p: {"policy": json.dumps(p), "type": ""},
    }
    session = boto3.Session()
    cache: dict[str, dict[str, Any]] = {method: {} for method in methods}
    csrf_token, rsession, cache = get_token(session, cache)

    if csrf_token:
        if build_arn_file:
            services_result = fetch_resources(cache, csrf_token, rsession)
            build_arn_dict(services_result, filename)
        else:
            awssvcs = get_iam_info(rsession, csrf_token, cache)
            build_actions_dict(awssvcs, filename)


def get_last_modified_date(file_path: str) -> datetime | None:
    result = subprocess.run(  # noqa: S603
        ["git", "log", "-1", "--format=%cd", "--date=iso", file_path],  # noqa: S607
        capture_output=True,
        text=True,
        check=True,
    )
    last_modified_str = result.stdout.strip()
    if not last_modified_str:
        return None
    return parse(last_modified_str)


def has_24_hours_passed(file_path: str) -> bool:
    last_modified_date = get_last_modified_date(file_path)
    if not last_modified_date:
        return True
    now = datetime.now(UTC)
    time_difference = now - last_modified_date
    return time_difference > timedelta(hours=24)


@pytest.mark.skims_test_group("cspm_aws_json")
def test_aws_arn() -> None:
    original_json = "skims/skims/utils/aws/arn.json"

    expected_json = "skims/test/outputs/expected.json"
    backup_json = "skims/test/outputs/aws_arn_backup.json"

    if not Path(backup_json).exists():
        shutil.copyfile(original_json, backup_json)
    else:
        shutil.copyfile(backup_json, original_json)
        Path(backup_json).unlink()

    if has_24_hours_passed(original_json):
        run_fetch_resources(filename=expected_json, build_arn_file=True)

        data1 = load_json_results(expected_json)
        data2 = load_json_results(original_json)

        Path(expected_json).unlink()
        save_arn_dict(data1, original_json)

        assert data1 == data2

    Path(backup_json).unlink()


@pytest.mark.skims_test_group("cspm_aws_json")
def test_aws_actions() -> None:
    original_json = "skims/skims/utils/aws/iam_actions.json"

    expected_json = "skims/test/outputs/expected.json"
    backup_json = "skims/test/outputs/aws_iam_actions_backup.json"

    if not Path(backup_json).exists():
        shutil.copyfile(original_json, backup_json)
    else:
        shutil.copyfile(backup_json, original_json)
        Path(backup_json).unlink()

    if has_24_hours_passed(original_json):
        run_fetch_resources(filename=expected_json, build_arn_file=False)

        data1 = load_json_results(expected_json)
        data2 = load_json_results(original_json)

        Path(expected_json).unlink()
        save_arn_dict(data1, original_json)

        assert data1 == data2

    Path(backup_json).unlink()
