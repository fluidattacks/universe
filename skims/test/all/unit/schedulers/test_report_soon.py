# noqa: INP001
import pytest
from model.core import (
    DeveloperEnum,
    FindingEnum,
    ScaAdvisoriesMetadata,
    SkimsVulnerabilityMetadata,
    Vulnerability,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from pytest_mock import (
    MockerFixture,
)
from utils.report_soon import (
    get_sast_vuln_json_message,
)


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.parametrize(
    ("vulnerability", "expected_vuln_msg"),
    [
        (
            Vulnerability(
                finding=FindingEnum.F001,
                kind=VulnerabilityTechnique.SAST,
                vulnerability_type=VulnerabilityType.LINES,
                namespace="universe",
                stream="skims",
                what="view.cs",
                where="20",
                skims_metadata=SkimsVulnerabilityMetadata(
                    source_method="cs_test_sql_injection",
                    developer=DeveloperEnum.DEFAULT,
                    description="SQL injection found in view.cs",
                    snippet="sql.query(user_input)",
                    cvss="CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N",
                    cvss_v4="CVSS:4.0/AV:A/AC:H/PR:L/UI:N/AT:N/VC:L/VI:N",
                    cwe_ids=["CWE-11"],
                ),
            ),
            {
                "commit_hash": "0000",
                "line": "20",
                "path": "view.cs",
                "repo_nickname": "universe",
                "state": "open",
                "skims_method": "cs_test_sql_injection",
                "skims_description": "SQL injection found in view.cs",
                "technique": "SAST",
                "developer": "machine@fluidattacks.com",
                "source": "MACHINE",
                "hash": 9862853613397930517,
                "cwe_ids": ["CWE-11"],
                "cvss_v3": "CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N",
                "cvss_v4": "CVSS:4.0/AV:A/AC:H/PR:L/UI:N/AT:N/VC:L/VI:N",
                "advisories": None,
                "message": {
                    "description": "SQL injection found in view.cs",
                    "snippet": "sql.query(user_input)",
                },
            },
        ),
        (
            Vulnerability(
                finding=FindingEnum.F002,
                kind=VulnerabilityTechnique.SAST,
                vulnerability_type=VulnerabilityType.LINES,
                namespace="universe",
                stream="skims",
                what="my_app.js",
                where="11",
                skims_metadata=SkimsVulnerabilityMetadata(
                    source_method="reachability_cve2021_01080",
                    developer=DeveloperEnum.DEFAULT,
                    description="Use of vulnerable dependency in my_app.js",
                    snippet="express.redirect(url)",
                    cvss="CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N",
                    cvss_v4="CVSS:4.0/AV:A/AC:H/PR:L/UI:N/AT:N/VC:L/VI:N",
                    cwe_ids=["CWE-11"],
                    sca_metadata=ScaAdvisoriesMetadata(
                        cve=["CVE-2020-01080"],
                        vulnerable_version="3.8.1",
                        package="express",
                    ),
                ),
            ),
            {
                "commit_hash": "0000",
                "line": "11",
                "path": "my_app.js",
                "repo_nickname": "universe",
                "state": "open",
                "skims_method": "reachability_cve2021_01080",
                "skims_description": "Use of vulnerable dependency in my_app.js",
                "technique": "SAST",
                "developer": "machine@fluidattacks.com",
                "source": "MACHINE",
                "hash": 17657594505334636834,
                "cwe_ids": ["CWE-11"],
                "cvss_v3": "CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N",
                "cvss_v4": "CVSS:4.0/AV:A/AC:H/PR:L/UI:N/AT:N/VC:L/VI:N",
                "advisories": {
                    "package": "express",
                    "vulnerable_version": "3.8.1",
                    "cve": ["CVE-2020-01080"],
                },
                "message": {
                    "description": "Use of vulnerable dependency in my_app.js",
                    "snippet": "express.redirect(url)",
                },
            },
        ),
    ],
)
def test_report_soon(
    mocker: MockerFixture,
    vulnerability: Vulnerability,
    expected_vuln_msg: dict,
) -> None:
    mocker.patch("utils.report_soon.get_repo_head_hash", return_value="0000")
    vuln_msg = get_sast_vuln_json_message(vulnerability, "universe", "universe")
    assert vuln_msg == expected_vuln_msg
