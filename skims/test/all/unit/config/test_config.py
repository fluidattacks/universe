# noqa: INP001
from unittest.mock import (
    AsyncMock,
    patch,
)

import ctx
import pytest

from test.utils import (
    get_suite_config,
    skims,
)


@pytest.mark.skims_test_group("all_unittesting")
def test_help() -> None:
    code, stdout, stderr = skims("--help")
    assert code == 0
    assert "Usage:" in stdout
    assert not stderr


@pytest.mark.skims_test_group("all_unittesting")
def test_non_existent_config() -> None:
    code, stdout, _ = skims("scan", "#")
    assert code == 2
    assert "Wrong scan argument" in stdout


@pytest.mark.skims_test_group("all_unittesting")
def test_config_with_extra_parameters() -> None:
    code, _, stderr = skims("scan", get_suite_config("bad_extra_things"))
    assert code == 0
    assert not stderr, stderr


@pytest.mark.skims_test_group("all_unittesting")
@patch("core.cli.skims_main")
def test_strict_execution(
    mock_run_main: AsyncMock,
) -> None:
    mock_run_main.return_value = [True, 3]
    code, _, _ = skims("--strict", "scan", get_suite_config("skims"))
    assert code == 1


@pytest.mark.skims_test_group("all_unittesting")
@patch("core.cli.skims_main")
def test_execute_multiple_modules(
    mock_run_main: AsyncMock,
) -> None:
    mock_run_main.return_value = [True, 0]
    _, _, stderr = skims("scan", get_suite_config("skims"), "--execution-module", "sast,cspm")
    captured_config = ctx.SKIMS_CONFIG
    assert len(captured_config.sast.include) > 0
    assert len(captured_config.sca.include) == 0
    assert captured_config.cspm
    assert len(captured_config.cspm.aws_credentials) > 0
    assert not stderr


@pytest.mark.skims_test_group("all_unittesting")
@patch("core.cli.skims_main")
def test_scan_url(
    mock_run_main: AsyncMock,
) -> None:
    mock_run_main.return_value = [True, 0]
    _, _, stderr = skims("scan", "https://fluidattacks.com/")
    captured_config = ctx.SKIMS_CONFIG
    assert captured_config.dast
    assert captured_config.dast.http_checks
    assert captured_config.dast.ssl_checks
    assert captured_config.dast.urls == ("https://fluidattacks.com/",)
    assert not stderr
