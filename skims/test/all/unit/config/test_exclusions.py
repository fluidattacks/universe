# noqa: INP001
from datetime import (
    datetime,
)

import pytest
from core.load_exclusions import (
    DastExclusion,
    ScaExclusion,
    SkimsExclusions,
    get_fluidattacks_exclusions,
)


@pytest.mark.skims_test_group("all_unittesting")
def test_load_skims_exclusions() -> None:
    exclusions = get_fluidattacks_exclusions("skims/test/all/unit/config/")
    assert exclusions == SkimsExclusions(
        dast=[
            DastExclusion(
                endpoint="https://example.com/test",
                target_findings={
                    "f050": "another reason",
                    "f100": "test reason",
                },
            ),
            DastExclusion(
                endpoint="https://docs.example.com/test",
                target_findings={
                    "f100": "test reason",
                },
            ),
        ],
        sca=[
            ScaExclusion(
                dependency_name="test_ip",
                reason="Testing",
                date_limit=None,
            ),
            ScaExclusion(
                dependency_name="test_wp",
                reason="Testing",
                date_limit=datetime(2024, 9, 1, 0, 0),  # noqa: DTZ001
            ),
        ],
    )
