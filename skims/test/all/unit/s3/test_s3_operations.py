# noqa: INP001
import json

import pytest
from botocore.exceptions import (
    ClientError,
)
from pytest_mock import (
    MockerFixture,
)
from utils.aws.s3 import (
    download_json_fileobj,
    upload_object,
)
from utils.custom_exceptions import (
    UnavailabilityError,
)


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.asyncio
async def test_upload_object(
    mocker: MockerFixture,
) -> None:
    file_name: str = "file_name"
    dict_object: dict = {"key": "value"}
    bucket: str = "bucket"
    mocker.patch("builtins.print")
    put_object_mock = mocker.AsyncMock()
    get_s3_resource_mock = mocker.patch(
        "utils.aws.s3.get_s3_resource",
        return_value=mocker.AsyncMock(put_object=put_object_mock),
    )
    await upload_object(file_name, dict_object, bucket)
    assert get_s3_resource_mock.await_count == 1
    assert put_object_mock.await_count == 1
    assert put_object_mock.call_args.kwargs == {
        "Body": json.dumps(dict_object, indent=2),
        "Bucket": bucket,
        "Key": file_name,
    }


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.asyncio
async def test_upload_object_error(
    mocker: MockerFixture,
) -> None:
    mocker.patch("utils.aws.s3.get_s3_resource", side_effect=ClientError({}, "message"))
    with pytest.raises(UnavailabilityError):
        await upload_object("", {}, "")


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.asyncio
async def test_download_json_fileobj(
    mocker: MockerFixture,
) -> None:
    file_name: str = "file_name"
    bucket: str = "bucket"
    expected_dict = {"key": "value"}
    download_fileobj_mock = mocker.AsyncMock(
        side_effect=lambda _, __, fileobj: (
            fileobj.write(json.dumps(expected_dict).encode("utf-8"))
        ),
    )
    get_s3_resource_mock = mocker.patch(
        "utils.aws.s3.get_s3_resource",
        return_value=mocker.AsyncMock(download_fileobj=download_fileobj_mock),
    )
    result: dict = await download_json_fileobj(bucket, file_name)
    assert get_s3_resource_mock.await_count == 1
    assert download_fileobj_mock.await_count == 1
    assert download_fileobj_mock.call_args.args == (
        bucket,
        file_name,
        mocker.ANY,
    )
    assert result == expected_dict


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.asyncio
async def test_download_json_fileobj_error(
    mocker: MockerFixture,
) -> None:
    file_name: str = "file_name"
    bucket: str = "bucket"
    value_error: Exception = ValueError("message")
    download_fileobj_mock = mocker.AsyncMock(side_effect=value_error)
    get_s3_resource_mock = mocker.patch(
        "utils.aws.s3.get_s3_resource",
        return_value=mocker.AsyncMock(download_fileobj=download_fileobj_mock),
    )
    log_blocking_mock = mocker.patch("utils.aws.s3.log_blocking")
    await download_json_fileobj(bucket, file_name)
    assert get_s3_resource_mock.await_count == 1
    assert download_fileobj_mock.await_count == 1
    assert log_blocking_mock.call_args.args == ("error", "%s", value_error)
