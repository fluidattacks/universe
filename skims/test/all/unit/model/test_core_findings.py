# noqa: INP001
import json
import os
from pathlib import Path

import pytest
from model import (
    core,
)

from skims.lib_sast.reachability import QUERIES


@pytest.mark.skims_test_group("all_unittesting")
def test_manifest_finding_codes() -> None:
    path: str = "skims/manifests/finding_codes.json"
    expected: str = (
        json.dumps(
            [finding.name for finding in core.FindingEnum],
            indent=2,
            sort_keys=True,
        )
        + "\n"
    )

    expected_path = Path(os.environ["STATE"]) / path
    expected_path.parent.mkdir(parents=True, exist_ok=True)
    with expected_path.open("w", encoding="utf-8") as handle_w:
        handle_w.write(expected)

    with Path(path).open(encoding="utf-8") as handle_r:
        assert handle_r.read() == expected


@pytest.mark.skims_test_group("all_unittesting")
def test_covered_reachability_cves() -> None:
    path: str = "skims/skims/lib_sast/reachability/covered_cves.json"
    covered_cves = {
        query.cve: query.finding.name for queries in QUERIES.values() for query in queries
    }
    expected: str = json.dumps(covered_cves, indent=2, sort_keys=True) + "\n"

    expected_path = Path(os.environ["STATE"]) / path
    expected_path.parent.mkdir(parents=True, exist_ok=True)
    with expected_path.open("w", encoding="utf-8") as handle_w:
        handle_w.write(str(expected))

    with Path(path).open(encoding="utf-8") as handle_r:
        assert handle_r.read() == expected
