# noqa: INP001
import pytest
from utils.cvss import (
    get_cvss_vector_v3_from_criteria,
    get_cvss_vector_v4_from_criteria,
)


@pytest.mark.skims_test_group("all_unittesting")
def test_get_cvss_vector() -> None:
    assert (
        get_cvss_vector_v3_from_criteria("001")
        == "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C"
    )
    assert (
        get_cvss_vector_v3_from_criteria("172")
        == "CVSS:3.1/AV:L/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N/E:U/RL:O/RC:C"
    )
    assert (
        get_cvss_vector_v3_from_criteria("402")
        == "CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:R"
    )
    assert (
        get_cvss_vector_v4_from_criteria("001") == "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI"
        ":N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U"
    )
    assert (
        get_cvss_vector_v4_from_criteria("172") == "CVSS:4.0/AV:L/AC:H/AT:N/PR:N/UI:N/"
        "VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:U"
    )
    assert (
        get_cvss_vector_v4_from_criteria("402") == "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:N/"
        "VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U"
    )
