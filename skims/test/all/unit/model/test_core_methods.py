# noqa: INP001
import json
import os
import re
import subprocess
from pathlib import (
    Path,
)

import pytest
from lib_apk.methods_enum import (
    MethodsEnumAPK,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM,
)
from lib_dast.methods_enum_http import (
    MethodsEnumHTTP,
)
from lib_dast.methods_enum_ssl import (
    MethodsEnumSSL,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA,
)
from model.core import (
    MethodInfo,
    VulnerabilityTechnique,
)
from utils.cvss import (
    is_cvss_v3,
    is_cvss_v4,
)
from utils.translations import (
    load_translations,
)

TECHNIQUES_MAPPING: dict[str, VulnerabilityTechnique] = {
    "lib_apk": VulnerabilityTechnique.DAST,
    "lib_cspm": VulnerabilityTechnique.CSPM,
    "lib_dast/http": VulnerabilityTechnique.DAST,
    "lib_dast/ssl": VulnerabilityTechnique.DAST,
    "lib_sast/root": VulnerabilityTechnique.SAST,
    "lib_sast/path": VulnerabilityTechnique.SAST,
    "lib_sast/reachability": VulnerabilityTechnique.SAST,
    "lib_sca": VulnerabilityTechnique.SCA,
}

CWE_PATTERN = re.compile(r"CWE-(\d{1,4})")

TotalMethodsEnum = [
    MethodsEnumSAST,
    MethodsEnumAPK,
    MethodsEnumHTTP,
    MethodsEnumSSL,
    MethodsEnumSCA,
    MethodsEnumCSPM,
]


@pytest.mark.skims_test_group("all_unittesting")
def test_methods_order() -> None:
    # This makes reviewing process easier, let's keep them ordered
    for methods_enum in TotalMethodsEnum:
        prev_finding_code = "001"
        for member in methods_enum.__members__.values():
            method_fin = str(member.value.finding)[-3:]
            assert method_fin >= prev_finding_code, (
                "Please put method in order according to the finding Enum"
            )

            prev_finding_code = method_fin


@pytest.mark.skims_test_group("all_unittesting")
def test_methods_utils() -> None:
    for methods_enum in TotalMethodsEnum:
        for member in methods_enum.__members__.values():
            assert member.value.module in TECHNIQUES_MAPPING
            assert member.value.technique == TECHNIQUES_MAPPING[member.value.module]
            assert is_cvss_v3(member.value)
            assert is_cvss_v4(member.value)

            if not member.value.cvss:
                continue
            metrics = member.value.cvss.split("/")
            assert any(metric.startswith("E:") for metric in metrics)
            assert any(metric.startswith("RL:") for metric in metrics)
            assert any(metric.startswith("RC:") for metric in metrics)
            assert {"E:X", "RL:X", "RC:X"}.intersection(metrics) == set()


def _has_method_correct_information(
    method_info: MethodInfo,
    method_name: str,
) -> bool:
    found = False
    module_path = Path.cwd() / f"skims/skims/{method_info.module}"
    file_options = [
        os.path.join(  # noqa: PTH118
            method_info.module,
            method_info.file_name,
        ),  # lib_http/analyze_headers
        os.path.join(  # noqa: PTH118
            method_info.module,
            method_info.finding.name.lower(),
            method_info.file_name,
        ),  # lib_path/f001/java
        os.path.join(  # noqa: PTH118
            method_info.module,
            method_info.file_name,
            method_info.finding.name.lower(),
        ),  # lib_dast/aws/f001
        os.path.join(  # noqa: PTH118
            method_info.module,
            method_info.finding.name.lower(),
            method_info.file_name,
            method_info.name,
        ),  # lib_path/f001/java/method
    ]

    with subprocess.Popen(  # noqa: S603
        ["grep", "-lr", method_name, module_path],  # noqa: S607
        stdout=subprocess.PIPE,
    ) as proc:
        stdout, _ = proc.communicate()
        if proc.returncode == 0:
            # For some methods, there may be several result paths
            results = stdout.decode().strip("\n").split("\n")
            found = any(
                result.split(".", maxsplit=1)[0].endswith(option)
                for option in file_options
                for result in results
            )

    return found


def _load_allowed_cwes() -> set[str]:
    with Path("skims/static/cwe/cwe_classification.json").open(encoding="utf-8") as file:
        cwe_data = json.load(file)

    vulnerability_mapping = cwe_data.get("vulnerability_mapping", {})
    return {
        f"CWE-{cwe}"
        for cwe in set().union(
            vulnerability_mapping.get("allowed_with_review", []),
            vulnerability_mapping.get("allowed", []),
        )
    }


def _has_allowed_cwe_ids(
    method_info: MethodInfo,
    method_name: str,
    allowed_cwe_list: set[str],
) -> None:
    for cwe_id in method_info.cwe_ids or []:
        match = CWE_PATTERN.fullmatch(cwe_id)
        assert match, f"{method_name}: Invalid format: {cwe_id}. Expected format: CWE-123"
        assert cwe_id in allowed_cwe_list, (
            f"{method_name}: {cwe_id} is not Allowed. "
            f"https://cwe.mitre.org/data/definitions/{match.group(1)}.html"
        )


@pytest.mark.skims_test_group("all_unittesting")
def test_methods_model() -> None:
    method_names = set()
    methods_amount = 0
    allowed_cwe_list = _load_allowed_cwes()
    for methods_enum in TotalMethodsEnum:
        for method in methods_enum:
            method_names.add(method.value.get_name())
            methods_amount += 1
            if "APK" in method.name or method.value.file_name == "generic":
                continue
            if "lib_sca" in method.value.module:
                continue

            assert method.name.lower() == method.value.name, (
                f"{method.name} in lowercase differs from {method.value.name}"
            )
            assert _has_method_correct_information(method.value, method.name)
            _has_allowed_cwe_ids(method.value, method.name, allowed_cwe_list)

    assert len(method_names) == methods_amount, "Please ensure all methods have unique names"


@pytest.mark.skims_test_group("all_unittesting")
def test_method_desc_keys() -> None:
    err_message = "Desc length must be between 30 and 200 chars and don't end with a dot"
    translation_keys = set()
    translations = load_translations(include_criteria=False).items()

    for key, descriptions in translations:
        if key in {"words.line", "words.in"}:
            continue
        translation_keys.add(key)
        for description in descriptions.values():
            assert len(description) >= 30, err_message
            assert len(description) <= 200, err_message
            assert not description.endswith("."), err_message

    method_names = {
        method.name
        if method.value.module != "lib_sast/reachability"
        or method.name in {"JS_CVE_2023_28155", "TS_CVE_2023_28155"}
        else "REACHABILITY_METHOD_DESC"
        for methods_enum in TotalMethodsEnum
        for method in methods_enum
    }

    unmatched_keys = translation_keys.symmetric_difference(method_names)

    assert unmatched_keys == set(), f"Please match {unmatched_keys} methods and desc_keys."


def has_at_least_one_file(test_directory: Path) -> bool:
    return any(item.is_file() for item in test_directory.rglob("*"))


def get_file_tests(module: str) -> set[str]:
    test_files_path = Path(f"skims/test/lib_sast/data/lib_{module}")
    test_directories_by_method = set()
    for directory in test_files_path.iterdir():
        finding_path = Path(directory)
        for method_directory in finding_path.iterdir():
            if method_directory.name == "package.json":
                continue
            err_message = (
                "Test files must be placed insidedirectories with the name of its associated method"
            )
            assert method_directory.is_dir(), err_message
            assert directory.is_dir(), err_message
            if has_at_least_one_file(method_directory):
                test_directories_by_method.add(f"{directory.name} - {method_directory.name}")
    return test_directories_by_method


def get_methods_in_model(module: str) -> set[str]:
    methods_in_model = set()
    for method in MethodsEnumSAST:
        if method.value.module.endswith(module):
            methods_in_model.add(f"{method.value.finding.name.lower()} - {method.value.name}")
    return methods_in_model


@pytest.mark.skims_test_group("all_unittesting")
def test_methods_have_test_files() -> None:
    target_modules = {"path", "root"}
    for module in target_modules:
        module_test_files: set[str] = get_file_tests(module)
        methods_in_model: set[str] = get_methods_in_model(module)
        unmatched_methods = module_test_files.symmetric_difference(methods_in_model)
        assert len(unmatched_methods) == 0, (
            f"Following methods or testfiles are not matched {unmatched_methods}"
        )
