# noqa: INP001
import json

import pytest
import yaml
from jsonschema import (
    validate,
)
from pytest_mock import (
    MockerFixture,
)

from test.utils import (
    load_json_results,
    skims,
)


def _format_sarif_vulns(content: dict) -> list[dict]:
    results = []
    for report in content:
        report_summary = {
            "rule_id": report["ruleId"],
            "vuln_id": report["fingerprints"]["guid"],
            "vuln_where": report["locations"][0]["physicalLocation"]["artifactLocation"]["uri"],
            "vuln_specific": report["locations"][0]["physicalLocation"]["region"]["message"][
                "text"
            ],
            "kind": report["properties"]["kind"],
            "type": report["properties"]["vulnerability_type"],
            # Only useful for SCA vulns
            "package": report["message"]["properties"].get("package", ""),
            "vulnerable_version": report["message"]["properties"].get(
                "vulnerable_version",
                "",
            ),
            "cve": report["message"]["properties"].get("cve", ""),
            "epss": report["message"]["properties"].get("epss", ""),
            "reachability_info": {
                "package": reach_info["package"],
                "vulnerable_version": reach_info["vulnerable_version"],
                "cve": reach_info["cve"],
                "pkg_id": reach_info["pkg_id"],
            }
            if (reach_info := report["properties"].get("reachability_info"))
            else None,
        }
        results.append(report_summary)
    results.sort(key=lambda report: report["vuln_id"])
    return results


def _validate_rule_id(produced_item: dict) -> None:
    err_message = (
        "Your change altered important aspects of the sarif. "
        "Be sure to understand the consequences before adjusting the test."
    )
    sca_guids = {
        "script-manager": "18370341630494125352",
        "py": "1570701733394560690",
        "slug": "16422110563903705951",
        "vm2": "7897955115222216703",
    }
    match produced_item["rule_id"]:
        case "008":
            assert (
                produced_item["vuln_where"]
                == "skims/test/all/data/sarif_test/js_sandbox_escape_vm2.js"
            )
            assert produced_item["reachability_info"]["package"] == "vm2"
            assert produced_item["reachability_info"]["vulnerable_version"] == "3.9.17"
            assert produced_item["reachability_info"]["cve"]
            assert produced_item["reachability_info"]["pkg_id"]

        case "011":
            assert produced_item["package"] in sca_guids
            assert produced_item["vuln_id"] == sca_guids[produced_item["package"]]
            assert produced_item["vulnerable_version"] != ""
            assert produced_item["cve"] != ""
            assert produced_item["epss"].isdigit()
            assert int(produced_item["epss"]) >= 0
            assert produced_item["type"] == "LINES"

        case "015":
            assert produced_item["kind"] == "SAST"
            assert produced_item["vuln_specific"] == "19"
            assert produced_item["type"] == "LINES"

        case "016":
            assert produced_item["kind"] == "DAST"
            assert len(produced_item["vuln_specific"]) > 10
            assert produced_item["vuln_where"] == "https://localhost:4446"
            assert produced_item["type"] == "INPUTS"

        case "043":
            assert produced_item["kind"] == "DAST"
            assert len(produced_item["vuln_specific"]) > 10
            assert produced_item["vuln_where"] == "http://localhost:48000/headers_unsafe_0"
            assert produced_item["type"] == "INPUTS"

        case "052":
            assert str(produced_item["vuln_id"]) == "2229949251667622090", err_message
            assert (
                produced_item["vuln_where"] == "skims/test/all/data/sarif_test/insecure_cipher.java"
            ), err_message
            assert produced_item["vuln_specific"] == "5", err_message
            assert produced_item["type"] == "LINES"

        case "120":
            assert produced_item["package"] in {
                "futures",
                "botocore",
            }
            assert produced_item["vuln_id"] in {
                "5638615762264140627",
                "10380945428693859483",
            }
            assert produced_item["type"] == "LINES"

        case "398":
            assert produced_item["kind"] == "DAST"
            assert (
                produced_item["vuln_specific"]
                == "The application is vulnerable to Fragment Injection"
            )
            assert produced_item["type"] == "INPUTS"


def check_that_sarif_results_match() -> None:
    sarif_schema = load_json_results("skims/test/all/data/sarif_schema.json")
    sarif_data = load_json_results("skims/test/all/results/results.sarif")
    validate(instance=sarif_data, schema=sarif_schema)

    produced_results = yaml.safe_load(json.dumps(sarif_data))["runs"][0]
    produced_vulns = _format_sarif_vulns(produced_results["results"])
    for produced_item in produced_vulns:
        _validate_rule_id(produced_item)

    # Ensure all guids are unique
    vuln_guids = [vuln["vuln_id"] for vuln in produced_vulns]
    assert len(vuln_guids) == len(set(vuln_guids))

    # Ensure web responses are saved to avoid instability in dast reports
    http_responses = {
        response["properties"]["url"]: response["statusCode"]
        for response in produced_results["webResponses"]
        if response.get("statusCode")
    }
    assert http_responses["http://localhost:48000/headers_unsafe_0"] == 200
    assert http_responses["http://localhost:48000/error"] >= 400
    assert http_responses["https://localhost:48000/error"] == 404

    ssl_responses = {
        response["properties"]["url"]: response["properties"]["ssl_status"]
        for response in produced_results["webResponses"]
        if response["properties"].get("ssl_status", None) is not None
    }
    assert ssl_responses["https://localhost:4446"] is True
    assert ssl_responses["https://localhost:48000"] is False

    cspm_responses = next(
        (
            req["properties"].get("error_methods")
            for req in produced_results["webResponses"]
            if req["properties"]["url"] == "cspm_methods"
        ),
        None,
    )

    assert cspm_responses
    assert list(cspm_responses.keys()) == [
        "aws_permissions_problems",
        "aws",
        "azure",
        "gcp",
    ]


def run_skims_for_sarif(mocker: MockerFixture) -> None:
    path = "skims/test/all/test_configs/sarif_results.yaml"
    with (
        mocker.patch(
            "core.sarif_result.get_repo_branch",
            return_value="trunk",
        ),
        mocker.patch(
            "lib_cspm.aws.analyze.get_role",
            return_value="arn:aws:test_role",
        ),
    ):
        code, stdout, _ = skims("scan", path)
    assert code == 0, stdout
    assert "[INFO] Startup work dir is:" in stdout

    check_that_sarif_results_match()


@pytest.mark.usefixtures(
    "test_mocks_http",
    "test_mocks_ssl_unsafe",
)
@pytest.mark.skims_test_group("all_sarif_report")
def test_all_sarif_report(mocker: MockerFixture) -> None:
    run_skims_for_sarif(mocker)
