const {VM} = require("vm2");
const vm = new VM();


function vulnFunc (req, res) {
  const code = req.params.code;
  console.log(vm.run(code));
}


function safeFunc (req, res) {
  const code = "some internal code";
  console.log(vm.run(code));
}
