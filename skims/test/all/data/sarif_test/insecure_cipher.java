import javax.net.ssl.SSLContext;

public class SslInsecure {
	private static final String PROTOCOL="TLSv1.1";
	SSLContext unsafecontext = SSLContext.getInstance(PROTOCOL);
}

public class SslSecure {
	private static final String SSL="TLSv1.2";
	SSLContext safecontext = SSLContext.getInstance(SSL);
}
