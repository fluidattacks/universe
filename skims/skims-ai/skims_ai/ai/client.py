import os

from openai import AsyncOpenAI

openai_api_key = os.getenv("OPENAI_API_KEY")
openai_client = AsyncOpenAI(api_key=openai_api_key, timeout=30)
