from dataclasses import dataclass
from enum import Enum


class CVEStatus(Enum):
    IMPLEMENTED = "Implemented"
    DISCARDED = "Discarded"
    NOT_IMPLEMENTED = "Not implemented"


@dataclass
class EnrichedCVE:
    sbom_cve_id: str
    cve_id: str = ""
    affected_groups: str = "-"
    url: str = "-"
    pkg: str = "-"
    finding: str = "-"
    language: str = "-"
    severity: str = "-"
    is_candidate: bool | None = None
    under_review_by: str = "-"
    status: str = CVEStatus.NOT_IMPLEMENTED.value
    reason: str = "-"
    description: str = "-"


class SheetColumn(Enum):
    AFFECTED_GROUPS = 0
    SBOM_CVE_ID = 1
    CVE = 2
    PKG = 3
    FINDING = 4
    LANGUAGE = 5
    SEVERITY = 6
    UNDER_REVIEW_BY = 7
    STATUS = 8
    REASON = 9


class Sheet(Enum):
    CANDIDATES = "candidates"
    DISCARDED = "discarded"
