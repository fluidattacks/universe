import json
from pathlib import Path

import anyio

from skims_ai.config.logger import (
    LOGGER,
)
from skims_ai.prioritizes.defs import CVEStatus, EnrichedCVE, SheetColumn


def get_hyperlink(cve_id: str) -> str:
    if cve_id.startswith("=HYPERLINK("):
        return cve_id
    if cve_id.startswith("CVE-"):
        return f'=HYPERLINK("https://nvd.nist.gov/vuln/detail/{cve_id}", "{cve_id}")'
    if cve_id.startswith("GHSA-"):
        return f'=HYPERLINK("https://github.com/advisories/{cve_id}", "{cve_id}")'
    return "-"


def get_covered_cves() -> dict[str, str]:
    file_path = "../skims/lib_sast/reachability/covered_cves.json"
    with Path(file_path).open(encoding="utf-8") as covered_cves:
        return json.load(covered_cves)


def get_findings_by_from_classifier() -> dict[str, str]:
    file_path = "../static/cve_findings/cve_findings.json"
    with Path(file_path).open(encoding="utf-8") as findings:
        return json.load(findings)


def classify_advisory_type(advisory_id: str) -> dict[str, str]:
    if advisory_id.startswith("CVE-"):
        return {
            "type": "CVE",
            "url": f"https://www.cvedetails.com/api/v1/vulnerability/cve-json?cveId={advisory_id}",
            "bucket_prefix": "cves",
        }
    if advisory_id.startswith("GHSA-"):
        return {
            "type": "GHSA",
            "url": f"https://www.cvedetails.com/api/v1/osv/data-info?dataGuid={advisory_id}",
            "bucket_prefix": "ghsa-advisories",
        }
    LOGGER.warning("Unknown advisory type for %s", advisory_id)
    return {
        "type": "Unknown",
        "url": "-",
        "bucket_prefix": "-",
    }


def get_reason(automatable: bool, status: str) -> str:
    if status == "Implemented":
        return "Skims method found by Prioritize"
    if automatable:
        return "Lack of implementation"
    return "Discarded by Prioritizes"


def get_cve_status(automatable: bool) -> str:
    if automatable:
        return CVEStatus.NOT_IMPLEMENTED.value
    return CVEStatus.DISCARDED.value


def get_rows(cves: list[EnrichedCVE]) -> list[list[str]]:
    return [
        [
            str(cve.affected_groups),
            get_hyperlink(cve_id=cve.sbom_cve_id),
            get_hyperlink(cve_id=cve.cve_id),
            cve.pkg,
            cve.finding,
            cve.language,
            str(cve.severity),
            cve.under_review_by,
            cve.status,
            cve.reason,
        ]
        for cve in cves
    ]


def prioritize_candidates(cves_list: list[list[str]]) -> list[list[str]]:
    prioritized_cves = sorted(
        cves_list,
        key=lambda row: (
            -int(row[SheetColumn.AFFECTED_GROUPS.value]),
            -float(row[SheetColumn.SEVERITY.value]),
        ),
    )
    not_implemented = [
        row
        for row in prioritized_cves
        if row[SheetColumn.STATUS.value] == CVEStatus.NOT_IMPLEMENTED.value
    ]
    implemented_and_discarded = [
        row
        for row in prioritized_cves
        if row[SheetColumn.STATUS.value] in [CVEStatus.IMPLEMENTED.value, CVEStatus.DISCARDED.value]
    ]
    return not_implemented + implemented_and_discarded


async def get_context_messages() -> list[dict[str, str]]:
    ai_msgs_path = "skims_ai/prioritizes/ai_messages"
    system_msg = await anyio.Path(f"{ai_msgs_path}/system_message.md").read_text(encoding="utf-8")
    context = await anyio.Path(f"{ai_msgs_path}/context_messages.json").read_text(encoding="utf-8")
    return [{"role": "system", "content": system_msg}, *json.loads(context)]
