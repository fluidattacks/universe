import logging
import os
import re
import sys

import bugsnag
from bugsnag.handlers import (
    BugsnagHandler,
)
from bugsnag.notification import (
    Notification,
)

LOGGER_HANDLER = logging.StreamHandler()
LOGGER_FORMATTER = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
LOGGER = logging.getLogger()

LOGGER_REMOTE_HANDLER = BugsnagHandler()


def configure_logger() -> None:
    LOGGER_HANDLER.setStream(sys.stdout)
    LOGGER_HANDLER.setLevel(logging.INFO)
    LOGGER_HANDLER.setFormatter(LOGGER_FORMATTER)

    LOGGER.setLevel(logging.INFO)
    LOGGER.addHandler(LOGGER_HANDLER)

    LOGGER_REMOTE_HANDLER.setLevel(logging.ERROR)
    LOGGER.addHandler(LOGGER_REMOTE_HANDLER)


def guess_environment() -> str:
    return (
        "production"
        if os.environ.get("AWS_BATCH_JOB_ID") or os.environ.get("CI_COMMIT_REF_NAME") == "trunk"
        else "development"
    )


def _remove_nix_hash(path: str) -> str:
    pattern = r"(\/nix\/store\/[a-z0-9]{32}-)"
    result = re.search(pattern, path)
    if not result:
        return path
    return path[result.end(0) :]


def bugsnag_remove_nix_hash(
    notification: Notification,
) -> None:
    notification.stacktrace = [
        {**trace, "file": _remove_nix_hash(trace["file"])} for trace in notification.stacktrace
    ]


def bugsnag_add_batch_metadata(
    notification: Notification,
) -> None:
    if batch_job_id := os.environ.get("AWS_BATCH_JOB_ID"):
        batch_job_info = {"batch_job_id": batch_job_id}
        notification.add_tab("batch_job_info", batch_job_info)


def initialize_bugsnag() -> None:
    bugsnag.before_notify(bugsnag_add_batch_metadata)
    bugsnag.before_notify(bugsnag_remove_nix_hash)
    bugsnag.configure(
        notify_release_stages=["production"],
        release_stage=guess_environment(),
        send_environment=True,
    )
    bugsnag.start_session()
