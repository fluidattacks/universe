import json
from pathlib import Path
from typing import NamedTuple, cast

from ruamel.yaml import YAML

from skims_ai.classifies.ai_queries import (
    get_embeddings,
)

BUCKET_NAME = "skims.sca"

CRITERIA_EMBEDDINGS_LOCAL = "./outputs/criteria_embeddings.json"
CRITERIA_REQUIREMENTS = "../../common/criteria/src/vulnerabilities/data.yaml"

PRIORITIZED_FINDINGS = [
    "F002",
    "F003",
    "F004",
    "F005",
    "F006",
    "F007",
    "F008",
    "F009",
    "F013",
    "F014",
    "F017",
    "F020",
    "F021",
    "F026",
    "F027",
    "F028",
    "F029",
    "F032",
    "F034",
    "F035",
    "F037",
    "F038",
    "F039",
    "F041",
    "F042",
    "F044",
    "F045",
    "F047",
    "F048",
    "F050",
    "F052",
    "F053",
    "F056",
    "F061",
    "F062",
    "F063",
    "F065",
    "F067",
    "F068",
    "F069",
    "F072",
    "F076",
    "F078",
    "F079",
    "F081",
    "F082",
    "F083",
    "F085",
    "F086",
    "F087",
    "F088",
    "F090",
    "F091",
    "F093",
    "F095",
    "F096",
    "F097",
    "F098",
    "F100",
    "F106",
    "F108",
    "F110",
    "F111",
    "F113",
    "F115",
    "F117",
    "F120",
    "F121",
    "F123",
    "F124",
    "F126",
    "F138",
    "F156",
    "F157",
    "F159",
    "F161",
    "F163",
    "F164",
    "F183",
    "F184",
    "F200",
    "F201",
    "F204",
    "F205",
    "F211",
    "F231",
    "F274",
    "F279",
    "F280",
    "F297",
    "F308",
    "F310",
    "F332",
    "F360",
    "F390",
    "F404",
    "F410",
    "F416",
    "F417",
    "F420",
    "F422",
    "F425",
    "F431",
    "F448",
]


class CriteriaData(NamedTuple):
    fin_code: str
    fin_cwe: set[str]
    fin_embeddings: list[float]


def get_criteria_cwe() -> dict[str, set[str]]:
    cve_data_path = Path("./skims_ai/classifies/findings_cwe.json")
    with cve_data_path.open("rb") as f:
        criteria_cwe: dict[str, list[str]] = json.load(f)
        return {fin_code: set(fin_cwe) for fin_code, fin_cwe in criteria_cwe.items()}


def get_criteria_descriptions() -> dict[str, str]:
    criteria_data: dict[str, str] = {}
    yaml = YAML(typ="safe")
    with Path(CRITERIA_REQUIREMENTS).open(encoding="utf-8") as handle:
        yaml_data = cast(dict[str, dict[str, dict]], yaml.load(handle))
        for code, data in yaml_data.items():
            fin_code = f"F{code}"
            criteria_data[fin_code] = (
                f"Title: {data['en']['title']}\n"
                f"Description: {data['en']['description']}\n"
                f"Impact: {data['en']['impact']}\n"
                f"Recommendation: {data['en']['recommendation']}\n"
                f"Threat: {data['en']['threat']}"
            )
    return criteria_data


async def obtain_criteria_embeddings() -> dict[str, list[float]]:
    criteria_data = get_criteria_descriptions()
    criteria_embeddings = await get_embeddings(criteria_data)
    with Path(CRITERIA_EMBEDDINGS_LOCAL).open(mode="w") as handler:  # noqa:ASYNC230
        json.dump(criteria_embeddings, handler, indent=2)
    return criteria_embeddings


async def get_criteria_embeddings() -> dict[str, list[float]]:
    criteria_path = Path(CRITERIA_EMBEDDINGS_LOCAL)
    if criteria_path.is_file():
        with criteria_path.open("rb") as f:  # noqa: ASYNC230
            return json.load(f)
    return await obtain_criteria_embeddings()


async def get_criteria_data() -> dict[str, CriteriaData]:
    criteria_cwe = get_criteria_cwe()
    criteria_embeddings = await get_criteria_embeddings()

    return {
        fin_code: CriteriaData(
            fin_code=fin_code,
            fin_cwe=criteria_cwe[fin_code],
            fin_embeddings=fin_embeddings,
        )
        for fin_code, fin_embeddings in criteria_embeddings.items()
    }
