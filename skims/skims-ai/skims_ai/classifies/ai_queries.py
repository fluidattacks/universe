import json

from openai import BadRequestError

from skims_ai.ai.client import (
    openai_client,
)
from skims_ai.ai.open_ai import (
    assistant,
)
from skims_ai.config.logger import (
    LOGGER,
)


async def query_open_ai_for_embeddings(
    queries: list[str],
) -> list[list[float]]:
    result = await openai_client.embeddings.create(
        input=queries,
        model="text-embedding-3-small",
        encoding_format="float",
        dimensions=1024,
    )
    sorted_data = sorted(result.data, key=lambda x: x.index)
    return [d.embedding for d in sorted_data]


async def get_embeddings(queries_data: dict[str, str]) -> dict[str, list[float]]:
    batch_size = 264
    queries = list(queries_data.values())
    result_queries: list[list[float]] = []
    for i in range(0, len(queries), batch_size):
        query_emb = await query_open_ai_for_embeddings(queries[i : i + batch_size])
        result_queries.extend(query_emb)

    data_keys = list(queries_data.keys())
    return dict(zip(data_keys, result_queries, strict=True))


MAIN_PROMPT_FOR_CWE = """
You are a cybersecurity expert specializing in evaluating CVE descriptions to determine the best
CWE category to classify them.
Only use official CWE that are contained in the MITRE database and find the two most
appropriate for the CVE description that will be provided to you.
"""


async def obtain_best_cwe_match(advisory_id: str, cve_details: str) -> set[str]:
    try:
        context_messages = [
            {
                "role": "system",
                "content": MAIN_PROMPT_FOR_CWE,
            },
            {
                "role": "user",
                "content": (
                    "possible denial of service vulnerability in rack's header parsing "
                    "there is a denial of service vulnerability in the header parsing "
                    "component of rack. carefully crafted input can cause header parsing "
                    "in rack to take an unexpected amount of time, possibly resulting in a "
                    "denial of service attack vector"
                ),
            },
            {"role": "assistant", "content": '{"best_cwe_lists": ["CWE-1333", "CWE-400"]}'},
        ]
        classification = await assistant(
            context_messages=context_messages,
            prompt=cve_details,
            model="gpt-4o",
        )
        results = json.loads(classification)
        return {cwe for cwe in results["best_cwe_lists"] if cwe.startswith("CWE-")}
    except (json.JSONDecodeError, BadRequestError):
        LOGGER.error("Unable to get CWE for advisory %s", advisory_id)

    return set()


MAIN_PROMPT_FOR_XSS = """
You are a cybersecurity expert specializing in evaluating CVE descriptions to determine the best
vulnerability category to classify them.
In this case, the CVE is known to be an XSS vulnerability and you need to decide if the XSS is
reflected or stored.
A Reflected XSS occurs when a malicious script is reflected off of a web application to the
victim's browser, so usually, the cve will mention things like html, browser, dom, or it will
mention libraries used mainly for front-end development, like jquery or bootstrap.
A Stored XSS occurs when a malicious script is injected directly into a vulnerable web application.
In this case, it will mention libraries used for back-end development, like express or django.
Stored XSS is uncommon in a CVE, so you should try to classify it as a reflected XSS first and
only classify it as a Stored XSS if there is a strong reason to do so.
"""


async def verify_best_xss_match(initial_fin_code: str, cve_details: str) -> str:
    try:
        context_messages = [
            {
                "role": "system",
                "content": MAIN_PROMPT_FOR_XSS,
            },
            {
                "role": "user",
                "content": (
                    "cross-site scripting in jquery versions of `jquery` prior to 1.9.0 are "
                    "vulnerable to cross-site scripting. the load method fails to recognize and "
                    "remove `` html tags that contain a whitespace character, i.e: ``, which "
                    "results in the enclosed script logic to be executed. this allows attackers "
                    "to execute arbitrary javascript in a victim's browser. recommendation upgrade"
                    " to version 1.9.0 or later."
                ),
            },
            {"role": "assistant", "content": '{"correct_category": "Reflected"}'},
        ]
        classification = await assistant(
            context_messages=context_messages,
            prompt=cve_details,
            model="gpt-4o",
        )
        results = json.loads(classification)
        xss_type = results["correct_category"].lower()
        if xss_type == "reflected":
            return "F008"
        if xss_type == "stored":
            return "F425"
    except (json.JSONDecodeError, BadRequestError):
        LOGGER.error("Unable to get correct xss cwe for advisory %s", cve_details)

    return initial_fin_code
