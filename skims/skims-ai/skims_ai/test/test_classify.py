import logging
from unittest.mock import AsyncMock, patch

import pytest

from skims_ai.prioritizes.classify import classify_cves, is_automation_candidate
from skims_ai.prioritizes.defs import EnrichedCVE
from skims_ai.prioritizes.utils import get_context_messages

INPUT_CVE = EnrichedCVE(
    sbom_cve_id="CVE-2021-26540",
    cve_id="CVE-2021-26540",
    url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2021-26540", "CVE-2021-26540")',
    language="lang1",
    affected_groups="294",
    finding="F060",
    description="Description for CVE-2021-26540",
    pkg="pkg1",
    severity="3",
)

OUTPUT_CVE = EnrichedCVE(
    affected_groups="294",
    url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2021-26540", "CVE-2021-26540")',
    sbom_cve_id="CVE-2021-26540",
    cve_id="CVE-2021-26540",
    pkg="pkg1",
    finding="F060",
    language="lang1",
    severity="3",
    status="Implemented",
    reason="Skims method found by Prioritize",
    is_candidate=True,
)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("cve", "assistant_response", "result_expected"),
    [
        (
            EnrichedCVE(
                sbom_cve_id="CVE-2020-X1",
                cve_id="CVE-2020-X1",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X1", "CVE-2020-X1")',
                language="javascript",
                affected_groups="40",
                finding="-",
                pkg="slash",
                severity="4",
            ),
            '{"automatable": true}',
            EnrichedCVE(
                is_candidate=True,
                affected_groups="40",
                sbom_cve_id="CVE-2020-X1",
                cve_id="CVE-2020-X1",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X1", "CVE-2020-X1")',
                pkg="slash",
                finding="-",
                language="javascript",
                severity="4",
                status="Not implemented",
                reason="Lack of implementation",
            ),
        ),
        (
            EnrichedCVE(
                sbom_cve_id="CVE-2020-X2",
                cve_id="CVE-2020-X2",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X2", "CVE-2020-X2")',
                language="javascript",
                affected_groups="40",
                finding="F002",
                pkg="slash",
                severity="4",
            ),
            '{"automatable": true}',
            EnrichedCVE(
                is_candidate=True,
                affected_groups="40",
                sbom_cve_id="CVE-2020-X2",
                cve_id="CVE-2020-X2",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X2", "CVE-2020-X2")',
                pkg="slash",
                finding="F002",
                language="javascript",
                severity="4",
                status="Not implemented",
                reason="Lack of implementation",
            ),
        ),
        (
            EnrichedCVE(
                sbom_cve_id="CVE-2020-X3",
                cve_id="CVE-2020-X3",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X3", "CVE-2020-X3")',
                language="javascript",
                affected_groups="40",
                finding="-",
                pkg="slash",
                severity="4",
            ),
            '{"automatable": false}',
            EnrichedCVE(
                is_candidate=False,
                affected_groups="40",
                sbom_cve_id="CVE-2020-X3",
                cve_id="CVE-2020-X3",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X3", "CVE-2020-X3")',
                pkg="slash",
                finding="-",
                language="javascript",
                severity="4",
                status="Discarded",
                reason="Discarded by Prioritizes",
            ),
        ),
        (
            EnrichedCVE(
                sbom_cve_id="CVE-2020-X4",
                cve_id="CVE-2020-X4",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X4", "CVE-2020-X4")',
                language="javascript",
                affected_groups="40",
                finding="-",
                pkg="slash",
                severity="4",
            ),
            '{"Unparseable JSON"}',
            EnrichedCVE(
                sbom_cve_id="CVE-2020-X4",
                cve_id="CVE-2020-X4",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2020-X4", "CVE-2020-X4")',
                language="javascript",
                affected_groups="40",
                finding="-",
                pkg="slash",
                severity="4",
            ),
        ),
    ],
)
async def test_is_automation_candidate(
    cve: EnrichedCVE,
    assistant_response: str,
    result_expected: EnrichedCVE,
    caplog: pytest.LogCaptureFixture,
) -> None:
    with (
        caplog.at_level(logging.CRITICAL),
        patch("skims_ai.prioritizes.classify.assistant", return_value=assistant_response),
    ):
        result = await is_automation_candidate(cve=cve)
        assert result == result_expected


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("cves", "assistant_response"),
    [
        ([INPUT_CVE], '{"automatable": true}'),
        ([INPUT_CVE], '{"automatable": false}'),
        ([INPUT_CVE], "{}"),
    ],
)
async def test_classify_cves(cves: list[EnrichedCVE], assistant_response: str) -> None:
    context_messages = await get_context_messages()
    with (
        patch(
            "skims_ai.prioritizes.classify.assistant",
            new=AsyncMock(return_value=assistant_response),
        ) as mock_assistant,
    ):
        await classify_cves(cves=cves)
        mock_assistant.assert_called_once_with(
            context_messages=context_messages, prompt="Description for CVE-2021-26540"
        )
