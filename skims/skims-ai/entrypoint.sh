# shellcheck shell=bash

function run {
  sops_export_vars __argSecrets__ OPENAI_API_KEY \
    FI_AWS_OPENSEARCH_HOST \
    GOOGLE_SHEETS_ID

  GOOGLE_SHEETS_TOKEN="$(sops --decrypt __argGoogleSheetsToken__)"
  export GOOGLE_SHEETS_TOKEN
  export BUGSNAG_API_KEY="f990c9a571de4cb44c96050ff0d50ddb"

  poetry run skims_ai "${@}"
}

function _lint {
  if [[ ${CI:-} == "" ]]; then
    poetry run ruff format --config ruff.toml
    poetry run ruff check --config ruff.toml --fix
  else
    poetry run ruff format --config ruff.toml --diff
    poetry run ruff check --config ruff.toml
  fi

  poetry run mypy --config-file mypy.ini skims_ai
}

function test {
  export OPENAI_API_KEY="FAKE_API_KEY"

  poetry run coverage run -m pytest -rP skims_ai/test "${@}"
  poetry run coverage report -m
}

function main {
  local dir="skims/skims-ai"

  pushd ${dir} || error "${dir} directory not found"
  poetry install

  case "${1:-}" in
    lint) _lint "${@:2}" ;;
    test) test "${@:2}" ;;
    *) run "${@}" ;;
  esac
}

main "${@}"
