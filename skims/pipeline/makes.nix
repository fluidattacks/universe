{ inputs, lib, projectPath, ... }:
let
  arch = let
    commit = "3c8778c732597a2fdee42d7c20d1ab5d948e13d4";
    sha256 = "sha256:0ii4qam4ykq9gbi2lmkk9p9gixmrypmz54jgcji9hpypk4azxy3w";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  categoriesJson = builtins.listToAttrs (map (key: {
    name = key;
    value = inputs.skimsTestsMap.${key};
  }) inputs.skimsCICategories);

  all = let products = [ "skims-.+" "skims" "all" ];
  in {
    default = arch.core.rules.titleRule {
      inherit products;
      types = [ ];
    };
    noRotate = arch.core.rules.titleRule {
      inherit products;
      types = [ "feat" "fix" "refac" ];
    };
    skimsScanner = arch.core.rules.titleRule {
      products = [
        "all"
        "skims"
        "skims-apk"
        "skims-cspm"
        "skims-dast"
        "skims-sast"
        "skims-sca"
      ];
      types = [ "feat" "fix" "refac" ];
    };
    skimsAi = arch.core.rules.titleRule {
      products = [ "all" "skims" "skims-ai" ];
      types = [ "feat" "fix" "refac" ];
    };
    skimsSbom = arch.core.rules.titleRule {
      products = [ "all" "skims" "skims-sbom" ];
      types = [ "feat" "fix" "refac" ];
    };
  };

  testTitleRule = { modulesToRun }:
    arch.core.rules.titleRule {
      products = modulesToRun ++ [ "skims" "all" ];
      types = [ "feat" "fix" "refac" ];
    };

  assignTestTitleRule = category:
    if lib.hasAttr category categoriesJson then
      testTitleRule { modulesToRun = categoriesJson.${category}; }
    else
      all.noRotate;

in {
  pipelines = {
    skimsDefault = {
      gitlabPath = "/skims/pipeline/default.yaml";
      jobs = lib.flatten [
        {
          output = "/lintTerraform/skims";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/testTerraform/skims";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/deployTerraform/skims";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/skims/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-skims-lint";
              paths = [
                "skims/.ruff_cache"
                "skims/.import_linter_cache"
                "skims/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/skims/machine/sca";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.default ];
            script = [ "m . /skims scan $PWD/skims/sca_config.yaml" ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/pipelineOnGitlab/skimsDefault";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/pipelineOnGitlab/skimsTests";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/deployContainer/skimsAmd64";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common-x86 ];
          };
        }
        {
          output = "/deployContainer/skimsArm64";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.skims ];
            retry = {
              max = 2;
              when = [ "runner_system_failure" "script_failure" ];
            };
          };
        }
        {
          output = "/deployContainerManifest/skims";
          gitlabExtra = arch.extras.default // {
            needs =
              [ "/deployContainer/skimsAmd64" "/deployContainer/skimsArm64" ];
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/skims/coverage";
          gitlabExtra = arch.extras.default // {
            needs = null;
            rules = arch.rules.dev ++ [ all.skimsScanner ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.skims ];
            variables.GIT_DEPTH = 1000;
          };
        }
        {
          output = "/skims/skims-ai lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.skimsAi ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
            needs = null;
          };
        }
        {
          output = "/skims/skims-ai test";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.skimsAi ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
            needs = null;
          };
        }
        {
          output = "/common/utils/pypi-deploy fluid-sbom skims/sbom";
          gitlabExtra = arch.extras.default // {
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ all.skimsSbom ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/skims/sbom/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-skims-sbom-lint";
              paths = [
                "skims/sbom/.ruff_cache"
                "skims/sbom/.import_linter_cache"
                "skims/sbom/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ all.skimsSbom ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/skims/sbom/test/unit";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.skimsSbom ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
        {
          output = "/skims/sbom/test/functional reports";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ all.skimsSbom ];
            stage = arch.stages.test;
            tags = [ arch.tags.skims ];
          };
        }
      ];
    };
    skimsTests = {
      gitlabPath = "/skims/pipeline/tests.yaml";
      jobs = builtins.map (category: {
        output = "/testPython/skims@${category}";
        gitlabExtra = arch.extras.default // {
          artifacts = {
            paths = [ "skims/.coverage.*" ];
            expire_in = "1 day";
          };
          rules = arch.rules.dev ++ [ (assignTestTitleRule category) ];
          stage = arch.stages.test;
          tags = [ arch.tags.skims ];
        };
      }) inputs.skimsCICategories;
    };
  };
}
