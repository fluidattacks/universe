{ inputs, makeContainerImage, makeDerivation, outputs, ... }:
let
  credentials = {
    token = "DOCKER_HUB_PASS";
    user = "DOCKER_HUB_USER";
  };
  images = {
    amd64 = "docker.io/fluidattacks/cli:amd64";
    arm64 = "docker.io/fluidattacks/cli:arm64";
    latest = "docker.io/fluidattacks/cli:latest";
  };
  setup = [
    outputs."/secretsForAwsFromGitlab/dev"
    outputs."/secretsForEnvFromSops/skimsContainer"
  ];
  src = outputs."/skims/container";
in {
  deployContainer = {
    skimsAmd64 = {
      image = images.amd64;
      inherit credentials setup src;
    };
    skimsArm64 = {
      image = images.arm64;
      inherit credentials setup src;
    };
  };
  deployContainerManifest = {
    skims = {
      image = images.latest;
      manifests = [
        {
          image = images.amd64;
          platform = {
            architecture = "amd64";
            os = "linux";
          };
        }
        {
          image = images.arm64;
          platform = {
            architecture = "arm64";
            os = "linux";
          };
        }
      ];
      inherit credentials setup;
    };
  };
  secretsForEnvFromSops = {
    skimsContainer = {
      vars = [ "DOCKER_HUB_PASS" "DOCKER_HUB_USER" ];
      manifest = "/common/secrets/dev.yaml";
    };
  };
  jobs."/skims/container" = makeContainerImage {
    config.WorkingDir = "/src";
    extraCommands = ''
      mkdir -m 1777 tmp
      mkdir -m 1777 .cache
      mkdir -m 1777 .skims
    '';
    layers = [ inputs.nixpkgs.bash inputs.nixpkgs.coreutils outputs."/skims" ];
  };
}
