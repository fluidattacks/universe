# https://github.com/fluidattacks/makes
{ makeScript, outputs, projectPath, ... }: {
  imports = [
    ./benchmark/owasp/makes.nix
    ./benchmark/owasp/upload/makes.nix
    ./container/makes.nix
    ./coverage/makes.nix
    ./config/runtime/parsers/makes.nix
    ./config/runtime/makes.nix
    ./dev/makes.nix
    ./infra/makes.nix
    ./inputs/makes.nix
    ./lint/makes.nix
    ./sca/scheduler/makes.nix
    ./test/makes.nix
    ./test/hooks/pre-push/makes.nix
    ./test/mocks/http/makes.nix
    ./test/mocks/ssl/safe/makes.nix
    ./test/mocks/ssl/unsafe/makes.nix
    ./pipeline/makes.nix
    ./skims-ai/makes.nix
    ./sbom/test/unit/makes.nix
    ./sbom/test/functional/makes.nix
    ./sbom/config/runtime/makes.nix
    ./sbom/config/runtime/parsers/makes.nix
    ./sbom/schedules/refresh-advisories/build-db/grype-db/makes.nix
    ./sbom/schedules/refresh-advisories/list-providers/makes.nix
    ./sbom/schedules/refresh-advisories/merge-providers/makes.nix
    ./sbom/schedules/refresh-advisories/update-provider/makes.nix
    ./sbom/makes.nix
  ];
  inputs = {
    observesIndex = import (projectPath "/observes/architecture/index.nix");
  };
  secretsForAwsFromGitlab = {
    prodSkims = {
      roleArn = "arn:aws:iam::205810638802:role/prod_skims";
      duration = 3600;
    };
  };
  jobs."/skims" = makeScript {
    name = "skims";
    entrypoint = ./entrypoint.sh;
    searchPaths = { source = [ outputs."/skims/config/runtime" ]; };
  };
}
