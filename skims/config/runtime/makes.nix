{ inputs, makeTemplate, makePythonEnvironment, makeSearchPaths, projectPath
, outputs, ... }:
let
  pythonEnvironment = makePythonEnvironment {
    pythonProjectDir = projectPath "/skims";
    pythonVersion = "3.11";
    overrides = _self: super: {
      cyclonedx-python-lib = super.cyclonedx-python-lib.overridePythonAttrs
        (old: {
          preUnpack = ''
            export HOME=$(mktemp -d)
            rm -rf /homeless-shelter
          '' + (old.preUnpack or "");
          # Skip https://github.com/nix-community/poetry2nix/blob/master/overrides/default.nix#L588
          postPatch = "echo Skipping postPatch";
        });
      cpe = super.cpe.overridePythonAttrs (old: {
        preUnpack = ''
          export HOME=$(mktemp -d)
          rm -rf /homeless-shelter
        '' + (old.preUnpack or "");
        # Remove https://github.com/nix-community/poetry2nix/blob/master/overrides/default.nix#L2735
        buildInputs = [ super.setuptools ];
      });
      grimp = super.grimp.overridePythonAttrs (old: {
        preUnpack = ''
          export HOME=$(mktemp -d)
          rm -rf /homeless-shelter
        '' + (old.preUnpack or "");
        buildInputs = [ super.setuptools ];
      });
    };
  };
  skimsPythonEnvironment = makeSearchPaths {
    bin =
      [ inputs.nixpkgs.findutils inputs.nixpkgs.jadx inputs.nixpkgs.gnused ];
    export = [
      [ "CPATH" inputs.nixpkgs.graphviz "/include" ]
      [ "LIBRARY_PATH" inputs.nixpkgs.graphviz "/lib" ]
    ];
    source = [ pythonEnvironment ];
  };
in {
  jobs."/skims/config/runtime" = makeTemplate {
    replace = { __argSrcSkimsSkims__ = projectPath "/skims/skims"; };
    name = "skims-config-runtime";
    searchPaths = {
      bin = [ inputs.nixpkgs.git inputs.nixpkgs.graphviz inputs.nixpkgs.zstd ];
      pythonPackage = [
        "${inputs.nixpkgs.python311Packages.python_magic}/lib/python3.11/site-packages/"
        (projectPath "/skims/skims")
      ];
      source = [
        skimsPythonEnvironment
        (makeTemplate {
          replace = {
            __argCriteriaRequirements__ =
              projectPath "/common/criteria/src/requirements/data.yaml";
            __argCriteriaVulnerabilities__ =
              projectPath "/common/criteria/src/vulnerabilities/data.yaml";
            __argSrcTreeSitterParsers__ =
              outputs."/skims/config/runtime/parsers";
            __argSrcSkimsPath__ = projectPath "/skims/skims";
          };
          name = "skims-config-context-file";
          template = ''
            # There is no problem in making this key public
            # it's intentional so we can monitor Skims stability in remote users
            export BUGSNAG_API_KEY=f990c9a571de4cb44c96050ff0d50ddb
            export SKIMS_CRITERIA_REQUIREMENTS='__argCriteriaRequirements__'
            export SKIMS_CRITERIA_VULNERABILITIES='__argCriteriaVulnerabilities__'
            export SKIMS_PROJECT_PATH='__argSrcSkimsPath__'
            export TREE_SITTER_PARSERS_DIR='__argSrcTreeSitterParsers__'
            export TREE_SITTER_PARSERS='__argSrcTreeSitterParsers__'
          '';
        })
      ];
    };
    template = ./template.sh;
  };
}
