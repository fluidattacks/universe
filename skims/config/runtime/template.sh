# shellcheck shell=bash

if test -n "${HOME_IMPURE-}"; then
  export HOME="${HOME_IMPURE}"
fi
export PYTHONHASHSEED=0

function skims {
  export CORALOGIX_LOG_URL="https://ingress.cx498-aws-us-west-2.coralogix.com:443/api/v1/logs"
  python '__argSrcSkimsSkims__/core/cli.py' "$@"
}
