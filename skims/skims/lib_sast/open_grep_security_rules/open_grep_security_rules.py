import logging
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any

import git
import yaml

REPO_URL = "https://github.com/opengrep/opengrep-rules.git"
GITHUB_BASE_URL = "https://github.com/opengrep/opengrep-rules/blob/main/"
LANGUAGES = [
    "csharp",
    "dockerfile",
    "go",
    "java",
    "javascript",
    "kotlin",
    "python",
    "ruby",
    "scala",
    "swift",
    "terraform",
    "typescript",
    "html",
    "yaml",
    "json",
    "php",
    "dart",
]

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")
logger = logging.getLogger(__name__)


def fetch_yaml_files(language: str, base_dir: Path) -> list[dict[str, Any]]:
    language_dir = base_dir / language
    if not language_dir.exists():
        logger.info("Directory for language %s does not exist %s", language, language_dir)
        return []

    yaml_files = []
    for path in language_dir.rglob("security/**/*.yaml"):
        yaml_files.append(path)  # noqa: PERF402

    if not yaml_files:
        logger.info("No YAML files found in directory")
    else:
        logger.info("YAML files found in directory")

    rules = []
    for file_path in yaml_files:
        rules.extend(extract_rules_from_yaml(file_path, base_dir, language))

    return rules


def extract_rules_from_yaml(file_path: Path, base_dir: Path, language: str) -> list[dict[str, Any]]:
    with file_path.open(encoding="utf-8") as file:
        yaml_content = yaml.safe_load(file)

    relative_path = file_path.relative_to(base_dir)
    github_yaml_url = GITHUB_BASE_URL + str(relative_path).replace("\\", "/")
    code_file_path = file_path.with_suffix(get_extension_from_language(language))
    relative_code_path = code_file_path.relative_to(base_dir)
    github_code_url = GITHUB_BASE_URL + str(relative_code_path).replace("\\", "/")

    return [
        {
            "id": rule.get("id"),
            "description": rule.get("description"),
            "severity": rule.get("severity"),
            "file_link": github_yaml_url,
            "code_url": github_code_url,
        }
        for rule in yaml_content.get("rules", [])
    ]


def get_extension_from_language(language: str) -> str:
    extensions = {
        "csharp": ".cs",
        "dockerfile": ".dockerfile",
        "go": ".go",
        "java": ".java",
        "javascript": ".js",
        "kotlin": ".kt",
        "python": ".py",
        "ruby": ".rb",
        "scala": ".scala",
        "swift": ".swift",
        "terraform": ".tf",
        "typescript": ".ts",
        "html": ".html",
        "yaml": ".yaml",
        "json": ".json",
        "php": ".php",
        "dart": ".dart",
    }
    return extensions.get(language, "")


def get_security_rules() -> list[dict[str, Any]]:
    with TemporaryDirectory() as tmp_dir:
        tmp_path = Path(tmp_dir)
        try:
            git.Repo.clone_from(REPO_URL, tmp_path, depth=1)
        except git.GitError:
            logger.exception("Error cloning repository: %s", REPO_URL)
            raise

        all_rules = []
        for language in LANGUAGES:
            all_rules.extend(fetch_yaml_files(language, tmp_path))
        return all_rules


if __name__ == "__main__":
    security_rules = get_security_rules()
    for rule in security_rules:
        logger.info(
            "ID: %s, Description: %s, Severity: %s, Link: %s, Code URL: %s",
            rule["id"],
            rule["description"],
            rule["severity"],
            rule["file_link"],
            rule["code_url"],
        )
