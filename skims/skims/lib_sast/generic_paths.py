from concurrent.futures.process import (
    ProcessPoolExecutor,
)
from typing import TYPE_CHECKING

import ctx
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    FindingEnum,
    MethodExecutionResult,
    SkimsConfig,
    Vulnerability,
)
from utils.fs import (
    get_file_raw_content_blocking,
)
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
)
from utils.state import (
    VulnerabilitiesEphemeralStore,
)

if TYPE_CHECKING:
    from concurrent.futures import (
        Future,
    )

MAX_READ = 10240


@SHIELD_BLOCKING
def non_upgradeable_deps(path: str) -> MethodExecutionResult:
    method = MethodsEnum.NON_UPGRADEABLE_DEPS
    raw_content = get_file_raw_content_blocking(path, size=MAX_READ)
    return get_vulnerabilities_from_iterator_blocking(
        content=raw_content.decode(encoding="utf-8", errors="replace"),
        iterator=iter([(1, 0)]),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def unverifiable_files(path: str) -> MethodExecutionResult:
    # Integrates apk files are downloaded to this folder
    # See integrates/jobs/execute_machine/src/__init__.py
    method = MethodsEnum.UNVERIFIABLE_FILES
    if path.endswith(".apk") and "fa_apks_to_analyze" in path:
        return MethodExecutionResult(vulnerabilities=())

    raw_content = get_file_raw_content_blocking(path, size=MAX_READ)
    return get_vulnerabilities_from_iterator_blocking(
        content=raw_content.decode(encoding="utf-8", errors="replace"),
        iterator=iter([(0, 0)]),
        path=path,
        method=method,
    )


def _analyze_nv_path(path: str, config: SkimsConfig) -> MethodExecutionResult:
    # Re-export config to gain visibility in child subprocesses
    ctx.SKIMS_CONFIG = config
    return unverifiable_files(path=path)


def _analyze_nu_path(path: str, config: SkimsConfig) -> MethodExecutionResult:
    # Re-export config to gain visibility in child subprocesses
    ctx.SKIMS_CONFIG = config
    return non_upgradeable_deps(path=path)


def analyze_not_ok_paths(
    *,
    nv_paths: tuple[str, ...],
    nu_paths: tuple[str, ...],
    stores: VulnerabilitiesEphemeralStore,
) -> None:
    if not (nv_paths or nu_paths) or not any(
        finding in ctx.SKIMS_CONFIG.checks for finding in [FindingEnum.F079, FindingEnum.F117]
    ):
        return

    log_blocking("info", "Analyzing unverifiable and non-upgradable paths")

    with ProcessPoolExecutor() as executor:
        futures: list[Future] = []
        vulnerabilities: list[Vulnerability] = []
        if FindingEnum.F079 in ctx.SKIMS_CONFIG.checks:
            futures += [
                executor.submit(_analyze_nu_path, path, ctx.SKIMS_CONFIG) for path in nu_paths
            ]
        if FindingEnum.F117 in ctx.SKIMS_CONFIG.checks:
            futures += [
                executor.submit(_analyze_nv_path, path, ctx.SKIMS_CONFIG) for path in nv_paths
            ]

        for future in futures:
            try:
                execution_results = future.result(timeout=30)
                vulnerabilities += list(execution_results.vulnerabilities)
            except TimeoutError:
                msg = "Time-out error verifying generic paths"
                log_blocking("error", msg)
                log_to_remote_blocking(msg=msg, severity="error")
                future.cancel()

        stores.store_vulns(vulnerabilities)
