from lib_sast.symbolic_eval.f035.literal import (
    evaluate as evaluate_literal_f035,
)
from lib_sast.symbolic_eval.f042.literal import (
    evaluate as evaluate_literal_f042,
)
from lib_sast.symbolic_eval.f052.literal import (
    evaluate as evaluate_literal_f052,
)
from lib_sast.symbolic_eval.f060.literal import (
    evaluate as evaluate_literal_f060,
)
from lib_sast.symbolic_eval.f063.literal import (
    evaluate as evaluate_literal_f063,
)
from lib_sast.symbolic_eval.f083.literal import (
    evaluate as evaluate_literal_f083,
)
from lib_sast.symbolic_eval.f097.literal import (
    evaluate as evaluate_literal_f097,
)
from lib_sast.symbolic_eval.f106.literal import (
    evaluate as evaluate_literal_f106,
)
from lib_sast.symbolic_eval.f107.literal import (
    evaluate as evaluate_literal_f107,
)
from lib_sast.symbolic_eval.f128.literal import (
    evaluate as evaluate_literal_f128,
)
from lib_sast.symbolic_eval.f130.literal import (
    evaluate as evaluate_literal_f130,
)
from lib_sast.symbolic_eval.f134.literal import (
    evaluate as evaluate_literal_f134,
)
from lib_sast.symbolic_eval.f135.literal import (
    evaluate as evaluate_literal_f135,
)
from lib_sast.symbolic_eval.f152.literal import (
    evaluate as evaluate_literal_f152,
)
from lib_sast.symbolic_eval.f153.literal import (
    evaluate as evaluate_literal_f153,
)
from lib_sast.symbolic_eval.f160.literal import (
    evaluate as evaluate_literal_f160,
)
from lib_sast.symbolic_eval.f211.literal import (
    evaluate as evaluate_literal_f211,
)
from lib_sast.symbolic_eval.f239.literal import (
    evaluate as evaluate_literal_f239,
)
from lib_sast.symbolic_eval.f280.literal import (
    evaluate as evaluate_literal_f280,
)
from lib_sast.symbolic_eval.f297.literal import (
    evaluate as evaluate_literal_f297,
)
from lib_sast.symbolic_eval.f309.literal import (
    evaluate as evaluate_literal_f309,
)
from lib_sast.symbolic_eval.f313.literal import (
    evaluate as evaluate_literal_f313,
)
from lib_sast.symbolic_eval.f343.literal import (
    evaluate as evaluate_literal_f343,
)
from lib_sast.symbolic_eval.f354.literal import (
    evaluate as evaluate_literal_f354,
)
from lib_sast.symbolic_eval.f359.literal import (
    evaluate as evaluate_literal_f359,
)
from lib_sast.symbolic_eval.f368.literal import (
    evaluate as evaluate_literal_f368,
)
from lib_sast.symbolic_eval.f414.literal import (
    evaluate as evaluate_literal_f414,
)
from lib_sast.symbolic_eval.f421.literal import (
    evaluate as evaluate_literal_f421,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F035: evaluate_literal_f035,
    FindingEnum.F042: evaluate_literal_f042,
    FindingEnum.F052: evaluate_literal_f052,
    FindingEnum.F060: evaluate_literal_f060,
    FindingEnum.F063: evaluate_literal_f063,
    FindingEnum.F083: evaluate_literal_f083,
    FindingEnum.F097: evaluate_literal_f097,
    FindingEnum.F106: evaluate_literal_f106,
    FindingEnum.F107: evaluate_literal_f107,
    FindingEnum.F128: evaluate_literal_f128,
    FindingEnum.F130: evaluate_literal_f130,
    FindingEnum.F134: evaluate_literal_f134,
    FindingEnum.F135: evaluate_literal_f135,
    FindingEnum.F152: evaluate_literal_f152,
    FindingEnum.F153: evaluate_literal_f153,
    FindingEnum.F160: evaluate_literal_f160,
    FindingEnum.F211: evaluate_literal_f211,
    FindingEnum.F239: evaluate_literal_f239,
    FindingEnum.F280: evaluate_literal_f280,
    FindingEnum.F297: evaluate_literal_f297,
    FindingEnum.F309: evaluate_literal_f309,
    FindingEnum.F313: evaluate_literal_f313,
    FindingEnum.F343: evaluate_literal_f343,
    FindingEnum.F354: evaluate_literal_f354,
    FindingEnum.F359: evaluate_literal_f359,
    FindingEnum.F368: evaluate_literal_f368,
    FindingEnum.F414: evaluate_literal_f414,
    FindingEnum.F421: evaluate_literal_f421,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    s_ids = adj_ast(args.graph, args.n_id)
    if len(s_ids) > 0:
        danger = [args.generic(args.fork_n_id(_id)).danger for _id in s_ids]
        args.evaluation[args.n_id] = any(danger)

    if args.method_evaluators and (method_evaluator := args.method_evaluators.get("literal")):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
