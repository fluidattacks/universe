from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    init_id = args.graph.nodes[args.n_id]["operand_id"]
    args.evaluation[args.n_id] = args.generic(args.fork_n_id(init_id)).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
