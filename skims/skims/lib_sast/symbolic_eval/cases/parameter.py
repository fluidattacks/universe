from lib_sast.symbolic_eval.f063.parameter import (
    evaluate as evaluate_parameter_f063,
)
from lib_sast.symbolic_eval.f083.parameter import (
    evaluate as evaluate_parameter_f083,
)
from lib_sast.symbolic_eval.f089.parameter import (
    evaluate as evaluate_parameter_f089,
)
from lib_sast.symbolic_eval.f091.parameter import (
    evaluate as evaluate_parameter_f091,
)
from lib_sast.symbolic_eval.f098.parameter import (
    evaluate as evaluate_parameter_f098,
)
from lib_sast.symbolic_eval.f100.parameter import (
    evaluate as evaluate_parameter_f100,
)
from lib_sast.symbolic_eval.f107.parameter import (
    evaluate as evaluate_parameter_f107,
)
from lib_sast.symbolic_eval.f128.parameter import (
    evaluate as evaluate_parameter_f128,
)
from lib_sast.symbolic_eval.f130.parameter import (
    evaluate as evaluate_parameter_f130,
)
from lib_sast.symbolic_eval.f136.parameter import (
    evaluate as evaluate_parameter_f136,
)
from lib_sast.symbolic_eval.f146.parameter import (
    evaluate as evaluate_parameter_f146,
)
from lib_sast.symbolic_eval.f211.parameter import (
    evaluate as evaluate_parameter_f211,
)
from lib_sast.symbolic_eval.f280.parameter import (
    evaluate as evaluate_parameter_f280,
)
from lib_sast.symbolic_eval.f297.parameter import (
    evaluate as evaluate_parameter_f297,
)
from lib_sast.symbolic_eval.f404.parameter import (
    evaluate as evaluate_parameter_f404,
)
from lib_sast.symbolic_eval.f413.parameter import (
    evaluate as evaluate_parameter_f413,
)
from lib_sast.symbolic_eval.f416.parameter import (
    evaluate as evaluate_parameter_f416,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F063: evaluate_parameter_f063,
    FindingEnum.F083: evaluate_parameter_f083,
    FindingEnum.F089: evaluate_parameter_f089,
    FindingEnum.F091: evaluate_parameter_f091,
    FindingEnum.F098: evaluate_parameter_f098,
    FindingEnum.F100: evaluate_parameter_f100,
    FindingEnum.F107: evaluate_parameter_f107,
    FindingEnum.F128: evaluate_parameter_f128,
    FindingEnum.F130: evaluate_parameter_f130,
    FindingEnum.F136: evaluate_parameter_f136,
    FindingEnum.F146: evaluate_parameter_f146,
    FindingEnum.F211: evaluate_parameter_f211,
    FindingEnum.F280: evaluate_parameter_f280,
    FindingEnum.F297: evaluate_parameter_f297,
    FindingEnum.F404: evaluate_parameter_f404,
    FindingEnum.F413: evaluate_parameter_f413,
    FindingEnum.F416: evaluate_parameter_f416,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if val_id := args.graph.nodes[args.n_id].get("value_id"):
        args.evaluation[args.n_id] = args.generic(args.fork_n_id(val_id)).danger

    if args.method_evaluators and (method_evaluator := args.method_evaluators.get("parameter")):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
