from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_weak_credential(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    args.triggers.add(args.graph.nodes[args.n_id]["value"])

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
