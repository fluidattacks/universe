from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def common_nosql_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    value = args.graph.nodes[args.n_id].get("value", "")
    if "`" in value and "${" in value:
        args.triggers.add("NonParametrizedQuery")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
