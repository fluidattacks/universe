from lib_sast.symbolic_eval.common import (
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_insecure_assembly_load(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = args.graph.nodes[args.n_id]["member"] in NET_SYSTEM_HTTP_METHODS
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
