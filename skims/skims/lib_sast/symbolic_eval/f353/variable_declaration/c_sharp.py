from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def c_sharp_token_validation_checks(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if (var_type := args.graph.nodes[args.n_id].get("variable_type")) and (
        var_type == "TokenValidationParameters"
    ):
        args.triggers.add("token")
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
