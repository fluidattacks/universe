from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)


def xml_parser(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    graph = args.graph

    if (
        graph.nodes[args.n_id]["expression"] in {"setFeature", "setAttribute", "setProperty"}
        and (arg_list := match_ast_d(graph, args.n_id, "ArgumentList"))
        and (arg_features := adj_ast(graph, arg_list))
        and len(arg_features) >= 2
    ):
        args.triggers.add("sanitizedParser")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
