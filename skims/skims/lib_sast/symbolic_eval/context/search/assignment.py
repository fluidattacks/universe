from collections.abc import (
    Iterator,
)

from lib_sast.symbolic_eval.context.search.model import (
    SearchArgs,
    SearchResult,
)


def search(args: SearchArgs) -> Iterator[SearchResult]:
    assign_id = args.graph.nodes[args.n_id]["variable_id"]

    if args.symbol == args.graph.nodes[assign_id].get("symbol"):
        yield True, args.n_id
    elif (
        not args.def_only
        and (expr := args.graph.nodes[assign_id].get("expression"))
        and args.symbol in expr
    ):
        yield False, args.n_id
