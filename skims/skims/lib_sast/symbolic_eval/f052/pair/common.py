from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils import (
    graph as g,
)


def insecure_mode(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    key = n_attrs["key_id"]
    value = n_attrs["value_id"]
    if args.graph.nodes[key].get("symbol") == "mode":
        val_danger = args.generic(args.fork(n_id=value)).danger
        if val_danger:
            args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def insecure_key_pair(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    key = n_attrs["key_id"]
    value = n_attrs["value_id"]
    key_name = args.graph.nodes[key].get("symbol")

    if (
        key_name
        and key_name.lower() == "moduluslength"
        and args.generic(args.fork(n_id=value)).danger
    ):
        args.evaluation[args.n_id] = True
        args.triggers.add("unsafemodulus")

    if key_name and key_name.lower() == "namedcurve" and args.generic(args.fork(n_id=value)).danger:
        args.evaluation[args.n_id] = True
        args.triggers.add("unsafecurve")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def check_symbol(graph: Graph, symbol: str) -> NId | None:
    nodes = graph.nodes

    for n_id in g.matching_nodes(graph, label_type="VariableDeclaration", variable=symbol):
        if (
            (val_id := nodes[n_id].get("value_id"))
            and (nodes[val_id].get("label_type") == "Literal")
            and (nodes[val_id].get("value").lower() == "hs256")
        ):
            return val_id

    return None
