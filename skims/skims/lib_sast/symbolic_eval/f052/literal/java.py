from contextlib import (
    suppress,
)

from lib_sast.symbolic_eval.common import (
    INSECURE_HASHES,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from utils.cipher_suites import (
    is_iana_cipher_suite_vulnerable,
)
from utils.crypto import (
    insecure_elliptic_curve,
    is_vulnerable_cipher,
    is_vulnerable_cipher_no_mode,
)

DANGER_SSL_METHODS = {"ssl", "sslv3", "tlsv1", "tlsv1.1"}


def java_insecure_hash(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if args.graph.nodes[args.n_id]["value_type"] == "string":
        member_str = args.graph.nodes[args.n_id]["value"]
        if member_str.lower() in INSECURE_HASHES:
            args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_insecure_key_rsa(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs["value_type"] == "number":
        key_value = n_attrs["value"]
        with suppress(TypeError):
            key_length = int(key_value)
            args.evaluation[args.n_id] = key_length < 2048

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_insecure_key_ec(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs["value_type"] == "string":
        key_value = n_attrs["value"]
        args.evaluation[args.n_id] = insecure_elliptic_curve(key_value)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_evaluate_cipher_mode(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs["value_type"] == "string":
        key_value = n_attrs["value"]
        alg, mode, pad, *_ = (key_value.lower() + "///").split("/", 3)

        args.evaluation[args.n_id] = is_vulnerable_cipher(alg, mode, pad)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_evaluate_cipher(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs["value_type"] == "string":
        key_value = n_attrs["value"]
        alg = key_value.lower()

        args.evaluation[args.n_id] = is_vulnerable_cipher_no_mode(alg)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_evaluate_key_spec(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]

    if n_attrs["value_type"] == "string":
        key_value = n_attrs["value"]
        alg = key_value.lower()

        args.evaluation[args.n_id] = is_vulnerable_cipher_no_mode(alg)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_insecure_cipher_ssl(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if (
        args.graph.nodes[args.n_id]["value_type"] == "string"
        and (cipher_ssl := args.graph.nodes[args.n_id]["value"])
        and str(cipher_ssl).lower() in DANGER_SSL_METHODS
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_engine_cipher_ssl(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if (
        args.graph.nodes[args.n_id]["value_type"] == "string"
        and (cipher_ssl := args.graph.nodes[args.n_id]["value"])
        and str(cipher_ssl).lower() in DANGER_SSL_METHODS
    ):
        args.evaluation[args.n_id] = True
        args.triggers.add("danger_protocol")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_insecure_cipher_jmqi(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs["value_type"] == "string":
        iana_cipher = n_attrs["value"]
        args.evaluation[args.n_id] = is_iana_cipher_suite_vulnerable(iana_cipher)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_uses_unsafe_alg(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    node = args.graph.nodes[args.n_id]
    dang_values: set[str] = {"hmacsha256"}

    if (node_val := node.get("value")) and (node_val.lower() in dang_values):
        args.triggers.add(node_val.lower())
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
