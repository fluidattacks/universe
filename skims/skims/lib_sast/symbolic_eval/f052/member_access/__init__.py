from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f052.member_access.c_sharp import (
    cs_managed_secure_mode,
)
from lib_sast.symbolic_eval.f052.member_access.common import (
    insecure_mode,
)
from lib_sast.symbolic_eval.f052.member_access.go import (
    go_comes_from_jwt,
)
from lib_sast.symbolic_eval.f052.member_access.kotlin import (
    kt_insecure_cipher_http,
    kt_insecure_init_vector,
)
from lib_sast.symbolic_eval.f052.member_access.python import (
    py_insecure_cipher_mode,
    python_unsafe_ciphers,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.C_SHARP_MANAGED_SECURE_MODE: cs_managed_secure_mode,
    MethodsEnum.GO_HARDCODED_SYMMETRIC_KEY: go_comes_from_jwt,
    MethodsEnum.JAVASCRIPT_INSECURE_ENCRYPT: insecure_mode,
    MethodsEnum.TYPESCRIPT_INSECURE_ENCRYPT: insecure_mode,
    MethodsEnum.KOTLIN_INSECURE_CIPHER_HTTP: kt_insecure_cipher_http,
    MethodsEnum.PYTHON_UNSAFE_CIPHER: python_unsafe_ciphers,
    MethodsEnum.PYTHON_INSECURE_CIPHER_MODE: py_insecure_cipher_mode,
    MethodsEnum.KT_INSECURE_INIT_VECTOR: kt_insecure_init_vector,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
