from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f052.object.common import (
    insecure_encrypt,
)
from lib_sast.symbolic_eval.f052.object.kotlin import (
    kt_insecure_cert,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVASCRIPT_INSECURE_ENCRYPT: insecure_encrypt,
    MethodsEnum.TYPESCRIPT_INSECURE_ENCRYPT: insecure_encrypt,
    MethodsEnum.KT_INSECURE_CERTIFICATE_VALIDATION: kt_insecure_cert,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
