from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)


def insecure_encrypt(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    obj_keys = []
    for pair_id in match_ast_group_d(args.graph, args.n_id, "Pair"):
        pair_node = args.graph.nodes[pair_id]
        key = pair_node["key_id"]
        if symbol := args.graph.nodes[key].get("symbol"):
            obj_keys.append(symbol)

    if not any(key == "mode" for key in obj_keys):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
