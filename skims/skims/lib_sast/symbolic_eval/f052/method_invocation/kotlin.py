from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def kt_insecure_cert(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph
    if graph.nodes[args.n_id]["expression"] == "SSLContext.getInstance":
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def kt_insec_key_gen(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (
        nodes[args.n_id]["expression"] == "KeyGenerator.getInstance"
        and (f_arg := get_n_arg(args.graph, args.n_id, 0))
        and (nodes[f_arg].get("label_type", "") == "Literal")
        and (value := nodes[f_arg].get("value", ""))
    ):
        args.triggers.add(value)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def kt_insec_key_pair_gen(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph
    if graph.nodes[args.n_id]["expression"] == "KeyPairGenerator.getInstance":
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
