from contextlib import suppress

from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

DANGER_INSTANCES = {
    "io.jsonwebtoken.Jwts",
    "jsonwebtoken.Jwts",
    "Jwts",
}


def java_check_jwt_invocations(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes

    if (
        (nodes[args.n_id].get("expression") == "builder")
        and (obj_id := nodes[args.n_id].get("object_id"))
        and (nodes[obj_id].get("symbol") in DANGER_INSTANCES)
    ):
        args.triggers.add("builder_instance")

    if nodes[args.n_id].get("expression") == "signWith":
        args.triggers.add("signed")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_check_jwt_decode(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    dang_parsers = {"parser", "parserBuilder"}
    if (
        (nodes[args.n_id].get("expression") in dang_parsers)
        and (obj_id := nodes[args.n_id].get("object_id"))
        and (nodes[obj_id].get("symbol") in DANGER_INSTANCES)
    ):
        args.triggers.add("parser")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_engine_cipher_ssl(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if (
        (args.graph.nodes[args.n_id].get("expression") == "createSSLEngine")
        and (obj_id := args.graph.nodes[args.n_id].get("object_id"))
        and (args.graph.nodes[obj_id].get("symbol") == "serverSslContext")
    ):
        args.evaluation[args.n_id] = True
        args.triggers.add("ssl_engine")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_weak_rsa_key(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    is_rsa_instance = bool(
        args.graph.nodes[args.n_id].get("expression", "") == "getInstance"
        and (obj_id := args.graph.nodes[args.n_id].get("object_id"))
        and (args.graph.nodes[obj_id].get("symbol") == "KeyPairGenerator")
        and (f_arg := get_n_arg(args.graph, args.n_id, 0))
        and has_string_definition(args.graph, f_arg) == "RSA",
    )

    has_valid_size_less_than_2048 = False

    with suppress(ValueError):
        has_valid_size_less_than_2048 = bool(
            args.graph.nodes[args.n_id].get("expression", "") == "initialize"
            and (f_arg := get_n_arg(args.graph, args.n_id, 0))
            and (size_str := has_string_definition(args.graph, f_arg))
            and (size := int(size_str))
            and size < 2048,
        )
    if is_rsa_instance:
        args.triggers.add("rsa_instance")
    if has_valid_size_less_than_2048:
        args.triggers.add("size_less_than_2048")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
