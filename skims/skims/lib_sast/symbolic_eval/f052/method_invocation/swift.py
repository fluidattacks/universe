from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    match_ast,
)


def is_literal_node(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        graph.nodes[n_id]["label_type"] == "Argument"
        and (arg_id := match_ast(graph, n_id).get("__0__"))
        and graph.nodes[arg_id]["label_type"] == "Literal"
    ):
        return True

    return False


def swift_arg_from_literal(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph
    if (
        graph.nodes[args.n_id]["expression"] == "Data"
        and (args_id := graph.nodes[args.n_id].get("arguments_id"))
        and (child := match_ast(graph, args_id).get("__0__"))
        and is_literal_node(graph, child)
    ):
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
