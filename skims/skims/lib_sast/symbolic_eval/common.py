import re

from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
)

# https://learn.microsoft.com/en-us/dotnet/api/system.web.httprequest
NET_SYSTEM_HTTP_METHODS = {
    "Cookies",
    "Form",
    "Headers",
    "Params",
    "Path",
    "PathInfo",
    "QueryString",
    "ReadLine",
    "GetString",
}
NET_CONNECTION_OBJECTS = {
    "TcpClient",
    "TcpListener",
    "SqlCommand",
    "WebClient",
}

JAVA_CONNECTION_METHODS = {
    "batchUpdate",
    "getBytes",
    "getHeader",
    "getHeaderNames",
    "getHeaders",
    "getName",
    "getParameter",
    "getTheParameter",
    "getParameterMap",
    "getParameterNames",
    "getParameterValues",
    "getQueryString",
    "getValue",
    "query",
    "queryForList",
    "queryForMap",
    "queryForObject",
    "queryForRowSet",
}

JS_TS_HTTP_INPUTS = {
    r"\b(req|request)\.body\b",
    r"\b(req|request)\.params\b",
    r"\b(req|request)\.query\b",
    r"\b(req|request)\.url\b",
}

PYTHON_INPUTS = {
    "request.COOKIES.get",
    "request.GET.get",
    "request.POST.get",
    "request.args",
    "request.data.get",
    "request.files",
    "request.headers.get",
}

PHP_INPUTS = {"_GET", "_POST", "_REQUEST", "_FILES"}

PYTHON_REQUESTS_LIBRARIES = {
    "requests",
    "urllib",
    "urllib2",
    "urllib3",
    "httplib2",
    "httplib",
    "http",
    "treq",
    "aiohttp",
}

INSECURE_ALGOS = {
    "none",
    "blowfish",
    "bf",
    "des",
    "desede",
    "rc2",
    "rc4",
    "rsa",
    "3des",
}
INSECURE_MODES = {"cbc", "ecb", "ofb"}
INSECURE_HASHES = {"md2", "md4", "md5", "sha1", "sha-1"}


def owasp_user_connection(args: SymbolicEvalArgs) -> bool:
    ma_attr = args.graph.nodes[args.n_id]
    return bool(
        ma_attr["expression"] in JAVA_CONNECTION_METHODS
        and (obj_id := ma_attr.get("object_id"))
        and args.generic(args.fork(n_id=obj_id, evaluation={}, triggers=set())).triggers
        == {"userconnection"},
    )


def check_js_ts_http_inputs(args: SymbolicEvalArgs) -> bool:
    n_attrs = args.graph.nodes[args.n_id]
    member_access = f"{n_attrs['expression']}.{n_attrs['member']}"
    return next(
        (True for expr in JS_TS_HTTP_INPUTS if re.match(expr, member_access) is not None),
        False,
    )


def check_python_inputs(args: SymbolicEvalArgs) -> bool:
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs["label_type"] != "MemberAccess":
        return False
    member_access = f"{n_attrs['expression']}.{n_attrs['member']}"
    return member_access in PYTHON_INPUTS
