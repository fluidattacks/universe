from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f309.method_invocation.java import (
    java_none_alg,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVA_NONE_ALG_AUTH0_JWT_SIGN_KEY: java_none_alg,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
