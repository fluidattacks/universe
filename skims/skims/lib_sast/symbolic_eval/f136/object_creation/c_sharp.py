from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_cache_control_instance(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    args.evaluation[args.n_id] = bool(nodes[args.n_id].get("name") == "CacheControlHeaderValue")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
