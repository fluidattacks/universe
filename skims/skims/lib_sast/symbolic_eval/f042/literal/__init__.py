from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f042.literal.c_sharp import (
    cs_insecure_cookies,
)
from lib_sast.symbolic_eval.f042.literal.common import (
    js_insecure_cookies,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.CSHARP_INSECURELY_GENERATED_COOKIES: cs_insecure_cookies,
    MethodsEnum.JS_INSECURELY_GENERATED_COOKIES: js_insecure_cookies,
    MethodsEnum.TYPESCRIPT_INSECURELY_GENERATED_COOKIES: js_insecure_cookies,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
