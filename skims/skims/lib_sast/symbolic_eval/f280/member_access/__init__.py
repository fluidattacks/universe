from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f280.member_access.common import (
    is_user_input,
)
from lib_sast.symbolic_eval.f280.member_access.python import (
    xml_parser,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVASCRIPT_NON_SECURE_CONSTRUCTION_OF_COOKIES: is_user_input,
    MethodsEnum.PYTHON_SESSION_FIXATION: xml_parser,
    MethodsEnum.TYPESCRIPT_NON_SECURE_CONSTRUCTION_OF_COOKIES: is_user_input,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
