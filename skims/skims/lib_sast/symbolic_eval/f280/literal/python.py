from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def xml_parser(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["value"] == "sessionid":
        args.triggers.add("sessionid")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
