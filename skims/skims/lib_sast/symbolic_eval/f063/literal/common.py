from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def validate_extension(string: str) -> bool:
    known_extensions = [
        ".pdf",
        ".yml",
        ".txt",
        ".doc",
        ".docx",
        ".xls",
        ".xlsx",
        ".png",
        ".jpg",
        ".jpeg",
        ".gif",
        ".csv",
        ".info.yml",
    ]
    cleaned_string = string.strip("'\"`")
    return any(cleaned_string.endswith(ext) for ext in known_extensions)


def has_extension(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    value = args.graph.nodes[args.n_id].get("value", "")
    if value and validate_extension(value):
        args.triggers.add("forceExtension")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
