from lib_sast.symbolic_eval.common import (
    owasp_user_connection,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def java_unsafe_path_traversal(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if owasp_user_connection(args):
        args.triggers.clear()
        args.evaluation[args.n_id] = True
        args.triggers.add("userparameters")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
