from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    match_ast_d,
)


def var_is_literal_array(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (
        (val_id := nodes[args.n_id].get("value_id"))
        and (nodes[val_id].get("label_type") == "ArrayInitializer")
        and not match_ast_d(args.graph, val_id, "SymbolLookup", -1)
    ):
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
