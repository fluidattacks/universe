from lib_sast.root.utilities.common import (
    has_string_definition,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def get_bytes_from_string(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if nodes[args.n_id].get("expression") == "getBytes" and has_string_definition(
        args.graph,
        args.n_id,
    ):
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
