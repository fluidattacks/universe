from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f152.literal.typescript import (
    insecure_http_headers,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.TYPESCRIPT_UNSAFE_HTTP_XFRAME_OPTIONS: insecure_http_headers,
    MethodsEnum.JAVASCRIPT_UNSAFE_HTTP_XFRAME_OPTIONS: insecure_http_headers,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
