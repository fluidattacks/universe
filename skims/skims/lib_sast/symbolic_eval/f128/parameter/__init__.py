from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f128.parameter.c_sharp import (
    c_sharp_http_only_cookie,
)
from lib_sast.symbolic_eval.f128.parameter.java import (
    java_insecure_cookie_response,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.C_SHARP_HTTP_ONLY_COOKIE: c_sharp_http_only_cookie,
    MethodsEnum.JAVA_HTTP_ONLY_COOKIE: java_insecure_cookie_response,
    MethodsEnum.KOTLIN_HTTP_ONLY_COOKIE: java_insecure_cookie_response,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
