from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def php_insecure_cors(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    exp_id = nodes[args.n_id].get("expression_id")
    arg_id = nodes[args.n_id].get("arguments_id")

    if nodes[exp_id].get("symbol") == "_SERVER" and nodes[arg_id].get("value") == "HTTP_ORIGIN":
        args.triggers.add("dang_val")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
