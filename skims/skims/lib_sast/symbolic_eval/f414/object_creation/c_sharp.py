from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_header_check(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] == "HttpRuntimeSection":
        args.evaluation[args.n_id] = True
        args.triggers.add("http_runtime")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
