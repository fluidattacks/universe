from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_regex_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def cs_vuln_regex(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
