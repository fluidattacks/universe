from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def python_regex_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    value = args.graph.nodes[args.n_id]["value"]
    # Other insecure regex patterns must be added here
    insecure_chars = ("(.*", "+)*", "+.)", "+)+", "+)?", "?)+")
    if any(c in value for c in insecure_chars):
        args.evaluation[args.n_id] = True
        args.triggers.add("danger_regex")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
