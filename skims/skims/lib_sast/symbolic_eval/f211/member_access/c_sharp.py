from lib_sast.symbolic_eval.common import (
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_regex_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = args.graph.nodes[args.n_id]["member"] in NET_SYSTEM_HTTP_METHODS
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def cs_vuln_regex(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    args.evaluation[args.n_id] = False

    if ma_attr["member"] in NET_SYSTEM_HTTP_METHODS:
        args.triggers.add("user_parameters")
    elif ma_attr["expression"] == "TimeSpan":
        args.triggers.add("hastimespan")
    elif ma_attr["member"] == "Escape":
        args.triggers.add("safepattern")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
