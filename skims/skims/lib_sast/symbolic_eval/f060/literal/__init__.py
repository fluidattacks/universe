from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f060.literal.php import (
    php_insecure_ssl_tls_http,
)
from lib_sast.symbolic_eval.f060.literal.python import (
    python_ssl_hostname,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.PHP_INSECURE_SSL_TLS_HTTP: php_insecure_ssl_tls_http,
    MethodsEnum.PYTHON_UNSAFE_SSL_HOSTNAME: python_ssl_hostname,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
