from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def php_insecure_ssl_tls_http(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (value := nodes[args.n_id].get("value")) and value == "false":
        args.triggers.add(value)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
