from lib_sast.symbolic_eval.common import (
    check_js_ts_http_inputs,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

KNOWN_AS_NON_VULN = {"image"}


def insecure_inner_html(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if check_js_ts_http_inputs(args):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def insecure_danger_url(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    member_access = f"{n_attrs['expression']}.{n_attrs['member']}"
    if n_attrs["member"] not in KNOWN_AS_NON_VULN:
        args.triggers.add(n_attrs["expression"].split("[")[0])
    if ".target.value" in member_access or ".snapshot.queryParams" in member_access:
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
