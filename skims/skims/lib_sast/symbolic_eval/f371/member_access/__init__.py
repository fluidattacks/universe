from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f371.member_access.common import (
    insecure_danger_url,
    insecure_inner_html,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JS_USES_INNERHTML: insecure_inner_html,
    MethodsEnum.TS_USES_INNERHTML: insecure_inner_html,
    MethodsEnum.JS_USE_OF_BYPASS_SECURITY_TRUST_URL: insecure_danger_url,
    MethodsEnum.TS_USE_OF_BYPASS_SECURITY_TRUST_URL: insecure_danger_url,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
