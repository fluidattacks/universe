from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def has_string_value(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (
        (key_id := nodes[args.n_id].get("key_id", ""))
        and (nodes[key_id].get("symbol", "") == "__html")
        and (val_id := nodes[args.n_id].get("value_id", ""))
        and (nodes[val_id].get("label_type", "") == "Literal")
        and "<" not in nodes[val_id].get("value", "")
    ):
        args.triggers.add("Safe")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
