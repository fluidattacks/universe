from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def java_ldap_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if (var_type := args.graph.nodes[args.n_id].get("variable_type")) and var_type.rsplit(".", 1)[
        -1
    ] == "HttpServletRequest":
        args.triggers.add("userconnection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
