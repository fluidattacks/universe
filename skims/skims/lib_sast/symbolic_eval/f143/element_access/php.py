from lib_sast.symbolic_eval.common import (
    PHP_INPUTS,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def php_dangerous_eval(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (exp_n_id := nodes[args.n_id].get("expression_id")) and (
        nodes[exp_n_id].get("symbol") in PHP_INPUTS
    ):
        args.triggers.add("user_input")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
