from lib_sast.path.f325.dotnetconfig import (
    excessive_auth_privileges as run_excessive_auth_privileges,
)

__all__ = ["run_excessive_auth_privileges"]
