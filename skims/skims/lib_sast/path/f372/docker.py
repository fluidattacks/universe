import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.docker import (
    get_commands_with_lines,
)
from model.core import (
    MethodExecutionResult,
)


def has_location_flag(line: str) -> bool:
    tokens = line.split()

    return any(
        tok == "--location" or (tok.startswith("-") and tok.count("-") == 1 and "L" in tok)
        for tok in tokens
    )


def set_secure_protocols(protocols_str: str) -> bool:
    protocols = protocols_str.split(",")
    return any(
        secure_protocol in protocols
        for secure_protocol in (
            "=https",
            "-http",
            '"=https"',
            '"-http"',
            "'=https'",
            "'-http'",
        )
    )


def has_defined_protocols(line: str) -> bool:
    tokens = [*line.split(), ""]
    for idx, token in enumerate(tokens):
        if token in {"--proto", "--proto-redir"} and set_secure_protocols(tokens[idx + 1]):
            return True
    return False


def has_remote_url(command: str) -> bool:
    url_pattern = re.compile(r"(?i)(ftp|http|https)://([^/]+)")

    local_hosts = {"127.0.0.1", "0.0.0.0", "localhost"}  # noqa: S104

    return any(
        not command_token.startswith("-")
        and (match := url_pattern.search(command_token))
        and (hostname := match.group(2))
        and (hostname not in local_hosts)
        for command_token in command.split()
    )


def curl_allows_redirections(command: str) -> bool:
    return (
        command.startswith("curl")
        and has_location_flag(command)
        and not has_defined_protocols(command)
        and has_remote_url(command)
    )


def wget_allows_redirections(command: str) -> bool:
    wget_secure_flags = {" --max-redirect=0 ", " --https-only "}
    return (
        command.startswith("wget")
        and not any(sec_flag in command for sec_flag in wget_secure_flags)
        and has_remote_url(command)
    )


@SHIELD_BLOCKING
def docker_downgrade_protocol(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_DOWNGRADE_PROTOCOL

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, raw_command in get_commands_with_lines(content):
            command = raw_command.strip()

            if curl_allows_redirections(command) or wget_allows_redirections(command):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
