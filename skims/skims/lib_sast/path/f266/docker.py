import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)

COMMANDS_REGEX = [
    re.compile(r"(\s+|^RUN).*useradd"),
    re.compile(r"(\s+|^RUN).*adduser"),
    re.compile(r"(\s+|^RUN).*addgroup"),
    re.compile(r"(\s+|^RUN).*usergroup"),
    re.compile(r"(\s+|^RUN).*usermod"),
    re.compile(r"^USER"),
]


def get_vuln_lines(content: str) -> list[int]:
    vuln_lines: list[int] = []
    current_image = False
    has_user = False

    for line_number, line in enumerate(content.splitlines(), start=1):
        if re.match(r"FROM\s+\S+", line) and not current_image:
            line_no = line_number
            has_user = False
            current_image = True
            continue
        if any(regex.match(line) for regex in COMMANDS_REGEX):
            current_image = False
            has_user = True
            continue
        if re.match(r"FROM\s+\S+", line) and current_image and not has_user:
            current_image = True
            vuln_line = line_no
            line_no = line_number
            vuln_lines.append(vuln_line)

    if current_image and not has_user:
        vuln_lines.append(line_no)

    return vuln_lines


@SHIELD_BLOCKING
def container_without_user(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.CONTAINER_WITHOUT_USER

    def iterator() -> Iterator[tuple[int, int]]:
        for vuln_line in get_vuln_lines(content):
            yield vuln_line, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def container_with_user_root(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.CONTAINER_WITH_USER_ROOT

    def iterator() -> Iterator[tuple[int, int]]:
        vulnerable = False
        line_no = 0
        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith("USER"):
                if re.match(r"^USER root", line):
                    vulnerable = True
                    line_no = line_number
                else:
                    vulnerable = False
        if vulnerable:
            yield (line_no, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_sensitive_mount(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_SENSITIVE_MOUNT

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, line in enumerate(content.splitlines(), start=1):
            if (
                line.startswith("RUN")
                and (mount := re.search(r"--mount=type=(secret|ssh).*?mode=(\d{3,4})", line))
                and (mode := mount.group(2))
                and mode[-1] in {"3", "7"}
            ):
                yield line_number, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
