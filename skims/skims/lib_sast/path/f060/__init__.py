from lib_sast.path.f060.conf_files import (
    allow_acces_from_any_domain as run_allow_acces_from_any_domain,
)

__all__ = ["run_allow_acces_from_any_domain"]
