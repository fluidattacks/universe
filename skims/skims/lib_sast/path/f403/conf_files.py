from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from bs4.element import (
    Tag,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


def get_insecure_attr(tag: Tag, insecure_names: set[str]) -> str | None:
    for insecure_name in insecure_names:
        if tag.attrs.get(insecure_name, "").lower() == "true":
            return insecure_name

    insecure_name = "android-usescleartexttraffic"
    if (
        tag.name == "preference"
        and (tag.attrs.get("name", "").lower() == insecure_name)
        and (tag.attrs.get("value", "").lower() == "true")
    ):
        return insecure_name
    return None


@SHIELD_BLOCKING
def insecure_configuration(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.XML_INSECURE_CONFIGURATION

    def iterator() -> Iterator[tuple[int, int]]:
        vulnerable_tags = {
            "application",
            "base-config",
            "domain-config",
            "manifest",
            "preference",
        }
        insecure_configurations = {
            "cleartexttrafficpermitted",
            "android:usescleartexttraffic",
        }

        for tag in soup.find_all(vulnerable_tags):
            if danger_attr := get_insecure_attr(tag, insecure_configurations):
                line_no, col_no = get_dang_attr_line(tag, content, danger_attr)

                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
