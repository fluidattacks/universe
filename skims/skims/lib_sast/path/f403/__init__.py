from lib_sast.path.f403.conf_files import (
    insecure_configuration as run_insecure_configuration,
)

__all__ = ["run_insecure_configuration"]
