import time
from collections.abc import (
    Callable,
    Iterator,
)
from configparser import (
    ConfigParser,
)

import ctx
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from model.core import (
    MethodExecutionResult,
    Vulnerabilities,
)
from utils.build_vulns import (
    build_lines_vuln,
    build_metadata,
)
from utils.function import (
    shield_blocking,
)
from utils.logs import (
    log_blocking,
)
from utils.statics import (
    calculate_time_coefficient,
)
from utils.string_handlers import (
    is_exclusion,
    make_snippet_func,
)
from utils.translations import (
    t,
)

NAMES_DOCKERFILE = {"Dockerfile"}
EXTENSIONS_BASH = {"sh"}
EXTENSIONS_JAVA_PROPERTIES = {"properties"}
TRUE_OPTIONS = ["true", "True", True, "1", 1]
FALSE_OPTIONS = ["false", "False", False, "0", 0]


def get_path_from_attrs(*args: tuple[str, str] | dict) -> str:
    path = ""
    if len(args) > 1:
        if isinstance(args[1], dict) and "path" in args[1]:
            path = args[1]["path"]
    else:
        path = args[0][1]
    return path


SHIELD_BLOCKING: Callable[[Callable], Callable] = shield_blocking(
    on_error_return=MethodExecutionResult(vulnerabilities=()),
    get_path_from_attrs=get_path_from_attrs,
)


def get_vulnerabilities_from_iterator_blocking(
    content: str,
    iterator: Iterator[tuple[int, int]],
    path: str,
    method: MethodsEnum,
) -> MethodExecutionResult:
    init_time = time.time()

    results: Vulnerabilities = tuple(
        build_lines_vuln(
            method=method.value,
            what=path,
            where=str(line_no),
            metadata=build_metadata(
                method=method.value,
                description=(
                    f"{t(key=method.name)} {t(key='words.in')} {ctx.SKIMS_CONFIG.namespace}/{path}"
                ),
                snippet=make_snippet_func(
                    file_content=content,
                    line=line_no,
                    column=0,
                    path=path,
                ),
                skip=is_exclusion(content, line_no),
            ),
        )
        for line_no, column_no in iterator
    )
    elapsed_time = time.time() - init_time

    return MethodExecutionResult(
        method_name=method.value.name,
        time_coefficient=calculate_time_coefficient(
            elapsed_time,
            path,
            1,
        ),
        vulnerabilities=results,
    )


def get_vulnerabilities_include_metadata(
    content: str,
    iterator: Iterator[tuple[int, int, dict]],
    path: str,
    method: MethodsEnum,
) -> MethodExecutionResult:
    init_time = time.time()

    results: Vulnerabilities = tuple(
        build_lines_vuln(
            method=method.value,
            what=path,
            where=str(line_no),
            metadata=build_metadata(
                method=method.value,
                description=(
                    f"{t(key=method.name, **desc_params)} "
                    f"{t(key='words.in')} "
                    f"{ctx.SKIMS_CONFIG.namespace}/{path}"
                ),
                snippet=make_snippet_func(
                    file_content=content,
                    line=line_no,
                    column=0,
                    path=path,
                ),
                skip=is_exclusion(content, line_no),
            ),
        )
        for line_no, column_no, desc_params in iterator
    )
    elapsed_time = time.time() - init_time

    return MethodExecutionResult(
        method_name=method.value.name,
        time_coefficient=calculate_time_coefficient(
            elapsed_time,
            path,
            1,
        ),
        vulnerabilities=results,
    )


def find_key_in_content(content: str, key: str) -> int | None:
    """Find the line number of a specific key in the given content.

    Args:
        content (str): The content of the php.ini file.
        key (str): The key to search for in the content.

    Returns:
        int | None: The line number of the key if found, otherwise None.

    """
    lines = content.split("\n")
    for line_number, line in enumerate(lines, start=1):
        if line.strip().startswith(key):
            return line_number
    return None


def parse_php_ini(path: str) -> ConfigParser | None:
    config = ConfigParser()
    result: ConfigParser | None = None
    try:
        config.read(path)
        if "PHP" in config:
            result = config
    except Exception as err:  # noqa: BLE001
        log_blocking("warning", "Failed to parse %s %s", path, err)
    return result


def validate_is_file_with_credentials(file_name: str, file_extension: str) -> bool:
    return file_extension in {
        "bashrc",
        "bash_profile",
        "cfg",
        "conf",
        "config",
        "env",
        "groovy",
        "ini",
        "jpage",
        "properties",
        "ps1",
        "Renviron",
        "sbt",
        "sh",
        "sql",
        "xml",
    } or f"{file_name}.{file_extension}".endswith(
        (
            "env.dev",
            "env.development",
            "env.example",
            "env.local",
            "env.prod",
            "env.production",
        ),
    )


def validate_web_config(_: str, file_extension: str) -> bool:
    return file_extension in {"config", "httpsF5", "settings"}


def validate_name_is_dockerfile(file_name: str, file_extension: str) -> bool:
    return file_name in NAMES_DOCKERFILE or file_extension in NAMES_DOCKERFILE


def validate_is_bash_extension(_: str, file_extension: str) -> bool:
    return file_extension in EXTENSIONS_BASH


def validate_is_android_manifest(file_name: str, file_extension: str) -> bool:
    return (file_name, file_extension) == ("AndroidManifest", "xml")


def validate_extension_in_java_properties(_: str, file_extension: str) -> bool:
    return file_extension in EXTENSIONS_JAVA_PROPERTIES


def validate_is_config_extension(_: str, file_extension: str) -> bool:
    return file_extension == "config"


def validate_is_xml_jmx_extension(_: str, file_extension: str) -> bool:
    return file_extension in ("config", "xml", "jmx")


def validate_is_html_extension(_: str, file_extension: str) -> bool:
    return file_extension in {"html", "cshtml", "pug"}


def validate_is_script_type_file(file_name: str, file_extension: str) -> bool:
    return file_extension in ("sh", "com") or validate_name_is_dockerfile(file_name, file_extension)


def validate_php_ini(file_name: str, file_extension: str) -> bool:
    return f"{file_name}.{file_extension}" == "php.ini"
