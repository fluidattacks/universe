import re
from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from bs4.element import (
    Tag,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


def parse_tag(a_href: Tag) -> dict[str, str | None]:
    parsed: dict = {
        "href": a_href.get("href", ""),
        "[href]": a_href.get("[href]"),
        "target": a_href.get("target"),
        "rel": a_href.get("rel"),
    }

    for key, value in parsed.items():
        if isinstance(value, list):
            parsed.update({key: " ".join(value)})

    return parsed


@SHIELD_BLOCKING
def has_reverse_tabnabbing(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.HTML_HAS_REVERSE_TABNABBING

    def iterator() -> Iterator[tuple[int, int]]:
        http_re = re.compile(r"^http(s)?://|^\{.*\}")
        rel_re = re.compile(r"(?=.*noopener)(?=.*noreferrer)")

        for a_href in soup.findAll("a"):
            parsed: dict[str, str | None] = parse_tag(a_href)
            if (
                (http_re.match(str(parsed["href"])) or parsed["[href]"])
                and parsed["target"] == "_blank"
                and (not parsed["rel"] or not rel_re.match(parsed["rel"]))
            ):
                yield a_href.sourceline, a_href.sourcepos

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
