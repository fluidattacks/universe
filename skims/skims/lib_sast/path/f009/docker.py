import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.f009.utils import (
    is_key_sensitive,
)
from model.core import (
    MethodExecutionResult,
)

# Constants
WS = r"\s*"
WSM = r"\s+"
DOCKERFILE_ENV: re.Pattern[str] = re.compile(
    rf"^{WS}ENV{WS}(?P<key>[\w\.]+)(?:{WS}={WS}|{WSM})(?P<value>[^$].+?){WS}$",
)


@SHIELD_BLOCKING
def dockerfile_env_secrets(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKERFILE_ENV_SECRETS
    secret_smells: set[str] = {
        "api_key",
        "jboss_pass",
        "license_key",
        "password",
        "secret",
    }

    def iterator() -> Iterator[tuple[int, int]]:
        for line_no, line in enumerate(content.splitlines(), start=1):
            if match := DOCKERFILE_ENV.match(line):
                secret: str = match.group("key").lower()
                value: str = match.group("value").strip("\"'")
                is_interpolated: bool = (
                    not (value.startswith("$"))
                    and not value.startswith("#{")
                    and not value.endswith("}#")
                )
                if (
                    value
                    and is_interpolated
                    and (
                        any(smell in secret for smell in secret_smells) or is_key_sensitive(secret)
                    )
                ):
                    column: int = match.start("value")
                    yield line_no, column

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_hardcoded_credentials(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_HARDCODED_CREDENTIALS

    def iterator() -> Iterator[tuple[int, int]]:
        pattern = re.compile(r"\bwget\b.*--password=[\"'](?!\$)[^\"']*[\"']", re.IGNORECASE)

        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith("RUN") and pattern.search(line):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
