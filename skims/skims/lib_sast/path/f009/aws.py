from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.f009.utils import (
    AWS_CREDENTIALS_PATTERN,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def aws_credentials(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.AWS_CREDENTIALS

    def iterator() -> Iterator[tuple[int, int]]:
        for index, line in enumerate(content.splitlines(), 1):
            if len(line) < 1000 and AWS_CREDENTIALS_PATTERN.search(line):
                yield index, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
