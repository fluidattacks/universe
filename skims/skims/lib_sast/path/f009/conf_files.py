from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.f009.utils import (
    DB_CONNECT_PATTERN,
    JWT_TOKEN_PATTERN,
    USER_PASS_PATTERN,
    validate_jwt,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def jwt_token(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.JWT_TOKEN

    def iterator() -> Iterator[tuple[int, int]]:
        for index, line in enumerate(content.splitlines(), 1):
            if (
                len(line) < 1000
                and (matches := JWT_TOKEN_PATTERN.findall(line))
                and any(validate_jwt(jwt) for jwt in matches)
            ):
                yield index, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def web_config_db_connection(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.WEB_CONFIG_DB_CONNECTION

    def iterator() -> Iterator[tuple[int, int]]:
        for index, line in enumerate(content.splitlines(), 1):
            if len(line) < 1000 and DB_CONNECT_PATTERN.search(line):
                yield index, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def web_config_user_pass(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.WEB_CONFIG_USER_PASS

    def iterator() -> Iterator[tuple[int, int]]:
        for index, line in enumerate(content.splitlines(), 1):
            if len(line) < 1000 and USER_PASS_PATTERN.search(line):
                yield index, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
