from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def has_ssl_disabled(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    """Report ssl disabled.

    Method source:
    https://learn.microsoft.com/en-us/iis/configuration/
    system.webserver/security/access
    """
    method = MethodsEnum.DOTNETCONFIG_HAS_SSL_DISABLED
    is_main_config = path.endswith("ApplicationHost.config")

    def iterator() -> Iterator[tuple[int, int]]:
        for security_tag in soup.find_all("security"):
            if not (access_tags := security_tag.find_all("access")) and is_main_config:
                yield security_tag.sourceline, security_tag.sourcepos

            for access_tag in access_tags:
                access_has_ssl_flag = access_tag.attrs.get("sslflags", None)
                if access_has_ssl_flag is None:
                    yield access_tag.sourceline, access_tag.sourcepos
                elif access_has_ssl_flag.lower() == "none":
                    line_no, col_no = get_dang_attr_line(access_tag, content, "sslflags")
                    yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
