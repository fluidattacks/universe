from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def anon_auth_enabled(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.DOTNETCONFIG_ANON_AUTH_ENABLED

    def iterator() -> Iterator[tuple[int, int]]:
        for auth_tag in soup.find_all("authentication"):
            for anon_tag in auth_tag.find_all("anonymousauthentication", attrs={"enabled": "true"}):
                line_no, col_no = get_dang_attr_line(anon_tag, content, "enabled")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
