from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.utils.custom_parsers import (
    load_java_properties,
)
from model.core import (
    MethodExecutionResult,
)
from utils.cipher_suites import (
    is_iana_cipher_suite_vulnerable,
    is_open_ssl_cipher_suite_vulnerable,
)


@SHIELD_BLOCKING
def java_properties_missing_ssl(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_PROPERTIES_MISSING_SSL
    missing_ssl_key: str = "ibm.mq.use_ssl"
    missing_ssl_values: set[str] = {"false"}

    def _iterate_vulnerabilities() -> Iterator[tuple[int, int]]:
        for line_no, (key, val) in load_java_properties(content).items():
            if key == missing_ssl_key and val in missing_ssl_values:
                yield line_no, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=_iterate_vulnerabilities(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def java_properties_weak_cipher_suite(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_PROPERTIES_WEAK_CIPHER_SUITE
    weak_cipher_suite: str = "ibm.mq.cipher.suite"

    def _iterate_vulnerabilities() -> Iterator[tuple[int, int]]:
        for line_no, (key, val) in load_java_properties(content).items():
            if key == weak_cipher_suite and (
                is_iana_cipher_suite_vulnerable(val) or is_open_ssl_cipher_suite_vulnerable(val)
            ):
                yield line_no, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=_iterate_vulnerabilities(),
        path=path,
        method=method,
    )
