from lib_sast.path.f052.java import (
    java_properties_missing_ssl as run_java_properties_missing_ssl,
)
from lib_sast.path.f052.java import (
    java_properties_weak_cipher_suite as run_java_properties_weak_cipher_suite,
)

__all__ = [
    "run_java_properties_missing_ssl",
    "run_java_properties_weak_cipher_suite",
]
