from collections.abc import (
    Iterator,
)
from configparser import (
    ConfigParser,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    find_key_in_content,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_http_only_disabled(content: str, path: str, php_ini: ConfigParser) -> MethodExecutionResult:
    method = MethodsEnum.PHP_HTTP_ONLY_DISABLED

    def iterator() -> Iterator[tuple[int, int]]:
        if php_ini and "Session" in php_ini:
            key = "session.cookie_httponly"
            display_errors = php_ini["Session"].get(key)
            if (
                display_errors is not None and display_errors.strip() in {"0", "Off", ""}
            ) or display_errors is None:
                key_line = find_key_in_content(content, key)
                line_no: int = key_line if key_line else 0
                col_no: int = 0
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
