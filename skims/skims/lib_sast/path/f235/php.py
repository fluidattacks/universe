from collections.abc import (
    Iterator,
)
from configparser import (
    ConfigParser,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    find_key_in_content,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_discloses_server_version(
    content: str,
    path: str,
    php_ini: ConfigParser,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_DISCLOSES_SERVER_VERSION

    def iterator() -> Iterator[tuple[int, int]]:
        if php_ini and "PHP" in php_ini:
            expose_php = php_ini["PHP"].get("expose_php", "1")
            if expose_php.lower() not in {"off", "0"}:
                key_line = find_key_in_content(content, "expose_php")
                line_no: int = key_line if key_line else 0
                col_no: int = 0
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
