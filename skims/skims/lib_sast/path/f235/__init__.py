from lib_sast.path.f235.dotnetconfig import (
    asp_version_enabled as run_asp_version_enabled,
)
from lib_sast.path.f235.php import (
    php_discloses_server_version as run_php_discloses_server_version,
)

__all__ = ["run_asp_version_enabled", "run_php_discloses_server_version"]
