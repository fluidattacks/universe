from bs4.element import (
    Tag,
)


def get_dang_attr_line(tag: Tag, content: str, dang_attr: str) -> tuple[int, int]:
    attr_line = tag.sourceline if tag.sourceline else 1
    for line in content.split("\n")[attr_line - 1 :]:
        if any(substring in line.lower() for substring in [f"{dang_attr}=", ">"]):
            break
        attr_line += 1

    return attr_line, tag.sourcepos if tag.sourcepos else 0


def get_tag_attr_value(tag: Tag, key: str, default: str) -> str:
    attr: str
    key = key.lower()
    for attr, value in tag.attrs.items():
        if attr.lower() == key:
            return value
    return default
