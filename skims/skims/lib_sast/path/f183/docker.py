from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


def get_vuln_line(content: str) -> int | None:
    vuln_line: int | None = None
    app_debug: str | None = None
    env: str | None = None

    for line_number, line in enumerate(content.splitlines(), start=1):
        if line.startswith("ENV"):
            if "APP_DEBUG=" in line and app_debug != "true":
                app_debug = line.split("=", maxsplit=1)[-1].strip().lower()
                vuln_line = line_number

            if "ENV=" in line:
                env = line.split("=", maxsplit=1)[-1].strip().lower()
    if (env == "production" or env is None) and app_debug == "true":
        return vuln_line
    return None


@SHIELD_BLOCKING
def docker_debugging_enabled(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_DEBUGGING_ENABLED

    def iterator() -> Iterator[tuple[int, int]]:
        if debug_line := get_vuln_line(content):
            yield (debug_line, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
