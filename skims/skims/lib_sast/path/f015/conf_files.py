import re
from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from bs4.element import (
    Tag,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)
from utils.string_handlers import (
    get_numbers,
)


@SHIELD_BLOCKING
def jmx_header_basic(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.JMX_HEADER_BASIC

    def iterator() -> Iterator[tuple[int, int]]:
        for tag in soup.find_all("stringprop"):
            if isinstance(tag, Tag):
                tag_content = str(tag).lower()
                if (
                    (name_attr := tag.attrs.get("name"))
                    and name_attr.lower() == "header.value"
                    and tag.name == "stringprop"
                    and re.search(r">\s*basic\b", tag_content)
                ):
                    line_no, col_no = get_numbers(tag)
                    yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def basic_auth_method(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.XML_BASIC_AUTH_METHOD

    def iterator() -> Iterator[tuple[int, int]]:
        for tag in soup.find_all("auth-method"):
            if isinstance(tag, Tag):
                tag_name = tag.name
                tag_content = str(tag.string).lower()
                if tag_name == "auth-method" and re.search(r"\bbasic\b", tag_content):
                    line_no, col_no = get_numbers(tag)
                    yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
