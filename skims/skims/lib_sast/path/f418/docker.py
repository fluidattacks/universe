import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def docker_using_add_command(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_USING_ADD_COMMAND

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, line in enumerate(content.splitlines(), start=1):
            if re.match(r"^ADD[ \t]+.", line):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_weak_ssl_tls(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_WEAK_SSL_TLS

    curl_insecure_protocols = {
        "--tlsv1",
        "--tlsv1.0",
        "--tlsv1.1",
        "--sslv1",
        "--sslv2",
        "--sslv3",
    }
    wget_insecure_protocols = {
        "TLSv1",
        "TLSv1_1",
        "SSLv1",
        "SSLv2",
        "SSLv3",
    }

    curl_pattern = (
        r"(?<!\S)(?:" + "|".join(re.escape(proto) for proto in curl_insecure_protocols) + r")(?!\S)"
    )
    wget_pattern = (
        r"--secure-protocol\s+(?<!\S)(?:"
        + "|".join(re.escape(proto) for proto in wget_insecure_protocols)
        + r")(?!\S)"
    )

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith("RUN") and (
                re.search(r"curl\b.*" + curl_pattern, line)
                or re.search(r"wget\b.*" + wget_pattern, line)
            ):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_insecure_builder_sandbox(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_INSECURE_BUILDER_SANDBOX

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith("RUN") and re.search(r"--security=insecure", line):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_insecure_cleartext_protocol(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_INSECURE_CLEARTEXT_PROTOCOL

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith("RUN") and re.search(r"\bcurl(?:\s+.*)?\s+(http|ftp)://", line):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_weak_hash_algorithm(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_WEAK_HASH_ALGORITHM

    insecure_algorithms = "hmacmd5|sha1sum|hmacripemd|dsa|md2sum|md4sum|haval|ripemd|md5sum|sha1"

    pattern = re.compile(r"\b(" + insecure_algorithms + r")\b\s+-c\b")

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith("RUN") and pattern.search(line):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_insecure_network_host(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_INSECURE_NETWORK_HOST

    def iterator() -> Iterator[tuple[int, int]]:
        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith("RUN") and re.search(r"--network=host", line):
                yield (line_number, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_insecure_context_directory(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_INSECURE_CONTEXT_DIRECTORY

    def iterator() -> Iterator[tuple[int, int]]:
        patterns = {
            r"\.\s+\.",
            r"\./\S*\*\s+/",
        }
        for line_number, line in enumerate(content.splitlines(), start=1):
            if line.startswith(("COPY", "ADD")) and any(
                re.search(pattern, line) for pattern in patterns
            ):
                yield line_number, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def docker_socket_mount(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_SOCKET_MOUNT

    def iterator() -> Iterator[tuple[int, int]]:
        pattern_1 = re.compile(r"VOLUME\s*/var/run/docker.sock")

        for line_number, line in enumerate(content.splitlines(), start=1):
            if pattern_1.search(line):
                yield line_number, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
