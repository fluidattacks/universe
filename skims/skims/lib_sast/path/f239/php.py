from collections.abc import (
    Iterator,
)
from configparser import (
    ConfigParser,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    find_key_in_content,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_server_leaks_errors(
    content: str,
    path: str,
    php_ini: ConfigParser,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_SERVER_LEAKS_ERRORS

    def iterator() -> Iterator[tuple[int, int]]:
        if php_ini and "PHP" in php_ini:
            display_errors = php_ini["PHP"].get("display_errors")
            if display_errors and display_errors.lower() != "off":
                key_line = find_key_in_content(content, "display_errors")
                line_no: int = key_line if key_line else 0
                col_no: int = 0
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
