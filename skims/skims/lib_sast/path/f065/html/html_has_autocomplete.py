from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def has_autocomplete(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.HTML_HAS_AUTOCOMPLETE

    def iterator() -> Iterator[tuple[int, int]]:
        for form_obj in soup.find_all("form"):
            if form_obj.get("autocomplete", "on") == "off":
                continue

            for input_obj in form_obj.findChildren("input"):
                if (
                    input_obj.get("autocomplete", "on") != "off"
                    and input_obj.get("type", "text") in {"email", "password", "tel"}
                    and input_obj.get("disabled") != ""
                ):
                    yield input_obj.sourceline, input_obj.sourcepos

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
