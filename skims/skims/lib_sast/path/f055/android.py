from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
    get_tag_attr_value,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def android_backups_enabled(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.ANDROID_APK_BACKUPS_ENABLED

    def iterator() -> Iterator[tuple[int, int]]:
        for tag in soup.find_all("application"):
            if (
                allow_backup := get_tag_attr_value(
                    tag,
                    key="android:allowBackup",
                    default="not-set",
                ).lower()
            ) == "not-set":
                yield tag.sourceline, tag.sourcepos

            elif allow_backup == "true":
                line_no, col_no = get_dang_attr_line(tag, content, "android:allowbackup")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
