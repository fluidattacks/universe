from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
    Tag,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
    get_tag_attr_value,
)
from model.core import (
    MethodExecutionResult,
)


def _get_caseless_attr(tag: Tag, key: str, default: str) -> str:
    attr: str
    key = key.lower()
    for attr, value in tag.attrs.items():
        if attr.lower() == key:
            return value
    return default


def get_custom_permissions(manifest: BeautifulSoup) -> set[str]:
    perm_tags = manifest.find_all("permission")

    return {
        perm_name.lower()
        for perm_tag in perm_tags
        if (perm_name := _get_caseless_attr(perm_tag, "android:name", ""))
    }


@SHIELD_BLOCKING
def android_exported_cp(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.ANDROID_APK_EXPORTED_CP

    def iterator() -> Iterator[tuple[int, int]]:
        for tag in soup.find_all("provider"):
            exported = get_tag_attr_value(tag, key="android:exported", default="no-value").lower()
            if exported == "no-value":
                yield tag.sourceline, tag.sourcepos

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def android_uri_permissions(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.ANDROID_APK_EXPORTED_CP

    def iterator() -> Iterator[tuple[int, int]]:
        permissions = get_custom_permissions(soup)
        for tag in soup.find_all("provider"):
            exported = get_tag_attr_value(tag, key="android:exported", default="false").lower()
            if exported != "true":
                continue
            uri = get_tag_attr_value(
                tag,
                key="android:grantUriPermissions",
                default="false",
            ).lower()
            permission = get_tag_attr_value(tag, key="android:permission", default="").lower()

            if uri == "true" and permission not in permissions:
                line_no, col_no = get_dang_attr_line(tag, content, "android:permission")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
