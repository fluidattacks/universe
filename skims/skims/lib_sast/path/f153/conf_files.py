from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from bs4.element import (
    Tag,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


def _eval_header(tag: Tag) -> bool:
    if tag.name == "stringprop":
        tag_value = tag.attrs.get("name")
        if tag_value and tag_value.lower() == "header.name":
            tag_content = str(tag.string).lower()
            return tag_content == "accept"
    return False


def _next_sibling(tag: Tag) -> bool:
    next_tag = tag.find_next_sibling(name="stringprop", text="*/*")

    if next_tag and isinstance(next_tag, Tag):
        tag_value = next_tag.attrs.get("name")
        return bool(tag_value) and tag_value.lower() == "header.value"
    return False


@SHIELD_BLOCKING
def xml_accept_header(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.XML_ACCEPT_HEADER

    def iterator() -> Iterator[tuple[int, int]]:
        for tag in soup.find_all("stringprop"):
            if _eval_header(tag) and _next_sibling(tag):
                line_no: int = tag.sourceline
                col_no: int = tag.sourcepos
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
