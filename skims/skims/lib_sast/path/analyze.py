from concurrent.futures.process import (
    ProcessPoolExecutor,
)
from os.path import (
    split,
    splitext,
)
from typing import TYPE_CHECKING

import ctx
from lib_sast.path import (
    PHP_INI_CHECKS,
    REGEX_CHECKS,
    SOUP_CHECKS,
)
from lib_sast.path.check_contexts import (
    PATH_EXTENSIONS,
    PathContext,
    get_check_ctx,
)
from lib_sast.path.common import (
    parse_php_ini,
)
from model.core import (
    FindingEnum,
    MethodExecutionResult,
    SkimsConfig,
)
from utils.fs import (
    Paths,
    get_file_content_block,
)
from utils.logs import (
    log_blocking,
    log_to_remote_handled,
)
from utils.report_soon import (
    send_vulnerability_to_sqs,
)
from utils.state import (
    ExecutionStores,
)

if TYPE_CHECKING:
    from concurrent.futures import (
        Future,
    )

PATH_FINDINGS = (
    [check[0] for check in SOUP_CHECKS]
    + [check[0] for check in REGEX_CHECKS]
    + [check[0] for check in PHP_INI_CHECKS]
)


def analyze_one_path(
    checks: set[FindingEnum],
    check_ctx: PathContext,
) -> list[MethodExecutionResult]:
    path = check_ctx.path
    content = check_ctx.content
    soup = check_ctx.soup_content
    php_ini_content = None

    _, file_info = split(path)
    file_name, file_extension = splitext(file_info)  # noqa: PTH122
    file_extension = file_extension[1:]
    if file_info.endswith("php.ini"):
        php_ini_content = parse_php_ini(path)

    results: list[MethodExecutionResult] = (
        [
            analyzer(content, path)
            for finding, validator, analyzer in REGEX_CHECKS
            if finding in checks and validator(file_name, file_extension)
        ]
        + [
            analyzer(content, path, soup)
            for finding, validator, analyzer in SOUP_CHECKS
            if finding in checks and soup and validator(file_name, file_extension)
        ]
        + [
            analyzer(content, path, php_ini_content)
            for finding, validator, analyzer in PHP_INI_CHECKS
            if finding in checks and php_ini_content and validator(file_name, file_extension)
        ]
    )

    return results


def _analyze_one_path(
    *,
    config: SkimsConfig,
    path: str,
    checks: set[FindingEnum],
) -> list[MethodExecutionResult]:
    # Re-export config to gain visibility in child subprocesses
    ctx.SKIMS_CONFIG = config
    content = get_file_content_block(path)
    checks_ctx = get_check_ctx(path, content)
    return analyze_one_path(checks, checks_ctx)


def handle_results(
    results: list[MethodExecutionResult],
    stores: ExecutionStores,
) -> None:
    for result in results:
        stores.vuln_stores.store_vulns(result.vulnerabilities)

        if result.method_name and result.time_coefficient:
            method_time = (result.method_name, result.time_coefficient)
            stores.methods_stores["methods_results"].store(method_time)
        send_vulnerability_to_sqs(result.vulnerabilities)


def analyze(
    *,
    paths: Paths,
    stores: ExecutionStores,
) -> None:
    if not any(finding in ctx.SKIMS_CONFIG.checks for finding in PATH_FINDINGS):
        # No findings will be executed, early abort
        return

    all_paths = tuple(path for path in paths.ok_paths if path.endswith(PATH_EXTENSIONS))
    if not all_paths:
        return

    log_blocking("info", "Performing basic SAST analysis on %s paths", len(all_paths))

    with ProcessPoolExecutor(max_workers=ctx.CPU_CORES) as executor:
        futures: list[tuple[Future, str]] = [
            (
                executor.submit(
                    _analyze_one_path,
                    config=ctx.SKIMS_CONFIG,
                    path=path,
                    checks=ctx.SKIMS_CONFIG.checks,
                ),
                path,
            )
            for path in all_paths
        ]

        for future, path in futures:
            try:
                path_results = future.result(timeout=120)
                handle_results(path_results, stores)
            except TimeoutError:
                future.cancel()
                msg = f"Future timed out in SAST lines analysis on file: {path}"
                log_to_remote_handled(msg=msg, severity="warning")

    log_blocking("info", "Basic SAST analysis completed!")
