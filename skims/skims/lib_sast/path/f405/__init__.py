from lib_sast.path.f405.bash import (
    excessive_privileges_for_others as run_excessive_privileges_for_others,
)

__all__ = ["run_excessive_privileges_for_others"]
