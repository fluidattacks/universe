from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def xml_x_frame_options(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.XML_X_FRAME_OPTIONS

    def iterator() -> Iterator[tuple[int, int]]:
        for add_tag in soup.find_all("add"):
            if add_tag.attrs.get("name", "").lower() == "x-frame-options":
                line_no, col_no = get_dang_attr_line(add_tag, content, "name")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
