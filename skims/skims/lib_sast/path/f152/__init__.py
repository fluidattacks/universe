from lib_sast.path.f152.conf_files import (
    xml_x_frame_options as run_xml_x_frame_options,
)

__all__ = ["run_xml_x_frame_options"]
