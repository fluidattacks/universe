from lib_sast.path.f346.android import (
    has_dangerous_permissions as run_has_dangerous_permissions,
)

__all__ = ["run_has_dangerous_permissions"]
