from lib_sast.path.f135.conf_files import (
    has_x_xss_protection_header as run_xml_has_x_xss_protection_header,
)

__all__ = ["run_xml_has_x_xss_protection_header"]
