from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_cfg.model import (
    SyntaxCfgArgs,
)


def build(args: SyntaxCfgArgs) -> NId:
    return args.n_id
