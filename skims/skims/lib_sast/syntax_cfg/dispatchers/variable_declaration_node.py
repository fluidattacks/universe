from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_cfg.model import (
    SyntaxCfgArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)


def build(args: SyntaxCfgArgs) -> NId:
    if m_id := match_ast_d(args.graph, args.n_id, "MethodDeclaration"):
        args.graph.add_edge(
            args.n_id,
            args.generic(args.fork(m_id, args.nxt_id)),
            label_cfg="CFG",
        )

    if args.nxt_id:
        args.graph.add_edge(args.n_id, args.nxt_id, label_cfg="CFG")

    return args.n_id
