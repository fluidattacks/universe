from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_cfg.model import (
    SyntaxCfgArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def build(args: SyntaxCfgArgs) -> NId:
    for c_id in adj_ast(args.graph, args.n_id):
        args.graph.add_edge(
            args.n_id,
            args.generic(args.fork(c_id, args.nxt_id)),
            label_cfg="CFG",
        )
    return args.n_id
