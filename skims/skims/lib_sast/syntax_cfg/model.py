from collections.abc import (
    Callable,
)
from typing import (
    NamedTuple,
)

from lib_sast.sast_model import (
    Graph,
    NId,
)


class SyntaxCfgArgs(NamedTuple):
    generic: Callable[["SyntaxCfgArgs"], NId]
    graph: Graph
    n_id: NId
    nxt_id: NId | None
    is_multifile: bool = False

    def fork(self, n_id: NId, nxt_id: NId | None) -> "SyntaxCfgArgs":
        return SyntaxCfgArgs(
            generic=self.generic,
            graph=self.graph,
            n_id=n_id,
            nxt_id=nxt_id,
            is_multifile=self.is_multifile,
        )


class MissingCfgBuilderError(Exception):
    pass
