from lib_sast.root.f300.terraform import (
    tfm_azure_app_service_authentication_is_not_enabled as _tfm_azure_app_srv_auth_is_not_enabled,
)
from lib_sast.root.f300.terraform import (
    tfm_azure_as_client_certificates_enabled as _tfm_azure_certs_enabled,
)
from lib_sast.root.f300.terraform import (
    tfm_azure_dev_portal_has_auth_methods_inactive as _tfm_az_dev_portal_has_auth_methods_inactive,
)
from lib_sast.root.f300.terraform import (
    tfm_redis_cache_authnotrequired_enabled as _tfm_redis_cache_authnotrequired_enabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def tfm_azure_as_client_certificates_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_certs_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_app_service_authentication_is_not_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_app_srv_auth_is_not_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_redis_cache_authnotrequired_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_redis_cache_authnotrequired_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_dev_portal_has_auth_methods_inactive(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_az_dev_portal_has_auth_methods_inactive(shard, method_supplies)
