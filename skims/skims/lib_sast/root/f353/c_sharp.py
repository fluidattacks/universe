from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def check_token_validation_parameters(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    return get_node_evaluation_results(method, graph, n_id, {"token"})


def is_false_set(graph: Graph, n_id: NId) -> bool:
    for node in pred_ast(graph, n_id):
        if (
            graph.nodes[node].get("label_type") == "Assignment"
            and (val_id := graph.nodes[node].get("value_id"))
            and graph.nodes[val_id].get("label_type") == "Literal"
            and graph.nodes[val_id].get("value") == "false"
        ):
            return True
    return False


def comes_from_token_validation_params_obj(graph: Graph, n_id: NId) -> bool:
    if not (great_grandfather := pred_ast(graph, n_id, 3)[-1]):
        return False

    return (
        graph.nodes[great_grandfather].get("label_type") == "ObjectCreation"
        and graph.nodes[great_grandfather].get("name") == "TokenValidationParameters"
    )


def is_token_validation_check(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    token_checks = {
        "RequireExpirationTime",
        "ValidateAudience",
        "ValidateIssuer",
        "ValidateLifetime",
        "RequireSignedTokens",
    }
    if (
        graph.nodes[n_id]["label_type"] == "MemberAccess"
        and graph.nodes[n_id].get("member") in token_checks
        and is_false_set(graph, n_id)
    ):
        return check_token_validation_parameters(method, graph, n_id)
    if (
        graph.nodes[n_id].get("label_type") == "SymbolLookup"
        and graph.nodes[n_id].get("symbol") in token_checks
        and is_false_set(graph, n_id)
    ):
        return comes_from_token_validation_params_obj(graph, n_id)
    return False


def c_sharp_token_validation_checks(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_TOKEN_VALIDATION_CHECKS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if is_token_validation_check(method, graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
