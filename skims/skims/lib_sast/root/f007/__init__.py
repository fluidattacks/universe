from lib_sast.root.f007.java.csrf_protections_disabled import (
    java_csrf_protections_disabled as _java_csrf_protections_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_csrf_protections_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_csrf_protections_disabled(shard, method_supplies)
