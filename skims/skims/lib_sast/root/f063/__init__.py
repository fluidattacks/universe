from lib_sast.root.f063.c_sharp import (
    c_sharp_open_redirect as _c_sharp_open_redirect,
)
from lib_sast.root.f063.c_sharp import (
    c_sharp_unsafe_path_traversal as _c_sharp_unsafe_path_traversal,
)
from lib_sast.root.f063.java import (
    java_jax_rs_path_traversal as _java_jax_rs_path_traversal,
)
from lib_sast.root.f063.java import (
    java_unsafe_path_traversal as _java_unsafe_path_traversal,
)
from lib_sast.root.f063.java import (
    java_zip_slip_injection as _java_zip_slip_injection,
)
from lib_sast.root.f063.javascript import (
    javascript_insecure_path_traversal as _js_insecure_path_traversal,
)
from lib_sast.root.f063.javascript import (
    zip_slip_injection as _js_zip_slip_injection,
)
from lib_sast.root.f063.php import (
    php_unsafe_path_traversal as _php_unsafe_path_traversal,
)
from lib_sast.root.f063.python import (
    python_io_path_traversal as _python_io_path_traversal,
)
from lib_sast.root.f063.typescript import (
    ts_insecure_path_traversal as _ts_insecure_path_traversal,
)
from lib_sast.root.f063.typescript import (
    ts_zip_slip_injection as _ts_zip_slip_injection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_open_redirect(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_open_redirect(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_unsafe_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_unsafe_path_traversal(shard, method_supplies)


@SHIELD_BLOCKING
def java_jax_rs_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_jax_rs_path_traversal(shard, method_supplies)


@SHIELD_BLOCKING
def java_unsafe_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unsafe_path_traversal(shard, method_supplies)


@SHIELD_BLOCKING
def java_zip_slip_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_zip_slip_injection(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_insecure_path_traversal(shard, method_supplies)


@SHIELD_BLOCKING
def js_zip_slip_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_zip_slip_injection(shard, method_supplies)


@SHIELD_BLOCKING
def php_unsafe_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_unsafe_path_traversal(shard, method_supplies)


@SHIELD_BLOCKING
def python_io_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_io_path_traversal(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_insecure_path_traversal(shard, method_supplies)


@SHIELD_BLOCKING
def ts_zip_slip_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_zip_slip_injection(shard, method_supplies)
