import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_sanitized,
)
from lib_sast.root.utilities.java import (
    concatenate_name,
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    Path,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph import (
    filter_nodes,
    match_ast_group_d,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def has_canonical_check(graph: Graph, file_name: str, path: Path) -> bool:
    for n_id in g.matching_nodes(graph, label_type="MethodInvocation"):
        n_attrs = graph.nodes[n_id]
        if (
            n_attrs["expression"] == "getCanonicalPath"
            and (obj_id := graph.nodes[n_id].get("object_id"))
            and graph.nodes[obj_id].get("symbol") == file_name
            and g.pred(graph, n_id)[0] in path
        ):
            return True
    return False


def is_argument_danger(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> bool:
    symbol = graph.nodes[n_id].get("symbol")
    for path in get_backward_paths(graph, n_id):
        if (
            (evaluation := evaluate(method, graph, path, n_id, method_supplies.graph_db))
            and evaluation.danger
            and evaluation.triggers == {"ZipFile"}
            and not has_canonical_check(graph, symbol, path)
        ):
            return True
    return False


def java_zip_slip_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_ZIP_SLIP_INJECTION
    danger_methods = {"readFileToString"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["expression"] in danger_methods
                and (al_id := n_attrs.get("arguments_id"))
                and (test_id := g.match_ast(graph, al_id).get("__0__"))
                and is_argument_danger(graph, test_id, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def check_obj_danger_name(graph: Graph, n_id: NId) -> bool:
    danger_obj = {
        "java.io.File",
        "java.io.FileInputStream",
        "java.io.FileOutputStream",
    }

    return graph.nodes[n_id].get("name") in danger_obj


def eval_object_danger(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> bool:
    n_attrs = graph.nodes[n_id]

    return bool(
        check_obj_danger_name(graph, n_id)
        and (args_id := n_attrs.get("arguments_id"))
        and (al_id := g.adj_ast(graph, args_id))
        and any(
            (not check_obj_danger_name(graph, arg_id))
            and (
                get_node_evaluation_results(
                    method,
                    graph,
                    arg_id,
                    {"userparameters"},
                    graph_db=method_supplies.graph_db,
                )
            )
            for arg_id in al_id
        ),
    )


def eval_invocation_danger(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> bool:
    n_attrs = graph.nodes[n_id]

    return bool(
        (concatenate_name(graph, n_id) == "java.nio.file.Files.newInputStream")
        and (args_id := n_attrs.get("arguments_id"))
        and (al_id := g.adj_ast(graph, args_id))
        and (
            any(
                get_node_evaluation_results(
                    method,
                    graph,
                    arg_id,
                    {"userparameters"},
                    graph_db=method_supplies.graph_db,
                )
                for arg_id in al_id
            )
        ),
    )


def java_unsafe_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UNSAFE_PATH_TRAVERSAL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("label_type") == "MethodInvocation"
                and eval_invocation_danger(graph, node, method, method_supplies)
            ) or (
                graph.nodes[node].get("label_type") == "ObjectCreation"
                and eval_object_danger(graph, node, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_in_method_invocation(graph: Graph, n_id: NId) -> bool:
    if (parents_id := pred_ast(graph, n_id, 2)) and len(parents_id) > 1:
        return graph.nodes[parents_id[1]]["label_type"] == "MethodInvocation"

    return False


def danger_nodes(graph: Graph, param_names: set[str]) -> Iterator[NId]:
    def file_instances(node: dict[str, str]) -> bool:
        return bool(node.get("label_type") == "ObjectCreation") and (node.get("name", "") == "File")

    symbols_n_ids = {
        symbol_n_id
        for file_n_id in filter_nodes(graph, graph.nodes, file_instances)
        for symbol_n_id in match_ast_group_d(graph, file_n_id, "SymbolLookup", -1)
    }

    yield from {
        n_id
        for n_id in symbols_n_ids
        if graph.nodes[n_id].get("symbol") in param_names
        and not is_sanitized(graph, n_id)
        and not is_in_method_invocation(graph, n_id)
    }


def extract_var_names(graph: Graph, path_n_id: NId | None, path_param_n_ids: set[NId]) -> set[str]:
    def extract_val(n_id: NId) -> str | None:
        f_arg = get_n_arg(graph, n_id, 0)
        return has_string_definition(graph, f_arg)

    if path_n_id is None:
        return set()

    params_pattern = re.compile(r"\{(.*?)\}")

    path_names = (
        set(params_pattern.findall(path_value)) if (path_value := extract_val(path_n_id)) else set()
    )

    path_param_names = {
        param_name for n_id in path_param_n_ids if (param_name := extract_val(n_id))
    }

    return path_names & path_param_names


def get_user_param_name(graph: Graph, n_id: NId) -> set[str]:
    path_param_n_ids: set[NId] = set()
    path_n_id: NId | None = None

    for annotation_n_id in match_ast_group_d(graph, n_id, "Annotation", -1):
        if (name := graph.nodes[annotation_n_id].get("name")) == "Path":
            path_n_id = annotation_n_id
        if name == "PathParam":
            path_param_n_ids.add(annotation_n_id)

    return extract_var_names(
        graph,
        path_n_id,
        path_param_n_ids,
    )


def java_jax_rs_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_JAX_RS_PATH_TRAVERSAL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("javax.ws.rs",)):
            return

        danger_var_names: set[NId] = {
            name
            for n_id in method_supplies.selected_nodes
            for name in get_user_param_name(graph, n_id)
        }

        yield from danger_nodes(graph, danger_var_names)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
