from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def is_danger_attachment(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    args_ids = g.adj_ast(graph, n_id)
    if len(args_ids) < 2:
        return False

    for _id in args_ids[1:]:
        n_attrs = graph.nodes[_id]
        if n_attrs["label_type"] != "NamedArgument":
            continue
        if n_attrs["argument_name"] != "as_attachment":
            continue
        val_id = n_attrs["value_id"]
        return get_node_evaluation_results(method, graph, val_id, set())
    return False


def python_io_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_IO_PATH_TRAVERSAL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] == "send_file"
                and (al_id := n_attrs.get("arguments_id"))
                and is_danger_attachment(graph, al_id, method)
                and get_node_evaluation_results(method, graph, al_id, {"userparams"})
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
