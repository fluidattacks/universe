from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from model.core import (
    MethodExecutionResult,
)


def c_sharp_open_redirect(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_OPEN_REDIRECT
    danger_set = {"user_connection", "user_parameters"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            expression = graph.nodes[node]["expression"]
            if (
                expression == "Response.Redirect"
                and (al_id := graph.nodes[node].get("arguments_id"))
                and get_node_evaluation_results(method, graph, al_id, danger_set)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_unsafe_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_UNSAFE_PATH_TRAVERSAL
    danger_methods = {
        "File.Copy",
        "File.Create",
        "File.Delete",
        "File.Exists",
        "File.Move",
        "File.Open",
        "File.Replace",
    }
    danger_set = {"user_connection", "user_parameters"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            expression = graph.nodes[node]["expression"]
            if (
                expression in danger_methods
                and (al_id := graph.nodes[node].get("arguments_id"))
                and get_node_evaluation_results(method, graph, al_id, danger_set)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
