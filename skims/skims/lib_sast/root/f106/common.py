from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    get_pair_nodes,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from utils.string_handlers import (
    split_on_last_dot,
)


def get_danger_vuln_nid(
    *,
    method: MethodsEnum,
    graph: Graph,
    n_id: NId,
    method_name: str,
    danger_goal: bool = True,
) -> bool:
    triggers_goal: set[str] = {"UserConnection"}
    if method_name == "find":
        triggers_goal.add("NonParametrizedQuery")

    return get_node_evaluation_results(method, graph, n_id, triggers_goal, danger_goal=danger_goal)


def check_specific_imports(graph: Graph) -> bool:
    return bool(
        not file_imports_module(graph, "sequelize")
        and (
            file_imports_module(graph, "marsdb")
            or file_imports_module(graph, "mongodb", strict=False)
        ),
    )


def is_mongo_statement(graph: Graph, child_ids: tuple[NId, ...], method_name: str) -> bool:
    mongo_statements = {"$where", "$set", "$inc"}
    orm_statements = {"where", "include"}
    if len(child_ids) > 0:
        for n_id in get_pair_nodes(graph, child_ids):
            if (key_id := graph.nodes[n_id].get("key_id")) and (
                statement := graph.nodes[key_id].get("symbol", "")
            ):
                if statement in orm_statements:
                    return False
                if statement in mongo_statements:
                    return True
    return bool(method_name in {"insert", "findOne"})


def get_vuln_nodes(selected_nodes: list[NId], graph: Graph, method: MethodsEnum) -> list[NId]:
    if not file_imports_module(graph, "express") and not check_specific_imports(graph):
        return []

    vuln_nodes = []
    for n_id in selected_nodes:
        f_name = split_on_last_dot(graph.nodes[n_id]["expression"])
        if (
            f_name[-1] in {"find", "findOne", "insert", "update"}
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (nosql_arg := adj_ast(graph, al_id))
            and is_mongo_statement(graph, nosql_arg, f_name[-1])
            and get_danger_vuln_nid(
                method=method,
                graph=graph,
                n_id=al_id,
                method_name=f_name[-1],
                danger_goal=False,
            )
        ):
            vuln_nodes.append(n_id)

    return vuln_nodes


def any_ternary_option_comes_from_request(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    ternary_options = {
        nodes[n_id].get("true_id"),
        nodes[n_id].get("false_id"),
    }
    return any(
        g.adj_ast(
            graph,
            ternary_option,
            -1,
            label_type="MemberAccess",
            expression="req.body",
        )
        and nodes[ternary_option].get("label_type") != "MethodInvocation"
        for ternary_option in ternary_options
    )


def is_mongo_db_object(graph: Graph, n_id: NId, db_name: str) -> bool:
    nodes = graph.nodes
    for path in get_backward_paths(graph, n_id):
        if (
            (def_id := definition_search(graph, path, db_name))
            and (val_id := nodes[def_id].get("value_id"))
            and (nodes[val_id].get("label_type") == "MethodInvocation")
            and (nodes[val_id].get("expression") == "require")
            and (imported_lib := get_n_arg(graph, val_id, 0))
        ):
            return bool("mongodb" in nodes[imported_lib].get("value"))
    return False


def is_inside_db_operation(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    pred_n_ids = g.pred_ast(graph, n_id, 4)
    pred_types = [nodes[node].get("label_type") for node in pred_n_ids]
    danger_invocations = {"insert", "findOne"}

    return bool(
        (pred_types == ["Pair", "Object", "ArgumentList", "MethodInvocation"])
        and (nosql_op_n_id := pred_n_ids[-1])
        and (expression := nodes[nosql_op_n_id].get("expression"))
        and (expression.split(".")[-1] in danger_invocations)
        and is_mongo_db_object(graph, nosql_op_n_id, expression.split(".")[0]),
    )


def has_ternary_nosql_injection(graph: Graph, n_id: NId) -> bool:
    return bool(
        file_imports_module(graph, "mongodb", strict=False)
        and any_ternary_option_comes_from_request(graph, n_id)
        and is_inside_db_operation(graph, n_id),
    )
