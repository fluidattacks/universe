from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    TRUE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _ec2_has_not_an_iam_instance_profile(graph: Graph, nid: NId) -> NId | None:
    properties = get_optional_attribute(graph, nid, "Properties")
    if not properties:
        return None
    val_id = graph.nodes[properties[2]]["value_id"]

    launch_data = get_optional_attribute(graph, val_id, "LaunchTemplateData")
    data_id = val_id
    report_id = properties[2]
    if launch_data:
        data_id = graph.nodes[launch_data[2]]["value_id"]
        report_id = launch_data[2]
    if not get_optional_attribute(graph, data_id, "IamInstanceProfile"):
        return report_id
    return None


def _ec2_associate_public_ip_address(graph: Graph, nid: NId) -> Iterator[NId]:
    properties = get_optional_attribute(graph, nid, "Properties")
    if not properties:
        return
    val_id = graph.nodes[properties[2]]["value_id"]
    launch_data = get_optional_attribute(graph, val_id, "LaunchTemplateData")
    data_id = val_id
    if launch_data:
        data_id = graph.nodes[launch_data[2]]["value_id"]
    net_interface = get_optional_attribute(graph, data_id, "NetworkInterfaces")
    if net_interface:
        ni_attrs = graph.nodes[net_interface[2]]["value_id"]
        for c_id in adj_ast(graph, ni_attrs):
            public_ip = get_optional_attribute(graph, c_id, "AssociatePublicIpAddress")
            if public_ip and public_ip[1] in TRUE_OPTIONS:
                yield public_ip[2]


def cfn_ec2_associate_public_ip_address(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_EC2_ASSOCIATE_PUBLIC_IP_ADDRESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[1] in {
                "AWS::EC2::LaunchTemplate",
                "AWS::EC2::Instance",
            }:
                yield from _ec2_associate_public_ip_address(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_ec2_has_not_an_iam_instance_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_EC2_HAS_NOT_AN_IAM_INSTANCE_PROFILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::EC2::Instance"
                and (report := _ec2_has_not_an_iam_instance_profile(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
