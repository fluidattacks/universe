from lib_sast.root.f333.cloudformation import (
    cfn_ec2_associate_public_ip_address as _cfn_ec2_associate_public_address,
)
from lib_sast.root.f333.cloudformation import (
    cfn_ec2_has_not_an_iam_instance_profile as _cfn_ec2_not_iam_instance_prof,
)
from lib_sast.root.f333.terraform import (
    tfm_ec2_associate_public_ip_address as _tfm_ec2_associate_ip_address,
)
from lib_sast.root.f333.terraform import (
    tfm_ec2_has_not_an_iam_instance_profile as _tfm_ec2_not_iam_instance_prof,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_ec2_associate_public_ip_address(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_associate_public_address(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_ec2_has_not_an_iam_instance_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_not_iam_instance_prof(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_associate_public_ip_address(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_associate_ip_address(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_has_not_an_iam_instance_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_not_iam_instance_prof(shard, method_supplies)
