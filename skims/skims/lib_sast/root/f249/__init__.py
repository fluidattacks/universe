from lib_sast.root.f249.c_sharp import (
    c_sharp_sql_conn_hardcoded_secret as _c_sharp_sql_conn_hardcoded_secret,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_sql_conn_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_sql_conn_hardcoded_secret(shard, method_supplies)
