from lib_sast.root.f320.c_sharp import (
    c_sharp_ldap_connections_authenticated as _c_sharp_ldap_connections_auth,
)
from lib_sast.root.f320.c_sharp import (
    c_sharp_ldap_connections_directory as _c_sharp_ldap_connections_directory,
)
from lib_sast.root.f320.java import (
    java_anonymous_ldap_bind as _java_anonymous_ldap_bind,
)
from lib_sast.root.f320.python import (
    python_unsafe_ldap_connection as _python_unsafe_ldap_connection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_ldap_connections_authenticated(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_ldap_connections_auth(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_ldap_connections_directory(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_ldap_connections_directory(shard, method_supplies)


@SHIELD_BLOCKING
def java_anonymous_ldap_bind(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_anonymous_ldap_bind(shard, method_supplies)


@SHIELD_BLOCKING
def python_unsafe_ldap_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_unsafe_ldap_connection(shard, method_supplies)
