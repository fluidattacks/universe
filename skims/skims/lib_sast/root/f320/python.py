from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_danger_auth(graph: Graph, n_id: NId) -> bool:
    args_ids = adj_ast(graph, n_id)
    # Only if no password, or password is a literal or None it is deterministic
    if len(args_ids) < 2:
        return True
    n_attrs = graph.nodes[args_ids[1]]
    return n_attrs["label_type"] == "Literal" or n_attrs.get("value") == "None"


def python_unsafe_ldap_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_UNSAFE_LDAP_CONNECTIONS
    danger_set = {"simple_bind", "simple_bind_s", "bind", "bind_s"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["member"] in danger_set
                and (al_id := graph.nodes[pred_ast(graph, n_id)[0]].get("arguments_id"))
                and is_danger_auth(graph, al_id)
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_attrs["expression_id"],
                    {"ldap_connection"},
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
