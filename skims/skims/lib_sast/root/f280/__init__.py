from lib_sast.root.f280.javascript import (
    js_non_secure_construction as _js_non_secure_construction,
)
from lib_sast.root.f280.python import (
    python_xml_parser as _python_xml_parser,
)
from lib_sast.root.f280.typescript import (
    ts_non_secure_construction as _ts_non_secure_construction,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_non_secure_construction(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_non_secure_construction(shard, method_supplies)


@SHIELD_BLOCKING
def python_xml_parser(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _python_xml_parser(shard, method_supplies)


@SHIELD_BLOCKING
def ts_non_secure_construction(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_non_secure_construction(shard, method_supplies)
