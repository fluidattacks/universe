from lib_sast.root.f414.c_sharp import (
    c_sharp_disabled_http_header_check as _c_sharp_disabled_http_header_check,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_disabled_http_header_check(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_disabled_http_header_check(shard, method_supplies)
