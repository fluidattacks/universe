from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_node_vuln(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    if (  # noqa: SIM103
        (parent_id := pred_ast(graph, n_id)[0])
        and graph.nodes[parent_id].get("label_type") == "Assignment"
        and (value_id := graph.nodes[parent_id].get("value_id"))
        and get_node_evaluation_results(method, graph, value_id, set())
    ):
        return True

    return False


def c_sharp_disabled_http_header_check(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_DISABLED_HTTP_HEADER_CHECK

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("member") == "EnableHeaderChecking"
                and (expr_id := graph.nodes[n_id].get("expression_id"))
                and get_node_evaluation_results(method, graph, expr_id, {"http_runtime"})
                and is_node_vuln(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
