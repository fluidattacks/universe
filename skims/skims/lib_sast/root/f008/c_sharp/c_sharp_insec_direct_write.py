from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def _is_http_method(ma_attr: dict) -> bool:
    return ma_attr["member"] in NET_SYSTEM_HTTP_METHODS


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if ma_attr["expression"] == "Request" and _is_http_method(ma_attr):
        args.triggers.add("insecure")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
}


def _get_symbol_from_graph(graph: Graph, n_id: NId) -> tuple[str, NId]:
    if graph.nodes[n_id].get("label_type") == "MemberAccess" and (
        expr_id := graph.nodes[n_id].get("expression_id")
    ):
        n_id = expr_id

    if graph.nodes[n_id].get("label_type") == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol")
        return symbol, n_id
    return "", n_id


def _is_direct(graph: Graph, path: list) -> bool:
    for n_id in path:
        if (
            (exp_id := graph.nodes[n_id].get("expression_id"))
            and (exp := graph.nodes[exp_id].get("expression"))
            and exp == "Response"
        ):
            return True
    return False


def _is_direct_connection(graph: Graph, n_id: NId) -> bool:
    symbol, n_id = _get_symbol_from_graph(graph, n_id)
    if symbol:
        for path in get_backward_paths(graph, n_id):
            if _is_direct(graph, path):
                return True
    return False


def _is_insecure_direct_write(
    method: MethodsEnum,
    graph: Graph,
    expr_id: NId,
    args_id: NId,
) -> bool:
    return _is_direct_connection(graph, expr_id) and get_node_evaluation_results(
        method,
        graph,
        args_id,
        {"insecure"},
        method_evaluators=METHOD_EVALUATORS,
    )


def _get_n_ids(
    method_supplies: MethodSupplies,
    method: MethodsEnum,
    graph: Graph,
    danger_methods: set[str],
    is_insecure: Callable,
) -> Iterator[NId]:
    for n_id in method_supplies.selected_nodes:
        if (
            (expr := graph.nodes[n_id].get("expression"))
            and expr.split(".")[-1].lower() in danger_methods
            and (expr_id := graph.nodes[n_id].get("expression_id"))
            and (args_id := graph.nodes[n_id].get("arguments_id"))
            and is_insecure(method, graph, expr_id, args_id)
        ):
            yield n_id


def c_sharp_unsafe_direct_write(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSEC_DIRECT_WRITE
    danger_methods = {"addheader", "write"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        return _get_n_ids(
            method_supplies=method_supplies,
            method=method,
            graph=graph,
            danger_methods=danger_methods,
            is_insecure=_is_insecure_direct_write,
        )

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
