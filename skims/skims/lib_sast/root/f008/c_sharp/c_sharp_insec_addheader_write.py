from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    NET_CONNECTION_OBJECTS,
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.triggers.add("user_connection")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] in NET_CONNECTION_OBJECTS:
        args.triggers.add("user_connection")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _is_http_method(ma_attr: dict) -> bool:
    return ma_attr["member"] in NET_SYSTEM_HTTP_METHODS


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if _is_http_method(ma_attr):
        args.triggers.add("user_parameters")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
    "member_access": _member_access_evaluator,
}


def _get_symbol_from_graph(graph: Graph, n_id: NId) -> tuple[str, NId]:
    if graph.nodes[n_id].get("label_type") == "MemberAccess" and (
        expr_id := graph.nodes[n_id].get("expression_id")
    ):
        n_id = expr_id

    if graph.nodes[n_id].get("label_type") == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol")
        return symbol, n_id
    return "", n_id


def _is_direct(graph: Graph, path: list) -> bool:
    for n_id in path:
        if (
            (exp_id := graph.nodes[n_id].get("expression_id"))
            and (exp := graph.nodes[exp_id].get("expression"))
            and exp == "Response"
        ):
            return True
    return False


def _is_user_connection(graph: Graph, n_id: NId) -> bool:
    symbol, n_id = _get_symbol_from_graph(graph, n_id)
    if symbol:
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and graph.nodes[def_id].get("label_type") == "Parameter"
                and graph.nodes[def_id].get("variable_type") == "HttpResponse"
            ):
                return True
    return False


def _is_insecure_write(
    method: MethodsEnum,
    graph: Graph,
    expr_id: NId,
    args_id: NId,
) -> bool:
    return _is_user_connection(graph, expr_id) and get_node_evaluation_results(
        method,
        graph,
        args_id,
        {"user_parameters", "user_connection"},
        method_evaluators=METHOD_EVALUATORS,
    )


def _get_n_ids(
    method_supplies: MethodSupplies,
    method: MethodsEnum,
    graph: Graph,
    danger_methods: set[str],
    is_insecure: Callable,
) -> Iterator[NId]:
    for n_id in method_supplies.selected_nodes:
        if (
            (expr := graph.nodes[n_id].get("expression"))
            and expr.split(".")[-1].lower() in danger_methods
            and (expr_id := graph.nodes[n_id].get("expression_id"))
            and (args_id := graph.nodes[n_id].get("arguments_id"))
            and is_insecure(method, graph, expr_id, args_id)
        ):
            yield n_id


def c_sharp_unsafe_addheader_write(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSEC_ADDHEADER_WRITE
    danger_methods = {"addheader", "write"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        return _get_n_ids(
            method_supplies=method_supplies,
            method=method,
            graph=graph,
            danger_methods=danger_methods,
            is_insecure=_is_insecure_write,
        )

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _is_valid_status_description(
    n_id: NId,
    graph: Graph,
    method: MethodsEnum,
) -> bool:
    parent_id = pred_ast(graph, n_id)[0]
    if val_id := graph.nodes[parent_id].get("value_id"):
        return get_node_evaluation_results(
            method,
            graph,
            val_id,
            {"user_parameters", "user_connection"},
            method_evaluators=METHOD_EVALUATORS,
        )
    return False


def c_sharp_unsafe_status_write(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSEC_ADDHEADER_WRITE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["member"] == "StatusDescription"
                and _is_user_connection(graph, n_attrs["expression_id"])
                and _is_valid_status_description(n_id, graph, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
