from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f008.common import (
    unsafe_xss_content_nodes,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def ts_unsafe_xss_content(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.TYPESCRIPT_UNSAFE_XSS_CONTENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from unsafe_xss_content_nodes(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
