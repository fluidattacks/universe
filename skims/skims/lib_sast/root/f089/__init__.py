from lib_sast.root.f089.java import (
    java_trust_boundary_violation as _java_trust_boundary_violation,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_trust_boundary_violation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_trust_boundary_violation(shard, method_supplies)
