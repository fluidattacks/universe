from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_any_library_imported,
)
from lib_sast.root.utilities.java import (
    concatenate_name,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)

REQUIRED_IMPORTS = {"org.springframework.security.config.annotation.web.builders.HttpSecurity"}


def _get_http_security_param(graph: Graph, n_id: NId) -> str | None:
    params_id = graph.nodes[n_id].get("parameters_id")
    if params_id is None:
        return None
    for param in adj_ast(graph, params_id):
        if (
            graph.nodes[param].get("label_type") == "Parameter"
            and graph.nodes[param].get("variable_type") == "HttpSecurity"
            and (var_name := graph.nodes[param].get("variable"))
        ):
            return var_name
    return None


def _has_num_different_to_one(graph: Graph, n_id: NId) -> bool:
    if (
        graph.nodes[n_id].get("label_type") == "Literal"
        and graph.nodes[n_id].get("value_type") == "number"
    ):
        return int(graph.nodes[n_id].get("value")) > 1

    if (  # noqa: SIM103
        graph.nodes[n_id].get("label_type") == "UnaryExpression"
        and graph.nodes[n_id].get("operator") == "-"
        and (op_id := graph.nodes[n_id].get("operand_id"))
        and graph.nodes[op_id].get("label_type") == "Literal"
        and graph.nodes[op_id].get("value_type") == "number"
    ):
        return True

    return False


def _has_unsafe_value(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("label_type") == "SymbolLookup" and (
        symbol := graph.nodes[n_id].get("symbol")
    ):
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _has_num_different_to_one(graph, val_id)
            ):
                return True

    return _has_num_different_to_one(graph, n_id)


def _has_unsafe_max_sessions(graph: Graph, n_id: NId, http_sec_param: str) -> bool:
    block_id = graph.nodes[n_id].get("block_id")
    if block_id is None:
        return False

    for child in adj_ast(graph, block_id, -1, label_type="MethodInvocation"):
        if (
            graph.nodes[child].get("expression") == "maximumSessions"
            and (concat_name := concatenate_name(graph, child))
            and concat_name.split(".")[0] == http_sec_param
            and (first_arg := get_n_arg(graph, child, 0))
            and (_has_unsafe_value(graph, first_arg))
        ):
            return True
    return False


def java_spring_concurrent_sessions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_SPRING_CONCURRENT_SESSIONS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if is_any_library_imported(graph, REQUIRED_IMPORTS):
            for n_id in method_supplies.selected_nodes:
                if (
                    http_sec_param := _get_http_security_param(graph, n_id)
                ) and _has_unsafe_max_sessions(graph, n_id, http_sec_param):
                    yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
