from lib_sast.root.f413.c_sharp import (
    c_sharp_insecure_assembly_load as _c_sharp_insecure_assembly_load,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insecure_assembly_load(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_assembly_load(shard, method_supplies)
