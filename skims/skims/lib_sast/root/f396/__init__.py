from lib_sast.root.f396.cloudformation import (
    cfn_kms_key_is_key_rotation_absent_or_disabled as cfn_kms_rotation_absent,
)
from lib_sast.root.f396.terraform import (
    tfm_kms_key_is_key_rotation_absent_or_disabled as tfm_kms_rotation_absent,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_kms_key_is_key_rotation_absent_or_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return cfn_kms_rotation_absent(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_kms_key_is_key_rotation_absent_or_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return tfm_kms_rotation_absent(shard, method_supplies)
