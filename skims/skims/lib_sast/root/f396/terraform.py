from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _kms_danger_key_rotation(graph: Graph, nid: NId) -> NId | None:
    key_spec = get_optional_attribute(graph, nid, "customer_master_key_spec")
    if key_spec and key_spec[1] != "SYMMETRIC_DEFAULT":
        return None
    en_key_rot = get_optional_attribute(graph, nid, "enable_key_rotation")
    if not en_key_rot:
        return nid
    if en_key_rot[1] in {"false", "0"}:
        return en_key_rot[2]
    return None


def tfm_kms_key_is_key_rotation_absent_or_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_KMS_KEY_IS_KEY_ROTATION_ABSENT_OR_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_kms_key" and (
                report := _kms_danger_key_rotation(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
