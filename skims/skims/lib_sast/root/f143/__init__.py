from lib_sast.root.f143.javascript import (
    js_uses_eval as _js_uses_eval,
)
from lib_sast.root.f143.php import (
    php_uses_eval as _php_uses_eval,
)
from lib_sast.root.f143.typescript import (
    ts_uses_eval as _ts_uses_eval,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_uses_eval(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_uses_eval(shard, method_supplies)


@SHIELD_BLOCKING
def ts_uses_eval(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_uses_eval(shard, method_supplies)


@SHIELD_BLOCKING
def php_uses_eval(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _php_uses_eval(shard, method_supplies)
