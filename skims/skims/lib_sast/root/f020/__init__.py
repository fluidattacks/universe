from lib_sast.root.f020.java.java_null_cipher import (
    java_null_cipher as _java_null_cipher,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_null_cipher(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _java_null_cipher(shard, method_supplies)
