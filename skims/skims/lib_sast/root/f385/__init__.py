from lib_sast.root.f385.javascript import (
    js_exposed_private_key as _js_exposed_private_key,
)
from lib_sast.root.f385.javascript import (
    js_hardcoded_key_hmac as _js_hardcoded_key_hmac,
)
from lib_sast.root.f385.typescript import (
    ts_exposed_private_key as _ts_exposed_private_key,
)
from lib_sast.root.f385.typescript import (
    ts_hardcoded_key_hmac as _ts_hardcoded_key_hmac,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_exposed_private_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_exposed_private_key(shard, method_supplies)


@SHIELD_BLOCKING
def ts_exposed_private_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_exposed_private_key(shard, method_supplies)


@SHIELD_BLOCKING
def js_hardcoded_key_hmac(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_hardcoded_key_hmac(shard, method_supplies)


@SHIELD_BLOCKING
def ts_hardcoded_key_hmac(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_hardcoded_key_hmac(shard, method_supplies)
