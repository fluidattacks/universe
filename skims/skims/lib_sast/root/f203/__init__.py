from lib_sast.root.f203.cloudformation import (
    cfn_buckets_unauthorized_access as _cfn_buckets_unauthorized_access,
)
from lib_sast.root.f203.terraform import (
    tfm_bucket_policy_has_unauthorized_access as _tfm_bucket_unauth_access,
)
from lib_sast.root.f203.terraform import (
    tfm_public_bucket_acl as _tfm_public_bucket_acl,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_buckets_unauthorized_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_buckets_unauthorized_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_bucket_policy_has_unauthorized_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_bucket_unauth_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_public_bucket_acl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_public_bucket_acl(shard, method_supplies)
