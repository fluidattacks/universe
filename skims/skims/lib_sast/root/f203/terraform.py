from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f203.cloudformation import (
    _has_danger_actions,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_dict_from_attr,
    get_dict_values,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)
from utils.aws.iam import (
    ACTIONS,
)


def _bucket_public_acl(graph: Graph, nid: NId) -> Iterator[NId]:
    danger_perms = ["WRITE", "FULL_CONTROL", "WRITE_ACP"]
    for c_id in adj_ast(graph, nid, label_type="Object"):
        if (
            graph.nodes[c_id]["name"] == "grant"
            and (grantee := get_argument(graph, c_id, "grantee"))
            and (uri := get_optional_attribute(graph, grantee, "uri"))
        ):
            uri_value = uri[1]
            if (
                uri_value == "http://acs.amazonaws.com/groups/global/AllUsers"
                and (permission := get_optional_attribute(graph, c_id, "permission"))
                and permission[1] in danger_perms
            ):
                yield c_id


def tfm_public_bucket_acl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_PUBLIC_BUCKETS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_s3_bucket_acl" and (
                acp := get_argument(graph, nid, "access_control_policy")
            ):
                yield from _bucket_public_acl(graph, acp)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _analyze_bucket_policy_json(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    stmt = get_optional_attribute(graph, child_id, "Statement")
    if stmt:
        value_id = graph.nodes[stmt[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            effect = get_optional_attribute(graph, c_id, "Effect")
            principal = get_optional_attribute(graph, c_id, "Principal")
            if (
                effect
                and effect[1] == "Allow"
                and principal
                and principal[1] == "*"
                and _has_danger_actions(graph, c_id)
            ):
                yield effect[2]


def _analyze_bucket_policy_literal(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        if (
            stmt.get("Effect") == "Allow"
            and stmt.get("Principal") == "*"
            and (actions_list := stmt.get("Action"))
            and (
                any(action.split(":")[-1] not in ACTIONS["s3"]["read"] for action in actions_list)
                or "s3:*" in actions_list
            )
        ):
            yield attr_id


def _bucket_policy_has_danger_access(graph: Graph, nid: NId) -> Iterator[NId]:
    if (attr := get_optional_attribute(graph, nid, "policy")) and (
        val_id := graph.nodes[attr[2]]["value_id"]
    ):
        if graph.nodes[val_id]["label_type"] == "Literal":
            yield from _analyze_bucket_policy_literal(attr[1], attr[2])
        elif (
            graph.nodes[val_id]["label_type"] == "MethodInvocation"
            and graph.nodes[val_id]["expression"] == "jsonencode"
        ):
            yield from _analyze_bucket_policy_json(graph, val_id)


def tfm_bucket_policy_has_unauthorized_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_S3_BUCKETS_ALLOW_UNAUTHORIZED_PUBLIC_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_s3_bucket_policy":
                yield from _bucket_policy_has_danger_access(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
