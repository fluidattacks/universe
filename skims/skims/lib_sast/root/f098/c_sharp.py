from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_path_injection(graph: Graph, n_id: str, method: MethodsEnum) -> bool:
    if (  # noqa: SIM103
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (args_ids := adj_ast(graph, al_id))
        and len(args_ids) > 0
        and get_node_evaluation_results(method, graph, args_ids[0], {"user_connection"})
    ):
        return True
    return False


def c_sharp_path_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_PATH_INJECTION
    paths = {
        "System.IO.File.Open",
        "IO.File.Open",
        "File.Open",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id]["expression"] in paths and _is_path_injection(graph, n_id, method):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
