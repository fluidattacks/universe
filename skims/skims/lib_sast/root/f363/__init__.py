from lib_sast.root.f363.cloudformation import (
    cfn_insecure_generate_secret_string as _cfn_insec_generate_secret_string,
)
from lib_sast.root.f363.terraform import (
    tfm_insecure_generate_secret_string as _tfm_insec_generate_secret_string,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_insecure_generate_secret_string(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_insec_generate_secret_string(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_insecure_generate_secret_string(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_insec_generate_secret_string(shard, method_supplies)
