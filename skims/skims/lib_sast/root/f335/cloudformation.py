from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _s3_bucket_versioning_disabled(graph: Graph, nid: NId) -> NId | None:
    properties = get_optional_attribute(graph, nid, "Properties")
    if not properties:
        return None
    val_id = graph.nodes[properties[2]]["value_id"]
    version = get_optional_attribute(graph, val_id, "VersioningConfiguration")
    if version:
        data_id = graph.nodes[version[2]]["value_id"]
        status = get_optional_attribute(graph, data_id, "Status")
        if not status:
            return version[2]
        if status[1] != "Enabled":
            return status[2]
    else:
        return properties[2]
    return None


def cfn_s3_bucket_versioning_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_S3_BUCKET_VERSIONING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::S3::Bucket"
                and (report := _s3_bucket_versioning_disabled(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
