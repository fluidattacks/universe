from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def tfm_s3_bucket_versioning_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_S3_BUCKET_VERSIONING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                graph.nodes[nid].get("name") == "aws_s3_bucket_versioning"
                and (ver_conf := get_argument(graph, nid, "versioning_configuration"))
                and (status := get_optional_attribute(graph, ver_conf, "status"))
                and status[1].lower() in {"disabled", "suspended"}
            ):
                yield status[2]

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
