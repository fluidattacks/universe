from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from model.core import (
    MethodExecutionResult,
)


def ts_regex_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.TS_REGEX_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id]["expression"].split(".")[
                -1
            ] == "RegExp" and get_node_evaluation_results(
                method,
                graph,
                n_id,
                {"UserConnection"},
                danger_goal=False,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
