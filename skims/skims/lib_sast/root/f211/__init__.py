from lib_sast.root.f211.c_sharp import (
    c_sharp_regex_injection as _c_sharp_regex_injection,
)
from lib_sast.root.f211.c_sharp import (
    c_sharp_vuln_regular_expression as _c_sharp_vuln_regular_expression,
)
from lib_sast.root.f211.java import (
    java_vuln_regular_expression as _java_vuln_regular_expression,
)
from lib_sast.root.f211.javascript import (
    js_regex_injection as _js_regex_injection,
)
from lib_sast.root.f211.kotlin import (
    kt_vuln_regular_expression as _kt_vuln_regular_expression,
)
from lib_sast.root.f211.python import (
    python_regex_dos as _python_regex_dos,
)
from lib_sast.root.f211.python import (
    python_regex_injection as _python_regex_injection,
)
from lib_sast.root.f211.typescript import (
    ts_regex_injection as _ts_regex_injection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_regex_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_regex_injection(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_vuln_regular_expression(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_vuln_regular_expression(shard, method_supplies)


@SHIELD_BLOCKING
def java_vuln_regular_expression(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_vuln_regular_expression(shard, method_supplies)


@SHIELD_BLOCKING
def js_regex_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_regex_injection(shard, method_supplies)


@SHIELD_BLOCKING
def kt_vuln_regular_expression(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_vuln_regular_expression(shard, method_supplies)


@SHIELD_BLOCKING
def python_regex_dos(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _python_regex_dos(shard, method_supplies)


@SHIELD_BLOCKING
def python_regex_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_regex_injection(shard, method_supplies)


@SHIELD_BLOCKING
def ts_regex_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_regex_injection(shard, method_supplies)
