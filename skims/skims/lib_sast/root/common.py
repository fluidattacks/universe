import json
import os
import time
from collections.abc import (
    Iterable,
)
from pathlib import (
    Path,
)

import ctx
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.sast_model import (
    GraphShard,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
    Vulnerability,
)
from utils.build_vulns import (
    build_lines_vuln,
    build_metadata,
)
from utils.statics import (
    calculate_time_coefficient,
)
from utils.string_handlers import (
    is_exclusion,
    make_snippet_func,
)
from utils.translations import (
    t,
)


def load_json_safe(filepath: str) -> dict:
    if not os.path.exists(filepath):  # noqa: PTH110
        return {}
    with Path(filepath).open(encoding="utf-8") as file:
        return json.load(file)


def get_vulnerability_from_n_id(
    *,
    n_id: str,
    graph_shard: GraphShard,
    method: MethodsEnum,
    desc_params: dict[str, str],
) -> Vulnerability:
    node_attrs = graph_shard.graph.nodes[n_id]
    node_attrs_line = node_attrs["label_l"]
    content = graph_shard.content_as_str

    vuln_what = graph_shard.path
    return build_lines_vuln(
        method=method.value,
        what=vuln_what,
        where=str(node_attrs_line),
        metadata=build_metadata(
            method=method.value,
            description=(
                f"{t(key=method.name, **desc_params)} {t(key='words.in')} "
                f"{ctx.SKIMS_CONFIG.namespace}/{vuln_what}"
            ),
            snippet=make_snippet_func(
                file_content=content,
                line=int(node_attrs_line),
                column=0,
                path=vuln_what,
            ),
            skip=is_exclusion(content, int(node_attrs_line)),
        ),
    )


def get_vulnerabilities_from_n_ids_metadata(
    n_ids_metadata: Iterable[tuple[NId, dict]],
    query_supplies: QuerySupplies,
) -> MethodExecutionResult:
    method = query_supplies.method
    init_time = time.time()

    vulns = (
        get_vulnerability_from_n_id(
            n_id=n_id,
            graph_shard=query_supplies.graph_shard,
            method=method,
            desc_params=metadata,
        )
        for n_id, metadata in n_ids_metadata
    )

    elapsed_time = time.time() - init_time

    return MethodExecutionResult(
        method_name=method.value.name,
        time_coefficient=calculate_time_coefficient(
            elapsed_time,
            query_supplies.graph_shard.path,
            query_supplies.method_calls,
        ),
        vulnerabilities=tuple(vulns),
    )


def get_vulnerabilities_from_n_ids(
    n_ids: Iterable[str],
    query_supplies: QuerySupplies,
) -> MethodExecutionResult:
    method = query_supplies.method
    init_time = time.time()
    vulns = (
        get_vulnerability_from_n_id(
            n_id=n_id,
            graph_shard=query_supplies.graph_shard,
            method=method,
            desc_params={},
        )
        for n_id in n_ids
    )

    elapsed_time = time.time() - init_time

    return MethodExecutionResult(
        method_name=method.value.name,
        time_coefficient=calculate_time_coefficient(
            elapsed_time,
            query_supplies.graph_shard.path,
            query_supplies.method_calls,
        ),
        vulnerabilities=tuple(vulns),
    )


def is_test_file(path: str) -> bool:
    target_path = path.split("skims/test")[-1]
    return bool("test" in target_path or target_path.endswith(".spec.ts"))
