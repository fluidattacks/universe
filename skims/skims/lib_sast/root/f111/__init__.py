from lib_sast.root.f111.c_sharp.c_sharp_memory_marshal_create_span import (
    c_sharp_memory_marshal_create_span as _c_sharp_memory_marshal_create_span,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_memory_marshal_create_span(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_memory_marshal_create_span(shard, method_supplies)
