from lib_sast.root.f108.typescript import (
    ts_express_insecure_rate_limit as _ts_express_insecure_rate_limit,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def ts_express_insecure_rate_limit(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_express_insecure_rate_limit(shard, method_supplies)
