from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def has_any_danger_element(graph: Graph, access_n_id: NId) -> bool:
    nodes = graph.nodes
    return bool(
        (args_id := nodes[access_n_id].get("arguments_id"))
        and (nodes[args_id].get("value") == "X-Forwarded-For")
        and (p_ids := g.pred_ast(graph, access_n_id))
        and (nodes[p_ids[0]].get("label_type") != "ArgumentList"),
    )


def get_improper_limit_handling_n_id(graph: Graph, declaration_n_id: NId) -> NId | None:
    return_n_ids = g.adj_ast(graph, declaration_n_id, depth=-1, label_type="Return")
    for n_id in return_n_ids:
        for returned_access_n_ids in g.adj_ast(graph, n_id, depth=-1, label_type="ElementAccess"):
            if has_any_danger_element(graph, returned_access_n_ids):
                return n_id
    return None


def ts_express_insecure_rate_limit(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TS_EXPRESS_INSECURE_RATE_LIMIT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        if not file_imports_module(graph, "express-rate-limit"):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                (graph.nodes[n_id].get("name") == "RateLimit")
                and (f_arg := get_n_arg(graph, n_id, 0))
                and (
                    callbacks_n_ids := g.adj_ast(
                        graph,
                        f_arg,
                        label_type="MethodDeclaration",
                        name="keyGenerator",
                    )
                )
                and (vuln_n_id := get_improper_limit_handling_n_id(graph, callbacks_n_ids[0]))
            ):
                yield vuln_n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
