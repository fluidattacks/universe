from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_salt_literal(graph: Graph, al_id: NId) -> bool:
    n_id = adj_ast(graph, al_id)[0]
    if graph.nodes[n_id]["label_type"] == "Literal":
        return True

    if graph.nodes[n_id]["label_type"] != "SymbolLookup":
        return False
    searched_symbol = graph.nodes[n_id]["symbol"]
    for path in get_backward_paths(graph, n_id):
        if (
            (var_def := definition_search(graph, path, searched_symbol))
            and graph.nodes[var_def]["label_type"] == "VariableDeclaration"
            and (value_id := graph.nodes[var_def].get("value_id"))
            and graph.nodes[value_id].get("value_type") == "string"
        ):
            return True

    return False


def dart_salting_is_harcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.DART_SALT_IS_HARDCODED
    danger_methods = {
        "Pbkdf2Parameters",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (expr := graph.nodes[n_id].get("expression"))
                and expr.rsplit(".", maxsplit=1)[-1] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and is_salt_literal(graph, al_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
