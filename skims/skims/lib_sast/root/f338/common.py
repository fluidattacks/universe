from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    matching_nodes,
    pred_ast,
)


def get_created_hash_variables(graph: Graph) -> list[str]:
    return [
        var_name
        for n_id in matching_nodes(graph, label_type="VariableDeclaration")
        if (
            (val_id := graph.nodes[n_id].get("value_id"))
            and graph.nodes[val_id].get("expression") in {"createHash", "crypto.createHash"}
            and (var_name := graph.nodes[n_id].get("variable"))
        )
    ]


def get_vuln_nodes(graph: Graph, n_id: NId, hashes: list[str]) -> bool:
    if not file_imports_module(graph, "crypto"):
        return False
    if graph.nodes[n_id]["member"] == "update" and graph.nodes[n_id]["expression"] in hashes:
        parent_id = pred_ast(graph, n_id)[0]
        if first_arg := get_n_arg(graph, parent_id, 0):
            return has_string_definition(graph, first_arg) is not None
    return False
