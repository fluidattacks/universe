from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
    pred_ast,
    search_pred_until_type,
)
from model.core import (
    MethodExecutionResult,
)


def is_in_method_invocation(graph: Graph, n_id: NId) -> bool:
    if (parents_id := pred_ast(graph, n_id, 2)) and len(parents_id) > 1:
        return graph.nodes[parents_id[1]]["label_type"] == "MethodInvocation"

    return False


def custom_is_sanitized(graph: Graph, n_id: NId) -> bool:
    if not (
        (iv_var_name := graph.nodes[n_id].get("symbol"))
        and (md_n_ids := search_pred_until_type(graph, n_id, {"MethodDeclaration"}))
    ):
        return False

    return any(
        symbol_n_id != n_id
        and graph.nodes[symbol_n_id].get("symbol") == iv_var_name
        and is_in_method_invocation(graph, symbol_n_id)
        for symbol_n_id in match_ast_group_d(graph, md_n_ids[0], "SymbolLookup", -1)
    )


def java_hardcoded_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_HARDCODED_INIT_VECTOR

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("javax.crypto.spec",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("name", "") == "IvParameterSpec"
                and (f_arg := get_n_arg(graph, n_id, 0))
                and get_node_evaluation_results(method, graph, f_arg, set())
                and not custom_is_sanitized(graph, f_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
