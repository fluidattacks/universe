from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_hardcoded_iv(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        graph.nodes[n_id]["label_type"] == "ArrayInitializer"
        and (expr_stmt := match_ast_d(graph, n_id, "ExpressionStatement"))
        and all(graph.nodes[_id]["label_type"] == "Literal" for _id in adj_ast(graph, expr_stmt))
    ):
        return True
    return False


def eval_danger_iv_value(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        (parent := pred_ast(graph, n_id)[0])
        and graph.nodes[parent]["label_type"] == "Assignment"
        and (assignment_val := graph.nodes[parent]["value_id"])
        and is_node_definition_unsafe(graph, assignment_val, _is_hardcoded_iv)
    ):
        return True
    return False


def c_sharp_hardcoded_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_HARDCODED_INIT_VECTOR

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (member := graph.nodes[n_id].get("member"))
                and member == "IV"
                and eval_danger_iv_value(graph, n_id)
                and (expr_id := graph.nodes[n_id].get("expression_id"))
                and get_node_evaluation_results(method, graph, expr_id, {"createAes"})
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_insecure_cbc_iv(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_CBC_IV

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (exp_id := graph.nodes[n_id].get("expression_id"))
                and graph.nodes[exp_id].get("member") == "CreateEncryptor"
                and (second_arg := get_n_arg(graph, n_id, 1))
                and is_node_definition_unsafe(graph, second_arg, _is_hardcoded_iv)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
