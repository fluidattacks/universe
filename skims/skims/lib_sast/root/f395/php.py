from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def eval_danger_arg(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    if (label_type := nodes[n_id].get("label_type")) == "Literal":
        return True

    if label_type == "SymbolLookup":
        symbol = nodes[n_id].get("symbol")
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := nodes[def_id].get("value_id"))
                and nodes[val_id].get("label_type") == "Literal"
            ):
                return True
    return False


def php_hardcoded_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_HARDCODED_INIT_VECTOR

    danger_invocations = {"openssl_encrypt", "openssl_decrypt"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") in danger_invocations
                and (iv_arg_n_id := get_n_arg(graph, n_id, 4))
                and eval_danger_arg(graph, iv_arg_n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
