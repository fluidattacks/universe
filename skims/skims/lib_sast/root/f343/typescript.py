from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f343.common import (
    webpack_insecure_compression,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def ts_insecure_compression(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method: MethodsEnum = MethodsEnum.TYPESCRIPT_INSECURE_COMPRESSION_ALGORITHM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not (danger_library := get_default_alias(graph, "compression-webpack-plugin")):
            return

        for n_id in method_supplies.selected_nodes:
            if v_id := webpack_insecure_compression(graph, danger_library, n_id, method):
                yield v_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=MethodsEnum.TYPESCRIPT_INSECURE_COMPRESSION_ALGORITHM,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
