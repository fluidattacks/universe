from lib_sast.root.utilities.common import (
    get_import_nodes,
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
)

UNSAFE_PROTOCOL_METHODS = {
    "TLSv1_1_method",
    "TLSv1_method",
    "SSLv3_method",
}


UNSAFE_PROTOCOL_VERSION = {
    "TLSv1.1",
    "TLSv1",
    "SSLv3",
}


def has_unsafe_option(graph: Graph, n_id: NId, options: set[str], unsafe_values: set[str]) -> bool:
    return (
        (key_id := graph.nodes[n_id].get("key_id"))
        and graph.nodes[key_id].get("label_type") == "SymbolLookup"
        and graph.nodes[key_id].get("symbol") in options
        and (val_id := graph.nodes[n_id].get("value_id"))
        and graph.nodes[val_id].get("label_type") == "Literal"
        and graph.nodes[val_id].get("value_type") == "string"
        and graph.nodes[val_id].get("value") in unsafe_values
    )


def has_unsafe_secure_options(graph: Graph, n_id: NId) -> bool:
    protocols_to_turn_off = {
        "SSL_OP_NO_SSLv2": False,
        "SSL_OP_NO_SSLv3": False,
        "SSL_OP_NO_TLSv1": False,
        "SSL_OP_NO_TLSv1_1": False,
    }

    if (
        (key_id := graph.nodes[n_id].get("key_id"))
        and graph.nodes[key_id].get("label_type") == "SymbolLookup"
        and graph.nodes[key_id].get("symbol") == "secureOptions"
        and (val_id := graph.nodes[n_id].get("value_id"))
        and graph.nodes[val_id].get("label_type") == "BinaryOperation"
    ):
        for child in adj_ast(graph, val_id, -1, label_type="MemberAccess"):
            if (member := graph.nodes[child].get("member")) and member in protocols_to_turn_off:
                protocols_to_turn_off[member] = True

        if any(value is False for value in protocols_to_turn_off.values()):
            return True

    return False


def aux_has_unsafe_options(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("label_type") != "Object":
        return False

    for child in adj_ast(graph, n_id, label_type="Pair"):
        if (
            has_unsafe_option(graph, child, {"secureProtocol"}, UNSAFE_PROTOCOL_METHODS)
            or has_unsafe_option(
                graph,
                child,
                {"minVersion", "maxVersion"},
                UNSAFE_PROTOCOL_VERSION,
            )
            or has_unsafe_secure_options(graph, child)
        ):
            return True

    return False


def has_unsafe_options(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("label_type") == "SymbolLookup" and (
        symbol := graph.nodes[n_id].get("symbol")
    ):
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and aux_has_unsafe_options(graph, val_id)
            ):
                return True

        return False

    return aux_has_unsafe_options(graph, n_id)


def check_insecure(
    graph: Graph,
    n_id: NId,
    aliases: dict[str, str | None],
    exp_parts: list[str],
    checks: list[tuple[str, str, int]],
) -> bool:
    for alias_key, action, arg_index in checks:
        if (
            (alias_val := aliases.get(alias_key))
            and exp_parts[0] == alias_val
            and exp_parts[1] == action
            and (arg := get_n_arg(graph, n_id, arg_index))
            and has_unsafe_options(graph, arg)
        ):
            return True
    return False


def has_insecure_config(
    graph: Graph,
    n_id: NId,
    aliases: dict[str, str | None],
) -> bool:
    if not (exp := graph.nodes[n_id].get("expression")):
        return False

    if not (exp_parts := exp.split(".")) or len(exp_parts) != 2:
        return False

    checks = [("https_alias", "request", 0), ("tls_alias", "connect", 2)]
    return check_insecure(graph, n_id, aliases, exp_parts, checks)


def custom_requires_modules(graph: Graph, module_names: set[str]) -> str | None:
    for _id in match_ast_group_d(graph, "1", "VariableDeclaration"):
        if (
            (val_id := graph.nodes[_id].get("value_id"))
            and (graph.nodes[val_id].get("expression") == "require")
            and (library_n_id := get_n_arg(graph, val_id, 0))
            and (lib_name := graph.nodes[library_n_id].get("value"))
            and lib_name in module_names
        ):
            return graph.nodes[_id].get("variable")

    return None


def custom_get_default_aliases(graph: Graph, module_names: set[str]) -> str | None:
    for n_id in get_import_nodes(graph):
        if (
            (m_name := graph.nodes[n_id].get("expression"))
            and m_name.split(".")[0] in module_names
            and (alias := graph.nodes[n_id].get("label_alias"))
        ):
            return alias
    if alias := custom_requires_modules(graph, module_names):
        return alias

    return None


def uses_weak_ssl_tls_protocol(graph: Graph, method_supplies: MethodSupplies) -> list[NId]:
    aliases: dict[str, str | None] = {
        "https_alias": custom_get_default_aliases(graph, {"https", "node:https"}),
        "tls_alias": custom_get_default_aliases(graph, {"tls", "node:tls"}),
    }
    if any(value is not None for value in aliases.values()):
        return [
            n_id
            for n_id in method_supplies.selected_nodes
            if has_insecure_config(graph, n_id, aliases)
        ]
    return []
