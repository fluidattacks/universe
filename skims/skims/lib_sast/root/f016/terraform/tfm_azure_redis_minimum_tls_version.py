from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def tfm_azure_redis_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_REDIS_MINIMUM_TLS_VERSION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_redis_cache",
                method_supplies.selected_nodes,
            ),
        )
        without_minimum_tls_version = list(
            filter(
                lambda nid: get_optional_attribute(graph, nid, "minimum_tls_version") is None,
                resource_nids,
            ),
        )
        minimum_tls_version: list[NId] = [
            attr[2]
            for nid in resource_nids
            if (attr := get_optional_attribute(graph, nid, "minimum_tls_version"))
            and attr[1] in {"1.0", "1.1"}
        ]
        yield from without_minimum_tls_version + minimum_tls_version

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
