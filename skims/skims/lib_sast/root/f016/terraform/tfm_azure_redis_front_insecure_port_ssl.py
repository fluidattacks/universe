from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def tfm_azure_redis_front_insecure_port_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_REDIS_FRONT_INSECURE_PORT_SSL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_redis_cache",
                method_supplies.selected_nodes,
            ),
        )
        ssl_enforce_disabled: list[NId] = [
            attr[2]
            for nid in resource_nids
            if (attr := get_optional_attribute(graph, nid, "non_ssl_port_enabled"))
            and attr[1] == "false"
        ]
        yield from ssl_enforce_disabled

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
