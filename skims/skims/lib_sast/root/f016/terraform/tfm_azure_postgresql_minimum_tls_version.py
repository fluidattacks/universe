from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def tfm_azure_postgresql_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_POSTGRESQL_MINIMUM_TLS_VERSION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_postgresql_server",
                method_supplies.selected_nodes,
            ),
        )
        ssl_minimal_tls: list[NId] = [
            attr[2]
            for nid in resource_nids
            if (attr := get_optional_attribute(graph, nid, "ssl_minimal_tls_version_enforced"))
            and attr[1] in {"TLSEnforcementDisabled", "TLS1_0", "TLS1_1"}
        ]
        yield from ssl_minimal_tls

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
