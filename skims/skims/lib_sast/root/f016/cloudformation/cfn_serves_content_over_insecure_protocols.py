from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)
from model.core import (
    MethodExecutionResult,
)

VULN_ORIGIN_SSL_PROTOCOLS = ["SSLv3", "TLSv1", "TLSv1.1"]
VULNERABLE_MIN_PROT_VERSIONS = [
    "SSLv3",
    "TLSv1",
    "TLSv1_2016",
    "TLSv1.1_2016",
]


def _helper_insecure_protocols(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (object_id := match_ast_d(graph, nid, "Object"))
        and (c_origin := get_optional_attribute(graph, object_id, "CustomOriginConfig"))
        and (custom_attrs := graph.nodes[c_origin[2]]["value_id"])
        and (ssl_prot := get_optional_attribute(graph, custom_attrs, "OriginSSLProtocols"))
    ):
        ssl_list_id = graph.nodes[ssl_prot[2]]["value_id"]
        for c_id in adj_ast(graph, ssl_list_id):
            if graph.nodes[c_id].get("value") in VULN_ORIGIN_SSL_PROTOCOLS:
                yield c_id


def _serves_content_over_insecure_protocols(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (prop := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[prop[2]]["value_id"])
        and (dist_config := get_optional_attribute(graph, val_id, "DistributionConfig"))
        and (dist_attrs := graph.nodes[dist_config[2]]["value_id"])
    ):
        if (
            (v_cert := get_optional_attribute(graph, dist_attrs, "ViewerCertificate"))
            and (v_attrs := graph.nodes[v_cert[2]]["value_id"])
            and (min_prot := get_optional_attribute(graph, v_attrs, "MinimumProtocolVersion"))
            and min_prot[1] in VULNERABLE_MIN_PROT_VERSIONS
        ):
            yield min_prot[2]
        if origins := get_optional_attribute(graph, dist_attrs, "Origins"):
            origin_attr = graph.nodes[origins[2]]["value_id"]
            yield from _helper_insecure_protocols(graph, origin_attr)


def cfn_serves_content_over_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_SERVES_CONTENT_OVER_INSECURE_PROTOCOLS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[1] in {
                "AWS::CloudFront::Distribution",
            }:
                yield from _serves_content_over_insecure_protocols(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
