from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def java_unsafe_ssl_tls_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UNSAFE_SSL_TLS_PROTOCOL

    def n_ids() -> Iterator[NId]:
        danger_ssl_tls_versions = {"ssl", "sslv2hello", "sslv3", "tls1", "tlsv1", "tlsv1.1"}
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "setProperty"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and graph.nodes[first_arg].get("value", "") == "jdk.tls.client.protocols"
                and (second_arg := get_n_arg(graph, n_id, 1))
                and any(
                    value in danger_ssl_tls_versions
                    for value in graph.nodes[second_arg].get("value", "").lower().split(",")
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
