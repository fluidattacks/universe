from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def java_unsafe_default_http_client(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UNSAFE_DEFAULT_HTTP_CLIENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("org.apache.http.impl.client",)):
            return

        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("name", "") == "DefaultHttpClient":
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
