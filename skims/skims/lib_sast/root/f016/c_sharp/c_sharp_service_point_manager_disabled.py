from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _literal_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    member_str = "Switch.System.ServiceModel." + "DisableUsingServicePointManagerSecurityProtocols"

    if args.graph.nodes[args.n_id]["value"] == member_str:
        args.evaluation[args.n_id] = True
        args.triggers.add(member_str)
    if args.graph.nodes[args.n_id]["value"] == "true":
        args.evaluation[args.n_id] = True
        args.triggers.add("true")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "literal": _literal_evaluator,
}


def _is_point_manager_vulnerable(
    method: MethodsEnum,
    graph: Graph,
    n_id: str,
    method_supplies: MethodSupplies,
) -> NId | None:
    member_str = "Switch.System.ServiceModel.DisableUsingServicePointManagerSecurityProtocols"
    rules = {member_str, "true"}
    pred = pred_ast(graph, n_id)[0]
    if get_node_evaluation_results(
        method,
        graph,
        pred,
        rules,
        graph_db=method_supplies.graph_db,
        method_evaluators=METHOD_EVALUATORS,
    ):
        return pred
    return None


def c_sharp_service_point_manager_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_SERVICE_POINT_MANAGER_DISABLED
    members = {"AppContext"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("expression") in members
                and graph.nodes[node]["member"] == "SetSwitch"
                and (nid := _is_point_manager_vulnerable(method, graph, node, method_supplies))
            ):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
