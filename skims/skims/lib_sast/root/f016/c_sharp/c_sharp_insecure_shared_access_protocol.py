from collections.abc import (
    Callable,
    Iterable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_object_identifiers,
)
from model.core import (
    MethodExecutionResult,
)


def _member_access_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    args.evaluation[args.n_id] = (
        ma_attr["expression"] == "SharedAccessProtocol" and ma_attr["member"] == "HttpsOrHttp"
    )
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
}


def _is_insecure_protocol(graph: Graph, n_id: str, obj_identifiers: Iterable[str]) -> bool:
    method = MethodsEnum.C_SHARP_INSECURE_SHARED_ACCESS_PROTOCOL
    if (  # noqa: SIM103
        (expr := graph.nodes[n_id].get("expression"))
        and (split_expr := str(expr).split(".", maxsplit=1))
        and split_expr[0] in obj_identifiers
        and split_expr[-1] == "GetSharedAccessSignature"
        and get_node_evaluation_results(
            method,
            graph,
            n_id,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        )
    ):
        return True
    return False


def c_sharp_insecure_shared_access_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_SHARED_ACCESS_PROTOCOL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        obj_identifiers = get_object_identifiers(graph, {"CloudFile"})
        if not obj_identifiers:
            return

        for node in method_supplies.selected_nodes:
            if _is_insecure_protocol(graph, node, obj_identifiers):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
