from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids_metadata,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def c_sharp_weak_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_WEAK_PROTOCOL
    weak_protocols = ["Ssl3", "Tls", "Tls11", "None"]

    def n_ids() -> Iterator[tuple[NId, dict]]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            protocol = graph.nodes[node].get("member")
            if (
                graph.nodes[node].get("expression") == "SecurityProtocolType"
                and protocol in weak_protocols
            ):
                yield node, {"protocol": protocol}

    return get_vulnerabilities_from_n_ids_metadata(
        n_ids_metadata=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
