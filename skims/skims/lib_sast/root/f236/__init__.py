from lib_sast.root.f236.conf_files import (
    tsconfig_sourcemap_enabled as _tsconfig_sourcemap_enabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def tsconfig_sourcemap_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tsconfig_sourcemap_enabled(shard, method_supplies)
