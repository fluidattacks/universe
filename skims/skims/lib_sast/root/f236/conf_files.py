from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_key_value,
    is_parent,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _sourcemap_enabled(graph: Graph, nid: NId, key_pair: str, value: str) -> bool:
    if key_pair == "sourceMap" and value.lower() == "true":
        tsconfig_correct_parents = ["compilerOptions"]
        angular_vuln_parents_path = ["production", "configurations", "build"]
        if is_parent(graph, nid, tsconfig_correct_parents):
            return True
        if is_parent(graph, nid, angular_vuln_parents_path):
            return True
    if key_pair == "sourceMaps" and value.lower() == "true":
        serverless_correct_parents = ["configurations"]
        if is_parent(graph, nid, serverless_correct_parents):
            return True
    return False


def tsconfig_sourcemap_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TSCONFIG_SOURCEMAP_ENABLED

    def n_ids() -> Iterator[NId]:
        if shard.path.endswith("tsconfig.spec.json"):
            return
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            key, value = get_key_value(graph, nid)

            if _sourcemap_enabled(graph, nid, key, value):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
