from lib_sast.root.utilities.common import (
    get_import_nodes,
)
from lib_sast.sast_model import (
    Graph,
)


def get_all_imports_exp(graph: Graph) -> list[str]:
    return [graph.nodes[n_id]["expression"] for n_id in get_import_nodes(graph)]


def _is_dangerous_method_in_imports(
    method: str,
    import_lib: str,
    imps: list[str],
    n_attrs: dict[str, str],
) -> bool:
    return n_attrs["expression"] == method and f"{import_lib}.{method.split('.')[0]}" in imps


def _is_member_access(graph: Graph, import_lib: str, method: str, n_attrs: dict[str, str]) -> bool:
    expr_id = n_attrs["expression_id"]

    return (
        len(method.split(".")) == 1
        and graph.nodes[expr_id]["label_type"] == "MemberAccess"
        and (member := graph.nodes[expr_id]["member"])
        and member == method
        and (expression := graph.nodes[expr_id]["expression"])
        and expression == import_lib
    )


def _is_method_in_import_lib(method: str, import_lib: str, n_attrs: dict[str, str]) -> bool:
    return len(method.split(".")) == 1 and n_attrs["expression"] == f"{import_lib}.{method}"


def check_method_origin(graph: Graph, import_lib: str, danger_methods: set, n_attrs: dict) -> bool:
    imps = get_all_imports_exp(graph)
    for method in danger_methods:
        if (
            _is_dangerous_method_in_imports(method, import_lib, imps, n_attrs)
            or _is_member_access(graph, import_lib, method, n_attrs)
            or _is_method_in_import_lib(method, import_lib, n_attrs)
        ):
            return True
    return False
