from collections.abc import (
    Iterator,
)

from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    match_ast,
    matching_nodes,
)


def yield_syntax_graph_member_access(graph: Graph, members: set[str]) -> Iterator[NId]:
    for nid in matching_nodes(graph, label_type="MemberAccess"):
        if graph.nodes[nid].get("expression") in members:
            yield nid


def get_first_member_syntax_graph(graph: Graph, n_id: str) -> str | None:
    childs = match_ast(graph, n_id, "MemberAccess")
    searched_node = childs.get("MemberAccess")

    while searched_node:
        childs = match_ast(graph, searched_node, "MemberAccess")
        searched_node = childs.get("MemberAccess")

    return childs["__0__"]
