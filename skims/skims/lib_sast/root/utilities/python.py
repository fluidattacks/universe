from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph import (
    adj_ast,
)

USER_INPUTS = (
    "request.get_data",
    "request.get_json",
    "request.cookies",
    "request.get",
    "request.post",
    "request.args",
    "request.data",
    "request.files",
    "request.headers",
    "request.form",
    "request.values",
    "request.json",
    "request.body",
    "request.meta",
    "request.session",
    "request.user",
    "request.user_agent",
    "request.referrer",
    "request.referer",
    "request.content_params",
)


def contains_user_input(graph: Graph, n_id: NId) -> bool:
    for child in (n_id, *adj_ast(graph, n_id, -1, label_type="MemberAccess")):
        expression = (
            graph.nodes[child].get("expression", "") + "." + graph.nodes[child].get("member", "")
        )
        if expression.lower().startswith(USER_INPUTS):
            return True

    return False


def resolve_name(graph: Graph, imports_n_ids: list[NId], lib_name: str) -> str | None:
    nodes = graph.nodes
    tokens = lib_name.split(".")
    for n_id in imports_n_ids:
        for token in range(1, len(tokens) + 1):
            main_name = ".".join(tokens[:token])
            trailing_name = ".".join(tokens[token:])

            if nodes[n_id].get("expression") == main_name:
                if alias := nodes[n_id].get("label_alias"):
                    main_name = alias

                if trailing_name:
                    main_name += "." + trailing_name

                return main_name
    return None


def get_danger_imported_names(graph: Graph, lib_names: set[str]) -> set[str]:
    imports_n_ids = [
        _id for _id in g.adj_ast(graph, "1") if graph.nodes[_id].get("label_type") == "Import"
    ]
    danger_callings: set[str] = set()
    for lib_name in lib_names:
        if imported_name := resolve_name(graph, imports_n_ids, lib_name):
            danger_callings.add(imported_name)

    return danger_callings
