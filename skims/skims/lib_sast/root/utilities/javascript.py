from lib_sast.root.utilities.common import (
    get_import_nodes,
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
)


def _requires_module(graph: Graph, module_name: str, *, strict: bool = True) -> str | None:
    for _id in match_ast_group_d(graph, "1", "VariableDeclaration"):
        if (
            (val_id := graph.nodes[_id].get("value_id"))
            and (graph.nodes[val_id].get("expression") == "require")
            and (library_n_id := get_n_arg(graph, val_id, 0))
            and (lib_name := graph.nodes[library_n_id].get("value"))
        ):
            if strict and module_name == lib_name:
                return graph.nodes[_id].get("variable")
            if not strict and module_name in lib_name:
                return graph.nodes[_id].get("variable")

    return None


def file_imports_module(graph: Graph, module_name: str, *, strict: bool = True) -> bool:
    for n_id in get_import_nodes(graph):
        if (m_name := graph.nodes[n_id].get("expression")) and m_name.split(".")[0] == module_name:
            return True
    return bool(_requires_module(graph, module_name, strict=strict))


def get_default_alias(graph: Graph, module_name: str) -> str | None:
    for n_id in get_import_nodes(graph):
        if (
            (m_name := graph.nodes[n_id].get("expression"))
            and m_name.split(".")[0] == module_name
            and (alias := graph.nodes[n_id].get("label_alias"))
        ):
            return alias
    if alias := _requires_module(graph, module_name):
        return alias

    return None


def split_function_name(f_names: str) -> tuple[str, str]:
    name_l = f_names.lower().split(".")
    if len(name_l) < 2:
        return "", name_l[-1]
    return name_l[-2], name_l[-1]


def get_key_and_value_id(graph: Graph, nid: NId) -> tuple[str, str]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id].get("symbol", "")
    value_id = graph.nodes[nid]["value_id"]
    return key, value_id


def get_optional_attribute(
    graph: Graph,
    object_id: NId,
    expected_attr: str,
) -> tuple[str, NId] | None:
    for attr_id in adj_ast(graph, object_id, label_type="Pair"):
        key, value_id = get_key_and_value_id(graph, attr_id)
        if key == expected_attr:
            return key, value_id
    return None
