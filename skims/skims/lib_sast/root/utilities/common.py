from collections.abc import (
    Callable,
)

from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.types import (
    Path,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)
from utils.function import (
    shield_blocking,
)


def get_path_from_attrs(*args: tuple[GraphShard, MethodSupplies]) -> str:
    return args[0][0].path


SHIELD_BLOCKING: Callable[[Callable], Callable] = shield_blocking(
    on_error_return=MethodExecutionResult(vulnerabilities=()),
    get_path_from_attrs=get_path_from_attrs,
)


def build_attr_paths(*attrs: str) -> set[str]:
    return {".".join(attrs[index:]) for index, _ in enumerate(attrs)}


def complete_attrs_on_set(data: set[str]) -> set[str]:
    return {attr for path in data for attr in build_attr_paths(*path.split("."))}


def is_parameter(graph: Graph, n_id: NId) -> bool:
    return bool(
        (p_id := next(iter(pred_ast(graph, n_id)), None))
        and (graph.nodes[p_id].get("label_type") == "ArgumentList"),
    )


def literal_binary_operation(graph: Graph, n_id: NId, level: int) -> str | None:
    nodes = graph.nodes

    if left_n_id := nodes[n_id].get("left_id"):
        if nodes[left_n_id].get("label_type") == "Literal":
            return nodes[left_n_id].get("value")
        if nodes[left_n_id].get("label_type") == "BinaryOperation":
            return literal_binary_operation(graph, left_n_id, level + 1)
    if (
        (right_n_id := nodes[n_id].get("right_id"))
        and level == 0
        and nodes[right_n_id].get("label_type") == "Literal"
    ):
        return nodes[right_n_id].get("value")

    return None


def check_definitions(graph: Graph, susp_defs: set[NId]) -> str | None:
    nodes = graph.nodes
    for susp_def in susp_defs:
        symbol = nodes[susp_def].get("symbol")
        for path in get_backward_paths(graph, susp_def):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := nodes[def_id].get("value_id"))
                and (nodes[val_id].get("value_type") in {"string", "number"})
            ):
                return nodes[val_id].get("value")
    return None


def comes_from_string(graph: Graph, n_id: NId) -> str | None:
    if literal_value := literal_binary_operation(graph, n_id, 0):
        return literal_value

    susp_defs: set[NId] = set()

    if graph.nodes[n_id].get("label_type") == "SymbolLookup":
        susp_defs.add(n_id)

    else:
        raw_symbols = match_ast_group_d(graph, n_id, "SymbolLookup", -1)

        clean_symbols = filter(lambda node: not is_parameter(graph, node), raw_symbols)

        susp_defs.update(clean_symbols)

    return check_definitions(graph, susp_defs)


def has_string_definition(graph: Graph, n_id: NId | None) -> str | None:
    if not n_id:
        return None
    nodes = graph.nodes
    if nodes[n_id].get("label_type") == "Literal" and len(adj_ast(graph, n_id)) == 0:
        return nodes[n_id].get("value")

    if (obj_id := graph.nodes[n_id].get("object_id")) and (
        graph.nodes[obj_id].get("label_type") == "Literal"
    ):
        return nodes[obj_id].get("value")

    return comes_from_string(graph, n_id)


def get_n_arg(graph: Graph, n_id: NId | None, arg_idx: int) -> NId | None:
    if not n_id:
        return None

    nodes = graph.nodes
    n_attrs = nodes[n_id]

    if (
        (args_parent_n_id := n_attrs.get("arguments_id"))
        and (
            args_n_ids := [
                arg_id
                for arg_id in adj_ast(graph, args_parent_n_id)
                if nodes[arg_id].get("label_type") != "Comment"
            ]
        )
        and (len(args_n_ids) >= arg_idx + 1)
        and (n_arg := args_n_ids[arg_idx])
    ):
        return n_arg
    return None


def get_import_nodes(graph: Graph) -> set[NId]:
    return {
        _id
        for _id in adj_ast(graph, "1", depth=2)
        if graph.nodes[_id].get("label_type") in {"Import", "ModuleImport"}
    }


def is_any_library_imported(graph: Graph, lib_names: set[str]) -> bool:
    import_nodes = get_import_nodes(graph)
    return any(graph.nodes[n_id].get("expression") in lib_names for n_id in import_nodes)


def get_method_parameters(graph: Graph, node_id: NId) -> list[str]:
    parameters_names = []
    if graph.nodes[node_id]["label_type"] == "MethodDeclaration":
        parameters_id = graph.nodes[node_id].get("parameters_id")
        if parameters_id:
            for parameter in adj_ast(graph, parameters_id):
                var_name = graph.nodes[parameter].get(
                    "variable",
                    graph.nodes[parameter].get("symbol", ""),
                )
                parameters_names.append(var_name)
    if graph.nodes[node_id]["label_type"] == "ForEachStatement":
        var_name_id = graph.nodes[node_id].get("variable_id")
        if var_name_id:
            parameters_names.append(
                graph.nodes[var_name_id].get(
                    "variable",
                    graph.nodes[var_name_id].get("symbol", ""),
                ),
            )
    return parameters_names


def get_var(graph: Graph, n_id: NId) -> str:
    node_var = ""
    if (
        graph.nodes[n_id]["label_type"] == "Literal"
        and (c_ids := adj_ast(graph, n_id, label_type="MemberAccess"))
        and len(c_ids) > 0
    ):
        node_var = graph.nodes[c_ids[0]].get("expression", "").split(".")[0]
    else:
        node_var = graph.nodes[n_id].get("expression", "")
    return node_var


def get_func_parameters_names(graph: Graph, n_id: NId) -> list[str]:
    variables_name_list = []
    node_var = get_var(graph, n_id)
    for path in get_backward_paths(graph, n_id):
        for node_id in path:
            parameters_names = get_method_parameters(graph, node_id)
            variables_name_list.extend(
                [var_name for var_name in parameters_names if var_name in node_var],
            )
            if len(parameters_names) > 0:
                return variables_name_list
    return variables_name_list


def is_in_conditional(graph: Graph, n_id: NId, mode: str | None = None) -> bool:
    conditional_labels = {"If", "SwitchStatement"}
    if mode and mode == "AST":
        for parent in pred_ast(graph, n_id, 10):
            if graph.nodes[parent].get("label_type") in conditional_labels:
                return True
        return False
    for path in get_backward_paths(graph, n_id):
        for path_n_id in path:
            if graph.nodes[path_n_id].get("label_type") in conditional_labels:
                return True
    return False


def is_node_definition_unsafe(
    graph: Graph,
    n_id: NId,
    node_dangerous_condition: Callable[[Graph, NId], bool],
) -> bool:
    if node_dangerous_condition(graph, n_id):
        return True

    if graph.nodes[n_id]["label_type"] != "SymbolLookup":
        return False

    symbol = graph.nodes[n_id]["symbol"]
    for path in get_backward_paths(graph, n_id):
        if (
            (def_id := definition_search(graph, path, symbol))
            and (val_id := graph.nodes[def_id].get("value_id"))
            and node_dangerous_condition(graph, val_id)
        ):
            return True
    return False


def get_pair_nodes(
    graph: Graph,
    child_ids: tuple[NId, ...],
) -> list[NId]:
    return [
        pair_ids[0]
        for n_id in child_ids
        if (
            graph.nodes[n_id]["label_type"] == "Object"
            and (pair_ids := adj_ast(graph, n_id))
            and len(pair_ids) > 0
        )
    ]


METHODS_NO_VAR_FOUND = set()


def _concat_expression_in_member_access(graph: Graph, n_id: NId) -> str | None:
    if graph.nodes[n_id]["label_type"] != "MemberAccess":
        return None

    expression = graph.nodes[n_id].get("expression", "")
    member = graph.nodes[n_id].get("member", "")
    return f"{expression}.{member}"


def _is_var_inside_literal(graph: Graph, child: NId, var_name: str) -> bool:
    if graph.nodes[child]["label_type"] == "Literal":
        for literal_child in adj_ast(graph, child, label_type="SymbolLookup"):
            if graph.nodes[literal_child].get("symbol", "") == var_name:
                return True
    return False


def _is_var_name_in_member_access(graph: Graph, child: NId, var_name: str) -> bool:
    curr_var_name = _concat_expression_in_member_access(graph, child)
    return curr_var_name == var_name


def _is_var_in_children_with_literal(
    graph: Graph,
    n_id: NId,
    var_name: str,
    depth: int = 1,
) -> bool:
    if "." in var_name:
        for child in adj_ast(graph, n_id, depth, label_type="MemberAccess"):
            if _is_var_name_in_member_access(graph, child, var_name):
                return True
    else:
        for child in adj_ast(graph, n_id, depth):
            if _is_var_inside_literal(graph, child, var_name):
                return True

            if graph.nodes[child].get("symbol", "") == var_name:
                return True

    return False


def _is_var_in_children(
    graph: Graph,
    n_id: NId,
    var_name: str,
    depth: int = 1,
    danger: str = "",
) -> bool:
    if danger:
        return _is_var_in_children_with_literal(graph, n_id, var_name, depth)

    if "." in var_name:
        for child in adj_ast(graph, n_id, depth, label_type="MemberAccess"):
            if (
                curr_var_name := _concat_expression_in_member_access(graph, child)
            ) and curr_var_name == var_name:
                return True

    else:
        for child in adj_ast(graph, n_id, depth, label_type="SymbolLookup"):
            if graph.nodes[child].get("symbol", "") == var_name:
                return True

    return False


def _is_var_in_binary_operation(graph: Graph, n_id: NId, var_name: str) -> bool:
    and_or_operators = {"and", "&&", "or", "||"}
    return (
        graph.nodes[n_id]["label_type"] == "BinaryOperation"
        and (graph.nodes[n_id].get("operator", "") not in and_or_operators)
        and _is_var_in_children(graph, n_id, var_name)
    )


def _is_var_in_method_or_binary_operation_or_var_definition(
    graph: Graph,
    condition_id: NId,
    var_name: str,
    path: Path,
) -> bool:
    n_id_list = (condition_id, *adj_ast(graph, condition_id, -1))
    for child in n_id_list:
        if (
            _is_var_in_method(graph, child, var_name, path)
            or _is_var_in_binary_operation(graph, child, var_name)
            or _is_var_in_variable_definition(graph, child, var_name, path)
        ):
            return True

    return False


def _is_var_in_expression_prefix(graph: Graph, n_id: NId, var_name: str) -> bool:
    if graph.nodes[n_id].get("expression", "").startswith(f"{var_name}.") or (  # noqa: SIM103
        (obj_id := graph.nodes[n_id].get("object_id"))
        and graph.nodes[obj_id].get("symbol", "") == var_name
    ):
        return True

    return False


def _is_var_in_method_args(graph: Graph, n_id: NId, var_name: str, danger: str = "") -> bool:
    return (args_id := graph.nodes[n_id].get("arguments_id")) and _is_var_in_children(
        graph,
        args_id,
        var_name,
        danger=danger,
    )


def _aux_is_var_in_method_logic(graph: Graph, def_id: NId, var_name: str) -> bool:
    method_declaration_id = def_id
    if graph.nodes[def_id]["label_type"] == "VariableDeclaration":
        method_declaration_id = graph.nodes[def_id].get("value_id")

    if graph.nodes[method_declaration_id]["label_type"] == "MethodDeclaration" and (
        block_id := graph.nodes[method_declaration_id].get("block_id")
    ):
        return _is_var_in_children(graph, block_id, var_name, -1)

    return False


def _is_var_in_method_logic(graph: Graph, n_id: NId, var_name: str, path: Path) -> bool:
    if not (method_name := graph.nodes[n_id].get("expression", "").split(".")[0]):
        return False

    if method_name in METHODS_NO_VAR_FOUND:
        return False

    if (def_id := definition_search(graph, path, method_name)) and _aux_is_var_in_method_logic(
        graph,
        def_id,
        var_name,
    ):
        return True

    METHODS_NO_VAR_FOUND.add(method_name)
    return False


def get_first_var_declaration(graph: Graph, n_id: NId) -> NId | None:
    for c_id in pred_ast(graph, n_id, 3):
        if graph.nodes[c_id]["label_type"] == "VariableDeclaration":
            return c_id
    return None


def _get_relevant_nodes(graph: Graph, child_id: NId, n_id: NId) -> tuple[NId, ...]:
    return tuple(
        curr_node
        for curr_node in (child_id, *adj_ast(graph, child_id, 1))
        if int(curr_node) < int(n_id) and int(curr_node) > int(child_id)
    )


def _is_var_in_method_without_path(graph: Graph, n_id: NId, var_name: str) -> bool:
    if graph.nodes[n_id]["label_type"] != "MethodInvocation":
        return False

    return _is_var_in_expression_prefix(graph, n_id, var_name) or _is_var_in_method_args(
        graph,
        n_id,
        var_name,
        "danger",
    )


def _is_same_as_parent_declaration(
    c_id: NId,
    var_name: str,
    danger_source_var_name: str,
    danger_source: NId,
) -> bool:
    return c_id == danger_source and var_name.split(".")[0].lstrip("$") == danger_source_var_name


def _is_method_invocation(graph: Graph, c_id: NId, parent_ds: NId | None) -> NId | None:
    if c_id != parent_ds:
        v_id = graph.nodes[c_id].get("value_id")
        return v_id if v_id and graph.nodes[v_id]["label_type"] == "MethodInvocation" else None

    return None


def _get_new_var_name(graph: Graph, c_id: NId, v_id: NId, danger_source_var_name: str) -> str:
    return (
        graph.nodes[c_id].get("variable")
        if _is_var_in_method_args(graph, v_id, danger_source_var_name, "danger")
        else danger_source_var_name
    )


def _is_danger_source_sanitized(
    graph: Graph,
    child_id: NId,
    n_id: NId,
    danger_source: NId,
    var_name: str,
) -> bool:
    valid_methods = {"file_get_contents"}
    if graph.nodes[child_id]["label_type"] not in {"File", "ExecutionBlock"}:
        return False

    n_id_list = _get_relevant_nodes(graph, child_id, n_id)
    parent_ds = get_first_var_declaration(graph, danger_source)

    if not parent_ds or graph.nodes[parent_ds]["label_type"] != "VariableDeclaration":
        return False

    danger_source_var_name = graph.nodes[parent_ds]["variable"]
    for c_id in n_id_list:
        if _is_same_as_parent_declaration(c_id, var_name, danger_source_var_name, danger_source):
            return False
        if v_id := _is_method_invocation(graph, c_id, parent_ds):
            if graph.nodes[c_id].get("variable") == var_name.split(".")[0].lstrip("$"):
                if graph.nodes[v_id]["expression"] in valid_methods:
                    continue
                return _is_var_in_method_without_path(graph, v_id, danger_source_var_name)
            danger_source_var_name = _get_new_var_name(graph, c_id, v_id, danger_source_var_name)
    return False


def _is_var_in_method(graph: Graph, n_id: NId, var_name: str, path: Path) -> bool:
    if graph.nodes[n_id]["label_type"] != "MethodInvocation":
        return False

    return (
        _is_var_in_expression_prefix(graph, n_id, var_name)
        or _is_var_in_method_args(graph, n_id, var_name)
        or _is_var_in_method_logic(graph, n_id, var_name, path)
    )


def _is_var_in_condition(graph: Graph, n_id: NId, var_name: str, path: Path) -> bool:
    if graph.nodes[n_id]["label_type"] != "If":
        return False

    return (
        condition_id := graph.nodes[n_id].get("condition_id")
    ) and _is_var_in_method_or_binary_operation_or_var_definition(
        graph,
        condition_id,
        var_name,
        path,
    )


def _is_var_in_variable_definition(graph: Graph, n_id: NId, var_name: str, path: Path) -> bool:
    if graph.nodes[n_id]["label_type"] != "SymbolLookup":
        return False

    symbol = graph.nodes[n_id].get("symbol", "")
    if not (def_id := definition_search(graph, path, symbol)):
        return False

    return (
        graph.nodes[def_id].get("variable", "") != var_name
        and (val_id := graph.nodes[def_id].get("value_id"))
        and _is_var_in_method(graph, val_id, var_name, path)
    )


def _get_var_name(graph: Graph, n_id: NId) -> str | None:
    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        return graph.nodes[n_id].get("symbol")

    if graph.nodes[n_id]["label_type"] == "MemberAccess":
        return _concat_expression_in_member_access(graph, n_id)

    if (
        graph.nodes[n_id]["label_type"] == "MethodInvocation"
        and (args_id := graph.nodes[n_id].get("arguments_id"))
        and (members := adj_ast(graph, args_id, label_type="MemberAccess"))
    ):
        return _concat_expression_in_member_access(graph, members[0])

    if (
        graph.nodes[n_id]["label_type"] == "MethodInvocation"
        and (first_arg := get_n_arg(graph, n_id, 0))
        and (third_arg := get_n_arg(graph, n_id, 2))
        and graph.nodes[first_arg]["label_type"] == "SymbolLookup"
    ):
        symbol = graph.nodes[first_arg].get("symbol")
        return symbol if symbol != "LIBXML_NOENT" else graph.nodes[third_arg].get("symbol")

    return None


def _get_path_after_current_method(graph: Graph, path: Path) -> Path:
    if path and graph.nodes[path[0]]["label_type"] == "MethodInvocation":
        return path[1:]

    return path


def is_sanitized(graph: Graph, n_id: NId, danger_source: NId | None = None) -> bool:
    if not (var_name := _get_var_name(graph, n_id)):
        return False

    for path in get_backward_paths(graph, n_id):
        for child in _get_path_after_current_method(graph, path):
            if (
                _is_var_in_method(graph, child, var_name, path)
                or _is_var_in_condition(
                    graph,
                    child,
                    var_name,
                    path,
                )
                or (
                    danger_source
                    and _is_danger_source_sanitized(graph, child, n_id, danger_source, var_name)
                )
            ):
                return True

    return False


def get_parent_scope_n_id(graph: Graph, n_id: NId) -> NId | None:
    p_n_id: NId | None = None
    for parent_n_id in pred_ast(graph, n_id, -1):
        if graph.nodes[parent_n_id].get("label_type") in {
            "File",
            "MethodDeclaration",
            "ClassDeclaration",
        }:
            p_n_id = parent_n_id
            break
    return p_n_id
