from lib_sast.root.utilities.common import (
    get_import_nodes,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    match_ast,
    pred_ast,
)


def concatenate_name(graph: Graph, n_id: NId, name: str | None = None) -> str:
    prev_str = "." + name if name else ""

    node_type = graph.nodes[n_id]["label_type"]
    if node_type == "MethodInvocation":
        expr = graph.nodes[n_id]["expression"]
        if graph.nodes[n_id].get("object_id") and (next_node := match_ast(graph, n_id)["__0__"]):
            expr = concatenate_name(graph, next_node, expr)
    elif node_type == "SymbolLookup":
        expr = graph.nodes[n_id]["symbol"]
    else:
        expr = ""
    return expr + prev_str


def is_any_library_imported_prefix(graph: Graph, lib_names: tuple) -> bool:
    for n_id in get_import_nodes(graph):
        if graph.nodes[n_id].get("expression", "").startswith(lib_names):
            return True

    return False


def get_variable_name(graph: Graph, n_id: NId) -> str | None:
    nodes = graph.nodes
    var_name: str | None = None

    if (pred_n_ids := pred_ast(graph, n_id)) and (p_n_id := pred_n_ids[0]):
        if (label_type := nodes[p_n_id].get("label_type")) == "Assignment" and (
            var_n_id := nodes[p_n_id].get("variable_id")
        ):
            var_name = nodes[var_n_id].get("symbol")
        elif label_type == "VariableDeclaration":
            var_name = nodes[p_n_id].get("variable")

    return var_name
