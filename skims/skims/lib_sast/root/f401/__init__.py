from lib_sast.root.f401.terraform import (
    tfm_azure_kv_secret_no_expiration_date as _tfm_kv_secret_expiration_date,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def tfm_azure_kv_secret_no_expiration_date(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_kv_secret_expiration_date(shard, method_supplies)
