from lib_sast.root.f005.cloudformation.cfn_allow_priv_escalation_attach_policy import (
    cfn_allow_priv_escalation_attach_policy as c_escalation_attach_policy,
)
from lib_sast.root.f005.cloudformation.cfn_allows_priv_escalation_by_policies_versions import (
    cfn_allows_priv_escalation_by_policies_versions as c_escalation_policies,
)
from lib_sast.root.f005.terraform.tfm_allow_priv_escalation_attach_policy import (
    tfm_allow_priv_escalation_attach_policy as t_escalation_attach_policy,
)
from lib_sast.root.f005.terraform.tfm_allow_priv_escalation_attach_policy import (
    tfm_escalation_attach_interpolated as t_escalation_attach_interpolated,
)
from lib_sast.root.f005.terraform.tfm_allows_priv_escalation_by_policies_versions import (
    tfm_allows_priv_escalation_by_policies_versions as t_escalation_policies,
)
from lib_sast.root.f005.terraform.tfm_allows_priv_escalation_by_policies_versions import (
    tfm_escalation_versions_interpolated as t_escalation_policies_interpolated,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_allow_priv_escalation_attach_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return c_escalation_attach_policy(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_allows_priv_escalation_by_policies_versions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return c_escalation_policies(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_allow_priv_escalation_attach_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return t_escalation_attach_policy(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_escalation_attach_interpolated(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return t_escalation_attach_interpolated(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_allows_priv_escalation_by_policies_versions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return t_escalation_policies(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_escalation_versions_interpolated(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return t_escalation_policies_interpolated(shard, method_supplies)
