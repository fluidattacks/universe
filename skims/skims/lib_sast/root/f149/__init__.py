from lib_sast.root.f149.java import (
    java_insecure_smtp_connection as _java_insecure_smtp_connection,
)
from lib_sast.root.f149.java import (
    java_insecure_smtp_ssl as _java_insecure_smtp_ssl,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_insecure_smtp_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_smtp_connection(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_smtp_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_smtp_ssl(shard, method_supplies)
