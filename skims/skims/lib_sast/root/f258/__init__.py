from lib_sast.root.f258.cloudformation import (
    cfn_elb2_has_not_deletion_protection as _cfn_elb2_not_deletion_protection,
)
from lib_sast.root.f258.terraform import (
    tfm_elb2_has_not_deletion_protection as _tfm_elb2_not_deletion_protection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_elb2_has_not_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_elb2_not_deletion_protection(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_elb2_has_not_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_elb2_not_deletion_protection(shard, method_supplies)
