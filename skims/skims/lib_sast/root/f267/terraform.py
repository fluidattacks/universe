from collections.abc import (
    Callable,
    Iterator,
)
from itertools import (
    chain,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    K8S_CONTAINER_RESOURCE_DEPTHS,
    get_argument_iterator,
    get_argument_iterator_d,
    get_optional_attribute,
    get_optional_list,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _k8s_has_root_container(graph: Graph, nid: NId) -> Iterator[NId]:
    pod_run_as_non_root = _get_pod_run_as_non_root(graph, nid)
    spec_child_nids = adj_ast(graph, nid, label_type="Object")

    for spec_child_nid in spec_child_nids:
        if graph.nodes[spec_child_nid].get("name") == "container":
            yield from _check_container_nodes(graph, spec_child_nid, pod_run_as_non_root)


def _container_without_securitycontext(graph: Graph, nid: NId) -> Iterator[NId]:
    pod_has_safe_config = _get_pod_has_safe_config(graph, nid)

    def _has_no_security_context(cont_nid: NId) -> bool:
        sec_nids = adj_ast(graph, cont_nid, label_type="Object")
        names = [graph.nodes[cid].get("name") for cid in sec_nids]
        return "security_context" not in names and not pod_has_safe_config

    return filter(
        _has_no_security_context,
        get_argument_iterator(graph, nid, "container"),
    )


def _get_pod_run_as_non_root(graph: Graph, nid: NId) -> str:
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        pod_non_root = get_optional_attribute(graph, seccomp_nid, "run_as_non_root")
        if pod_non_root is not None:
            return pod_non_root[1]
    return "false"


def _get_pod_has_safe_config(graph: Graph, nid: NId) -> bool:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")

    names = [graph.nodes[cid].get("name") for cid in cont_childs_nids]
    has_container = any(name == "container" for name in names)
    has_security_context = any(name == "security_context" for name in names)

    return not has_container or has_security_context


def _check_container_nodes(graph: Graph, nid: NId, pod_run_as_non_root: str) -> Iterator[NId]:
    has_security_context = False

    for seccomp_nid in _get_security_context_nodes(graph, nid):
        has_security_context = True
        if _is_root_container(graph, seccomp_nid, pod_run_as_non_root):
            yield _get_run_as_non_root_value(graph, seccomp_nid)

    if not has_security_context and pod_run_as_non_root == "false":
        yield nid


def _is_root_container(graph: Graph, nid: NId, pod_run_as_non_root: str) -> bool:
    cont_non_root = get_optional_attribute(graph, nid, "run_as_non_root")
    if cont_non_root is None and pod_run_as_non_root == "false":
        return True
    if cont_non_root and cont_non_root[1] == "false":  # noqa: SIM103
        return True
    return False


def _get_run_as_non_root_value(graph: Graph, nid: NId) -> NId:
    run_as_non_root = get_optional_attribute(graph, nid, "run_as_non_root")
    if run_as_non_root is not None:
        return run_as_non_root[2]
    return nid


def _get_security_context_nodes(graph: Graph, nid: NId) -> Iterator[NId]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    for cont_childs_nid in cont_childs_nids:
        if graph.nodes[cont_childs_nid].get("name") == "security_context":
            yield cont_childs_nid


def _get_capability_lists(
    graph: Graph,
    nid: NId,
) -> tuple[tuple[str, list, NId] | None, tuple[str, list, NId] | None]:
    add = get_optional_list(graph, nid, "add")
    drop = get_optional_list(graph, nid, "drop")
    return add, drop


def _yield_containers_from(
    graph: Graph,
    nid: NId,
    check_attribute: Callable[[Graph, NId], Iterator[NId]],
) -> Iterator[NId]:
    spec_child_nids = adj_ast(graph, nid, label_type="Object")

    for spec_child_nid in spec_child_nids:
        if graph.nodes[spec_child_nid].get("name") == "container":
            yield from check_attribute(graph, spec_child_nid)


def _check_privilege_escalation(graph: Graph, nid: NId) -> Iterator[NId]:
    has_security_context = False
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        has_security_context = True
        allow_privilege_attr = get_optional_attribute(
            graph,
            seccomp_nid,
            "allow_privilege_escalation",
        )
        if allow_privilege_attr and allow_privilege_attr[1] == "true":
            yield allow_privilege_attr[2]
        if not allow_privilege_attr:
            yield seccomp_nid
    if not has_security_context:
        yield nid


def _check_root_filesystem(graph: Graph, nid: NId) -> Iterator[NId]:
    has_security_context = False
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        has_security_context = True
        allow_privilege_attr = get_optional_attribute(
            graph,
            seccomp_nid,
            "read_only_root_filesystem",
        )
        if allow_privilege_attr and allow_privilege_attr[1] == "false":
            yield allow_privilege_attr[2]
        if not allow_privilege_attr:
            yield seccomp_nid
    if not has_security_context:
        yield nid


def _check_if_sys_admin_exists(graph: Graph, nid: NId) -> Iterator[NId]:
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        for capability_nid in get_argument_iterator(graph, seccomp_nid, "capabilities"):
            add, _ = _get_capability_lists(graph, capability_nid)
            if add and any(cap.lower() == "sys_admin" for cap in add[1]):
                yield add[2]


def _check_drop_capability(graph: Graph, nid: NId) -> Iterator[NId]:
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        for capability_nid in get_argument_iterator(graph, seccomp_nid, "capabilities"):
            _, drop = _get_capability_lists(graph, capability_nid)
            if drop and all(cap.lower() != "all" for cap in drop[1]):
                yield drop[2]


def _check_if_capability_exists(graph: Graph, nid: NId) -> Iterator[NId]:
    has_security_context = False
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        has_security_context = True
        for capability_nid in get_argument_iterator(graph, seccomp_nid, "capabilities"):
            add, drop = _get_capability_lists(graph, capability_nid)
            if not add and not drop:
                yield capability_nid
    if not has_security_context:
        yield nid


def _check_run_as_user(graph: Graph, nid: NId) -> Iterator[NId]:
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        run_as_user = get_optional_attribute(graph, seccomp_nid, "run_as_user")
        if run_as_user and run_as_user[1].isdigit() and int(run_as_user[1]) < 10000:
            yield run_as_user[2]


def _check_privileged_container(graph: Graph, nid: NId) -> Iterator[NId]:
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        privilege_attr = get_optional_attribute(graph, seccomp_nid, "privileged")
        if privilege_attr and privilege_attr[1] == "true":
            yield privilege_attr[2]


def _check_get_host_process(graph: Graph, nid: NId) -> Iterator[NId]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    names = [graph.nodes[cid].get("name") for cid in cont_childs_nids]
    has_container = any(name == "container" for name in names)
    for cont_childs_nid in cont_childs_nids:
        for window_cont in get_argument_iterator_d(
            graph,
            cont_childs_nid,
            "windows_options",
            depth=2,
        ):
            host_process_attr = get_optional_attribute(graph, window_cont, "host_process")
            if has_container and host_process_attr and host_process_attr[1] == "true":
                yield host_process_attr[2]


def _check_get_host_path_volumes(graph: Graph, nid: NId) -> Iterator[NId]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    names = [graph.nodes[cid].get("name") for cid in cont_childs_nids]
    has_container = any(name == "container" for name in names)
    volume_ids = [cid for cid in cont_childs_nids if graph.nodes[cid].get("name") == "volume"]
    vol_childs_nids = tuple(
        chain.from_iterable(adj_ast(graph, vid, label_type="Object") for vid in volume_ids),
    )
    host_path_ids = [vci for vci in vol_childs_nids if graph.nodes[vci].get("name") == "host_path"]
    if has_container:
        yield from host_path_ids


def _k8s_has_seccomp_profile(graph: Graph, nid: NId) -> Iterator[NId]:
    seccomp_profile = _get_pod_seccomp_profile(graph, nid)
    spec_child_nids = adj_ast(graph, nid, label_type="Object")

    for spec_child_nid in spec_child_nids:
        if graph.nodes[spec_child_nid].get("name") == "container":
            yield from _check_seccomp_profile_nodes(graph, spec_child_nid, seccomp_profile)


def _get_pod_seccomp_profile(graph: Graph, nid: NId) -> str:
    for seccomp_nid in _get_security_context_nodes(graph, nid):
        for profile_nid in get_argument_iterator(graph, seccomp_nid, "seccomp_profile"):
            type_attr = get_optional_attribute(graph, profile_nid, "type")
            return type_attr[1] if type_attr else "false"
    return "false"


def _check_seccomp_profile_nodes(graph: Graph, nid: NId, seccomp_profile: str) -> Iterator[NId]:
    has_security_context = False
    report = nid

    for seccomp_nid in _get_security_context_nodes(graph, nid):
        report = seccomp_nid
        for profile_nid in get_argument_iterator(graph, seccomp_nid, "seccomp_profile"):
            has_security_context = True
            if _is_seccomp_unconfined(graph, profile_nid, seccomp_profile):
                yield _get_seccomp_unconfined(graph, profile_nid)

    if not has_security_context and seccomp_profile == "Unconfined":
        yield report


def _is_seccomp_unconfined(graph: Graph, nid: NId, seccomp_profile: str) -> bool:
    cont_non_root = get_optional_attribute(graph, nid, "type")
    if cont_non_root is None and seccomp_profile in ("false", "Unconfined"):
        return True
    if cont_non_root and cont_non_root[1] == "Unconfined":  # noqa: SIM103
        return True
    return False


def _get_seccomp_unconfined(graph: Graph, nid: NId) -> NId:
    seccomp_unconfined = get_optional_attribute(graph, nid, "type")
    if seccomp_unconfined is not None:
        return seccomp_unconfined[2]
    return nid


def _check_spec_attr_enabled(attr: str, graph: Graph, nid: NId) -> Iterator[NId]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    names = [graph.nodes[cid].get("name") for cid in cont_childs_nids]
    has_container = any(name == "container" for name in names)
    attr_value = get_optional_attribute(graph, nid, attr)
    if has_container and (attr_value is None or attr_value[1] == "true"):
        yield attr_value[2] if attr_value else nid


def _get_spec_nids(graph: Graph, nid: NId) -> Iterator[NId]:
    resource_name = graph.nodes[nid].get("name")
    return get_argument_iterator_d(
        graph,
        nid,
        "spec",
        depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
    )


def tfm_k8s_root_container(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_ROOT_CONTAINER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            resource_name = graph.nodes[nid].get("name")
            if resource_name in K8S_CONTAINER_RESOURCE_DEPTHS:
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _k8s_has_root_container(graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_allow_privilege_escalation_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_ALLOW_PRIVILEGE_ESCALATION_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _yield_containers_from(graph, spec_nid, _check_privilege_escalation)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_root_filesystem_read_only(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_ROOT_FILESYSTEM_READ_ONLY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _yield_containers_from(graph, spec_nid, _check_root_filesystem)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_check_seccomp_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_CHECK_SECCOMP_PROFILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _k8s_has_seccomp_profile(graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_check_drop_capability(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_CHECK_DROP_CAPABILITY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _yield_containers_from(graph, spec_nid, _check_drop_capability)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_check_if_capability_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_CHECK_IF_CAPABILITY_EXISTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _yield_containers_from(graph, spec_nid, _check_if_capability_exists)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_check_run_as_user(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_CHECK_RUN_AS_USER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _yield_containers_from(graph, spec_nid, _check_run_as_user)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_check_privileged_used(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_CHECK_PRIVILEGED_USED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _yield_containers_from(graph, spec_nid, _check_privileged_container)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_check_if_sys_admin_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_CHECK_IF_SYS_ADMIN_EXISTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _yield_containers_from(graph, spec_nid, _check_if_sys_admin_exists)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_container_without_securitycontext(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_CONTAINER_WITHOUT_SECURITYCONTEXT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _container_without_securitycontext(graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_host_process_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_HOST_PROCESS_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _check_get_host_process(graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_host_path_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_HOST_PATH_VOLUMES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _check_get_host_path_volumes(graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_k8s_sa_token_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_SA_TOKEN_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        selected_nids = filter(
            lambda nid: graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS,
            method_supplies.selected_nodes,
        )
        spec_nids = chain.from_iterable(_get_spec_nids(graph, nid) for nid in selected_nids)
        ipc_spec_nids = chain.from_iterable(
            _check_spec_attr_enabled("automount_service_account_token", graph, spec_nid)
            for spec_nid in spec_nids
        )

        yield from ipc_spec_nids

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
