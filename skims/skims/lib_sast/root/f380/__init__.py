from lib_sast.root.f380.docker_compose import (
    docker_compose_image_has_digest as _docker_compose_image_has_digest,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def docker_compose_image_has_digest(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _docker_compose_image_has_digest(shard, method_supplies)
