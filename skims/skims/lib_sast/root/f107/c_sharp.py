from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from model.core import (
    MethodExecutionResult,
)


def c_sharp_ldap_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_LDAP_INJECTION
    danger_methods = {"FindOne", "FindAll"}
    danger_params = {"dir_searcher", "user_parameters", "user_connection"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            expr = graph.nodes[node]["expression"].split(".")
            if expr[-1] in danger_methods and get_node_evaluation_results(
                method,
                graph,
                node,
                danger_params,
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
