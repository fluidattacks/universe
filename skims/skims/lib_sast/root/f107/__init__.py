from lib_sast.root.f107.c_sharp import (
    c_sharp_ldap_injection as _c_sharp_ldap_injection,
)
from lib_sast.root.f107.java import (
    java_ldap_injection as _java_ldap_injection,
)
from lib_sast.root.f107.kotlin import (
    kt_anonymous_ldap as _kt_anonymous_ldap,
)
from lib_sast.root.f107.python import (
    python_ldap_injection as _python_ldap_injection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_ldap_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_ldap_injection(shard, method_supplies)


@SHIELD_BLOCKING
def java_ldap_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_ldap_injection(shard, method_supplies)


@SHIELD_BLOCKING
def kt_anonymous_ldap(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _kt_anonymous_ldap(shard, method_supplies)


@SHIELD_BLOCKING
def python_ldap_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_ldap_injection(shard, method_supplies)
