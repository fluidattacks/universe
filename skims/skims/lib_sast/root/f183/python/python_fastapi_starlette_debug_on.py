from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_value_in_variable_true(graph: Graph, n_id: NId) -> bool:
    return (
        (graph.nodes[n_id]["label_type"] == "Literal")
        and (graph.nodes[n_id].get("value_type") == "bool")
        and (graph.nodes[n_id].get("value") == "True")
    )


def _is_debug_enabled(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        (args_id := graph.nodes[n_id].get("arguments_id"))
        and (
            childs := adj_ast(
                graph,
                args_id,
                label_type="NamedArgument",
                argument_name="debug",
            )
        )
        and (val_id := graph.nodes[childs[0]].get("value_id"))
        and is_node_definition_unsafe(graph, val_id, _is_value_in_variable_true)
    ):
        return True

    return False


def _is_fastapi_starlette_debug_mode_on(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "MethodInvocation"
        and (exp_id := graph.nodes[n_id].get("expression_id"))
        and graph.nodes[exp_id].get("label_type") == "SymbolLookup"
        and graph.nodes[exp_id].get("symbol") in {"FastAPI", "Starlette"}
        and _is_debug_enabled(graph, n_id)
    )


def python_fastapi_starlette_debug_on(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_FASTAPI_STARLETTE_DEBUG_ON

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if _is_fastapi_starlette_debug_mode_on(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
