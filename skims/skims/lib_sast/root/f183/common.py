from collections.abc import (
    Callable,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_in_conditional,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def _first_arg_is_errorhandler(graph: Graph, n_id: NId, var_name: str) -> bool:
    first_arg = get_n_arg(graph, n_id, 0)
    return (
        (first_arg is not None)
        and graph.nodes[first_arg].get("label_type") == "MethodInvocation"
        and graph.nodes[first_arg].get("expression") == var_name
    )


def _uses_express_instance(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
    method_evaluators: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]],
) -> bool:
    return (exp_id := graph.nodes[n_id].get("expression_id")) and get_node_evaluation_results(
        method,
        graph,
        exp_id,
        {"express_instance"},
        danger_goal=False,
        method_evaluators=method_evaluators,
    )


def _uses_errorhandler_in_prod(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
    var_name: str,
    method_evaluators: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]],
) -> bool:
    return (
        (exp_id := graph.nodes[n_id].get("expression_id"))
        and graph.nodes[exp_id].get("label_type") == "MemberAccess"
        and graph.nodes[exp_id].get("member") == "use"
        and _first_arg_is_errorhandler(graph, n_id, var_name)
        and _uses_express_instance(graph, exp_id, method, method_evaluators)
        and not (is_in_conditional(graph, n_id))
    )


def debug_mode_enabled(
    graph: Graph,
    method_supplies: MethodSupplies,
    method: MethodsEnum,
    method_evaluators: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]],
) -> list[NId]:
    if var_name := get_default_alias(graph, "errorhandler"):
        return [
            n_id
            for n_id in method_supplies.selected_nodes
            if _uses_errorhandler_in_prod(
                graph,
                n_id,
                method,
                var_name,
                method_evaluators,
            )
        ]

    return []
