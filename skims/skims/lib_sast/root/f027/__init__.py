from lib_sast.root.f027.typescript.ts_unvalidated_xml_parsed_in_vm import (
    ts_unvalidated_xml_parsed_in_vm as _ts_unvalidated_xml_parsed_in_vm,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def ts_unvalidated_xml_parsed_in_vm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_unvalidated_xml_parsed_in_vm(shard, method_supplies)
