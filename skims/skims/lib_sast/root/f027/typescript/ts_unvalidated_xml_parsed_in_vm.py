import re
from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def _method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs.get("expression", "").split(".")[-1] != "toString":
        args.triggers.add("possible_sanitization")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "Request":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "parameter": _parameter_evaluator,
    "method_invocation": _method_invocation_evaluator,
}


def _extract_variable_name(command_to_run: str) -> str | None:
    var_extraction_pattern = r"libxml\.parseXml\(\s*(\w+),\s*.*\)"
    if (match := re.search(var_extraction_pattern, command_to_run)) and (
        var_name := match.group(1)
    ):
        return var_name

    return None


def _is_not_sanitized_external_xml(
    graph: Graph,
    nodes: tuple[NId, ...],
    method: MethodsEnum,
) -> bool:
    return any(
        get_node_evaluation_results(
            method,
            graph,
            n_id,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        )
        for n_id in nodes
    )


def _vm_parses_dangerous_xml(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    def def_nodes_predicate(node: dict[str, str]) -> bool:
        return (
            node.get("label_type") in {"VariableDeclaration", "Parameter"}
            and node.get("variable") == var_name
        )

    nodes = graph.nodes
    command_to_run = nodes[n_id].get("value")
    return bool(
        (var_name := _extract_variable_name(command_to_run))
        and (nodes := g.filter_nodes(graph, graph.nodes, def_nodes_predicate))
        and _is_not_sanitized_external_xml(graph, nodes, method),
    )


def ts_unvalidated_xml_parsed_in_vm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TS_UNVALIDATED_XML_PARSED_IN_VM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if file_imports_module(graph, "vm") and file_imports_module(graph, "libxmljs2"):
            for n_id in method_supplies.selected_nodes:
                n_attrs = graph.nodes[n_id]
                if (
                    n_attrs.get("expression", "").endswith("runInContext")
                    and (f_arg := get_n_arg(graph, n_id, 0))
                    and (graph.nodes[f_arg].get("label_type") == "Literal")
                    and (_vm_parses_dangerous_xml(graph, f_arg, method))
                ):
                    yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
