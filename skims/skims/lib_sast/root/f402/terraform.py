from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _azure_app_service_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    if logs := get_argument(graph, nid, "logs"):
        fail_req = get_optional_attribute(graph, logs, "failed_request_tracing_enabled")
        det_err = get_optional_attribute(graph, logs, "detailed_error_messages_enabled")
        if (not fail_req or fail_req[1].lower() == "false") or (
            not det_err or det_err[1].lower() == "false"
        ):
            return logs
    else:
        return nid
    return None


def _azure_sql_server_audit_log_retention(graph: Graph, nid: NId) -> NId | None:
    if logs := get_argument(graph, nid, "extended_auditing_policy"):
        retention = get_optional_attribute(graph, logs, "retention_in_days")
        if not retention:
            return logs
        if retention[1].isdigit() and int(retention[1]) <= 90:
            return retention[2]
    else:
        return nid
    return None


def _azure_storage_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    if queue := get_argument(graph, nid, "queue_properties"):
        if logging := get_argument(graph, queue, "logging"):
            attrs = [
                get_optional_attribute(graph, logging, req) for req in ["delete", "read", "write"]
            ]
            if not all(req[1].lower() == "true" if req else False for req in attrs):
                return logging
        else:
            return queue
    else:
        return nid
    return None


def tfm_azure_storage_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_STORAGE_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "azurerm_storage_account" and (
                report := _azure_storage_logging_disabled(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_sql_server_audit_log_retention(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_SQL_SERVER_AUDIT_LOG_RETENTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "azurerm_sql_server" and (
                report := _azure_sql_server_audit_log_retention(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_app_service_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_APP_SERVICE_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "azurerm_app_service",
                "azurerm_windows_web_app",
                "azurerm_linux_web_app",
            } and (report := _azure_app_service_logging_disabled(graph, nid)):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
