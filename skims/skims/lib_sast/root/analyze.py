import os
from concurrent.futures import (
    Future,
)
from concurrent.futures.process import (
    ProcessPoolExecutor,
)
from concurrent.futures.thread import (
    ThreadPoolExecutor,
)
from contextlib import (
    redirect_stderr,
)
from pathlib import Path

import ctx
from fluid_sbom.advisories.database import (
    DATABASE,
)
from fluid_sbom.pkg.operations.package_operation import (
    package_operations_factory,
)
from fluid_sbom.sources.directory_source import (
    Directory,
)
from fluid_sbom.utils.exceptions import (
    UnexpectedExceptionError,
    UnexpectedNodeError,
)
from lib_sast.reachability import (
    QUERIES as REACHABILITY_QUERIES,
)
from lib_sast.reachability.analyze import (
    analyze_reachability,
)
from lib_sast.reachability.common import (
    determine_queries_to_execute,
    find_dependency_file,
)
from lib_sast.reachability.types import (
    Query,
)
from lib_sast.reachability.utils import (
    create_dependencies_dict,
    parse_sbom_json_results,
)
from lib_sast.root import (
    QUERIES,
)
from lib_sast.root.common import (
    load_json_safe,
)
from lib_sast.sast_model import (
    SUPPORTED_LANGUAGES,
    GraphDB,
    GraphShardMetadataLanguage,
    MethodSupplies,
    decide_language,
)
from lib_sast.syntax_graph.parse_graph.parse import (
    get_graph_db,
    get_shard,
)
from lib_sast.utils.graph import (
    nodes_by_type,
)
from model.core import (
    MethodExecutionResult,
    SkimsConfig,
)
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
    log_to_remote_handled,
)
from utils.report_soon import (
    send_vulnerability_to_sqs,
)
from utils.state import (
    ExecutionStores,
)

FINDINGS_BY_LANGUAGE = {
    language: {finding for fin_tuples in nodes.values() for finding, _ in fin_tuples}
    for language, nodes in QUERIES.items()
}
ROOT_FINDINGS = {finding for findings in FINDINGS_BY_LANGUAGE.values() for finding in findings}

REACHABILITY_FINDINGS = {
    query.finding for queries in REACHABILITY_QUERIES.values() for query in queries
}

ALL_FINDINGS = ROOT_FINDINGS | REACHABILITY_FINDINGS


def _store_results_callback(
    stores: ExecutionStores,
    future: Future,
    future_path: str,
) -> None:
    try:
        results: list[MethodExecutionResult] = future.result(timeout=120)
        for result in results:
            if result.method_name and result.time_coefficient:
                method_time = (result.method_name, result.time_coefficient)
                stores.methods_stores["methods_results"].store(method_time)

            stores.vuln_stores.store_vulns(result.vulnerabilities)
            send_vulnerability_to_sqs(result.vulnerabilities)
    except TimeoutError:
        future.cancel()
        msg = f"Future timed out in SAST graph analysis on {future_path}"
        log_to_remote_handled(msg=msg, severity="warning")


def _analyze_one_path(  # noqa: PLR0913
    *,
    config: SkimsConfig,
    path: str,
    graph_db: GraphDB | None,
    language: GraphShardMetadataLanguage,
    reachability_queries: dict[str, dict[GraphShardMetadataLanguage, tuple[Query, ...]]],
    package_path: str,
    deps_dict: dict[str, dict],
) -> list[MethodExecutionResult]:
    # Re-export config to gain visibility in child subprocesses
    ctx.SKIMS_CONFIG = config
    if not (graph_db and (shard := graph_db.get_path_shard(path))):
        shard = get_shard(path, language, ctx.SKIMS_CONFIG.working_dir)

    if not shard or not shard.syntax_graph:
        return []

    path_results: list[MethodExecutionResult] = []
    for label, nodes in nodes_by_type(shard.syntax_graph).items():
        method_supplies = MethodSupplies(nodes, graph_db)
        if queries_node := QUERIES[language].get(label):
            for finding, query in queries_node:
                if finding in ctx.SKIMS_CONFIG.checks:
                    path_results.append(query(shard, method_supplies))
    if package_path:
        path_results.extend(
            analyze_reachability(
                config=config,
                lang=language,
                queries_by_dep_file=reachability_queries,
                shard=shard,
                package_path=package_path,
                deps=deps_dict,
            ),
        )
    return path_results


async def initialize_reachability() -> tuple[
    dict[str, dict[GraphShardMetadataLanguage, tuple[Query, ...]]],
    dict[str, dict[str, dict[str, dict[str, str]]]],
]:
    DATABASE.initialize()
    if (sbom_input := ctx.SKIMS_CONFIG.sast.reachability_sbom_input) and (
        sbom_results := load_json_safe(sbom_input)
    ):
        log_blocking("info", "Reading already parsed packages")
        deps_dict, vuln_ranges = parse_sbom_json_results(sbom_results)
    else:
        log_blocking("info", "Generating SBOM for this project")
        if ctx.SKIMS_CONFIG.debug:
            packages, _ = package_operations_factory(Directory(root="./", exclude=()))
        else:
            with redirect_stderr(Path(os.devnull).open("w", encoding="utf-8")):  # noqa: ASYNC230
                packages, _ = package_operations_factory(Directory(root="./", exclude=()))

        deps_dict, vuln_ranges = create_dependencies_dict(packages)
    log_blocking("info", "Determining queries to execute")
    queries_to_execute = determine_queries_to_execute(REACHABILITY_QUERIES, deps_dict, vuln_ranges)
    return queries_to_execute, deps_dict


async def analyze(
    *,
    stores: ExecutionStores,
    paths: tuple[str, ...],
    include_reachability: bool = True,
) -> None:
    if not any(finding in ctx.SKIMS_CONFIG.checks for finding in ALL_FINDINGS):
        # No findings will be executed, early abort
        return

    paths_dict = {
        path: language
        for path in paths
        if (language := decide_language(path))
        and language
        not in {
            GraphShardMetadataLanguage.NOT_SUPPORTED,
            GraphShardMetadataLanguage.SCALA,
        }
    }

    if not paths_dict:
        return

    log_blocking(
        "info",
        "Performing advanced SAST analysis on %s paths",
        len(paths_dict),
    )

    execute_reachability = include_reachability and any(
        lang in SUPPORTED_LANGUAGES for lang in paths_dict.values()
    )

    queries_exec: dict[str, dict[GraphShardMetadataLanguage, tuple[Query, ...]]] = {}
    deps_dict: dict[str, dict[str, dict[str, dict[str, str]]]] = {}
    if execute_reachability:
        try:
            queries_exec, deps_dict = await initialize_reachability()
        except (
            UnexpectedExceptionError,
            UnexpectedNodeError,
        ) as error:
            msg = f"Error while parsing dependencies,skipping reachability analysis. Error: {error}"
            log_blocking("warning", msg)
            log_to_remote_blocking(msg=msg, severity="error")
            execute_reachability = False

    graph_db = (
        get_graph_db(paths, ctx.SKIMS_CONFIG.working_dir) if ctx.SKIMS_CONFIG.multifile else None
    )

    executor_client = ThreadPoolExecutor if ctx.SKIMS_CONFIG.multifile else ProcessPoolExecutor

    with executor_client(max_workers=ctx.CPU_CORES) as worker:
        for path, language in paths_dict.items():
            package_path = find_dependency_file(path, language) if execute_reachability else ""

            if not any(
                finding in ctx.SKIMS_CONFIG.checks for finding in FINDINGS_BY_LANGUAGE[language]
            ) and (package_path and not queries_exec.get(package_path, None)):
                continue

            future = worker.submit(
                _analyze_one_path,
                config=ctx.SKIMS_CONFIG,
                path=path,
                graph_db=graph_db,
                language=language,
                reachability_queries=queries_exec,
                package_path=package_path,
                deps_dict=deps_dict.get("items", {}).get(package_path, {}),
            )
            _store_results_callback(stores, future, path)
    log_blocking("info", "Advanced SAST analysis completed!")
