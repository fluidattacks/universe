import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def is_ftp_session_factory(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "DefaultFtpSessionFactory"
    )


def matches_insecure_ftp_url_pattern(value: str) -> bool:
    insecure_ftp_pattern = r"^[fF][tT][pP]:.*$"
    return re.match(insecure_ftp_pattern, value) is not None


def is_insecure_ftp_literal(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "Literal"
        and (value := graph.nodes[n_id].get("value"))
        and matches_insecure_ftp_url_pattern(value)
    )


def java_insecure_ftp_session_factory(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_FTP_SESSION_FACTORY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "setHost"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, obj_id, is_ftp_session_factory)
                and (first_arg := get_n_arg(graph, n_id, 0))
                and is_node_definition_unsafe(graph, first_arg, is_insecure_ftp_literal)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_ftp_client_instance(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "FTPClient"
    )


def java_insecure_ftp_client(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_FTP_CLIENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        nodes = graph.nodes
        for n_id in method_supplies.selected_nodes:
            if (
                nodes[n_id].get("expression", "") == "connect"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, obj_id, is_ftp_client_instance)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_literal_with_insecure_ftp_url(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id].get("label_type") == "Literal" and matches_insecure_ftp_url_pattern(
        graph.nodes[n_id].get("value", "")  # noqa: COM812
    )


def is_url_or_uri_with_insecure_ftp(graph: Graph, n_id: NId) -> bool:
    return bool(
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") in {"URL", "URI"}
        and (first_arg := get_n_arg(graph, n_id, 0))
        and is_node_definition_unsafe(graph, first_arg, is_literal_with_insecure_ftp_url),
    )


def java_insecure_ftp_url(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_FTP_URL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        nodes = graph.nodes
        for n_id in method_supplies.selected_nodes:
            if (
                nodes[n_id].get("expression", "") == "openConnection"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, obj_id, is_url_or_uri_with_insecure_ftp)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
