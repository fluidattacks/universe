from lib_sast.root.f148.c_sharp import (
    c_sharp_insecure_channel as _c_sharp_insecure_channel,
)
from lib_sast.root.f148.java import (
    java_insecure_ftp_client as _java_insecure_ftp_client,
)
from lib_sast.root.f148.java import (
    java_insecure_ftp_session_factory as _java_insecure_ftp_session_factory,
)
from lib_sast.root.f148.java import (
    java_insecure_ftp_url as _java_insecure_ftp_url,
)
from lib_sast.root.f148.terraform import (
    tfm_azure_app_service_ftp_deployments_enabled as _tfm_azure_app_service_ftp_deployments_enabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insecure_channel(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_channel(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_ftp_client(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_ftp_client(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_ftp_url(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_ftp_url(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_ftp_session_factory(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_ftp_session_factory(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_app_service_ftp_deployments_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_app_service_ftp_deployments_enabled(shard, method_supplies)
