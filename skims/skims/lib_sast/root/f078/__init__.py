from lib_sast.root.f078.php.php_generates_insecure_token import (
    php_generates_insecure_token as _php_generates_insecure_token,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_generates_insecure_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_generates_insecure_token(shard, method_supplies)
