from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] == "HttpRequest":
        args.evaluation[args.n_id] = True
        args.triggers.add("HttpRequest")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.evaluation[args.n_id] = True
        args.triggers.add("HttpRequest")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    member_access = f"{ma_attr['expression']}.{ma_attr['member']}"

    if member_access == "Type.GetType":
        args.evaluation[args.n_id] = True
        args.triggers.add(member_access)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
}


def c_sharp_check_xml_serializer(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_CHECK_XML_SERIALIZER
    danger_set = {"Type.GetType", "HttpRequest"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "XmlSerializer"
                and get_node_evaluation_results(
                    method,
                    graph,
                    node,
                    danger_set,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
