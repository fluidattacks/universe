from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f344.common import (
    local_storage_from_assignment,
    local_storage_from_http,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def ts_local_storage_with_sensitive_data(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from local_storage_from_http(method_supplies.selected_nodes, graph)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=MethodsEnum.TYPESCRIPT_LOCAL_STORAGE_WITH_SENSITIVE_DATA,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def ts_local_storage_sens_data(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TYPESCRIPT_LOCAL_STORAGE_SENSITIVE_DATA_ASSIGNMENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            yield from local_storage_from_assignment(graph, n_id, method)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
