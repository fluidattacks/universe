from collections.abc import (
    Iterable,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)


def get_async_danger_imports(graph: Graph) -> set[str]:
    danger_imports: set[str] = {"fetch"}
    if axios_alias := get_default_alias(graph, "axios"):
        danger_imports.add(axios_alias)
    if ky_alias := get_default_alias(graph, "ky"):
        danger_imports.add(ky_alias)
    if ky_universal_alias := get_default_alias(graph, "ky-universal"):
        danger_imports.add(ky_universal_alias)
    return danger_imports


def _is_using_http_library(graph: Graph, val_id: NId, dangerous_apis: set[str]) -> bool:
    if (  # noqa: SIM103
        (expr_id := graph.nodes[val_id].get("expression_id"))
        and (graph.nodes[expr_id]["label_type"] == "MethodInvocation")
        and (expr := graph.nodes[expr_id].get("expression"))
        and str(expr).rsplit(".", maxsplit=1)[0] in dangerous_apis
    ):
        return True
    return False


def _stores_http_response(graph: Graph, n_id: NId, dangerous_apis: set[str]) -> bool:
    if _is_using_http_library(graph, n_id, dangerous_apis):
        return True

    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id]["symbol"]
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_using_http_library(graph, val_id, dangerous_apis)
            ):
                return True

    return False


def local_storage_from_http(
    searched_nodes: list[NId],
    graph: Graph,
) -> Iterable[NId]:
    danger_imports = get_async_danger_imports(graph)
    for n_id in searched_nodes:
        if (
            graph.nodes[n_id].get("expression") == "localStorage.setItem"
            and (f_arg := get_n_arg(graph, n_id, 1))
            and _stores_http_response(graph, f_arg, danger_imports)
        ):
            yield f_arg


def vuln_assignment_n_ids(graph: Graph, method: MethodsEnum, n_id: NId) -> set[NId]:
    vuln_nodes: set[NId] = set()
    for path in get_backward_paths(graph, n_id):
        if evaluation := evaluate(method, graph, path, n_id):
            vuln_n_ids = {x.split("this_")[1] for x in evaluation.triggers}
            vuln_nodes.update(vuln_n_ids)
    return vuln_nodes


def local_storage_from_assignment(graph: Graph, n_id: NId, method: MethodsEnum) -> set[NId]:
    vuln_nodes: set[NId] = set()
    if (
        (var_id := graph.nodes[n_id].get("variable_id"))
        and (graph.nodes[var_id].get("label_type") == "MemberAccess")
        and (graph.nodes[var_id]["expression"] == "client")
        and (graph.nodes[var_id]["member"] == "onload")
    ):
        danger_id = graph.nodes[n_id].get("value_id")
        vuln_n_ids = vuln_assignment_n_ids(graph, method, danger_id)
        vuln_nodes.update(vuln_n_ids)
    return vuln_nodes
