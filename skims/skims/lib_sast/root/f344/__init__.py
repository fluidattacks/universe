from lib_sast.root.f344.javascript import (
    js_local_storage_sens_data as _js_local_storage_sens_data,
)
from lib_sast.root.f344.javascript import (
    js_local_storage_with_sensitive_data as _js_local_storage_sensitive_data,
)
from lib_sast.root.f344.typescript import (
    ts_local_storage_sens_data as _ts_local_storage_sens_data,
)
from lib_sast.root.f344.typescript import (
    ts_local_storage_with_sensitive_data as _ts_local_storage_sensitive_data,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_local_storage_sensitive_data(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_local_storage_sensitive_data(shard, method_supplies)


@SHIELD_BLOCKING
def js_local_storage_sens_data(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_local_storage_sens_data(shard, method_supplies)


@SHIELD_BLOCKING
def ts_local_storage_sensitive_data(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_local_storage_sensitive_data(shard, method_supplies)


@SHIELD_BLOCKING
def ts_local_storage_sens_data(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_local_storage_sens_data(shard, method_supplies)
