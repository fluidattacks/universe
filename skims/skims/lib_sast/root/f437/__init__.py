from lib_sast.root.f437.github_actions import (
    github_actions_without_hash as _github_actions_without_hash,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def github_actions_without_hash(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _github_actions_without_hash(shard, method_supplies)
