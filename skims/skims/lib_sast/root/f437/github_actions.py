import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_nid_list_from_node,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _not_includes_hash(value: str) -> bool:
    pattern = r"(actions|github)/([^@]+)@(?P<version>.+)$"
    if (match := re.match(pattern, value)) and len(match.group("version")) not in {40, 64}:
        return True
    docker_pattern = r"docker://([^:]+):(?P<version>.+)$"
    sha_pattern = r"([a-fA-F0-9]{64})$"
    if (  # noqa: SIM103
        (docker_match := re.match(docker_pattern, value))
        and (image_ref := docker_match.group("version"))
        and (not image_ref or not re.match(sha_pattern, image_ref.split(":")[-1]))
    ):
        return True
    return False


def get_actions_without_hash(graph: Graph, nid: NId) -> Iterator[NId]:
    steps_nid = get_nid_list_from_node(graph, nid)
    for step in steps_nid:
        if (image := get_optional_attribute(graph, step, "uses")) and _not_includes_hash(image[1]):
            yield image[2]


def github_actions_without_hash(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.GITHUB_ACTIONS_WITHOUT_HASH

    def n_ids() -> Iterator[NId]:
        if ".github/workflows" in shard.path:
            graph = shard.syntax_graph
            for nid in method_supplies.selected_nodes:
                key_id = graph.nodes[nid]["key_id"]
                if graph.nodes[key_id]["value"] != "steps":
                    continue

                yield from get_actions_without_hash(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
