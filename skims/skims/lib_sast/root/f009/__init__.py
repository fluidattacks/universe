from lib_sast.root.f009.conf_files.conf_files_credentials_exposed_in_code import (
    conf_files_credentials_exposed_in_code as _conf_files_exposed_credentials,
)
from lib_sast.root.f009.conf_files.db_credentials_exposed_in_code import (
    json_db_credentials_in_code as _json_db_credentials_in_code,
)
from lib_sast.root.f009.conf_files.sensitive_info_in_dotnet_json import (
    json_sensitive_info_in_dotnet as _json_sensitive_info_in_dotnet,
)
from lib_sast.root.f009.conf_files.sensitive_info_in_json import (
    json_sensitive_info as _json_sensitive_info,
)
from lib_sast.root.f009.conf_files.sensitive_key_in_json import (
    json_sensitive_key as _json_sensitive_key,
)
from lib_sast.root.f009.docker_compose.docker_compose_env_secrets import (
    docker_compose_env_secrets as _docker_compose_env_secrets,
)
from lib_sast.root.f009.java.java_credentials_exposed_in_code import (
    java_credentials_exposed_in_code as _java_credentials_exposed_in_code,
)
from lib_sast.root.f009.java.java_csrf_handler_hardcoded_password import (
    java_csrf_handler_hardcoded_password as _java_csrf_handler_hardcoded_password,
)
from lib_sast.root.f009.javascript.javascript_crypto_js_credentials import (
    js_crypto_js_credentials as _js_crypto_js_credentials,
)
from lib_sast.root.f009.javascript.js_credentials_exposed_in_code import (
    js_credentials_exposed_in_code as _js_credentials_exposed_in_code,
)
from lib_sast.root.f009.kotlin.kotlin_credentials_exposed_in_code import (
    kotlin_credentials_exposed_in_code as _kt_credentials_exposed_in_code,
)
from lib_sast.root.f009.python.exposed_authorization_token import (
    python_exposed_token as _python_exposed_token,
)
from lib_sast.root.f009.python.python_credentials_exposed_in_code import (
    python_credentials_exposed_in_code as _python_credentials_exposed_in_code,
)
from lib_sast.root.f009.swift.swift_credentials_exposed_in_code import (
    swift_credentials_exposed_in_code as _swift_credentials_exposed_in_code,
)
from lib_sast.root.f009.typescript.ts_credentials_exposed_in_code import (
    ts_credentials_exposed_in_code as _ts_credentials_exposed_in_code,
)
from lib_sast.root.f009.typescript.typescript_crypto_ts_credentials import (
    ts_crypto_ts_credentials as _ts_crypto_ts_credentials,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_csrf_handler_hardcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_csrf_handler_hardcoded_password(shard, method_supplies)


@SHIELD_BLOCKING
def json_sensitive_info(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_sensitive_info(shard, method_supplies)


@SHIELD_BLOCKING
def json_sensitive_info_in_dotnet(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_sensitive_info_in_dotnet(shard, method_supplies)


@SHIELD_BLOCKING
def json_sensitive_key(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _json_sensitive_key(shard, method_supplies)


@SHIELD_BLOCKING
def json_db_credentials_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_db_credentials_in_code(shard, method_supplies)


@SHIELD_BLOCKING
def docker_compose_env_secrets(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _docker_compose_env_secrets(shard, method_supplies)


@SHIELD_BLOCKING
def python_exposed_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_exposed_token(shard, method_supplies)


@SHIELD_BLOCKING
def python_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_credentials_exposed_in_code(shard, method_supplies)


@SHIELD_BLOCKING
def java_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_credentials_exposed_in_code(shard, method_supplies)


@SHIELD_BLOCKING
def js_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_credentials_exposed_in_code(shard, method_supplies)


@SHIELD_BLOCKING
def js_crypto_js_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_crypto_js_credentials(shard, method_supplies)


@SHIELD_BLOCKING
def ts_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_credentials_exposed_in_code(shard, method_supplies)


@SHIELD_BLOCKING
def ts_crypto_ts_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_crypto_ts_credentials(shard, method_supplies)


@SHIELD_BLOCKING
def kt_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_credentials_exposed_in_code(shard, method_supplies)


@SHIELD_BLOCKING
def swift_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _swift_credentials_exposed_in_code(shard, method_supplies)


@SHIELD_BLOCKING
def conf_files_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _conf_files_exposed_credentials(shard, method_supplies)
