from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.f009.utils import (
    is_key_sensitive,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.docker import (
    iterate_env_variables,
    validate_path,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _compose_env_secrets(graph: Graph, nid: NId) -> bool:
    secret_smells: set[str] = {
        "api_key",
        "jboss_pass",
        "license_key",
        "password",
        "secret",
    }
    env_var = graph.nodes[nid].get("value", "")
    env_var_str: str = env_var.lower()
    key_val = env_var_str.split("=", 1)
    secret = key_val[0]
    return (
        len(key_val) > 1
        and not (key_val[1].startswith("${") and key_val[1].endswith("}"))
        and (any(smell in secret for smell in secret_smells) or is_key_sensitive(secret))
    )


def docker_compose_env_secrets(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_COMPOSE_ENV_SECRETS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if shard.syntax_graph is None or not validate_path(shard.path):
            return
        for nid in iterate_env_variables(graph, method_supplies):
            if _compose_env_secrets(graph, nid):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
