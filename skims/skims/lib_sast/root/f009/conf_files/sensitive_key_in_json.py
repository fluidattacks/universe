import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_key_value,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _sensitive_key_in_json(key_pair: str, value: str) -> bool:
    key_smell = {
        "api_key",
        "current_key",
    }
    grammar = re.compile(r"[A-Za-z0-9]{5,}")
    return key_pair in key_smell and bool(re.fullmatch(grammar, value))


def json_sensitive_key(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.SENSITIVE_KEY_IN_JSON

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            key, value = get_key_value(graph, node)

            if key == "current_key" and shard.path.endswith("google-services.json"):
                return

            if _sensitive_key_in_json(key, value):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
