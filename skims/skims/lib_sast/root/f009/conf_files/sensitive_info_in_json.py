import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_key_value,
    is_parent,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _has_password(value: str) -> bool:
    regex_password = re.compile(r"Password=.*")
    return any(re.match(regex_password, element) for element in value.split(";"))


def _sensitive_info_json(graph: Graph, nid: NId, key_pair: str, value: str) -> bool:
    correct_parents = ["ConnectionStrings"]
    return key_pair == "Claims" and _has_password(value) and is_parent(graph, nid, correct_parents)


def json_sensitive_info(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.SENSITIVE_INFO_IN_JSON

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            key, value = get_key_value(graph, node)

            if _sensitive_info_json(graph, node, key, value):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
