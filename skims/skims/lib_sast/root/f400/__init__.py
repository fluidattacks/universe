from lib_sast.root.f400.cloudformation import (
    cfn_bucket_has_logging_conf_disabled as _cfn_bucket_logging_conf_disabled,
)
from lib_sast.root.f400.cloudformation import (
    cfn_cf_distribution_has_logging_disabled as _cfn_distribution_no_logging,
)
from lib_sast.root.f400.cloudformation import (
    cfn_elb2_has_access_logs_s3_disabled as _cfn_elb2_access_logs_s3_disabled,
)
from lib_sast.root.f400.cloudformation import (
    cfn_elb_has_access_logging_disabled as _cfn_elb_access_logging_disabled,
)
from lib_sast.root.f400.cloudformation import (
    cfn_redshift_has_audit_logging_disabled as _cfn_rds_audit_logs_disabled,
)
from lib_sast.root.f400.cloudformation import (
    cfn_redshift_user_act_logging_disabled as _cfn_rds_user_act_log_disabled,
)
from lib_sast.root.f400.cloudformation import (
    cfn_trails_not_multiregion as _cfn_trails_not_multiregion,
)
from lib_sast.root.f400.terraform import (
    tfm_distribution_has_logging_disabled as _tfm_dist_has_logging_disabled,
)
from lib_sast.root.f400.terraform import (
    tfm_load_balancers_logging_disabled as _tfm_elb_logging_disabled,
)
from lib_sast.root.f400.terraform import (
    tfm_redshift_has_audit_logging_disabled as _tfm_rds_audit_log_disabled,
)
from lib_sast.root.f400.terraform import (
    tfm_redshift_user_act_logging_disabled as _tfm_rds_user_act_log_disabled,
)
from lib_sast.root.f400.terraform import (
    tfm_trails_not_multiregion as _tfm_trails_not_multiregion,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_bucket_has_logging_conf_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_bucket_logging_conf_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_cf_distribution_has_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_distribution_no_logging(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_elb2_has_access_logs_s3_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_elb2_access_logs_s3_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_elb_has_access_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_elb_access_logging_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_redshift_has_audit_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_audit_logs_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_redshift_user_act_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_user_act_log_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_trails_not_multiregion(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_trails_not_multiregion(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_distribution_has_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_dist_has_logging_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_load_balancers_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_elb_logging_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_redshift_has_audit_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_audit_log_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_redshift_user_act_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_user_act_log_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_trails_not_multiregion(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_trails_not_multiregion(shard, method_supplies)
