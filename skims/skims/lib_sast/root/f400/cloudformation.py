from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _logging_disabled(graph: Graph, nid: NId) -> NId | None:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and not get_optional_attribute(graph, val_id, "LoggingProperties")
    ):
        return props[2]
    return None


def _user_activity_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    if not (props := get_optional_attribute(graph, nid, "Properties")):
        return None

    val_id = graph.nodes[props[2]]["value_id"]
    params = get_optional_attribute(graph, val_id, "Parameters")
    if not params:
        return props[2]

    p_exist = False
    for c_id in adj_ast(graph, graph.nodes[params[2]]["value_id"]):
        if (
            (p_name := get_optional_attribute(graph, c_id, "ParameterName"))
            and (p_val := get_optional_attribute(graph, c_id, "ParameterValue"))
            and p_name[1] == "enable_user_activity_logging"
        ):
            p_exist = True
            if p_val[1] != "true":
                return p_name[2]
    if not p_exist:
        return props[2]
    return None


def _bucket_has_logging_conf_disabled(graph: Graph, nid: NId) -> NId | None:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return None
    val_id = graph.nodes[prop_id]["value_id"]
    if not get_optional_attribute(graph, val_id, "LoggingConfiguration"):
        return prop_id
    return None


def _cf_dist_has_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return None
    val_id = graph.nodes[prop_id]["value_id"]
    if d_config := get_optional_attribute(graph, val_id, "DistributionConfig"):
        data_id = graph.nodes[d_config[2]]["value_id"]
        logging, _, _ = get_attribute(graph, data_id, "Logging")
        if not logging:
            return d_config[2]
    return None


def _elb2_has_access_logs_s3_disabled(graph: Graph, nid: NId) -> NId | None:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return None
    val_id = graph.nodes[prop_id]["value_id"]

    if not (load_bal := get_optional_attribute(graph, val_id, "LoadBalancerAttributes")):
        return prop_id
    key_exist = False
    load_attrs = graph.nodes[load_bal[2]]["value_id"]
    for c_id in adj_ast(graph, load_attrs):
        if (key := get_optional_attribute(graph, c_id, "Key")) and key[
            1
        ] == "access_logs.s3.enabled":
            key_exist = True
            value = get_optional_attribute(graph, c_id, "Value")
            if value and value[1] in FALSE_OPTIONS:
                return value[2]
    if not key_exist:
        return load_bal[2]
    return None


def _elb_has_access_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return None
    val_id = graph.nodes[prop_id]["value_id"]
    access_log = get_optional_attribute(graph, val_id, "AccessLoggingPolicy")
    if not access_log:
        return prop_id
    val_id = graph.nodes[access_log[2]]["value_id"]
    enabled = get_optional_attribute(graph, val_id, "Enabled")
    if not enabled:
        return access_log[2]
    if enabled[1] in FALSE_OPTIONS:
        return enabled[2]
    return None


def _trails_not_multiregion(graph: Graph, nid: NId) -> NId | None:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return None
    val_id = graph.nodes[prop_id]["value_id"]
    trail = get_optional_attribute(graph, val_id, "IsMultiRegionTrail")
    if not trail:
        return prop_id
    if trail[1] in FALSE_OPTIONS:
        return trail[2]
    return None


def cfn_trails_not_multiregion(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_TRAILS_NOT_MULTIREGION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::CloudTrail::Trail"
                and (report := _trails_not_multiregion(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_elb_has_access_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ELB_HAS_ACCESS_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::ElasticLoadBalancing::LoadBalancer"
                and (report := _elb_has_access_logging_disabled(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_elb2_has_access_logs_s3_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ELB2_HAS_ACCESS_LOGS_S3_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::ElasticLoadBalancingV2::LoadBalancer"
                and (report := _elb2_has_access_logs_s3_disabled(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_cf_distribution_has_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_CF_DISTRIBUTION_HAS_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::CloudFront::Distribution"
                and (report := _cf_dist_has_logging_disabled(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_bucket_has_logging_conf_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_BUCKET_HAS_LOGGING_CONF_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::S3::Bucket"
                and (report := _bucket_has_logging_conf_disabled(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_redshift_user_act_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_REDSHIFT_IS_USER_ACTIVITY_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::Redshift::ClusterParameterGroup"
                and (report := _user_activity_logging_disabled(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_redshift_has_audit_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_REDSHIFT_HAS_AUDIT_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::Redshift::Cluster"
                and (report_id := _logging_disabled(graph, nid))
            ):
                yield report_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
