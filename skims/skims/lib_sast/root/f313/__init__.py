from lib_sast.root.f313.c_sharp import (
    c_sharp_insecure_certificate_validation as _c_sharp_insec_cert_validation,
)
from lib_sast.root.f313.c_sharp import (
    c_sharp_insecure_x509_cert_2 as _c_sharp_insecure_x509_cert_2,
)
from lib_sast.root.f313.cloudformation import (
    cfn_insecure_certificate as _cfn_insecure_certificate,
)
from lib_sast.root.f313.python import (
    python_unsafe_certificate_validation as _python_unsafe_cert_validation,
)
from lib_sast.root.f313.python import (
    python_unsafe_ssl_context_certificate as _python_unsafe_ssl_ctx_cert,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insecure_certificate_validation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insec_cert_validation(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_x509_cert_2(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_x509_cert_2(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_insecure_certificate(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_insecure_certificate(shard, method_supplies)


@SHIELD_BLOCKING
def python_unsafe_certificate_validation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_unsafe_cert_validation(shard, method_supplies)


@SHIELD_BLOCKING
def python_unsafe_ssl_context_certificate(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_unsafe_ssl_ctx_cert(shard, method_supplies)
