from collections.abc import (
    Callable,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.common import (
    check_js_ts_http_inputs,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if check_js_ts_http_inputs(args):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
}


def remote_command_exec_nodes(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []

    if not (cp_lib := get_default_alias(graph, "child_process")):
        return vuln_nodes

    danger_methods = {f"{cp_lib}.exec", f"{cp_lib}.execSync", "execSync", "exec", "execa.command"}
    for n_id in method_supplies.selected_nodes:
        m_expr = graph.nodes[n_id]["expression"]

        if (
            m_expr in danger_methods
            and (f_arg := get_n_arg(graph, n_id, 0))
            and get_node_evaluation_results(
                method,
                graph,
                f_arg,
                set(),
                method_evaluators=METHOD_EVALUATORS,
            )
        ):
            vuln_nodes.append(f_arg)
    return vuln_nodes
