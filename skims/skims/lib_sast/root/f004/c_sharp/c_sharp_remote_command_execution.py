from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    NET_CONNECTION_OBJECTS,
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if ma_attr["member"] in NET_SYSTEM_HTTP_METHODS:
        args.triggers.add("user_parameters")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] in NET_CONNECTION_OBJECTS:
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
}


def _is_using_executor(graph: Graph, n_id: NId) -> bool:
    if (
        graph.nodes[n_id]["label_type"] == "MemberAccess"
        and (symbol_id := graph.nodes[n_id].get("expression_id"))
        and graph.nodes[symbol_id]["label_type"] == "SymbolLookup"
    ):
        symbol = graph.nodes[symbol_id]["symbol"]
        if symbol in {"Process", "Executor"}:
            return True

        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and (graph.nodes[val_id]["label_type"] == "ObjectCreation")
                and graph.nodes[val_id].get("name") in {"Process", "Executor"}
            ):
                return True
    return False


def _is_user_connection(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    danger_p1 = {"user_connection", "user_parameters"}
    if al_id := graph.nodes[n_id].get("arguments_id"):
        return get_node_evaluation_results(
            method,
            graph,
            al_id,
            danger_p1,
            danger_goal=False,
            method_evaluators=METHOD_EVALUATORS,
        )
    return False


def c_sharp_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_REMOTE_COMMAND_EXECUTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            expression = n_attrs["expression"].split(".")[-1].lower()
            if (
                expression in {"start", "execute"}
                and (expr_id := n_attrs.get("expression_id"))
                and _is_using_executor(graph, expr_id)
                and _is_user_connection(method, graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
