from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    PYTHON_INPUTS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    member_access = f"{n_attrs['expression']}.{n_attrs['member']}"
    if member_access in PYTHON_INPUTS:
        args.evaluation[args.n_id] = True
        args.triggers.add("userparams")
    elif member_access == "shlex.quote":
        args.triggers.add("safeparams")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _literal_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["value"] == "True":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "literal": _literal_evaluator,
    "member_access": _member_access_evaluator,
}


def _is_danger_shell(graph: Graph, n_ids: list[NId], method: MethodsEnum) -> bool:
    for _id in n_ids:
        if graph.nodes[_id].get("argument_name") != "shell":
            continue
        val_id = graph.nodes[_id]["value_id"]
        return get_node_evaluation_results(
            method,
            graph,
            val_id,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        )
    return False


def _is_danger_expression(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    m_attrs = graph.nodes[n_id]
    expr: str = m_attrs["expression"]
    al_id = m_attrs.get("arguments_id")
    if not al_id:
        return False
    args_ids = list(adj_ast(graph, al_id))

    return (
        expr.startswith("os.")
        and get_node_evaluation_results(
            method,
            graph,
            args_ids[0],
            {"userparams"},
            method_evaluators=METHOD_EVALUATORS,
        )
    ) or (
        len(args_ids) > 1
        and _is_danger_shell(graph, args_ids[1:], method)
        and get_node_evaluation_results(
            method,
            graph,
            args_ids[0],
            {"userparams"},
            method_evaluators=METHOD_EVALUATORS,
        )
    )


def python_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_REMOTE_COMMAND_EXECUTION
    danger_set = {"os.popen", "subprocess.Popen"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node]["expression"] in danger_set and _is_danger_expression(
                graph,
                node,
                method,
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
