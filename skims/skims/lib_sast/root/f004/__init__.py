from lib_sast.root.f004.c_sharp.c_sharp_remote_command_execution import (
    c_sharp_remote_command_execution as _c_sharp_remote_command_execution,
)
from lib_sast.root.f004.java.java_remote_command_execution import (
    java_remote_command_1 as _java_remote_command_1,
)
from lib_sast.root.f004.java.java_remote_command_execution import (
    java_remote_command_2 as _java_remote_command_2,
)
from lib_sast.root.f004.javascript.javascript_remote_command_execution import (
    js_remote_command_execution as _js_remote_command_execution,
)
from lib_sast.root.f004.kotlin.kt_remote_command_execution import (
    kt_remote_command_exec as _kt_remote_command_exec,
)
from lib_sast.root.f004.php.php_remote_command_execution import (
    php_remote_command_execution as _php_remote_command_execution,
)
from lib_sast.root.f004.python.python_remote_command_execution import (
    python_remote_command_execution as _python_remote_command_execution,
)
from lib_sast.root.f004.typescript.typescript_remote_command_execution import (
    ts_remote_command_execution as _ts_remote_command_execution,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_remote_command_execution(shard, method_supplies)


@SHIELD_BLOCKING
def java_remote_command_1(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_remote_command_1(shard, method_supplies)


@SHIELD_BLOCKING
def java_remote_command_2(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_remote_command_2(shard, method_supplies)


@SHIELD_BLOCKING
def js_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_remote_command_execution(shard, method_supplies)


@SHIELD_BLOCKING
def kt_remote_command_exec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_remote_command_exec(shard, method_supplies)


@SHIELD_BLOCKING
def python_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_remote_command_execution(shard, method_supplies)


@SHIELD_BLOCKING
def php_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_remote_command_execution(shard, method_supplies)


@SHIELD_BLOCKING
def ts_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_remote_command_execution(shard, method_supplies)
