from lib_sast.root.f332.cloudformation import (
    cfn_sec_group_uses_insecure_protocol as _cfn_sec_group_insecure_protocol,
)
from lib_sast.root.f332.cloudformation import (
    cfn_server_disabled_ssl as _cfn_server_disabled_ssl,
)
from lib_sast.root.f332.java import (
    java_unencrypted_socket as _java_unencrypted_socket,
)
from lib_sast.root.f332.java import (
    java_unsafe_tls_renegotiation as _java_unsafe_tls_renegotiation,
)
from lib_sast.root.f332.kotlin import (
    kt_unencrypted_channel as _kt_unencrypted_channel,
)
from lib_sast.root.f332.kubernetes import (
    k8s_insecure_port as _k8s_insecure_port,
)
from lib_sast.root.f332.kubernetes import (
    k8s_insecure_server_protocol as _k8s_insecure_server_protocol,
)
from lib_sast.root.f332.terraform import (
    tfm_k8s_insecure_port as _tfm_k8s_insecure_port,
)
from lib_sast.root.f332.terraform import (
    tfm_sec_group_uses_insecure_protocol as _tfm_sec_group_insecure_protocol,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_sec_group_uses_insecure_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_sec_group_insecure_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_server_disabled_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_server_disabled_ssl(shard, method_supplies)


@SHIELD_BLOCKING
def java_unencrypted_socket(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unencrypted_socket(shard, method_supplies)


@SHIELD_BLOCKING
def java_unsafe_tls_renegotiation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unsafe_tls_renegotiation(shard, method_supplies)


@SHIELD_BLOCKING
def kt_unencrypted_channel(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_unencrypted_channel(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_insecure_port(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _k8s_insecure_port(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_insecure_port(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_insecure_port(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_insecure_server_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_insecure_server_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_sec_group_uses_insecure_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_sec_group_insecure_protocol(shard, method_supplies)
