from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def is_unsafe_param(graph: Graph, n_id: NId) -> bool:
    if (
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (children := g.adj_ast(graph, al_id))
        and len(children) > 0
        and (member := graph.nodes[children[0]].get("member"))
    ):
        return member == "CLEARTEXT"
    return False


def kt_unencrypted_channel(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_UNENCRYPTED_CHANNEL
    danger_methods = {"FTPClient", "SMTPClient", "TelnetClient"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if n_attrs["expression"] in danger_methods or (
                n_attrs["expression"] == "ConnectionSpec.Builder" and is_unsafe_param(graph, node)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
