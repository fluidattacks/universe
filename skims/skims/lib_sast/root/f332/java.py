from collections.abc import Iterator

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_any_library_imported,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def is_unsafe_pattern(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id].get("label_type") == "Literal" and graph.nodes[n_id].get(
        "value",
        "",
    ).lower().startswith(("http://", "ftp://", "telnet://"))


def is_unsafe_url_or_uri_instance(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("label_type") == "ObjectCreation":
        return bool(
            graph.nodes[n_id].get("name", "") in {"URL", "URI"}
            and (first_arg := get_n_arg(graph, n_id, 0))
            and is_node_definition_unsafe(graph, first_arg, is_unsafe_pattern),
        )

    if graph.nodes[n_id].get("label_type") == "MethodInvocation":
        return bool(
            graph.nodes[n_id].get("expression", "") == "create"
            and (obj_id := graph.nodes[n_id].get("object_id"))
            and graph.nodes[obj_id].get("symbol", "") == "URI"
            and (first_arg := get_n_arg(graph, n_id, 0))
            and is_node_definition_unsafe(graph, first_arg, is_unsafe_pattern),
        )

    return False


def is_allow_unsafe_renegotiation_str(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "Literal"
        and graph.nodes[n_id].get("value", "") == "sun.security.ssl.allowUnsafeRenegotiation"
    )


def is_true_bool(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("value_type", "") == "bool"
        and graph.nodes[n_id].get("value", "") == "true"
    )


def java_unsafe_tls_renegotiation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UNSAFE_TLS_RENEGOTIATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "setProperty"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and is_node_definition_unsafe(graph, first_arg, is_allow_unsafe_renegotiation_str)
                and (second_arg := get_n_arg(graph, n_id, 1))
                and is_node_definition_unsafe(graph, second_arg, is_true_bool)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_unencrypted_socket(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UNENCRYPTED_SOCKET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"java.net.Socket", "java.net.ServerSocket"}):
            return

        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("name", "") in {"Socket", "ServerSocket"}:
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
