from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _aws_sec_group_using_tcp(graph: Graph, nid: NId) -> NId | None:
    if (
        (ingress := get_argument(graph, nid, "ingress"))
        and (protocol := get_optional_attribute(graph, ingress, "protocol"))
        and protocol[1] in {"6", "tcp"}
        and (init_port := get_optional_attribute(graph, ingress, "from_port"))
        and init_port[1] == "80"
    ):
        return protocol[2]
    return None


def tfm_sec_group_uses_insecure_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_SEC_GROUP_USING_TCP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_security_group" and (
                report := _aws_sec_group_using_tcp(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _check_insecure_port(graph: Graph, nid: NId) -> list[str]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    port_ids = [cid for cid in cont_childs_nids if graph.nodes[cid].get("name") == "port"]
    port_childs_nids: list[tuple[str, str, NId] | None] = [
        get_optional_attribute(graph, pid, "port") for pid in port_ids
    ]
    return [pid[2] for pid in port_childs_nids if pid and pid[1] == "80"]


def tfm_k8s_insecure_port(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_KUBERNETES_INSECURE_PORT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in [
                "kubernetes_service",
                "kubernetes_service_v1",
            ]:
                for spec_nid in get_argument_iterator(
                    graph,
                    nid,
                    "spec",
                ):
                    yield from _check_insecure_port(graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
