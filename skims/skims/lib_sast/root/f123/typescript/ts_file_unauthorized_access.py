from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_sanitized,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    args.triggers.add("possible_sanitization")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "Request":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def get_send_resolve_args(graph: Graph, n_id: NId) -> set[NId]:
    nodes = graph.nodes
    path_args: set[NId] = set()

    if nodes[n_id].get("expression") == "path.resolve" and (p_ids := g.pred_ast(graph, n_id, 2)):
        parent_types = {nodes[node].get("label_type") for node in p_ids}
        if (
            (parent_types == {"ArgumentList", "MethodInvocation"})
            and (nodes[p_ids[-1]].get("expression") == "res.sendFile")
            and (resolve_args := nodes[n_id].get("arguments_id"))
        ):
            path_args.update(
                arg_n_id
                for arg_n_id in g.adj_ast(graph, resolve_args)
                if nodes[arg_n_id].get("label_type") == "SymbolLookup"
            )

    return path_args


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": method_invocation_evaluator,
    "parameter": parameter_evaluator,
}


def has_not_sanitized_user_input(
    graph: Graph,
    path_element_n_ids: set[NId],
    method: MethodsEnum,
) -> bool:
    for n_id in path_element_n_ids:
        if get_node_evaluation_results(
            method,
            graph,
            n_id,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        ) and not is_sanitized(
            graph,
            n_id,
        ):
            return True
    return False


def ts_file_unauthorized_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TS_FILE_UNAUTHORIZED_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if file_imports_module(graph, "express") and file_imports_module(graph, "path"):
            for n_id in method_supplies.selected_nodes:
                if (path_components := get_send_resolve_args(graph, n_id)) and (
                    has_not_sanitized_user_input(graph, path_components, method)
                ):
                    yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
