from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _rds_has_unencrypted_storage(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if prop:
        val_id = graph.nodes[prop_id]["value_id"]
        storage, storage_val, storage_id = get_attribute(graph, val_id, "StorageEncrypted")
        if storage:
            if storage_val in FALSE_OPTIONS:
                yield storage_id
        else:
            yield prop_id


def cfn_rds_has_unencrypted_storage(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_RDS_HAS_UNENCRYPTED_STORAGE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[1] in {
                "AWS::RDS::DBCluster",
                "AWS::RDS::DBInstance",
            }:
                yield from _rds_has_unencrypted_storage(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
