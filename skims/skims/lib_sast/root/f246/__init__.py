from lib_sast.root.f246.cloudformation import (
    cfn_rds_has_unencrypted_storage as _cfn_rds_has_unencrypted_storage,
)
from lib_sast.root.f246.terraform import (
    tfm_rds_has_unencrypted_storage as _tfm_rds_has_unencrypted_storage,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_rds_has_unencrypted_storage(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_has_unencrypted_storage(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_rds_has_unencrypted_storage(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_has_unencrypted_storage(shard, method_supplies)
