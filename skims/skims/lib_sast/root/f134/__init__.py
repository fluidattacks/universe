from lib_sast.root.f134 import (
    java,
)
from lib_sast.root.f134.c_sharp import (
    c_sharp_insecure_cors as _c_sharp_insecure_cors,
)
from lib_sast.root.f134.c_sharp import (
    c_sharp_insecure_cors_origin_attribute as _c_sharp_insec_cors_attribute,
)
from lib_sast.root.f134.c_sharp import (
    c_sharp_insecure_cors_origin_method as _c_sharp_insecure_cors_method,
)
from lib_sast.root.f134.cloudformation import (
    cfn_cors_true as _cfn_cors_true,
)
from lib_sast.root.f134.cloudformation import (
    cfn_wildcard_in_allowed_origins as _cfn_wildcard_in_allowed_origins,
)
from lib_sast.root.f134.javascript.javascript_insecure_cors_origin import (
    javascript_insecure_cors_origin as _javascript_insecure_cors_origin,
)
from lib_sast.root.f134.javascript.javascript_nestjs_insecure_cors import (
    javascript_nestjs_insecure_cors as _javascript_nestjs_insecure_cors,
)
from lib_sast.root.f134.php import (
    php_insecure_cors as _php_insecure_cors,
)
from lib_sast.root.f134.python import (
    python_django_insecure_cors as _python_django_insecure_cors,
)
from lib_sast.root.f134.python import (
    python_fastapi_insecure_cors as _python_fastapi_insecure_cors,
)
from lib_sast.root.f134.python import (
    python_flask_insecure_cors as _python_flask_insecure_cors,
)
from lib_sast.root.f134.terraform import (
    tfm_wildcard_in_allowed_origins as _tfm_wildcard_in_allowed_origins,
)
from lib_sast.root.f134.typescript.typescript_insecure_cors_origin import (
    typescript_insecure_cors_origin as _typescript_insecure_cors_origin,
)
from lib_sast.root.f134.typescript.typescript_nestjs_insecure_cors import (
    typescript_nestjs_insecure_cors as _typescript_nestjs_insecure_cors,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_cors(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_cors_origin_attribute(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insec_cors_attribute(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_cors_origin_method(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_cors_method(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_cors_true(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _cfn_cors_true(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_wildcard_in_allowed_origins(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_wildcard_in_allowed_origins(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_cors_origin(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return java.java_insecure_cors_origin(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_cors_origin_modifier(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return java.java_insecure_cors_origin_modifier(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_cors_origin(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_insecure_cors_origin(shard, method_supplies)


@SHIELD_BLOCKING
def js_nestjs_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_nestjs_insecure_cors(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_wildcard_in_allowed_origins(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_wildcard_in_allowed_origins(shard, method_supplies)


@SHIELD_BLOCKING
def php_insecure_cors(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _php_insecure_cors(shard, method_supplies)


@SHIELD_BLOCKING
def python_django_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_django_insecure_cors(shard, method_supplies)


@SHIELD_BLOCKING
def python_fastapi_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_fastapi_insecure_cors(shard, method_supplies)


@SHIELD_BLOCKING
def python_flask_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_flask_insecure_cors(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_cors_origin(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_insecure_cors_origin(shard, method_supplies)


@SHIELD_BLOCKING
def ts_nestjs_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_nestjs_insecure_cors(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_cors_web_view(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return java.java_insecure_cors_web_view(shard, method_supplies)
