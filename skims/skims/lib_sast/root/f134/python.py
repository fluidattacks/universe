from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _node_literal_true_condition(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id]["label_type"] == "Literal" and graph.nodes[n_id].get("value") == "True"


def cors_allow_all_origins_enabled(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        graph.nodes[n_id].get("variable") == "CORS_ALLOW_ALL_ORIGINS"
        and (value_id := graph.nodes[n_id].get("value_id"))
        and is_node_definition_unsafe(graph, value_id, _node_literal_true_condition)
    ):
        return True
    return False


def has_corsheader_package(graph: Graph, n_id: NId) -> bool:
    if (
        graph.nodes[n_id].get("variable") == "INSTALLED_APPS"
        and (value_id := graph.nodes[n_id].get("value_id"))
        and (graph.nodes[value_id]["label_type"] == "ArrayInitializer")
    ):
        for child in adj_ast(graph, value_id):
            if (
                graph.nodes[child]["label_type"] == "Literal"
                and graph.nodes[child].get("value") == "corsheaders"
            ):
                return True
    return False


def python_django_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_DJANGO_INSECURE_CORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        corsheaders_package_found = False
        for n_id in method_supplies.selected_nodes:
            if has_corsheader_package(graph, n_id):
                corsheaders_package_found = True
            elif corsheaders_package_found and cors_allow_all_origins_enabled(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _node_wildcard_condition(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id]["label_type"] == "Literal" and graph.nodes[n_id].get("value") == "*"


def has_wildcard_in_origin_param(graph: Graph, n_id: NId) -> bool:
    val_id = graph.nodes[n_id].get("value_id")
    return bool(val_id) and is_node_definition_unsafe(
        graph,
        val_id,
        _node_wildcard_condition,
    )


def has_wildcard_in_resources_param(graph: Graph, n_id: NId) -> bool:
    value_id = graph.nodes[n_id].get("value_id")
    if not value_id:
        return False

    for child in adj_ast(graph, value_id, -1):
        if (
            (graph.nodes[child].get("label_type") == "Pair")
            and (key_id_pair := graph.nodes[child].get("key_id"))
            and (graph.nodes[key_id_pair].get("value") == "origins")
            and (value_id_pair := graph.nodes[child].get("value_id"))
            and (graph.nodes[value_id_pair].get("value") == "*")
        ):
            return True
    return False


def has_insecure_cors_config(graph: Graph, args_id: NId) -> bool:
    for argument in adj_ast(graph, args_id):
        arg_name = graph.nodes[argument].get("argument_name")
        if arg_name and arg_name == "origins":
            return has_wildcard_in_origin_param(graph, argument)
        if arg_name and arg_name == "resources":
            return has_wildcard_in_resources_param(graph, argument)
    return True


def has_flask_instance(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    if (  # noqa: SIM103
        (args_id := graph.nodes[n_id].get("arguments_id"))
        and (first_arg := match_ast(graph, args_id).get("__0__"))
        and get_node_evaluation_results(
            method,
            graph,
            first_arg,
            {"python_flask"},
            danger_goal=False,
        )
    ):
        return True
    return False


def python_flask_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_FLASK_INSECURE_CORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "CORS"
                and (args_id := graph.nodes[n_id].get("arguments_id"))
                and has_insecure_cors_config(graph, args_id)
                and has_flask_instance(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def array_contains_wildcard(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id]["label_type"] != "ArrayInitializer":
        return False

    return any(
        graph.nodes[array_arg]["label_type"] == "Literal"
        and graph.nodes[array_arg].get("value") == "*"
        for array_arg in adj_ast(graph, n_id)
    )


def has_allow_origins_with_wildcard(graph: Graph, args_id: NId) -> bool:
    args_ids = adj_ast(graph, args_id)
    if (
        len(args_ids) >= 2
        and (first_arg := adj_ast(graph, args_id)[0])
        and graph.nodes[first_arg]["label_type"] == "SymbolLookup"
        and graph.nodes[first_arg].get("symbol") == "CORSMiddleware"
    ):
        for argument in args_ids[1:]:
            if (
                (graph.nodes[argument]["label_type"] == "NamedArgument")
                and (graph.nodes[argument].get("argument_name") == "allow_origins")
                and (value_id := graph.nodes[argument].get("value_id"))
                and is_node_definition_unsafe(graph, value_id, array_contains_wildcard)
            ):
                return True
    return False


def has_fastapi_instance(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    return (
        (exp_id := graph.nodes[n_id].get("expression_id"))
        and (graph.nodes[exp_id]["label_type"] == "MemberAccess")
        and (exp_id_child := graph.nodes[exp_id].get("expression_id"))
        and get_node_evaluation_results(
            method,
            graph,
            exp_id_child,
            {"python_fastapi"},
            danger_goal=False,
        )
    )


def python_fastapi_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_FASTAPI_INSECURE_CORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (args_id := graph.nodes[n_id].get("arguments_id"))
                and has_allow_origins_with_wildcard(graph, args_id)
                and has_fastapi_instance(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
