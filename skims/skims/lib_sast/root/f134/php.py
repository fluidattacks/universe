from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from model.core import (
    MethodExecutionResult,
)


def check_literal(literal_val: str) -> bool:
    tokens = literal_val.lower().strip(" ").split(": ")
    if len(tokens) < 2:
        return False
    return bool(tokens[0] == "access-control-allow-origin" and tokens[1] == "*")


def eval_danger_arg(graph: Graph, c_id: NId, p_id: NId, method: MethodsEnum) -> bool:
    n_attrs = graph.nodes[c_id]

    if (node_type := n_attrs.get("label_type")) == "Literal":
        return check_literal(n_attrs.get("value"))
    if (
        node_type == "BinaryOperation"
        and (header_n_id := n_attrs.get("left_id"))
        and (graph.nodes[header_n_id].get("label_type") == "Literal")
        and (
            graph.nodes[header_n_id].get("value").strip(" ").lower()
            != "access-control-allow-origin:"
        )
    ):
        return False

    return get_node_evaluation_results(
        method,
        graph,
        p_id,
        {"dang_header", "dang_val"},
        danger_goal=False,
    )


def php_insecure_cors(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_CORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "header"
                and (f_arg := get_n_arg(graph, n_id, 0))
                and eval_danger_arg(graph, f_arg, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
