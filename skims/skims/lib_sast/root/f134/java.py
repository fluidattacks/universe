from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)


def is_vulnerable_origin(method: MethodsEnum, graph: Graph, nid: NId, check: str) -> bool:
    arg_id = graph.nodes[nid].get("arguments_id")
    if not arg_id:
        return False

    children = adj_ast(graph, arg_id)
    test_node = None
    if (
        check in {"header", "add", "addheader", "set", "setheader"}
        and len(children) > 1
        and graph.nodes[children[0]].get("value", "").lower() == "access-control-allow-origin"
    ):
        test_node = children[1]

    if check in {"allowedorigins", "addallowedorigin"} and len(children) == 1:
        test_node = children[0]

    if test_node:
        return get_node_evaluation_results(method, graph, test_node, set())

    return False


def java_insecure_cors_origin(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CORS_ORIGIN
    insecure_methods = {
        "header",
        "add",
        "addheader",
        "set",
        "setheader",
        "allowedorigins",
        "addallowedorigin",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            expr = graph.nodes[nid].get("expression")
            if (
                expr
                and expr.lower() in insecure_methods
                and is_vulnerable_origin(method, graph, nid, expr.lower())
            ):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _has_vulnerable_modifiers(graph: Graph, annotation_ids: list[NId]) -> NId | None:
    for _id in annotation_ids:
        if graph.nodes[_id]["name"] != "CrossOrigin":
            continue

        al_id = match_ast_d(graph, _id, "ArgumentList")
        if al_id is None:
            return _id

        for arg_id in match_ast_group_d(graph, al_id, "NamedArgument"):
            if (
                graph.nodes[arg_id]["argument_name"] == "origins"
                and (val_id := graph.nodes[arg_id]["value_id"])
                and (value := graph.nodes[val_id].get("value"))
                and value == "*"
            ):
                return arg_id
    return None


def java_insecure_cors_origin_modifier(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CORS_ORIGIN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (mod_id := graph.nodes[nid].get("modifiers_id"))
                and (anon_ids := match_ast_group_d(graph, mod_id, "Annotation"))
                and (vuln_id := _has_vulnerable_modifiers(graph, anon_ids))
            ):
                yield vuln_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_cors_web_view(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CORS_WEB_VIEW

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node_id in method_supplies.selected_nodes:
            node_attrs = graph.nodes[node_id]
            if node_attrs.get("expression") in (
                "setAllowUniversalAccessFromFileURLs",
                "setAllowFileAccessFromFileURLs",
            ):
                for arg_id in adj_ast(graph, node_attrs["arguments_id"]):
                    arg_attrs = graph.nodes[arg_id]
                    if arg_attrs.get("value") == "true":
                        yield node_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
