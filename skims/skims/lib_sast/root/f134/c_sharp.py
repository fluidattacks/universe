from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.c_sharp import (
    get_first_member_syntax_graph,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
    match_ast_d,
    match_ast_group_d,
    matching_nodes,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def get_insecure_vars(graph: Graph) -> list[str]:
    object_names = {"CorsPolicyBuilder"}
    insecure_vars = []
    for nid in matching_nodes(graph, label_type="ObjectCreation"):
        if (
            graph.nodes[nid].get("label_type") == "ObjectCreation"
            and graph.nodes[nid].get("name") in object_names
        ):
            var_nid = pred_ast(graph, nid)[0]
            if graph.nodes[var_nid].get("label_type") == "VariableDeclaration":
                insecure_vars.append(graph.nodes[var_nid].get("variable"))
    return insecure_vars


def is_insecure_use_cors(graph: Graph, nid: NId) -> bool:
    if not (
        (parent := pred_ast(graph, nid)[0])
        and (al_id := graph.nodes[parent].get("arguments_id"))
        and (arg_nid := match_ast(graph, al_id).get("__0__"))
    ):
        return False

    n_attrs = graph.nodes[arg_nid]
    if n_attrs["label_type"] == "MemberAccess" and n_attrs.get("member") == "AllowAll":
        return True
    return n_attrs["label_type"] == "MethodDeclaration" and "AllowAnyOrigin" in graph.nodes[
        n_attrs["block_id"]
    ].get("expression", "").split(".")


def is_vulnerable_enable_attribute(
    method: MethodsEnum,
    graph: Graph,
    al_id: NId,
) -> bool:
    arguments_ids = match_ast_group_d(graph, al_id, "Argument")

    test_nodes = [
        [
            *match_ast_group_d(graph, arg_id, "SymbolLookup"),
            *match_ast_group_d(graph, arg_id, "Literal"),
        ]
        for arg_id in arguments_ids
    ]

    args_childs = [
        childs
        for childs in test_nodes
        if len(childs) == 2
        and (symbol := graph.nodes[childs[0]].get("symbol"))
        and (label_type := graph.nodes[childs[1]].get("label_type"))
        and symbol == "origins"
        and label_type == "Literal"
        and get_node_evaluation_results(method, graph, childs[1], set())
    ]

    return len(args_childs) > 0


def is_vulnerable_policy(graph: Graph, n_id: NId) -> bool:
    if (
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (al_list := adj_ast(graph, al_id))
        and len(al_list) >= 2
        and graph.nodes[al_list[1]]["label_type"] == "MethodDeclaration"
    ):
        block_id = graph.nodes[al_list[1]]["block_id"]
        if graph.nodes[block_id]["label_type"] == "ExecutionBlock":
            m_id = match_ast(graph, block_id).get("__0__")
        else:
            m_id = block_id

        expr = graph.nodes[m_id].get("expression", "")
        if "AllowAnyOrigin" in expr:
            return True

    return False


def is_vulnerable_origin(graph: Graph, nid: NId, expr: str, method: MethodsEnum) -> bool:
    if "addpolicy" in expr.lower() and is_vulnerable_policy(graph, nid):
        return True

    if (
        "origins.add" in expr.lower()
        and (fr_m := get_first_member_syntax_graph(graph, nid))
        and get_node_evaluation_results(method, graph, fr_m, {"CorsObject"})
        and (arg_id := graph.nodes[nid].get("arguments_id"))
    ):
        children = adj_ast(graph, arg_id)
        if len(children) > 0 and get_node_evaluation_results(method, graph, children[0], set()):
            return True

    return False


def c_sharp_insecure_cors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_INSECURE_CORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        insecure_vars = get_insecure_vars(graph)
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("member") == "AllowAnyOrigin"
                and graph.nodes[node].get("expression").split(".")[0] in insecure_vars
            ) or (
                graph.nodes[node].get("member") == "UseCors" and is_insecure_use_cors(graph, node)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_insecure_cors_origin_method(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_INSECURE_CORS_ORIGIN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (expr := graph.nodes[n_id].get("expression")) and is_vulnerable_origin(
                graph,
                n_id,
                expr,
                method,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_insecure_cors_origin_attribute(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_INSECURE_CORS_ORIGIN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (str(graph.nodes[n_id].get("name")).lower() == "enablecors")
                and (al_id := match_ast_d(graph, n_id, "ArgumentList"))
                and is_vulnerable_enable_attribute(method, graph, al_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
