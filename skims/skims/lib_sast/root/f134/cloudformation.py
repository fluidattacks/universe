from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_key_value,
    is_parent,
    list_has_string,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    search_pred_until_type,
)
from model.core import (
    MethodExecutionResult,
)


def has_wildcard(graph: Graph, nid: NId) -> bool:
    parents_paths = [
        ["CorsRules", "CorsConfiguration", "Properties"],
        ["Cors", "Properties"],
        ["cors", "http"],
    ]

    if not any(is_parent(graph, nid, path) for path in parents_paths):
        return False

    value_id = graph.nodes[nid]["value_id"]

    return (
        graph.nodes[value_id]["label_type"] == "Literal"
        and graph.nodes[value_id]["value"] in {"*", "'*'"}
    ) or (
        graph.nodes[value_id]["label_type"] == "ArrayInitializer"
        and list_has_string(graph, value_id, "*")
    )


def cfn_wildcard_in_allowed_origins(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_WILDCARD_IN_ALLOWED_ORIGINS
    danger_keys = {"allowedorigins", "alloworigin", "origin"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            key_id = graph.nodes[nid]["key_id"]
            key = graph.nodes[key_id]["value"]
            if key.lower() in danger_keys and has_wildcard(graph, nid):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_in_path(graph: Graph, nid: NId, key_dict: str, value: str) -> bool:
    if (  # noqa: SIM103
        key_dict == "cors"
        and value == "true"
        and (parents := search_pred_until_type(graph, nid, {"Pair"}))
        and (parent_id := parents[0])
        and get_key_value(graph, parent_id)[0] == "http"
    ):
        return True
    return False


def cfn_cors_true(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.YML_SERVERLESS_CORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            key, value = get_key_value(graph, nid)
            if is_in_path(graph, nid, key, value):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
