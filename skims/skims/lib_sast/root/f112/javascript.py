from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f112.common import (
    has_create_pool,
    sql_injection,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def unsafe_sql_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_SQL_API_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not file_imports_module(graph, "mysql"):
            return

        danger_set = {"userconnection"}
        if has_create_pool(graph):
            danger_set = set()

        for n_id in method_supplies.selected_nodes:
            if sql_injection(graph, n_id, method, danger_set):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
