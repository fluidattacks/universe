from lib_sast.root.f083.c_sharp import (
    c_sharp_schema_by_url as _c_sharp_schema_by_url,
)
from lib_sast.root.f083.c_sharp import (
    c_sharp_xsl_transform_object as _c_sharp_xsl_transform_object,
)
from lib_sast.root.f083.c_sharp import (
    c_sharp_xxe_resolver as _c_sharp_xxe_resolver,
)
from lib_sast.root.f083.java import (
    java_allowed_external_entities as _java_allowed_external_entities,
)
from lib_sast.root.f083.java import (
    java_insecure_parser as _java_insecure_parser,
)
from lib_sast.root.f083.java import (
    java_xmlinputfactory_external_entities as _java_xmlinputfactory_external_entities,
)
from lib_sast.root.f083.javascript import (
    js_xml_parser as _js_xml_parser,
)
from lib_sast.root.f083.kotlin import (
    kt_insecure_parser as _kt_insecure_parser,
)
from lib_sast.root.f083.php import (
    php_xml_member_parser as _php_xml_member_parser,
)
from lib_sast.root.f083.php import (
    php_xml_symbol_parser as _php_xml_symbol_parser,
)
from lib_sast.root.f083.python import (
    python_xml_parser as _python_xml_parser,
)
from lib_sast.root.f083.typescript import (
    ts_xml_parser as _ts_xml_parser,
)
from lib_sast.root.f083.typescript import (
    ts_xml_parser_inside_context as _ts_xml_parser_inside_context,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_schema_by_url(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_schema_by_url(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_xsl_transform_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_xsl_transform_object(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_xxe_resolver(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_xxe_resolver(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_parser(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_parser(shard, method_supplies)


@SHIELD_BLOCKING
def java_allowed_external_entities(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_allowed_external_entities(shard, method_supplies)


@SHIELD_BLOCKING
def java_xmlinputfactory_external_entities(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_xmlinputfactory_external_entities(shard, method_supplies)


@SHIELD_BLOCKING
def js_xml_parser(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_xml_parser(shard, method_supplies)


@SHIELD_BLOCKING
def kt_insecure_parser(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _kt_insecure_parser(shard, method_supplies)


@SHIELD_BLOCKING
def php_xml_member_parser(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_xml_member_parser(shard, method_supplies)


@SHIELD_BLOCKING
def php_xml_symbol_parser(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_xml_symbol_parser(shard, method_supplies)


@SHIELD_BLOCKING
def python_xml_parser(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _python_xml_parser(shard, method_supplies)


@SHIELD_BLOCKING
def ts_xml_parser(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_xml_parser(shard, method_supplies)


@SHIELD_BLOCKING
def ts_xml_parser_inside_context(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_xml_parser_inside_context(shard, method_supplies)
