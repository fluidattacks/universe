from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_sanitized,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import evaluate
from lib_sast.symbolic_eval.utils import get_backward_paths
from model.core import (
    MethodExecutionResult,
)


def is_vuln(graph: Graph, method: MethodsEnum, n_id: NId) -> bool:
    for path in get_backward_paths(graph, n_id):
        if (evaluation := evaluate(method, graph, path, n_id)) and evaluation.danger:
            danger_source = next(iter(evaluation.triggers), None)
            return not is_sanitized(graph, n_id, danger_source)
    return False


def _is_member_external_entity(
    graph: Graph,
    n_id: NId,
    expression: str,
    method: MethodsEnum,
) -> bool:
    danger_symbols = {"LIBXML_NOENT", "SUBST_ENTITIES"}

    first_arg = get_n_arg(graph, n_id, 0)
    second_arg = get_n_arg(graph, n_id, 1)

    if first_arg is None or second_arg is None:
        return False

    if expression == "setParserProperty":
        return (
            (first_val := graph.nodes[first_arg].get("expression"))
            and first_val in danger_symbols
            # Assuming that the second argument is a boolean value
            and (second_val := graph.nodes[second_arg].get("value"))
            and second_val == "true"
        )
    second_val = graph.nodes[second_arg].get("symbol")
    return bool(second_val) and second_val in danger_symbols and is_vuln(graph, method, n_id)


def php_xml_member_parser(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_XML_PARSER
    danger_expressions = {
        "load",
        "loadXML",
        "setParserProperty",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (exp_id := graph.nodes[n_id].get("expression_id"))
                and graph.nodes[exp_id].get("label_type") == "MemberAccess"
                and (member := graph.nodes[exp_id].get("member"))
                and member in danger_expressions
                and _is_member_external_entity(graph, n_id, member, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _is_symbol_external_entity(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    danger_symbols = {"LIBXML_NOENT", "SUBST_ENTITIES"}

    first_arg = get_n_arg(graph, n_id, 0)
    third_arg = get_n_arg(graph, n_id, 2)

    if first_arg is None or third_arg is None:
        return False
    third_val = graph.nodes[third_arg].get("symbol")
    return bool(third_val) and third_val in danger_symbols and is_vuln(graph, method, n_id)


def php_xml_symbol_parser(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_XML_PARSER
    danger_expressions = {
        "simplexml_load_string",
        "simplexml_load_file",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (exp_id := graph.nodes[n_id].get("expression_id"))
                and graph.nodes[n_id].get("label_type") == "MethodInvocation"
                and (symbol := graph.nodes[exp_id].get("symbol"))
                and symbol in danger_expressions
                and _is_symbol_external_entity(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
