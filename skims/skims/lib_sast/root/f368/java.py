from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def is_session_vuln(
    method: MethodsEnum,
    graph: Graph,
    session_args: tuple[NId, ...],
) -> bool:
    return get_node_evaluation_results(
        method,
        graph,
        session_args[0],
        {"hostkey"},
    ) and get_node_evaluation_results(method, graph, session_args[1], {"disablecheck"})


def check_vulnerability_results(
    method: MethodsEnum,
    graph: Graph,
    n_attrs: dict,
) -> bool:
    if "object_id" not in n_attrs:
        return False
    if (  # noqa: SIM103
        get_node_evaluation_results(
            method,
            graph,
            n_attrs["object_id"],
            {"sshSession", "jshObject"},
        )
        and (al_list := n_attrs.get("arguments_id"))
        and (args_nodes := g.adj_ast(graph, al_list))
        and len(args_nodes) == 2
        and is_session_vuln(method, graph, args_nodes)
    ):
        return True
    return False


def java_host_key_checking(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_HOST_KEY_CHECKING

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                (expr := graph.nodes[node].get("expression"))
                and expr.split(".")[-1] == "setConfig"
                and check_vulnerability_results(method, graph, n_attrs)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
