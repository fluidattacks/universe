from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_any_library_imported,
    is_in_conditional,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _overriden_with_poor_logic(graph: Graph, n_id: NId) -> Iterator[NId]:
    if graph.nodes[n_id].get("modifiers_id"):
        yield from [
            child_n_id
            for child_n_id in adj_ast(
                graph,
                n_id,
                -1,
                label_type="MethodInvocation",
                expression="proceed",
            )
            if not is_in_conditional(graph, child_n_id)
        ]


def java_ignore_ssl_certificate_errors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_IGNORE_SSL_CERTIFICATE_ERRORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"android.webkit.WebViewClient"}):
            return
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("inherited_class", "") != "WebViewClient":
                continue
            for child_n_id in adj_ast(
                graph,
                n_id,
                -1,
                label_type="MethodDeclaration",
                name="onReceivedSslError",
            ):
                yield from _overriden_with_poor_logic(graph, child_n_id)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
