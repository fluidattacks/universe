from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def get_key_value_id(graph: Graph, n_id: NId) -> tuple[NId, NId]:
    return graph.nodes[n_id]["key_id"], graph.nodes[n_id]["value_id"]


def is_danger_http(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    al_id = graph.nodes[n_id].get("arguments_id")
    if al_id is None:
        return False
    for arg_id in adj_ast(graph, al_id):
        if graph.nodes[arg_id].get("label_type") != "ArrayInitializer":
            continue
        for pair_id in adj_ast(graph, arg_id, label_type="Pair"):
            val_id = graph.nodes[pair_id]["value_id"]
            return get_node_evaluation_results(method, graph, val_id, {"false"}, danger_goal=False)
    return False


def php_insecure_ssl_tls_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_SSL_TLS_HTTP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (graph.nodes[n_id].get("expression") == "Http::withOptions") and is_danger_http(
                graph,
                n_id,
                method,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
