import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.c_sharp import (
    get_first_member_syntax_graph,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_any_library_imported,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)

VULN_PATTERN = re.compile(r"(http|https)://[\*\+](.[a-zA-Z]{2,})?:\d+")


def is_http_listener_obj(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id]["label_type"] == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "HttpListener"
    )


def comes_from_http_listener(graph: Graph, n_id: NId) -> bool:
    first_member = get_first_member_syntax_graph(graph, n_id)
    return (first_member is not None) and is_node_definition_unsafe(
        graph,
        first_member,
        is_http_listener_obj,
    )


def has_unsafe_prefix_value(graph: Graph, n_id: NId) -> bool:
    first_arg = get_n_arg(graph, n_id, 0)
    return (
        first_arg is not None
        and graph.nodes[first_arg].get("label_type") == "Literal"
        and VULN_PATTERN.match(graph.nodes[first_arg].get("value", "")) is not None
    )


def c_sharp_http_listener_wildcard(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_HTTP_LISTENER_WILDCARD

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if is_any_library_imported(graph, {"System.Net"}):
            for n_id in method_supplies.selected_nodes:
                if (
                    graph.nodes[n_id].get("expression", "").endswith(".Prefixes.Add")
                    and has_unsafe_prefix_value(graph, n_id)
                    and comes_from_http_listener(graph, n_id)
                ):
                    yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
