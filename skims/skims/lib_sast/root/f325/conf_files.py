from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_attribute,
    is_parent,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def is_in_path(graph: Graph, nid: NId) -> NId | None:
    correct_parents = ["Statement"]
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    principal, principal_val, principal_id = get_attribute(graph, nid, "Principal")
    if (
        effect
        and principal
        and principal_val == "*"
        and effect_val == "Allow"
        and is_parent(graph, principal_id, correct_parents)
    ):
        return principal_id
    return None


def json_principal_wildcard(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JSON_PRINCIPAL_WILDCARD

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if report := is_in_path(graph, nid):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
