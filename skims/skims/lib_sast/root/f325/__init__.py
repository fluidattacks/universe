from lib_sast.root.f325.cloudformation import (
    cfn_bucket_policy_allows_public_access as _cfn_bckt_pol_allows_all_access,
)
from lib_sast.root.f325.cloudformation import (
    cfn_iam_has_wildcard_action_policy as _cfn_iam_has_wildcard_action_policy,
)
from lib_sast.root.f325.cloudformation import (
    cfn_iam_has_wildcard_action_trust_policy as _cfn_iam_wildcard_trust_pol,
)
from lib_sast.root.f325.cloudformation import (
    cfn_iam_has_wildcard_resource_policy as _cfn_iam_wildcard_resource_pol,
)
from lib_sast.root.f325.cloudformation import (
    cfn_iam_has_wildcard_resource_trust_policy as _cfn_iam_wildcard_res_pol,
)
from lib_sast.root.f325.cloudformation import (
    cfn_iam_permissive_policy as _cfn_iam_permissive_policy,
)
from lib_sast.root.f325.cloudformation import (
    cfn_kms_master_keys_exposed_to_everyone as _cfn_kms_master_keys_exposed,
)
from lib_sast.root.f325.cloudformation import (
    cfn_sqs_is_public as _cfn_sqs_is_public,
)
from lib_sast.root.f325.conf_files import (
    json_principal_wildcard as _json_principal_wildcard,
)
from lib_sast.root.f325.terraform import (
    tfm_bucket_policy_allows_public_access as _tfm_bucket_pol_allows_public,
)
from lib_sast.root.f325.terraform import (
    tfm_iam_has_wildcard_on_policy as _tfm_iam_has_wildcard_on_policy,
)
from lib_sast.root.f325.terraform import (
    tfm_iam_has_wildcard_on_trust_policy as _tfm_iam_wildcard_on_trust_policy,
)
from lib_sast.root.f325.terraform import (
    tfm_iam_permissive_policy as _tfm_iam_permissive_policy,
)
from lib_sast.root.f325.terraform import (
    tfm_kms_master_keys_exposed_to_everyone as _tfm_kms_master_keys_exposed,
)
from lib_sast.root.f325.terraform import (
    tfm_sqs_is_public as _tfm_sqs_is_public,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_bucket_policy_allows_public_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_bckt_pol_allows_all_access(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_has_wildcard_action_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_has_wildcard_action_policy(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_has_wildcard_action_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_wildcard_trust_pol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_has_wildcard_resource_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_wildcard_resource_pol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_has_wildcard_resource_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_wildcard_res_pol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_permissive_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_permissive_policy(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_kms_master_keys_exposed_to_everyone(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_kms_master_keys_exposed(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_sqs_is_public(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _cfn_sqs_is_public(shard, method_supplies)


@SHIELD_BLOCKING
def json_principal_wildcard(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_principal_wildcard(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_bucket_policy_allows_public_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_bucket_pol_allows_public(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_has_wildcard_on_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_has_wildcard_on_policy(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_has_wildcard_on_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_wildcard_on_trust_policy(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_permissive_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_permissive_policy(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_kms_master_keys_exposed_to_everyone(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_kms_master_keys_exposed(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_sqs_is_public(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _tfm_sqs_is_public(shard, method_supplies)
