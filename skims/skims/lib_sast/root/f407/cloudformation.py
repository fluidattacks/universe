from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _aws_ebs_volumes_unencrypted(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return
    val_id = graph.nodes[prop_id]["value_id"]
    if blocks := get_optional_attribute(graph, val_id, "BlockDeviceMappings"):
        mappings_attr = graph.nodes[blocks[2]]["value_id"]
        for c_id in adj_ast(graph, mappings_attr):
            if ebs_block := get_optional_attribute(graph, c_id, "Ebs"):
                ebs_attrs = graph.nodes[ebs_block[2]]["value_id"]
                encrypted = get_optional_attribute(graph, ebs_attrs, "Encrypted")
                if not encrypted:
                    yield ebs_block[2]
                elif encrypted[1] in FALSE_OPTIONS:
                    yield encrypted[2]


def cfn_aws_ebs_volumes_unencrypted(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_AWS_EBS_VOLUMES_UNENCRYPTED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::AutoScaling::LaunchConfiguration":
                yield from _aws_ebs_volumes_unencrypted(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
