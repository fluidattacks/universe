from lib_sast.root.f012.java.java_jpa_like import (
    java_jpa_like as _java_jpa_like,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_jpa_like(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _java_jpa_like(shard, method_supplies)
