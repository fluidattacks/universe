from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def _check_side_effect_generation(graph: Graph, dang_id: NId) -> bool:
    dang_value = graph.nodes[dang_id].get("symbol")

    for n_id in g.matching_nodes(
        graph,
        label_type="MethodInvocation",
        expression="nextBytes",
        object="java.util.Random",
    ):
        if (
            (args_id := graph.nodes[n_id].get("arguments_id"))
            and (f_arg_id := next(iter(g.adj_ast(graph, args_id)), None))
            and (graph.nodes[f_arg_id].get("symbol") == dang_value)
        ):
            return True

    return False


def _method_invocation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]

    invocation_condition = bool(
        n_attrs["expression"] == "random"
        and (obj_id := n_attrs.get("object_id"))
        and args.graph.nodes[obj_id].get("symbol") == "java.lang.Math",
    )

    side_effect_condition = bool(
        (args_id := n_attrs.get("arguments_id"))
        and (f_arg_id := next(iter(g.adj_ast(args.graph, args_id)), None))
        and (_check_side_effect_generation(args.graph, f_arg_id)),
    )
    if invocation_condition or side_effect_condition:
        args.triggers.add("weakrandom")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletRequest":
        args.triggers.add("userrequest")
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletResponse":
        args.triggers.add("userresponse")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] in {
        "java.util.Random",
        "java.lang.Math.random",
    }:
        args.triggers.add("weakrandom")

    if args.graph.nodes[args.n_id]["name"] == "HttpServletRequest":
        args.triggers.add("userrequest")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
    "method_invocation": _method_invocation_evaluator,
}


def is_weak_random_cookie(
    method: MethodsEnum,
    graph: Graph,
    obj_id: NId,
    al_id: NId,
) -> bool:
    if (
        (args_ids := g.adj_ast(graph, al_id))
        and len(args_ids) == 1
        and get_node_evaluation_results(
            method,
            graph,
            obj_id,
            {"userresponse"},
            danger_goal=False,
            method_evaluators=METHOD_EVALUATORS,
        )
    ):
        return get_node_evaluation_results(
            method,
            graph,
            args_ids[0],
            {"weakrandom"},
            danger_goal=False,
            method_evaluators=METHOD_EVALUATORS,
        )
    return False


def java_weak_random_add_cookie(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_WEAK_RANDOM

    def n_ids() -> Iterator[NId]:
        # Do not run method for owasp tests
        if method_supplies.graph_db:
            return
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            expr = n_attrs["expression"].split(".")
            if (
                expr[-1] == "addCookie"
                and (obj_id := n_attrs.get("object_id"))
                and (al_id := graph.nodes[node].get("arguments_id"))
                and is_weak_random_cookie(method, graph, obj_id, al_id)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _is_attribute_weak(
    method: MethodsEnum,
    graph: Graph,
    obj_id: NId,
    al_id: NId,
    method_supplies: MethodSupplies,
) -> bool:
    args_ids = g.adj_ast(graph, al_id)

    if len(args_ids) == 2 and get_node_evaluation_results(
        method,
        graph,
        obj_id,
        {"userrequest"},
        danger_goal=False,
        graph_db=method_supplies.graph_db,
        method_evaluators=METHOD_EVALUATORS,
    ):
        return get_node_evaluation_results(
            method,
            graph,
            args_ids[1],
            {"weakrandom"},
            danger_goal=False,
            graph_db=method_supplies.graph_db,
            method_evaluators=METHOD_EVALUATORS,
        )

    return False


def java_weak_random(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_WEAK_RANDOM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            expr = n_attrs["expression"].split(".")
            if (
                expr[-1] == "setAttribute"
                and (obj_id := n_attrs.get("object_id"))
                and graph.nodes[obj_id].get("expression") == "getSession"
                and (al_id := graph.nodes[node].get("arguments_id"))
                and _is_attribute_weak(method, graph, obj_id, al_id, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
