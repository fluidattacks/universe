from lib_sast.root.f259.cloudformation import (
    cfn_dynamo_has_not_deletion_protection as _cfn_dynamo_not_del_protect,
)
from lib_sast.root.f259.cloudformation import (
    cfn_has_not_point_in_time_recovery as _cfn_not_point_in_time_recovery,
)
from lib_sast.root.f259.terraform import (
    tfm_db_no_point_in_time_recovery as _tfm_db_no_point_recovery,
)
from lib_sast.root.f259.terraform import (
    tfm_dynamo_has_not_deletion_protection as _tfm_dynamo_not_del_protect,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_dynamo_has_not_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_dynamo_not_del_protect(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_has_not_point_in_time_recovery(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_not_point_in_time_recovery(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_db_no_point_in_time_recovery(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_db_no_point_recovery(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_dynamo_has_not_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_dynamo_not_del_protect(shard, method_supplies)
