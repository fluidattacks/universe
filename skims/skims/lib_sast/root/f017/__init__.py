from lib_sast.root.f017.c_sharp.c_sharp_jwt_signed import (
    c_sharp_jwt_signed_object_creation as _c_sharp_jwt_signed_object_creation,
)
from lib_sast.root.f017.c_sharp.c_sharp_verify_decoder import (
    c_sharp_verify_decoder as _c_sharp_verify_decoder,
)
from lib_sast.root.f017.php.php_sensitive_http_sent import (
    php_sensitive_http_sent as _php_sensitive_http_sent,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_jwt_signed_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_jwt_signed_object_creation(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_verify_decoder(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_verify_decoder(shard, method_supplies)


@SHIELD_BLOCKING
def php_sensitive_http_sent(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_sensitive_http_sent(shard, method_supplies)
