from lib_sast.root.f282.c_sharp import (
    cs_insecure_ecb_mode as _cs_insecure_ecb_mode,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cs_insecure_ecb_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cs_insecure_ecb_mode(shard, method_supplies)
