from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)

UNSAFE_CIPHERS_ALGO = {
    "SymmetricAlgorithm",
    "Aes",
    "Rijndael",
    "DES",
    "TripleDES",
    "RC2",
}


def is_node_definition_unsafe_algorithm(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("variable_type") in UNSAFE_CIPHERS_ALGO:
        return True

    if graph.nodes[n_id]["label_type"] != "SymbolLookup":
        return False

    symbol = graph.nodes[n_id]["symbol"]
    for path in get_backward_paths(graph, n_id):
        if (def_id := definition_search(graph, path, symbol)) and graph.nodes[def_id].get(
            "variable_type",
        ) in UNSAFE_CIPHERS_ALGO:
            return True
    return False


def has_insecure_ecb_mode(graph: Graph, n_id: NId) -> bool:
    if (
        graph.nodes[n_id].get("member") == "ECB"
        and graph.nodes[n_id].get("expression") == "CipherMode"
    ):
        for path in get_backward_paths(graph, n_id):
            if not path:
                continue
            if (
                graph.nodes[path[0]].get("label_type") == "Assignment"
                and (val_id := graph.nodes[path[0]].get("variable_id"))
                and graph.nodes[val_id].get("label_type") == "MemberAccess"
                and graph.nodes[val_id].get("member") == "Mode"
                and is_node_definition_unsafe_algorithm(
                    graph,
                    graph.nodes[val_id].get("expression_id"),
                )
            ):
                return True

    elif graph.nodes[n_id].get("member") in {
        "EncryptEcb",
        "DecryptEcb",
    } and is_node_definition_unsafe_algorithm(graph, graph.nodes[n_id].get("expression_id")):
        return True

    return False


def cs_insecure_ecb_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CS_INSECURE_ECB_MODE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if has_insecure_ecb_mode(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
