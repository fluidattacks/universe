from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def _is_execution_code(method: MethodsEnum, graph: Graph, n_id: NId) -> NId | None:
    if (
        (second_arg := get_n_arg(graph, n_id, 1))
        and graph.nodes[second_arg]["label_type"] == "SymbolLookup"
        and (symbol := graph.nodes[second_arg].get("symbol"))
    ):
        for path in get_backward_paths(graph, second_arg):
            if (
                (def_id := definition_search(graph, path, symbol))
                and graph.nodes[def_id]["label_type"] == "VariableDeclaration"
                and (var_value := graph.nodes[def_id].get("value_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    var_value,
                    {"source"},
                    danger_goal=False,
                )
            ):
                return var_value
    return None


def c_sharp_code_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_CODE_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (expr := graph.nodes[n_id].get("expression"))
                and str(expr).rsplit(".", 1)[-1] == "CompileAssemblyFromSource"
                and _is_execution_code(method, graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
