from lib_sast.root.f256.cloudformation import (
    cfn_rds_has_not_automated_backups as _cfn_rds_has_not_automated_backups,
)
from lib_sast.root.f256.cloudformation import (
    cfn_rds_has_not_termination_protection as _cfn_rds_not_termination_protec,
)
from lib_sast.root.f256.terraform import (
    tfm_rds_has_not_automated_backups as _tfm_rds_has_not_automated_backups,
)
from lib_sast.root.f256.terraform import (
    tfm_rds_no_deletion_protection as _tfm_rds_no_deletion_protection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_rds_has_not_automated_backups(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_has_not_automated_backups(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_rds_has_not_termination_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_not_termination_protec(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_rds_has_not_automated_backups(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_has_not_automated_backups(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_rds_no_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_no_deletion_protection(shard, method_supplies)
