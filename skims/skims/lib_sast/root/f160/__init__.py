from lib_sast.root.f160.c_sharp import (
    c_sharp_file_create_temp_file as _c_sharp_file_create_temp_file,
)
from lib_sast.root.f160.java import (
    java_file_create_temp_file as _java_file_create_temp_file,
)
from lib_sast.root.f160.python import (
    python_unsafe_temp_file as _python_unsafe_temp_file,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_file_create_temp_file(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_file_create_temp_file(shard, method_supplies)


@SHIELD_BLOCKING
def java_file_create_temp_file(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_file_create_temp_file(shard, method_supplies)


@SHIELD_BLOCKING
def python_unsafe_temp_file(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_unsafe_temp_file(shard, method_supplies)
