from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    match_ast,
    matching_nodes,
)
from model.core import (
    MethodExecutionResult,
)


def is_method_danger(
    method: MethodsEnum,
    graph: Graph,
    n_id: NId,
    imported_libs: list[str],
) -> bool:
    exp = graph.nodes[n_id]["expression"]

    if (obj := graph.nodes[n_id].get("object_id")) and (
        expr_symbol := graph.nodes[obj].get("symbol")
    ):
        imp_check = expr_symbol + "." + exp
    else:
        imp_check = exp

    is_danger_lib = any(
        imported_lib + "." + imp_check == "java.io.File.createTempFile"
        for imported_lib in imported_libs
    )

    if (  # noqa: SIM103
        is_danger_lib
        and (al_id := graph.nodes[n_id].get("arguments_id"))
        and (test_nid := match_ast(graph, al_id).get("__1__"))
        and get_node_evaluation_results(method, graph, test_nid, set())
    ):
        return True
    return False


def java_file_create_temp_file(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_FILE_CREATE_TEMP_FILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        imported_libs = [
            str(graph.nodes[node].get("expression")).rsplit(".", maxsplit=1)[0]
            for node in matching_nodes(graph, label_type="Import")
        ]
        for n_id in method_supplies.selected_nodes:
            if (
                (expr := graph.nodes[n_id].get("expression"))
                and expr.split(".")[-1] == "createTempFile"
                and is_method_danger(method, graph, n_id, imported_libs)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
