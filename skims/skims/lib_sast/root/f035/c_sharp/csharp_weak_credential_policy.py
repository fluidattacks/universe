# https://docs.microsoft.com/es-es/aspnet/core/security/authentication/identity-configuration
from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _literal_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    args.triggers.add(args.graph.nodes[args.n_id]["value"])

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "literal": _literal_evaluator,
}


def _is_danger_value(graph: Graph, n_id: NId, member_name: str) -> bool:
    method = MethodsEnum.CSHARP_WEAK_CREDENTIAL_POLICY
    insec_rules = {
        "RequireDigit": ["false"],
        "RequireNonAlphanumeric": ["false"],
        "RequireUppercase": ["false"],
        "RequireLowercase": ["false"],
        "RequiredLength": ["0", "1", "2", "3", "4", "5", "6", "7"],
        "RequiredUniqueChars": ["0", "1", "2", "3", "4", "5"],
    }
    if not insec_rules.get(member_name):
        return False

    for path in get_backward_paths(graph, n_id):
        if (
            (
                evaluation := evaluate(
                    method,
                    graph,
                    path,
                    n_id,
                    method_evaluators=METHOD_EVALUATORS,
                )
            )
            and (results := list(evaluation.triggers))
            and len(results) > 0
            and results[0] in insec_rules[member_name]
        ):
            return True
    return False


def _get_weak_policies_ids(graph: Graph, n_id: NId) -> set[NId]:
    weak_nodes: set[NId] = set()
    parent_id = pred_ast(graph, n_id)[0]
    al_id = graph.nodes[parent_id].get("arguments_id")
    opt_id = match_ast(graph, al_id).get("__0__")
    if opt_id and graph.nodes[opt_id]["label_type"] == "MethodDeclaration":
        block_id = graph.nodes[opt_id]["block_id"]
        config_options = adj_ast(graph, block_id)
        for assignment in config_options:
            arg_list = adj_ast(graph, assignment)
            if (
                len(arg_list) >= 2
                and (member_n := graph.nodes[arg_list[0]])
                and "Password" in member_n.get("expression")
                and (member := member_n.get("member"))
                and _is_danger_value(graph, arg_list[1], member)
            ):
                weak_nodes.add(arg_list[0])
    return weak_nodes


# https://docs.microsoft.com/es-es/aspnet/core/security/authentication/identity-configuration
def c_sharp_weak_credential_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_WEAK_CREDENTIAL_POLICY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        config_options = "Configure<IdentityOptions>"
        vuln_nodes: set[NId] = set()
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("member") != config_options:
                continue
            vuln_nodes.update(_get_weak_policies_ids(graph, node))

        yield from vuln_nodes

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
