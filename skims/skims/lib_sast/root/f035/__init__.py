from lib_sast.root.f035.c_sharp.csharp_weak_credential_policy import (
    c_sharp_weak_credential_policy as _c_sharp_weak_credential_policy,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_weak_credential_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_weak_credential_policy(shard, method_supplies)
