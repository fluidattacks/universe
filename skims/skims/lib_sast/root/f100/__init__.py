from lib_sast.root.f100.c_sharp import (
    c_sharp_insec_create as _c_sharp_insec_create,
)
from lib_sast.root.f100.javascript import (
    js_express_ssrf as _js_express_ssrf,
)
from lib_sast.root.f100.typescript import (
    ts_express_ssrf as _ts_express_ssrf,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insec_create(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insec_create(shard, method_supplies)


@SHIELD_BLOCKING
def js_express_ssrf(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_express_ssrf(shard, method_supplies)


@SHIELD_BLOCKING
def ts_express_ssrf(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_express_ssrf(shard, method_supplies)
