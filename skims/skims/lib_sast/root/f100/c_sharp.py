from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    match_ast_d,
)
from model.core import (
    MethodExecutionResult,
)


def c_sharp_insec_create(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSEC_CREATE
    paths = {
        "System.Net.WebRequest.Create",
        "Net.WebRequest.Create",
        "WebRequest.Create",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            expr = graph.nodes[node].get("expression")
            if (
                expr.split(".")[-1] == "Create"
                and (member := match_ast_d(graph, node, "MemberAccess"))
                and (n_attrs := graph.nodes[member])
                and f"{n_attrs['expression']}.{n_attrs['member']}" in paths
                and get_node_evaluation_results(method, graph, node, set())
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
