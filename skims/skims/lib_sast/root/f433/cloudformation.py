from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def redshift_has_encryption_disabled(graph: Graph, nid: NId) -> NId | None:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return None
    val_id = graph.nodes[prop_id]["value_id"]
    encrypted = get_optional_attribute(graph, val_id, "Encrypted")
    if not encrypted:
        return prop_id
    if encrypted[1].lower() in FALSE_OPTIONS:
        return encrypted[2]
    return None


def cfn_redshift_has_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_REDSHIFT_HAS_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::Redshift::Cluster"
                and (report := redshift_has_encryption_disabled(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
