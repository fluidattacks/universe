from lib_sast.root.f433.cloudformation import (
    cfn_redshift_has_encryption_disabled as _cfn_rds_has_encryption_disabled,
)
from lib_sast.root.f433.terraform import (
    tfm_redshift_has_encryption_disabled as _tfm_rds_has_encryption_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_redshift_has_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_has_encryption_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_redshift_has_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_has_encryption_disabled(shard, method_supplies)
