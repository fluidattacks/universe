from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
    get_optional_attribute_inside_path,
    yield_statements_from_policy,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _bucket_ss_enc_disabled(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (pol_doc := get_optional_attribute(graph, val_id, "PolicyDocument"))
    ):
        for stmt in yield_statements_from_policy(graph, graph.nodes[pol_doc[2]]["value_id"]):
            if (
                (effect := get_optional_attribute(graph, stmt, "Effect"))
                and effect[1] == "Allow"
                and (
                    ss_enc := get_optional_attribute_inside_path(
                        graph,
                        stmt,
                        [
                            "Condition",
                            "Null",
                            "s3:x-amz-server-side-encryption",
                        ],
                    )
                )
                and ss_enc[1] in FALSE_OPTIONS
            ):
                yield ss_enc[2]


def cfn_bucket_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_BUCKET_POLICY_HAS_SERVER_SIDE_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::S3::BucketPolicy":
                yield from _bucket_ss_enc_disabled(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
