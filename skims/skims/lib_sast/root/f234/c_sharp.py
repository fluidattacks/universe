from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_printing_exception(graph: Graph, n_id: NId) -> bool:
    if (
        (write_arg := match_ast(graph, n_id).get("__0__"))
        and (
            first_symbol := next(
                iter(adj_ast(graph, write_arg, -1, label_type="SymbolLookup")),
                None,
            )
        )
        and (symbol := graph.nodes[first_symbol].get("symbol"))
    ):
        for path in get_backward_paths(graph, first_symbol):
            if (
                (def_id := definition_search(graph, path, symbol))
                and graph.nodes[def_id]["label_type"] == "Parameter"
                and graph.nodes[def_id].get("variable_type") == "Exception"
            ):
                return True
    return False


def c_sharp_technical_info_leak(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_TECHNICAL_INFO_LEAK
    insecure_methods = (
        "Response.WriteLine",
        "Response.Write",
        "Response.WriteLineAsync",
        "Response.WriteAsync",
    )

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (
                (expr := graph.nodes[n_id].get("expression"))
                and str(expr).endswith(insecure_methods)
                and (args_id := graph.nodes[n_id].get("arguments_id"))
                and _is_printing_exception(graph, args_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_development_member(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id]["label_type"] == "MethodInvocation"
        and (exp_parts := graph.nodes[n_id].get("expression", "").split("."))
        and len(exp_parts) == 2
        and exp_parts[1] == "IsDevelopment"
    )


def has_safe_dev_condition(graph: Graph, n_id: NId, prev_n_id: NId) -> bool:
    if graph.nodes[prev_n_id].get("label_type", "") != "ExecutionBlock":
        return False

    true_id = graph.nodes[n_id].get("true_id", "")
    false_id = graph.nodes[n_id].get("false_id", "")
    if prev_n_id not in (true_id, false_id):
        return False

    member_is_on_true_condition_path = prev_n_id == true_id
    if not (condition_id := graph.nodes[n_id].get("condition_id")):
        return False

    for child in (*adj_ast(graph, condition_id), condition_id):
        if is_node_definition_unsafe(graph, child, is_development_member):
            is_negated = (
                (parent_id := pred_ast(graph, child)[0])
                and graph.nodes[parent_id]["label_type"] == "UnaryExpression"
                and graph.nodes[parent_id].get("operator", "") == "!"
            )
            return member_is_on_true_condition_path != is_negated

    return False


def has_development_condition(graph: Graph, n_id: NId) -> bool:
    for path in get_backward_paths(graph, n_id):
        prev_n_id = n_id
        for path_n_id in path:
            if graph.nodes[path_n_id]["label_type"] == "If":
                return has_safe_dev_condition(graph, path_n_id, prev_n_id)
            prev_n_id = path_n_id

    return False


def has_application_builder_param(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id]["label_type"] != "SymbolLookup":
        return False

    symbol = graph.nodes[n_id].get("symbol", "")
    for path in get_backward_paths(graph, n_id):
        if (
            (def_id := definition_search(graph, path, symbol))
            and graph.nodes[def_id]["label_type"] == "Parameter"
            and graph.nodes[def_id].get("variable_type", "") == "IApplicationBuilder"
        ):
            return True
    return False


def c_sharp_stacktrace_disclosure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_STACKTRACE_DISCLOSURE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("member", "") == "UseDeveloperExceptionPage"
                and not has_development_condition(graph, n_id)
                and (exp_id := graph.nodes[n_id].get("expression_id"))
                and has_application_builder_param(graph, exp_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
