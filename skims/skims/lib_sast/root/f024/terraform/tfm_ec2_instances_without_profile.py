from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _ec2_instances_without_profile(graph: Graph, nid: NId) -> NId | None:
    if not get_optional_attribute(graph, nid, "iam_instance_profile"):
        return nid
    return None


def tfm_ec2_instances_without_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EC2_INSTANCES_WITHOUT_PROFILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_instance" and (
                report := _ec2_instances_without_profile(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
