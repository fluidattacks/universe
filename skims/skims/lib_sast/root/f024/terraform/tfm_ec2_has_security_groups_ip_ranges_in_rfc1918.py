from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
    is_cidr,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _ec2_has_security_groups_ip_ranges_in_rfc1918(graph: Graph, nid: NId) -> Iterator[NId]:
    rfc1918 = {"10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"}
    cidr_block = get_optional_attribute(graph, nid, "cidr_blocks") or get_optional_attribute(
        graph,
        nid,
        "ipv6_cidr_blocks",
    )
    if not cidr_block:
        return
    cidr_vals = set(cidr_block[1] if isinstance(cidr_block[1], list) else [cidr_block[1]])
    valid_cidrs = set(filter(is_cidr, cidr_vals))
    if rfc1918.intersection(valid_cidrs):
        yield cidr_block[2]


def tfm_ec2_has_security_groups_ip_ranges_in_rfc1918(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EC2_HAS_SECURITY_GROUPS_IP_RANGES_IN_RFC1918

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_security_group",
                "aws_security_group_rule",
                "ingress",
                "egress",
            }:
                yield from _ec2_has_security_groups_ip_ranges_in_rfc1918(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
