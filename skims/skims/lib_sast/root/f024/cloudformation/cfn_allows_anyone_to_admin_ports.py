from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)
from ipaddress import (
    AddressValueError,
    IPv4Network,
    IPv6Network,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
    get_ports_cfn,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    matching_nodes,
)
from model.core import (
    MethodExecutionResult,
)


def _allows_anyone_to_admin_ports(graph: Graph, nid: NId) -> Iterator[NId]:
    admin_ports = {
        22,  # SSH
        1521,  # Oracle
        1433,  # MSSQL
        1434,  # MSSQL
        2438,  # Oracle
        3306,  # MySQL
        3389,  # RDP
        5432,  # Postgres
        6379,  # Redis
        7199,  # Cassandra
        8111,  # DAX
        8888,  # Cassandra
        9160,  # Cassandra
        11211,  # Memcached
        27017,  # MongoDB
        445,  # CIFS
    }
    unrestricted_ipv4 = IPv4Network("0.0.0.0/0")
    unrestricted_ipv6 = IPv6Network("::/0")
    unrestricted_ip = False
    ports = get_ports_cfn(graph, nid)
    if ports and (port_range := set(range(ports[0], ports[1] + 1))):
        if cidr := get_optional_attribute(graph, nid, "CidrIpv6"):
            with suppress(AddressValueError, KeyError):
                unrestricted_ip = IPv6Network(cidr[1], strict=False) == unrestricted_ipv6
        if cidr_v4 := get_optional_attribute(graph, nid, "CidrIp"):
            with suppress(AddressValueError, KeyError):
                unrestricted_ip = (
                    IPv4Network(cidr_v4[1], strict=False) == unrestricted_ipv4 or unrestricted_ip
                )
        if unrestricted_ip and admin_ports.intersection(port_range):
            yield ports[2]
            yield ports[3]


def _iterate_sec_group_traffic(
    graph: Graph,
    traffic_dir: str,
) -> Iterator[NId]:
    for nid in matching_nodes(graph, label_type="Object"):
        if (
            (resource := get_optional_attribute(graph, nid, "Type"))
            and resource[1] == traffic_dir
            and (prop := get_optional_attribute(graph, nid, "Properties"))
            and (val_id := graph.nodes[prop[2]]["value_id"])
        ):
            yield val_id


def _iterate_traffic_dir(
    graph: Graph,
    val_id: NId,
    traffic_dir: str,
    *,
    is_traffic_dir: bool,
) -> Iterator[NId]:
    if (
        is_traffic_dir
        and (traffic := get_optional_attribute(graph, val_id, traffic_dir))
        and (traffic_attrs := graph.nodes[traffic[2]]["value_id"])
    ):
        yield from adj_ast(graph, traffic_attrs)


def _iterate_ec2_egress_ingress(
    graph: Graph,
    method_supplies: MethodSupplies,
    *,
    is_ingress: bool = False,
    is_egress: bool = False,
) -> Iterator[NId]:
    for nid in method_supplies.selected_nodes:
        resource = get_optional_attribute(graph, nid, "Type")
        if (
            resource
            and resource[1] == "AWS::EC2::SecurityGroup"
            and (prop := get_optional_attribute(graph, nid, "Properties"))
        ):
            val_id = graph.nodes[prop[2]]["value_id"]
            yield from _iterate_traffic_dir(
                graph,
                val_id,
                "SecurityGroupIngress",
                is_traffic_dir=is_ingress,
            )
            yield from _iterate_traffic_dir(
                graph,
                val_id,
                "SecurityGroupEgress",
                is_traffic_dir=is_egress,
            )
    if is_ingress:
        yield from _iterate_sec_group_traffic(graph, "AWS::EC2::SecurityGroupIngress")
    if is_egress:
        yield from _iterate_sec_group_traffic(graph, "AWS::EC2::SecurityGroupEgress")


def cfn_allows_anyone_to_admin_ports(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ALLOWS_ANYONE_TO_ADMIN_PORTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in _iterate_ec2_egress_ingress(graph, method_supplies, is_ingress=True):
            yield from _allows_anyone_to_admin_ports(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
