from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f024.constants import (
    PUBLIC_CIDRS,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
    get_ports_cfn,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    matching_nodes,
)
from model.core import (
    MethodExecutionResult,
)


def _ec2_has_unrestricted_ftp_access(graph: Graph, nid: NId) -> Iterator[NId]:
    cidr = get_optional_attribute(graph, nid, "CidrIp") or get_optional_attribute(
        graph,
        nid,
        "CidrIpv6",
    )
    ports = get_ports_cfn(graph, nid)
    if cidr and cidr[1] in PUBLIC_CIDRS and ports:
        ip_prot = get_optional_attribute(graph, nid, "IpProtocol")
        if (
            ip_prot
            and ip_prot[1] in ("tcp", "-1")
            and any(ports[0] <= port <= ports[1] for port in range(20, 22))
        ):
            yield ports[2]


def _iterate_sec_group_traffic(
    graph: Graph,
    traffic_dir: str,
) -> Iterator[NId]:
    for nid in matching_nodes(graph, label_type="Object"):
        if (
            (resource := get_optional_attribute(graph, nid, "Type"))
            and resource[1] == traffic_dir
            and (prop := get_optional_attribute(graph, nid, "Properties"))
            and (val_id := graph.nodes[prop[2]]["value_id"])
        ):
            yield val_id


def _iterate_traffic_dir(
    graph: Graph,
    val_id: NId,
    traffic_dir: str,
    *,
    is_traffic_dir: bool,
) -> Iterator[NId]:
    if (
        is_traffic_dir
        and (traffic := get_optional_attribute(graph, val_id, traffic_dir))
        and (traffic_attrs := graph.nodes[traffic[2]]["value_id"])
    ):
        yield from adj_ast(graph, traffic_attrs)


def _iterate_ec2_egress_ingress(
    graph: Graph,
    method_supplies: MethodSupplies,
    *,
    is_ingress: bool = False,
    is_egress: bool = False,
) -> Iterator[NId]:
    for nid in method_supplies.selected_nodes:
        resource = get_optional_attribute(graph, nid, "Type")
        if (
            resource
            and resource[1] == "AWS::EC2::SecurityGroup"
            and (prop := get_optional_attribute(graph, nid, "Properties"))
        ):
            val_id = graph.nodes[prop[2]]["value_id"]
            yield from _iterate_traffic_dir(
                graph,
                val_id,
                "SecurityGroupIngress",
                is_traffic_dir=is_ingress,
            )
            yield from _iterate_traffic_dir(
                graph,
                val_id,
                "SecurityGroupEgress",
                is_traffic_dir=is_egress,
            )
    if is_ingress:
        yield from _iterate_sec_group_traffic(graph, "AWS::EC2::SecurityGroupIngress")
    if is_egress:
        yield from _iterate_sec_group_traffic(graph, "AWS::EC2::SecurityGroupEgress")


def cfn_ec2_has_unrestricted_ftp_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_EC2_HAS_UNRESTRICTED_FTP_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in _iterate_ec2_egress_ingress(
            graph,
            method_supplies,
            is_ingress=True,
            is_egress=True,
        ):
            yield from _ec2_has_unrestricted_ftp_access(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
