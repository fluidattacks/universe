from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    complete_attrs_on_set,
    has_string_definition,
    is_any_library_imported,
)
from lib_sast.root.utilities.java import (
    concatenate_name,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)
from model.core import (
    MethodExecutionResult,
)


def get_eval_danger(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> bool:
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id, method_supplies.graph_db)
        if evaluation and evaluation.danger:
            return True
    return False


def java_insecure_pass(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_PASS
    framework = "org.springframework.security"
    insecure_instances = complete_attrs_on_set(
        {
            f"{framework}.authentication.encoding.ShaPasswordEncoder",
            f"{framework}.authentication.encoding.Md5PasswordEncoder",
            f"{framework}.crypto.password.LdapShaPasswordEncoder",
            f"{framework}.crypto.password.Md4PasswordEncoder",
            f"{framework}.crypto.password.MessageDigestPasswordEncoder",
            f"{framework}.crypto.password.NoOpPasswordEncoder",
            f"{framework}.crypto.password.StandardPasswordEncoder",
            f"{framework}.crypto.scrypt.SCryptPasswordEncoder",
        },
    )

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node]["name"] in insecure_instances:
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_key_rsa(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_KEY_RSA
    insecure_rsa_spec = {
        "java.security.spec.RSAKeyGenParameterSpec",
        "security.spec.RSAKeyGenParameterSpec",
        "spec.RSAKeyGenParameterSpec",
        "RSAKeyGenParameterSpec",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["name"] in insecure_rsa_spec
                and (al_id := n_attrs.get("arguments_id"))
                and (param := match_ast(graph, al_id).get("__0__"))
                and get_eval_danger(graph, param, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_key_ec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_KEY_EC
    insecure_ec_spec = {
        "java.security.spec.ECGenParameterSpec",
        "security.spec.ECGenParameterSpec",
        "spec.ECGenParameterSpec",
        "ECGenParameterSpec",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["name"] in insecure_ec_spec
                and (al_id := n_attrs.get("arguments_id"))
                and (param := match_ast(graph, al_id).get("__0__"))
                and get_eval_danger(graph, param, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_key_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_KEY_SECRET
    insecure_secret_spec = {
        "javax.crypto.spec.SecretKeySpec",
        "crypto.spec.SecretKeySpec",
        "spec.SecretKeySpec",
        "SecretKeySpec",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["name"] in insecure_secret_spec
                and (al_id := n_attrs.get("arguments_id"))
                and (children := adj_ast(graph, al_id))
                and (len(children) == 2)
            ):
                alg_danger = get_eval_danger(graph, children[1], method, method_supplies)
                pass_danger = has_string_definition(graph, children[0]) is not None

                if alg_danger or pass_danger:
                    yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_jwt_without_proper_sign(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_JWT_WITHOUT_PROPER_SIGN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (n_attrs["expression"] == "compact") and (
                get_node_evaluation_results(
                    method,
                    graph,
                    node,
                    {"builder_instance"},
                    danger_goal=False,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_jwt_unsafe_decode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_JWT_UNSAFE_DECODE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        possible_names = {
            "io.jsonwebtoken.Jwts",
            "io.jsonwebtoken",
        }
        if is_any_library_imported(graph, possible_names):
            for node in method_supplies.selected_nodes:
                n_attrs = graph.nodes[node]
                if (n_attrs["expression"] == "parse") and (
                    get_node_evaluation_results(method, graph, node, {"parser"}, danger_goal=False)
                ):
                    yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_hash(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_HASH
    insecure_digests_1 = complete_attrs_on_set(
        {
            "org.apache.commons.codec.digest.DigestUtils.getMd2Digest",
            "org.apache.commons.codec.digest.DigestUtils.getMd5Digest",
            "org.apache.commons.codec.digest.DigestUtils.getShaDigest",
            "org.apache.commons.codec.digest.DigestUtils.getSha1Digest",
            "org.apache.commons.codec.digest.DigestUtils.md2",
            "org.apache.commons.codec.digest.DigestUtils.md2Hex",
            "org.apache.commons.codec.digest.DigestUtils.md5",
            "org.apache.commons.codec.digest.DigestUtils.md5Hex",
            "org.apache.commons.codec.digest.DigestUtils.sha",
            "org.apache.commons.codec.digest.DigestUtils.shaHex",
            "org.apache.commons.codec.digest.DigestUtils.sha1",
            "org.apache.commons.codec.digest.DigestUtils.sha1Hex",
            "com.google.common.hash.Hashing.adler32",
            "com.google.common.hash.Hashing.crc32",
            "com.google.common.hash.Hashing.crc32c",
            "com.google.common.hash.Hashing.goodFastHash",
            "com.google.common.hash.Hashing.hmacMd5",
            "com.google.common.hash.Hashing.hmacSha1",
            "com.google.common.hash.Hashing.md5",
            "com.google.common.hash.Hashing.sha1",
            "java.security.spec.MGF1ParameterSpec.SHA1",
        },
    )

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            method_name = concatenate_name(graph, node)
            if method_name in insecure_digests_1:
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_hash_argument(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_HASH
    insecure_digests_2 = {
        "java.security.MessageDigest.getInstance",
        "security.MessageDigest.getInstance",
        "MessageDigest.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            method_name = concatenate_name(graph, node)
            n_attrs = graph.nodes[node]
            if (
                method_name in insecure_digests_2
                and (al_id := n_attrs.get("arguments_id"))
                and (param := match_ast(graph, al_id).get("__0__"))
                and get_eval_danger(graph, param, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CIPHER
    ciphers = {
        "javax.crypto.KeyGenerator.getInstance",
        "crypto.KeyGenerator.getInstance",
        "KeyGenerator.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            method_name = concatenate_name(graph, node)
            n_attrs = graph.nodes[node]
            if (
                method_name in ciphers
                and (al_id := n_attrs.get("arguments_id"))
                and (param := match_ast(graph, al_id).get("__0__"))
                and get_eval_danger(graph, param, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_cipher_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CIPHER_MODE
    ciphers = {
        "javax.crypto.Cipher.getInstance",
        "crypto.Cipher.getInstance",
        "Cipher.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            method_name = concatenate_name(graph, node)
            n_attrs = graph.nodes[node]
            if (
                method_name in ciphers
                and (al_id := n_attrs.get("arguments_id"))
                and (param := match_ast(graph, al_id).get("__0__"))
                and get_eval_danger(graph, param, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_cipher_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CIPHER_SSL
    ssl_ciphers = {
        "javax.net.ssl.SSLContext.getInstance",
        "net.ssl.SSLContext.getInstance",
        "ssl.SSLContext.getInstance",
        "SSLContext.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            method_name = concatenate_name(graph, node)
            if (
                method_name in ssl_ciphers
                and (al_id := n_attrs.get("arguments_id"))
                and (param := match_ast(graph, al_id).get("__0__"))
                and get_eval_danger(graph, param, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_danger_protocol(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (param := match_ast(graph, al_id).get("__0__"))
        and get_node_evaluation_results(method, graph, param, {"danger_protocol"})
    ):
        return True
    return False


def java_insecure_engine_cipher_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_ENGINE_CIPHER_SSL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] == "setEnabledProtocols"
                and (obj_id := n_attrs.get("object_id"))
                and get_node_evaluation_results(method, graph, obj_id, {"ssl_engine"})
                and is_danger_protocol(method, graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_cipher_jmqi(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CIPHER_JMQI
    jmqi_ciphers = complete_attrs_on_set(
        {
            "com.ibm.mq.jmqi.JmqiUtils.toCipherSuite",
        },
    )

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            method_name = concatenate_name(graph, node)

            n_attrs = graph.nodes[node]
            if (
                method_name in jmqi_ciphers
                and (al_id := n_attrs.get("arguments_id"))
                and (param := match_ast(graph, al_id).get("__0__"))
                and get_eval_danger(graph, param, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insecure_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CONNECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            expr = graph.nodes[node].get("expression")
            if (
                expr.split(".")[-1] == "tlsVersions"
                and (args_id := graph.nodes[node].get("arguments_id"))
                and get_eval_danger(graph, args_id, method, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_insec_sign_algorithm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSEC_SIGN_ALGORITHM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                (n_attrs.get("expression") == "signWith")
                and (al_id := n_attrs.get("arguments_id"))
                and (child_id := match_ast(graph, al_id).get("__0__"))
                and (has_string_definition(graph, child_id) is not None)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_weak_rsa_key(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_WEAK_RSA_KEY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"java.security.KeyPairGenerator"}):
            return
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get(
                "expression",
                "",
            ) == "initialize" and get_node_evaluation_results(
                method,
                graph,
                n_id,
                {"size_less_than_2048", "rsa_instance"},
                danger_goal=False,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
