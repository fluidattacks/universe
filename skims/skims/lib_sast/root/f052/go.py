from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def go_insecure_hash(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.GO_INSECURE_HASH
    danger_methods = {"md4", "md5", "ripemd160", "sha1"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if n_attrs["expression"] in danger_methods and n_attrs["member"] == "New":
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def go_insecure_cipher(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.GO_INSECURE_CIPHER
    danger_methods = {"des.NewTripleDESCipher", "blowfish.NewCipher"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if n_attrs["expression"] in danger_methods:
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def comes_from_str_cast(graph: Graph, n_id: NId) -> bool:
    cast_id: NId | None = None
    label_type = graph.nodes[n_id].get("label_type")
    if label_type == "UnaryExpression":
        cast_id = n_id
    elif label_type == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol")
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and (graph.nodes[val_id].get("label_type") == "UnaryExpression")
            ):
                cast_id = val_id
    return bool(
        cast_id
        and graph.nodes[cast_id].get("operator") == "[]byte"
        and (op_id := graph.nodes[cast_id].get("operand_id"))
        and has_string_definition(graph, op_id) is not None,
    )


def go_hardcoded_symmetric_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.GO_HARDCODED_SYMMETRIC_KEY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]

            if (
                (exp := n_attrs.get("expression"))
                and (exp.split(".")[-1] == "SignedString")
                and (get_node_evaluation_results(method, graph, n_id, {"jwt"}, danger_goal=False))
                and (first_arg := get_n_arg(graph, n_id, 0))
                and (comes_from_str_cast(graph, first_arg))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
