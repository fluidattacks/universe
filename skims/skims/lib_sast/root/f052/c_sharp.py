from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
    get_vulnerabilities_from_n_ids_metadata,
)
from lib_sast.root.f052.common import (
    C_SHARP_INSECURE_CIPHERS,
    C_SHARP_INSECURE_HASH,
)
from lib_sast.root.utilities.c_sharp import (
    yield_syntax_graph_member_access,
)
from lib_sast.root.utilities.common import (
    comes_from_string,
    get_n_arg,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    get_ast_children,
    match_ast,
    match_ast_d,
    matching_nodes,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_insecure_keys(graph: Graph, n_id: str, method_supplies: MethodSupplies) -> bool:
    method = MethodsEnum.C_SHARP_INSECURE_KEYS
    n_attrs = graph.nodes[n_id]
    unsafe_method = "RSACryptoServiceProvider"

    if n_attrs["name"] == unsafe_method and is_rsa_insecure(graph, n_id):
        return True

    if (
        n_attrs["name"] in {"DSACng", "RSACng"}
        and (a_id := n_attrs.get("arguments_id"))
        and (test_nid := match_ast(graph, a_id).get("__0__"))
    ):
        return get_node_evaluation_results(
            method,
            graph,
            test_nid,
            set(),
            graph_db=method_supplies.graph_db,
        )

    return False


def get_crypto_var_names(graph: Graph) -> list[NId]:
    name_vars = []
    for var_id in matching_nodes(graph, label_type="VariableDeclaration"):
        node_var = graph.nodes[var_id]
        if node_var.get("variable_type") == "RSACryptoServiceProvider":
            name_vars.append(graph.nodes[var_id].get("variable"))
    return name_vars


def get_mode_node(
    graph: Graph,
    members: tuple[str, ...],
    identifier: str,
) -> NId | None:
    test_node = None
    for member in members:
        if graph.nodes[member].get(identifier) == "Mode":
            test_node = graph.nodes[pred_ast(graph, member)[0]].get("value_id")
    return test_node


def is_rsa_insecure(graph: Graph, n_id: NId) -> bool:
    method = MethodsEnum.C_SHARP_INSECURE_KEYS
    n_attrs = graph.nodes[n_id]
    a_id = n_attrs.get("arguments_id")

    if not a_id or (  # noqa: SIM103
        (test_nid := match_ast(graph, a_id).get("__0__"))
        and get_node_evaluation_results(method, graph, test_nid, set())
    ):
        return True
    return False


def is_managed_mode_insecure(
    graph: Graph,
    n_id: NId,
) -> NId | None:
    method = MethodsEnum.C_SHARP_MANAGED_SECURE_MODE

    if match_ast_d(graph, n_id, "ExpressionStatement"):
        props = get_ast_children(graph, n_id, "SymbolLookup", depth=3)
        test_nid = get_mode_node(graph, props, "symbol")
    else:
        parent_id = pred_ast(graph, n_id)[0]
        var_name = {graph.nodes[parent_id].get("variable")}
        members = [*yield_syntax_graph_member_access(graph, var_name)]
        test_nid = get_mode_node(graph, tuple(members), "member")

    if test_nid and get_node_evaluation_results(method, graph, test_nid, set()):
        return test_nid

    return None


def c_sharp_insecure_keys(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_KEYS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if is_insecure_keys(graph, node, method_supplies):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_rsa_secure_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_RSA_SECURE_MODE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        name_vars = get_crypto_var_names(graph)
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            parent_nid = pred_ast(graph, node)[0]
            if (
                n_attrs["expression"] in name_vars
                and n_attrs.get("member") == "Encrypt"
                and (al_id := graph.nodes[parent_nid].get("arguments_id"))
                and (test_nid := match_ast(graph, al_id).get("__1__"))
                and get_node_evaluation_results(method, graph, test_nid, set())
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_managed_secure_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    insecure_objects = {"AesManaged"}
    method = MethodsEnum.C_SHARP_MANAGED_SECURE_MODE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("name") in insecure_objects and (
                mode_nid := is_managed_mode_insecure(graph, node)
            ):
                yield mode_nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_insecure_cipher_member_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_CIPHER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("expression") in C_SHARP_INSECURE_CIPHERS:
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_insecure_cipher_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_CIPHER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("name") in C_SHARP_INSECURE_CIPHERS:
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_insecure_hash_member_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_HASH

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("expression") in C_SHARP_INSECURE_HASH:
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_insecure_hash_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_HASH

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("name") in C_SHARP_INSECURE_HASH:
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_disabled_strong_crypto(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_DISABLED_STRONG_CRYPTO
    rules = {"Switch.System.Net.DontEnableSchUseStrongCrypto", "true"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("expression") == "AppContext":
                test_nid = pred_ast(graph, node)[0]
                if graph.nodes[node]["member"] == "SetSwitch" and get_node_evaluation_results(
                    method,
                    graph,
                    test_nid,
                    rules,
                    graph_db=method_supplies.graph_db,
                ):
                    yield test_nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_obsolete_key_derivation_method_invocation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_OBSOLETE_KEY_DERIVATION
    possible_paths = {
        "System.Security.Cryptography.rfc2898DeriveBytes.CryptDeriveKey",
        "Security.Cryptography.rfc2898DeriveBytes.CryptDeriveKey",
        "Cryptography.rfc2898DeriveBytes.CryptDeriveKey",
        "rfc2898DeriveBytes.CryptDeriveKey",
    }

    def n_ids() -> Iterator[tuple[NId, dict]]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (expr := n_attrs.get("expression")) and expr in possible_paths:
                yield node, {"expression": expr}

    return get_vulnerabilities_from_n_ids_metadata(
        n_ids_metadata=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_obsolete_key_derivation_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_OBSOLETE_KEY_DERIVATION

    def n_ids() -> Iterator[tuple[NId, dict]]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if n_attrs.get("name") == "PasswordDeriveBytes":
                yield node, {"expression": n_attrs["name"]}

    return get_vulnerabilities_from_n_ids_metadata(
        n_ids_metadata=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def get_invocation_node(graph: Graph, n_id: NId) -> NId | None:
    nodes = graph.nodes
    label_type = nodes[n_id].get("label_type")

    if label_type == "MethodInvocation":
        return n_id

    if label_type == "SymbolLookup":
        symbol = nodes[n_id].get("symbol")

        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := nodes[def_id].get("value_id"))
                and (nodes[val_id].get("label_type") == "MethodInvocation")
            ):
                return val_id
    return None


def comes_from_hardcoded_str(graph: Graph, n_id: NId) -> bool:
    danger_invocations = {
        "Convert.FromBase64String",
    }
    nodes = graph.nodes

    return bool(
        (cast_fun_id := get_invocation_node(graph, n_id))
        and (exp := nodes[cast_fun_id].get("expression"))
        and ((exp.split(".")[-1] == "GetBytes") or (exp in danger_invocations))
        and (first_arg := get_n_arg(graph, cast_fun_id, 0))
        and comes_from_string(graph, first_arg) is not None,
    )


def c_sharp_hardcoded_symmetric_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_HARDCODED_SYMMETRIC_KEY
    danger_methods = {
        "Microsoft.IdentityModel.Tokens.SymmetricSecurityKey",
        "IdentityModel.Tokens.SymmetricSecurityKey",
        "Tokens.SymmetricSecurityKey",
        "SymmetricSecurityKey",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs.get("name") in danger_methods
                and (first_arg := get_n_arg(graph, node, 0))
                and comes_from_hardcoded_str(graph, first_arg)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_rsa_pkcs1_instance(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id].get("label_type") == "ObjectCreation" and graph.nodes[n_id].get(
        "name",
    ) in {
        "RSAPKCS1KeyExchangeFormatter",
        "RSAPKCS1KeyExchangeDeformatter",
    }


def uses_weak_rsa_pkcs1_class(graph: Graph, n_id: str) -> bool:
    return (
        (exp_id := graph.nodes[n_id].get("expression_id"))
        and (exp_id_child := graph.nodes[exp_id].get("expression_id"))
        and is_node_definition_unsafe(graph, exp_id_child, is_rsa_pkcs1_instance)
    )


def c_sharp_weak_rsa_encrypt_padding(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_WEAK_RSA_ENCRYPT_PADDING

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                (exp_parts := n_attrs.get("expression").split("."))
                and len(exp_parts) == 2
                and exp_parts[1] in {"CreateKeyExchange", "DecryptKeyExchange"}
                and uses_weak_rsa_pkcs1_class(graph, node)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
