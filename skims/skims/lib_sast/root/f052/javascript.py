from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f052.common import (
    insecure_create_cipher,
    insecure_ec_keypair,
    insecure_ecdh_key,
    insecure_encrypt,
    insecure_hash,
    insecure_hash_library,
    insecure_rsa_keypair,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def javascript_insecure_hash(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_INSECURE_HASH

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from insecure_hash(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def javascript_insecure_create_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_INSECURE_CREATE_CIPHER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from insecure_create_cipher(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def javascript_insecure_encrypt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_INSECURE_ENCRYPT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from insecure_encrypt(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def javascript_insecure_ecdh_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_INSECURE_ECDH_KEY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from insecure_ecdh_key(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def javascript_insecure_rsa_keypair(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_INSECURE_RSA_KEYPAIR

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from insecure_rsa_keypair(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def javascript_insecure_ec_keypair(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_INSECURE_EC_KEYPAIR

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from insecure_ec_keypair(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def javascript_insecure_hash_library(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVASCRIPT_INSECURE_HASH_LIBRARY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from insecure_hash_library(graph, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
