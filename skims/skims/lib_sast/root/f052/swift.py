from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    match_ast_d,
    match_ast_group_d,
    matching_nodes,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def get_all_imports(graph: Graph) -> list[str]:
    return [
        graph.nodes[n_id]["expression"]
        for n_id in matching_nodes(
            graph,
            label_type="Import",
        )
    ]


def cc_algorithms(graph: Graph, ident: str, n_id: NId, file_imports: list[str]) -> bool:
    if (
        ident == "CCAlgorithm"
        and "CommonCrypto" in file_imports
        and (args_id := graph.nodes[n_id].get("arguments_id"))
    ):
        arg = match_ast_d(graph, args_id, "SymbolLookup", 2)
        if graph.nodes[arg]["symbol"] == "kCCAlgorithmDES":
            return True
    return False


def swift_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.SWIFT_INSECURE_CIPHER
    danger_methods = {
        ("Blowfish", "CryptoSwift"),
        (
            ".des",
            "IDZSwiftCommonCrypto",
        ),
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        file_imports = get_all_imports(graph)
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            ident = n_attrs.get("expression") or n_attrs.get("symbol")

            if any(
                meth == ident and imp in file_imports for meth, imp in danger_methods
            ) or cc_algorithms(graph, ident, n_id, file_imports):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def uses_des_algorithm(graph: Graph, n_id: NId) -> bool:
    named_args = match_ast_group_d(graph, n_id, "NamedArgument", 2)
    for arg in named_args:
        arg_name = graph.nodes[arg]["argument_name"]
        val_id = graph.nodes[arg]["value_id"]
        if arg_name == "algorithm" and graph.nodes[val_id]["symbol"] == ".des":
            return True
    return False


def swift_insecure_crypto(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.SWIFT_INSECURE_CRYPTOR
    danger_methods = {"Cryptor"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            ident = n_attrs.get("expression")

            if ident in danger_methods and uses_des_algorithm(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def swift_insecure_cryptalgo(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    danger_methods = {"CryptAlgorithm"}
    method = MethodsEnum.SWIFT_INSECURE_CRYPTOR

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id]["member"] in danger_methods
                and (parent := pred_ast(graph, n_id)[0])
                and (var_id := graph.nodes[parent]["value_id"])
                and get_node_evaluation_results(method, graph, var_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def swift_hc_secret_jwt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    danger_methods = {"JWTSigner.hs256", "JWTSigner.hs384", "JWTSigner.hs512"}
    method = MethodsEnum.SWIFT_HC_SECRET_JWT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (
                (graph.nodes[n_id].get("expression") in danger_methods)
                and (first_arg := get_n_arg(graph, n_id, 0))
                and get_node_evaluation_results(method, graph, first_arg, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
