from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)

DANGER_ALGORITHMS = {"-cbc", "bf-"}


def _is_dangerous_algorithm(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        graph.nodes[n_id].get("label_type") == "Literal"
        and (value := graph.nodes[n_id].get("value"))
        and any(danger_cipher in value for danger_cipher in DANGER_ALGORITHMS)
    ):
        return True
    return False


def eval_danger_algo(graph: Graph, n_id: NId) -> bool:
    if _is_dangerous_algorithm(graph, n_id):
        return True
    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol")
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_dangerous_algorithm(graph, val_id)
            ):
                return True
    return False


def php_insecure_mcrypt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_MCRYPT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("expression") in {
                "mcrypt_encrypt",
                "mcrypt_decrypt",
            }:
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def php_insecure_openssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_OPENSSL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (
                (graph.nodes[n_id].get("expression") in {"openssl_encrypt", "openssl_decrypt"})
                and (scnd_arg := get_n_arg(graph, n_id, 1))
                and eval_danger_algo(graph, scnd_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
