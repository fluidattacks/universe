from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)

ELEVATED_POLICIES = {
    "PowerUserAccess",
    "IAMFullAccess",
    "AdministratorAccess",
}


def tfm_admin_policy_attached(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TERRAFORM_ADMIN_POLICY_ATTACHED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                graph.nodes[nid].get("name")
                in {
                    "aws_iam_group_policy_attachment",
                    "aws_iam_policy_attachment",
                    "aws_iam_role_policy_attachment",
                    "aws_iam_user_policy_attachment",
                }
                and (policy := get_optional_attribute(graph, nid, "policy_arn"))
                and policy[1].split("/")[-1] in ELEVATED_POLICIES
            ):
                yield policy[2]

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
