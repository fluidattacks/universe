from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    iterate_iam_policy_resources,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _iam_has_full_access_to_ssm(graph: Graph, nid: NId) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    action, action_val, action_id = get_attribute(graph, nid, "Action")
    if effect and action and effect_val == "Allow":
        action_attrs = graph.nodes[action_id]["value_id"]
        if graph.nodes[action_attrs]["label_type"] == "ArrayInitializer":
            for act in adj_ast(graph, action_attrs):
                if graph.nodes[act]["value"] == "ssm:*":
                    yield act
        elif action_val == "ssm:*":
            yield action_id


def cfn_iam_has_full_access_to_ssm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_HAS_FULL_ACCESS_TO_SSM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in iterate_iam_policy_resources(graph, method_supplies):
            yield from _iam_has_full_access_to_ssm(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
