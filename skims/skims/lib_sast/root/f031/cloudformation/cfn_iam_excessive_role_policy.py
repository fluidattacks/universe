from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_list_from_node,
    get_optional_attribute,
    iterate_iam_policy_resources,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _action_has_attach_role(
    actions: str | list,
    resources: str | list,
) -> bool:
    actions_list = actions if isinstance(actions, list) else [actions]
    resource_list = resources if isinstance(resources, list) else [resources]
    for action in actions_list:
        if action == "iam:Attach*" and any(
            res.startswith("arn:aws:iam") and ":role/" in res for res in resource_list
        ):
            return True
    return False


def _iam_excessive_role_policy(graph: Graph, stmt: NId) -> Iterator[NId]:
    if (
        (effect := get_optional_attribute(graph, stmt, "Effect"))
        and effect[1] == "Allow"
        and (action := get_optional_attribute(graph, stmt, "Action"))
        and (resource := get_optional_attribute(graph, stmt, "Resource"))
    ):
        res_list = get_list_from_node(graph, resource[2])
        action_list = get_list_from_node(graph, action[2])
        if _action_has_attach_role(action_list, res_list):
            yield stmt


def cfn_iam_excessive_role_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_EXCESSIVE_ROLE_POLICY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in iterate_iam_policy_resources(graph, method_supplies):
            yield from _iam_excessive_role_policy(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
