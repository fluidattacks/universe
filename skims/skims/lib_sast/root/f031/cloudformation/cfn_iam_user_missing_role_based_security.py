from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _iam_user_missing_role_based_security(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (properties := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[properties[2]]["value_id"])
        and (policies := get_optional_attribute(graph, val_id, "Policies"))
        and (pol_attrs_id := graph.nodes[policies[2]]["value_id"])
    ):
        for pol in adj_ast(graph, pol_attrs_id):
            if pol_name := get_optional_attribute(graph, pol, "PolicyName"):
                yield pol_name[2]


def cfn_iam_user_missing_role_based_security(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_USER_MISSING_ROLE_BASED_SECURITY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::IAM::User":
                yield from _iam_user_missing_role_based_security(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
