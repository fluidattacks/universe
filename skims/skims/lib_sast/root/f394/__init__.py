from lib_sast.root.f394.cloudformation import (
    cfn_log_files_not_validated as _cfn_log_files_not_validated,
)
from lib_sast.root.f394.terraform import (
    tfm_trail_log_files_not_validated as _tfm_trail_log_files_not_validated,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_log_files_not_validated(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_log_files_not_validated(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_trail_log_files_not_validated(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_trail_log_files_not_validated(shard, method_supplies)
