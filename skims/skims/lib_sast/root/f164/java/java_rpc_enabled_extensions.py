from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def is_true_boolean(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("value_type", "") == "bool"
        and graph.nodes[n_id].get("value", "") == "true"
    )


def is_xml_rpc_instance(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id]["label_type"] == "ObjectCreation" and graph.nodes[n_id].get(
        "name",
        "",
    ) in {"XmlRpcClientConfigImpl", "XmlRpcServerConfigImpl"}


def java_rpc_enabled_extensions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_RPC_ENABLED_EXTENSIONS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("org.apache.xmlrpc",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "setEnabledForExtensions"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and is_node_definition_unsafe(graph, first_arg, is_true_boolean)
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, obj_id, is_xml_rpc_instance)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
