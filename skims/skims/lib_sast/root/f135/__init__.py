from lib_sast.root.f135.javascript import (
    js_insecure_http_headers as _js_insecure_http_headers,
)
from lib_sast.root.f135.typescript import (
    ts_insecure_http_headers as _ts_insecure_http_headers,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_insecure_http_headers(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_insecure_http_headers(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_http_headers(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_insecure_http_headers(shard, method_supplies)
