from lib_sast.root.f085.javascript import (
    js_client_storage as _js_client_storage,
)
from lib_sast.root.f085.typescript import (
    ts_client_storage as _ts_client_storage,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_client_storage(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_client_storage(shard, method_supplies)


@SHIELD_BLOCKING
def ts_client_storage(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_client_storage(shard, method_supplies)
