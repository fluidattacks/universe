from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    match_ast,
)
from model.core import (
    MethodExecutionResult,
)


def c_sharp_xaml_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_XAML_INJECTION

    danger_methods = {
        "System.Windows.Markup.XamlReader.Load",
        "Windows.Markup.XamlReader.Load",
        "Markup.XamlReader.Load",
        "XamlReader.Load",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id]["expression"] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, {"user_connection"})
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
