from lib_sast.root.f406.cloudformation import (
    cfn_aws_efs_unencrypted as _cfn_aws_efs_unencrypted,
)
from lib_sast.root.f406.terraform import (
    tfm_aws_efs_unencrypted as _tfm_aws_efs_unencrypted,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_aws_efs_unencrypted(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_aws_efs_unencrypted(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_aws_efs_unencrypted(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_aws_efs_unencrypted(shard, method_supplies)
