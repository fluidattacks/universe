from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def check_literal_danger(graph: Graph, literal_n_id: NId, *, has_iv: bool) -> bool:
    return bool(
        (literal_val := graph.nodes[literal_n_id].get("value").lower()) == "aes-128-cbc"
        or (literal_val == "aes-256-cbc" and not has_iv),
    )


def eval_danger_args(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    c_ids = g.adj_ast(graph, n_id)

    algo_n_id = c_ids[1]
    has_iv = len(c_ids) == 5

    if (node_type := nodes[algo_n_id].get("label_type")) == "Literal" and check_literal_danger(
        graph,
        algo_n_id,
        has_iv=has_iv,
    ):
        return True

    if node_type == "SymbolLookup" and (symbol := nodes[algo_n_id].get("symbol")):
        for path in get_backward_paths(graph, algo_n_id):
            if (
                (var_n_id := definition_search(graph, path, symbol))
                and (val_n_id := nodes[var_n_id].get("value_id"))
                and nodes[val_n_id].get("label_type") == "Literal"
                and check_literal_danger(graph, val_n_id, has_iv=has_iv)
            ):
                return True

    return False


def php_insecure_encrypt_aes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_ENCRYPT_AES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        dang_invocations = {"openssl_encrypt", "openssl_decrypt"}

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs.get("expression") in dang_invocations
                and (args_n_id := n_attrs.get("arguments_id"))
                and eval_danger_args(graph, args_n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
