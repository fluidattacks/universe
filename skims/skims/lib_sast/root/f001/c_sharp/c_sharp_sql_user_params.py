from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    NET_CONNECTION_OBJECTS,
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] in NET_CONNECTION_OBJECTS:
        args.triggers.add("user_connection")
    elif args.graph.nodes[args.n_id]["name"] == "SqlParameter":
        args.triggers.add("sanitized_command")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    ma_attr = args.graph.nodes[args.n_id]

    if f"{ma_attr['expression']}.{ma_attr['member']}" == "IO.GetDBConnection":
        args.triggers.add("db_connection")
    if ma_attr["member"] in NET_SYSTEM_HTTP_METHODS:
        args.triggers.add("user_parameters")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
}


def c_sharp_sql_user_params(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_SQL_USER_PARAMS
    danger_methods = {
        "ExecuteNonQuery",
        "ExecuteScalar",
        "ExecuteOracleNonQuery",
        "ExecuteOracleScalar",
        "ExecuteNonQueryAsync",
        "ExecuteScalarAsync",
        "ExecuteReaderAsync",
    }
    danger_triggers = {"db_connection", "user_parameters", "user_connection"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                (expr := n_attrs.get("expression"))
                and expr.rsplit(".", maxsplit=1)[-1] in danger_methods
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_attrs["expression_id"],
                    danger_triggers,
                    danger_goal=False,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
