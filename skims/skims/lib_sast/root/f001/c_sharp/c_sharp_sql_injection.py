from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _param_from_get(graph: Graph, n_id: NId, param: str) -> bool:
    childs = match_ast_group_d(graph, n_id, "Attribute")
    if (  # noqa: SIM103
        len(childs) > 0
        and graph.nodes[childs[0]].get("name") == "HttpGet"
        and (attr_args := adj_ast(graph, childs[0], 2))
        and any(graph.nodes[_id].get("value") == f"{{{param}}}" for _id in attr_args)
    ):
        return True
    return False


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    var_name = args.graph.nodes[args.n_id]["variable"]
    if (
        (parents := pred_ast(args.graph, args.n_id, 2))
        and len(parents) > 1
        and args.graph.nodes[parents[1]]["label_type"] == "MethodDeclaration"
        and (mod_id := args.graph.nodes[parents[1]].get("attributes_id"))
        and _param_from_get(args.graph, mod_id, var_name)
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "parameter": _parameter_evaluator,
}


def _is_sql_injection(
    graph: Graph,
    n_id: str,
    method: MethodsEnum,
    triggers_goal: set[str],
) -> bool:
    return bool(
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (args_ids := adj_ast(graph, al_id))
        and len(args_ids) > 0
        and get_node_evaluation_results(
            method,
            graph,
            args_ids[0],
            triggers_goal,
            method_evaluators=METHOD_EVALUATORS,
        ),
    )


def c_sharp_sql_injection_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_SQL_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (expr := graph.nodes[n_id].get("expression"))
                and expr.rsplit(".", maxsplit=1)[-1] == "ExecuteSqlCommand"
                and _is_sql_injection(graph, n_id, method, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_sql_injection_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_SQL_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "SqlCommand"
                and _is_sql_injection(graph, node, method, set())
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
