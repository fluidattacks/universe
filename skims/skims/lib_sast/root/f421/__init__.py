from lib_sast.root.f421.c_sharp import (
    c_sharp_insecure_elliptic_curve as _c_sharp_insecure_elliptic_curve,
)
from lib_sast.root.f421.php import (
    php_insecure_elliptic_curve as _php_insecure_elliptic_curve,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insecure_elliptic_curve(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_elliptic_curve(shard, method_supplies)


@SHIELD_BLOCKING
def php_insecure_elliptic_curve(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_elliptic_curve(shard, method_supplies)
