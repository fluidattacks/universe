from collections.abc import (
    Iterator,
)
from itertools import chain

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_argument_iterator,
    get_optional_attribute,
    get_optional_list,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)

NETBIOS_RULE = [("*", {137, 138, 139}), ("udp", {137, 138}), ("tcp", {139})]


def _get_port_range(port_val: str) -> set[int]:
    try:
        if "-" in port_val:
            from_port, to_port = map(int, port_val.split("-"))
            return set(range(from_port, to_port + 1))
        return {int(port_val)}
    except (TypeError, ValueError):
        return set()


def _is_rule_matching(
    protocol: str,
    ports: set[int],
    rule_criteria: list[tuple[str, set[int]]],
) -> bool:
    return any(
        (allowed_protocol == "*" or protocol.lower() == allowed_protocol.lower())
        and (ports & allowed_ports)
        for allowed_protocol, allowed_ports in rule_criteria
    )


def _aws_acl_broad_network_access(graph: Graph, nid: NId) -> NId | None:
    if (
        (ingress := get_argument(graph, nid, "ingress"))
        and (attr := get_optional_attribute(graph, ingress, "cidr_block"))
        and attr[1] in {"::/0", "0.0.0.0/0"}
    ):
        return attr[2]
    return None


def _aux_azure_sa_default_network_access(graph: Graph, nid: NId) -> NId | None:
    if not (attr := get_optional_attribute(graph, nid, "default_action")):
        return nid
    if attr[1].lower() != "deny":
        return attr[2]
    return None


def _azure_sa_default_network_access(graph: Graph, nid: NId) -> NId | None:
    if graph.nodes[nid].get("name") == "azurerm_storage_account_network_rules":
        return _aux_azure_sa_default_network_access(graph, nid)

    for c_id in adj_ast(graph, nid, name="network_rules"):
        return _aux_azure_sa_default_network_access(graph, c_id)
    return None


def _azure_kv_danger_bypass(graph: Graph, nid: NId) -> NId | None:
    if network := get_argument(graph, nid, "network_acls"):
        bypass = get_optional_attribute(graph, network, "bypass")
        if not bypass:
            return nid
        if bypass[1].lower() != "azureservices":
            return bypass[2]
    return None


def _azure_kv_default_network_access(graph: Graph, nid: NId) -> NId | None:
    if network := get_argument(graph, nid, "network_acls"):
        attr = get_optional_attribute(graph, network, "default_action")
        if not attr:
            return nid
        if attr[1].lower() != "deny":
            return attr[2]
    return None


def _azure_unrestricted_access_network_segments(graph: Graph, nid: NId) -> NId | None:
    attr = get_optional_attribute(graph, nid, "public_network_enabled")
    if not attr:
        return nid
    if attr[1].lower() == "true":
        return attr[2]
    return None


def tfm_aws_acl_broad_network_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_ACL_BROAD_NETWORK_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_default_network_acl" and (
                report := _aws_acl_broad_network_access(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_kv_danger_bypass(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_KV_DANGER_BYPASS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "azurerm_key_vault" and (
                report := _azure_kv_danger_bypass(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_kv_default_network_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_KV_DEFAULT_NETWORK_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "azurerm_key_vault" and (
                report := _azure_kv_default_network_access(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_unrestricted_access_network_segments(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_UNRESTRICTED_ACCESS_NETWORK_SEGMENTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "azurerm_data_factory" and (
                report := _azure_unrestricted_access_network_segments(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_sa_default_network_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_SA_DEFAULT_NETWORK_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "azurerm_storage_account_network_rules",
                "azurerm_storage_account",
            } and (report := _azure_sa_default_network_access(graph, nid)):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_nsg_unrestricted_netbios_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_NSG_UNRESTRICTED_NETBIOS_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        def is_netbios_rule(nid: NId) -> bool:
            direction = get_optional_attribute(graph, nid, "direction")
            access = get_optional_attribute(graph, nid, "access")
            protocol = get_optional_attribute(graph, nid, "protocol")
            source_address_prefix = get_optional_attribute(graph, nid, "source_address_prefix")
            if source_address_prefix is not None:
                source_address_prefix_list = [source_address_prefix[1]]
            else:
                source_address_prefixes = get_optional_list(graph, nid, "source_address_prefixes")
                if source_address_prefixes is not None:
                    source_address_prefix_list = source_address_prefixes[1]
                else:
                    return False
            dest_port = get_optional_attribute(graph, nid, "destination_port_range")
            if dest_port is not None:
                dest_port_range_list = [dest_port[1]]
            else:
                dest_port_ranges = get_optional_list(graph, nid, "destination_port_ranges")
                if dest_port_ranges is not None:
                    dest_port_range_list = dest_port_ranges[1]
                else:
                    return False

            if not direction or not access or not protocol:
                return False
            direction_val = direction[1]
            access_val = access[1]
            protocol_val = protocol[1]
            expanded_ports = set(
                chain.from_iterable(
                    _get_port_range(port_range) for port_range in dest_port_range_list
                ),
            )
            return (
                _is_rule_matching(protocol_val, expanded_ports, NETBIOS_RULE)
                and direction_val == "Inbound"
                and access_val == "Allow"
                and any(addr in {"*", "Internet", "Any"} for addr in source_address_prefix_list)
            )

        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_network_security_group",
                method_supplies.selected_nodes,
            ),
        )
        security_rules: list[NId] = [
            attr
            for nid in resource_nids
            for attr in get_argument_iterator(graph, nid, "security_rule")
        ]
        unsafe_security_rules = [nid for nid in security_rules if is_netbios_rule(nid)]
        yield from unsafe_security_rules

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
