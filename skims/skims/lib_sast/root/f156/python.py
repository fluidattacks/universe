from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_any_library_imported,
    is_node_definition_unsafe,
    is_sanitized,
)
from lib_sast.root.utilities.python import (
    contains_user_input,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def has_unsanitized_input(graph: Graph, args_id: NId) -> bool:
    children = adj_ast(graph, args_id, -1, label_type="SymbolLookup")
    return (
        len(children) == 1
        and is_node_definition_unsafe(graph, children[0], contains_user_input)
        and not is_sanitized(graph, children[0])
    )


def python_insecure_redirect(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_INSECURE_REDIRECT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        if not is_any_library_imported(graph, {"flask.redirect"}):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "redirect"
                and (args_id := graph.nodes[n_id].get("arguments_id"))
                and has_unsanitized_input(graph, args_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
