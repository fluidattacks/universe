from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def get_vuln_nodes(
    graph: Graph,
    node: NId,
    method: MethodsEnum,
) -> bool:
    nodes = graph.nodes
    if (  # noqa: SIM103
        (parent_n_id := next(iter(g.pred_ast(graph, node)), None))
        and nodes[parent_n_id].get("label_type") == "Assignment"
        and (sibling_n_id := nodes[parent_n_id].get("value_id"))
        and get_node_evaluation_results(
            method,
            graph,
            sibling_n_id,
            {"return_true"},
            danger_goal=False,
        )
    ):
        return True

    return False


def c_sharp_cert_validation_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_CERT_VALIDATION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("expression") == "ServicePointManager"
                and graph.nodes[node].get("member") == "ServerCertificateValidationCallback"
                and get_vuln_nodes(graph, node, method)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
