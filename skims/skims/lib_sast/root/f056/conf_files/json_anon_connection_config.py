from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_key_value,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)


def _get_anon_auth(graph: Graph, children: list[NId]) -> NId | None:
    for n_id in children:
        key, value = get_key_value(graph, n_id)
        if key == "anonymousAuthentication" and value == "true":
            return n_id
    return None


def anon_connection_config(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JSON_ANON_CONNECTION_CONFIG
    if not shard.path.endswith("launchSettings.json"):
        return MethodExecutionResult(vulnerabilities=())

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            key_id = graph.nodes[nid]["key_id"]
            if (
                graph.nodes[key_id]["value"] == "iisSettings"
                and (children := match_ast_group_d(graph, graph.nodes[nid]["value_id"], "Pair"))
                and len(children) > 0
                and (anon_auth := _get_anon_auth(graph, children))
            ):
                yield anon_auth

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
