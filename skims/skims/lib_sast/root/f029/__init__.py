from lib_sast.root.f029.javascript.js_file_size_limit_missing import (
    js_file_size_limit_missing as _js_file_size_limit_missing,
)
from lib_sast.root.f029.typescript.ts_file_size_limit_missing import (
    ts_file_size_limit_missing as _ts_file_size_limit_missing,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_file_size_limit_missing(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_file_size_limit_missing(shard, method_supplies)


@SHIELD_BLOCKING
def ts_file_size_limit_missing(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_file_size_limit_missing(shard, method_supplies)
