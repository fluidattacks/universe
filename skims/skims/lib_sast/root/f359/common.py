import re

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    get_nodes_by_path,
    matching_nodes,
    pred_ast,
)


def aux_hardcoded_password(graph: Graph, n_id: NId, var_name: str) -> bool:
    n_attrs = graph.nodes[n_id]
    if n_attrs.get("name", "") == var_name:
        al_id = graph.nodes[n_id].get("arguments_id")
        if (
            al_id
            and (args_ids := adj_ast(graph, al_id))
            and len(args_ids) > 2
            and graph.nodes[args_ids[2]]["label_type"] == "Literal"
        ):
            return True
    return False


def get_var_name(graph: Graph) -> str | None:
    for n_id in matching_nodes(graph, label_type="MethodInvocation"):
        n_attrs = graph.nodes[n_id]
        if n_attrs.get("expression", "") == "require":
            al_id = graph.nodes[n_id].get("arguments_id")
            if (
                al_id
                and (args_ids := adj_ast(graph, al_id))
                and len(args_ids) == 1
                and graph.nodes[args_ids[0]].get("value") == "sequelize"
            ):
                return graph.nodes[pred_ast(graph, n_id)[0]].get("variable")
    return None


def hardcoded_password(graph: Graph, method_supplies: MethodSupplies) -> list[NId]:
    var_name = get_var_name(graph)
    if not var_name:
        return []

    return [
        n_id
        for n_id in method_supplies.selected_nodes
        if aux_hardcoded_password(graph, n_id, var_name)
    ]


def get_eval_results(method: MethodsEnum, graph: Graph, n_id: NId) -> tuple[bool, list[str]]:
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id)
        if evaluation and evaluation.danger:
            return True, list(evaluation.triggers)
    return False, []


def get_n_first_child_node(graph: Graph, n_id: NId, depth: int) -> NId | None:
    return_node = n_id
    for _ in range(depth):
        node = adj_ast(graph, return_node)
        if len(node) > 0:
            return_node = node[0]
    if graph.nodes[return_node]["label_type"] != "MethodInvocation":
        return None
    return return_node


def get_status_node(graph: Graph, n_id: NId, endpoint: str) -> NId | None:
    if endpoint.endswith("then"):
        status_node = get_n_first_child_node(graph, n_id, 2)
        if status_node and graph.nodes[status_node].get("expression", "").endswith("expect"):
            return status_node

    if endpoint.endswith("expect"):
        count = endpoint.count(".expect")
        if count == 1:
            return n_id
        status_node = get_n_first_child_node(graph, n_id, count + 1)
        if status_node and graph.nodes[status_node].get("expression", "").endswith("expect"):
            return status_node
    return None


def get_return_node(graph: Graph, n_id: NId) -> NId | None:
    return_nodes = adj_ast(graph, n_id, 4, label_type="Return")
    if len(return_nodes) > 0:
        method = adj_ast(graph, return_nodes[0], label_type="MethodInvocation")
        if len(method) == 1:
            endpoint = graph.nodes[method[0]].get("expression", "").split("post(")
            if len(endpoint) > 1 and "/login" in endpoint[1]:
                return get_status_node(graph, method[0], endpoint[-1])
    return None


def hardcoded_credentials_in_test(
    graph: Graph,
    method_supplies: MethodSupplies,
    method: MethodsEnum,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    for n_id in method_supplies.selected_nodes:
        if (
            graph.nodes[n_id].get("expression", "") in {"it", "beforeAll", "xit"}
            and (method_id := get_return_node(graph, n_id))
            and (status_node := get_n_arg(graph, method_id, 1))
            and graph.nodes[status_node].get("value", "") in {"200", "201"}
        ):
            is_dangerous, danger_list = get_eval_results(method, graph, method_id)
            if is_dangerous:
                vuln_nodes.extend(danger_list)

    return vuln_nodes


def is_jwt_and_use_member(expression: str, alias_jsonwebtoken: str) -> bool:
    splitted = expression.split(".")
    return (
        len(splitted) == 2
        and splitted[0] == alias_jsonwebtoken
        and splitted[1] in {"sign", "verify"}
    )


def second_arg_is_hardcoded(graph: Graph, n_id: NId) -> bool:
    private_key_pattern = (
        r"-----BEGIN RSA PRIVATE KEY-----\\r?\\n?\r?\n?"
        r"([a-zA-Z0-9+/=\s]+)\\r?\\n?\r?\n?-----END RSA PRIVATE KEY-----"
    )
    return bool(
        (second_arg := get_n_arg(graph, n_id, 1))
        and (string_literal := has_string_definition(graph, second_arg))
        and not re.match(private_key_pattern, string_literal),
    )


def is_pair_with_hardcoded_value(graph: Graph, n_id: NId) -> bool:
    return (
        (key_id := graph.nodes[n_id].get("key_id"))
        and graph.nodes[key_id].get("label_type") == "SymbolLookup"
        and graph.nodes[key_id].get("symbol") == "secret"
        and (val_id := graph.nodes[n_id].get("value_id"))
        and (has_string_definition(graph, val_id) is not None)
    )


def is_hardcoded_session_secret(graph: Graph, n_id: NId) -> bool:
    for arg in get_nodes_by_path(graph, n_id, (), "Object", "Pair"):
        if is_pair_with_hardcoded_value(graph, arg):
            return True
    return False


def hardcoded_session_secret_in_session_method(
    graph: Graph,
    n_id: NId,
    alias_express_session: str,
) -> bool:
    for arg in adj_ast(graph, n_id, label_type="MethodInvocation"):
        if (
            graph.nodes[arg].get("expression") == alias_express_session
            and (arg_id := graph.nodes[arg].get("arguments_id"))
            and is_hardcoded_session_secret(graph, arg_id)
        ):
            return True
    return False
