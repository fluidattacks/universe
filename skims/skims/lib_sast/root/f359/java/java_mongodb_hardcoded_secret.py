import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def is_hardcoded_secret_in_mongo_uri(uri: str) -> bool:
    pattern = r"mongodb://([^:@]+):([^:@]+)@"
    match = re.search(pattern, uri)

    return bool(
        match
        and any(
            not (credential.startswith("<") and credential.endswith(">"))
            for credential in match.groups()
        )  #  noqa: COM812
    )


def java_mongodb_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_MONGODB_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(
            graph,
            ("com.mongodb", "org.mongodb", "org.springframework.data.mongodb"),
        ):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get(
                    "name",
                    "",
                )
                == "MongoClientURI"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and (db_url := has_string_definition(graph, first_arg))
                and is_hardcoded_secret_in_mongo_uri(db_url)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
