from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def _is_hardcoded_password_in_argument(graph: Graph, n_id: NId) -> bool:
    string_n_id = (
        obj_id
        if graph.nodes[n_id].get("expression", "") == "toCharArray"
        and (obj_id := graph.nodes[n_id].get("object_id"))
        else n_id
    )
    return bool(
        (value := has_string_definition(graph, string_n_id)) and value != ""  # noqa: COM812
    )


def java_pbekeyspec_kerberos_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_PBEKEYSPEC_KERBEROS_HARDCODED_SECRET

    danger_name_to_idx_mapper = {
        "PBEKeySpec": 0,
        "KerberosKey": 1,
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        if not is_any_library_imported_prefix(
            graph,
            ("javax.crypto.spec.PBEKeySpec", "javax.security.auth.kerberos"),
        ):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                ((danger_name := graph.nodes[n_id].get("name")) in danger_name_to_idx_mapper)
                and (dang_arg := get_n_arg(graph, n_id, danger_name_to_idx_mapper[danger_name]))
                and (is_node_definition_unsafe(graph, dang_arg, _is_hardcoded_password_in_argument))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
