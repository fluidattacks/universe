import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    concatenate_name,
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model import (
    core,
)


def has_non_empty_string_definition(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "Literal"
        and graph.nodes[n_id].get("value_type", "") == "string"
        and graph.nodes[n_id].get("value", "") != ""
    )


def concatenate_string(graph: Graph, n_id: NId) -> str:
    concat_string = ""
    for child in sorted((n_id, *adj_ast(graph, n_id, -1)), key=int):
        if graph.nodes[child].get("label_type") in {"Literal", "SymbolLookup"} and (
            str_definition := has_string_definition(graph, child)
        ):
            concat_string += str_definition

    return concat_string


def has_hardcoded_password_in_uri(graph: Graph, first_arg: NId) -> bool:
    string_value = concatenate_string(graph, first_arg)
    vuln_pattern = r"(redis|rediss):\/\/(?!<.*>)([A-Za-z0-9_-]){0,31}:(?!<.*>)([^@]*)@.*"
    return re.match(vuln_pattern, string_value) is not None


def is_obj_with_jedis_hardcoded_secret_in_uri(graph: Graph, n_id: NId) -> bool:
    return bool(
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "Jedis"
        and (first_arg := get_n_arg(graph, n_id, 0))
        and has_hardcoded_password_in_uri(graph, first_arg),
    )


def is_hardcoded_secret_in_fifth_arg_of_instance_creation(graph: Graph, n_id: NId) -> bool:
    if (graph.nodes[n_id].get("name", "") == "DefaultJedisClientConfig") or (
        graph.nodes[n_id].get("expression", "") == "create"
        and (obj_id := graph.nodes[n_id].get("object_id"))
        and graph.nodes[obj_id].get("symbol", "") == "DefaultJedisClientConfig"
    ):
        return bool(
            (fifth_arg := get_n_arg(graph, n_id, 4))
            and is_node_definition_unsafe(graph, fifth_arg, has_non_empty_string_definition),
        )

    return False


def is_default_jedis_client_config_instance(graph: Graph, n_id: NId) -> bool:
    return bool(
        concatenate_name(graph, n_id).startswith(("DefaultJedisClientConfig", "JedisClientConfig")),
    )


def is_hardcoded_secret_in_password_method(graph: Graph, n_id: NId) -> bool:
    return bool(
        graph.nodes[n_id].get("expression", "") in {"password", "updatePassword"}
        and (first_arg := get_n_arg(graph, n_id, 0))
        and is_node_definition_unsafe(graph, first_arg, has_non_empty_string_definition)
        and (obj_id := graph.nodes[n_id].get("object_id"))
        and is_node_definition_unsafe(graph, obj_id, is_default_jedis_client_config_instance),
    )


def is_default_jedis_client_config_with_hardcoded_secret(graph: Graph, n_id: NId) -> bool:
    return is_hardcoded_secret_in_fifth_arg_of_instance_creation(
        graph,
        n_id,
    ) or is_hardcoded_secret_in_password_method(graph, n_id)


def is_jedis_factory_instance(graph: Graph, n_id: NId) -> bool:
    return bool(
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "JedisFactory",
    )


def is_jedis_factory_with_hardcoded_secret(graph: Graph, n_id: NId) -> bool:
    return bool(
        graph.nodes[n_id].get("expression", "") == "setPassword"
        and (first_arg := get_n_arg(graph, n_id, 0))
        and is_node_definition_unsafe(graph, first_arg, has_non_empty_string_definition)
        and (obj_id := graph.nodes[n_id].get("object_id"))
        and is_node_definition_unsafe(graph, obj_id, is_jedis_factory_instance),
    )


def is_jedis_pool_with_hardcoded_secret(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("label_type") != "ObjectCreation":
        return False

    if graph.nodes[n_id].get("name", "") == "JedisPool":
        return bool(
            (
                (fifth_arg := get_n_arg(graph, n_id, 4))
                and is_node_definition_unsafe(
                    graph,
                    fifth_arg,
                    has_non_empty_string_definition,
                )
            )
            or (
                (sixth_arg := get_n_arg(graph, n_id, 5))
                and is_node_definition_unsafe(
                    graph,
                    sixth_arg,
                    has_non_empty_string_definition,
                )
            ),
        )

    if graph.nodes[n_id].get("name", "") == "JedisSentinelPool":
        return bool(
            (fifth_arg := get_n_arg(graph, n_id, 4))
            and is_node_definition_unsafe(graph, fifth_arg, has_non_empty_string_definition),
        )

    return False


def java_jedis_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_JEDIS_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("redis",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                is_default_jedis_client_config_with_hardcoded_secret(graph, n_id)
                or is_jedis_factory_with_hardcoded_secret(graph, n_id)
                or is_jedis_pool_with_hardcoded_secret(graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
