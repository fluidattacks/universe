from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model import (
    core,
)


def danger_property_hardcoded(graph: Graph, args_n_id: NId) -> bool:
    danger_properties = {
        "javax.net.ssl.keyStorePassword",
        "javax.net.ssl.trustStorePassword",
    }

    arg_values = [
        has_string_definition(graph, n_id)
        for n_id in adj_ast(graph, args_n_id)
        if graph.nodes[n_id].get("label_type") != "Comment"
    ]

    return bool(
        (len(arg_values) == 2) and (arg_values[0] in danger_properties) and arg_values[1],
    )


def java_system_setproperty_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_SYSTEM_SETPROPERTY_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "setProperty"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and graph.nodes[obj_id].get("symbol", "") == "System"
                and (args_n_id := graph.nodes[n_id].get("arguments_id"))
                and (danger_property_hardcoded(graph, args_n_id))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
