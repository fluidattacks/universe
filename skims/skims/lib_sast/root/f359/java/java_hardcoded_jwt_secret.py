from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_any_library_imported,
)
from lib_sast.root.utilities.java import (
    concatenate_name,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def has_str_def_and_uses_jwt(graph: Graph, n_id: NId, arg: NId) -> bool:
    concat_name = concatenate_name(graph, n_id)
    return (
        (has_string_definition(graph, arg) is not None)
        and (concat_name is not None)
        and (concat_name.split(".")[0] == "Jwts")
    )


def sign_with_exp_with_hardcoded_value(graph: Graph, n_id: NId) -> bool:
    second_arg = get_n_arg(graph, n_id, 1)
    return (
        graph.nodes[n_id].get("expression") == "signWith"
        and second_arg is not None
        and has_str_def_and_uses_jwt(graph, n_id, second_arg)
    )


def set_signing_key_exp_with_hardcoded_value(graph: Graph, n_id: NId) -> bool:
    first_arg = get_n_arg(graph, n_id, 0)
    return (
        graph.nodes[n_id].get("expression") == "setSigningKey"
        and first_arg is not None
        and has_str_def_and_uses_jwt(graph, n_id, first_arg)
    )


def java_hardcoded_jwt_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_HARDCODED_JWT_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if is_any_library_imported(
            graph,
            {"org.junit.jupiter.api.Test"},
        ) or not is_any_library_imported(graph, {"io.jsonwebtoken.Jwts"}):
            return

        for n_id in method_supplies.selected_nodes:
            if sign_with_exp_with_hardcoded_value(
                graph,
                n_id,
            ) or set_signing_key_exp_with_hardcoded_value(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
