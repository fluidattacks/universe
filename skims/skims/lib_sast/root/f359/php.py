from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def eval_danger_arg(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    if (label_type := nodes[n_id].get("label_type")) == "Literal":
        return True

    if label_type == "SymbolLookup":
        symbol = nodes[n_id].get("symbol")
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := nodes[def_id].get("value_id"))
                and nodes[val_id].get("label_type") == "Literal"
            ):
                return True
    return False


def php_hardcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_HARCODED_PASSWORD

    danger_expressions = {"mysqli_connect", "mysqli"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (
                    (expression := graph.nodes[n_id].get("expression"))
                    or (expression := graph.nodes[n_id].get("name"))
                )
                and expression in danger_expressions
                and (pass_arg := get_n_arg(graph, n_id, 2))
                and eval_danger_arg(graph, pass_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
