import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.docker import (
    validate_path,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _uses_sshpass_command(graph: Graph, value_id: NId) -> bool:
    value = ""
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        for _id in adj_ast(graph, value_id):
            if graph.nodes[_id]["label_type"] != "Literal":
                return False
            value = f"{value} {graph.nodes[_id]['value']}"
    elif graph.nodes[value_id]["label_type"] == "Literal":
        value = graph.nodes[value_id]["value"]

    if value and re.match(r"^(?!#)(.*?)sshpass -p [^$]", value):  # noqa: SIM103
        return True
    return False


def docker_compose_ssh_pass(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_COMPOSE_SSH_PASS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not validate_path(shard.path):
            return
        for n_id in method_supplies.selected_nodes:
            key_id = graph.nodes[n_id]["key_id"]
            key = graph.nodes[key_id]["value"]
            if key == "command" and _uses_sshpass_command(graph, graph.nodes[n_id]["value_id"]):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
