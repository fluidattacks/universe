from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    filter_nodes,
    match_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_hardcoded_password(
    method: MethodsEnum,
    graph: Graph,
    n_id: NId,
) -> bool:
    for path in get_backward_paths(graph, n_id):
        if (
            (evaluation := evaluate(method, graph, path, n_id))
            and (conn_str := "".join(list(evaluation.triggers)))
            and any(
                argument
                for argument in conn_str.split(";")
                if len(argument.split("=")) == 2
                and argument.split("=")[0] == "Password"
                and len(argument.split("=")[1]) > 0
            )
        ):
            return True
    return False


def c_sharp_harcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_STORED_PASSWORD

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            m_expr = graph.nodes[n_id]["expression"].split(".")[-1]
            expr_id = graph.nodes[n_id]["expression_id"]
            if (
                m_expr == "UseSqlServer"
                and get_node_evaluation_results(method, graph, expr_id, set())
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (test_nid := match_ast(graph, al_id).get("__0__"))
                and is_hardcoded_password(method, graph, test_nid)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def custom_get_first_member_syntax_graph(graph: Graph, n_id: str) -> str | None:
    childs = match_ast(graph, n_id, "MemberAccess")
    searched_node = childs.get("MemberAccess")

    while searched_node:
        childs = match_ast(graph, searched_node, "MemberAccess")
        searched_node = childs.get("MemberAccess")

    first_member = childs["__0__"]
    if graph.nodes[first_member]["label_type"] == "MethodInvocation" and (first_member is not None):
        first_member = custom_get_first_member_syntax_graph(graph, first_member)

    return first_member


def method_has_dir_entry_obj(graph: Graph, n_id: NId) -> bool | None:
    if "newDirectoryEntry(" in graph.nodes[n_id].get("expression", ""):
        return True

    if not (first_member := custom_get_first_member_syntax_graph(graph, n_id)):
        return None

    return has_recursive_directory_entry_definition(graph, first_member)


def has_recursive_directory_entry_definition(graph: Graph, n_id: NId) -> bool:
    if not (
        graph.nodes[n_id]["label_type"] == "SymbolLookup"
        and (symbol := graph.nodes[n_id].get("symbol"))
    ):
        return False

    for path in get_backward_paths(graph, n_id):
        if not (
            (def_id := definition_search(graph, path, symbol))
            and (val_id := graph.nodes[def_id].get("value_id"))
        ):
            continue

        if graph.nodes[val_id]["label_type"] == "ObjectCreation":
            return graph.nodes[val_id].get("name", "") == "DirectoryEntry"

        if graph.nodes[val_id]["label_type"] == "SymbolLookup":
            return has_recursive_directory_entry_definition(graph, def_id)

        if graph.nodes[val_id]["label_type"] == "MethodInvocation" and (
            has_dir_entry := method_has_dir_entry_obj(graph, val_id)
        ):
            return has_dir_entry

    return False


def is_hardcoded_string(graph: Graph, n_id: NId) -> bool:
    secret_candidate = n_id
    if graph.nodes[n_id]["label_type"] == "ArrayInitializer":
        children_arr = adj_ast(graph, n_id, -1, label_type="Literal")
        if len(children_arr) != 1:
            return False
        secret_candidate = children_arr[0]

    str_def = has_string_definition(graph, secret_candidate)
    return (str_def is not None) and (str_def != "")


def has_directory_entry_definition(graph: Graph, n_id: NId) -> bool:
    if not (first_member := custom_get_first_member_syntax_graph(graph, n_id)):
        return False

    if graph.nodes[first_member]["label_type"] == "ObjectCreation":
        return graph.nodes[first_member].get("name", "") == "DirectoryEntry"

    return has_recursive_directory_entry_definition(graph, first_member)


def has_hardcoded_secret(graph: Graph, n_id: str) -> bool:
    if not (
        (method_invocation_id := pred_ast(graph, n_id)[0])
        and graph.nodes[method_invocation_id]["label_type"] == "MethodInvocation"
        and (args_id := graph.nodes[method_invocation_id].get("arguments_id"))
    ):
        return False

    children = adj_ast(graph, args_id)
    return (
        len(children) == 2
        and graph.nodes[children[0]]["label_type"] == "Literal"
        and graph.nodes[children[0]].get("value", "") == "SetPassword"
        and is_hardcoded_string(graph, children[1])
    )


def c_sharp_dir_entry_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_DIR_ENTRY_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("member", "") == "Invoke"
                and has_hardcoded_secret(graph, n_id)
                and has_directory_entry_definition(graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def object_creation_has_hardcoded_credential(graph: Graph, n_id: NId) -> bool:
    second_arg = get_n_arg(graph, n_id, 1)
    return (second_arg is not None) and (has_string_definition(graph, second_arg) is not None)


def object_has_var_declaration(graph: Graph, n_id: NId) -> NId | None:
    parents = pred_ast(graph, n_id)
    if len(parents) > 0 and graph.nodes[parents[0]]["label_type"] == "VariableDeclaration":
        return parents[0]

    return None


def is_symbol_definition_in_nodes(graph: Graph, n_id: NId, nodes_to_check: set[NId]) -> bool:
    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol", "")
        for path in get_backward_paths(graph, n_id):
            if (def_id := definition_search(graph, path, symbol)) and (def_id in nodes_to_check):
                return True

    return False


def has_string_assigned(graph: Graph, n_id: NId) -> bool:
    parents = pred_ast(graph, n_id)
    return (
        len(parents) > 0
        and graph.nodes[parents[0]]["label_type"] == "Assignment"
        and (val_id := graph.nodes[parents[0]].get("value_id"))
        and (has_string_definition(graph, val_id) is not None)
    )


def get_element_access_if_literal(graph: Graph, n_id: NId) -> NId:
    if (
        graph.nodes[n_id].get("label_type") == "Literal"
        and (parents := pred_ast(graph, n_id, 3))
        and len(parents) == 3
        and graph.nodes[parents[2]]["label_type"] == "ElementAccess"
    ):
        return parents[2]
    return n_id


def nodes_have_hardcoded_credential(graph: Graph, nodes_to_check: set[NId]) -> set[NId]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            ((node.get("label_type") == "MemberAccess") and (node.get("member", "") == "Password"))
            or (
                (node.get("label_type") == "Literal")
                and (node.get("value", "").lower() == "password")
            ),
        )

    vulnerable_nodes = set()
    for n_id in filter_nodes(graph, graph.nodes, predicate_matcher):
        node = get_element_access_if_literal(graph, n_id)
        if (
            has_string_assigned(graph, node)
            and (exp_id := graph.nodes[node].get("expression_id"))
            and is_symbol_definition_in_nodes(graph, exp_id, nodes_to_check)
        ):
            vulnerable_nodes.add(n_id)

    return vulnerable_nodes


def c_sharp_hardcoded_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_HARDCODED_CREDENTIALS
    sensitive_objects = {
        "NetworkCredential",
        "OracleConnectionStringBuilder",
        "PasswordAuthenticationMethod",
        "SqlConnectionStringBuilder",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        nodes_to_check = set()
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("name", "") in sensitive_objects:
                if object_creation_has_hardcoded_credential(graph, n_id):
                    yield n_id

                elif var_decla_id := object_has_var_declaration(graph, n_id):
                    nodes_to_check.add(var_decla_id)

        if nodes_to_check:
            yield from nodes_have_hardcoded_credential(graph, nodes_to_check)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
