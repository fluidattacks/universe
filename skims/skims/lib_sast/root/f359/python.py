import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    has_string_definition,
)
from lib_sast.root.utilities.python import (
    get_danger_imported_names,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    filter_nodes,
)
from model.core import (
    MethodExecutionResult,
)

CREDENTIAL_SETTINGS = {
    "EMAIL_HOST_PASSWORD",
    "API_KEY",
    "SECRET_KEY",
    "AWS_SECRET_ACCESS_KEY",
    "AWS_S3_SECRET_ACCESS_KEY",
    "SOCIAL_AUTH_GOOGLE_OAUTH_SECRET",
    "SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET",
    "SOCIAL_AUTH_GOOGLE_PLUS_SECRET",
    "AUTH_LDAP_BIND_PASSWORD",
    "SECRET_KEY_FALLBACKS",
}


CREDENTIAL_SETTINGS_IN_OBJECT = {"DATABASES", "CACHES"}


def contains_hardcoded_creds_in_str_or_arr(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("label_type") != "VariableDeclaration":
        return False

    if (  # noqa: SIM103
        graph.nodes[n_id].get("variable") in CREDENTIAL_SETTINGS
        and (val_id := graph.nodes[n_id].get("value_id"))
        and (
            graph.nodes[val_id].get("label_type") == "Literal"
            or (
                graph.nodes[val_id].get("label_type") == "ArrayInitializer"
                and any(
                    graph.nodes[child].get("label_type") == "Literal"
                    for child in adj_ast(graph, val_id)
                )
            )
        )
    ):
        return True
    return False


def contains_hardcoded_creds_in_obj(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("label_type") != "VariableDeclaration":
        return False

    if (  # noqa: SIM103
        graph.nodes[n_id].get("variable") in CREDENTIAL_SETTINGS_IN_OBJECT
        and (val_id := graph.nodes[n_id].get("value_id"))
        and (
            graph.nodes[val_id].get("label_type") == "Object"
            and any(
                (graph.nodes[child].get("label_type") == "Pair")
                and (key_id := graph.nodes[child].get("key_id"))
                and (graph.nodes[key_id].get("label_type") == "Literal")
                and (value := graph.nodes[key_id].get("value"))
                and (re.fullmatch(r"password", value, re.IGNORECASE))
                and (value_id := graph.nodes[child].get("value_id"))
                and (graph.nodes[value_id].get("label_type") == "Literal")
                for child in adj_ast(graph, val_id, -1)
            )
        )
    ):
        return True
    return False


def python_django_hardcoded_creds(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_DJANGO_HARDCODED_CREDS

    def n_ids() -> Iterator[NId]:
        if not shard.path.endswith("settings.py"):
            return
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if contains_hardcoded_creds_in_str_or_arr(
                graph,
                n_id,
            ) or contains_hardcoded_creds_in_obj(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_secret_key_config(graph: Graph, n_id: NId, flask_name: str) -> bool:
    nodes = graph.nodes
    return (
        (nodes[n_id].get("expression") == flask_name)
        and (nodes[n_id].get("member") == "secret_key")
    ) or (
        (exp_id := nodes[n_id].get("expression_id"))
        and (nodes[exp_id].get("expression") == flask_name)
        and (nodes[exp_id].get("member") == "config")
        and (args_id := nodes[n_id].get("arguments_id"))
        and (nodes[args_id].get("value") == "SECRET_KEY")
    )


def resolve_flask_name(graph: Graph, flask_imported_name: str) -> str | None:
    nodes = graph.nodes

    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            (node.get("label_type") == "VariableDeclaration")
            and (val_id := node.get("value_id"))
            and (nodes[val_id].get("expression") == flask_imported_name),
        )

    if flask_inst_n_ids := filter_nodes(graph, nodes, predicate_matcher):
        return nodes[flask_inst_n_ids[0]].get("variable")

    return None


def python_flask_hardcoded_secret_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_FLASK_HARDCODED_SECRET_KEY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        nodes = graph.nodes

        if not (
            (
                flask_name := next(
                    iter(get_danger_imported_names(graph, {"flask.Flask"})),
                    None,
                )
            )
            and (flask_var_name := resolve_flask_name(graph, flask_name.split(".")[-1]))
        ):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                (var_n_id := nodes[n_id].get("variable_id"))
                and is_secret_key_config(graph, var_n_id, flask_var_name)
                and (val_id := nodes[n_id].get("value_id"))
                and (has_string_definition(graph, val_id))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
