from lib_sast.root.f359.c_sharp import (
    c_sharp_dir_entry_hardcoded_secret as _c_sharp_dir_entry_hardcoded_secret,
)
from lib_sast.root.f359.c_sharp import (
    c_sharp_harcoded_password as _c_sharp_harcoded_password,
)
from lib_sast.root.f359.c_sharp import (
    c_sharp_hardcoded_credentials as _c_sharp_hardcoded_credentials,
)
from lib_sast.root.f359.docker_compose import (
    docker_compose_ssh_pass as _docker_compose_ssh_pass,
)
from lib_sast.root.f359.java.java_datanucleus_hardcoded_connect_password import (
    java_datanucleus_hardcoded_connect_password as _java_datanucleus_hardcoded_connect_password,
)
from lib_sast.root.f359.java.java_drivermanager_hardcoded_secret import (
    java_drivermanager_hc_secret_method as _java_drivermanager_hc_secret_method,
)
from lib_sast.root.f359.java.java_drivermanager_hardcoded_secret import (
    java_drivermanager_hc_secret_object as _java_drivermanager_hc_secret_object,
)
from lib_sast.root.f359.java.java_hardcoded_auth0_jwt_sign_key import (
    java_hardcoded_auth0_jwt_sign_key as _java_hardcoded_auth0_jwt_sign_key,
)
from lib_sast.root.f359.java.java_hardcoded_jwt_secret import (
    java_hardcoded_jwt_secret as _java_hardcoded_jwt_secret,
)
from lib_sast.root.f359.java.java_jedis_hardcoded_credentials import (
    java_jedis_hardcoded_credentials as _java_jedis_hardcoded_credentials,
)
from lib_sast.root.f359.java.java_jedis_hardcoded_secret import (
    java_jedis_hardcoded_secret as _java_jedis_hardcoded_secret,
)
from lib_sast.root.f359.java.java_jedis_hardcoded_secret_auth import (
    java_jedis_hardcoded_secret_auth as _java_jedis_hardcoded_secret_auth,
)
from lib_sast.root.f359.java.java_jsch_hardcoded_secret import (
    java_jsch_hardcoded_secret as _java_jsch_hardcoded_secret,
)
from lib_sast.root.f359.java.java_key_manager_factory_hardcoded_passwords import (
    java_key_manager_factory_hardcoded_passwords as _java_key_manager_factory_hardcoded_passwords,
)
from lib_sast.root.f359.java.java_mongodb_hardcoded_secret import (
    java_mongodb_hardcoded_secret as _java_mongodb_hardcoded_secret,
)
from lib_sast.root.f359.java.java_mysql_jdbc_hardcoded_secret import (
    java_mysql_jdbc_hardcoded_secret as _java_mysql_jdbc_hardcoded_secret,
)
from lib_sast.root.f359.java.java_okhttp_hardcoded_secret import (
    java_okhttp_hardcoded_secret as _java_okhttp_hardcoded_secret,
)
from lib_sast.root.f359.java.java_password_authentication_hardcoded_secret import (
    java_password_authentication_hardcoded_secret as _java_password_authentication_hardcoded_secret,
)
from lib_sast.root.f359.java.java_pbekeyspec_kerberos_hardcoded_secret import (
    java_pbekeyspec_kerberos_hardcoded_secret as _java_pbekeyspec_kerberos_hardcoded_secret,
)
from lib_sast.root.f359.java.java_properties_hardcoded_secret import (
    java_properties_hardcoded_secret as _java_properties_hardcoded_secret,
)
from lib_sast.root.f359.java.java_system_setproperty_hardcoded_secret import (
    java_system_setproperty_hardcoded_secret as _java_system_setproperty_hardcoded_secret,
)
from lib_sast.root.f359.javascript import (
    js_expressjs_hardcoded_sess_secret as _js_expressjs_hardcoded_sess_secret,
)
from lib_sast.root.f359.javascript import (
    js_hardcoded_credentials_in_test as _js_hardcoded_credentials_in_test,
)
from lib_sast.root.f359.javascript import (
    js_hardcoded_jwt_secret as _js_hardcoded_jwt_secret,
)
from lib_sast.root.f359.javascript import (
    js_hardcoded_password as _js_hardcoded_password,
)
from lib_sast.root.f359.php import (
    php_hardcoded_password as _php_hardcoded_password,
)
from lib_sast.root.f359.python import (
    python_django_hardcoded_creds as _python_django_hardcoded_creds,
)
from lib_sast.root.f359.python import (
    python_flask_hardcoded_secret_key as _python_flask_hardcoded_secret_key,
)
from lib_sast.root.f359.typescript import (
    ts_express_hardcoded_sess_secret as _ts_express_hardcoded_sess_secret,
)
from lib_sast.root.f359.typescript import (
    ts_hardcoded_credentials_in_test as _ts_hardcoded_credentials_in_test,
)
from lib_sast.root.f359.typescript import (
    ts_hardcoded_jwt_secret as _ts_hardcoded_jwt_secret,
)
from lib_sast.root.f359.typescript import (
    ts_hardcoded_password as _ts_hardcoded_password,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_hardcoded_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_hardcoded_credentials(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_harcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_harcoded_password(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_dir_entry_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_dir_entry_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def docker_compose_ssh_pass(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _docker_compose_ssh_pass(shard, method_supplies)


@SHIELD_BLOCKING
def php_hardcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_hardcoded_password(shard, method_supplies)


@SHIELD_BLOCKING
def java_datanucleus_hardcoded_connect_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_datanucleus_hardcoded_connect_password(shard, method_supplies)


@SHIELD_BLOCKING
def java_drivermanager_hc_secret_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_drivermanager_hc_secret_object(shard, method_supplies)


@SHIELD_BLOCKING
def java_drivermanager_hc_secret_method(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_drivermanager_hc_secret_method(shard, method_supplies)


@SHIELD_BLOCKING
def java_hardcoded_jwt_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_hardcoded_jwt_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_hardcoded_auth0_jwt_sign_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_hardcoded_auth0_jwt_sign_key(shard, method_supplies)


@SHIELD_BLOCKING
def java_jedis_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_jedis_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_jedis_hardcoded_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_jedis_hardcoded_credentials(shard, method_supplies)


@SHIELD_BLOCKING
def java_jedis_hardcoded_secret_auth(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_jedis_hardcoded_secret_auth(shard, method_supplies)


@SHIELD_BLOCKING
def java_jsch_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_jsch_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_key_manager_factory_hardcoded_passwords(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_key_manager_factory_hardcoded_passwords(shard, method_supplies)


@SHIELD_BLOCKING
def java_mongodb_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_mongodb_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_mysql_jdbc_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_mysql_jdbc_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_okhttp_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_okhttp_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_password_authentication_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_password_authentication_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_pbekeyspec_kerberos_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_pbekeyspec_kerberos_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_properties_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_properties_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_system_setproperty_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_system_setproperty_hardcoded_secret(shard, method_supplies)


@SHIELD_BLOCKING
def js_hardcoded_credentials_in_test(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_hardcoded_credentials_in_test(shard, method_supplies)


@SHIELD_BLOCKING
def js_expressjs_hardcoded_sess_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_expressjs_hardcoded_sess_secret(shard, method_supplies)


@SHIELD_BLOCKING
def js_hardcoded_jwt_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_hardcoded_jwt_secret(shard, method_supplies)


@SHIELD_BLOCKING
def js_hardcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_hardcoded_password(shard, method_supplies)


@SHIELD_BLOCKING
def python_django_hardcoded_creds(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_django_hardcoded_creds(shard, method_supplies)


@SHIELD_BLOCKING
def python_flask_hardcoded_secret_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_flask_hardcoded_secret_key(shard, method_supplies)


@SHIELD_BLOCKING
def ts_hardcoded_credentials_in_test(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_hardcoded_credentials_in_test(shard, method_supplies)


@SHIELD_BLOCKING
def ts_express_hardcoded_sess_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_express_hardcoded_sess_secret(shard, method_supplies)


@SHIELD_BLOCKING
def ts_hardcoded_jwt_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_hardcoded_jwt_secret(shard, method_supplies)


@SHIELD_BLOCKING
def ts_hardcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_hardcoded_password(shard, method_supplies)
