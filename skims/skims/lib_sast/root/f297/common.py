from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from utils.string_handlers import (
    split_on_last_dot,
)


def get_vuln_nodes(selected_nodes: list[NId], graph: Graph, method: MethodsEnum) -> list[NId]:
    if not file_imports_module(graph, "mysql") and not file_imports_module(graph, "express"):
        return []

    danger_set = {"UserConnection", "NonParametrizedQuery"}
    vuln_nodes = []
    for n_id in selected_nodes:
        f_name = split_on_last_dot(graph.nodes[n_id]["expression"])
        if (
            f_name[-1] == "query"
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (sql_query_arg := adj_ast(graph, al_id)[0])
            and get_node_evaluation_results(
                method,
                graph,
                sql_query_arg,
                danger_set,
                danger_goal=False,
            )
        ):
            vuln_nodes.append(n_id)

    return vuln_nodes
