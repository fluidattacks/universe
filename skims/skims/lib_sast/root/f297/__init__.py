from lib_sast.root.f297.go import (
    go_insecure_query as _go_insecure_query,
)
from lib_sast.root.f297.javascript import (
    js_sql_injection as _js_sql_injection,
)
from lib_sast.root.f297.python import (
    python_django_sql_injection as _python_django_sql_injection,
)
from lib_sast.root.f297.typescript import (
    ts_sql_injection as _ts_sql_injection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def go_insecure_query(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _go_insecure_query(shard, method_supplies)


@SHIELD_BLOCKING
def js_sql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_sql_injection(shard, method_supplies)


@SHIELD_BLOCKING
def python_django_sql_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_django_sql_injection(shard, method_supplies)


@SHIELD_BLOCKING
def ts_sql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_sql_injection(shard, method_supplies)
