from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def is_vulnerable_to_sql_injection(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    return bool(
        (f_arg := get_n_arg(graph, n_id, 0))
        and (symbols_n_ids_in_query := g.match_ast_group_d(graph, f_arg, "SymbolLookup", -1))
        and any(
            get_node_evaluation_results(method, graph, symbol_n_id, set())
            for symbol_n_id in symbols_n_ids_in_query
        ),
    )


def python_django_sql_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_DJANGO_SQL_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        danger_expressions = ["RawSQL", "extra", "raw"]
        for n_id in method_supplies.selected_nodes:
            if (graph.nodes[n_id].get("expression", "").split(".")[-1] in danger_expressions) and (
                is_vulnerable_to_sql_injection(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
