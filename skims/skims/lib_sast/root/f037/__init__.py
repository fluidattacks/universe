from lib_sast.root.f037.kubernetes.k8s_host_ipc_enabled import (
    k8s_host_ipc_enabled as _k8s_host_ipc_enabled,
)
from lib_sast.root.f037.kubernetes.k8s_host_network_enabled import (
    k8s_host_network_enabled as _k8s_host_network_enabled,
)
from lib_sast.root.f037.kubernetes.k8s_hostpid_enabled import (
    k8s_hostpid_enabled as _k8s_hostpid_enabled,
)
from lib_sast.root.f037.terraform.tfm_k8s_host_ipc_enabled import (
    tfm_k8s_host_ipc_enabled as _tfm_k8s_host_ipc_enabled,
)
from lib_sast.root.f037.terraform.tfm_k8s_host_network_enabled import (
    tfm_k8s_host_network_enabled as _tfm_k8s_host_network_enabled,
)
from lib_sast.root.f037.terraform.tfm_k8s_hostpid_enabled import (
    tfm_k8s_hostpid_enabled as _tfm_k8s_hostpid_enabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def k8s_hostpid_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_hostpid_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_host_network_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_host_network_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_host_ipc_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_host_ipc_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_hostpid_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_hostpid_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_host_network_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_host_network_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_host_ipc_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_host_ipc_enabled(shard, method_supplies)
