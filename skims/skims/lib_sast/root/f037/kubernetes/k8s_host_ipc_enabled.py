from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.kubernetes import (
    check_template_integrity,
    get_key_value,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)


def _get_host_namespace(graph: Graph, selected_nodes: list[NId], namespace: str) -> Iterator[NId]:
    valid_nodes = [nid for nid in selected_nodes if check_template_integrity(graph, nid)]

    yield from [
        p_id
        for node in valid_nodes
        for p_id in match_ast_group_d(graph, node, "Pair", depth=-1)
        if (key_val := get_key_value(graph, p_id))
        and key_val[0] == namespace
        and key_val[1] == "true"
    ]


def k8s_host_ipc_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        yield from _get_host_namespace(graph, method_supplies.selected_nodes, "hostIPC")

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=MethodsEnum.K8S_HOST_IPC_ENABLED,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
