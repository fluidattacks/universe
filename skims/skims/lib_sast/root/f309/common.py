from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    pred_ast,
)


def is_insecure_jwt(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
) -> bool:
    return bool(
        (p_id := next(iter(pred_ast(graph, n_id)), None))
        and (third_arg_id := get_n_arg(graph, p_id, 2))
        and get_node_evaluation_results(method, graph, third_arg_id, {"unsafealgorithm"}),
    )
