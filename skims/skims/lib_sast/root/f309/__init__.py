from lib_sast.root.f309.java import (
    java_none_alg_auth0_jwt_sign_key as _java_none_alg_auth0_jwt_sign_key,
)
from lib_sast.root.f309.javascript import (
    js_insecure_jwt_token as _js_insecure_jwt_token,
)
from lib_sast.root.f309.typescript import (
    ts_insecure_jwt_token as _ts_insecure_jwt_token,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_insecure_jwt_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_insecure_jwt_token(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_jwt_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_insecure_jwt_token(shard, method_supplies)


@SHIELD_BLOCKING
def java_none_alg_auth0_jwt_sign_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_none_alg_auth0_jwt_sign_key(shard, method_supplies)
