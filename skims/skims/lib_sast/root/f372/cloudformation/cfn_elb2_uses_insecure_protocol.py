from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _elb2_uses_http_protocol(graph: Graph, nid: NId) -> NId | None:
    properties = get_optional_attribute(graph, nid, "Properties")
    if not properties:
        return None
    val_id = graph.nodes[properties[2]]["value_id"]
    protocol = get_optional_attribute(graph, val_id, "Protocol")
    target = get_optional_attribute(graph, val_id, "TargetType")
    if protocol and protocol[1] == "HTTP" and (not target or target[1] != "lambda"):
        return protocol[2]
    return None


def cfn_elb2_uses_insecure_http_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ELB2_USES_INSECURE_PROTOCOL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::ElasticLoadBalancingV2::TargetGroup"
                and (report := _elb2_uses_http_protocol(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
