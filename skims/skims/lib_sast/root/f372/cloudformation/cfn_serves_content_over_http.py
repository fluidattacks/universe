from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _aux_serves_content_over_http(graph: Graph, dist_config_id: NId) -> Iterator[NId]:
    config_attrs = graph.nodes[dist_config_id]["value_id"]
    if (
        (def_cache := get_optional_attribute(graph, config_attrs, "DefaultCacheBehavior"))
        and (dcb_attrs := graph.nodes[def_cache[2]]["value_id"])
        and (viewer_p := get_optional_attribute(graph, dcb_attrs, "ViewerProtocolPolicy"))
        and viewer_p[1] == "allow-all"
    ):
        yield viewer_p[2]

    if cache_beh := get_optional_attribute(graph, config_attrs, "CacheBehaviors"):
        cb_attrs = graph.nodes[cache_beh[2]]["value_id"]
        for c_id in adj_ast(graph, cb_attrs):
            if (
                viewer_p := get_optional_attribute(graph, c_id, "ViewerProtocolPolicy")
            ) and viewer_p[1] == "allow-all":
                yield viewer_p[2]


def _serves_content_over_http(graph: Graph, nid: NId) -> Iterator[NId]:
    props = get_optional_attribute(graph, nid, "Properties")
    if not props:
        return
    val_id = graph.nodes[props[2]]["value_id"]
    if d_config := get_optional_attribute(graph, val_id, "DistributionConfig"):
        yield from _aux_serves_content_over_http(graph, d_config[2])


def cfn_serves_content_over_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_SERVES_CONTENT_OVER_HTTP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::CloudFront::Distribution":
                yield from _serves_content_over_http(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
