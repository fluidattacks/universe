from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _aux_serves_content_over_http(graph: Graph, nid: NId, arg: str) -> Iterator[NId]:
    if (
        (cache := get_argument(graph, nid, arg))
        and (v_prot := get_optional_attribute(graph, cache, "viewer_protocol_policy"))
        and v_prot[1] == "allow-all"
    ):
        yield v_prot[2]


def _serves_content_over_http(graph: Graph, nid: NId) -> Iterator[NId]:
    yield from _aux_serves_content_over_http(graph, nid, "default_cache_behavior")
    yield from _aux_serves_content_over_http(graph, nid, "ordered_cache_behavior")


def tfm_serves_content_over_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_SERVES_CONTENT_OVER_HTTP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_cloudfront_distribution":
                yield from _serves_content_over_http(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
