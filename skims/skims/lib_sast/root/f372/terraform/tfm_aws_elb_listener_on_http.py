from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _get_nid_http_protocol(graph: Graph, nid: NId) -> Iterator[NId]:
    for listener in get_argument_iterator(graph, nid, "listener"):
        lb_prot = get_optional_attribute(graph, listener, "lb_protocol")
        if lb_prot and lb_prot[1].lower() == "http":
            yield lb_prot[2]


def tfm_aws_elb_listener_on_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_ELB_LISTENER_ON_HTTP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_elb":
                yield from _get_nid_http_protocol(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
