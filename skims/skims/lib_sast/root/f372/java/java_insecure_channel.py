from collections.abc import Iterator
from contextlib import (
    suppress,
)
from urllib.parse import (
    ParseResult,
    urlparse,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_any_library_imported,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _is_http_remote_url(graph: Graph, n_id: NId) -> bool:
    local_hosts = {"127.0.0.1", "0.0.0.0", "localhost"}  # noqa: S104

    if url := has_string_definition(graph, n_id):
        result: ParseResult | None = None
        with suppress(ValueError):
            result = urlparse(url)

        return bool(result and result.scheme == "http" and result.netloc not in local_hosts)

    return False


def java_insecure_channel(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_CHANNEL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        if not is_any_library_imported(graph, {"android.webkit.WebView"}):
            return

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs.get("expression") == "loadUrl"
                and (f_arg := get_n_arg(graph, n_id, 0))
                and _is_http_remote_url(graph, f_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
