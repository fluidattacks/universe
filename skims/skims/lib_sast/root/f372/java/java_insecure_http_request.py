from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _method_invocation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    graph = args.graph
    n_attrs = graph.nodes[args.n_id]

    if (
        n_attrs.get("expression") == "create"
        and (obj_id := n_attrs.get("object_id"))
        and (graph.nodes[obj_id].get("symbol") == "URI")
        and _f_arg_is_http_url(graph, args.n_id)
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": _method_invocation_evaluator,
}


def _f_arg_is_http_url(graph: Graph, n_id: NId) -> bool:
    return bool(
        (f_arg := get_n_arg(graph, n_id, 0))
        and (arg_val := has_string_definition(graph, f_arg))
        and arg_val.startswith("http://")  # noqa: COM812
    )


def java_insecure_http_request(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_HTTP_REQUEST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            expression = graph.nodes[n_id].get("expression", "")

            if expression == "newBuilder" and (
                _f_arg_is_http_url(graph, n_id)
                or get_node_evaluation_results(
                    method,
                    graph,
                    n_id,
                    set(),
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

            if (
                (expression == "uri")
                and (f_arg := get_n_arg(graph, n_id, 0))
                and get_node_evaluation_results(
                    method,
                    graph,
                    f_arg,
                    set(),
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield f_arg

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
