from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    graph = args.graph

    if (
        graph.nodes[args.n_id].get("name") == "URI"
        and (f_arg := get_n_arg(graph, args.n_id, 0))
        and (arg_val := has_string_definition(graph, f_arg))
        and arg_val.startswith("http://")
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_creation": object_creation_evaluator,
}


def java_insecure_http_components(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_HTTP_COMPONENTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(
            graph,
            (
                "org.apache.http.client.methods",
                "org.apache.hc.client5.http.classic.methods",
            ),
        ):
            return

        suspicious_methods = {
            "HttpGet",
            "HttpPost",
            "HttpPut",
            "HttpDelete",
            "HttpPatch",
            "HttpOptions",
            "HttpHead",
        }
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("name", "") in suspicious_methods
                and (f_arg := get_n_arg(graph, n_id, 0))
                and (
                    (
                        (arg_val := has_string_definition(graph, f_arg))
                        and arg_val.startswith("http://")
                    )
                    or (
                        get_node_evaluation_results(
                            method,
                            graph,
                            f_arg,
                            set(),
                            method_evaluators=METHOD_EVALUATORS,
                        )
                    )
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
