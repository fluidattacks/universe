from collections.abc import Iterator

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_any_library_imported,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def is_unsafe_http_url(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id].get("value", "").lower().startswith("http://")


def ruby_http_client_requests(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.RUBY_HTTP_CLIENT_REQUESTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"httparty", "rest-client"}):
            return

        request_methods = {
            "get",
            "post",
            "put",
            "patch",
            "delete",
            "head",
            "options",
        }
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") in request_methods
                and (object_id := graph.nodes[n_id].get("object_id"))
                and graph.nodes[object_id].get("symbol", "") in {"HTTParty", "RestClient"}
                and (first_arg := get_n_arg(graph, n_id, 0))
                and is_node_definition_unsafe(graph, first_arg, is_unsafe_http_url)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
