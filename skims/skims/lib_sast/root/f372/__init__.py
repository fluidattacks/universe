from lib_sast.root.f372.cloudformation.cfn_aws_elb_listener_on_http import (
    cfn_aws_elb_listener_on_http as _cfn_aws_elb_listener_on_http,
)
from lib_sast.root.f372.cloudformation.cfn_elb2_uses_insecure_protocol import (
    cfn_elb2_uses_insecure_http_protocol as _cfn_elb2_use_insec_http_protocol,
)
from lib_sast.root.f372.cloudformation.cfn_serves_content_over_http import (
    cfn_serves_content_over_http as _cfn_serves_content_over_http,
)
from lib_sast.root.f372.conf_files.json_https_flag_missing import (
    https_flag_missing as _json_https_flag_missing,
)
from lib_sast.root.f372.java.java_insecure_channel import (
    java_insecure_channel as _java_insecure_channel,
)
from lib_sast.root.f372.java.java_insecure_http_components import (
    java_insecure_http_components as _java_insecure_http_components,
)
from lib_sast.root.f372.java.java_insecure_http_open_connection import (
    java_insecure_http_open_connection as _java_insecure_http_open_connection,
)
from lib_sast.root.f372.java.java_insecure_http_request import (
    java_insecure_http_request as _java_insecure_http_request,
)
from lib_sast.root.f372.java.java_insecure_spring_http_request import (
    java_insecure_spring_http_request as _java_insecure_spring_http_request,
)
from lib_sast.root.f372.ruby.ruby_http_client_requests import (
    ruby_http_client_requests as _ruby_http_client_requests,
)
from lib_sast.root.f372.terraform.tfm_aws_elb_listener_on_http import (
    tfm_aws_elb_listener_on_http as _tfm_aws_elb_listener_on_http,
)
from lib_sast.root.f372.terraform.tfm_azure_kv_only_accessible_over_https import (
    tfm_azure_kv_only_accessible_over_https as _tfm_kv_only_access_over_https,
)
from lib_sast.root.f372.terraform.tfm_azure_sa_insecure_transfer import (
    tfm_azure_sa_insecure_transfer as _tfm_azure_sa_insecure_transfer,
)
from lib_sast.root.f372.terraform.tfm_elb2_uses_insecure_protocol import (
    tfm_elb2_uses_insecure_http_protocol as _tfm_elb2_use_insec_http_protocol,
)
from lib_sast.root.f372.terraform.tfm_serves_content_over_http import (
    tfm_serves_content_over_http as _tfm_serves_content_over_http,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_aws_elb_listener_on_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_aws_elb_listener_on_http(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_elb2_uses_insecure_http_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_elb2_use_insec_http_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_serves_content_over_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_serves_content_over_http(shard, method_supplies)


@SHIELD_BLOCKING
def json_https_flag_missing(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_https_flag_missing(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_aws_elb_listener_on_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_aws_elb_listener_on_http(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_kv_only_accessible_over_https(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_kv_only_access_over_https(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_sa_insecure_transfer(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_sa_insecure_transfer(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_elb2_uses_insecure_http_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_elb2_use_insec_http_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_serves_content_over_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_serves_content_over_http(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_channel(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_channel(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_http_components(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_http_components(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_http_request(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_http_request(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_http_open_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_http_open_connection(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_spring_http_request(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_spring_http_request(shard, method_supplies)


@SHIELD_BLOCKING
def ruby_http_client_requests(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ruby_http_client_requests(shard, method_supplies)
