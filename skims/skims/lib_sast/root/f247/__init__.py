from lib_sast.root.f247.python import (
    python_aws_hardcoded_credentials as _python_aws_hardcoded_credentials,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def python_aws_hardcoded_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_aws_hardcoded_credentials(shard, method_supplies)
