import re
from collections.abc import (
    Iterator,
)
from re import Pattern

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_any_library_imported,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def custom_is_node_definition_unsafe(
    graph: Graph,
    n_id: NId,
    pattern: Pattern,
) -> bool:
    if is_aws_hardcoded_credentials(graph, n_id, pattern):
        return True

    if graph.nodes[n_id]["label_type"] != "SymbolLookup":
        return False

    symbol = graph.nodes[n_id]["symbol"]
    for path in get_backward_paths(graph, n_id):
        if (
            (def_id := definition_search(graph, path, symbol))
            and (val_id := graph.nodes[def_id].get("value_id"))
            and is_aws_hardcoded_credentials(graph, val_id, pattern)
        ):
            return True
    return False


def is_aws_hardcoded_credentials(graph: Graph, n_id: NId, pattern: Pattern) -> bool:
    return (
        graph.nodes[n_id]["label_type"] == "Literal"
        and graph.nodes[n_id].get("value_type", "") == "string"
        and bool(re.fullmatch(pattern, graph.nodes[n_id].get("value", "")))
    )


def python_aws_hardcoded_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_AWS_HARDCODED_CREDENTIALS
    boto3_imports = {
        "boto",
        "boto3",
        "botocore",
        "s3fs",
        "pyathena",
        "aiobotocore",
    }
    aws_cred_patterns = {
        "aws_access_key_id": re.compile(r"^A[A-Z\d]{19}$"),
        "aws_secret_access_key": re.compile(r"^[A-Za-z\d+/]{40}$"),
        "aws_session_token": re.compile(r"^[A-Za-z\d+/]{270,360}={0,2}$"),
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, boto3_imports):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                (arg_name := graph.nodes[n_id].get("argument_name"))
                and arg_name in aws_cred_patterns
                and (pattern := aws_cred_patterns[arg_name])
                and (val_id := graph.nodes[n_id].get("value_id"))
                and custom_is_node_definition_unsafe(graph, val_id, pattern)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
