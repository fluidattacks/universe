from lib_sast.root.f101.terraform import (
    tfm_azure_db_postgresql_insecure_log_retention_days as _tfm_az_db_psql_insecure_log_ret_days,
)
from lib_sast.root.f101.terraform import (
    tfm_azure_key_vault_purge_protection_disabled as _tfm_azure_key_vault_purge_protection_disabled,
)
from lib_sast.root.f101.terraform import (
    tfm_azure_storage_account_blob_soft_delete_disabled as _tfm_az_stg_acc_blob_soft_del_disabled,
)
from lib_sast.root.f101.terraform import (
    tfm_azure_storage_account_geo_replication_disabled as _tfm_az_stg_acc_geo_replication_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def tfm_azure_db_postgresql_insecure_log_retention_days(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_az_db_psql_insecure_log_ret_days(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_storage_account_geo_replication_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_az_stg_acc_geo_replication_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_storage_account_blob_soft_delete_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_az_stg_acc_blob_soft_del_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_key_vault_purge_protection_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_key_vault_purge_protection_disabled(shard, method_supplies)
