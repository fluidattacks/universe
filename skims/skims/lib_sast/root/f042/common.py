from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.f085.common import (
    get_smell_value,
    is_smell_dangerous,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)


def is_insecure_param(
    method: MethodsEnum,
    graph: Graph,
    al_id: NId,
) -> bool:
    danger_types = {"Object", "SymbolLookup"}
    rules = {"InsecureCookie"}
    args_ids = adj_ast(graph, al_id)
    for p_id in args_ids:
        if graph.nodes[p_id]["label_type"] in danger_types and get_node_evaluation_results(
            method,
            graph,
            p_id,
            rules,
        ):
            return True
    return False


def is_insecure_cookie(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []

    for n_id in method_supplies.selected_nodes:
        n_attrs = graph.nodes[n_id]
        m_name = n_attrs["expression"].split(".")[-1]
        expr_id = n_attrs["expression_id"]
        if (
            m_name == "cookie"
            and graph.nodes[expr_id]["label_type"] == "MemberAccess"
            and graph.nodes[expr_id]["expression"] in {"res", "response"}
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and is_insecure_param(method, graph, al_id)
        ):
            vuln_nodes.append(n_id)

    return vuln_nodes


def _is_sensitive_cookie(graph: Graph, n_id: NId) -> bool:
    smell_value = get_smell_value(graph, n_id)
    return bool(smell_value) and is_smell_dangerous(smell_value)


def cookie_service_sensitive_cookies(
    selected_nodes: list[NId],
    graph: Graph,
) -> Iterator[NId]:
    if not file_imports_module(graph, "ngx-cookie-service"):
        return
    for n_id in selected_nodes:
        expr = graph.nodes[n_id].get("expression")
        if (
            str(expr).lower().endswith("cookieservice.set")
            and (args_id := match_ast_d(graph, n_id, "ArgumentList"))
            and (args_nids := adj_ast(graph, args_id))
            and len(args_nids) > 1
            and _is_sensitive_cookie(graph, args_nids[1])
        ):
            yield n_id
