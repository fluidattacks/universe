from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)

DANGER_METHODS = {
    "ClientSession",
    "Request",
    "add_header",
    "add_unredirected_header",
    "urlopen",
    "make_headers",
    "delete",
    "get",
    "head",
    "patch",
    "post",
    "put",
    "request",
}


def is_danger_headers(
    method: MethodsEnum,
    graph: Graph,
    al_id: NId,
) -> bool:
    for _id in adj_ast(graph, al_id):
        if graph.nodes[_id].get("argument_name") != "headers":
            continue
        val_id = graph.nodes[_id]["value_id"]
        return get_node_evaluation_results(
            method,
            graph,
            val_id,
            {"danger_accept"},
            danger_goal=False,
        )
    return False


def python_danger_accept_header(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_ACCEPTS_ANY_MIME

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            m_attrs = graph.nodes[n_id]
            expr = str(m_attrs.get("expression"))
            if (
                expr.rsplit(".", maxsplit=1)[-1] in DANGER_METHODS
                and (al_id := m_attrs.get("arguments_id"))
                and is_danger_headers(method, graph, al_id)
                and (expr_id := m_attrs.get("expression_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    expr_id,
                    {"client_connection"},
                    danger_goal=False,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
