from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    matching_nodes,
)
from model.core import (
    MethodExecutionResult,
)


def check_danger_arguments(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    if (
        (args_n_id := nodes[n_id].get("arguments_id"))
        and (args := adj_ast(graph, args_n_id))
        and len(args) > 0
        and len(args) % 2 == 0
    ):
        for index in range(0, len(args), 2):
            if (
                nodes[args[index]].get("value") == "Accept"
                and nodes[args[index + 1]].get("value") == "*/*"
            ):
                return True
    return False


def get_vuln_nodes_plain(
    graph: Graph,
    method: MethodsEnum,
    dang_invocations: set[str],
    method_supplies: MethodSupplies,
) -> Iterator[NId]:
    for node in method_supplies.selected_nodes:
        if graph.nodes[node].get("expression") in dang_invocations and get_node_evaluation_results(
            method,
            graph,
            node,
            {"all_myme_types_allowed"},
        ):
            yield node
    for node in matching_nodes(graph, label_type="ObjectCreation", name="Header"):
        if check_danger_arguments(graph, node):
            yield node


def java_http_accepts_any_mime_type(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_HTTP_ACCEPTS_ANY_MIME_TYPE
    dang_invocations: set[str] = {
        "setRequestProperty",
        "header",
        "setHeader",
        "addHeader",
        "add",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("expression") in dang_invocations
                and check_danger_arguments(graph, node)
                and get_node_evaluation_results(method, graph, node, set())
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_http_accepts_any_mime_type_obj(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_ACCEPTS_ANY_MIME_TYPE_OBJ

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("name") == "Header" and check_danger_arguments(graph, node):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_accepts_any_mime_type_chain(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_ACCEPTS_ANY_MIME_TYPE_CHAIN
    dang_invocations: set[str] = {"header", "setHeader", "headers"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("expression") in dang_invocations
                and check_danger_arguments(graph, node)
                and get_node_evaluation_results(
                    method,
                    graph,
                    node,
                    set(),
                    graph_db=method_supplies.graph_db,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
