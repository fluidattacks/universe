import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def get_vuln_nodes(graph: Graph, n_id: NId) -> bool:
    if args_n_id := g.match_ast_d(graph, n_id, "ArgumentList"):
        args_values = set()
        for arg_n_id in g.adj(graph, args_n_id):
            if (graph.nodes[arg_n_id].get("label_type") == "Literal") and (
                val := graph.nodes[arg_n_id].get("value")
            ):
                args_values.add(val.lower())
        if args_values == {"accept", "*/*"}:
            return True
    return False


def kt_accepts_any_mime_type(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_ACCEPTS_ANY_MIME_TYPE
    method_re = re.compile(r"^\w+\.setRequestProperty$")

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (expr := graph.nodes[n_id].get("expression"))
                and method_re.match(expr)
                and get_vuln_nodes(graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
