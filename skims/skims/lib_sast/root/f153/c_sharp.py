from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def check_danger_arguments(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    if (
        (args_n_id := nodes[n_id].get("arguments_id"))
        and (args := g.adj_ast(graph, args_n_id))
        and (len(args) > 0 and len(args) % 2 == 0)
    ):
        for index in range(0, len(args), 2):
            if (nodes[args[index]].get("value") == "Accept") and (
                nodes[args[index + 1]].get("value") == "*/*"
            ):
                return True
    return False


def get_dang_instances(graph: Graph) -> set[str]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            (
                (node.get("label_type") == "VariableDeclaration")
                and (node.get("variable_type") in dang_classes)
            )
            or (
                (node.get("label_type") == "ObjectCreation") and (node.get("name") in dang_classes)
            ),
        )

    dang_instances: set[str] = set()
    dang_classes = {"HttpClient", "HttpRequestMessage", "WebClient"}

    for n_id in g.filter_nodes(graph, graph.nodes, predicate_matcher):
        if (var_name := graph.nodes[n_id].get("variable")) or (
            (p_id := g.pred_ast(graph, n_id))
            and (v_id := graph.nodes[p_id[0]].get("variable_id"))
            and (var_name := graph.nodes[v_id].get("symbol"))
        ):
            dang_instances.add(var_name)

    return dang_instances


def get_dang_callings(graph: Graph) -> set[str]:
    dang_callings: set[str] = set()
    dang_invocations = {
        "DefaultRequestHeaders.Add",
        "DefaultRequestHeaders.Accept.Add",
        "Headers.Add",
        "Headers.Accept.Add",
    }
    for invocation in dang_invocations:
        for inst_name in get_dang_instances(graph):
            dang_callings.add(f"{inst_name}.{invocation}")

    return dang_callings


def c_sharp_accepts_any_mime_type(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_ACCEPTS_ANY_MIME_TYPE_CHAIN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        dang_callings = get_dang_callings(graph)
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("expression") in dang_callings
                and (args_id := graph.nodes[node].get("arguments_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    args_id,
                    set(),
                    graph_db=method_supplies.graph_db,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
