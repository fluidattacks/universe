from lib_sast.root.f165.cloudformation import (
    cfn_eks_has_endpoints_publicly_accessible as _cfn_eks_endpoints_access,
)
from lib_sast.root.f165.cloudformation import (
    cfn_elasticache_is_transit_encryption_disabled as _cfn_aec_not_encryption,
)
from lib_sast.root.f165.cloudformation import (
    cfn_elasticache_uses_default_port as _cfn_elasticache_uses_default_port,
)
from lib_sast.root.f165.cloudformation import (
    cfn_iam_allow_not_action_perms_policies as _cfn_iam_not_action_policies,
)
from lib_sast.root.f165.cloudformation import (
    cfn_iam_allow_not_actions_trust_policy as _cfn_iam_not_actions_trust_pol,
)
from lib_sast.root.f165.cloudformation import (
    cfn_iam_allow_not_principal_trust_policy as _cfn_iam_not_prin_trust_pol,
)
from lib_sast.root.f165.cloudformation import (
    cfn_iam_allow_not_resource_perms_policies as _cfn_iam_not_res_perms_pol,
)
from lib_sast.root.f165.cloudformation import (
    cfn_iam_is_policy_applying_to_users as _cfn_iam_policy_applying_to_users,
)
from lib_sast.root.f165.cloudformation import (
    cfn_rds_not_uses_iam_authentication as _cfn_rds_not_iam_authentication,
)
from lib_sast.root.f165.cloudformation import (
    cfn_redshift_has_public_clusters as _cfn_redshift_has_public_clusters,
)
from lib_sast.root.f165.cloudformation import (
    cfn_redshift_not_requires_ssl as _cfn_redshift_not_requires_ssl,
)
from lib_sast.root.f165.cloudformation import (
    cfn_sns_is_server_side_encryption_disabled as _cfn_sns_is_sse_disabled,
)
from lib_sast.root.f165.cloudformation import (
    cfn_sqs_is_encryption_disabled as _cfn_sqs_is_encryption_disabled,
)
from lib_sast.root.f165.terraform import (
    tfm_eks_has_endpoints_publicly_accessible as _tfm_eks_endpoints_access,
)
from lib_sast.root.f165.terraform import (
    tfm_elasticache_is_transit_encryption_disabled as _tfm_aec_not_encryption,
)
from lib_sast.root.f165.terraform import (
    tfm_elasticache_uses_default_port as _tfm_elasticache_uses_default_port,
)
from lib_sast.root.f165.terraform import (
    tfm_iam_allow_not_action_perms_policies as _tfm_iam_not_action_policies,
)
from lib_sast.root.f165.terraform import (
    tfm_iam_allow_not_actions_trust_policy as _tfm_iam_not_actions_trust_pol,
)
from lib_sast.root.f165.terraform import (
    tfm_iam_allow_not_principal_trust_policy as _tfm_iam_not_prin_trust_pol,
)
from lib_sast.root.f165.terraform import (
    tfm_iam_allow_not_resource_perms_policies as _tfm_iam_not_res_perms_pol,
)
from lib_sast.root.f165.terraform import (
    tfm_iam_is_policy_applying_to_users as _tfm_iam_policy_applying_to_users,
)
from lib_sast.root.f165.terraform import (
    tfm_rds_not_uses_iam_authentication as _tfm_rds_not_iam_authentication,
)
from lib_sast.root.f165.terraform import (
    tfm_redshift_has_public_clusters as _tfm_redshift_has_public_clusters,
)
from lib_sast.root.f165.terraform import (
    tfm_redshift_not_requires_ssl as _tfm_redshift_not_requires_ssl,
)
from lib_sast.root.f165.terraform import (
    tfm_sns_is_server_side_encryption_disabled as _tfm_sns_is_sse_disabled,
)
from lib_sast.root.f165.terraform import (
    tfm_sqs_is_encryption_disabled as _tfm_sqs_is_encryption_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_eks_has_endpoints_publicly_accessible(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_eks_endpoints_access(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_elasticache_is_transit_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_aec_not_encryption(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_elasticache_uses_default_port(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_elasticache_uses_default_port(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_allow_not_action_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_not_action_policies(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_allow_not_actions_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_not_actions_trust_pol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_allow_not_principal_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_not_prin_trust_pol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_allow_not_resource_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_not_res_perms_pol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_is_policy_applying_to_users(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_policy_applying_to_users(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_rds_not_uses_iam_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_not_iam_authentication(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_redshift_has_public_clusters(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_redshift_has_public_clusters(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_redshift_not_requires_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_redshift_not_requires_ssl(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_sns_is_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_sns_is_sse_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_sqs_is_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_sqs_is_encryption_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_eks_has_endpoints_publicly_accessible(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_eks_endpoints_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_elasticache_is_transit_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_aec_not_encryption(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_elasticache_uses_default_port(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_elasticache_uses_default_port(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_allow_not_action_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_not_action_policies(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_allow_not_actions_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_not_actions_trust_pol(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_allow_not_principal_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_not_prin_trust_pol(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_allow_not_resource_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_not_res_perms_pol(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_is_policy_applying_to_users(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_policy_applying_to_users(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_rds_not_uses_iam_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_not_iam_authentication(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_redshift_has_public_clusters(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_redshift_has_public_clusters(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_redshift_not_requires_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_redshift_not_requires_ssl(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_sns_is_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_sns_is_sse_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_sqs_is_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_sqs_is_encryption_disabled(shard, method_supplies)
