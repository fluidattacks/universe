from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_argument_iterator,
    get_dict_from_attr,
    get_dict_values,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)


def _iam_role_is_over_privileged_in_jsonencode(
    graph: Graph,
    nid: NId,
    danger_attr: str,
) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if stmt := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[stmt[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            if (
                (effect := get_optional_attribute(graph, c_id, "Effect"))
                and effect[1] == "Allow"
                and (not_principal := get_optional_attribute(graph, c_id, danger_attr))
            ):
                yield not_principal[2]


def _iam_role_is_over_privileged_in_literal(policy_dict: str, danger_attr: str) -> bool:
    dict_value = get_dict_from_attr(policy_dict)
    statements = get_dict_values(dict_value, "Statement")
    if isinstance(statements, list):
        for stmt in statements:
            if stmt.get("Effect") == "Allow" and stmt.get(danger_attr):
                return True
    return False


def _iam_role_is_over_privileged(graph: Graph, nid: NId, danger_attr: str) -> Iterator[NId]:
    if attr := get_optional_attribute(graph, nid, "assume_role_policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id][
            "label_type"
        ] == "Literal" and _iam_role_is_over_privileged_in_literal(attr[1], danger_attr):
            yield attr[2]
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _iam_role_is_over_privileged_in_jsonencode(graph, value_id, danger_attr)


def eks_has_endpoints_publicly_accessible(graph: Graph, nid: NId) -> NId | None:
    if vpc_config := get_argument(graph, nid, "vpc_config"):
        public = get_optional_attribute(graph, vpc_config, "endpoint_public_access")
        private = get_optional_attribute(graph, vpc_config, "endpoint_private_access")
        if public and private and public[1].lower() == "true" and private[1].lower() == "false":
            return public[2]
    return None


def rds_not_uses_iam_authentication(graph: Graph, nid: NId) -> NId | None:
    if iam_auth := get_optional_attribute(graph, nid, "iam_database_authentication_enabled"):
        if iam_auth[1].lower() != "true":
            return iam_auth[2]
    else:
        return nid
    return None


def redshift_has_public_clusters(graph: Graph, nid: NId) -> NId | None:
    public_attr = get_optional_attribute(graph, nid, "publicly_accessible")
    if public_attr and public_attr[1].lower() == "true":
        return public_attr[2]
    return None


def redshift_not_requires_ssl(graph: Graph, nid: NId) -> Iterator[NId]:
    exist = False
    for parameter in get_argument_iterator(graph, nid, "parameter"):
        p_name = get_optional_attribute(graph, parameter, "name")
        if p_name and p_name[1] == "require_ssl":
            exist = True
            p_value = get_optional_attribute(graph, parameter, "value")
            if p_value and p_value[1] == "false":
                yield parameter
    if not exist:
        yield nid


def elasticache_uses_default_port(graph: Graph, nid: NId) -> NId | None:
    if (
        (port := get_optional_attribute(graph, nid, "port"))
        and (engine := get_optional_attribute(graph, nid, "engine"))
        and engine[1] == "memcached"
        and port[1] in {"11211", "6379"}
    ):
        return port[2]
    return None


def elasticache_is_transit_encryption_disabled(graph: Graph, nid: NId) -> NId | None:
    if (
        (engine := get_optional_attribute(graph, nid, "engine"))
        and engine[1] == "redis"
        and (encryption := get_optional_attribute(graph, nid, "transit_encryption_enabled"))
        and encryption[1].lower() != "true"
    ):
        return encryption[2]
    return None


def sns_is_server_side_encryption_disabled(graph: Graph, nid: NId) -> NId | None:
    encryption = get_optional_attribute(graph, nid, "kms_master_key_id")
    if not encryption:
        return nid
    return None


def sqs_is_encryption_disabled(graph: Graph, nid: NId) -> NId | None:
    if (
        (sse_enc := get_optional_attribute(graph, nid, "sqs_managed_sse_enabled"))
        and sse_enc[1] == "false"
        and not get_optional_attribute(graph, nid, "kms_master_key_id")
    ):
        return sse_enc[2]
    return None


def tfm_sqs_is_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_SQS_IS_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name == "aws_sqs_queue" and (rep := sqs_is_encryption_disabled(graph, nid)):
                yield rep

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_sns_is_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_SNS_IS_SERVER_SIDE_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name == "aws_sns_topic" and (
                rep := sns_is_server_side_encryption_disabled(graph, nid)
            ):
                yield rep

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_elasticache_is_transit_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_ELASTICACHE_IS_TRANSIT_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name == "aws_elasticache_cluster" and (
                rep := elasticache_is_transit_encryption_disabled(graph, nid)
            ):
                yield rep

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_elasticache_uses_default_port(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_ELASTICACHE_USES_DEFAULT_PORT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name == "aws_elasticache_cluster" and (
                rep := elasticache_uses_default_port(graph, nid)
            ):
                yield rep

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_redshift_not_requires_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_REDSHIFT_NOT_REQUIRES_SSL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_redshift_parameter_group":
                yield from redshift_not_requires_ssl(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_redshift_has_public_clusters(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_REDSHIFT_HAS_PUBLIC_CLUSTERS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name == "aws_redshift_cluster" and (rep := redshift_has_public_clusters(graph, nid)):
                yield rep

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_rds_not_uses_iam_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_RDS_NOT_USES_IAM_AUTHENTICATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name == "aws_db_instance" and (rep := rds_not_uses_iam_authentication(graph, nid)):
                yield rep

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_eks_has_endpoints_publicly_accessible(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EKS_HAS_ENDPOINTS_PUBLICLY_ACCESSIBLE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name == "aws_eks_cluster" and (
                rep := eks_has_endpoints_publicly_accessible(graph, nid)
            ):
                yield rep

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_iam_allow_not_principal_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_ROLE_IS_OVER_PRIVILEGED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_iam_role":
                yield from _iam_role_is_over_privileged(graph, nid, "NotPrincipal")

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_iam_allow_not_actions_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_TRUST_POLICY_NOT_ACTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_iam_role":
                yield from _iam_role_is_over_privileged(graph, nid, "NotAction")

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _search_policy_in_jsonencode(graph: Graph, nid: NId, danger_el: str) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if statements := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[statements[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            if (
                (effect := get_optional_attribute(graph, c_id, "Effect"))
                and effect[1] == "Allow"
                and (d_attr := get_optional_attribute(graph, c_id, danger_el))
            ):
                yield d_attr[2]


def _search_policy_in_literal(
    attr_val: str,
    danger_el: str,
) -> bool:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    if isinstance(statements, list):
        for stmt in statements:
            if stmt.get("Effect") == "Allow" and stmt.get(danger_el):
                return True
    return False


def _search_policy_document(graph: Graph, nid: NId, danger_el: str) -> Iterator[NId]:
    for stmt in match_ast_group_d(graph, nid, "Object"):
        if (
            graph.nodes[stmt].get("name") == "statement"
            and (
                not (effect := get_optional_attribute(graph, stmt, "effect"))
                or effect[1] == "Allow"
            )
            and (danger_attr := get_optional_attribute(graph, stmt, danger_el))
        ):
            yield danger_attr[2]


def _iam_search_danger_vals_in_policies(
    graph: Graph,
    nid: NId,
    danger_vals: tuple[str, str],
) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _search_policy_document(graph, nid, danger_vals[0])
    elif attr := get_optional_attribute(graph, nid, "policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal" and _search_policy_in_literal(
            attr[1],
            danger_vals[1],
        ):
            yield attr[2]
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _search_policy_in_jsonencode(graph, value_id, danger_vals[1])


def tfm_iam_allow_not_resource_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_PERMISSIONS_POLICY_ALLOW_NOT_RESOURCE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _iam_search_danger_vals_in_policies(
                    graph,
                    nid,
                    ("not_resources", "NotResource"),
                )

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_iam_allow_not_action_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_PERMISSIONS_POLICY_ALLOW_NOT_ACTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _iam_search_danger_vals_in_policies(
                    graph,
                    nid,
                    ("not_actions", "NotAction"),
                )

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_iam_is_policy_applying_to_users(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_POLICY_APPLY_TO_USERS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                graph.nodes[nid].get("name") == "aws_iam_user_policy"
                and get_optional_attribute(graph, nid, "policy")
                and (user := get_optional_attribute(graph, nid, "user"))
            ):
                yield user[2]

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
