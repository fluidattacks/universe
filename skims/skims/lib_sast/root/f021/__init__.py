from lib_sast.root.f021.c_sharp.c_sharp_xpath_injection import (
    c_sharp_xpath_injection as _c_sharp_xpath_injection,
)
from lib_sast.root.f021.c_sharp.c_sharp_xpath_injection_node import (
    c_sharp_xpath_injection_node as _c_sharp_xpath_injection_node,
)
from lib_sast.root.f021.java.java_xpath_injection import (
    java_unsafe_xpath_injection as _java_unsafe_xpath_injection,
)
from lib_sast.root.f021.javascript.javascript_dynamic_xpath import (
    js_dynamic_xpath as _js_dynamic_xpath,
)
from lib_sast.root.f021.typescript.typescript_dynamic_xpath import (
    ts_dynamic_xpath as _ts_dynamic_xpath,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_xpath_injection_node(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_xpath_injection_node(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_xpath_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_xpath_injection(shard, method_supplies)


@SHIELD_BLOCKING
def java_unsafe_xpath_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unsafe_xpath_injection(shard, method_supplies)


@SHIELD_BLOCKING
def js_dynamic_xpath(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_dynamic_xpath(shard, method_supplies)


@SHIELD_BLOCKING
def ts_dynamic_xpath(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_dynamic_xpath(shard, method_supplies)
