from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.c_sharp import (
    get_first_member_syntax_graph,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_object_identifiers,
)
from model.core import (
    MethodExecutionResult,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    args.evaluation[args.n_id] = n_attrs["member"] in NET_SYSTEM_HTTP_METHODS or (
        n_attrs["member"] == "SelectSingleNode" and args.evaluation[n_attrs["expression_id"]]
    )

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] == "XPathNavigator":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
}


def c_sharp_xpath_injection_node(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_XPATH_INJECTION_NODE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        xpath_obj = get_object_identifiers(graph, {"XPathNavigator"})
        if not xpath_obj:
            return

        for node in method_supplies.selected_nodes:
            expr = graph.nodes[node]["expression"]
            if (
                expr.split(".")[-1] == "SelectSingleNode"
                and (member := get_first_member_syntax_graph(graph, node))
                and graph.nodes[member].get("symbol") in xpath_obj
                and get_node_evaluation_results(
                    method,
                    graph,
                    node,
                    set(),
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
