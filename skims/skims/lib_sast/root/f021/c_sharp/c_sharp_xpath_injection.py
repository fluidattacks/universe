from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    NET_CONNECTION_OBJECTS,
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _object_creation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] in NET_CONNECTION_OBJECTS:
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _method_invocation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if "SecurityElement.Escape" in ma_attr["expression"]:
        args.triggers.add("Escaped")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    ma_attr = args.graph.nodes[args.n_id]

    if ma_attr["member"] == "CreateNavigator":
        args.triggers.add("xpath")
    elif ma_attr["member"] in NET_SYSTEM_HTTP_METHODS:
        args.triggers.add("user_parameters")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
    "method_invocation": _method_invocation_evaluator,
}


def c_sharp_xpath_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_XPATH_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                (expr := n_attrs.get("expression"))
                and expr.rsplit(".", maxsplit=1)[-1] == "Evaluate"
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_attrs["expression_id"],
                    {"xpath"},
                    danger_goal=False,
                    method_evaluators=METHOD_EVALUATORS,
                )
                and (al_id := n_attrs.get("arguments_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    al_id,
                    {"user_parameters", "user_connection"},
                    danger_goal=False,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
