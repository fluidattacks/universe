from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_list_from_node,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)


def _cors_uses_danger_methods(graph: Graph, nid: NId) -> Iterator[NId]:
    for c_id in match_ast_group_d(graph, nid, "Object"):
        if (
            graph.nodes[c_id].get("name") == "cors_rule"
            and (allow_methods := get_optional_attribute(graph, c_id, "allowed_methods"))
            and (methods := get_list_from_node(graph, allow_methods[2]))
            and any(
                danger_method in methods for danger_method in ["TRACE", "DELETE", "PUT", "CONNECT"]
            )
        ):
            yield allow_methods[2]


def tfm_has_danger_https_methods_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_HTTP_METHODS_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_s3_bucket_cors_configuration":
                yield from _cors_uses_danger_methods(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
