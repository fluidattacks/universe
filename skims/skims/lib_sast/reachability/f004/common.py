from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    nodes_by_type as nodes_type,
)
from lib_sast.utils.graph import (
    pred_ast,
)


def aux_sandbox_escape_vm2(graph: Graph, n_id: NId) -> NId | None:
    parent_id = pred_ast(graph, n_id)[0]
    if graph.nodes[parent_id]["label_type"] == "VariableDeclaration":
        var_name = graph.nodes[parent_id]["variable"]
        dangerous_expression = f"{var_name}.run"
        nodes = nodes_type(graph).get("MethodInvocation", [])
        for method_id in nodes:
            if graph.nodes[method_id].get("expression") == dangerous_expression:
                return method_id
    return None
