from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.sast_model import (
    NId,
)
from model.core import (
    MethodExecutionResult,
)


def py_cve_2024_39303(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.PY_CVE_2024_39303

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        for n_id in methods_args.nodes:
            if (
                "weblate" in methods_args.var
                and "projectbackup" in methods_args.var
                and graph.nodes[n_id]["expression"].endswith("restore")
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
