from lib_sast.reachability.f002.c_sharp.c_sharp_cve_2024_21907 import (
    c_sharp_cve_2024_21907 as _c_sharp_cve_2024_21907,
)
from lib_sast.reachability.f002.javascript.js_cve_2018_1109 import (
    js_cve_2018_1109 as _js_cve_2018_1109,
)
from lib_sast.reachability.f002.javascript.js_cve_2019_10775 import (
    js_cve_2019_10775 as _js_cve_2019_10775,
)
from lib_sast.reachability.f002.python.py_cve_2020_28975 import (
    py_cve_2020_28975 as _py_cve_2020_28975,
)
from lib_sast.reachability.f002.python.py_cve_2022_24859 import (
    py_cve_2022_24859 as _py_cve_2022_24859,
)
from lib_sast.reachability.f002.typescript.ts_cve_2018_1109 import (
    ts_cve_2018_1109 as _ts_cve_2018_1109,
)
from lib_sast.reachability.f002.typescript.ts_cve_2019_10775 import (
    ts_cve_2019_10775 as _ts_cve_2019_10775,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_cve_2024_21907(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _c_sharp_cve_2024_21907(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2019_10775(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2019_10775(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2018_1109(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2018_1109(methods_args=methods_args)


@SHIELD_BLOCKING
def py_cve_2020_28975(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _py_cve_2020_28975(methods_args=methods_args)


@SHIELD_BLOCKING
def py_cve_2022_24859(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _py_cve_2022_24859(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2018_1109(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2018_1109(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2019_10775(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2019_10775(methods_args=methods_args)
