from collections.abc import (
    Callable,
)
from typing import (
    NamedTuple,
)

from lib_sast.sast_model import (
    GraphShard,
    NId,
)
from model.core import (
    FindingEnum,
    MethodExecutionResult,
)


class MethodsArgs(NamedTuple):
    shard: GraphShard
    nodes: list[NId]
    var: list[str]
    current_version: str
    dep: str
    cve: str
    pkg_id: str


class Query(NamedTuple):
    finding: FindingEnum
    method: Callable[
        [MethodsArgs],
        MethodExecutionResult,
    ]
    node_type: str
    dependency: tuple[str, ...]
    cve: str
    pkg_id: str | None = None
