from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.common import (
    check_js_ts_http_inputs,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if check_js_ts_http_inputs(args):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def get_key_value(graph: Graph, nid: NId) -> tuple[str, str]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id].get("symbol", graph.nodes[key_id].get("value", ""))
    value_id = graph.nodes[nid]["value_id"]
    value = ""
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        child_id = adj_ast(graph, value_id, label_type="Literal")
        if len(child_id) > 0:
            value = graph.nodes[child_id[0]].get("value", "")
    else:
        value = graph.nodes[value_id].get("value", "")
    return key, value


def object_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    graph = args.graph
    childs = adj_ast(graph, args.n_id, label_type="Pair")
    iframe_is_allowed = False
    has_allowed_iframe_hostnames = False
    has_allow_iframe_relative_urls = False
    for child_id in childs:
        key, value = get_key_value(graph, child_id)
        if key == "allowedTags" and "iframe" in value:
            iframe_is_allowed = True
        if key == "allowedIframeHostnames":
            has_allowed_iframe_hostnames = True
        if key == "allowIframeRelativeUrls" and value.lower() == "true":
            has_allow_iframe_relative_urls = True
    if iframe_is_allowed and has_allow_iframe_relative_urls and has_allowed_iframe_hostnames:
        args.triggers.add("insecure_config")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": member_access_evaluator,
    "object_node": object_evaluator,
}


def js_cve_2021_26540(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_CVE_2021_26540

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        for n_id in methods_args.nodes:
            if graph.nodes[n_id].get(
                "expression",
            ) in methods_args.var and get_node_evaluation_results(
                method,
                graph,
                n_id,
                {"insecure_config"},
                method_evaluators=METHOD_EVALUATORS,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
