from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def uses_replacements(graph: Graph, n_id: NId | None) -> bool:
    uses_where = False
    uses_replacement = False
    first_arg = get_n_arg(graph, n_id, 0)
    second_arg = get_n_arg(graph, n_id, 1)
    if not first_arg or not second_arg:
        return False
    uses_where = (
        graph.nodes[first_arg].get("label_type") == "Literal"
        and "where" in graph.nodes[first_arg].get("value", "").lower()
    )
    if graph.nodes[second_arg].get("label_type") == "Object":
        for pair_id in adj_ast(graph, second_arg, label_type="Pair"):
            key_n_id = graph.nodes[pair_id].get("key_id")
            if graph.nodes[key_n_id].get("symbol") == "replacements":
                uses_replacement = True
            if graph.nodes[key_n_id].get("symbol") == "where":
                uses_where = True
    return bool(uses_where and uses_replacement)
