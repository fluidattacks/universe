import os
from collections import (
    defaultdict,
)

from fluid_sbom.advisories import (
    get_package_advisories,
    get_related_vulns,
)
from fluid_sbom.model.core import (
    Advisory,
    Package,
)


def process_advisories(package: Package, vuln_range_dict: dict) -> None:
    """Process advisories for a package and update the vulnerability range."""
    advisories = get_package_advisories(package)
    if not advisories:
        return
    for adv in advisories:
        related_vuln = get_related_vulns(package.name, adv.id)
        if isinstance(adv.version_constraint, str):
            upd_vuln_range(package.name, related_vuln, adv, vuln_range_dict)


def upd_vuln_range(
    package_name: str,
    related_vuln: str | None,
    adv: Advisory,
    vuln_range_dict: dict,
) -> None:
    """Update vulnerability range for a package in vuln_range_dict."""
    if related_vuln and related_vuln not in vuln_range_dict[package_name]:
        vuln_range_dict[package_name][related_vuln] = adv.version_constraint
    vuln_range_dict[package_name][adv.id] = adv.version_constraint


def process_package_location(
    package: Package,
    dependencies_dict: dict,
    location_path: str | None,
) -> None:
    """Process package location and update dependencies dictionary."""
    if location_path:
        dependencies_dict[location_path][package.name] = {
            "id": package.id_by_hash(),
            "version": package.version,
        }


def create_dependencies_dict(
    packages: list[Package],
) -> tuple[dict[str, dict[str, dict[str, dict[str, str]]]], dict[str, dict[str, str]]]:
    """Generate a dependency tree that maps a dependency file path to its parsed packages."""
    deps_dict: dict[str, dict[str, dict[str, str]]] = defaultdict(dict)
    vuln_range_dict: dict[str, dict[str, str]] = defaultdict(dict)

    for package in packages:
        location_path = (
            package.locations[0].access_path
            if hasattr(package.locations[0], "access_path")
            else None
        )
        process_advisories(package, vuln_range_dict)
        process_package_location(package, deps_dict, location_path)
    return {"items": dict(deps_dict)}, vuln_range_dict


def parse_sbom_json_results(
    sbom_results: dict,
) -> tuple[dict[str, dict[str, dict[str, dict[str, str]]]], dict[str, dict[str, str]]]:
    deps_dict: dict[str, dict[str, dict[str, str]]] = defaultdict(dict)
    pkg_vulns: dict[str, dict[str, str]] = defaultdict(dict)
    sbom_dir = sbom_results.get("sbom_details", {}).get("name", "")
    if not sbom_dir.endswith("/"):
        sbom_dir = ""
    for package in sbom_results.get("packages", []):
        package_name = package["name"]
        pkg_location_paths = package.get("locations", [])
        for location in pkg_location_paths:
            full_path = sbom_dir + location["path"]
            deps_dict[full_path][package_name] = {
                "id": package["id"],
                "version": package["version"],
            }

        for adv in package["advisories"]:
            adv_id = adv.get("id", None)
            adv_version_constraint = adv.get("version_constraint", None)
            if not adv_id or not adv_version_constraint:
                continue

            pkg_vulns[package_name][adv_id] = adv_version_constraint

            if (
                related_vuln := get_related_vulns(package_name, adv_id)
            ) and related_vuln not in pkg_vulns[package_name]:
                pkg_vulns[package_name][related_vuln] = adv_version_constraint

    return {"items": dict(deps_dict)}, pkg_vulns


def find_lock_file(current_path: str, possible_locks: list[str]) -> str | None:
    """Search for a lock file in the current directory.

    Args:
        current_path (str): The directory to search.
        possible_locks (list[str]): List of lock file names to look for.

    Returns:
        str | None: Path to the lock file, or None if not found.

    """
    for lock_file in possible_locks:
        lock_candidate = os.path.join(current_path, lock_file)  # noqa: PTH118
        if os.path.isfile(lock_candidate):  # noqa: PTH113
            return lock_candidate
    return None
