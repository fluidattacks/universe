from lib_sast.reachability.f353.java.java_cve_2021_43570 import (
    java_cve_2021_43570 as _java_cve_2021_43570,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_cve_2021_43570(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _java_cve_2021_43570(methods_args=methods_args)
