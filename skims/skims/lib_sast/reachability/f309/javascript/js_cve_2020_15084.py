from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    if n_attrs.get("expression", "").split(".")[-1] != "expressJwtSecret":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": method_invocation_evaluator,
}


def auth_bypass_express_jwt(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    has_insecure_secret = False
    first_arg = get_n_arg(graph, n_id, 0)
    if not first_arg:
        return False
    for p_id in adj_ast(graph, first_arg):
        key_id = graph.nodes[p_id]["key_id"]
        key = graph.nodes[key_id]["symbol"]
        value_id = graph.nodes[p_id]["value_id"]
        if key == "secret" and get_node_evaluation_results(
            method,
            graph,
            value_id,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        ):
            has_insecure_secret = True
        if key == "algorithms":
            has_insecure_secret = False
    return has_insecure_secret


def js_cve_2020_15084(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_CVE_2020_15084

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        if not file_imports_module(graph, "jwks-rsa"):
            return

        for n_id in methods_args.nodes:
            if graph.nodes[n_id].get(
                "expression",
            ) in methods_args.var and auth_bypass_express_jwt(graph, n_id, method):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
