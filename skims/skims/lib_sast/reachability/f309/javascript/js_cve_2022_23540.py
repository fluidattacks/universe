from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from model.core import (
    MethodExecutionResult,
)


def is_third_arg_method_declaration(graph: Graph, n_id: NId | None) -> bool:
    third_arg_id = get_n_arg(graph, n_id, 2)
    if third_arg_id is None:
        return True
    return graph.nodes[third_arg_id].get("label_type") == "MethodDeclaration"


def js_cve_2022_23540(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_CVE_2022_23540

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        expr_list: list[str] = [f"{var}.verify" for var in methods_args.var]
        for n_id in methods_args.nodes:
            if graph.nodes[n_id].get("expression") in expr_list and is_third_arg_method_declaration(
                graph,
                n_id,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
