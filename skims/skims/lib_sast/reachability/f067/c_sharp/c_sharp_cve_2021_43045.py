from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.sast_model import (
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def object_creation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("name") == "BinaryDecoder":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_creation": object_creation_evaluator,
}


def c_sharp_cve_2021_43045(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_CVE_2021_43045

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        for n_id in methods_args.nodes:
            if (
                graph.nodes[n_id]["expression"].split(".")[-1].lower() == "readstring"
                and any(
                    imported_name in methods_args.var for imported_name in ["avro.io", "io", "*"]
                )
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_id,
                    set(),
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
