from lib_sast.reachability.f067.c_sharp.c_sharp_cve_2021_43045 import (
    c_sharp_cve_2021_43045 as _c_sharp_cve_2021_43045,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_cve_2021_43045(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _c_sharp_cve_2021_43045(methods_args=methods_args)
