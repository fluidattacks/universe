from lib_sast.reachability.f115.javascript.js_cve_2023_28155 import (
    js_cve_2023_28155 as _js_cve_2023_28155,
)
from lib_sast.reachability.f115.javascript.js_cve_2024_29415 import (
    js_cve_2024_29415 as _js_cve_2024_29415,
)
from lib_sast.reachability.f115.typescript.ts_cve_2023_28155 import (
    ts_cve_2023_28155 as _ts_cve_2023_28155,
)
from lib_sast.reachability.f115.typescript.ts_cve_2024_29415 import (
    ts_cve_2024_29415 as _ts_cve_2024_29415,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_cve_2023_28155(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2023_28155(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2024_29415(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2024_29415(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2023_28155(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2023_28155(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2024_29415(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2024_29415(methods_args=methods_args)
