from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
    is_sanitized,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.common import (
    check_js_ts_http_inputs,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if check_js_ts_http_inputs(args):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": member_access_evaluator,
}


def is_true_boolean(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("value_type", "") == "bool"
        and graph.nodes[n_id].get("value", "") == "true"
    )


def js_cve_2020_7766(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_CVE_2020_7766

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        expr_list: list[str] = []
        for var in methods_args.var:
            expr_list.extend(
                [
                    f"{var}.set",
                ],
            )
        for n_id in methods_args.nodes:
            if (
                graph.nodes[n_id].get("expression") in expr_list
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_id,
                    set(),
                    method_evaluators=METHOD_EVALUATORS,
                )
                and not is_sanitized(graph, n_id)
            ):
                f_arg = get_n_arg(graph, n_id, 3)

                if f_arg and is_node_definition_unsafe(graph, f_arg, is_true_boolean):
                    yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
