from collections.abc import (
    Callable,
)

from lib_sast.reachability.imports.common.import_node import (
    get_variables_from_import,
)
from lib_sast.reachability.imports.common.method_invocation import (
    get_variables_from_require,
)
from lib_sast.sast_model import (
    GraphShard,
    NId,
)
from lib_sast.utils.graph import (
    filter_nodes,
    pred_has_labels,
)

CASES_BY_NODE: dict[str, set[Callable[[GraphShard, NId, set[str]], dict[str, list[str]]]]] = {
    "Import": {get_variables_from_import},
    "ModuleImport": {get_variables_from_import},
    "MethodInvocation": {get_variables_from_require},
}


def get_var_dict(global_dict: dict[str, list[str]], result_dict: dict[str, list[str]]) -> None:
    for key, value in result_dict.items():
        if key in global_dict:
            global_dict[key].extend(value)
        else:
            global_dict[key] = value


def get_variables_from_dependencies(
    shard: GraphShard,
    dependencies: set[str],
) -> dict[str, list[str]]:
    graph = shard.syntax_graph

    variables_dict: dict[str, list[str]] = {}

    for node_type, callables in CASES_BY_NODE.items():
        for nid in filter_nodes(graph, graph.nodes, pred_has_labels(label_type=node_type)):
            for func in callables:
                get_var_dict(variables_dict, func(shard, nid, dependencies))
    return variables_dict
