from collections.abc import (
    Callable,
)

from lib_sast.reachability.imports.java.import_node import (
    get_variables_from_import,
)
from lib_sast.sast_model import (
    GraphShard,
    NId,
)
from lib_sast.utils.graph import (
    filter_nodes,
    pred_has_labels,
)

CASES_BY_NODE: dict[str, set[Callable[[GraphShard, NId, set[str]], dict[str, list[str]]]]] = {
    "Import": {get_variables_from_import},
}


def get_variables_from_dependencies(
    shard: GraphShard,
    dependencies: set[str],
) -> dict[str, list[str]]:
    graph = shard.syntax_graph

    variables_dict: dict[str, list[str]] = {}

    for node_type, callables in CASES_BY_NODE.items():
        for nid in filter_nodes(graph, graph.nodes, pred_has_labels(label_type=node_type)):
            for func in callables:
                func_result = func(shard, nid, dependencies)
                for key, value in func_result.items():
                    variables_dict.setdefault(key, []).extend(value)

    return {key: list(dict.fromkeys(value)) for key, value in variables_dict.items()}
