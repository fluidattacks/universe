from collections.abc import (
    Callable,
)

from lib_sast.reachability.imports.c_sharp import (
    get_variables_from_dependencies as c_sharp_get_variables_from_dependencies,
)
from lib_sast.reachability.imports.java import (
    get_variables_from_dependencies as java_get_variables_from_dependencies,
)
from lib_sast.reachability.imports.javascript import (
    get_variables_from_dependencies as js_get_variables_from_dependencies,
)
from lib_sast.reachability.imports.python import (
    get_variables_from_dependencies as py_get_variables_from_dependencies,
)
from lib_sast.reachability.imports.typescript import (
    get_variables_from_dependencies as ts_get_variables_from_dependencies,
)
from lib_sast.sast_model import (
    GraphShard,
)
from lib_sast.sast_model import (
    GraphShardMetadataLanguage as GraphLanguage,
)

VARS_BY_LANG: dict[GraphLanguage, Callable[[GraphShard, set[str]], dict[str, list[str]]]] = {
    GraphLanguage.CSHARP: c_sharp_get_variables_from_dependencies,
    GraphLanguage.JAVA: java_get_variables_from_dependencies,
    GraphLanguage.JAVASCRIPT: js_get_variables_from_dependencies,
    GraphLanguage.PYTHON: py_get_variables_from_dependencies,
    GraphLanguage.TYPESCRIPT: ts_get_variables_from_dependencies,
}
