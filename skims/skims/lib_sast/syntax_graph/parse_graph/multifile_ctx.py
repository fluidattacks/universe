from collections.abc import (
    Callable,
)

from lib_sast.sast_model import (
    GraphShard,
    GraphShardMetadataLanguage,
)


def set_context_java(
    shard: GraphShard,
    context: dict,
    language: GraphShardMetadataLanguage,
) -> None:
    if package_metadata := shard.syntax_graph.nodes.get("0", {}).get("package"):
        if language not in context:
            context[language] = {}

        ctx_java = context[language]

        file_struct = shard.syntax_graph.nodes["0"]["structure"]

        if package_metadata in ctx_java:
            ctx_java[package_metadata][shard.path] = file_struct
        else:
            ctx_java[package_metadata] = {shard.path: file_struct}


CONTEXT_SETTERS: dict[
    GraphShardMetadataLanguage,
    Callable[[GraphShard, dict, GraphShardMetadataLanguage], None],
] = {GraphShardMetadataLanguage.JAVA: set_context_java}


def set_context_by_lang(
    shard: GraphShard,
    context: dict,
    language: GraphShardMetadataLanguage,
) -> None:
    if context_setter := CONTEXT_SETTERS.get(language):
        context_setter(shard, context, language)
