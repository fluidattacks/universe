from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def build_import_module_node(
    args: SyntaxGraphArgs,
    expression: str,
    alias: str | None = None,
) -> NId:
    args.syntax_graph.add_node(
        args.n_id,
        expression=expression,
        label_type="ModuleImport",
    )

    if alias:
        args.syntax_graph.nodes[args.n_id]["label_alias"] = alias

    return args.n_id
