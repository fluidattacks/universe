from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def build_catch_clause_node(
    args: SyntaxGraphArgs,
    block_node: NId | None,
    catch_decl: NId | None = None,
) -> NId:
    args.syntax_graph.add_node(
        args.n_id,
        label_type="CatchClause",
    )

    if block_node:
        args.syntax_graph.nodes[args.n_id]["block_id"] = block_node
        args.syntax_graph.add_edge(
            args.n_id,
            args.generic(args.fork_n_id(block_node)),
            label_ast="AST",
        )

    if catch_decl:
        args.syntax_graph.nodes[args.n_id]["catch_declaration"] = catch_decl
        args.syntax_graph.add_edge(
            args.n_id,
            args.generic(args.fork_n_id(catch_decl)),
            label_ast="AST",
        )

    return args.n_id
