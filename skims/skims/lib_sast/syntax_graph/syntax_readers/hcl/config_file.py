from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.file import (
    build_file_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    body_id = match_ast_d(args.ast_graph, args.n_id, "body")
    c_ids = adj_ast(args.ast_graph, body_id) if body_id else adj_ast(args.ast_graph, args.n_id)
    return build_file_node(args, iter(c_ids))
