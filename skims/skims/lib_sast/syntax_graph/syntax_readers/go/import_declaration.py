from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.import_global import (
    build_import_global_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    alias: str | None = None

    exp_n_id = g.match_ast_d(args.ast_graph, args.n_id, "interpreted_string_literal")
    expression = graph.nodes[exp_n_id].get("label_text")[1:-1]

    if alias_n_id := g.match_ast_d(args.ast_graph, args.n_id, "package_identifier"):
        alias = graph.nodes[alias_n_id].get("label_text")

    return build_import_global_node(args, expression, set(), alias)
