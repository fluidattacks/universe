from collections.abc import (
    Iterator,
)
from typing import (
    cast,
)

from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.file import (
    build_file_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    ignored_labels = {
        "\n",
        "\r\n",
    }
    graph = args.ast_graph
    c_ids = g.adj_ast(graph, args.n_id)

    filtered_ids: list[NId] = []
    for _id in c_ids:
        if (label_type := graph.nodes[_id]["label_type"]) in ignored_labels:
            continue
        if label_type == "import_declaration":
            filtered_ids.extend(g.match_ast_group_d(args.ast_graph, _id, "import_spec", depth=2))
            continue
        filtered_ids.append(_id)

    return build_file_node(args, cast(Iterator[str], filtered_ids))
