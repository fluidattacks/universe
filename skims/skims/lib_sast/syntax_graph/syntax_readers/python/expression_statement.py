from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    childs = match_ast(graph, args.n_id, "del", "global")
    expr_id = childs.get("__0__")
    if not expr_id:
        expr_id = adj_ast(graph, args.n_id)[0]
    return args.generic(args.fork_n_id(expr_id))
