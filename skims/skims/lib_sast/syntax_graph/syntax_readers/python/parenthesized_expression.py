from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.parenthesized_expression import (
    build_parenthesized_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    childs = adj_ast(args.ast_graph, args.n_id)
    c_id = childs[1] if len(childs) == 3 else childs[-2]
    return build_parenthesized_expression_node(args, c_id)
