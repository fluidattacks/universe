from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.assignment import (
    build_assignment_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    alias_id = graph.nodes[args.n_id].get("label_field_alias")
    children = adj_ast(graph, args.n_id)
    var_id = adj_ast(graph, alias_id)[0] if alias_id else children[-1]
    return build_assignment_node(args, var_id, children[0], None)
