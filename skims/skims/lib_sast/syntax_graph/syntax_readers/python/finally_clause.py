from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.finally_clause import (
    build_finally_clause_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    block = match_ast_d(graph, args.n_id, "block")
    return build_finally_clause_node(args, block)
