from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.if_statement import (
    build_if_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    childs = match_ast(args.ast_graph, args.n_id, "if", "else")
    if childs["if"] and childs["else"] and len(childs) == 5:
        return build_if_node(args, str(childs["__1__"]), childs["__0__"], childs["__2__"])
    return build_if_node(args, str(childs["__0__"]))
