from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.throw import (
    build_throw_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    match = match_ast(args.ast_graph, args.n_id, "raise")
    expr_id = match.get("__0__")
    return build_throw_node(args, expr_id)
