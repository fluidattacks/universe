from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.binary_operation import (
    build_binary_operation_node,
)
from lib_sast.syntax_graph.syntax_nodes.string_literal import (
    build_string_literal_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]
    operator_id: NId | None = n_attrs.get("label_field_operators") or n_attrs.get(
        "label_field_operator",
    )
    operator = node_to_str(graph, operator_id) if operator_id else ""
    childs = adj_ast(graph, args.n_id)
    if all(
        graph.nodes[_id]["label_type"] == "string"
        for _id in adj_ast(graph, args.n_id)
        if _id != operator_id
    ):
        text = node_to_str(graph, args.n_id)
        return build_string_literal_node(args, text)

    return build_binary_operation_node(args, operator, childs[0], childs[-1])
