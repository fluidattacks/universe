from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.file import (
    build_file_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = g.adj_ast(graph, args.n_id)
    filtered_ids: list[NId] = []

    for _id in c_ids:
        n_attrs = graph.nodes[_id]
        if n_attrs.get("label_type") in {
            "import_statement",
            "import_from_statement",
        }:
            filtered_ids.extend(g.match_ast_group_d(args.ast_graph, _id, "dotted_name", depth=-1))
            if g.match_ast_d(graph, _id, "wildcard_import"):
                continue
            if module_n_id := n_attrs.get("label_field_module_name"):
                if module_n_id in filtered_ids:
                    filtered_ids.remove(module_n_id)
                elif d_name_n_id := g.match_ast_d(graph, module_n_id, "dotted_name"):
                    filtered_ids.remove(d_name_n_id)

            continue
        filtered_ids.append(_id)

    return build_file_node(args, iter(filtered_ids))
