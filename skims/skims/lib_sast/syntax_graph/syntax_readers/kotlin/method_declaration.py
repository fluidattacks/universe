from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.method_declaration import (
    build_method_declaration_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    name_id = graph.nodes[args.n_id].get("label_field_name")
    name = node_to_str(graph, name_id) if name_id else None

    block_id = match_ast_d(graph, args.n_id, "function_body")
    if not block_id:
        block_id = match_ast_d(graph, args.n_id, "statements")

    children_nid = {}

    parameters_id = graph[args.n_id].get("label_field_parameters") or match_ast_d(
        graph,
        args.n_id,
        "function_value_parameters",
    )

    if parameters_id:
        children_nid["parameters_id"] = [parameters_id]

    return build_method_declaration_node(args, name, block_id, children_nid)
