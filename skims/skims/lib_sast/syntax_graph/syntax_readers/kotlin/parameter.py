from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.parameter import (
    build_parameter_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    val_id = match_ast_d(graph, args.n_id, "identifier")
    var_name = node_to_str(graph, val_id) if val_id else "UnnamedParam"

    var_type_id = match_ast_d(graph, args.n_id, "user_type")
    var_type = node_to_str(graph, var_type_id) if var_type_id else None

    return build_parameter_node(
        args=args,
        variable=var_name,
        variable_type=var_type,
        value_id=val_id,
    )
