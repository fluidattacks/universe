from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.assignment import (
    build_assignment_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    children = adj_ast(graph, args.n_id)
    operator = None
    if len(children) >= 3:
        var_id = children[0]
        operator = node_to_str(graph, children[1])
        val_id = children[2]
    else:
        var_id = children[0]
        val_id = children[-1]

    return build_assignment_node(args, var_id, val_id, operator)
