from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.execution_block import (
    build_execution_block_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    match_ast_group_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    if block_id := match_ast_d(graph, args.n_id, "block"):
        c_ids = [
            adj_ast(graph, stmt_id)[0]
            for stmt_id in match_ast_group_d(graph, block_id, "statement")
        ]
    elif expr := match_ast_d(graph, args.n_id, "expression") or (
        expr := match_ast_d(graph, args.n_id, "comparison_expression")
    ):
        c_ids = [expr]
    else:
        c_ids = [
            adj_ast(graph, stmt_id)[0]
            for stmt_id in match_ast_group_d(graph, args.n_id, "statement")
        ]
    return build_execution_block_node(args, iter(c_ids))
