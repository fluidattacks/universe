from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.while_statement import (
    build_while_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    block = graph.nodes[args.n_id].get("label_field_body")
    if not block:
        block = adj_ast(graph, args.n_id)[-1]
    conditional_node = graph.nodes[args.n_id]["label_field_condition"]
    return build_while_statement_node(args, block, conditional_node)
