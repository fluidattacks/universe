from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.method_declaration import (
    build_method_declaration_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    nodes = adj_ast(args.ast_graph, args.n_id)
    block_node = None
    if len(nodes) > 1:
        block_node = nodes[1]
    return build_method_declaration_node(args, "ArrowMethod", block_node, {})
