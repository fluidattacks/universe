from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.member_access import (
    build_member_access_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    expression_id = graph.nodes[args.n_id]["label_field_condition"]
    expression = node_to_str(graph, expression_id)
    member_id = (
        match_ast_d(graph, args.n_id, "member_binding_expression")
        or match_ast_d(graph, args.n_id, "element_binding_expression")
        or adj_ast(graph, args.n_id)[-1]
    )
    member = node_to_str(graph, member_id).strip(".")
    return build_member_access_node(args, member, expression, expression_id)
