from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.array_node import (
    build_array_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    children = match_ast_group_d(args.ast_graph, args.n_id, "initializer_expression")
    return build_array_node(args, iter(children))
