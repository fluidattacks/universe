from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.constants import (
    C_SHARP_STATEMENT,
)
from lib_sast.syntax_graph.syntax_nodes.switch_section import (
    build_switch_section_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    childs = match_ast(graph, args.n_id, "case_switch_label")
    case_id = childs.get("case_switch_label")
    case_expr = node_to_str(graph, case_id) if case_id else "Default"

    execution_ids = [
        _id
        for _id in adj_ast(graph, args.n_id)
        if graph.nodes[_id]["label_type"] in C_SHARP_STATEMENT
        and graph.nodes[_id]["label_type"] != "break_statement"
    ]

    return build_switch_section_node(args, case_expr, execution_ids)
