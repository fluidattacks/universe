from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.expression_statement import (
    build_expression_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    invalid_types = {
        ";",
    }
    childs_id = adj_ast(graph, args.n_id)
    return build_expression_statement_node(
        args,
        c_ids=(c_id for c_id in childs_id if graph.nodes[c_id]["label_type"] not in invalid_types),
    )
