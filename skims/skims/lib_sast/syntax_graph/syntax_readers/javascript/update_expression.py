from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.unary_expression import (
    build_unary_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = adj_ast(graph, args.n_id)
    val_id = c_ids[0]
    operator = graph.nodes[c_ids[1]].get("label_text") if len(c_ids) >= 1 else "Unary"

    return build_unary_expression_node(args, operator, val_id)
