from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.do_statement import (
    build_do_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    body_id = graph.nodes[args.n_id]["label_field_body"]
    if graph.nodes[body_id]["label_type"] == "expression_statement":
        body_id = match_ast(graph, body_id)["__0__"]

    condition_node = graph.nodes[args.n_id]["label_field_condition"]

    return build_do_statement_node(args, body_id, condition_node)
