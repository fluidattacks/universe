from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.new_expression import (
    build_new_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    childs = match_ast(args.ast_graph, args.n_id, "&")
    const_id = childs.get("__0__")
    if not const_id:
        const_id = adj_ast(args.ast_graph, args.n_id)[0]

    args_id = childs.get("__1__")
    return build_new_expression_node(args, const_id, args_id)
