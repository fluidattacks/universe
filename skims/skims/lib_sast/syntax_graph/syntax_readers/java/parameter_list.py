from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.parameter_list import (
    build_parameter_list_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    valid_parameters = {
        "identifier",
        "formal_parameter",
        "receiver_parameter",
        "spread_parameter",
    }
    graph = args.ast_graph
    c_ids = adj_ast(graph, args.n_id)
    return build_parameter_list_node(
        args,
        c_ids=(_id for _id in c_ids if graph.nodes[_id]["label_type"] in valid_parameters),
    )
