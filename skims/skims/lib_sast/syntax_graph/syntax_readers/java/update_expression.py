from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.unary_expression import (
    build_unary_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = adj_ast(graph, args.n_id)
    exp_type = graph.nodes[c_ids[1]].get("label_text", "UpdateExpression")
    val_id = c_ids[0]

    return build_unary_expression_node(args, exp_type, val_id)
