from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.switch_section import (
    build_switch_section_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    case_n_id = graph.nodes[args.n_id].get("label_field_value")

    case_expr = node_to_str(graph, case_n_id)

    if case_expr.startswith(('"', "'")):
        case_expr = case_expr[1:-1]

    execution_ids: set[NId] = set()

    for n_id in g.adj_ast(graph, args.n_id)[3:]:
        if (graph.nodes[n_id].get("label_type") == "expression_statement") and (
            c_ids := g.adj_ast(graph, n_id)
        ):
            execution_ids.add(c_ids[0])
            continue
        execution_ids.add(n_id)

    return build_switch_section_node(args, case_expr, execution_ids)
