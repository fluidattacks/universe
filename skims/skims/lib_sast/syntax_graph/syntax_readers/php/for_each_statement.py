from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.for_each_statement import (
    build_for_each_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    c_ids = adj_ast(graph, args.n_id)
    if len(c_ids) >= 5:
        iter_item = c_ids[2]
        var_node = c_ids[4]
    else:
        iter_item = c_ids[0]
        var_node = c_ids[-1]

    body_id = graph.nodes[args.n_id].get("label_field_body")

    return build_for_each_statement_node(args, var_node, iter_item, body_id)
