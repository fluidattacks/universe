from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.symbol_lookup import (
    build_symbol_lookup_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    symbol_n_id = args.n_id

    if graph.nodes[args.n_id].get("label_type") == "name" and (
        c_ids := g.adj_ast(graph, args.n_id)
    ):
        symbol_n_id = c_ids[0]

    symbol = args.ast_graph.nodes[symbol_n_id]["label_text"]

    return build_symbol_lookup_node(args, symbol)
