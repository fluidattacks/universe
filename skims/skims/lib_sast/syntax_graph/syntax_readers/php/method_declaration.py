from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.method_declaration import (
    build_method_declaration_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    nodes = graph.nodes
    n_attrs = graph.nodes[args.n_id]

    name_n_id = n_attrs.get("label_field_name")
    name = nodes[name_n_id].get("label_text")

    block_id = n_attrs.get("label_field_body")
    parameters_id = n_attrs.get("label_field_parameters")

    children_nid = {
        "parameters_id": [parameters_id],
    }
    return build_method_declaration_node(args, name, block_id, children_nid)
