from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.switch_body import (
    build_switch_body_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    def_n_id: NId | None = None

    c_ids = g.match_ast_group(graph, args.n_id, "case_statement", "default_statement")

    case_ids = c_ids.get("case_statement", [])

    if def_n_ids := c_ids.get("default_statement"):
        def_n_id = def_n_ids[0]

    return build_switch_body_node(args, iter(case_ids), def_n_id)
