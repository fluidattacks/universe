from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.reserved_word import (
    build_reserved_word_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    return build_reserved_word_node(args, node_to_str(args.ast_graph, args.n_id))
