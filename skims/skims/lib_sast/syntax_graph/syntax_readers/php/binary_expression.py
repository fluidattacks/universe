from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.binary_operation import (
    build_binary_operation_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    nodes = graph.nodes
    operator_id = nodes[args.n_id].get("label_field_operator")
    operator = nodes[operator_id].get("label_text")
    left_id = nodes[args.n_id].get("label_field_left")
    right_id = nodes[args.n_id].get("label_field_right")

    if nodes[left_id].get("label_type") == "parenthesized_expression":
        c_ids = g.match_ast(graph, left_id)
        left_id = c_ids.get("__1__")
    if nodes[right_id].get("label_type") == "parenthesized_expression":
        c_ids = g.match_ast(graph, right_id)
        right_id = c_ids.get("__1__")

    return build_binary_operation_node(args, operator, left_id, right_id)
