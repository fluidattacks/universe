from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.member_access import (
    build_member_access_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    nodes = graph.nodes

    member_n_id = nodes[args.n_id].get("label_field_name")
    name_n_id = g.match_ast_d(graph, member_n_id, "name") or member_n_id

    member = nodes[name_n_id].get("label_text")

    obj_p_id = nodes[args.n_id].get("label_field_scope")

    if expression := nodes[obj_p_id].get("label_text"):
        obj_id = obj_p_id
    else:
        obj_c_ids = g.adj_ast(graph, obj_p_id)
        obj_id = obj_c_ids[0]
        expression = node_to_str(graph, obj_id)

    return build_member_access_node(args, member, expression, obj_id)
