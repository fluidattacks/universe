from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.member_access import (
    build_member_access_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = g.adj_ast(graph, args.n_id)

    relative_n_id = c_ids[0]
    member_n_id = c_ids[-1]

    expression = graph.nodes[member_n_id].get("label_text")

    obj_id = g.match_ast_d(graph, relative_n_id, "self") or member_n_id

    return build_member_access_node(args, "self", expression, obj_id)
