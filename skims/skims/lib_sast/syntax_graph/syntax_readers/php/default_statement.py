from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.switch_section import (
    build_switch_section_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    execution_ids: set[NId] = set()

    for n_id in g.adj_ast(graph, args.n_id)[2:]:
        if (graph.nodes[n_id].get("label_type") == "expression_statement") and (
            c_ids := g.adj_ast(graph, n_id)
        ):
            execution_ids.add(c_ids[0])
            continue
        execution_ids.add(n_id)

    return build_switch_section_node(args, "Default", execution_ids)
