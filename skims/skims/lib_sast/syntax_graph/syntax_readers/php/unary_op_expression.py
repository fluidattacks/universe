from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.unary_expression import (
    build_unary_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = g.match_ast(graph, args.n_id)
    operator_n_id = c_ids.get("__0__")
    operator = graph.nodes[operator_n_id].get("label_text")
    operand = c_ids.get("__1__") or args.n_id

    return build_unary_expression_node(args, operator, operand)
