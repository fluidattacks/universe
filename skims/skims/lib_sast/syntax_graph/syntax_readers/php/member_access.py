from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.member_access import (
    build_member_access_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = g.adj_ast(args.ast_graph, args.n_id)

    obj_id = c_ids[0]
    expression: str = node_to_str(graph, obj_id)

    member_n_id = c_ids[-1]
    member: str = graph.nodes[member_n_id].get("label_text")

    return build_member_access_node(args, member, expression, obj_id)
