from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.argument_list import (
    build_argument_list_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    invalid_types = {
        "echo",
        ",",
        ";",
    }

    nested_types = {"sequence_expression"}

    p_id = g.pred_ast(graph, args.n_id)[0]

    valid_childs: set[NId] = set()
    for c_id in g.adj_ast(args.ast_graph, p_id):
        if (label_type := graph.nodes[c_id].get("label_type")) in invalid_types:
            continue
        if label_type in nested_types:
            clean_nested_childs = [
                n_id
                for n_id in g.adj_ast(graph, c_id)
                if graph.nodes[n_id].get("label_type") not in invalid_types
            ]
            valid_childs.update(clean_nested_childs)
            continue
        valid_childs.add(c_id)

    return build_argument_list_node(args, iter(valid_childs))
