from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.string_literal import (
    build_string_literal_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    value: str = ""
    text_id = g.match_ast_d(graph, args.n_id, "text")
    if text_id:
        value = node_to_str(graph, text_id)

    return build_string_literal_node(args, value)
