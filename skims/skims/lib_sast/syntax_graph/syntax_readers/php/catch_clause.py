from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.catch_clause import (
    build_catch_clause_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def reader(args: SyntaxGraphArgs) -> NId:
    n_attrs = args.ast_graph.nodes[args.n_id]
    block_id = n_attrs.get("label_field_body")
    param_id = n_attrs.get("label_field_name")

    return build_catch_clause_node(args, block_id, param_id)
