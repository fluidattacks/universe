from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.if_statement import (
    build_if_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]
    condition_id = n_attrs["label_field_condition"]
    true_id = n_attrs["label_field_body"]

    return build_if_node(args, condition_id, true_id)
