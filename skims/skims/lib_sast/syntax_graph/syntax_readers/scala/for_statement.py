from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.for_statement import (
    build_for_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    get_nodes_by_path,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]
    body_id = n_attrs["label_field_body"]

    var_nodes = get_nodes_by_path(graph, args.n_id, (), "enumerators", "enumerator", "identifier")
    var_node = var_nodes[0] if var_nodes else None

    condition_nodes = get_nodes_by_path(
        graph,
        args.n_id,
        (),
        "enumerators",
        "enumerator",
        "infix_expression",
    )
    condition_node = condition_nodes[0] if condition_nodes else None

    return build_for_statement_node(args, var_node, condition_node, None, body_id)
