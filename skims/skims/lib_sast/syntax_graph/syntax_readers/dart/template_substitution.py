from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.expression_statement import (
    build_expression_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    c_ids = [
        child
        for child in adj_ast(graph, args.n_id)
        if graph.nodes[child]["label_type"] not in {"$", "{", "}"}
    ]

    return build_expression_statement_node(args, iter(c_ids))
