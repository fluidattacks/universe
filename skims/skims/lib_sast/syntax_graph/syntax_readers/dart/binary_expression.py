from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.binary_operation import (
    build_binary_operation_node,
)
from lib_sast.syntax_graph.syntax_nodes.expression_statement import (
    build_expression_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = adj_ast(graph, args.n_id)
    invalid_childs = {";", "type_test", "(", ")"}

    if len(c_ids) == 3:
        operator = node_to_str(graph, c_ids[1])
        return build_binary_operation_node(args, operator, c_ids[0], c_ids[2])

    return build_expression_statement_node(
        args,
        c_ids=(_id for _id in c_ids if graph.nodes[_id]["label_type"] not in invalid_childs),
    )
