from lib_sast.syntax_graph.syntax_readers.hcl import (
    attribute as hcl_attribute,
)
from lib_sast.syntax_graph.syntax_readers.hcl import (
    block as hcl_block,
)
from lib_sast.syntax_graph.syntax_readers.hcl import (
    config_file as hcl_config_file,
)
from lib_sast.syntax_graph.syntax_readers.hcl import (
    expression as hcl_expression,
)
from lib_sast.syntax_graph.syntax_readers.hcl import (
    function_arguments as hcl_function_arguments,
)
from lib_sast.syntax_graph.syntax_readers.hcl import (
    identifier as hcl_identifier,
)
from lib_sast.syntax_graph.types import (
    Dispatcher,
)

HCL_DISPATCHERS: Dispatcher = {
    "attribute": hcl_attribute.reader,
    "object_elem": hcl_attribute.reader,
    "block": hcl_block.reader,
    "config_file": hcl_config_file.reader,
    "expression": hcl_expression.reader,
    "function_arguments": hcl_function_arguments.reader,
    "identifier": hcl_identifier.reader,
}
