from lib_sast.syntax_graph.syntax_readers.ruby import (
    argument_list as ruby_argument_list,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    array_node as ruby_array_node,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    assignment as ruby_assignment,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    binary_expression as ruby_binary_expression,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    boolean_literal as ruby_boolean_literal,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    call as ruby_call,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    class_definition as ruby_class_definition,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    comment as ruby_comment,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    execution_block as ruby_execution_block,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    for_statement as ruby_for_statement,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    hash as ruby_hash,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    identifier as ruby_identifier,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    if_conditional as ruby_if_conditional,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    if_modifier as ruby_if_modifier,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    integer as ruby_integer,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    method_declaration as ruby_method_declaration,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    method_parameters as ruby_method_parameters,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    module_definition as ruby_module_definition,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    pair as ruby_pair,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    parameter as ruby_parameter,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    program as ruby_program,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    return_statement as ruby_return_statement,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    string_literal as ruby_string_literal,
)
from lib_sast.syntax_graph.syntax_readers.ruby import (
    while_statement as ruby_while_statement,
)
from lib_sast.syntax_graph.types import (
    Dispatcher,
)

RUBY_DISPATCHERS: Dispatcher = {
    "array": ruby_array_node.reader,
    "argument_list": ruby_argument_list.reader,
    "assignment": ruby_assignment.reader,
    "binary": ruby_binary_expression.reader,
    "block_parameter": ruby_parameter.reader,
    "body_statement": ruby_execution_block.reader,
    "call": ruby_call.reader,
    "class": ruby_class_definition.reader,
    "comment": ruby_comment.reader,
    "constant": ruby_identifier.reader,
    "do": ruby_execution_block.reader,
    "else": ruby_execution_block.reader,
    "elsif": ruby_if_conditional.reader,
    "false": ruby_boolean_literal.reader,
    "for": ruby_for_statement.reader,
    "hash": ruby_hash.reader,
    "hash_key_symbol": ruby_identifier.reader,
    "hash_splat_parameter": ruby_parameter.reader,
    "identifier": ruby_identifier.reader,
    "if_modifier": ruby_if_modifier.reader,
    "if": ruby_if_conditional.reader,
    "integer": ruby_integer.reader,
    "method": ruby_method_declaration.reader,
    "method_parameters": ruby_method_parameters.reader,
    "module": ruby_module_definition.reader,
    "optional_parameter": ruby_parameter.reader,
    "pair": ruby_pair.reader,
    "program": ruby_program.reader,
    "regex": ruby_string_literal.reader,
    "return": ruby_return_statement.reader,
    "simple_symbol": ruby_identifier.reader,
    "singleton_class": ruby_class_definition.reader,
    "singleton_method": ruby_method_declaration.reader,
    "splat_parameter": ruby_parameter.reader,
    "string": ruby_string_literal.reader,
    "then": ruby_execution_block.reader,
    "true": ruby_boolean_literal.reader,
    "while": ruby_while_statement.reader,
    "while_modifier": ruby_while_statement.reader,
}
