from lib_sast.syntax_graph.syntax_readers.scala import (
    argument_list as scala_argument_list,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    assignment as scala_assignment,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    binary_expression as scala_binary_expression,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    boolean_literal as scala_boolean_literal,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    call_expression as scala_call_expression,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    catch_declaration as scala_catch_declaration,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    class_definition as scala_class_definition,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    comment as scala_comment,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    conditional_expression as scala_conditional_expression,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    do_statement as scala_do_statement,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    except_clause as scala_except_clause,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    execution_block as scala_execution_block,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    finally_clause as scala_finally_clause,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    for_statement as scala_for_statement,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    identifier as scala_identifier,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    method_declaration as scala_method_declaration,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    number_literal as scala_number_literal,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    object as scala_object,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    parameter as scala_parameter,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    parenthesized_expression as scala_parenthesized_expression,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    program as scala_program,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    return_statement as scala_return_statement,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    string_literal as scala_string_literal,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    switch_statement as scala_switch_statement,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    try_statement as scala_try_statement,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    variable_declaration as scala_variable_declaration,
)
from lib_sast.syntax_graph.syntax_readers.scala import (
    while_statement as scala_while_statement,
)
from lib_sast.syntax_graph.types import (
    Dispatcher,
)

SCALA_DISPATCHERS: Dispatcher = {
    "finally_clause": scala_finally_clause.reader,
    "parameters": scala_argument_list.reader,
    "arguments": scala_argument_list.reader,
    "function_definition": scala_method_declaration.reader,
    "comment": scala_comment.reader,
    "block_comment": scala_comment.reader,
    "template_body": scala_execution_block.reader,
    "block": scala_execution_block.reader,
    "case_block": scala_execution_block.reader,
    "call_expression": scala_call_expression.reader,
    "class_definition": scala_class_definition.reader,
    "if_expression": scala_conditional_expression.reader,
    "val_definition": scala_variable_declaration.reader,
    "var_definition": scala_variable_declaration.reader,
    "boolean_literal": scala_boolean_literal.reader,
    "integer_literal": scala_number_literal.reader,
    "floating_point_literal": scala_number_literal.reader,
    "for_expression": scala_for_statement.reader,
    "character_literal": scala_string_literal.reader,
    "string": scala_string_literal.reader,
    "interpolated_string_expression": scala_string_literal.reader,
    "identifier": scala_identifier.reader,
    "object_definition": scala_object.reader,
    "compilation_unit": scala_program.reader,
    "assignment_expression": scala_assignment.reader,
    "while_expression": scala_while_statement.reader,
    "infix_expression": scala_binary_expression.reader,
    "parenthesized_expression": scala_parenthesized_expression.reader,
    "do_while_expression": scala_do_statement.reader,
    "match_expression": scala_switch_statement.reader,
    "parameter": scala_parameter.reader,
    "catch_clause": scala_except_clause.reader,
    "case_clause": scala_catch_declaration.reader,
    "try_expression": scala_try_statement.reader,
    "return_expression": scala_return_statement.reader,
}
