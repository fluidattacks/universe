from lib_sast.syntax_graph.syntax_readers.json import (
    array_node as json_array,
)
from lib_sast.syntax_graph.syntax_readers.json import (
    boolean as json_boolean,
)
from lib_sast.syntax_graph.syntax_readers.json import (
    comment as json_comment,
)
from lib_sast.syntax_graph.syntax_readers.json import (
    document as json_document,
)
from lib_sast.syntax_graph.syntax_readers.json import (
    number as json_number,
)
from lib_sast.syntax_graph.syntax_readers.json import (
    object as json_object,
)
from lib_sast.syntax_graph.syntax_readers.json import (
    pair as json_pair,
)
from lib_sast.syntax_graph.syntax_readers.json import (
    string_node as json_string,
)
from lib_sast.syntax_graph.types import (
    Dispatcher,
)

JSON_DISPATCHERS: Dispatcher = {
    "array": json_array.reader,
    "false": json_boolean.reader,
    "true": json_boolean.reader,
    "comment": json_comment.reader,
    "document": json_document.reader,
    "number": json_number.reader,
    "object": json_object.reader,
    "pair": json_pair.reader,
    "string": json_string.reader,
}
