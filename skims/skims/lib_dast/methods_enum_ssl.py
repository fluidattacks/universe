from enum import (
    Enum,
)

from model.core import (
    DeveloperEnum,
    FindingEnum,
    MethodInfo,
    MethodOriginEnum,
    VulnerabilityTechnique,
)


class MethodsEnumSSL(Enum):
    SSLV3_ENABLED = MethodInfo(
        file_name="analyze_protocol",
        name="sslv3_enabled",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    TLSV1_ENABLED = MethodInfo(
        file_name="analyze_protocol",
        name="tlsv1_enabled",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    TLSV1_1_ENABLED = MethodInfo(
        file_name="analyze_protocol",
        name="tlsv1_1_enabled",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    TLSV1_2_OR_HIGHER_DISABLED = MethodInfo(
        file_name="analyze_protocol",
        name="tlsv1_2_or_higher_disabled",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    FALLBACK_SCSV_DISABLED = MethodInfo(
        file_name="analyze_protocol",
        name="fallback_scsv_disabled",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    TLSV1_3_DOWNGRADE = MethodInfo(
        file_name="analyze_protocol",
        name="tlsv1_3_downgrade",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HEARTBLEED_POSSIBLE = MethodInfo(
        file_name="analyze_protocol",
        name="heartbleed_possible",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    FREAK_POSSIBLE = MethodInfo(
        file_name="analyze_protocol",
        name="freak_possible",
        module="lib_dast/ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    WEAK_CIPHERS_ALLOWED = MethodInfo(
        file_name="analyze_protocol",
        name="weak_ciphers_allowed",
        module="lib_dast/ssl",
        finding=FindingEnum.F052,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        cvss="CVSS:3.1/AV:A/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N/E:U/RL:O/RC:C",
        cvss_v4=("CVSS:4.0/AV:A/AT:N/AC:H/PR:N/UI:N/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:U"),
        cwe_ids=["CWE-327"],
        auto_approve=True,
    )
    CBC_ENABLED = MethodInfo(
        file_name="analyze_protocol",
        name="cbc_enabled",
        module="lib_dast/ssl",
        finding=FindingEnum.F094,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    PFS_DISABLED = MethodInfo(
        file_name="analyze_protocol",
        name="pfs_disabled",
        module="lib_dast/ssl",
        finding=FindingEnum.F133,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SSL_CERTIFICATE_EXPIRED = MethodInfo(
        file_name="analyze_certificate",
        name="ssl_certificate_expired",
        module="lib_dast/ssl",
        finding=FindingEnum.F313,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        cwe_ids=["CWE-298"],
        auto_approve=True,
    )
    SSL_SELF_SIGNED_CERTIFICATE = MethodInfo(
        file_name="analyze_certificate",
        name="ssl_self_signed_certificate",
        module="lib_dast/ssl",
        finding=FindingEnum.F313,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        cwe_ids=["CWE-296"],
        auto_approve=True,
    )
    SSL_WRONG_CN = MethodInfo(
        file_name="analyze_certificate",
        name="ssl_wrong_cn",
        module="lib_dast/ssl",
        finding=FindingEnum.F313,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        cwe_ids=["CWE-297"],
        auto_approve=True,
    )
    SSL_WILDCARD_CERTIFICATE = MethodInfo(
        file_name="analyze_certificate",
        name="ssl_wildcard_certificate",
        module="lib_dast/ssl",
        finding=FindingEnum.F313,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        cwe_ids=["CWE-297"],
        auto_approve=True,
    )
