from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_dast.ssl.model import (
    SSLCertificateVulnerability,
    SSLSettings,
    SSLVulnerability,
)
from model.core import (
    LocalesEnum,
)
from utils.cipher_suites import (
    SSLVersionId,
    SSLVersionName,
)


def ssl_name2ssl_id(ssl_name: SSLVersionName) -> int:
    return getattr(SSLVersionId, ssl_name.name).value


def ssl_id2ssl_name(ssl_id: SSLVersionId) -> str:
    return getattr(SSLVersionName, ssl_id.name).value


def get_server(ssl_vulnerability: SSLVulnerability, locale: LocalesEnum) -> str:
    if locale == LocalesEnum.ES:
        return f"Servidor: {ssl_vulnerability.get_context()}"
    return f"Server: {ssl_vulnerability.get_context()}"


def get_versions(ssl_vulnerability: SSLVulnerability, locale: LocalesEnum) -> str:
    tls_vers = ssl_vulnerability.get_context().get_supported_tls_versions()
    versions = ", ".join([ssl_id2ssl_name(v_id) for v_id in tls_vers])
    if locale == LocalesEnum.ES:
        return f"Versiones disponibles en servidor: {versions}"
    return f"Available versions on server: {versions}"


def get_intention_title(locale: LocalesEnum) -> str:
    if locale == LocalesEnum.ES:
        return "-------------------------------Intención-------------------------------"
    return "-------------------------------Intention-------------------------------"


def get_intention(ssl_vulnerability: SSLVulnerability, locale: LocalesEnum) -> str:
    return ssl_vulnerability.get_intention(locale)


def get_request_title(locale: LocalesEnum) -> str:
    if locale == LocalesEnum.ES:
        return "-----------Petición realizada con los siguientes parámetros------------"
    return "--------------Request made with the following parameters---------------"


def get_request(ssl_vulnerability: SSLVulnerability, locale: LocalesEnum) -> str:
    ssl_settings: SSLSettings = ssl_vulnerability.ssl_settings

    tls_version = ssl_id2ssl_name(ssl_settings.tls_version)
    key_exchange = ", ".join(ssl_settings.key_exchange_names)
    authentication = ", ".join(ssl_settings.authentication_names)
    cipher = ", ".join(ssl_settings.cipher_names)
    ssl_hash = ", ".join(ssl_settings.hash_names)
    if locale == LocalesEnum.ES:
        return (
            f"Versión TLS: {tls_version}\n"
            f"Intercambio de llaves: {key_exchange}\n"
            f"Autenticación: {authentication}\n"
            f"Encripción: {cipher}\n"
            f"Hash: {ssl_hash}"
        )
    return (
        f"TLS version: {tls_version}\n"
        f"Key exchange: {key_exchange}\n"
        f"Authentication: {authentication}\n"
        f"Cipher: {cipher}\n"
        f"Hash: {ssl_hash}"
    )


def get_response_title(locale: LocalesEnum) -> str:
    if locale == LocalesEnum.ES:
        return "--------------------Respuesta obtenida del servidor--------------------"
    return "---------------------Response obtained from server---------------------"


def get_response_en(ssl_vulnerability: SSLVulnerability) -> str:
    response = ssl_vulnerability.server_response

    if response is None:
        return "Result: UNSUCCESSFUL_CONNECTION"
    if response.alert is not None:
        level = response.alert.level.name
        description = response.alert.description.name
        return f"Result: CONNECTION_FAILED\nType: ALERT\nLevel: {level}\nDescription: {description}"
    if response.handshake is not None and response.handshake.cipher_suite is not None:
        version = ssl_id2ssl_name(response.handshake.version_id)
        iana = response.handshake.cipher_suite.iana_name
        openssl = response.handshake.cipher_suite.get_openssl_name()
        code = response.handshake.cipher_suite.get_code_str()
        vulns = response.handshake.cipher_suite.get_vuln_str()
        return (
            "Result: CONNECTION_SUCCESS\n"
            f"TLS version: {version}\n"
            f"Selected cipher suite: {iana}\n"
            f"    Openssl name: {openssl}\n"
            f"    Code: {code}\n"
            f"    Vulnerabilities: {vulns}"
        )

    return "NONE"


def get_response_es(ssl_vulnerability: SSLVulnerability) -> str:
    response = ssl_vulnerability.server_response

    if response is None:
        return "Resultado: UNSUCCESSFUL_CONNECTION"

    if response.alert is not None:
        level = response.alert.level.name
        description = response.alert.description.name
        return (
            f"Resultado: CONNECTION_FAILED\nTipo: ALERT\nNivel: {level}\nDescripción: {description}"
        )

    if response.handshake is not None and response.handshake.cipher_suite is not None:
        version = ssl_id2ssl_name(response.handshake.version_id)
        iana = response.handshake.cipher_suite.iana_name
        openssl = response.handshake.cipher_suite.get_openssl_name()
        code = response.handshake.cipher_suite.get_code_str()
        vulns = response.handshake.cipher_suite.get_vuln_str()
        return (
            "Resultado: CONNECTION_SUCCESS\n"
            f"Versión TLS: {version}\n"
            f"Suite de cifrado seleccionada: {iana}\n"
            f"    Nombre openssl: {openssl}\n"
            f"    Código: {code}\n"
            f"    Vulnerabilidades: {vulns}"
        )

    return "NONE"


def get_conclusion_title(locale: LocalesEnum) -> str:
    if locale == LocalesEnum.ES:
        return "------------------------------Conclusión-------------------------------"
    return "------------------------------Conclusion-------------------------------"


def construct_snippet(ssl_vulnerability: SSLVulnerability, locale: LocalesEnum) -> str:
    server = get_server(ssl_vulnerability, locale)
    versions = get_versions(ssl_vulnerability, locale)
    intention = get_intention(ssl_vulnerability, locale)
    request = get_request(ssl_vulnerability, locale)
    if locale == LocalesEnum.ES:
        response = get_response_es(ssl_vulnerability)
    else:
        response = get_response_en(ssl_vulnerability)
    conclusion = ssl_vulnerability.description
    return (
        f"{server}\n"
        f"{versions}\n"
        f"{get_intention_title(locale)}\n"
        f"{intention}\n"
        f"{get_request_title(locale)}\n"
        f"{request}\n"
        f"{get_response_title(locale)}\n"
        f"{response}\n"
        f"{get_conclusion_title(locale)}\n"
        f"{conclusion}\n"
    )


def snippet(locale: LocalesEnum, ssl_vulnerability: SSLVulnerability) -> str:
    return make_snippet(
        content=construct_snippet(ssl_vulnerability, locale),
        viewport=SnippetViewport(line=0, column=0, wrap=True),
    ).content


CERTIFICATE_TRANSLATIONS = {
    LocalesEnum.ES: {
        "title": "Validación de certificado",
        "intention": "Preparando conexión con dominio:",
        "response": "Analizando vulnerabilidades en el certificado X509",
    },
    LocalesEnum.EN: {
        "title": "Certificate validation",
        "intention": "Preparing connection with domain:",
        "response": "Checking X509 certificate for vulnerabilities",
    },
}


def construct_certificate_snippet(
    ssl_vulnerability: SSLCertificateVulnerability,
    locale: LocalesEnum,
) -> str:
    title = CERTIFICATE_TRANSLATIONS[locale]["title"]
    intention = CERTIFICATE_TRANSLATIONS[locale]["intention"]
    response = CERTIFICATE_TRANSLATIONS[locale]["response"]
    conclusion = ssl_vulnerability.description
    return (
        f"{title}\n"
        f"{get_intention_title(locale)}\n{intention}\n"
        f"{ssl_vulnerability.host}\n"
        f"{get_response_title(locale)}\n{response}\n"
        f"{get_conclusion_title(locale)}\n{conclusion}\n"
    )
