import textwrap
from collections.abc import (
    Callable,
)
from contextlib import (
    suppress,
)
from typing import (
    NamedTuple,
)

from dns import (
    exception,
    resolver,
)
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_dast.http.model import (
    URLContext,
)
from lib_dast.methods_enum_http import (
    MethodsEnumHTTP as MethodsEnum,
)
from model.core import (
    FindingEnum,
    HTTPProperties,
    LocalesEnum,
    Vulnerabilities,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.translations import (
    t,
)


class DNSCheckCtx(NamedTuple):
    url_ctx: URLContext


class Location(NamedTuple):
    description: str
    snippet: str


class Locations(NamedTuple):
    locations: list[Location]

    def append(
        self,
        method: MethodsEnum,
        snippet: str,
        **desc_kwargs: LocalesEnum,
    ) -> None:
        self.locations.append(
            Location(
                description=t(
                    method.name,
                    **desc_kwargs,
                ),
                snippet=snippet,
            ),
        )


def _add_dns_location(
    domain: str,
    locations: Locations,
    method: MethodsEnum,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                $ python3.11

                >>> # We'll use the version 2.6.1 of "dnspython"
                >>> from dns import resolver

                >>> # This object will help us analyze the domain
                >>> resolution = resolver.Resolver()

                >>> # Check TXT records in the domain
                >>> resolution.resolve(
                >>>   "_dmarc." + {domain!r}, "TXT",
                >>>   lifetime=2.0
                >>> )

                []  # Empty list means no dmarc record exist in the domain
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=15, wrap=True),
        ).content,
    )


def _build_dns_vulnerabilities(
    ctx: DNSCheckCtx,
    locations: Locations,
    method: MethodsEnum,
) -> Vulnerabilities:
    return tuple(
        build_inputs_vuln(
            method=method.value,
            stream="home,response,dns",
            what=ctx.url_ctx.original_url,
            where=location.description,
            metadata=build_metadata(
                method=method.value,
                description=(
                    f"{location.description} {t(key='words.in')} {ctx.url_ctx.original_url}"
                ),
                snippet=location.snippet,
                http_properties=HTTPProperties(
                    has_redirect=ctx.url_ctx.has_redirect,
                    original_url=ctx.url_ctx.original_url,
                ),
            ),
        )
        for location in locations.locations
    )


def _query_dns(domain: str) -> list:
    resolution = resolver.Resolver()
    resolution.resolve(domain, "TXT", lifetime=2.0)
    try:
        dmarc_resolution = resolution.resolve(f"_dmarc.{domain}", "TXT", lifetime=2.0)
        records = [res.strings for res in dmarc_resolution]
        _resource_record = [b"".join(record) for record in records if record]
        records = [r.decode() for r in _resource_record]
    except resolver.NXDOMAIN:
        records = []

    return records


def _check_dns_records(ctx: DNSCheckCtx) -> Vulnerabilities:
    locations = Locations(locations=[])
    method = MethodsEnum.CHECK_DNS_RECORDS
    domain = ctx.url_ctx.get_base_domain()
    with suppress(exception.DNSException):
        records = _query_dns(domain)
        if len(records) == 0:
            _add_dns_location(domain, locations, method)

    return _build_dns_vulnerabilities(
        locations=locations,
        ctx=ctx,
        method=method,
    )


def get_check_ctx(url: URLContext) -> DNSCheckCtx:
    return DNSCheckCtx(
        url_ctx=url,
    )


CHECKS: dict[
    FindingEnum,
    list[Callable[[DNSCheckCtx], Vulnerabilities]],
] = {
    FindingEnum.F182: [_check_dns_records],
}
