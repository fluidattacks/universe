from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    XPoweredByHeader,
)


def _is_x_powered_by(name: str) -> bool:
    return name.lower() == "x-powered-by"


def parse(line: str) -> XPoweredByHeader | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_x_powered_by(name):
        return None

    return XPoweredByHeader(
        name=name,
        value=value,
    )
