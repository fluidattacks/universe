from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    PermissionsPolicyHeader,
)


def _is_permissions_policy(name: str) -> bool:
    return name.lower() == "permissions-policy"


def parse(line: str) -> PermissionsPolicyHeader | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_permissions_policy(name):
        return None

    return PermissionsPolicyHeader(
        name=name,
        value=value,
    )
