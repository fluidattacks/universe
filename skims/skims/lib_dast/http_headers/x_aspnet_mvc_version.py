from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    XAspNetMvcVersionHeader,
)


def _is_aspnet_version(name: str) -> bool:
    return name.lower() == "x-aspnetmvc-version"


def parse(line: str) -> XAspNetMvcVersionHeader | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_aspnet_version(name):
        return None

    return XAspNetMvcVersionHeader(name=name, value=value)
