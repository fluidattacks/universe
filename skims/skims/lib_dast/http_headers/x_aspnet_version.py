from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    XAspNetVersionHeader,
)


def _is_aspnet_version(name: str) -> bool:
    return name.lower() == "x-aspnet-version"


def parse(line: str) -> XAspNetVersionHeader | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_aspnet_version(name):
        return None

    return XAspNetVersionHeader(name=name, value=value)
