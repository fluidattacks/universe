from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    XXSSProtectionHeader,
)


def _is_x_xss_protection(name: str) -> bool:
    return name.lower() == "x-xss-protection"


def parse(line: str) -> XXSSProtectionHeader | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_x_xss_protection(name):
        return None

    return XXSSProtectionHeader(
        name=name,
        value=value.split(";")[0],
    )
