import socket
from contextlib import (
    suppress,
)
from string import (
    whitespace,
)

from bs4 import (
    BeautifulSoup,
)
from ntplib import (
    NTPClient,
)
from utils.logs import (
    log_blocking,
)


def get_offset() -> float | None:
    with suppress(Exception):
        response = NTPClient().request("pool.ntp.org", port=123, version=3)
        return response.offset
    return None


def tcp_connect(
    hostname: str,
    port: int,
    intention: str = "establish tcp connection",
) -> socket.socket | None:
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(30)
        sock.connect((hostname, port))
    except OSError as error:
        log_blocking(
            "warning",
            "%s occurred in tcp connect with %s:%d trying to %s",
            type(error).__name__,
            hostname,
            port,
            intention,
        )
        return None
    else:
        return sock


def tcp_read(sock: socket.socket, size: int) -> bytes | None:
    try:
        return sock.recv(size)
    except OSError as error:
        log_blocking(
            "warning",
            "%s occurred reading socket",
            type(error).__name__,
        )
        return None


def is_html(string: str, soup: BeautifulSoup | None = None) -> bool:
    string = string.strip(whitespace)
    # Check if it is a json file
    if string.startswith("{"):
        return False
    if soup is None:
        soup = BeautifulSoup(string, "html.parser")
    return soup.find("html", recursive=False) is not None
