from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


def _get_vuln_db_instances(
    response: dict[str, Any],
) -> Vulnerabilities:
    method = MethodsEnum.HAS_PUBLIC_INSTANCES
    db_instances = response.get("DBInstances", []) if response else []
    vulns: Vulnerabilities = ()
    for index, instance in enumerate(db_instances):
        instance_arn = instance["DBInstanceArn"]

        if instance.get("PubliclyAccessible", False):
            locations = [
                Location(
                    access_patterns=(f"/{index}/PubliclyAccessible",),
                    arn=(f"{instance_arn}"),
                    values=(instance["PubliclyAccessible"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_instances,
            )
    return vulns


def _get_vulns_db_clusters(
    response: dict[str, Any],
) -> Vulnerabilities:
    method = MethodsEnum.HAS_PUBLIC_CLUSTER
    db_clusters = response.get("DBClusters", []) if response else []
    vulns: Vulnerabilities = ()
    for index, clusters in enumerate(db_clusters):
        cluster_arn = clusters["DBClusterArn"]
        if clusters.get("PubliclyAccessible", False):
            locations = [
                Location(
                    access_patterns=(f"/{index}/PubliclyAccessible",),
                    arn=(f"{cluster_arn}"),
                    values=(clusters["PubliclyAccessible"],),
                    method=method,
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_clusters,
            )

    return vulns


@SHIELD
async def has_public_instances(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    describe_db_instances: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_instances",
        paginated_results_key="DBInstances",
    )
    vulns = _get_vuln_db_instances(describe_db_instances)

    return (vulns, True)


@SHIELD
async def has_public_cluster(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    describe_db_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="rds",
        function="describe_db_clusters",
        region=region,
        paginated_results_key="DBClusters",
    )
    vulns = _get_vulns_db_clusters(describe_db_clusters)

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    has_public_instances,
    has_public_cluster,
)
