import functools
import sys
import traceback
from collections.abc import (
    Callable,
)
from typing import (
    Any,
    TypeVar,
    cast,
)

from lib_cspm.aws.model import (
    BotoClientError,
)
from model.core import (
    Vulnerabilities,
)
from utils.logs import (
    log,
    log_to_remote,
)

# Constants
Tfun = TypeVar("Tfun", bound=Callable[..., Any])
AUTH_ERRORS = {
    "(AuthorizationError)",
    "(AccessDenied)",
    "(UnauthorizedOperation)",
}


def shield_cspm_aws_decorator(function: Tfun) -> Tfun:
    @functools.wraps(function)
    async def wrapper(
        *args: Any,  # noqa: ANN401
        **kwargs: Any,  # noqa: ANN401
    ) -> tuple[Vulnerabilities, bool | str]:
        try:
            return await function(*args, **kwargs)
        except BotoClientError:
            if any(auth_error in str(traceback.format_exc()) for auth_error in AUTH_ERRORS):
                return ((), f"AccessDenied error in aws.{function.__name__}")
            await log(
                "warning",
                "Unable to obtain AWS service information in %s",
                function.__name__,
            )
            return ((), f"aws.{function.__name__}")
        except Exception:  # noqa: BLE001
            exc_type, exc_value, exc_traceback = sys.exc_info()

            await log(
                "error",
                "Function %s failed\n%s",
                function.__name__,
                traceback.format_exc(),
            )

            await log_to_remote(
                msg=(exc_type, exc_value, exc_traceback),
                severity="error",
                function_id=function.__name__,
            )
            return ((), f"aws.{function.__name__}")

    return cast(Tfun, wrapper)


SHIELD: Callable = shield_cspm_aws_decorator
