import fnmatch
import functools
import hashlib
import json
from collections.abc import (
    Callable,
    Iterable,
)
from datetime import (
    UTC,
    datetime,
)
from typing import (
    Any,
    TypeVar,
    cast,
)

import aioboto3
import aiocache
import botocore
from aiocache.serializers import (
    PickleSerializer,
)
from botocore.exceptions import (
    ClientError as BotocoreClientError,
)
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from json_source_map import (
    calculate,
    types,
)
from lib_cspm.aws.model import (
    BotoClientError,
    Location,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)
from utils.aws.iam import (
    ARN,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.logs import (
    log,
)
from utils.translations import (
    t,
)

# Constants
Tfun = TypeVar("Tfun", bound=Callable[..., Any])

UTC_NOW: datetime = datetime.now(UTC)


def build_arn(
    *,
    service: str,
    resource: str,
    parameters: dict[str, str],
) -> str:
    arn = ARN[service][resource].replace("$", "")
    return arn.format(**parameters)


def _get_action(item: dict, value: str) -> list | str:
    if isinstance(item[value], str):
        return [item[value]]
    return item[value]


def _is_action_permissive(action: Any) -> bool:  # noqa: ANN401
    if not isinstance(action, str):
        # A var or syntax error
        return False

    splitted = action.split(":", 1)  # a:b
    provider = splitted[0]  # a
    effect = splitted[1] if splitted[1:] else None  # b

    return (
        (provider == "*")
        or (effect and effect.startswith("*"))
        or ("*" in provider and effect is None)
    )


def sanitize(specific: str) -> str:
    if "-1" in specific or "-" in specific or "," in specific:
        sanitized = specific.replace("-", "_")
        sanitized = sanitized.replace(" _1", " (-1)")
        return sanitized.replace(",", "")
    return specific


def _build_where(location: Location) -> str:
    if len(location.access_patterns) == 1:
        return sanitize(f"{location.access_patterns[0]}: {location.values[0]}")
    return "; ".join(
        [
            sanitize(f"{path}: {location.values[index_path]}")
            for index_path, path in enumerate(location.access_patterns)
        ],
    )


def evaluate_condition(location: Location, json_paths: dict) -> types.Location:
    access_pattern = json_paths.get(location.access_patterns[-1])
    not_list_defined = "/".join(location.access_patterns[-1].split("/")[:-1])
    has_key = access_pattern.key_start if access_pattern else json_paths[not_list_defined].key_start
    if has_key:
        return has_key
    return json_paths[location.access_patterns[-1]].value_start


def build_vulnerabilities(
    locations: Iterable[Location],
    method: MethodsEnum,
    aws_response: dict[str, dict | list] | list[dict],
) -> Vulnerabilities:
    str_content = json.dumps(aws_response, indent=4, default=str)
    json_paths = calculate(str_content)

    vulns: Vulnerabilities = ()
    for location in locations:
        description = (
            f"{t(location.method.name, **location.desc_params)} {t(key='words.in')} {location.arn}"
        )

        vulns += (
            build_inputs_vuln(
                method=method.value,
                what=location.arn,
                where=_build_where(location) if location.access_patterns else sanitize(description),
                stream="skims",
                metadata=build_metadata(
                    method=method.value,
                    description=description,
                    snippet=make_snippet(
                        content=str_content,
                        viewport=SnippetViewport(
                            column=evaluate_condition(
                                location,
                                json_paths,
                            ).column
                            if location.access_patterns
                            else 0,
                            line=evaluate_condition(location, json_paths).line + 1
                            if location.access_patterns
                            else 0,
                            wrap=True,
                        ),
                    ).content,
                    skip=False,
                ),
            ),
        )

    return vulns


def custom_key_builder(_func: Any, *args: Any, **kwargs: Any) -> str:  # noqa: ANN401
    args = tuple(arg if not isinstance(arg, set) else list(arg) for arg in args)
    kwargs = {
        key: value if not isinstance(value, set) else list(value) for key, value in kwargs.items()
    }
    input_data = (args, kwargs)
    input_json = json.dumps(input_data, sort_keys=True)
    hash_generator = hashlib.sha256()
    hash_generator.update(input_json.encode("utf-8"))
    return hash_generator.hexdigest()


AUTH_ERRORS = {
    "(AuthorizationError)",
    "(AccessDenied)",
    "(UnauthorizedOperation)",
}

CONNECTION_ERRORS = {
    """Could not connect to the endpoint URL: "https://bedrock.""",
}

NON_EXCLUSIVE_ERRORS = {
    "(NoSuchBucketPolicy)",
    "(TrailNotFoundException)",
}


async def handle_botocore_exceptions_client_errors(
    exc: BotocoreClientError,
    service: str,
    region: str | None = None,
) -> dict[str, Any]:
    if any(auth_error in str(exc) for auth_error in AUTH_ERRORS):
        msg = f"Credentials do not have permissions to access {service}"
        if region:
            msg += f" in {region}"
        await log("warning", msg)
        raise BotoClientError from exc
    if "(AccessDeniedException)" in str(exc):
        msg = f"Credentials do not have permissions to access {service}"
        if region:
            msg += f" in {region}"
        await log("warning", msg)
    if any(auth_error in str(exc) for auth_error in NON_EXCLUSIVE_ERRORS):
        return {}
    raise BotoClientError from exc


def handle_run_boto3_exceptions(_run_boto3_fun: Tfun) -> Tfun:
    @functools.wraps(_run_boto3_fun)
    async def wrapper(  # noqa: PLR0913
        credentials: AwsCredentials,
        service: str,
        function: str,
        region: str | None = None,
        parameters: dict[str, object] | None = None,
        paginated_results_key: str | None = None,
    ) -> dict[str, dict | list]:
        try:
            return await _run_boto3_fun(
                credentials=credentials,
                service=service,
                function=function,
                region=region,
                parameters=parameters,
                paginated_results_key=paginated_results_key,
            )
        except BotocoreClientError as exc:
            return await handle_botocore_exceptions_client_errors(
                exc,
                service,
                region,
            )
        except botocore.exceptions.EndpointConnectionError as exc:
            if any(connect_err in str(exc) for connect_err in CONNECTION_ERRORS):
                return {}
            raise BotoClientError from exc

    return cast(Tfun, wrapper)


@handle_run_boto3_exceptions
@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_boto3_fun(  # noqa: PLR0913
    credentials: AwsCredentials,
    service: str,
    function: str,
    region: str | None = None,
    parameters: dict[str, object] | None = None,
    paginated_results_key: str | None = None,
) -> dict[str, dict | list]:
    session = aioboto3.Session(
        aws_access_key_id=credentials.access_key_id,
        aws_secret_access_key=credentials.secret_access_key,
        aws_session_token=credentials.session_token,
    )
    async with session.client(service, region) as client:
        if paginated_results_key and client.can_paginate(function):
            paginator = client.get_paginator(function)
            page_iterator = paginator.paginate(**(parameters or {}))
            merged_pages_list = []
            async for page in page_iterator:
                merged_pages_list.extend(page[paginated_results_key])
            return {paginated_results_key: merged_pages_list}
        return await getattr(client, function)(**(parameters or {}))


async def add_owner_id(arn: str, credentials: AwsCredentials) -> str:
    parts = arn.split(":")
    if len(parts) < 5 or parts[4]:
        return arn

    owner_id: dict[str, Any] = await run_boto3_fun(
        credentials,
        service="sts",
        function="get_caller_identity",
    )
    account_id = owner_id["Account"]
    parts[4] = account_id
    return ":".join(parts)


async def get_owner_id(credentials: AwsCredentials) -> str:
    """Get the account ID based on the given credentials.

    Args:
        credentials: Credentials used to interact with AWS cloud.

    Returns:
        str: Account ID.

    """
    owner_id: dict[str, Any] = await run_boto3_fun(
        credentials,
        service="sts",
        function="get_caller_identity",
    )
    return owner_id["Account"]


def remove_account_id_from_arn(arn: str) -> str:
    """Remove the account ID from a complete ARN (Amazon Resource Name).

    Args:
        arn: The ARN from which to remove the account ID.

    Returns:
        str: The ARN without the account ID.

    """
    arn_parts = arn.split(":")
    arn_parts[4] = ""
    return ":".join(arn_parts)


def arn_no_fluid(tags: list[dict[str, str]]) -> list[str]:
    """Determine which findings are excluded for a specific resource.

    This information comes from a specific tag named NOFLUID, and its value
    has this format: f000.f001.f002_reason

    Args:
        tags: The list of all the tags for a specific resource.

    Returns:
        list[str]: List containing all the findings found within the
        NOFLUID tag.

    """
    for tag in tags:
        if tag["Key"] == "NOFLUID" and tag["Value"] != "" and "_" in tag["Value"]:
            return tag["Value"].split("_", 1)[0].split(".")
    return []


async def get_iam_users_with_tag(
    session: aioboto3.Session,
) -> dict[str, list[str]]:
    """Get iam users with NOFLUID exclusions.

    Returns:
        dict[str, list[str]]: dictionary of keys as ARN of users,
        and the values are the list of excluded findings

    """
    arns: dict[str, list[str]] = {}
    try:
        async with session.client("iam") as iam_client:
            paginator = iam_client.get_paginator("list_users")
            async for response in paginator.paginate():
                for user in response["Users"]:
                    tags_response = await iam_client.list_user_tags(
                        UserName=user["UserName"],
                    )
                    if arn := arn_no_fluid(tags_response["Tags"]):
                        arns[user["Arn"]] = arn
    except BotocoreClientError as error:
        if "AccessDenied" in str(error):
            msg = "Credentials do not have permissions to access IAM users"
            await log("warning", msg)
    return arns


async def get_iam_roles_with_tag(
    session: aioboto3.Session,
) -> dict[str, list[str]]:
    """Get iam roles with NOFLUID exclusions.

    Returns:
        dict[str, list[str]]: dictionary of keys as ARN of roles,
        and the values are the list of excluded findings

    """
    arns: dict[str, list[str]] = {}
    try:
        async with session.client("iam") as iam_client:
            paginator = iam_client.get_paginator("list_roles")
            async for response in paginator.paginate():
                for role in response["Roles"]:
                    tags_response = await iam_client.list_role_tags(
                        RoleName=role["RoleName"],
                    )
                    if arn := arn_no_fluid(tags_response["Tags"]):
                        arns[role["Arn"]] = arn
    except BotocoreClientError as error:
        if "AccessDenied" in str(error):
            msg = "Credentials do not have permissions to access IAM roles"
            await log("warning", msg)
    return arns


async def get_arns_that_are_excluded(
    credentials: AwsCredentials,
    regions: list[str],
) -> dict[str, list[str]]:
    """Determine which roles or users have exclusions.

    defined within their tags using a service named "resourcegroupstaggingapi" that covers most AWS
    resources (with the exception of IAM roles and users).

    Args:
        credentials: Credentials used to interact with AWS cloud.
        regions: Regions in the AWS account

    Returns:
        dict[str, list[str]]: A dictionary that maps a list of excluded
        findings to its corresponding ARN.
        In some cases, it is necessary to add the ARN with and without
        the account ID to cover all scenarios.

    """
    session = aioboto3.Session(
        aws_access_key_id=credentials.access_key_id,
        aws_secret_access_key=credentials.secret_access_key,
        aws_session_token=credentials.session_token,
    )
    iam_users_exclusions = await get_iam_users_with_tag(session)
    iam_roles_exclusions = await get_iam_roles_with_tag(session)

    resources_exclusions = {}
    for region in regions:
        try:
            async with session.client(
                "resourcegroupstaggingapi",
                region_name=region,
            ) as client:
                paginator = client.get_paginator("get_resources")
                async for page in paginator.paginate(
                    TagFilters=[{"Key": "NOFLUID"}],
                ):
                    for resource in page.get("ResourceTagMappingList", []):
                        exclusions = arn_no_fluid(resource["Tags"])
                        if exclusions:
                            resources_exclusions[resource["ResourceARN"]] = exclusions
                            resources_exclusions[
                                remove_account_id_from_arn(
                                    resource["ResourceARN"],
                                )
                            ] = exclusions

        except BotocoreClientError as error:
            if "AccessDenied" in str(error):
                msg = (
                    "Credentials do not have permissions to access "
                    f"resources tags on region {region}"
                )
                await log("warning", msg)
                continue
            raise BotoClientError from error

    return iam_users_exclusions | iam_roles_exclusions | resources_exclusions


def exclude_reports(
    reports: Vulnerabilities,
    exclusions: dict[str, list[str]],
) -> Vulnerabilities:
    if not exclusions:
        return reports

    modified_reports = []
    for vuln in reports:
        code = vuln.finding.name.lower()
        excluded_findings = exclusions.get(vuln.what, [])
        if code in excluded_findings:
            modified_reports.append(
                vuln.set_exclusion_value(is_exclusion=True),
            )
        else:
            modified_reports.append(vuln)

    return tuple(modified_reports)


async def aws_virtual_gateways_for_app_meshes(
    credentials: AwsCredentials,
    region: str,
) -> list[dict[str, Any]]:
    virtual_gateways = []
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="appmesh",
        function="list_meshes",
    )
    meshes = response.get("meshes", [])
    for mesh in meshes:
        mesh_name = mesh.get("meshName", "")
        list_virtual_gateways = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="appmesh",
            function="list_virtual_gateways",
            parameters={"meshName": mesh_name},
        )
        for virtual_gateway in list_virtual_gateways["virtualGateways"]:
            virtual_gateway_name = virtual_gateway.get("virtualGatewayName")
            describe_vgw = await run_boto3_fun(
                credentials=credentials,
                region=region,
                service="appmesh",
                function="describe_virtual_gateway",
                parameters={
                    "meshName": mesh_name,
                    "virtualGatewayName": virtual_gateway_name,
                },
            )
            virtual_gateways.append(describe_vgw)
    return virtual_gateways


def build_mesh_virtual_gateway_arn(
    region: str,
    virtual_gateway_data: dict[str, Any],
) -> str:
    parameters = {
        "Region": region,
        "Account": virtual_gateway_data["metadata"]["meshOwner"],
        "MeshName": virtual_gateway_data["meshName"],
        "VirtualGatewayName": virtual_gateway_data["virtualGatewayName"],
    }
    return build_arn(
        service="appmesh",
        resource="virtualGateway",
        parameters=parameters,
    )


def is_permissive_policy_statement(
    resource_arn: str,
    statement: dict[str, Any],
) -> bool:
    return (
        statement.get("Principal", "") in ["*", {"AWS": "*"}, ["*"]]
        and statement.get("Effect") == "Allow"
        and (
            any(
                fnmatch.fnmatch(resource_arn, resource)
                for resource in statement.get("Resource", [])
            )
            if isinstance(statement.get("Resource", []), list)
            else fnmatch.fnmatch(resource_arn, statement.get("Resource", ""))
        )
        and not statement.get("Condition")
    )
