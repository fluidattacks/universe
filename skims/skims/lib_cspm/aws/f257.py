from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def ec2_has_not_termination_protection(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        parameters={
            "Filters": [
                {
                    "Name": "instance-lifecycle",
                    "Values": ["scheduled", "capacity-block"],
                },
            ],
        },
        paginated_results_key=paginated_results_key,
    )

    reservations = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.EC2_HAS_NOT_TERMINATION_PROTECTION
    vulns: Vulnerabilities = ()
    for reservation in reservations:
        locations: list[Location] = []
        for instance in reservation.get("Instances", []):
            autoscaling: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="autoscaling",
                function="describe_auto_scaling_instances",
                parameters={
                    "InstanceIds": [instance["InstanceId"]],
                },
            )

            if autoscaling.get("AutoScalingInstances"):
                continue

            disable_api_termination: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="ec2",
                function="describe_instance_attribute",
                parameters={
                    "Attribute": "disableApiTermination",
                    "InstanceId": str(instance["InstanceId"]),
                },
            )

            is_disabled = disable_api_termination.get("DisableApiTermination", {}).get(
                "Value",
                True,
            )
            if is_disabled is False:
                parameters = {
                    "Region": region,
                    "Account": reservation["OwnerId"],
                    "InstanceId": instance["InstanceId"],
                }
                locations = [
                    Location(
                        access_patterns=("/DisableApiTermination/Value",),
                        arn=build_arn(
                            service="ec2",
                            resource="instance",
                            parameters=parameters,
                        ),
                        values=(is_disabled,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=disable_api_termination,
                )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (ec2_has_not_termination_protection,)
