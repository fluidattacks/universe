from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    get_owner_id,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def ebs_has_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Volumes"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="ec2",
        function="describe_volumes",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    volumes = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.EBS_HAS_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()

    for volume in volumes:
        if not volume.get("Encrypted", False):
            owner_id = await get_owner_id(credentials)
            parameters = {
                "Region": region,
                "Account": owner_id,
                "VolumeId": volume["VolumeId"],
            }
            arn = build_arn(service="ec2", resource="volume", parameters=parameters)
            locations = [
                Location(
                    arn=arn,
                    method=method,
                    values=(),
                    access_patterns=(),
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=volume,
            )

    return (vulns, True)


@SHIELD
async def aws_workspaces_has_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Workspaces"
    method = MethodsEnum.AWS_WORKSPACES_HAS_VOLUME_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()
    workspaces: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="workspaces",
        function="describe_workspaces",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    for workspace in workspaces.get(paginated_results_key, []):
        root_volume_encryption = workspace.get("RootVolumeEncryptionEnabled")
        user_volume_encryption = workspace.get("UserVolumeEncryptionEnabled")
        if not root_volume_encryption or not user_volume_encryption:
            owner_id = await get_owner_id(credentials)
            parameters = {
                "Region": region,
                "Account": owner_id,
                "WorkspaceId": workspace["WorkspaceId"],
            }
            arn = build_arn(
                service="workspaces",
                resource="workspaceid",
                parameters=parameters,
            )
            values: tuple[bool, ...] = ()
            access_patterns: tuple[str, ...] = ()
            if not root_volume_encryption:
                workspace["RootVolumeEncryptionEnabled"] = False
                access_patterns += ("/RootVolumeEncryptionEnabled",)
                values += (False,)
            if not user_volume_encryption:
                workspace["UserVolumeEncryptionEnabled"] = False
                access_patterns += ("/UserVolumeEncryptionEnabled",)
                values += (False,)
            locations = [
                Location(
                    arn=(arn),
                    method=method,
                    values=values,
                    access_patterns=access_patterns,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=workspace,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    ebs_has_encryption_disabled,
    aws_workspaces_has_encryption_disabled,
)
