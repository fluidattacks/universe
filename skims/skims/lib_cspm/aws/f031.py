import ast
import datetime
import json
import re
import traceback
from contextlib import (
    suppress,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    BotoClientError,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    _get_action,
    _is_action_permissive,
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


async def get_paginated_items(
    credentials: AwsCredentials,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "service": "iam",
        "function": "list_policies",
        "parameters": {"MaxItems": 50, "Scope": "Local", "OnlyAttached": True},
    }
    data = await run_boto3_fun(**args)
    object_name = "Policies"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


async def get_paginated_policies(
    credentials: AwsCredentials,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "service": "iam",
        "function": "list_policies",
        "parameters": {"MaxItems": 50},
    }
    data = await run_boto3_fun(**args)
    object_name = "Policies"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


async def _check_if_policy_in_use(credentials: AwsCredentials, policy: dict[str, Any]) -> bool:
    list_entities_for_policy = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_entities_for_policy",
        parameters={
            "PolicyArn": str(policy["Arn"]),
        },
    )
    roles = list_entities_for_policy["PolicyRoles"]
    users = list_entities_for_policy["PolicyUsers"]
    groups = list_entities_for_policy["PolicyGroups"]

    if users or groups:
        return True

    for role in roles:
        is_being_used = await run_boto3_fun(
            credentials,
            service="iam",
            function="get_role",
            parameters={
                "RoleName": str(role["RoleName"]),
            },
        )
        return is_being_used["Role"]["RoleLastUsed"] != {}
    return False


@SHIELD
async def admin_policy_attached(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ADMIN_POLICY_ATTACHED
    policies = await get_paginated_policies(credentials)
    elevated_policies = {
        "arn:aws:iam::aws:policy/PowerUserAccess",
        "arn:aws:iam::aws:policy/IAMFullAccess",
        "arn:aws:iam::aws:policy/AdministratorAccess",
    }
    vulns: Vulnerabilities = ()
    for policy in policies:
        locations: list[Location] = []
        if policy["Arn"] in elevated_policies and policy["AttachmentCount"] != 0:
            is_policy_in_use = await _check_if_policy_in_use(credentials, policy)
            arn = policy["Arn"]
            if is_policy_in_use:
                locations = [
                    Location(
                        access_patterns=("/Arn", "/AttachmentCount"),
                        arn=(arn),
                        values=(
                            policy["Arn"],
                            policy["AttachmentCount"],
                        ),
                        method=method,
                    ),
                ]

                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=policy,
                )

    return (vulns, True)


def _match_pattern(pattern: str, target: str, flags: int = 0) -> bool:
    # Escape everything that is not `*` and replace `*` with regex `.*`
    pattern = r".*".join(map(re.escape, pattern.split("*")))
    return bool(re.match(f"^{pattern}$", target, flags=flags))


def _match_iam_passrole(action: str) -> bool:
    return _match_pattern(str(action), "iam:PassRole")


@SHIELD
async def open_passrole(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.OPEN_PASSROLE
    policies = await get_paginated_items(credentials)

    vulns: Vulnerabilities = ()
    if policies:
        for policy in policies:
            locations: list[Location] = []
            pol_ver: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="get_policy_version",
                parameters={
                    "PolicyArn": str(policy["Arn"]),
                    "VersionId": str(policy["DefaultVersionId"]),
                },
            )
            policy_names = pol_ver.get("PolicyVersion", {})
            pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
            policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))

            if not isinstance(policy_statements, list):
                policy_statements = [policy_statements]

            for index, raw_item in enumerate(policy_statements):
                item = ast.literal_eval(str(raw_item))
                with suppress(KeyError):
                    action = _get_action(item, "Action")

                    if (
                        item["Effect"] == "Allow"
                        and any(map(_match_iam_passrole, action))
                        and item["Resource"] == "*"
                    ):
                        locations.append(
                            Location(
                                access_patterns=(
                                    f"/Statement/{index}/Effect",
                                    f"/Statement/{index}/Action",
                                    f"/Statement/{index}/Resource",
                                ),
                                arn=(f"{policy['Arn']}"),
                                values=(
                                    raw_item["Effect"],
                                    raw_item["Action"],
                                    raw_item["Resource"],
                                ),
                                method=method,
                            ),
                        )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=pol_access,
            )

    return (vulns, True)


async def _role_with_permissive_policies(
    method: MethodsEnum,
    credentials: AwsCredentials,
    policy_name: str,
    role_name: str,
    role_arn: str,
) -> Vulnerabilities:
    policy_role_response = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_role_policy",
        parameters={
            "PolicyName": policy_name,
            "RoleName": role_name,
        },
    )
    policy_role = dict(policy_role_response["PolicyDocument"]) if policy_role_response else {}
    policy_statements = ast.literal_eval(str(policy_role.get("Statement", [])))
    locations: list[Location] = []
    for index, statement in enumerate(policy_statements):
        if isinstance(statement, str):
            continue

        with suppress(KeyError):
            if statement["Action"] in ("*", ["*"]) and statement["Effect"] == "Allow":
                locations.append(
                    Location(
                        access_patterns=(
                            f"/{index}/Effect",
                            f"/{index}/Action",
                        ),
                        arn=role_arn,
                        values=(statement["Effect"], statement["Action"]),
                        method=method,
                    ),
                )

    return build_vulnerabilities(
        locations=locations,
        method=method,
        aws_response=policy_statements,
    )


@SHIELD
async def has_permissive_role_policies(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAS_PERMISSIVE_ROLE_POLICIES
    response = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_roles",
    )
    roles = response.get("Roles", []) if response else []
    vulns: Vulnerabilities = ()
    paginated_results_key = "PolicyNames"
    for role in roles:
        role_policies_response = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="list_role_policies",
            parameters={"RoleName": role["RoleName"]},
            paginated_results_key=paginated_results_key,
        )
        role_policies = (
            list(role_policies_response[paginated_results_key]) if role_policies_response else []
        )
        for policy_name in role_policies:
            policy_vulns = await _role_with_permissive_policies(
                method,
                credentials,
                policy_name,
                role["RoleName"],
                role["Arn"],
            )
            vulns += policy_vulns

    return (vulns, True)


@SHIELD
async def full_access_to_ssm(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.FULL_ACCESS_TO_SSM
    policies = await get_paginated_items(credentials)
    vulns: Vulnerabilities = ()
    if policies:
        for policy in policies:
            locations: list[Location] = []
            pol_ver: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="get_policy_version",
                parameters={
                    "PolicyArn": str(policy["Arn"]),
                    "VersionId": str(policy["DefaultVersionId"]),
                },
            )
            policy_names = pol_ver.get("PolicyVersion", {})
            pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
            policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))

            if not isinstance(policy_statements, list):
                policy_statements = [policy_statements]

            for index, raw_item in enumerate(policy_statements):
                item = ast.literal_eval(str(raw_item))
                with suppress(KeyError):
                    action = _get_action(item, "Action")

                    if item["Effect"] == "Allow" and any(act == "ssm:*" for act in action):
                        locations = [
                            *[
                                Location(
                                    access_patterns=(f"/Statement/{index}/Action",),
                                    arn=(f"{policy['Arn']}"),
                                    values=(raw_item["Action"],),
                                    method=method,
                                ),
                            ],
                        ]

            vulns = (
                *vulns,
                *build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=pol_access,
                ),
            )

    return (vulns, True)


async def _get_locations(
    index: int,
    item: dict,
    policy: dict[str, Any],
    policy_statements: dict[int, Any],
    method: MethodsEnum,
) -> list:
    locations: list[Location] = []
    action = _get_action(item, "NotAction")
    if item["Effect"] == "Allow" and not _is_action_permissive(action):
        locations = [
            Location(
                access_patterns=(f"/Statement/{index}/NotAction",),
                arn=(f"{policy['Arn']}"),
                values=policy_statements[index]["NotAction"],
                method=method,
            ),
        ]

    if item["Effect"] == "Allow" and item["NotResource"] != "*":
        locations = [
            Location(
                access_patterns=(f"/Statement/{index}/NotResource",),
                arn=(f"{policy['Arn']}"),
                values=policy_statements[index]["NotResource"],
                method=method,
            ),
        ]
    return locations


@SHIELD
async def negative_statement(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NEGATIVE_STATEMENT
    policies = await get_paginated_items(credentials)
    vulns: Vulnerabilities = ()
    if policies:
        for policy in policies:
            locations: list[Location] = []
            pol_ver: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="get_policy_version",
                parameters={
                    "PolicyArn": str(policy["Arn"]),
                    "VersionId": str(policy["DefaultVersionId"]),
                },
            )
            policy_names = pol_ver.get("PolicyVersion", {})
            pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
            policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))

            if not isinstance(policy_statements, list):
                policy_statements = [policy_statements]

            for index, raw_item in enumerate(policy_statements):
                item = ast.literal_eval(str(raw_item))
                with suppress(KeyError):
                    locations = await _get_locations(
                        index=index,
                        item=item,
                        policy=policy,
                        policy_statements=policy_statements,
                        method=method,
                    )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=pol_access,
            )

    return (vulns, True)


@SHIELD
async def users_with_password_and_access_keys(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    paginated_results_key = "Users"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_users",
        paginated_results_key=paginated_results_key,
    )
    users = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.USERS_WITH_PASSWORD_AND_ACCESS_KEYS
    locations: list[Location] = []
    for user in users:
        access_keys: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="list_access_keys",
            parameters={
                "UserName": user["UserName"],
            },
        )
        access_key_metadata = access_keys["AccessKeyMetadata"] if access_keys else {}
        if not any(key.get("Status") for key in access_key_metadata):
            continue

        login_profile = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="get_login_profile",
            parameters={"UserName": str(user["UserName"])},
        )

        if login_profile:
            access_keys_str = {
                key: (
                    value.strftime("%Y-%m-%d %H:%M:%S")
                    if isinstance(value, datetime.datetime)
                    else str(value)
                )
                for keys in access_keys["AccessKeyMetadata"]
                for key, value in keys.items()
            }
            locations.append(
                Location(
                    access_patterns=("/AccessKeyMetadata",),
                    arn=(f"{user['Arn']}"),
                    values=(access_keys_str,),
                    method=method,
                ),
            )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=access_keys,
            )

    return (vulns, True)


@SHIELD
async def vpc_endpoints_exposed(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_keys = "VpcEndpoints"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_vpc_endpoints",
        paginated_results_key=paginated_results_keys,
    )
    vpc_endpoints = response.get(paginated_results_keys, []) if response else []
    vulns: Vulnerabilities = ()
    method = MethodsEnum.VPC_ENDPOINTS_EXPOSED
    for endpoint in vpc_endpoints:
        owner_id = endpoint["OwnerId"]
        parameters = {
            "Region": region,
            "Account": owner_id,
            "VpcEndpointId": endpoint["VpcEndpointId"],
        }
        if endpoint["VpcEndpointType"] != "Interface" and (
            raw_policy_doc := endpoint.get("PolicyDocument", None)
        ):
            policy_document = json.loads(raw_policy_doc)
            for index, sts in enumerate(policy_document["Statement"]):
                if sts["Principal"] in ["*", {"AWS": "*"}] and "Condition" not in sts:
                    locations = [
                        Location(
                            access_patterns=(f"/Statement/{index}/Principal",),
                            arn=build_arn(
                                service="ec2",
                                resource="vpc-endpoint",
                                parameters=parameters,
                            ),
                            values=(sts["Principal"],),
                            method=method,
                        ),
                    ]

                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=policy_document,
                    )

    return (vulns, True)


@SHIELD
async def aws_s3_log_delivery_write_access(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_S3_LOG_DELIVERY_WRITE_ACCESS
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    for bucket in response.get("Buckets", []):
        bucket_name = bucket.get("Name", "")
        bucket_logging: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="s3",
            function="get_bucket_logging",
            parameters={"Bucket": bucket_name},
        )
        logging_enabled = bucket_logging.get("LoggingEnabled")
        if logging_enabled and logging_enabled["TargetBucket"] != bucket_name:
            bucket_acl: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="s3",
                function="get_bucket_acl",
                parameters={"Bucket": bucket_name},
            )
            for index, grant in enumerate(bucket_acl.get("Grants", [])):
                grantee = grant.get("Grantee", {})
                uri = grantee.get("URI", "")
                arn = build_arn(
                    service="s3",
                    resource="bucket",
                    parameters={"BucketName": bucket_name},
                )
                if uri == "http://acs.amazonaws.com/groups/s3/LogDelivery" and grant.get(
                    "Permission",
                    "",
                ) in {"WRITE", "WRITE_ACP", "FULL_CONTROL"}:
                    locations = [
                        Location(
                            access_patterns=(f"/Grants/{index}",),
                            arn=arn,
                            values=(grant,),
                            method=method,
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=bucket_acl,
                    )

    return (vulns, True)


@SHIELD
async def aws_ecr_repository_exposed(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_ECR_REPOSITORY_EXPOSED
    paginated_results_key = "repositories"
    ecr_repositories: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="ecr",
        function="describe_repositories",
        paginated_results_key=paginated_results_key,
    )
    for repository in ecr_repositories.get(paginated_results_key, []):
        repository_name = repository["repositoryName"]
        try:
            repository_policy = await run_boto3_fun(
                credentials=credentials,
                region=region,
                service="ecr",
                function="get_repository_policy",
                parameters={"repositoryName": repository_name},
            )
            arn = repository["repositoryArn"]
            policy = json.loads(repository_policy.get("policyText", ""))
            for index, statement in enumerate(policy.get("Statement", [])):
                principal = statement.get("Principal")
                if (
                    principal in ["*", {"AWS": "*"}, ["*"]]
                    and statement.get("Effect") == "Allow"
                    and not statement.get("Condition")
                ):
                    repository["Policy"] = policy
                    locations = [
                        Location(
                            arn=arn,
                            access_patterns=(f"/Policy/Statement/{index}/Principal",),
                            values=(principal,),
                            method=method,
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=repository,
                    )
        except BotoClientError:
            if "RepositoryPolicyNotFoundException" in str(traceback.format_exc()):
                continue
            raise

    return (vulns, True)


@SHIELD
async def aws_opensearch_domain_exposed(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_OPENSEARCH_DOMAIN_EXPOSED
    environments: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="opensearch",
        function="list_domain_names",
    )
    for domain in environments.get("DomainNames", []):
        domain_name = domain["DomainName"]
        domain_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="opensearch",
            function="describe_domain",
            parameters={
                "DomainName": domain_name,
            },
        )
        domain_status = domain_config.get("DomainStatus", {})
        access_policies = json.loads(domain_status.get("AccessPolicies", {}))
        domain_config["DomainStatus"]["AccessPolicies"] = access_policies
        for index, statement in enumerate(access_policies.get("Statement", [])):
            principal = statement.get("Principal")
            if (
                principal in ["*", {"AWS": "*"}, ["*"]]
                and statement.get("Effect") == "Allow"
                and not statement.get("Condition")
            ):
                locations = [
                    Location(
                        arn=domain_status["ARN"],
                        access_patterns=(
                            f"/DomainStatus/AccessPolicies/Statement/{index}/Principal",
                        ),
                        values=(principal,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=domain_config,
                )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    aws_s3_log_delivery_write_access,
    admin_policy_attached,
    open_passrole,
    negative_statement,
    full_access_to_ssm,
    has_permissive_role_policies,
    users_with_password_and_access_keys,
    vpc_endpoints_exposed,
    aws_ecr_repository_exposed,
    aws_opensearch_domain_exposed,
)
