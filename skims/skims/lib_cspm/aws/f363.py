from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    add_owner_id,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def not_requires_uppercase(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NOT_REQUIRES_UPPERCASE
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_password_policy",
    )

    vulns: Vulnerabilities = ()
    password_policy = response.get("PasswordPolicy", {})
    if password_policy and not password_policy.get("RequireUppercaseCharacters", False):
        arn = await add_owner_id("arn:aws:iam:::accountSettings/passwordPolicy", credentials)
        locations = [
            Location(
                access_patterns=("/RequireUppercaseCharacters",)
                if password_policy.get("RequireUppercaseCharacters", "") is False
                else (),
                arn=arn,
                values=(password_policy["RequireUppercaseCharacters"],)
                if password_policy.get("RequireUppercaseCharacters", "") is False
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=password_policy,
        )

    return (vulns, True)


@SHIELD
async def not_requires_lowercase(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NOT_REQUIRES_LOWERCASE
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_password_policy",
    )

    vulns: Vulnerabilities = ()
    password_policy = response.get("PasswordPolicy", {})
    if password_policy and not password_policy.get("RequireLowercaseCharacters", False):
        arn = await add_owner_id("arn:aws:iam:::accountSettings/passwordPolicy", credentials)
        locations = [
            Location(
                access_patterns=("/RequireLowercaseCharacters",)
                if password_policy.get("RequireLowercaseCharacters", "") is False
                else (),
                arn=arn,
                values=(password_policy["RequireLowercaseCharacters"],)
                if password_policy.get("RequireLowercaseCharacters", "") is False
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=password_policy,
        )

    return (vulns, True)


@SHIELD
async def not_requires_symbols(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NOT_REQUIRES_SYMBOLS
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_password_policy",
    )

    vulns: Vulnerabilities = ()
    password_policy = response.get("PasswordPolicy", {})
    if password_policy and not password_policy.get("RequireSymbols", False):
        arn = await add_owner_id("arn:aws:iam:::accountSettings/passwordPolicy", credentials)
        locations = [
            Location(
                access_patterns=("/RequireSymbols",)
                if password_policy.get("RequireSymbols", "") is False
                else (),
                arn=arn,
                values=(password_policy["RequireSymbols"],)
                if password_policy.get("RequireSymbols", "") is False
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=password_policy,
        )

    return (vulns, True)


@SHIELD
async def not_requires_numbers(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NOT_REQUIRES_NUMBERS
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_password_policy",
    )

    vulns: Vulnerabilities = ()
    password_policy = response.get("PasswordPolicy", {})
    if password_policy and not password_policy.get("RequireNumbers", False):
        arn = await add_owner_id("arn:aws:iam:::accountSettings/passwordPolicy", credentials)
        locations = [
            Location(
                access_patterns=("/RequireNumbers",)
                if password_policy.get("RequireNumbers", "") is False
                else (),
                arn=arn,
                values=(password_policy["RequireNumbers"],)
                if password_policy.get("RequireNumbers", "") is False
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=password_policy,
        )

    return (vulns, True)


@SHIELD
async def min_password_len_unsafe(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.MIN_PASSWORD_LEN_UNSAFE
    min_length: int = 20
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_password_policy",
    )

    vulns: Vulnerabilities = ()
    password_policy = response.get("PasswordPolicy", {})
    if password_policy and password_policy.get("MinimumPasswordLength", 0) < min_length:
        arn = await add_owner_id("arn:aws:iam:::accountSettings/passwordPolicy", credentials)
        locations = [
            Location(
                access_patterns=("/MinimumPasswordLength",)
                if password_policy.get("MinimumPasswordLength", "")
                else (),
                arn=arn,
                values=(password_policy["MinimumPasswordLength"],)
                if password_policy.get("MinimumPasswordLength", "")
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=password_policy,
        )

    return (vulns, True)


@SHIELD
async def password_reuse_unsafe(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.PASSWORD_REUSE_UNSAFE
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_password_policy",
    )

    vulns: Vulnerabilities = ()
    if (password_policy := response.get("PasswordPolicy", {})) and password_policy.get(
        "PasswordReusePrevention",
        0,
    ) < 24:
        arn = await add_owner_id("arn:aws:iam:::accountSettings/passwordPolicy", credentials)
        locations = [
            Location(
                access_patterns=("/PasswordReusePrevention",)
                if password_policy.get("PasswordReusePrevention", "")
                else (),
                arn=arn,
                values=(password_policy["PasswordReusePrevention"],)
                if password_policy.get("PasswordReusePrevention", "")
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=password_policy,
        )

    return (vulns, True)


@SHIELD
async def password_expiration_unsafe(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_password_policy",
    )

    vulns: Vulnerabilities = ()
    method = MethodsEnum.PASSWORD_EXPIRATION_UNSAFE
    password_policy = response.get("PasswordPolicy", {})
    max_days = 90
    password_max_age: int = password_policy.get("MaxPasswordAge", max_days + 1)
    if password_policy and password_max_age > max_days:
        arn = await add_owner_id("arn:aws:iam:::accountSettings/passwordPolicy", credentials)
        locations = [
            Location(
                access_patterns=("/MaxPasswordAge",)
                if password_policy.get("MaxPasswordAge", "")
                else (),
                arn=arn,
                values=(password_policy["MaxPasswordAge"],)
                if password_policy.get("MaxPasswordAge", "")
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=password_policy,
        )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    not_requires_uppercase,
    not_requires_lowercase,
    not_requires_symbols,
    not_requires_numbers,
    min_password_len_unsafe,
    password_reuse_unsafe,
    password_expiration_unsafe,
)
