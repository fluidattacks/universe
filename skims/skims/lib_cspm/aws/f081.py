from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    BotoClientError,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    get_owner_id,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def root_without_mfa(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ROOT_WITHOUT_MFA
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_account_summary",
    )
    vulns: Vulnerabilities = ()
    summary = response.get("SummaryMap", {})
    if summary and summary.get("AccountMFAEnabled", 0) != 1:
        arn = f"arn:aws:iam::{await get_owner_id(credentials)}:root_account"
        locations = [
            Location(
                access_patterns=("/AccountMFAEnabled",)
                if summary.get("AccountMFAEnabled", "") == 0
                else (),
                arn=(arn),
                values=(summary["AccountMFAEnabled"],)
                if summary.get("AccountMFAEnabled", "") == 0
                else (),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=summary,
        )

    return (vulns, True)


@SHIELD
async def mfa_disabled_for_users_with_console_password(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Users"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_users",
        paginated_results_key=paginated_results_key,
    )
    method = MethodsEnum.MFA_DISABLED_FOR_USERS_WITH_CONSOLE_PASSWORD
    vulns: Vulnerabilities = ()
    users = response.get(paginated_results_key, []) if response else []
    if users:
        for user in users:
            try:
                login_profile: dict[str, Any] = await run_boto3_fun(
                    credentials=credentials,
                    service="iam",
                    function="get_login_profile",
                    parameters={"UserName": str(user["UserName"])},
                )
            except BotoClientError:
                login_profile = {}
            user_policies: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="list_mfa_devices",
                parameters={"UserName": str(user["UserName"])},
            )
            mfa_devices = user_policies.get("MFADevices", [])
            if len(mfa_devices) < 1 and login_profile:
                locations = [
                    Location(
                        access_patterns=(),
                        arn=(f"{user['Arn']}"),
                        values=(),
                        method=method,
                    ),
                ]

                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=user_policies,
                )

    return (vulns, True)


@SHIELD
async def cognito_has_mfa_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.COGNITO_HAS_MFA_DISABLED
    vulns: Vulnerabilities = ()
    paginated_results_key = "UserPools"
    pools: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="cognito-idp",
        function="list_user_pools",
        paginated_results_key=paginated_results_key,
        parameters={"MaxResults": 50},
    )
    for pool in pools.get(paginated_results_key, []):
        mfa: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="cognito-idp",
            function="get_user_pool_mfa_config",
            parameters={"UserPoolId": str(pool["Id"])},
        )
        mfa_config = mfa.get("MfaConfiguration", "")
        if mfa_config in {"OFF", "OPTIONAL"}:
            id_providers: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="cognito-idp",
                function="list_identity_providers",
                parameters={"UserPoolId": str(pool["Id"])},
            )
            if id_providers and len(id_providers.get("Providers", 0)) != 0:
                continue
            owner_id = await get_owner_id(credentials)
            parameters = {
                "Region": region,
                "Account": owner_id,
                "UserPoolId": pool["Id"],
            }
            arn = build_arn(
                service="cognito-idp",
                resource="userpool",
                parameters=parameters,
            )
            locations = [
                Location(
                    access_patterns=("/MfaConfiguration",),
                    arn=arn,
                    values=(mfa_config,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=mfa,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    cognito_has_mfa_disabled,
    root_without_mfa,
    mfa_disabled_for_users_with_console_password,
)
