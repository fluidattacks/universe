from typing import (
    Any,
)

from lib_cspm.aws.model import (
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def aws_cloudfront_is_not_protected_with_waf(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_CLOUDFRONT_IS_NOT_PROTECTED_WITH_WAF
    distributions: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="cloudfront",
        function="list_distributions",
    )
    for distribution in distributions.get("DistributionList", {}).get("Items", []):
        waf = distribution.get("WebACLId")
        if not waf:
            arn = distribution["ARN"]
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/WebACLId",),
                    values=(waf,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=distribution,
            )
    return (vulns, True)


CHECKS = (aws_cloudfront_is_not_protected_with_waf,)
