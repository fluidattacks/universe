import csv
import fnmatch
import json
import re
import traceback
from contextlib import (
    suppress,
)
from io import (
    StringIO,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    BotoClientError,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    get_owner_id,
    is_permissive_policy_statement,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def users_with_multiple_access_keys(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.USERS_WITH_MULTIPLE_ACCESS_KEYS
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_users",
        paginated_results_key="Users",
    )
    vulns: Vulnerabilities = ()

    users = response.get("Users", [])

    if users:
        for user in users:
            access_keys: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="list_access_keys",
                parameters={
                    "UserName": user["UserName"],
                },
                paginated_results_key="AccessKeyMetadata",
            )
            access_key_metadata = access_keys["AccessKeyMetadata"] if access_keys else {}
            locations: list[Location] = []
            access_keys_activated = list(
                filter(
                    lambda y: y == "Active",
                    [attr["Status"] for attr in access_key_metadata],
                ),
            )
            if len(access_keys_activated) > 1:
                arn = user["Arn"]
                locations = [
                    Location(
                        access_patterns=(),
                        arn=(arn),
                        values=(),
                        method=method,
                    ),
                ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=access_key_metadata,
            )

    return (vulns, True)


@SHIELD
async def root_has_access_keys(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ROOT_HAS_ACCESS_KEYS
    await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="generate_credential_report",
    )
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_credential_report",
    )

    vulns: Vulnerabilities = ()
    users_csv = StringIO(response.get("Content", b"").decode())
    credentials_report = tuple(csv.DictReader(users_csv, delimiter=","))

    key_names = ("access_key_1_active", "access_key_2_active")
    with suppress(KeyError):
        if credentials_report:
            locations: list[Location] = []
            root_user = credentials_report[0]
            locations = [
                Location(
                    access_patterns=(f"/{name}",),
                    arn=root_user["arn"],
                    values=(root_user[name],),
                    method=method,
                )
                for name in key_names
                if root_user.get(name) == "true"
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=root_user,  # type: ignore[arg-type]
            )

    return (vulns, True)


def _get_insecure_certificates(root_user: dict[str, Any], method: MethodsEnum) -> list[Location]:
    return [
        Location(
            access_patterns=(f"/{name}",),
            arn=root_user["arn"],
            values=(root_user[name],),
            method=method,
        )
        for name in ["cert_1_active", "cert_2_active"]
        if root_user.get(name) == "true"
    ]


@SHIELD
async def has_root_active_signing_certificates(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAS_ROOT_ACTIVE_SIGNING_CERTIFICATES
    await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="generate_credential_report",
    )
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_credential_report",
    )
    vulns: Vulnerabilities = ()
    users_csv = StringIO(response.get("Content", b"").decode())
    credentials_report = tuple(csv.DictReader(users_csv, delimiter=","))
    if credentials_report:
        root_user = credentials_report[0]
        root_has_active_signing_certs = (
            root_user.get("cert_1_active") == "true" or root_user.get("cert_2_active") == "true"
        )
        if root_has_active_signing_certs:
            locations = _get_insecure_certificates(root_user, method)
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=root_user,  # type: ignore[arg-type]
            )

    return (vulns, True)


@SHIELD
async def eks_has_endpoints_publicly_accessible(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="eks",
        function="list_clusters",
        region=region,
        paginated_results_key="clusters",
    )
    method = MethodsEnum.EKS_HAS_ENDPOINTS_PUBLICLY_ACCESSIBLE
    cluster_names = response.get("clusters", []) if response else []
    vulns: Vulnerabilities = ()
    for cluster in cluster_names:
        cluster_desc = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="eks",
            function="describe_cluster",
            parameters={"name": str(cluster)},
        )
        cluster_description = dict(cluster_desc["cluster"])
        vulnerable = (
            cluster_description["resourcesVpcConfig"]["endpointPublicAccess"]
            and not cluster_description["resourcesVpcConfig"]["endpointPrivateAccess"]
        )
        if vulnerable:
            locations = [
                Location(
                    access_patterns=(
                        "/resourcesVpcConfig/endpointPrivateAccess",
                        "/resourcesVpcConfig/endpointPublicAccess",
                    ),
                    arn=(cluster_description["arn"]),
                    values=(
                        cluster_description["resourcesVpcConfig"]["endpointPrivateAccess"],
                        cluster_description["resourcesVpcConfig"]["endpointPublicAccess"],
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster_description,
            )

    return (vulns, True)


@SHIELD
async def rds_has_public_snapshots(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_snapshots",
        paginated_results_key="DBSnapshots",
    )
    snapshots = response.get("DBSnapshots", []) if response else []
    method = MethodsEnum.RDS_HAS_PUBLIC_SNAPSHOTS
    vulns: Vulnerabilities = ()
    for snapshot in snapshots:
        locations: list[Location] = []
        snapshot_attributes: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="rds",
            function="describe_db_snapshot_attributes",
            parameters={
                "DBSnapshotIdentifier": snapshot["DBSnapshotIdentifier"],
            },
        )
        attr_results = snapshot_attributes.get("DBSnapshotAttributesResult", {})
        for index, attr in enumerate(attr_results["DBSnapshotAttributes"]):
            if "all" in attr["AttributeValues"]:
                arn = snapshot["DBSnapshotArn"]
                locations.append(
                    Location(
                        access_patterns=(f"/{index}/AttributeValues",),
                        arn=(arn),
                        values=(attr["AttributeValues"],),
                        method=method,
                    ),
                )
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=attr_results["DBSnapshotAttributes"],
        )

    return (vulns, True)


@SHIELD
async def rds_not_uses_iam_authentication(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_instances",
        paginated_results_key="DBInstances",
    )
    instances = response.get("DBInstances", []) if response else []
    method = MethodsEnum.RDS_NOT_USES_IAM_AUTHENTICATION
    vulns: Vulnerabilities = ()
    for instance in instances:
        if not instance.get("IAMDatabaseAuthenticationEnabled"):
            locations = [
                Location(
                    access_patterns=("/IAMDatabaseAuthenticationEnabled",),
                    arn=(instance["DBInstanceArn"]),
                    values=(instance["IAMDatabaseAuthenticationEnabled"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=instance,
            )

    return (vulns, True)


async def redshift_get_paginated_items(
    credentials: AwsCredentials,
    region: str,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "region": region,
        "service": "redshift",
        "function": "describe_clusters",
        "parameters": {"MaxRecords": 25},
    }
    data = await run_boto3_fun(**args)
    object_name = "Clusters"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


@SHIELD
async def redshift_has_public_clusters(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    clusters = await redshift_get_paginated_items(credentials, region)
    method = MethodsEnum.REDSHIFT_HAS_PUBLIC_CLUSTERS
    vulns: Vulnerabilities = ()
    if clusters:
        for cluster in clusters:
            if cluster["PubliclyAccessible"]:
                locations = [
                    Location(
                        access_patterns=("/PubliclyAccessible",),
                        arn=(cluster["ClusterNamespaceArn"]),
                        values=(cluster["PubliclyAccessible"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=cluster,
                )

    return (vulns, True)


@SHIELD
async def redshift_not_requires_ssl(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDSHIFT_NOT_REQUIRES_SSL
    vulns: Vulnerabilities = ()
    locations: list[Location] = []
    clusters = await redshift_get_paginated_items(credentials, region)
    for cluster in clusters:
        param_groups = cluster.get("ClusterParameterGroups", [])

        for group in param_groups:
            describe_cluster_parameters: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="redshift",
                function="describe_cluster_parameters",
                parameters={
                    "ParameterGroupName": group["ParameterGroupName"],
                },
                paginated_results_key="Parameters",
            )
            params = describe_cluster_parameters.get("Parameters", [])

            for param in params:
                if param["ParameterName"] == "require_ssl" and param["ParameterValue"] == "false":
                    parameters = {
                        "Region": region,
                        "Account": await get_owner_id(credentials),
                        "ClusterName": cluster["ClusterIdentifier"],
                    }
                    arn = build_arn(
                        service="redshift",
                        resource="cluster",
                        parameters=parameters,
                    )
                    locations = [
                        Location(
                            arn=(arn),
                            method=method,
                            values=(
                                param["ParameterName"],
                                param["ParameterValue"],
                            ),
                            access_patterns=(
                                "/ParameterName",
                                "/ParameterValue",
                            ),
                        ),
                    ]

                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=param,
                    )

    return (vulns, True)


@SHIELD
async def elasticache_uses_default_port(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elasticache",
        function="describe_cache_clusters",
        paginated_results_key="CacheClusters",
    )
    caches = response.get("CacheClusters", []) if response else []
    method = MethodsEnum.ELASTICACHE_USES_DEFAULT_PORT
    vulns: Vulnerabilities = ()
    for cluster in caches:
        if cluster.get("Engine") == "memcached" and cluster.get("ConfigurationEndpoint")[
            "Port"
        ] in (11211, 6379):
            locations = [
                Location(
                    access_patterns=(
                        "/Engine",
                        "/ConfigurationEndpoint/Port",
                    ),
                    arn=(cluster["ARN"]),
                    values=(
                        cluster["Engine"],
                        cluster["ConfigurationEndpoint"]["Port"],
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster,
            )

    return (vulns, True)


@SHIELD
async def elasticache_is_transit_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elasticache",
        function="describe_cache_clusters",
        paginated_results_key="CacheClusters",
    )
    caches = response.get("CacheClusters", []) if response else []
    method = MethodsEnum.ELASTICACHE_IS_TRANSIT_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()
    for cluster in caches:
        if cluster.get("Engine") == "redis" and not cluster.get("TransitEncryptionEnabled", True):
            locations = [
                Location(
                    access_patterns=(
                        "/Engine",
                        "/TransitEncryptionEnabled",
                    ),
                    arn=(cluster["ARN"]),
                    values=(
                        cluster["Engine"],
                        cluster["TransitEncryptionEnabled"],
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster,
            )

    return (vulns, True)


@SHIELD
async def elasticache_is_at_rest_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elasticache",
        function="describe_cache_clusters",
        paginated_results_key="CacheClusters",
    )
    caches = response.get("CacheClusters", []) if response else []
    method = MethodsEnum.ELASTICACHE_IS_AT_REST_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()
    for cluster in caches:
        if cluster.get("Engine") == "redis" and not cluster.get("AtRestEncryptionEnabled", True):
            locations = [
                Location(
                    access_patterns=(
                        "/Engine",
                        "/AtRestEncryptionEnabled",
                    ),
                    arn=(cluster["ARN"]),
                    values=(
                        cluster["Engine"],
                        cluster["AtRestEncryptionEnabled"],
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster,
            )

    return (vulns, True)


@SHIELD
async def sns_is_server_side_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="sns",
        function="list_topics",
        region=region,
        paginated_results_key="Topics",
    )
    topics = response.get("Topics", []) if response else []
    method = MethodsEnum.SNS_IS_SERVER_SIDE_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()
    if topics:
        for topic in topics:
            topic_arn = topic["TopicArn"]
            get_topic_attributes: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="sns",
                function="get_topic_attributes",
                parameters={
                    "TopicArn": topic_arn,
                },
            )
            attrs = get_topic_attributes["Attributes"]
            if not attrs.get("KmsMasterKeyId", ""):
                locations = [
                    Location(
                        access_patterns=(),
                        arn=(topic_arn),
                        values=(),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=attrs,
                )
    return (vulns, True)


@SHIELD
async def sqs_is_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="sqs",
        function="list_queues",
        region=region,
    )
    queues = response.get("QueueUrls", []) if response else []
    method = MethodsEnum.SQS_IS_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()
    if queues:
        for queue_url in queues:
            get_queue_attributes: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="sqs",
                function="get_queue_attributes",
                parameters={
                    "QueueUrl": queue_url,
                    "AttributeNames": [
                        "QueueArn",
                        "KmsMasterKeyId",
                        "SqsManagedSseEnabled",
                    ],
                },
            )
            attr = get_queue_attributes.get("Attributes", {})
            if attr.get("SqsManagedSseEnabled") == "false" and not attr.get("KmsMasterKeyId"):
                locations = [
                    Location(
                        access_patterns=(),
                        arn=(attr["QueueArn"]),
                        values=(),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=attr,
                )

    return (vulns, True)


def aux_check_principal(statement: dict[str, Any]) -> bool:
    return (
        statement.get("Principal", {}).get("AWS", "") == "*"
        if isinstance(statement.get("Principal"), dict)
        else statement.get("Principal", "") == "*"
    )


@SHIELD
async def sns_can_anyone_publish(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="sns",
        function="list_topics",
        region=region,
        paginated_results_key="Topics",
    )
    topics = response.get("Topics", []) if response else []
    method = MethodsEnum.SNS_CAN_ANYONE_PUBLISH
    vulns: Vulnerabilities = ()
    for topic in topics:
        topic_arn = topic["TopicArn"]
        get_topic_attributes: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="sns",
            function="get_topic_attributes",
            parameters={
                "TopicArn": topic_arn,
            },
        )
        attrs = get_topic_attributes["Attributes"]
        if policy := json.loads(attrs.get("Policy", "{}")):
            for statement in policy.get("Statement", []):
                condition = (
                    statement.get("Effect", "") == "Allow"
                    and aux_check_principal(statement)
                    and (
                        {"SNS:Publish"}.issubset(statement.get("Action", {}))
                        or statement.get("Action", "") == "SNS:Publish"
                    )
                    and statement.get("Resource", "") == topic_arn
                )
                locations = []
                if condition and not statement.get("Condition", {}):
                    locations.append(
                        Location(
                            access_patterns=("/Principal/AWS", "/Action"),
                            arn=(topic_arn),
                            values=(
                                statement["Principal"]["AWS"],
                                statement["Action"],
                            ),
                            method=method,
                        ),
                    )
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=statement,
                )

    return (vulns, True)


@SHIELD
async def sns_can_anyone_subscribe(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="sns",
        function="list_topics",
        region=region,
        paginated_results_key="Topics",
    )
    topics = response.get("Topics", []) if response else []
    method = MethodsEnum.SNS_CAN_ANYONE_SUBSCRIBE
    vulns: Vulnerabilities = ()
    for topic in topics:
        topic_arn = topic["TopicArn"]
        get_topic_attributes: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="sns",
            function="get_topic_attributes",
            parameters={
                "TopicArn": topic_arn,
            },
        )
        attrs = get_topic_attributes["Attributes"]
        if policy := json.loads(attrs.get("Policy", "{}")):
            for statement in policy.get("Statement", []):
                condition = (
                    statement.get("Effect", "") == "Allow"
                    and aux_check_principal(statement)
                    and {"SNS:Subscribe", "SNS:Receive"}.issubset(statement.get("Action", {}))
                    and statement.get("Resource", "") == topic_arn
                )
                locations = []
                if condition and not statement.get("Condition", {}):
                    locations.append(
                        Location(
                            access_patterns=("/Principal/AWS", "/Action"),
                            arn=(topic_arn),
                            values=(
                                statement["Principal"]["AWS"],
                                statement["Action"],
                            ),
                            method=method,
                        ),
                    )
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=statement,
                )

    return (vulns, True)


@SHIELD
async def policies_attached_to_users(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.POLICIES_ATTACHED_TO_USERS
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_users",
        paginated_results_key="Users",
    )
    users = response.get("Users", []) if response else []

    vulns: Vulnerabilities = ()
    if users:
        for user in users:
            user_policies: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="list_attached_user_policies",
                parameters={"UserName": str(user["UserName"])},
                paginated_results_key="AttachedPolicies",
            )
            attached_policies = user_policies.get("AttachedPolicies", [])

            if attached_policies:
                locations = [
                    Location(
                        access_patterns=("/AttachedPolicies",),
                        arn=(f"{user['Arn']}"),
                        values=(attached_policies,),
                        method=method,
                    ),
                ]

                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=user_policies,
                )

    return (vulns, True)


@SHIELD
async def acm_certificate_expired(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ACM_CERTIFICATE_EXPIRED
    vulns: Vulnerabilities = ()
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="acm",
        function="list_certificates",
        region=region,
        parameters={
            "CertificateStatuses": [
                "EXPIRED",
            ],
        },
        paginated_results_key="CertificateSummaryList",
    )
    acm_expired_certificates = response.get("CertificateSummaryList", [])
    for expired_certificate in acm_expired_certificates:
        arn = expired_certificate.get("CertificateArn")
        status = expired_certificate.get("Status")
        locations = [
            Location(
                arn=(arn),
                access_patterns=("/Status",),
                values=(status,),
                method=method,
            ),
        ]
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=expired_certificate,
        )

    return (vulns, True)


def get_vulns_for_apis_with_unencrypted_cache(
    stage: dict[str, Any],
    arn: str,
    method: MethodsEnum,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    cache_status = stage.get("cacheClusterStatus")
    cache_enabled = stage.get("cacheClusterEnabled")
    if not cache_enabled or cache_status != "AVAILABLE":
        return ()
    method_settings = stage.get("methodSettings", {})
    for key, setting in method_settings.items():
        cache_data_encrypted = setting.get("cacheDataEncrypted")
        if not cache_data_encrypted:
            locations = [
                Location(
                    access_patterns=(f"/methodSettings/{key}/cacheDataEncrypted",),
                    values=(cache_data_encrypted,),
                    arn=arn,
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=stage,
            )
    return vulns


@SHIELD
async def api_gateway_cache_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.API_GATEWAY_CACHE_ENCRYPTION_DISABLED
    api_rests: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="apigateway",
        function="get_rest_apis",
        paginated_results_key="items",
    )
    items = api_rests.get("items", [])
    for rest_api in items:
        api_id = rest_api["id"]
        api_stages: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="apigateway",
            function="get_stages",
            parameters={"restApiId": api_id},
        )
        for stage in api_stages["item"]:
            parameters = {
                "Region": region,
                "StageName": stage["stageName"],
                "ApiId": api_id,
            }
            arn = build_arn(
                service="apigateway",
                resource="Stage",
                parameters=parameters,
            )
            vulns += get_vulns_for_apis_with_unencrypted_cache(stage, arn, method)
    return (vulns, True)


async def list_backup_vaults(credentials: AwsCredentials, region: str) -> list[dict[str, Any]]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="backup",
        function="list_backup_vaults",
        paginated_results_key="BackupVaultList",
    )
    return response.get("BackupVaultList", [])


async def get_backup_vault_access_policy(
    credentials: AwsCredentials,
    region: str,
    vault_name: str,
) -> dict[str, Any]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="backup",
        function="get_backup_vault_access_policy",
        parameters={"BackupVaultName": vault_name},
    )
    if response and response.get("Policy", {}):
        return json.loads(response.get("Policy", {}))
    return {}


@SHIELD
async def backup_vault_policy_allow_delete_recovery_points(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.BACKUP_VAULT_POLICY_ALLOW_DELETE_RECOVERY_POINTS
    backup_vaults = await list_backup_vaults(
        credentials=credentials,
        region=region,
    )
    for vault in backup_vaults:
        vault_name = vault["BackupVaultName"]
        vault_arn = vault["BackupVaultArn"]
        try:
            vault_policy = await get_backup_vault_access_policy(
                credentials=credentials,
                region=region,
                vault_name=vault_name,
            )
            statements = vault_policy.get("Statement", {})
            if not any(
                statement["Effect"] == "Deny"
                and "backup:DeleteRecoveryPoint" in statement["Action"]
                and statement["Principal"] in ["*", {"AWS": "*"}]
                and (
                    any(
                        fnmatch.fnmatchcase(vault_arn, resource)
                        for resource in statement["Resource"]
                    )
                    if isinstance(statement["Resource"], list)
                    else fnmatch.fnmatchcase(vault_arn, statement["Resource"])
                )
                for statement in statements
            ):
                locations = [
                    Location(
                        arn=vault_arn,
                        access_patterns=("/VaultAccesPolicy/Statement",),
                        values=("No statement blocks deletion of recovery points",),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response={
                        "BackupVaultArn": vault_arn,
                        "VaultAccesPolicy": {"Statement": statements},
                    },
                )
        except BotoClientError:
            err = traceback.format_exc()
            vuln_pattern = r"An error occurred.*Backup Vault.*has no associated POLICY"
            if re.search(vuln_pattern, err):
                locations = [
                    Location(
                        arn=vault_arn,
                        access_patterns=(),
                        values=(),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response={"BackupVaultArn": vault_arn},
                )
            else:
                raise
    return (vulns, True)


@SHIELD
async def aws_bedrock_guardrails_no_sensitive_info_filter(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_BEDROCK_GUARDRAILS_NO_SENSITIVE_INFO_FILTER
    bedrock_guardrails: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="bedrock",
        function="list_guardrails",
        paginated_results_key="guardrails",
    )
    for guardrail in bedrock_guardrails.get("guardrails", []):
        guardrail_id = guardrail["id"]
        guardrail_arn = guardrail["arn"]
        guardrail_data: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="bedrock",
            function="get_guardrail",
            parameters={"guardrailIdentifier": guardrail_id},
        )
        sensitive_information_policy = guardrail_data.get("sensitiveInformationPolicy")
        if not sensitive_information_policy:
            locations = [
                Location(
                    arn=guardrail_arn,
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=guardrail,
            )

    return (vulns, True)


@SHIELD
async def aws_event_bridge_default_event_bus_exposed(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_EVENT_BRIDGE_DEFAULT_EVENT_BUS_EXPOSED
    default_event_bus: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="events",
        function="describe_event_bus",
    )
    arn = default_event_bus.get("Arn")
    if arn:
        policy = json.loads(default_event_bus["Policy"]) if default_event_bus.get("Policy") else {}
        for index, statement in enumerate(policy.get("Statement", [])):
            principal = statement.get("Principal")
            if (
                principal in ["*", {"AWS": "*"}, ["*"]]
                and statement.get("Effect") == "Allow"
                and (
                    any(fnmatch.fnmatch(arn, resource) for resource in statement.get("Resource"))
                    if isinstance(statement["Resource"], list)
                    else fnmatch.fnmatch(arn, statement.get("Resource"))
                )
                and not statement.get("Condition")
            ):
                default_event_bus["Policy"] = policy
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=(f"/Policy/Statement/{index}/Principal",),
                        values=(principal,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=default_event_bus,
                )

    return (vulns, True)


@SHIELD
async def aws_lambda_url_without_authentication(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_LAMBDA_URL_WITHOUT_AUTHENTICATION
    functions: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="lambda",
        function="list_functions",
        paginated_results_key="Functions",
    )
    for function in functions.get("Functions", []):
        arn = function["FunctionArn"]
        name = function.get("FunctionName", "")
        urls: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="lambda",
            function="list_function_url_configs",
            parameters={
                "FunctionName": name,
            },
        )
        configs = urls.get("FunctionUrlConfigs", [])
        for index, config in enumerate(configs):
            auth_type = config.get("AuthType")
            if auth_type == "NONE":
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=(f"/FunctionUrlConfigs/{index}/AuthType",),
                        values=(auth_type,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=urls,
                )

    return (vulns, True)


@SHIELD
async def aws_lambda_function_exposed(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_LAMBDA_FUNCTION_EXPOSED
    functions: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="lambda",
        function="list_functions",
        paginated_results_key="Functions",
    )
    for function in functions.get("Functions", []):
        arn = function["FunctionArn"]
        name = function.get("FunctionName", "")
        lambda_policy = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="lambda",
            function="get_policy",
            parameters={"FunctionName": name},
        )
        policy = json.loads(lambda_policy.get("Policy", "{}"))
        for index, statement in enumerate(policy.get("Statement", [])):
            principal = statement.get("Principal")
            if is_permissive_policy_statement(resource_arn=arn, statement=statement):
                function["Policy"] = policy
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=(f"/Policy/Statement/{index}/Principal",),
                        values=(principal,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=function,
                )

    return (vulns, True)


@SHIELD
async def aws_comprehend_analysis_without_encryption(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_COMPREHEND_ANALYSIS_WITHOUT_ENCRYPTION
    paginated_results_key = "EntitiesDetectionJobPropertiesList"
    jobs: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="comprehend",
        function="list_entities_detection_jobs",
        paginated_results_key=paginated_results_key,
    )
    for job in jobs.get(paginated_results_key, []):
        output_config = job.get("OutputDataConfig", {})
        kms_key = output_config.get("KmsKeyId")
        arn = job["JobArn"]
        if not kms_key:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/OutputDataConfig",),
                    values=("KmsKeyId parameter not found",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=job,
            )

    return (vulns, True)


@SHIELD
async def aws_ebs_public_snapshot(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_EBS_PUBLIC_SNAPSHOT
    paginated_results_key = "Snapshots"
    owner_id = await get_owner_id(credentials)
    snapshots: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="ec2",
        function="describe_snapshots",
        paginated_results_key=paginated_results_key,
        parameters={
            "Filters": [
                {
                    "Name": "status",
                    "Values": [
                        "completed",
                    ],
                },
                {"Name": "owner-id", "Values": [owner_id]},
            ],
        },
    )
    for snapshot in snapshots.get(paginated_results_key, []):
        snapshot_id = snapshot["SnapshotId"]
        attributes = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="ec2",
            function="describe_snapshot_attribute",
            parameters={
                "SnapshotId": snapshot_id,
                "Attribute": "createVolumePermission",
            },
        )
        permissions = attributes.get("CreateVolumePermissions", {})
        if permissions == [{"Group": "all"}]:
            snapshot["CreateVolumePermissions"] = permissions
            parameters = {
                "Region": region,
                "SnapshotId": snapshot_id,
            }
            arn = build_arn(
                service="ec2",
                resource="snapshot",
                parameters=parameters,
            )
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/CreateVolumePermissions",),
                    values=(permissions,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=snapshot,
            )

    return (vulns, True)


@SHIELD
async def aws_eks_unencrypted_secrets(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_EKS_UNENCRYPTED_SECRETS
    paginated_results_key = "clusters"
    eks_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="eks",
        function="list_clusters",
        paginated_results_key=paginated_results_key,
    )
    for cluster_name in eks_clusters.get(paginated_results_key, []):
        _cluster = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="eks",
            function="describe_cluster",
            parameters={"name": cluster_name},
        )
        cluster = _cluster["cluster"]
        arn = cluster["arn"]
        encryption_config = cluster.get("encryptionConfig")
        if not encryption_config:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster,
            )

    return (vulns, True)


@SHIELD
async def aws_emr_has_not_security_config(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_EMR_HAS_NOT_SECURITY_CONFIG
    paginated_results_key = "Clusters"
    emr_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="emr",
        function="list_clusters",
        paginated_results_key=paginated_results_key,
    )
    for cluster in emr_clusters.get(paginated_results_key, []):
        cluster_id = cluster["Id"]
        _cluster = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="emr",
            function="describe_cluster",
            parameters={"ClusterId": cluster_id},
        )
        cluster_attrs = _cluster["Cluster"]
        arn = cluster_attrs["ClusterArn"]
        encryption_config = cluster_attrs.get("SecurityConfiguration")
        if not encryption_config:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster_attrs,
            )

    return (vulns, True)


@SHIELD
async def aws_opensearch_without_encryption_at_rest(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_OPENSEARCH_WITHOUT_ENCRYPTION_AT_REST
    environments: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="opensearch",
        function="list_domain_names",
    )
    for domain in environments.get("DomainNames", []):
        domain_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="opensearch",
            function="describe_domain",
            parameters={
                "DomainName": domain["DomainName"],
            },
        )
        domain_status = domain_config.get("DomainStatus", {})
        encryption_at_rest_bool = domain_status.get("EncryptionAtRestOptions", {}).get(
            "Enabled",
            True,
        )
        if not encryption_at_rest_bool:
            locations = [
                Location(
                    arn=domain_status["ARN"],
                    access_patterns=("/DomainStatus/EncryptionAtRestOptions/Enabled",),
                    values=(encryption_at_rest_bool,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=domain_config,
            )

    return (vulns, True)


@SHIELD
async def aws_opensearch_domain_node_to_node_encryption(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_OPENSEARCH_DOMAIN_NODE_TO_NODE_ENCRYPTION
    environments: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="opensearch",
        function="list_domain_names",
    )
    for domain in environments.get("DomainNames", []):
        domain_name = domain["DomainName"]
        domain_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="opensearch",
            function="describe_domain",
            parameters={
                "DomainName": domain_name,
            },
        )
        domain_status = domain_config.get("DomainStatus", {})
        node_to_node_encryption_bool = domain_status.get("NodeToNodeEncryptionOptions", {}).get(
            "Enabled",
        )
        if not node_to_node_encryption_bool:
            locations = [
                Location(
                    arn=domain_status["ARN"],
                    access_patterns=("/DomainStatus/NodeToNodeEncryptionOptions/Enabled",),
                    values=(node_to_node_encryption_bool,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=domain_config,
            )

    return (vulns, True)


@SHIELD
async def aws_glue_catalog_without_encryption_at_rest(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_GLUE_CATALOG_WITHOUT_ENCRYPTION_AT_REST
    databases: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="glue",
        function="get_databases",
    )
    catalog_encryption_settings: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="glue",
        function="get_data_catalog_encryption_settings",
    )
    catalog_encryption_at_rest = (
        catalog_encryption_settings.get("DataCatalogEncryptionSettings", {})
        .get("EncryptionAtRest", {})
        .get("CatalogEncryptionMode")
    )
    if len(databases.get("DatabaseList", [])) > 0 and catalog_encryption_at_rest == "DISABLED":
        parameters = {
            "Account": await get_owner_id(credentials),
            "Region": region,
            "CatalogName": "DefaultCatalog",
        }
        arn = build_arn(
            service="glue",
            resource="catalog",
            parameters=parameters,
        )
        locations = [
            Location(
                arn=arn,
                access_patterns=(
                    "/DataCatalogEncryptionSettings/EncryptionAtRest/CatalogEncryptionMode",
                ),
                values=("DISABLED",),
                method=method,
            ),
        ]
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=catalog_encryption_settings,
        )

    return (vulns, True)


@SHIELD
async def aws_kinesis_stream_without_encryption_at_rest(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_KINESIS_STREAM_WITHOUT_ENCRYPTION_AT_REST
    streams: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="kinesis",
        function="list_streams",
        paginated_results_key="StreamNames",
    )
    for stream in streams.get("StreamNames", []):
        stream_desc = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="kinesis",
            function="describe_stream",
            parameters={"StreamName": stream},
        )
        stream_description = stream_desc.get("StreamDescription", {})
        stream_encryption_type = stream_description.get("EncryptionType", "")
        if stream_encryption_type == "NONE":
            locations = [
                Location(
                    arn=stream_description.get("StreamARN"),
                    access_patterns=("/StreamDescription/EncryptionType",),
                    values=(stream_encryption_type,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=stream_desc,
            )

    return (vulns, True)


@SHIELD
async def aws_mq_broker_publicly_accessible(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_MQ_BROKER_PUBLICLY_ACCESSIBLE
    paginated_results_key = "BrokerSummaries"
    brokers: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="mq",
        function="list_brokers",
        paginated_results_key=paginated_results_key,
    )
    for summary in brokers.get(paginated_results_key, []):
        broker_description: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="mq",
            function="describe_broker",
            parameters={"BrokerId": summary["BrokerId"]},
        )
        publicly_accessible = broker_description.get("PubliclyAccessible", {})
        if publicly_accessible:
            locations = [
                Location(
                    arn=broker_description["BrokerArn"],
                    access_patterns=("/PubliclyAccessible",),
                    values=(publicly_accessible,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=broker_description,
            )

    return (vulns, True)


@SHIELD
async def aws_msk_cluster_is_publicly_accessible(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_MSK_CLUSTER_IS_PUBLICLY_ACCESSIBLE
    paginated_results_key = "ClusterInfoList"
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="kafka",
        function="list_clusters_v2",
        paginated_results_key=paginated_results_key,
    )
    for cluster in clusters.get(paginated_results_key, []):
        if cluster.get("ClusterType", "") == "SERVERLESS":
            continue
        cluster_arn = cluster["ClusterArn"]
        cluster_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="kafka",
            function="describe_cluster",
            parameters={
                "ClusterArn": cluster_arn,
            },
        )
        public_access_type = (
            cluster_config.get("ClusterInfo", {})
            .get("BrokerNodeGroupInfo", {})
            .get("ConnectivityInfo", {})
            .get("PublicAccess", {})
            .get("Type", "")
        )
        if public_access_type == "SERVICE_PROVIDED_EIPS":
            locations = [
                Location(
                    arn=cluster_arn,
                    access_patterns=(
                        "/ClusterInfo/BrokerNodeGroupInfo/ConnectivityInfo/PublicAccess/Type",
                    ),
                    values=(public_access_type,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster_config,
            )

    return (vulns, True)


@SHIELD
async def aws_neptune_db_instance_without_encryption_at_rest(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_NEPTUNE_DB_INSTANCE_WITHOUT_ENCRYPTION_AT_REST
    paginated_results_key = "DBInstances"
    db_instances: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="neptune",
        function="describe_db_instances",
        paginated_results_key=paginated_results_key,
    )
    for db_instance in db_instances.get(paginated_results_key, []):
        arn = db_instance["DBInstanceArn"]
        storage_encrypted = db_instance.get("StorageEncrypted")
        if "StorageEncrypted" in db_instance and not storage_encrypted:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/StorageEncrypted",),
                    values=(storage_encrypted,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_instance,
            )

    return (vulns, True)


@SHIELD
async def aws_route53_transfer_lock_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_ROUTE53_TRANSFER_LOCK_DISABLED
    paginated_results_key = "Domains"
    domains: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="route53domains",
        function="list_domains",
        paginated_results_key=paginated_results_key,
    )
    for domain in domains.get(paginated_results_key, []):
        domain_name = domain.get("DomainName", "")
        domain_details: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="route53domains",
            function="get_domain_detail",
            parameters={"DomainName": domain_name},
        )
        status_list = domain_details.get("StatusList", {})
        if not any(status == "clientTransferProhibited" for status in status_list):
            account = get_owner_id(credentials)
            # Dummy ARN, since the domains do not have ARNs
            arn = f"arn:aws:route53domains:::{account}:domain/{domain_name}"
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/StatusList",),
                    values=(status_list,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=domain_details,
            )
    return (vulns, True)


@SHIELD
async def aws_sagemaker_training_job_intercontainer_encryption(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_SAGEMAKER_TRAINING_JOB_INTERCONTAINER_ENCRYPTION
    paginated_results_key = "TrainingJobSummaries"
    training_jobs: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="sagemaker",
        function="list_training_jobs",
        paginated_results_key=paginated_results_key,
    )
    for job in training_jobs.get(paginated_results_key, []):
        job_name = job.get("TrainingJobName", "")
        arn = job.get("TrainingJobArn", "")
        jobs_description: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="sagemaker",
            function="describe_training_job",
            parameters={"TrainingJobName": job_name},
        )
        intercontainer_encryption = jobs_description.get(
            "EnableInterContainerTrafficEncryption",
            True,
        )
        if not intercontainer_encryption:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/EnableInterContainerTrafficEncryption",),
                    values=(intercontainer_encryption,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=jobs_description,
            )
    return (vulns, True)


@SHIELD
async def aws_sagemaker_notebook_instance_encryption(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_SAGEMAKER_NOTEBOOK_INSTANCE_ENCRYPTION
    paginated_results_key = "NotebookInstances"
    notebooks: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="sagemaker",
        function="list_notebook_instances",
        paginated_results_key=paginated_results_key,
    )
    for instance in notebooks.get(paginated_results_key, []):
        notebook_name = instance.get("NotebookInstanceName")
        notebook_details: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="sagemaker",
            function="describe_notebook_instance",
            parameters={"NotebookInstanceName": notebook_name},
        )
        if "KmsKeyId" not in notebook_details:
            notebook_details["KmsKeyId"] = None
            locations = [
                Location(
                    arn=notebook_details["NotebookInstanceArn"],
                    access_patterns=("/KmsKeyId",),
                    values=(None,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=notebook_details,
            )
    return (vulns, True)


@SHIELD
async def aws_athena_workgroup_query_results_not_encrypted(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_ATHENA_WORKGROUP_QUERY_RESULTS_NOT_ENCRYPTED
    paginated_results_key = "WorkGroups"
    work_groups: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="athena",
        function="list_work_groups",
        paginated_results_key=paginated_results_key,
    )
    for work_group in work_groups.get(paginated_results_key, []):
        query_executions: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="athena",
            function="list_query_executions",
            parameters={"WorkGroup": work_group["Name"]},
        )
        if len(query_executions["QueryExecutionIds"]) == 0:
            continue
        _work_group_details: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="athena",
            function="get_work_group",
            parameters={"WorkGroup": work_group["Name"]},
        )
        work_group_details = _work_group_details.get("WorkGroup", {})
        configuration = work_group_details.get("Configuration", {})
        encryption_conf = configuration.get("ResultConfiguration", {}).get(
            "EncryptionConfiguration",
        )
        if configuration and not encryption_conf:
            arn = work_group_details.get("Arn")
            if not arn:
                parameters = {
                    "Account": await get_owner_id(credentials),
                    "Region": region,
                    "WorkGroupName": work_group["Name"],
                }
                arn = build_arn(
                    service="athena",
                    resource="workgroup",
                    parameters=parameters,
                )
            locations = [
                Location(
                    arn=arn,
                    access_patterns=(
                        "/WorkGroup/Configuration/ResultConfiguration/EncryptionOption",
                    ),
                    values=(encryption_conf,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=_work_group_details,
            )
    return (vulns, True)


@SHIELD
async def aws_dax_cluster_without_encryption_at_rest(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_DAX_CLUSTER_WITHOUT_ENCRYPTION_AT_REST
    paginated_results_key = "Clusters"
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="dax",
        function="describe_clusters",
        paginated_results_key=paginated_results_key,
    )
    for cluster in clusters.get(paginated_results_key, []):
        server_side_encryption = cluster.get("SSEDescription", {}).get("Status")
        if server_side_encryption == "DISABLED":
            locations = [
                Location(
                    arn=(cluster["ClusterArn"]),
                    access_patterns=("/SSEDescription",),
                    values=(server_side_encryption,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster,
            )

    return (vulns, True)


@SHIELD
async def aws_unencrypted_ecr_repository(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_UNENCRYPTED_ECR_REPOSITORY
    paginated_results_key = "repositories"
    ecr_repositories: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="ecr",
        function="describe_repositories",
        paginated_results_key=paginated_results_key,
    )
    for repository in ecr_repositories.get(paginated_results_key, []):
        arn = repository["repositoryArn"]
        encryption_config = repository.get("encryptionConfiguration", {})
        if not encryption_config:
            locations = [
                Location(
                    arn=(arn),
                    access_patterns=("/encryptionConfiguration",),
                    values=(encryption_config,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=repository,
            )
    return (vulns, True)


@SHIELD
async def aws_rds_unencrypted_db_cluster_snapshot(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_UNENCRYPTED_DB_CLUSTER_SNAPSHOT
    paginated_results_key = "DBClusterSnapshots"
    rds_cluster_snapshots: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_cluster_snapshots",
        paginated_results_key=paginated_results_key,
    )
    for snapshot in rds_cluster_snapshots.get(paginated_results_key, []):
        arn = snapshot["DBClusterSnapshotArn"]
        storage_encrypted = snapshot.get("StorageEncrypted", True)
        if not storage_encrypted:
            locations = [
                Location(
                    arn=(arn),
                    access_patterns=("/StorageEncrypted",),
                    values=(storage_encrypted,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=snapshot,
            )
    return (vulns, True)


@SHIELD
async def aws_rds_unencrypted_db_snapshot(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_UNENCRYPTED_DB_SNAPSHOT
    paginated_results_key = "DBSnapshots"
    rds_db_instance_snapshots: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_snapshots",
        paginated_results_key=paginated_results_key,
    )
    for snapshot in rds_db_instance_snapshots.get(paginated_results_key, []):
        arn = snapshot["DBSnapshotArn"]
        encrypted = snapshot.get("Encrypted", True)
        if not encrypted:
            locations = [
                Location(
                    arn=(arn),
                    access_patterns=("/Encrypted",),
                    values=(encrypted,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=snapshot,
            )
    return (vulns, True)


@SHIELD
async def aws_alb_does_not_drop_invalid_header_fields(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_ALB_DOES_NOT_DROP_INVALID_HEADER_FIELDS
    paginated_results_key = "LoadBalancers"
    load_balancers: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elbv2",
        function="describe_load_balancers",
        paginated_results_key=paginated_results_key,
    )
    for load_balancer in load_balancers.get(paginated_results_key, []):
        if load_balancer.get("Type", "") != "application":
            continue
        arn = load_balancer["LoadBalancerArn"]
        alb_attrs: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="elbv2",
            function="describe_load_balancer_attributes",
            parameters={"LoadBalancerArn": arn},
        )
        for index, attr in enumerate(alb_attrs.get("Attributes", [])):
            if (
                attr.get("Key") == ("routing.http.drop_invalid_header_fields.enabled")
                and attr.get("Value") == "false"
            ):
                locations = [
                    Location(
                        arn=(arn),
                        access_patterns=(f"/Attributes/{index}",),
                        values=(attr,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=alb_attrs,
                )
    return (vulns, True)


@SHIELD
async def aws_public_accessible_dms_replication(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_PUBLIC_ACCESSIBLE_DMS_REPLICATION
    paginated_results_key = "ReplicationInstances"
    replication_instances: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="dms",
        function="describe_replication_instances",
        paginated_results_key=paginated_results_key,
    )
    for replication_instance in replication_instances.get(paginated_results_key, []):
        arn = replication_instance["ReplicationInstanceArn"]
        if is_public := replication_instance.get("PubliclyAccessible", False):
            locations = [
                Location(
                    arn=(arn),
                    access_patterns=("/PubliclyAccessible",),
                    values=(is_public,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=replication_instance,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    acm_certificate_expired,
    users_with_multiple_access_keys,
    root_has_access_keys,
    has_root_active_signing_certificates,
    eks_has_endpoints_publicly_accessible,
    rds_has_public_snapshots,
    rds_not_uses_iam_authentication,
    redshift_has_public_clusters,
    redshift_not_requires_ssl,
    elasticache_uses_default_port,
    elasticache_is_transit_encryption_disabled,
    elasticache_is_at_rest_encryption_disabled,
    sns_is_server_side_encryption_disabled,
    sqs_is_encryption_disabled,
    sns_can_anyone_publish,
    sns_can_anyone_subscribe,
    policies_attached_to_users,
    api_gateway_cache_encryption_disabled,
    backup_vault_policy_allow_delete_recovery_points,
    aws_bedrock_guardrails_no_sensitive_info_filter,
    aws_event_bridge_default_event_bus_exposed,
    aws_lambda_url_without_authentication,
    aws_lambda_function_exposed,
    aws_comprehend_analysis_without_encryption,
    aws_ebs_public_snapshot,
    aws_eks_unencrypted_secrets,
    aws_emr_has_not_security_config,
    aws_opensearch_without_encryption_at_rest,
    aws_opensearch_domain_node_to_node_encryption,
    aws_glue_catalog_without_encryption_at_rest,
    aws_kinesis_stream_without_encryption_at_rest,
    aws_mq_broker_publicly_accessible,
    aws_msk_cluster_is_publicly_accessible,
    aws_neptune_db_instance_without_encryption_at_rest,
    aws_route53_transfer_lock_disabled,
    aws_sagemaker_training_job_intercontainer_encryption,
    aws_sagemaker_notebook_instance_encryption,
    aws_athena_workgroup_query_results_not_encrypted,
    aws_dax_cluster_without_encryption_at_rest,
    aws_unencrypted_ecr_repository,
    aws_rds_unencrypted_db_cluster_snapshot,
    aws_rds_unencrypted_db_snapshot,
    aws_alb_does_not_drop_invalid_header_fields,
    aws_public_accessible_dms_replication,
)
