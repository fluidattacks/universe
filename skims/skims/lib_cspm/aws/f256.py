from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


async def clusters_with_deletion_protection_disabled(
    credentials: AwsCredentials,
    region: str,
) -> list[str]:
    paginated_results_key = "DBClusters"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="rds",
        function="describe_db_clusters",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    if not response:
        return []

    return [
        cluster.get("DBClusterIdentifier")
        for cluster in response.get(paginated_results_key, [])
        if not cluster.get("DeletionProtection", True)
    ]


@SHIELD
async def rds_has_not_deletion_protection(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    cluster_without_deletion_protection = await clusters_with_deletion_protection_disabled(
        credentials,
        region,
    )
    paginated_results_key = "DBInstances"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_instances",
        paginated_results_key=paginated_results_key,
    )
    db_instances = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.RDS_HAS_NOT_DELETION_PROTECTION
    vulns: Vulnerabilities = ()
    for instance in db_instances:
        db_cluster = instance.get("DBClusterIdentifier")
        if not instance.get("DeletionProtection") and (
            not db_cluster or db_cluster in cluster_without_deletion_protection
        ):
            locations = [
                Location(
                    access_patterns=("/DeletionProtection",),
                    arn=(instance["DBInstanceArn"]),
                    values=(instance.get("DeletionProtection"),),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=instance,
            )

    return (vulns, True)


@SHIELD
async def rds_has_not_automated_backups(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "DBInstances"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_instances",
        paginated_results_key=paginated_results_key,
    )
    db_instances = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.RDS_HAS_NOT_AUTOMATED_BACKUPS
    vulns: Vulnerabilities = ()

    for instance in db_instances:
        if instance.get("BackupRetentionPeriod") == 0:
            instance_arn = instance["DBInstanceArn"]
            locations = [
                Location(
                    access_patterns=("/BackupRetentionPeriod",),
                    arn=(f"{instance_arn}"),
                    values=(instance.get("BackupRetentionPeriod"),),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=instance,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    rds_has_not_deletion_protection,
    rds_has_not_automated_backups,
)
