from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def dynamodb_has_not_deletion_protection(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "TableNames"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="dynamodb",
        function="list_tables",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    table_names = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.DYNAMODB_HAS_NOT_DELETION_PROTECTION
    vulns: Vulnerabilities = ()

    for table in table_names:
        locations: list[Location] = []
        describe_table: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="dynamodb",
            function="describe_table",
            parameters={
                "TableName": table,
            },
        )
        table_arn = describe_table["Table"]["TableArn"]
        del_protec = describe_table["Table"].get("DeletionProtectionEnabled", None)
        if del_protec is not None and del_protec not in {
            True,
            "true",
            "True",
        }:
            locations = [
                Location(
                    access_patterns=("/Table/DeletionProtectionEnabled",),
                    arn=(table_arn),
                    values=(del_protec,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=describe_table,
            )
    return (vulns, True)


@SHIELD
async def dynamodb_has_not_point_in_time_recovery(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "TableNames"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="dynamodb",
        function="list_tables",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    table_names = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.DYNAMODB_HAS_NOT_POINT_IN_TIME_RECOVERY
    vulns: Vulnerabilities = ()

    for table in table_names:
        table_backup: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="dynamodb",
            function="describe_continuous_backups",
            parameters={
                "TableName": table,
            },
        )
        describe_table: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="dynamodb",
            function="describe_table",
            parameters={
                "TableName": table,
            },
        )
        table_arn = describe_table["Table"]["TableArn"]
        backup_description = table_backup.get("ContinuousBackupsDescription", {})

        if (
            backup_description["PointInTimeRecoveryDescription"]["PointInTimeRecoveryStatus"]
            == "DISABLED"
        ):
            locations = [
                Location(
                    access_patterns=(
                        (
                            "/ContinuousBackupsDescription/"
                            "PointInTimeRecoveryDescription/"
                            "PointInTimeRecoveryStatus"
                        ),
                    ),
                    arn=(table_arn),
                    values=(
                        backup_description["PointInTimeRecoveryDescription"][
                            "PointInTimeRecoveryStatus"
                        ],
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=table_backup,
            )
    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    dynamodb_has_not_deletion_protection,
    dynamodb_has_not_point_in_time_recovery,
)
