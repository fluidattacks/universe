from contextlib import (
    suppress,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    aws_virtual_gateways_for_app_meshes,
    build_arn,
    build_mesh_virtual_gateway_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)
from utils.cipher_suites import (
    PREDEFINED_SSL_POLICY_VALUES,
    SAFE_SSL_POLICY_VALUES,
)


def _minimum_protocol_version(
    distribution_config: dict[Any, Any],
    distribution: dict[Any, Any],
    method: MethodsEnum,
) -> list[Location]:
    locations: list[Location] = []
    vulnerable_min_prot_versions = [
        "SSLv3",
        "TLSv1",
        "TLSv1_2016",
        "TLSv1.1_2016",
    ]
    with suppress(KeyError):
        min_prot_ver = distribution_config["ViewerCertificate"]["MinimumProtocolVersion"]

        if min_prot_ver in vulnerable_min_prot_versions:
            locations = [
                Location(
                    access_patterns=("/ViewerCertificate/MinimumProtocolVersion",),
                    arn=(f"{distribution['ARN']}"),
                    values=(min_prot_ver,),
                    method=method,
                ),
            ]
    return locations


async def describe_load_balancers(credentials: AwsCredentials, region: str) -> list[dict[str, Any]]:
    paginated_results_key = "LoadBalancers"
    load_balancers: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="elbv2",
        function="describe_load_balancers",
        paginated_results_key=paginated_results_key,
    )
    return load_balancers.get(paginated_results_key, []) if load_balancers else []


def _origin_ssl_protocols(
    distribution_config: dict[Any, Any],
    distribution: dict[Any, Any],
    method: MethodsEnum,
) -> list[Location]:
    vulnerable_origin_ssl_protocols = ["SSLv3", "TLSv1", "TLSv1.1"]
    locations: list[Location] = []
    for index, origin in enumerate(distribution_config["Origins"]["Items"]):
        if custom_origin_config := origin.get("CustomOriginConfig", {}):
            locations.extend(
                [
                    Location(
                        access_patterns=(
                            (f"/Origins/Items/{index}/CustomOriginConfig/OriginSslProtocols/Items"),
                        ),
                        arn=(f"{distribution['ARN']}"),
                        values=(ssl_protocols,),
                        method=method,
                    )
                    for ssl_protocols in custom_origin_config["OriginSslProtocols"]["Items"]
                    if ssl_protocols in vulnerable_origin_ssl_protocols
                ],
            )
    return locations


@SHIELD
async def serves_content_over_insecure_protocols(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.SERVES_CONTENT_OVER_INSECURE_PROTOCOLS
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="cloudfront",
        function="list_distributions",
    )
    distribution_list = response.get("DistributionList", {}) if response else {}
    vulns: Vulnerabilities = ()
    if distribution_list:
        for distribution in distribution_list.get("Items", []):
            config: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="cloudfront",
                function="get_distribution",
                parameters={
                    "Id": str(distribution["Id"]),
                },
            )
            distribution_config = config["Distribution"]["DistributionConfig"]
            locations = _minimum_protocol_version(distribution_config, dict(distribution), method)
            locations.extend(_origin_ssl_protocols(distribution_config, dict(distribution), method))

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=distribution_config,
            )

    return (vulns, True)


@SHIELD
async def elbv2_uses_insecure_ssl_protocol(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ELBV2_USES_INSECURE_SSL_PROTOCOL
    vuln_protocols = ["SSLv3", "TLSv1", "TLSv1.1"]
    balancers = await describe_load_balancers(credentials, region)
    vulns: Vulnerabilities = ()
    if balancers:
        for balancer in balancers:
            load_balancer_arn = balancer["LoadBalancerArn"]

            config: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                region=region,
                service="elbv2",
                function="describe_listeners",
                parameters={
                    "LoadBalancerArn": str(load_balancer_arn),
                },
            )
            locations: list[Location] = []
            listeners = config.get("Listeners", [])
            for listener in listeners:
                if listener.get("SslPolicy", ""):
                    describe_ssl_policies: dict[str, Any] = await run_boto3_fun(
                        credentials=credentials,
                        region=region,
                        service="elbv2",
                        function="describe_ssl_policies",
                        parameters={
                            "Names": [listener["SslPolicy"]],
                        },
                    )
                    policy = describe_ssl_policies.get("SslPolicies", [])
                    locations = [
                        Location(
                            access_patterns=("/0/SslProtocols",),
                            arn=(f"{listener['LoadBalancerArn']}"),
                            values=(protocol,),
                            method=method,
                        )
                        for protocol in policy[0]["SslProtocols"]
                        if protocol in vuln_protocols
                    ]

                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=policy,
                    )

    return (vulns, True)


@SHIELD
async def elbv2_uses_insecure_ssl_cipher(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ELBV2_USES_INSECURE_SSL_CIPHER
    vuln_protocols = [
        "ECDHE-ECDSA-AES128-SHA256",
        "ECDHE-RSA-AES128-SHA256",
        "ECDHE-ECDSA-AES128-SHA",
        "ECDHE-RSA-AES128-SHA",
        "ECDHE-ECDSA-AES256-SHA384",
        "ECDHE-RSA-AES256-SHA384",
        "ECDHE-RSA-AES256-SHA",
        "ECDHE-ECDSA-AES256-SHA",
        "AES128-GCM-SHA256",
        "AES128-SHA256",
        "AES128-SHA",
        "AES256-GCM-SHA384",
        "AES256-SHA256",
        "AES256-SHA",
    ]
    balancers = await describe_load_balancers(credentials, region)
    vulns: Vulnerabilities = ()
    if balancers:
        for balancer in balancers:
            load_balancer_arn = balancer["LoadBalancerArn"]

            config: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                region=region,
                service="elbv2",
                function="describe_listeners",
                parameters={
                    "LoadBalancerArn": str(load_balancer_arn),
                },
            )

            locations: list[Location] = []
            for listener in config.get("Listeners", []):
                if listener.get("SslPolicy", ""):
                    describe_ssl_policies: dict[str, Any] = await run_boto3_fun(
                        credentials=credentials,
                        region=region,
                        service="elbv2",
                        function="describe_ssl_policies",
                        parameters={
                            "Names": [listener["SslPolicy"]],
                        },
                    )
                    policy = describe_ssl_policies.get("SslPolicies", [])
                    locations = [
                        Location(
                            access_patterns=("/0/Ciphers",),
                            arn=(f"{listener['LoadBalancerArn']}"),
                            values=(cipher["Name"],),
                            method=method,
                        )
                        for cipher in policy[0]["Ciphers"]
                        if cipher["Name"] in vuln_protocols
                    ]

                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=policy,
                    )

    return (vulns, True)


@SHIELD
async def uses_insecure_security_policy(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.USES_INSECURE_SECURITY_POLICY
    paginated_results_key = "LoadBalancers"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elbv2",
        function="describe_load_balancers",
        paginated_results_key=paginated_results_key,
    )
    balancers = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for balancer in balancers:
        locations: list[Location] = []
        load_balancer_arn = balancer["LoadBalancerArn"]

        listeners: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="elbv2",
            function="describe_listeners",
            parameters={
                "LoadBalancerArn": str(load_balancer_arn),
            },
        )
        for index, listener in enumerate(listeners.get("Listeners", [])):
            ssl_policy = listener.get("SslPolicy", None)
            if (
                ssl_policy
                and ssl_policy in PREDEFINED_SSL_POLICY_VALUES
                and ssl_policy not in SAFE_SSL_POLICY_VALUES
            ):
                locations.append(
                    Location(
                        access_patterns=(f"/Listeners/{index}/SslPolicy",),
                        arn=(f"{listener['ListenerArn']}"),
                        values=(ssl_policy,),
                        method=method,
                    ),
                )

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=listeners,
        )

    return (vulns, True)


@SHIELD
async def api_gateway_insecure_tls_version(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.API_GATEWAY_INSECURE_TLS_VERSION
    paginated_results_key = "items"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="apigateway",
        function="get_domain_names",
        paginated_results_key=paginated_results_key,
    )
    items = response.get(paginated_results_key, [])
    for domain in items:
        tls_version = domain.get("securityPolicy", "")
        if tls_version in ("TLS_1_0", "TLS_1_1"):
            parameters = {
                "Region": region,
                "DomainName": domain["domainName"],
            }
            arn = build_arn(
                service="apigateway",
                resource="DomainName",
                parameters=parameters,
            )
            locations = [
                Location(
                    access_patterns=("/securityPolicy",),
                    values=(tls_version,),
                    arn=arn,
                    method=method,
                    desc_params={"version": tls_version[-3:].replace("_", ".")},
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=domain,
            )
    return (vulns, True)


def get_vulns_for_mesh_virtual_gateway_tls_disabled(
    region: str,
    virtual_gw_data: dict[str, Any],
    method: MethodsEnum,
) -> Vulnerabilities:
    client_policy = virtual_gw_data["spec"]["backendDefaults"]["clientPolicy"]
    arn = build_mesh_virtual_gateway_arn(region=region, virtual_gateway_data=virtual_gw_data)
    policy_path = "/spec/backendDefaults/clientPolicy"
    locations = []
    if not client_policy:
        locations.append(
            Location(
                access_patterns=(policy_path,),
                values=("{}",),
                arn=arn,
                method=method,
            ),
        )
    elif not client_policy["tls"]["enforce"]:
        locations.append(
            Location(
                access_patterns=(f"{policy_path}/tls/enforce",),
                values=(client_policy["tls"]["enforce"],),
                arn=arn,
                method=method,
            ),
        )
    return build_vulnerabilities(
        locations=locations,
        method=method,
        aws_response=virtual_gw_data,
    )


@SHIELD
async def aws_app_mesh_virtual_gateway_tls_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_APP_MESH_VIRTUAL_GATEWAY_TLS_DISABLED
    virtual_gateways_data = await aws_virtual_gateways_for_app_meshes(
        credentials=credentials,
        region=region,
    )
    for virtual_gateway in virtual_gateways_data:
        virtual_gateway_data = virtual_gateway["virtualGateway"]
        vulns += get_vulns_for_mesh_virtual_gateway_tls_disabled(
            region=region,
            virtual_gw_data=virtual_gateway_data,
            method=method,
        )
    return (vulns, True)


@SHIELD
async def aws_rds_instance_tls_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_INSTANCE_TLS_DISABLED
    instances: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="rds",
        function="describe_db_instances",
        paginated_results_key="DBInstances",
    )
    for instance in instances.get("DBInstances", []):
        arn = instance["DBInstanceArn"]
        parameter_groups = instance["DBParameterGroups"]
        for param_group in parameter_groups:
            if param_group.get("ParameterApplyStatus", "in-sync"):
                param_group_name = param_group.get("DBParameterGroupName")
                db_params_descriptions: dict[str, Any] = await run_boto3_fun(
                    credentials=credentials,
                    region=region,
                    service="rds",
                    function="describe_db_parameters",
                    parameters={
                        "DBParameterGroupName": param_group_name,
                        "Filters": [
                            {
                                "Name": "parameter-name",
                                "Values": [
                                    "require_secure_transport",
                                    "rds.force_ssl",
                                ],
                            },
                        ],
                    },
                )
                for parameter in db_params_descriptions.get("Parameters", []):
                    parameter_name = parameter.get("ParameterName")
                    parameter_value = str(parameter.get("ParameterValue"))
                    if parameter_value in {"0", "OFF"}:
                        instance["DBParameterGroups"] = [parameter]
                        locations = [
                            Location(
                                arn=arn,
                                access_patterns=(
                                    "/DBParameterGroups/0/ParameterName",
                                    "/DBParameterGroups/0/ParameterValue",
                                ),
                                values=(parameter_value, parameter_name),
                                method=method,
                                desc_params={
                                    "param_name": parameter_name,
                                    "param_value": parameter_value,
                                },
                            ),
                        ]
                        vulns += build_vulnerabilities(
                            locations=locations,
                            method=method,
                            aws_response=instance,
                        )

    return (vulns, True)


@SHIELD
async def aws_rds_cluster_tls_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_CLUSTER_TLS_DISABLED
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="rds",
        function="describe_db_clusters",
        paginated_results_key="DBClusters",
    )
    for cluster in clusters.get("DBClusters", []):
        arn = cluster["DBClusterArn"]
        engine = cluster["Engine"]
        param_group_name = cluster["DBClusterParameterGroup"]
        cluster_params_descriptions: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="rds",
            function="describe_db_cluster_parameters",
            parameters={
                "DBClusterParameterGroupName": param_group_name,
                "Filters": [
                    {
                        "Name": "parameter-name",
                        "Values": [
                            "require_secure_transport",
                            "rds.force_ssl",
                        ],
                    },
                ],
            },
        )
        for parameter in cluster_params_descriptions.get("Parameters", []):
            parameter_name = parameter.get("ParameterName")
            parameter_value = str(parameter.get("ParameterValue"))
            if parameter_value in {"0", "OFF"}:
                cluster["DBParameterGroups"] = [parameter]
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=(
                            "/DBParameterGroups/0/ParameterName",
                            "/DBParameterGroups/0/ParameterValue",
                        ),
                        values=(parameter_value, parameter_name),
                        method=method,
                        desc_params={
                            "param_name": parameter_name,
                            "param_value": parameter_value,
                            "engine": engine,
                        },
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=cluster,
                )

    return (vulns, True)


@SHIELD
async def aws_opensearch_domain_insecure_tls_version(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_OPENSEARCH_DOMAIN_INSECURE_TLS_VERSION
    environments: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="opensearch",
        function="list_domain_names",
    )
    for domain in environments.get("DomainNames", []):
        domain_name = domain["DomainName"]
        domain_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="opensearch",
            function="describe_domain",
            parameters={
                "DomainName": domain_name,
            },
        )
        domain_status = domain_config.get("DomainStatus", {})
        tls_policy = domain_status.get("DomainEndpointOptions", {}).get("TLSSecurityPolicy")
        if tls_policy[11:18] in ["TLS-1-0", "TLS-1-1"]:
            locations = [
                Location(
                    arn=domain_status["ARN"],
                    access_patterns=("/DomainStatus/DomainEndpointOptions/TLSSecurityPolicy",),
                    values=(tls_policy,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=domain_config,
            )

    return (vulns, True)


@SHIELD
async def aws_msk_client_broker_tls_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_MSK_CLIENT_BROKER_TLS_DISABLED
    paginated_results_key = "ClusterInfoList"
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="kafka",
        function="list_clusters_v2",
        paginated_results_key=paginated_results_key,
    )
    for cluster in clusters.get(paginated_results_key, []):
        if cluster.get("ClusterType", "") == "SERVERLESS":
            continue
        cluster_arn = cluster["ClusterArn"]
        cluster_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="kafka",
            function="describe_cluster",
            parameters={
                "ClusterArn": cluster_arn,
            },
        )
        encryption_in_transit = (
            cluster_config.get("ClusterInfo", {})
            .get("EncryptionInfo", {})
            .get("EncryptionInTransit", {})
        )
        tls_in_client_broker = encryption_in_transit.get("ClientBroker")
        if tls_in_client_broker == "PLAINTEXT":
            locations = [
                Location(
                    arn=cluster_arn,
                    access_patterns=("/ClusterInfo/EncryptionInfo/EncryptionInTransit",),
                    values=(encryption_in_transit,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster_config,
            )

    return (vulns, True)


@SHIELD
async def aws_msk_broker_broker_tls_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_MSK_BROKER_BROKER_TLS_DISABLED
    paginated_results_key = "ClusterInfoList"
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="kafka",
        function="list_clusters_v2",
        paginated_results_key=paginated_results_key,
    )
    for cluster in clusters.get(paginated_results_key, []):
        if cluster.get("ClusterType", "") == "SERVERLESS":
            continue
        cluster_arn = cluster["ClusterArn"]
        cluster_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="kafka",
            function="describe_cluster",
            parameters={
                "ClusterArn": cluster_arn,
            },
        )
        encryption_in_transit = (
            cluster_config.get("ClusterInfo", {})
            .get("EncryptionInfo", {})
            .get("EncryptionInTransit", {})
        )
        tls_in_client_broker = encryption_in_transit.get("ClientBroker")
        tls_in_cluster_brokers = encryption_in_transit.get("InCluster")
        if tls_in_client_broker == "PLAINTEXT" or not tls_in_cluster_brokers:
            access_patterns: tuple[str, ...] = ()
            values: tuple[str, ...] = ()
            if tls_in_client_broker == "PLAINTEXT":
                access_patterns += ("/ClusterInfo/EncryptionInfo/EncryptionInTransit/ClientBroker",)
                values += (tls_in_client_broker,)
            if not tls_in_cluster_brokers:
                access_patterns += ("/ClusterInfo/EncryptionInfo/EncryptionInTransit/InCluster",)
                values += (tls_in_cluster_brokers,)
            locations = [
                Location(
                    arn=cluster_arn,
                    access_patterns=access_patterns,
                    values=values,
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster_config,
            )

    return (vulns, True)


@SHIELD
async def aws_document_db_cluster_tls_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_DOCUMENT_DB_CLUSTER_TLS_DISABLED
    paginated_results_key = "DBClusters"
    db_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="docdb",
        function="describe_db_clusters",
        paginated_results_key=paginated_results_key,
    )
    for db_cluster in db_clusters.get(paginated_results_key, []):
        arn = db_cluster["DBClusterArn"]
        parameter_group = db_cluster.get("DBClusterParameterGroup", "")
        param_groups: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="docdb",
            function="describe_db_cluster_parameters",
            parameters={
                "DBClusterParameterGroupName": parameter_group,
                "Filters": [
                    {
                        "Name": "parameter-name",
                        "Values": [
                            "tls",
                        ],
                    },
                ],
            },
        )

        if (
            (tls_param := param_groups.get("Parameters", []))
            and (tls_param_value := tls_param[0].get("ParameterValue"))
            and tls_param_value == "disabled"
        ):
            db_cluster["ClusterParameters"] = tls_param
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/ClusterParameters/0/ParameterValue",),
                    values=(tls_param_value,),
                    method=method,
                    desc_params={
                        "param_name": "tls",
                        "param_value": tls_param_value,
                        "param_group": parameter_group,
                    },
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_cluster,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    api_gateway_insecure_tls_version,
    serves_content_over_insecure_protocols,
    elbv2_uses_insecure_ssl_protocol,
    elbv2_uses_insecure_ssl_cipher,
    uses_insecure_security_policy,
    aws_app_mesh_virtual_gateway_tls_disabled,
    aws_rds_instance_tls_disabled,
    aws_rds_cluster_tls_disabled,
    aws_opensearch_domain_insecure_tls_version,
    aws_msk_client_broker_tls_disabled,
    aws_msk_broker_broker_tls_disabled,
    aws_document_db_cluster_tls_disabled,
)
