import csv
from contextlib import (
    suppress,
)
from datetime import (
    UTC,
    datetime,
    timedelta,
)
from io import (
    StringIO,
)
from typing import (
    Any,
)

from dateutil import (
    parser,
)
from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def has_old_ssh_public_keys(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAS_OLD_SSH_PUBLIC_KEYS
    paginated_results_key = "Users"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_users",
        paginated_results_key=paginated_results_key,
    )
    vulns: Vulnerabilities = ()
    three_months_ago = datetime.now() - timedelta(days=90)  # noqa: DTZ005
    three_months_ago = three_months_ago.replace(tzinfo=UTC)
    users = response.get(paginated_results_key, [])

    if users:
        for user in users:
            paginated_results_key = "SSHPublicKeys"
            access_keys: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="list_ssh_public_keys",
                parameters={
                    "UserName": user["UserName"],
                },
                paginated_results_key=paginated_results_key,
            )
            keys = access_keys[paginated_results_key] if access_keys else {}
            locations: list[Location] = []
            for index, key in enumerate(keys):
                if key["UploadDate"] < three_months_ago:
                    arn = user["Arn"]
                    locations.append(
                        Location(
                            access_patterns=(f"/{index}/UploadDate",),
                            arn=(arn),
                            values=(key["UploadDate"],),
                            method=method,
                        ),
                    )
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=keys,
            )

    return (vulns, True)


@SHIELD
async def have_old_creds_enabled(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAVE_OLD_CREDS_ENABLED
    await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="generate_credential_report",
    )
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_credential_report",
    )

    three_months_ago = datetime.now() - timedelta(days=90)  # noqa: DTZ005
    three_months_ago = three_months_ago.replace(tzinfo=UTC)
    vulns: Vulnerabilities = ()
    users_csv = StringIO(response.get("Content", b"").decode())
    credentials_report = tuple(csv.DictReader(users_csv, delimiter=","))
    for user in credentials_report:
        if user["password_enabled"] != "true":  # noqa: S105
            continue

        get_user: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="get_user",
            parameters={"UserName": user["user"]},
        )
        with suppress(KeyError):
            user_pass_last_used = get_user["User"]["PasswordLastUsed"]
            if user_pass_last_used < three_months_ago:
                locations = [
                    Location(
                        access_patterns=("/User/PasswordLastUsed",),
                        arn=user["arn"],
                        values=(user_pass_last_used,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=get_user,
                )

    return (vulns, True)


@SHIELD
async def have_old_access_keys(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAVE_OLD_ACCESS_KEYS
    await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="generate_credential_report",
    )
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="get_credential_report",
    )

    three_months_ago = datetime.now() - timedelta(days=90)  # noqa: DTZ005
    three_months_ago = three_months_ago.replace(tzinfo=UTC)
    vulns: Vulnerabilities = ()
    users_csv = StringIO(response.get("Content", b"").decode())
    credentials_report = tuple(csv.DictReader(users_csv, delimiter=","))

    for user in credentials_report:
        locations: list[Location] = []
        if any(
            (
                user["access_key_1_active"] != "true",
                user["access_key_2_active"] != "true",
            ),
        ):
            continue

        key_names = ("access_key_1_last_rotated", "access_key_2_last_rotated")
        with suppress(KeyError):
            for name in key_names:
                if parser.parse(user[name]).replace(tzinfo=UTC) < three_months_ago:
                    user_arn = user["arn"]
                    locations.append(
                        Location(
                            access_patterns=(f"/{name}",),
                            arn=(f"{user_arn}"),
                            values=(user[name],),
                            method=method,
                        ),
                    )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=user,  # type: ignore[arg-type]
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    has_old_ssh_public_keys,
    have_old_creds_enabled,
    have_old_access_keys,
)
