import ast
from contextlib import (
    suppress,
)
from datetime import (
    UTC,
    datetime,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    arn_no_fluid,
    build_arn,
    build_vulnerabilities,
    get_owner_id,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)

UTC_NOW: datetime = datetime.now(UTC)


async def get_paginated_items(
    credentials: AwsCredentials,
    region: str,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "region": region,
        "credentials": credentials,
        "service": "ec2",
        "function": "describe_launch_templates",
        "parameters": {"MaxResults": 50},
    }
    data = await run_boto3_fun(**args)
    object_name = "LaunchTemplates"
    pools += data.get(object_name, [])

    next_token = data.get("NextToken", None)
    while next_token:
        args["parameters"]["NextToken"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("NextToken", None)

    return pools


async def iterate_shutdown_behavior(
    *,
    instance_id: str,
    owner_id: str,
    vulns: Vulnerabilities,
    credentials: AwsCredentials,
    region: str,
) -> Vulnerabilities:
    locations: list[Location] = []
    method = MethodsEnum.EC2_HAS_TERMINATE_SHUTDOWN_BEHAVIOR
    paginated_results_key = "AutoScalingInstances"
    autoscaling: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="autoscaling",
        function="describe_auto_scaling_instances",
        parameters={
            "InstanceIds": [instance_id],
        },
        paginated_results_key=paginated_results_key,
    )
    is_autoscale = autoscaling.get(paginated_results_key, [])
    if not is_autoscale:
        shutdown_behavior: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="ec2",
            function="describe_instance_attribute",
            parameters={
                "Attribute": "instanceInitiatedShutdownBehavior",
                "InstanceId": instance_id,
            },
        )
        value = shutdown_behavior.get("InstanceInitiatedShutdownBehavior", {}).get("Value")
        if value == "terminate":
            parameters = {
                "Region": region,
                "Account": owner_id,
                "InstanceId": instance_id,
            }
            arn = build_arn(service="ec2", resource="instance", parameters=parameters)
            locations = [
                Location(
                    access_patterns=(("/InstanceInitiatedShutdownBehavior/Value"),),
                    arn=arn,
                    values=(shutdown_behavior["InstanceInitiatedShutdownBehavior"]["Value"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=shutdown_behavior,
            )

    return vulns


@SHIELD
async def ec2_has_terminate_shutdown_behavior(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        paginated_results_key=paginated_results_key,
    )
    reservations = response.get(paginated_results_key, []) if response else []

    vulns: Vulnerabilities = ()
    for reservation in reservations:
        owner_id = reservation["OwnerId"]
        for instance in reservation.get("Instances", []):
            instance_id = instance["InstanceId"]
            is_spot = instance.get("InstanceLifecycle") == "spot"
            if not is_spot:
                vulns = await iterate_shutdown_behavior(
                    instance_id=instance_id,
                    owner_id=owner_id,
                    vulns=vulns,
                    credentials=credentials,
                    region=region,
                )

    return (vulns, True)


async def process_template(
    *,
    method: MethodsEnum,
    template: dict[str, Any],
    owner_id: str,
    template_id: dict[str, Any],
    region: str,
) -> Vulnerabilities:
    template_network = template.get("LaunchTemplateData", {}).get("NetworkInterfaces", [])
    locations: list[Location] = []
    for index, interface in enumerate(template_network):
        template_public_ip = interface.get("AssociatePublicIpAddress", "none")
        if template_public_ip is True:
            parameters = {
                "Region": region,
                "Account": owner_id,
                "LaunchTemplateId": template_id["LaunchTemplateId"],
            }

            arn = build_arn(
                service="ec2",
                resource="launch-template",
                parameters=parameters,
            )
            locations.append(
                Location(
                    access_patterns=(
                        f"/LaunchTemplateData/NetworkInterfaces/{index}/AssociatePublicIpAddress",
                    ),
                    arn=arn,
                    values=(template_public_ip,),
                    method=method,
                ),
            )

    return build_vulnerabilities(
        locations=locations,
        method=method,
        aws_response=template,
    )


@SHIELD
async def ec2_has_associate_public_ip_address(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.EC2_HAS_ASSOCIATE_PUBLIC_IP_ADDRESS
    vulns: Vulnerabilities = ()
    owner_id: str = await get_owner_id(credentials)
    describe_templates_ids = await get_paginated_items(credentials, region)
    for template_id in describe_templates_ids:
        describe_launch_template_versions: dict = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="ec2",
            function="describe_launch_template_versions",
            parameters={
                "LaunchTemplateId": template_id["LaunchTemplateId"],
            },
        )
        template_versions = describe_launch_template_versions.get("LaunchTemplateVersions", [])

        for template in template_versions:
            vulns += await process_template(
                method=method,
                template=template,
                owner_id=owner_id,
                template_id=template_id,
                region=region,
            )

    return (vulns, True)


@SHIELD
async def ec2_iam_instances_without_profile(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.EC2_IAM_INSTANCES_WITHOUT_PROFILE
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    instances = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for instance in instances:
        locations: list[Location] = []
        for config in instance["Instances"]:
            if "IamInstanceProfile" not in config and config["State"]["Name"] not in {
                "terminated",
                "shutting-down",
            }:
                parameters = {
                    "Region": region,
                    "Account": instance["OwnerId"],
                    "InstanceId": config["InstanceId"],
                }
                arn = build_arn(service="ec2", resource="instance", parameters=parameters)
                locations.append(
                    Location(
                        arn=arn,
                        method=method,
                        values=(),
                        access_patterns=(),
                    ),
                )
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=instance,
        )

    return (vulns, True)


async def iterate_template_has_key_name(
    credentials: AwsCredentials,
    region: str,
) -> set[str]:
    key_names = set()
    describe_templates_ids = await get_paginated_items(credentials, region)
    for template_id in describe_templates_ids:
        describe_launch_template_versions: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="ec2",
            function="describe_launch_template_versions",
            parameters={
                "LaunchTemplateId": template_id["LaunchTemplateId"],
            },
        )
        template_versions = describe_launch_template_versions.get("LaunchTemplateVersions", [])

        for template in template_versions:
            template_key_name = template.get("LaunchTemplateData", {}).get("KeyName", None)
            if template_key_name is not None:
                key_names.add(template_key_name)

    return key_names


@SHIELD
async def has_unused_ec2_key_pairs(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAS_UNUSED_EC2_KEY_PAIRS
    paginated_results_key = "KeyPairs"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="ec2",
        function="describe_key_pairs",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    key_used_by_templates = await iterate_template_has_key_name(credentials, region)
    vulns: Vulnerabilities = ()

    for key in response.get(paginated_results_key, []) if response else []:
        locations: list[Location] = []
        filters = [
            {
                "Name": "instance-state-name",
                "Values": ["running", "stopping", "stopped"],
            },
            {"Name": "key-name", "Values": [key["KeyName"]]},
        ]

        if key["KeyName"] not in key_used_by_templates:
            instances: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="ec2",
                function="describe_instances",
                parameters={"Filters": filters},
            )
            reservations = instances.get("Reservations")
            if not reservations:
                owner_id = await get_owner_id(credentials)
                parameters = {
                    "Region": region,
                    "Account": owner_id,
                    "KeyPairName": key["KeyPairId"],
                }
                arn = build_arn(service="ec2", resource="key-pair", parameters=parameters)

                locations = [
                    Location(
                        arn=(arn),
                        method=method,
                        values=(),
                        access_patterns=(),
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=instances,
                )
    return (vulns, True)


@SHIELD
async def has_unencrypted_amis(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAS_UNENCRYPTED_AMIS
    paginated_results_key = "Images"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_images",
        parameters={"Owners": ["self"]},
        paginated_results_key=paginated_results_key,
    )
    images = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()

    if images:
        for image in images:
            locations: list[Location] = []
            for index, block in enumerate(image["BlockDeviceMappings"]):
                with suppress(KeyError):
                    if not block["Ebs"].get("Encrypted", True):
                        parameters = {
                            "Region": region,
                            "ImageId": image["ImageId"],
                        }
                        arn = build_arn(
                            service="ec2",
                            resource="image",
                            parameters=parameters,
                        )
                        locations = [
                            *locations,
                            *[
                                Location(
                                    access_patterns=(
                                        (f"/BlockDeviceMappings/{index}/Ebs/Encrypted"),
                                    ),
                                    arn=(arn),
                                    values=(block["Ebs"]["Encrypted"],),
                                    method=method,
                                ),
                            ],
                        ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=image,
            )
    return (vulns, True)


@SHIELD
async def has_publicly_shared_amis(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.HAS_PUBLICLY_SHARED_AMIS
    paginated_results_key = "Images"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_images",
        parameters={"Owners": ["self"]},
        paginated_results_key=paginated_results_key,
    )
    images = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    locations: list[Location] = []
    for image in images:
        if image.get("Public", False):
            parameters = {
                "Region": region,
                "ImageId": image["ImageId"],
            }
            arn = build_arn(
                service="ec2",
                resource="image",
                parameters=parameters,
            )
            locations = [
                Location(
                    arn=(arn),
                    method=method,
                    values=(image.get("Public"),),
                    access_patterns=("/Public",),
                ),
            ]
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=image,
        )
    return (vulns, True)


@SHIELD
async def has_unencrypted_snapshots(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="sts",
        function="get_caller_identity",
    )

    if not response.get("Account", False):
        return (vulns, True)
    paginated_results_key = "Snapshots"
    describe_snapshots: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_snapshots",
        parameters={"OwnerIds": [response["Account"]]},
        paginated_results_key=paginated_results_key,
    )
    snapshots = describe_snapshots.get(paginated_results_key, [])
    method = MethodsEnum.HAS_UNENCRYPTED_SNAPSHOTS
    for snapshot in snapshots:
        snapshot_id = snapshot["SnapshotId"]
        if not snapshot.get("Encrypted", True):
            parameters = {
                "Region": region,
                "SnapshotId": snapshot_id,
            }
            arn = build_arn(
                service="ec2",
                resource="snapshot",
                parameters=parameters,
            )
            locations = [
                Location(
                    arn=(arn),
                    method=method,
                    values=(snapshot["Encrypted"],),
                    access_patterns=("/Encrypted",),
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=snapshot,
            )
    return (vulns, True)


@SHIELD
async def has_instances_using_unapproved_amis(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        paginated_results_key=paginated_results_key,
    )
    reservations = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.HAS_INSTANCES_USING_UNAPPROVED_AMIS
    vulns: Vulnerabilities = ()
    if reservations:
        for instances in reservations:
            locations: list[Location] = []
            for instance in instances["Instances"]:
                paginated_results_key = "Images"
                describe_images: dict[str, Any] = await run_boto3_fun(
                    region=region,
                    credentials=credentials,
                    service="ec2",
                    function="describe_images",
                    parameters={
                        "ImageIds": [instance["ImageId"]],
                    },
                    paginated_results_key=paginated_results_key,
                )
                images: list[dict] = describe_images.get(paginated_results_key, [])
                if (
                    images
                    and "ImageOwnerAlias" in images[0]
                    and images[0]["ImageOwnerAlias"] != "amazon"
                ):
                    parameters = {
                        "Region": region,
                        "Account": instances["OwnerId"],
                        "InstanceId": instance["InstanceId"],
                    }
                    arn = build_arn(
                        service="ec2",
                        resource="instance",
                        parameters=parameters,
                    )
                    locations = [
                        Location(
                            access_patterns=("/Images/0/ImageOwnerAlias",),
                            arn=arn,
                            values=(images[0]["ImageOwnerAlias"],),
                            method=method,
                        ),
                    ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=describe_images,
                )

    return (vulns, True)


def _get_action(item: dict, value: str) -> list | str:
    if isinstance(item[value], str):
        return [item[value]]
    return item[value]


def _match_ec2_instance_attribute(action: str) -> bool:
    vuln_roles = {"ec2:ModifyInstanceAttribute", "ec2:*"}
    return any(act == action for act in vuln_roles)


async def iterate_policies(
    credentials: AwsCredentials,
    attached_policies: list,
    arn: str,
) -> Vulnerabilities:
    locations: list[Location] = []
    vulns: Vulnerabilities = ()
    for policy in attached_policies:
        get_policy: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="get_policy",
            parameters={
                "PolicyArn": policy["PolicyArn"],
            },
        )
        default_version = get_policy["Policy"]["DefaultVersionId"]
        pol_ver: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="get_policy_version",
            parameters={
                "PolicyArn": policy["PolicyArn"],
                "VersionId": default_version,
            },
        )
        policy_names = pol_ver.get("PolicyVersion", {})
        pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
        policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))
        if not isinstance(policy_statements, list):
            policy_statements = [policy_statements]

        for index, stmt in enumerate(policy_statements):
            item = ast.literal_eval(str(stmt))
            with suppress(KeyError):
                action = _get_action(item, "Action")
                if (
                    item["Effect"] == "Allow"
                    and any(map(_match_ec2_instance_attribute, action))
                    and item["Resource"] == "*"
                ):
                    locations = [
                        Location(
                            access_patterns=(
                                f"/Statement/{index}/Effect",
                                f"/Statement/{index}/Resource",
                                f"/Statement/{index}/Action",
                            ),
                            arn=(arn),
                            values=(
                                stmt["Effect"],
                                stmt["Resource"],
                                stmt["Action"],
                            ),
                            method=(MethodsEnum.HAS_MODIFY_INSTANCE_ATTRIBUTE),
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=MethodsEnum.HAS_MODIFY_INSTANCE_ATTRIBUTE,
                        aws_response=pol_access,
                    )
    return vulns


@SHIELD
async def has_modify_instance_attribute(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        paginated_results_key=paginated_results_key,
    )
    reservations = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for instances in reservations:
        for instance in instances["Instances"]:
            if "IamInstanceProfile" in instance:
                iam_profile_arn = instance["IamInstanceProfile"]["Arn"]
                iam_profile_name = iam_profile_arn.split("/")[-1]

                get_instance_profile: dict[str, Any] = await run_boto3_fun(
                    credentials=credentials,
                    service="iam",
                    function="get_instance_profile",
                    parameters={
                        "InstanceProfileName": iam_profile_name,
                    },
                )
                get_roles = get_instance_profile.get("InstanceProfile", {}).get("Roles", [])
                for role in get_roles:
                    role_name = role["RoleName"]

                    list_attached_role_policies: dict[str, Any] = await run_boto3_fun(
                        credentials=credentials,
                        service="iam",
                        function="list_attached_role_policies",
                        parameters={
                            "RoleName": role_name,
                        },
                    )
                    attached_policies = list_attached_role_policies.get("AttachedPolicies", [])
                    vulns += await iterate_policies(
                        credentials,
                        attached_policies,
                        iam_profile_arn,
                    )
    return (vulns, True)


@SHIELD
async def aws_ec2_instance_has_multiple_network_interfaces(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.EC2_INSTANCE_HAS_MULTIPLE_NETWORK_INTERFACES
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        paginated_results_key=paginated_results_key,
    )
    reservations = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for reservation in reservations:
        owner_id = reservation["OwnerId"]
        for instance in reservation["Instances"]:
            state = instance["State"]
            tags = instance.get("Tags", [])
            instance_id = instance["InstanceId"]
            network_interfaces = instance["NetworkInterfaces"]
            network_interfaces_only_ids = [
                {"NetworkInterfaceId": net_in.get("NetworkInterfaceId")}
                for net_in in network_interfaces
            ]
            if (
                state["Name"] not in {"terminated", "shutting-down"}
                and len(network_interfaces) > 1
                and "f333" not in arn_no_fluid(tags)
            ):
                parameters = {
                    "Region": region,
                    "Account": owner_id,
                    "InstanceId": instance_id,
                }
                arn = build_arn(service="ec2", resource="instance", parameters=parameters)
                locations = [
                    Location(
                        access_patterns=("/NetworkInterfaces",),
                        values=(network_interfaces_only_ids,),
                        arn=arn,
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=instance,
                )
    return (vulns, True)


@SHIELD
async def aws_ec2_instance_using_imds_v1(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AWS_EC2_INSTANCE_USING_IMDS_V1
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        parameters={
            "Filters": [
                {
                    "Name": "instance-state-name",
                    "Values": [
                        "pending",
                        "running",
                    ],
                },
            ],
        },
        paginated_results_key=paginated_results_key,
    )
    reservations = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for reservation in reservations:
        owner_id = reservation["OwnerId"]
        for instance in reservation["Instances"]:
            instance_id = instance["InstanceId"]
            metadata_options = instance["MetadataOptions"]
            http_tokens = metadata_options["HttpTokens"]
            if http_tokens == "optional":
                parameters = {
                    "Region": region,
                    "Account": owner_id,
                    "InstanceId": instance_id,
                }
                arn = build_arn(service="ec2", resource="instance", parameters=parameters)
                locations = [
                    Location(
                        access_patterns=("/MetadataOptions/HttpTokens",),
                        values=(http_tokens,),
                        arn=arn,
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=instance,
                )
    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    aws_ec2_instance_has_multiple_network_interfaces,
    aws_ec2_instance_using_imds_v1,
    has_modify_instance_attribute,
    ec2_has_terminate_shutdown_behavior,
    ec2_has_associate_public_ip_address,
    ec2_iam_instances_without_profile,
    has_unused_ec2_key_pairs,
    has_publicly_shared_amis,
    has_unencrypted_snapshots,
    has_unencrypted_amis,
    has_instances_using_unapproved_amis,
)
