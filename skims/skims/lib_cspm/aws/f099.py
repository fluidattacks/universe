import ast
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def bucket_policy_has_server_side_encryption_disable(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.BUCKET_POLICY_HAS_SERVER_SIDE_ENCRYPTION_DISABLE
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    buckets = response.get("Buckets", []) if response else []
    vulns: Vulnerabilities = ()
    for bucket in buckets:
        bucket_policy_string: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="s3",
            function="get_bucket_policy",
            parameters={"Bucket": str(bucket["Name"])},
        )
        if bucket_policy_string:
            policy = ast.literal_eval(str(bucket_policy_string["Policy"]))
            bucket_statements = policy["Statement"]
            locations: list[Location] = []
            parameters = {
                "BucketName": bucket["Name"],
            }
            for index, stm in enumerate(bucket_statements):
                if (
                    (conditions := stm.get("Condition", None))
                    and conditions.get("Null", None)
                    and conditions["Null"].get("s3:x-amz-server-side-encryption", None)
                    and conditions["Null"]["s3:x-amz-server-side-encryption"] != "true"
                ):
                    condition = stm["Condition"]["Null"]["s3:x-amz-server-side-encryption"]
                    locations.append(
                        Location(
                            access_patterns=(
                                (
                                    f"/Statement/{index}/Condition/Null"
                                    "/s3:x-amz-server-side-encryption"
                                ),
                            ),
                            arn=build_arn(
                                service="s3",
                                resource="bucket",
                                parameters=parameters,
                            ),
                            values=(f"{condition}",),
                            method=method,
                        ),
                    )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=policy,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (bucket_policy_has_server_side_encryption_disable,)
