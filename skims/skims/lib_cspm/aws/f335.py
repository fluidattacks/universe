from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def s3_bucket_versioning_disabled(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    method = MethodsEnum.S3_BUCKET_VERSIONING_DISABLED
    buckets = response.get("Buckets", []) if response else []
    vulns: Vulnerabilities = ()
    if buckets:
        for bucket in buckets:
            locations: list[Location] = []
            bucket_name = bucket["Name"]
            bucket_versioning: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="s3",
                function="get_bucket_versioning",
                parameters={"Bucket": str(bucket_name)},
            )
            status = bucket_versioning.get("Status", "")
            parameters = {
                "BucketName": bucket["Name"],
            }
            arn = build_arn(service="s3", resource="bucket", parameters=parameters)
            if not status:
                locations = [
                    Location(
                        arn=(arn),
                        method=method,
                        values=(),
                        access_patterns=(),
                    ),
                ]
            elif status != "Enabled":
                locations = [
                    Location(
                        arn=(arn),
                        method=method,
                        values=(status,),
                        access_patterns=("/Status",),
                    ),
                ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=bucket_versioning,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (s3_bucket_versioning_disabled,)
