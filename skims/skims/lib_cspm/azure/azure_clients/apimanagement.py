from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.apimanagement.aio import (
    ApiManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
    get_resource_group_from_resource_id,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_api_management_services(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with ApiManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_apimgmt:
        api_mgmt_services = []
        async for mgmt_service in client_apimgmt.api_management_service.list():
            api_mgmt_services.append(dict(mgmt_service.as_dict()))  # noqa: PERF401
        return api_mgmt_services


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_apis(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    api_mgmt_services = await run_api_management_services(credentials, client_credential)
    async with ApiManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_apimgmt:
        apis = []
        for service in api_mgmt_services:
            service_id = service.get("id", "")
            resource_group = get_resource_group_from_resource_id(service_id)
            service_name = service.get("name", "")
            async for api in client_apimgmt.api.list_by_service(
                resource_group_name=resource_group,
                service_name=service_name,
            ):
                apis.append(dict(api.as_dict()))  # noqa: PERF401
        return apis


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_api_mgmt_services_portal_configuration(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    api_mgmt_services = await run_api_management_services(credentials, client_credential)
    async with ApiManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_apimgmt:
        api_mgmt_services_portal_configuration = []
        for mgmt_service in api_mgmt_services:
            service_id = mgmt_service.get("id", "")
            service_name = mgmt_service.get("name", "")
            resource_group = get_resource_group_from_resource_id(service_id)
            service_portal_config = await client_apimgmt.portal_config.list_by_service(
                resource_group_name=resource_group,
                service_name=service_name,
            )
            api_mgmt_services_portal_configuration.append(dict(service_portal_config.as_dict()))
        return api_mgmt_services_portal_configuration
