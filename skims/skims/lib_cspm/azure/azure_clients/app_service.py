from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.web.v2023_01_01.aio._web_site_management_client import (
    WebSiteManagementClient,
)
from lib_cspm.azure.azure_clients.authorization import (
    run_roles_by_principal,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_web_apps(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    filter_type: str | None = None,
) -> list[dict[str, Any]]:
    async with WebSiteManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_web_site:
        all_web_apps = []
        async for web_app in client_web_site.web_apps.list():
            app_kind = str(web_app.kind)
            app_type = app_kind.split(",")
            if filter_type and filter_type != app_type[0]:
                continue
            all_web_apps.append(dict(web_app.as_dict()))
    return all_web_apps


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_web_apps_configuration(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    filter_type: str | None = None,
) -> list[dict[str, Any]]:
    web_apps = await run_azure_web_apps(
        credentials,
        client_credential,
        filter_type=filter_type,
    )
    async with WebSiteManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_web_site:
        all_web_apps_configurations = []
        for web_app in web_apps:
            resource_group = web_app.get("resource_group")
            name = web_app.get("name")
            web_app_configuration = await client_web_site.web_apps.get_configuration(
                resource_group,
                name,
            )
            all_web_apps_configurations.append(dict(web_app_configuration.as_dict()))
    return all_web_apps_configurations


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_web_apps_auth_configuration(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    web_apps = await run_azure_web_apps(
        credentials,
        client_credential,
    )
    async with WebSiteManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_web_site:
        web_apps_auth_settings = []
        for web_app in web_apps:
            resource_group = web_app.get("resource_group")
            name = web_app.get("name")
            auth_settings = await client_web_site.web_apps.get_auth_settings_v2(
                resource_group,
                name,
            )
            web_apps_auth_settings.append(dict(auth_settings.as_dict()))
    return web_apps_auth_settings


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_function_app_host_keys(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    web_apps = await run_azure_web_apps(
        credentials,
        client_credential,
    )
    function_apps = [app for app in web_apps if "functionapp" in app["kind"]]
    async with WebSiteManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_web_site:
        funct_app_host_keys = []
        for funct_app in function_apps:
            resource_group = funct_app.get("resource_group")
            name = funct_app.get("name")
            host_keys = await client_web_site.web_apps.list_host_keys(resource_group, name)
            host_keys_dict = host_keys.as_dict()
            host_keys_dict["function_id"] = funct_app.get("id")
            funct_app_host_keys.append(dict(host_keys_dict))
    return funct_app_host_keys


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_function_app_with_roles(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    function_apps = await run_azure_web_apps(
        credentials,
        client_credential,
        filter_type="functionapp",
    )
    roles_by_principal = await run_roles_by_principal(credentials, client_credential)
    for func_app in function_apps:
        identity = func_app.get("identity", {})
        principal_id = identity.get("principal_id", "")
        func_app["roles"] = roles_by_principal.get(principal_id, [])
    return function_apps
