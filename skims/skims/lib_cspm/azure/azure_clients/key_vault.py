from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.keyvault.v2022_07_01.aio._key_vault_management_client import (
    KeyVaultManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
    get_resource_group_from_resource_id,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def list_key_vaults(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with KeyVaultManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as key_vault_client:
        vaults = []
        async for vault in key_vault_client.vaults.list():
            vaults.append(dict(vault.as_dict()))  # noqa: PERF401
        return vaults


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_key_vault(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with KeyVaultManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as key_vault_client:
        vaults = []
        list_vaults = await list_key_vaults(credentials, client_credential)
        for vault in list_vaults:
            vault_id = vault.get("id", "")
            resource_group = get_resource_group_from_resource_id(vault_id)
            vault_data = await key_vault_client.vaults.get(
                resource_group_name=resource_group,
                vault_name=vault.get("name", ""),
            )
            vault_dict = vault_data.as_dict()
            vaults.append(dict(vault_dict))
        return vaults


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_key_vault_keys(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with KeyVaultManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as key_vault_client:
        keys = []
        list_vaults = await list_key_vaults(credentials, client_credential)
        for vault in list_vaults:
            vault_id = vault.get("id", "")
            resource_group = get_resource_group_from_resource_id(vault_id)
            async for key in key_vault_client.keys.list(
                resource_group_name=resource_group,
                vault_name=vault.get("name", ""),
            ):
                keys.append(dict(key.as_dict()))  # noqa: PERF401
        return keys


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_key_vault_secrets(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with KeyVaultManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as key_vault_client:
        secrets = []
        list_vaults = await list_key_vaults(credentials, client_credential)
        for vault in list_vaults:
            vault_id = vault.get("id", "")
            resource_group = get_resource_group_from_resource_id(vault_id)
            async for secret in key_vault_client.secrets.list(
                resource_group_name=resource_group,
                vault_name=vault.get("name", ""),
            ):
                secrets.append(dict(secret.as_dict()))  # noqa: PERF401
        return secrets
