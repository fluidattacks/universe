from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.rdbms.mysql_flexibleservers.aio import (
    MySQLManagementClient as FlexibleMySQL,
)
from azure.mgmt.rdbms.postgresql.aio._postgre_sql_management_client import (
    PostgreSQLManagementClient as PostgreSQLManagementClientSingleServers,
)
from azure.mgmt.rdbms.postgresql_flexibleservers.aio import (
    PostgreSQLManagementClient as PostgreSQLManagementClientFlexibleServers,
)
from lib_cspm.azure.azure_clients.azure_resource import (
    list_resource_groups_names,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
    get_resource_group_from_resource_id,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_mysql_flexible_servers_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with FlexibleMySQL(
        client_credential,
        credentials.subscription_id,
    ) as mysql_client:
        flexible_servers = []
        async for server in mysql_client.servers.list():
            flexible_servers.append(server.as_dict())  # noqa: PERF401

    return flexible_servers


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_mysql_configs_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    config_name: str,
) -> list[dict[str, Any]]:
    servers = await run_azure_db_mysql_flexible_servers_client(credentials, client_credential)
    async with FlexibleMySQL(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as mysql_client:
        mysql_servers_configs = []

        for server in servers:
            resource_group = server["id"].split("/")[4]
            config = await mysql_client.configurations.get(
                resource_group_name=resource_group,
                server_name=server["name"],
                configuration_name=config_name,
            )
            mysql_servers_configs.append(config.as_dict())
    return mysql_servers_configs


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_mysql_flex_server_firewall_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    flexible_servers = await run_azure_db_mysql_flexible_servers_client(
        credentials,
        client_credential,
    )
    async with FlexibleMySQL(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as mysql_client:
        mysql_flex_server_firewall = []
        for server in flexible_servers:
            server_id = server.get("id", "")
            server_name = server.get("name", "")
            network_config = server.get("network", {})
            resource_group = get_resource_group_from_resource_id(resource_id=server_id)
            rules = []
            async for firewall in mysql_client.firewall_rules.list_by_server(
                resource_group_name=resource_group,
                server_name=server_name,
            ):
                firewall_dict = firewall.as_dict()
                firewall_dict["network"] = network_config
                rules.append(firewall_dict)
                mysql_flex_server_firewall += rules
    return mysql_flex_server_firewall


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_postgresql_server_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    resource_groups = await list_resource_groups_names(credentials, client_credential)
    async with PostgreSQLManagementClientSingleServers(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as postgresql_client:
        postgresql_servers = []
        for resource_group in resource_groups:
            async for server in postgresql_client.servers.list_by_resource_group(
                resource_group_name=resource_group,
            ):
                postgresql_servers.append(server.as_dict())  # noqa: PERF401
    return postgresql_servers


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_postgresql_firewall_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    servers = await run_azure_db_postgresql_server_client(credentials, client_credential)
    async with PostgreSQLManagementClientSingleServers(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as psql_client:
        postgresql_server_firewall = []
        for server in servers:
            server_id = server.get("id", "")
            server_name = server.get("name", "")
            public_network_access = server.get("public_network_access", {})
            resource_group = get_resource_group_from_resource_id(server_id)
            rules = []
            async for firewall in psql_client.firewall_rules.list_by_server(
                resource_group_name=resource_group,
                server_name=server_name,
            ):
                firewall_dict = firewall.as_dict()
                firewall_dict["public_network_access"] = public_network_access
                rules.append(firewall_dict)
            if not rules:
                postgresql_server_firewall.append(
                    {
                        "id": server_id,
                        "network": public_network_access,
                        "start_ip_address": None,
                        "end_ip_address": None,
                    },
                )
            else:
                postgresql_server_firewall += rules

    return postgresql_server_firewall


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_postgresql_configs_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    config_name: str | None = None,
) -> list[dict[str, Any]]:
    servers = await run_azure_db_postgresql_server_client(credentials, client_credential)
    async with PostgreSQLManagementClientSingleServers(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as postgresql_client:
        postgresql_servers_configs = []

        for server in servers:
            resource_group = server["id"].split("/")[4]
            config = await postgresql_client.configurations.get(
                resource_group_name=resource_group,
                server_name=server["name"],
                configuration_name=config_name,
            )
            postgresql_servers_configs.append(config.as_dict())
    return postgresql_servers_configs


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_psql_flexible_server_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    resource_groups = await list_resource_groups_names(credentials, client_credential)
    async with PostgreSQLManagementClientFlexibleServers(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as postgresql_client:
        psql_flexible_servers = []
        for resource_group in resource_groups:
            async for server in postgresql_client.servers.list_by_resource_group(
                resource_group_name=resource_group,
            ):
                psql_flexible_servers.append(dict(server.as_dict()))  # noqa: PERF401
    return psql_flexible_servers


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_psql_flexible_server_configs_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    config_name: str,
) -> list[dict[str, Any]]:
    servers = await run_azure_db_psql_flexible_server_client(credentials, client_credential)
    async with PostgreSQLManagementClientFlexibleServers(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as postgresql_client:
        psql_flexible_servers = []
        for server in servers:
            server_id = server.get("id", "")
            server_name = server.get("name", "")
            resource_group = get_resource_group_from_resource_id(server_id)
            config = await postgresql_client.configurations.get(
                resource_group_name=resource_group,
                server_name=server_name,
                configuration_name=config_name,
            )
            psql_flexible_servers.append(config.as_dict())
    return psql_flexible_servers


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_psql_flexible_server_firewall_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    flexible_servers = await run_azure_db_psql_flexible_server_client(
        credentials,
        client_credential,
    )
    async with PostgreSQLManagementClientFlexibleServers(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as psql_client:
        psql_flex_server_firewall = []
        for server in flexible_servers:
            server_id = server.get("id", "")
            server_name = server.get("name", "")
            network_config = server.get("network", {})
            resource_group = get_resource_group_from_resource_id(resource_id=server_id)
            rules = []
            async for firewall in psql_client.firewall_rules.list_by_server(
                resource_group_name=resource_group,
                server_name=server_name,
            ):
                firewall_dict = firewall.as_dict()
                firewall_dict["network"] = network_config
                rules.append(firewall_dict)
                psql_flex_server_firewall += rules
    return psql_flex_server_firewall
