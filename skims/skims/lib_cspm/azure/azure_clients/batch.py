import re
from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.batch.aio import (
    BatchManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def list_batch_accounts(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with BatchManagementClient(client_credential, credentials.subscription_id) as client:
        batch_accounts = []
        async for batch_account in client.batch_account.list():
            batch_accounts.append(dict(batch_account.as_dict()))  # noqa: PERF401
        return batch_accounts


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_batch_pools(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with BatchManagementClient(client_credential, credentials.subscription_id) as client:
        batch_accounts = await list_batch_accounts(credentials, client_credential)
        batch_pools = []
        for batch_account in batch_accounts:
            pattern = r"/resourceGroups/([^/]+)"
            resource_group = re.search(string=batch_account.get("id", ""), pattern=pattern)
            if not resource_group:
                continue
            async for pool in client.pool.list_by_batch_account(
                resource_group_name=resource_group.group(1),
                account_name=batch_account.get("name", ""),
            ):
                batch_pools.append(dict(pool.as_dict()))  # noqa: PERF401
        return batch_pools
