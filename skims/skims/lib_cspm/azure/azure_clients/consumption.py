from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.consumption.aio import (
    ConsumptionManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_subscription_cost_budgets(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    budgets = []
    subscription_id = credentials.subscription_id
    async with ConsumptionManagementClient(client_credential, subscription_id) as client:
        async for budget in client.budgets.list(scope=f"/subscriptions/{subscription_id}"):
            budgets.append(budget.as_dict())  # noqa: PERF401
    return budgets
