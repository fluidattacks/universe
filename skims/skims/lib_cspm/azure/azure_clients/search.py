from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.search.aio import (
    SearchManagementClient,
)
from lib_cspm.azure.azure_clients.azure_resource import (
    list_resource_groups_names,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_search_services(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    resource_group_names = await list_resource_groups_names(credentials, client_credential)
    search_services = []
    for resource_group in resource_group_names:
        async with SearchManagementClient(
            credential=client_credential,
            subscription_id=credentials.subscription_id,
        ) as client:
            async for search_service in client.services.list_by_resource_group(
                resource_group_name=resource_group,
            ):
                search_service_dict = search_service.as_dict()
                if not search_service.identity:
                    search_service_dict["identity"] = "None"
                search_services.append(dict(search_service_dict))
    return search_services
