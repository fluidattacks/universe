from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.compute.v2023_07_01.aio._compute_management_client import (
    ComputeManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_compute_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with ComputeManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_compute:
        all_vms = []
        async for vm_machine in client_compute.virtual_machines.list_all():
            all_vms.append(dict(vm_machine.as_dict()))  # noqa: PERF401
    return all_vms


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_compute_scale_sets_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with ComputeManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_compute:
        all_vms_scale_sets = []
        async for vm_machine in client_compute.virtual_machine_scale_sets.list_all():
            all_vms_scale_sets.append(dict(vm_machine.as_dict()))  # noqa: PERF401
    return all_vms_scale_sets
