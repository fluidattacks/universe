from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.sql.aio._sql_management_client import (
    SqlManagementClient,
)
from azure.mgmt.sql.models import (
    TransparentDataEncryptionName,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
    get_resource_group_from_resource_id,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_sql_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with SqlManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as sql_client:
        sql_servers = []
        async for server in sql_client.servers.list():
            sql_servers.append(server.as_dict())  # noqa: PERF401
    return sql_servers


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_databases(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    dbs = []
    async with SqlManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as sql_client:
        servers = await run_azure_sql_client(credentials, client_credential)
        for server in servers:
            server_id = server.get("id", "")
            server_name = server.get("name", "")
            resource_group = get_resource_group_from_resource_id(resource_id=server_id)
            async for database in sql_client.databases.list_by_server(
                resource_group_name=resource_group,
                server_name=server_name,
            ):
                db_dict = database.as_dict()
                db_dict["server_name"] = server_name
                dbs.append(db_dict)
    return dbs


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_dbs_transparent_data_encryption(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    all_tdes = []
    async with SqlManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as sql_client:
        dbs = await run_azure_databases(credentials, client_credential)
        for database in dbs:
            db_id = database.get("id", "")
            db_name = database.get("name", "")
            resource_group = get_resource_group_from_resource_id(db_id)
            server_name = database.get("server_name", "")
            tde = await sql_client.transparent_data_encryptions.get(
                resource_group_name=resource_group,
                database_name=db_name,
                server_name=server_name,
                transparent_data_encryption_name=(TransparentDataEncryptionName.CURRENT),
            )
            all_tdes.append(tde.as_dict())
    return all_tdes


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_sql_firewall_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    servers = await run_azure_sql_client(credentials, client_credential)
    async with SqlManagementClient(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as sql_client:
        sql_server_firewall = []
        for server in servers:
            server_id = server.get("id", "")
            server_name = server.get("name", "")
            public_network_access = server.get("public_network_access", "")
            resource_group = get_resource_group_from_resource_id(resource_id=server_id)
            rules = []
            async for firewall in sql_client.firewall_rules.list_by_server(
                resource_group_name=resource_group,
                server_name=server_name,
            ):
                firewall_dict = firewall.as_dict()
                firewall_dict["public_network_access"] = public_network_access
                rules.append(firewall_dict)
                sql_server_firewall += rules
    return sql_server_firewall


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_db_sql_extended_audit_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    servers = await run_azure_sql_client(credentials, client_credential)
    async with SqlManagementClient(
        credential=client_credential,
        subscription_id=credentials.subscription_id,
    ) as sql:
        sql_server_audit = []
        for server in servers:
            resource_group = server["id"].split("/")[4]
            async for audit in sql.extended_server_blob_auditing_policies.list_by_server(
                resource_group_name=resource_group,
                server_name=server["name"],
            ):
                sql_server_audit.append(audit.as_dict())  # noqa: PERF401

    return sql_server_audit
