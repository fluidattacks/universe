from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.authorization.v2022_04_01.aio import (
    AuthorizationManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_subscription_roles(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with AuthorizationManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as auth_client:
        roles = []
        subscription = f"/subscriptions/{credentials.subscription_id}"
        async for role in auth_client.role_definitions.list(scope=subscription):
            roles.append(dict(role.as_dict()))  # noqa: PERF401
        return roles


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_subscription_role_assignments(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with AuthorizationManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as auth_client:
        assignments = []
        async for assignment in auth_client.role_assignments.list_for_subscription():
            assignments.append(dict(assignment.as_dict()))  # noqa: PERF401
        return assignments


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_roles_by_principal(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    filter_principal_type: str | None = None,
) -> dict[str, list[str]]:
    roles = await run_azure_subscription_roles(credentials, client_credential)
    roles_name_by_id = {role.get("id"): role.get("role_name") for role in roles}
    role_assignments = await run_azure_subscription_role_assignments(
        credentials,
        client_credential,
    )
    roles_by_principal: dict[str, list[str]] = {}
    for assignation in role_assignments:
        principal_id = assignation.get("principal_id")
        prev_principal_roles = roles_by_principal.get(principal_id, [])
        role_id = assignation.get("role_definition_id", "")
        role_name = roles_name_by_id.get(role_id, "")
        principal_type = assignation.get("principal_type")
        if filter_principal_type and filter_principal_type != principal_type:
            continue
        updated_roles = [
            *prev_principal_roles,
            role_name,
        ]
        roles_by_principal[principal_id] = updated_roles
    return roles_by_principal


async def run_roles_by_principal_list(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    filter_principal_type: str | None = None,
) -> list[dict[str, list[str]]]:
    roles_dict = await run_roles_by_principal(credentials, client_credential, filter_principal_type)
    if roles_dict:
        return [roles_dict]
    return []
