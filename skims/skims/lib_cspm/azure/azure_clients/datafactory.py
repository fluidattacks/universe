from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.datafactory.aio import (
    DataFactoryManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_datafactory(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with DataFactoryManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_datafactory:
        factories = []
        async for factory in client_datafactory.factories.list():
            factories.append(dict(factory.as_dict()))  # noqa: PERF401
    return factories
