from typing import (
    Any,
)

from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.network import (
    run_azure_network_security_groups,
)
from lib_cspm.azure.azure_clients.storage import (
    run_azure_storage_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
    check_source_prefixes,
    has_vulnerable_ip_rules,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


def check_ports_access_from_any_ip(
    rule: dict[str, Any],
    protocols_to_verify: list[str],
    ports_to_verify: set[str] | None = None,
) -> bool:
    destination_port_ranges = set(
        rule.get("destination_port_ranges", [])
        if rule.get("destination_port_ranges")
        else [rule.get("destination_port_range", "")],
    )
    protocol = rule.get("protocol", "")
    access = rule.get("access", "")
    direction = rule.get("direction", "")
    return (
        (not ports_to_verify or len(ports_to_verify.intersection(destination_port_ranges)) > 0)
        and access == "Allow"
        and protocol in protocols_to_verify
        and check_source_prefixes(rule)
        and direction == "Inbound"
    )


@shield_cspm_azure_decorator
async def storage_account_allow_public_traffic(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.STORAGE_ACCOUNT_ALLOW_PUBLIC_TRAFFIC
    storage_accounts = await run_azure_client(
        run_azure_storage_client,
        credentials,
        client_credential,
    )
    vulns: Vulnerabilities = ()
    for storage_account in storage_accounts:
        public_network_access = storage_account.get("public_network_access", "Disabled")
        network_rule_set = storage_account.get("network_rule_set", {})
        default_action = network_rule_set.get("default_action", "Deny")
        ip_rules = network_rule_set.get("ip_rules", [])
        if public_network_access.lower() == "enabled":
            locations = []
            if default_action.lower() == "allow":
                locations.append(
                    Location(
                        id=storage_account["id"],
                        access_patterns=(
                            "/public_network_access",
                            "/network_rule_set/default_action",
                        ),
                        values=(
                            public_network_access,
                            default_action,
                        ),
                        method=method,
                    ),
                )
            if has_vulnerable_ip_rules(ip_rules):
                locations.append(
                    Location(
                        id=storage_account["id"],
                        access_patterns=(
                            "/public_network_access",
                            "/network_rule_set/ip_rules",
                        ),
                        values=(
                            public_network_access,
                            ip_rules,
                        ),
                        method=method,
                    ),
                )
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=storage_account,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_mongodb_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_MONGODB_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        mongodb_ports = {"27017", "27018", "27019"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=mongodb_ports,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_ms_sql_server_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_MS_SQL_SERVER_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        ms_sql_server_port = {"1433"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=ms_sql_server_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_mysql_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_MYSQL_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        mysql_server_port = {"3306"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=mysql_server_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_netbios_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_NETBIOS_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        netbios_tcp_port = {"139"}
        netbios_udp_ports = {"137", "138"}
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=["TCP", "*"],
                ports_to_verify=netbios_tcp_port,
            ) or check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=["UDP", "*"],
                ports_to_verify=netbios_udp_ports,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_oracle_database_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_ORACLE_DATABASE_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        oracle_database_port = {"1521"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=oracle_database_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_postgresql_db_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_POSTGRESQL_DB_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        postgresql_port = {"5432"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=postgresql_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_vms_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_VMS_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        vms_port = {"3389"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=vms_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_rpc_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_RPC_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        rpc_port = {"135"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=rpc_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_smtp_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SMTP_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        smtp_port = {"25"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=smtp_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_ssh_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SSH_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        ssh_port = {"22"}
        protocols_to_verify = ["TCP", "*"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
                ports_to_verify=ssh_port,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_udp_ports_nsg_allows_unrestricted_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_UDP_PORTS_NSG_ALLOWS_UNRESTRICTED_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        protocols_to_verify = ["UDP"]
        for rule in nsg.get("security_rules", []):
            if check_ports_access_from_any_ip(
                rule=rule,
                protocols_to_verify=protocols_to_verify,
            ):
                locations = [
                    Location(
                        id=rule.get("id", ""),
                        access_patterns=("/source_address_prefixes",)
                        if rule["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        values=(rule["source_address_prefixes"],)
                        if rule["source_address_prefixes"]
                        else (rule["source_address_prefix"],),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rule,
                )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    azure_netbios_nsg_allows_unrestricted_access,
    azure_mongodb_nsg_allows_unrestricted_access,
    azure_ms_sql_server_nsg_allows_unrestricted_access,
    azure_mysql_nsg_allows_unrestricted_access,
    azure_oracle_database_nsg_allows_unrestricted_access,
    azure_postgresql_db_nsg_allows_unrestricted_access,
    azure_rpc_nsg_allows_unrestricted_access,
    azure_smtp_nsg_allows_unrestricted_access,
    azure_ssh_nsg_allows_unrestricted_access,
    azure_udp_ports_nsg_allows_unrestricted_access,
    azure_vms_nsg_allows_unrestricted_access,
    storage_account_allow_public_traffic,
)
