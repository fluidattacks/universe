from lib_cspm.azure.azure_clients.app_service import (
    run_azure_web_apps_configuration,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def azure_app_service_remote_debugging_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_REMOTE_DEBUGGING_ENABLED
    vulns: Vulnerabilities = ()
    web_apps_configuration = await run_azure_client(
        run_azure_web_apps_configuration,
        credentials,
        client_credential,
    )
    for app_configuration in web_apps_configuration:
        remote_debugging_enabled = app_configuration.get("remote_debugging_enabled")
        if remote_debugging_enabled:
            locations = [
                Location(
                    id=app_configuration["id"],
                    access_patterns=("/remote_debugging_enabled",),
                    values=(remote_debugging_enabled,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=app_configuration,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (azure_app_service_remote_debugging_enabled,)
