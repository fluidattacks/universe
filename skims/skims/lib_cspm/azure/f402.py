from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.key_vault import (
    run_azure_key_vault,
)
from lib_cspm.azure.azure_clients.network import (
    run_azure_network_flow_logs,
    run_azure_network_watchers,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def network_flow_log_insecure_retention_period(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    min_retention_policy = 90
    method = MethodsEnum.NETWORK_FLOW_LOG_INSECURE_RETENTION_PERIOD
    vulns: Vulnerabilities = ()
    network_flow_logs = await run_azure_client(
        run_azure_network_flow_logs,
        credentials,
        client_credential,
    )
    for flow_log in network_flow_logs:
        retention_policy = flow_log["retention_policy"]
        if retention_policy["days"] < min_retention_policy or not retention_policy["enabled"]:
            locations = [
                Location(
                    values=(
                        retention_policy["days"],
                        retention_policy["enabled"],
                    ),
                    id=flow_log["id"],
                    access_patterns=(
                        "/retention_policy/days",
                        "/retention_policy/enabled",
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=flow_log,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def network_watcher_not_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_WATCHER_NOT_ENABLED
    vulns: Vulnerabilities = ()
    network_watcher = await run_azure_client(
        run_azure_network_watchers,
        credentials,
        client_credential,
    )
    if not network_watcher:
        locations = [
            Location(
                values=(),
                id=(
                    f"/subscriptions/{credentials.subscription_id}"
                    "/resourceGroups//providers/"
                    "Microsoft.Network/networkWatchers"
                ),
                access_patterns=(),
                method=method,
            ),
        ]
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            azure_response={},
        )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_key_vault_soft_delete_retention(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_KEY_VAULT_SOFT_DELETE_RETENTION
    vulns: Vulnerabilities = ()
    vaults = await run_azure_client(run_azure_key_vault, credentials, client_credential)
    for vault in vaults:
        vault_properties = vault.get("properties", {})
        enable_soft_delete = vault_properties.get(
            "enable_soft_delete",
        )
        soft_delete_retention_in_days = vault_properties.get(
            "soft_delete_retention_in_days",
        )
        if enable_soft_delete and soft_delete_retention_in_days < 90:
            locations = [
                Location(
                    id=vault["id"],
                    access_patterns=("/properties/soft_delete_retention_in_days",),
                    values=(soft_delete_retention_in_days,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vault,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    network_flow_log_insecure_retention_period,
    network_watcher_not_enabled,
    azure_key_vault_soft_delete_retention,
)
