from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.redis import (
    run_azure_redis_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def redis_cache_insecure_port(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDIS_CACHE_INSECURE_PORT
    vulns: Vulnerabilities = ()
    redis_caches = await run_azure_client(run_azure_redis_client, credentials, client_credential)
    for redis in redis_caches:
        enable_non_ssl_port = redis["enable_non_ssl_port"]
        if enable_non_ssl_port:
            locations = [
                Location(
                    values=(enable_non_ssl_port,),
                    id=redis["id"],
                    access_patterns=("/enable_non_ssl_port",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=redis,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (redis_cache_insecure_port,)
