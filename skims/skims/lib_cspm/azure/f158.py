from typing import (
    Any,
)

from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.network import (
    run_azure_firewall_network_rules,
    run_azure_network_security_groups,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    FORBIDDEN_PREFIXES,
    Location,
    build_vulnerabilities,
    check_source_prefixes,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)

DANGER_PORTS = {
    20,  # ftp
    21,  # ftp
    22,  # ssh
    23,  # telnet
    53,  # dns
    80,  # http
    443,  # https
    445,  # cifs
    3389,  # rdp
}


def check_if_port_ranges(rules: dict[str, Any]) -> bool:
    port_ranges = [
        *rules.get("destination_port_ranges", []),
        rules.get("destination_port_range", ""),
    ]
    return any("-" in port for port in port_ranges)


def ranges_include_dangerous_port(rules: dict[str, Any]) -> bool:
    port_ranges = [
        *rules.get("destination_port_ranges", []),
        rules.get("destination_port_range", ""),
    ]
    if "*" in port_ranges:
        return True
    for port in port_ranges:
        if "-" in port:
            port_split = port.split("-")
            start_port = int(port_split[0])
            end_port = int(port_split[1])
            if any(start_port <= danger_port <= end_port for danger_port in DANGER_PORTS):
                return True
        if port in DANGER_PORTS:
            return True
    return False


@shield_cspm_azure_decorator
async def network_security_group_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_SECURITY_GROUP_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )

    for nsg in network_security_groups:
        for rules in nsg["security_rules"]:
            if (
                rules["direction"] == "Inbound"
                and rules["access"] == "Allow"
                and check_source_prefixes(rules)
            ):
                locations = [
                    Location(
                        values=(rules["source_address_prefixes"],)
                        if rules["source_address_prefixes"]
                        else (rules["source_address_prefix"],),
                        id=rules["id"],
                        access_patterns=("/source_address_prefixes",)
                        if rules["source_address_prefixes"]
                        else ("/source_address_prefix",),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rules,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def network_security_group_using_port_ranges(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_SECURITY_GROUP_USING_PORT_RANGES
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )
    for nsg in network_security_groups:
        for rules in nsg["security_rules"]:
            if check_if_port_ranges(rules):
                locations = [
                    Location(
                        values=(rules["destination_port_ranges"],)
                        if rules["destination_port_ranges"]
                        else (rules["destination_port_range"],),
                        id=rules["id"],
                        access_patterns=("/destination_port_ranges",)
                        if rules["destination_port_ranges"]
                        else ("/destination_port_range",),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rules,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def network_security_group_access_on_ports(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_SECURITY_GROUP_ACCESS_ON_PORTS
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )

    for nsg in network_security_groups:
        for rules in nsg["security_rules"]:
            if (
                rules["direction"] == "Inbound"
                and rules["access"] == "Allow"
                and rules["protocol"] in {"TCP", "*"}
                and check_source_prefixes(rules)
                and ranges_include_dangerous_port(rules)
            ):
                locations = [
                    Location(
                        values=(rules["destination_port_ranges"],)
                        if rules["destination_port_ranges"]
                        else (rules["destination_port_range"],),
                        id=rules["id"],
                        access_patterns=("/destination_port_ranges",)
                        if rules["destination_port_ranges"]
                        else ("/destination_port_range",),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rules,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def network_icmp_ingress_not_restricted(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_ICMP_INGRESS_NOT_RESTRICTED
    vulns: Vulnerabilities = ()
    network_security_groups = await run_azure_client(
        run_azure_network_security_groups,
        credentials,
        client_credential,
    )

    for nsg in network_security_groups:
        for rules in nsg["security_rules"]:
            if rules["protocol"] in {"ICMP", "*"} and check_source_prefixes(rules):
                locations = [
                    Location(
                        values=(
                            rules["source_address_prefixes"],
                            rules["protocol"],
                        )
                        if rules["source_address_prefixes"]
                        else (
                            rules["source_address_prefix"],
                            rules["protocol"],
                        ),
                        id=rules["id"],
                        access_patterns=(
                            "/source_address_prefixes",
                            "/protocol",
                        )
                        if rules["source_address_prefixes"]
                        else (
                            "/source_address_prefix",
                            "/protocol",
                        ),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rules,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def network_firewall_app_rule_unrestricted(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_FIREWALL_APP_RULE_UNRESTRICTED
    vulns: Vulnerabilities = ()
    firewall_rules = await run_azure_client(
        run_azure_firewall_network_rules,
        credentials,
        client_credential,
    )
    for firewall in firewall_rules:
        for rules in firewall.get("application_rule_collections", []):
            for rule in rules.get("rules", []):
                if any(prefix in FORBIDDEN_PREFIXES for prefix in rule["source_addresses"]):
                    locations = [
                        Location(
                            values=(rule["source_addresses"],),
                            id=firewall["id"],
                            access_patterns=("/source_addresses",),
                            method=method,
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        azure_response=rule,
                    )

    return (vulns, True)


@shield_cspm_azure_decorator
async def network_firewall_network_rule_unrestricted(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_FIREWALL_NETWORK_RULE_UNRESTRICTED
    vulns: Vulnerabilities = ()
    firewall_rules = await run_azure_client(
        run_azure_firewall_network_rules,
        credentials,
        client_credential,
    )
    for firewall in firewall_rules:
        for rules in firewall.get("network_rule_collections", []):
            for rule in rules.get("rules", []):
                if any(prefix in FORBIDDEN_PREFIXES for prefix in rule["source_addresses"]):
                    locations = [
                        Location(
                            values=(rule["source_addresses"],),
                            id=firewall["id"],
                            access_patterns=("/source_addresses",),
                            method=method,
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        azure_response=rule,
                    )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    network_security_group_allows_public_access,
    network_security_group_using_port_ranges,
    network_security_group_access_on_ports,
    network_icmp_ingress_not_restricted,
    network_firewall_network_rule_unrestricted,
    network_firewall_app_rule_unrestricted,
)
