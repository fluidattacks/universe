from lib_cspm.azure.azure_clients.apimanagement import (
    run_api_mgmt_services_portal_configuration,
)
from lib_cspm.azure.azure_clients.app_service import (
    run_azure_web_apps_auth_configuration,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.redis import (
    run_azure_redis_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def redis_cache_authnotrequired_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDIS_CACHE_AUTHNOTREQUIRED_ENABLED
    vulns: Vulnerabilities = ()
    redis_caches = await run_azure_client(run_azure_redis_client, credentials, client_credential)
    for redis in redis_caches:
        authnotrequired = redis["redis_configuration"].get("authnotrequired", "Disabled")
        if authnotrequired != "Disabled":
            locations = [
                Location(
                    values=(authnotrequired,),
                    id=redis["id"],
                    access_patterns=("/redis_configuration/authnotrequired",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=redis,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_app_service_authentication_is_not_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_AUTHENTICATION_IS_NOT_ENABLED
    vulns: Vulnerabilities = ()
    web_apps_auth_settings = await run_azure_client(
        run_azure_web_apps_auth_configuration,
        credentials,
        client_credential,
    )
    for app_settings in web_apps_auth_settings:
        auth_platform = app_settings.get("platform", {})
        is_auth_enabled = auth_platform.get("enabled")
        if not is_auth_enabled:
            locations = [
                Location(
                    id=app_settings["id"],
                    access_patterns=("/platform/enabled",),
                    values=(is_auth_enabled,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=app_settings,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_dev_portal_has_auth_methods_inactive(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DEV_PORTAL_HAS_AUTH_METHODS_INACTIVE
    vulns: Vulnerabilities = ()
    api_mgmt_services_portal_config = await run_azure_client(
        run_api_mgmt_services_portal_configuration,
        credentials,
        client_credential,
    )
    for portal_configuration in api_mgmt_services_portal_config:
        value = portal_configuration.get("value", {})[0]
        portal_id = value.get("id", "")
        sign_in = value.get("signin", {})
        require_sign_in = sign_in.get("require", True)
        if not require_sign_in:
            locations = [
                Location(
                    id=portal_id,
                    access_patterns=("/value/0/signin/require",),
                    values=(require_sign_in,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=portal_configuration,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    redis_cache_authnotrequired_enabled,
    azure_app_service_authentication_is_not_enabled,
    azure_dev_portal_has_auth_methods_inactive,
)
