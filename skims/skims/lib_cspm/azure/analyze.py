import ctx
from aioextensions import (
    resolve,
)
from lib_cspm.azure import (
    f016,
    f101,
    f148,
    f157,
    f158,
    f183,
    f203,
    f281,
    f300,
    f319,
    f325,
    f372,
    f392,
    f402,
    f446,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
)
from model.core import (
    AzureCredentials,
    FindingEnum,
    Vulnerability,
)
from utils.logs import (
    log_blocking,
)
from utils.state import EphemeralStore, VulnerabilitiesEphemeralStore

AZURE_CHECKS: dict[FindingEnum, AzureMethodsTuple] = {
    FindingEnum.F016: f016.CHECKS,
    FindingEnum.F101: f101.CHECKS,
    FindingEnum.F148: f148.CHECKS,
    FindingEnum.F157: f157.CHECKS,
    FindingEnum.F158: f158.CHECKS,
    FindingEnum.F183: f183.CHECKS,
    FindingEnum.F203: f203.CHECKS,
    FindingEnum.F281: f281.CHECKS,
    FindingEnum.F300: f300.CHECKS,
    FindingEnum.F319: f319.CHECKS,
    FindingEnum.F325: f325.CHECKS,
    FindingEnum.F372: f372.CHECKS,
    FindingEnum.F392: f392.CHECKS,
    FindingEnum.F402: f402.CHECKS,
    FindingEnum.F446: f446.CHECKS,
}


def handle_vulnerabilities(
    vulnerabilities: tuple[Vulnerability, ...],
    stores: VulnerabilitiesEphemeralStore,
) -> None:
    stores.store_vulns(vulnerabilities)


async def analyze_cloud(
    credentials: AzureCredentials,
    client_credentials: ClientSecretCredential,
    stores: VulnerabilitiesEphemeralStore,
    execution_checks: set[FindingEnum],
) -> set[str]:
    methods_with_errors: set[str] = set()
    for finding, checks in AZURE_CHECKS.items():
        if finding not in execution_checks:
            continue
        log_blocking("info", "Running finding %s", finding.name)

        futures = [check(credentials, client_credentials) for check in checks]

        for azure_method in resolve(futures, workers=ctx.CPU_CORES):
            method_vulns, method_succeeded = await azure_method

            handle_vulnerabilities(method_vulns, stores)

            if isinstance(method_succeeded, str):
                methods_with_errors.add(method_succeeded)

    return set(methods_with_errors)


async def analyze(
    *,
    credentials: AzureCredentials,
    stores: VulnerabilitiesEphemeralStore,
    error_stores: dict[str, EphemeralStore],
) -> None:
    if not any(finding in ctx.SKIMS_CONFIG.checks for finding in AZURE_CHECKS):
        return

    log_blocking("info", "Starting CSPM/Azure analysis on credentials")

    async with ClientSecretCredential(
        tenant_id=credentials.tenant_id,
        client_id=credentials.client_id,
        client_secret=credentials.client_secret,
    ) as client_credential:
        error_methods = await analyze_cloud(
            credentials,
            client_credential,  # type: ignore[arg-type]
            stores,
            ctx.SKIMS_CONFIG.checks,
        )

    error_stores["azure_errors"].store(list(error_methods))

    log_blocking("info", "CSPM/Azure analysis on credentials completed!")
