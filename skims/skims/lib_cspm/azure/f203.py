from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.storage import (
    run_azure_storage_blob_containers,
    run_azure_storage_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def blob_containers_are_public(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.BLOB_CONTAINERS_ARE_PUBLIC
    all_containers = await run_azure_client(
        run_azure_storage_blob_containers,
        credentials,
        client_credential,
    )
    vulns: Vulnerabilities = ()
    for container in all_containers:
        if container["public_access"] != "None":
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=("/public_access",),
                    values=(container["public_access"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def storage_account_allows_public_blobs(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.STORAGE_ACCOUNT_ALLOWS_PUBLIC_BLOBS
    storages = await run_azure_client(run_azure_storage_client, credentials, client_credential)
    vulns: Vulnerabilities = ()
    for storage in storages:
        if storage["allow_blob_public_access"]:
            locations = [
                Location(
                    id=storage["id"],
                    access_patterns=("/allow_blob_public_access",),
                    values=(storage["allow_blob_public_access"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=storage,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    blob_containers_are_public,
    storage_account_allows_public_blobs,
)
