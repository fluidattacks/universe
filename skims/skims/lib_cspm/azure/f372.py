from lib_cspm.azure.azure_clients.apimanagement import (
    run_azure_apis,
)
from lib_cspm.azure.azure_clients.app_service import (
    run_azure_web_apps,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.storage import (
    run_azure_storage_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def storage_account_not_enforce_https(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.STORAGE_ACCOUNT_NOT_ENFORCE_HTTPS
    vulns: Vulnerabilities = ()

    storage_accounts = await run_azure_client(
        run_azure_storage_client,
        credentials,
        client_credential,
    )
    for storage_account in storage_accounts:
        https_enabled = storage_account["enable_https_traffic_only"]
        if not https_enabled:
            locations = [
                Location(
                    id=storage_account["id"],
                    access_patterns=("/enable_https_traffic_only",),
                    values=(https_enabled,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=storage_account,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_app_service_allows_http_trafic(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_ALLOWS_HTTP_TRAFIC
    vulns: Vulnerabilities = ()
    web_apps_configuration = await run_azure_client(
        run_azure_web_apps,
        credentials,
        client_credential,
    )
    for app_configuration in web_apps_configuration:
        https_only = app_configuration.get("https_only")
        if not https_only:
            locations = [
                Location(
                    id=app_configuration["id"],
                    access_patterns=("/https_only",),
                    values=(https_only,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=app_configuration,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_api_not_enforce_https(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_API_NOT_ENFORCE_HTTPS
    vulns: Vulnerabilities = ()
    apis = await run_azure_client(run_azure_apis, credentials, client_credential)
    for api in apis:
        protocols = api.get("protocols", [])
        http_index = protocols.index("http") if "http" in protocols else -1
        if http_index != -1:
            locations = [
                Location(
                    id=api.get("id", ""),
                    access_patterns=("/protocols",),
                    values=("http",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=api,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    azure_api_not_enforce_https,
    storage_account_not_enforce_https,
    azure_app_service_allows_http_trafic,
)
