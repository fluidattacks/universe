from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.rdbms import (
    run_azure_db_psql_flexible_server_firewall_client,
)
from lib_cspm.azure.azure_clients.storage import (
    run_azure_storage_client,
)
from lib_cspm.azure.azure_clients.synapse import (
    run_azure_synapse_workspaces_ip_firewall_rules,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
    is_valid_ip_address_or_range,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


async def validate_service_public_network_access(
    service_config: dict,
    method: MethodsEnum,
) -> Vulnerabilities:
    locations = []
    public_network_access = service_config.get("public_network_access", "")
    network_rule_set = service_config.get("network_rule_set", {})
    virtual_network_rules = network_rule_set.get("virtual_network_rules", [])
    network_rules_default_action = network_rule_set.get("default_action", "")
    ip_rules = network_rule_set.get("ip_rules", [])
    invalid_ip_rules = set()
    for rule in ip_rules:
        ip_address_or_range = rule.get("ip_address_or_range", "-")
        if not is_valid_ip_address_or_range(ip_address_or_range):
            invalid_ip_rules.add(ip_address_or_range)
    if public_network_access.lower() == "enabled":
        if network_rules_default_action.lower() == "allow":
            locations = [
                Location(
                    id=service_config["id"],
                    access_patterns=(
                        "/public_network_access",
                        "/network_rule_set/default_action",
                    ),
                    values=(
                        public_network_access,
                        network_rules_default_action,
                    ),
                    method=method,
                ),
            ]
        elif not virtual_network_rules and invalid_ip_rules:
            locations = [
                Location(
                    id=service_config["id"],
                    access_patterns=(
                        "/public_network_access",
                        "/network_rule_set/ip_rules",
                    ),
                    values=(
                        public_network_access,
                        invalid_ip_rules,
                    ),
                    method=method,
                ),
            ]
    return build_vulnerabilities(
        locations=locations,
        method=method,
        azure_response=service_config,
    )


@shield_cspm_azure_decorator
async def azure_data_lake_allows_access_from_any_source(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DATA_LAKE_ALLOWS_ACCESS_FROM_ANY_SOURCE
    vulns: Vulnerabilities = ()
    storage_accounts = await run_azure_client(
        run_azure_storage_client,
        credentials,
        client_credential,
    )
    for storage_account in storage_accounts:
        is_data_lake = storage_account.get("is_hns_enabled", False)
        if is_data_lake:
            vulns += await validate_service_public_network_access(
                service_config=storage_account,
                method=method,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_synapse_firewall_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SYNAPSE_FIREWALL_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    firewall_config = await run_azure_client(
        run_azure_synapse_workspaces_ip_firewall_rules,
        credentials,
        client_credential,
    )
    for firewall in firewall_config:
        start_ip = firewall.get("start_ip_address", "")
        end_ip = firewall.get("end_ip_address", "")
        public_network_access = firewall.get("public_network_access", "")
        locations = []
        if (
            public_network_access.lower() == "enabled"
            and start_ip
            and end_ip
            and not is_valid_ip_address_or_range(start_ip)
            and not is_valid_ip_address_or_range(end_ip)
        ):
            firewall.pop("public_network_access")
            locations = [
                Location(
                    id=firewall.get("id", ""),
                    access_patterns=("/start_ip_address", "/end_ip_address"),
                    values=(
                        start_ip,
                        end_ip,
                    ),
                    method=method,
                ),
            ]
        vulns += build_vulnerabilities(locations=locations, method=method, azure_response=firewall)
    return (vulns, True)


@shield_cspm_azure_decorator
async def az_db_psql_flex_server_firewall_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZ_DB_PSQL_FLEX_SERVER_FIREWALL_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    firewall_config = await run_azure_client(
        run_azure_db_psql_flexible_server_firewall_client,
        credentials,
        client_credential,
    )
    for firewall in firewall_config:
        network = firewall.get("network", {})
        public_network_access = network.get("public_network_access", "Disabled")
        start_ip = firewall.get("start_ip_address", "")
        end_ip = firewall.get("end_ip_address", "")
        if (
            public_network_access.lower() == "enabled"
            and start_ip
            and end_ip
            and not is_valid_ip_address_or_range(start_ip)
            and not is_valid_ip_address_or_range(end_ip)
        ):
            firewall.pop("network")
            locations = [
                Location(
                    id=firewall.get("id", ""),
                    access_patterns=("/start_ip_address", "/end_ip_address"),
                    values=(
                        start_ip,
                        end_ip,
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=firewall,
            )
    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    az_db_psql_flex_server_firewall_allows_public_access,
    azure_data_lake_allows_access_from_any_source,
    azure_synapse_firewall_allows_public_access,
)
