from lib_cspm.azure.azure_clients.authorization import (
    run_azure_subscription_roles,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def azure_role_actions_is_a_wildcard(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_ROLE_ACTIONS_IS_A_WILDCARD
    vulns: Vulnerabilities = ()
    roles = await run_azure_client(
        run_azure_subscription_roles,
        credentials,
        client_credential,
    )
    for role in roles:
        if role.get("role_type") == "CustomRole":
            permissions = role.get("permissions", [])
            for permission_index, permission in enumerate(permissions):
                actions = permission.get("actions")
                if "*" in actions:
                    locations = [
                        Location(
                            values=("'*'",),
                            id=role["id"],
                            access_patterns=(f"/permissions/{permission_index}/actions",),
                            method=method,
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        azure_response=role,
                    )
    return (vulns, True)


CHECKS: AzureMethodsTuple = (azure_role_actions_is_a_wildcard,)
