from lib_cspm.azure.azure_clients.apimanagement import (
    run_api_management_services,
)
from lib_cspm.azure.azure_clients.app_service import (
    run_azure_web_apps_configuration,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.rdbms import (
    run_azure_db_mysql_configs_client,
    run_azure_db_postgresql_server_client,
    run_azure_db_psql_flexible_server_configs_client,
)
from lib_cspm.azure.azure_clients.redis import (
    run_azure_redis_client,
)
from lib_cspm.azure.azure_clients.storage import (
    run_azure_storage_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def storage_account_not_enforce_latest_tls(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.STORAGE_ACCOUNT_NOT_ENFORCE_LATEST_TLS
    vulns: Vulnerabilities = ()

    storage_accounts = await run_azure_client(
        run_azure_storage_client,
        credentials,
        client_credential,
    )
    for storage_account in storage_accounts:
        tls_version = storage_account["minimum_tls_version"]
        if tls_version != "TLS1_2":
            locations = [
                Location(
                    id=storage_account["id"],
                    access_patterns=("/minimum_tls_version",),
                    values=(tls_version,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=storage_account,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def redis_cache_insecure_tls_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDIS_CACHE_INSECURE_TLS_VERSION
    vulns: Vulnerabilities = ()
    redis_caches = await run_azure_client(run_azure_redis_client, credentials, client_credential)
    for redis in redis_caches:
        minimum_tls_version = redis.get("minimum_tls_version", "1.2")
        if minimum_tls_version < "1.2":
            locations = [
                Location(
                    values=(minimum_tls_version,),
                    id=redis["id"],
                    access_patterns=("/minimum_tls_version",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=redis,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_web_app_insecure_tls_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_WEB_APP_INSECURE_TLS_VERSION
    vulns: Vulnerabilities = ()
    web_apps_configuration = await run_azure_client(
        run_azure_web_apps_configuration,
        credentials,
        client_credential,
    )
    for app_configuration in web_apps_configuration:
        minimum_tls_version = app_configuration.get("min_tls_version", "1.2")
        if minimum_tls_version < "1.2":
            locations = [
                Location(
                    values=(minimum_tls_version,),
                    id=app_configuration["id"],
                    access_patterns=("/min_tls_version",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=app_configuration,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_for_mysql_flex_servers_insecure_tls_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_FOR_MYSQL_FLEX_SERVERS_INSECURE_TLS_VERSION
    vulns: Vulnerabilities = ()
    dbs_configurations = await run_azure_client(
        run_azure_db_mysql_configs_client,
        credentials,
        client_credential,
        config_name="tls_version",
    )
    for configuration in dbs_configurations:
        tls_version = configuration.get("value", "TLSv1.2")
        tls_version_float = float(tls_version[4:])
        if tls_version_float < 1.2:
            locations = [
                Location(
                    values=(tls_version,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_for_postgresql_insecure_tls_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_POSTGRESQL_INSECURE_TLS_VERSION
    vulns: Vulnerabilities = ()
    server_config = await run_azure_client(
        run_azure_db_postgresql_server_client,
        credentials,
        client_credential,
    )
    for configuration in server_config:
        tls_version = configuration.get("minimal_tls_version", "")
        if tls_version != "TLS1_2":
            locations = [
                Location(
                    values=(tls_version,),
                    id=configuration["id"],
                    access_patterns=("/minimal_tls_version",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_postgresql_ssl_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_FOR_POSTGRESQL_SSL_DISABLED
    vulns: Vulnerabilities = ()
    dbs_configurations = await run_azure_client(
        run_azure_db_postgresql_server_client,
        credentials,
        client_credential,
    )
    for configuration in dbs_configurations:
        ssl_enforcement = configuration.get("ssl_enforcement")
        if ssl_enforcement != "Enabled":
            locations = [
                Location(
                    values=(ssl_enforcement,),
                    id=configuration["id"],
                    access_patterns=("/ssl_enforcement",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_mysql_ssl_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_MYSQL_SSL_DISABLED
    vulns: Vulnerabilities = ()
    dbs_configurations = await run_azure_client(
        run_azure_db_mysql_configs_client,
        credentials,
        client_credential,
        config_name="require_secure_transport",
    )
    for configuration in dbs_configurations:
        ssl_enforcement = configuration.get("value", "on")
        if ssl_enforcement.lower() == "off":
            locations = [
                Location(
                    values=(ssl_enforcement,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_api_mgmt_back__insecure_tls_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_API_MGMT_BACK_INSECURE_TLS_VERSION
    vulns: Vulnerabilities = ()
    api_mgmt_services = await run_azure_client(
        run_api_management_services,
        credentials,
        client_credential,
    )
    tls_10_key = "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Backend.Protocols.Tls10"
    tls_11_key = "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Backend.Protocols.Tls11"
    for api_mgmt_service in api_mgmt_services:
        api_mgmt_service_id = api_mgmt_service.get("id")
        custom_properties = api_mgmt_service.get("custom_properties", {})
        allows_tls_10 = custom_properties.get(tls_10_key, "False").capitalize()
        allows_tls_11 = custom_properties.get(tls_11_key, "False").capitalize()
        locations = []
        if allows_tls_10 == "True":
            locations.append(
                Location(
                    id=f"{api_mgmt_service_id}-TlsV1.0",
                    access_patterns=(f"/custom_properties/{tls_10_key}",),
                    values=(allows_tls_10,),
                    method=method,
                    desc_params={"version": "V1.0"},
                ),
            )
        if allows_tls_11 == "True":
            locations.append(
                Location(
                    id=f"{api_mgmt_service_id}-TlsV1.1",
                    access_patterns=(f"/custom_properties/{tls_11_key}",),
                    values=(allows_tls_11,),
                    method=method,
                    desc_params={"version": "V1.1"},
                ),
            )
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            azure_response=api_mgmt_service,
        )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_api_mgmt_front__insecure_tls_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_API_MGMT_FRONT_INSECURE_TLS_VERSION
    vulns: Vulnerabilities = ()
    api_mgmt_services = await run_azure_client(
        run_api_management_services,
        credentials,
        client_credential,
    )
    tls_10_key = "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Protocols.Tls10"
    tls_11_key = "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Protocols.Tls11"
    for api_mgmt_service in api_mgmt_services:
        api_mgmt_service_id = api_mgmt_service.get("id")
        custom_properties = api_mgmt_service.get("custom_properties", {})
        allows_tls_10 = custom_properties.get(tls_10_key, "False").capitalize()
        allows_tls_11 = custom_properties.get(tls_11_key, "False").capitalize()
        locations = []
        if allows_tls_10 == "True":
            locations.append(
                Location(
                    id=f"{api_mgmt_service_id}-TlsV1.0",
                    access_patterns=(f"/custom_properties/{tls_10_key}",),
                    values=(allows_tls_10,),
                    method=method,
                    desc_params={"version": "V1.0"},
                ),
            )
        if allows_tls_11 == "True":
            locations.append(
                Location(
                    id=f"{api_mgmt_service_id}-TlsV1.1",
                    access_patterns=(f"/custom_properties/{tls_11_key}",),
                    values=(allows_tls_11,),
                    method=method,
                    desc_params={"version": "V1.1"},
                ),
            )
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            azure_response=api_mgmt_service,
        )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_psql_flex_server_insecure_tls_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_PSQL_FLEX_SERVER_INSECURE_TLS_VERSION
    vulns: Vulnerabilities = ()
    server_config = await run_azure_client(
        run_azure_db_psql_flexible_server_configs_client,
        credentials,
        client_credential,
        config_name="ssl_min_protocol_version",
    )
    for configuration in server_config:
        min_ssl_version = configuration.get("value", "")
        protocol = min_ssl_version[:3]
        min_ssl_version_float = float(min_ssl_version[4:])
        if protocol == "TLS" and min_ssl_version_float < 1.2:
            locations = [
                Location(
                    values=(min_ssl_version,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_redis_cache_allows_connections_without_ssl(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_REDIS_CACHE_ALLOWS_CONNECTIONS_WITHOUT_SSL
    vulns: Vulnerabilities = ()
    server_config = await run_azure_client(
        run_azure_redis_client,
        credentials,
        client_credential,
    )
    for configuration in server_config:
        enable_non_ssl_port = configuration.get("enable_non_ssl_port", "Disabled")
        if enable_non_ssl_port:
            locations = [
                Location(
                    id=configuration["id"],
                    access_patterns=("/enable_non_ssl_port",),
                    values=(enable_non_ssl_port,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_psql_flexible_server__ssl_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_PSQL_FLEXIBLE_SERVER__SSL_DISABLED
    vulns: Vulnerabilities = ()
    dbs_configurations = await run_azure_client(
        run_azure_db_psql_flexible_server_configs_client,
        credentials,
        client_credential,
        config_name="require_secure_transport",
    )
    for configuration in dbs_configurations:
        ssl_enforcement = configuration.get("value", "on")
        if ssl_enforcement.lower() == "off":
            locations = [
                Location(
                    values=(ssl_enforcement,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    azure_db_mysql_ssl_disabled,
    azure_db_postgresql_ssl_disabled,
    azure_db_psql_flex_server_insecure_tls_version,
    storage_account_not_enforce_latest_tls,
    redis_cache_insecure_tls_version,
    azure_web_app_insecure_tls_version,
    azure_db_for_mysql_flex_servers_insecure_tls_version,
    azure_db_for_postgresql_insecure_tls_version,
    azure_api_mgmt_back__insecure_tls_version,
    azure_api_mgmt_front__insecure_tls_version,
    azure_redis_cache_allows_connections_without_ssl,
    azure_db_psql_flexible_server__ssl_disabled,
)
