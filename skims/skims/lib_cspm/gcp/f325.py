from lib_cspm.gcp.model import (
    GcpMethodsTuple,
    GcpServices,
    Location,
    SnippetObject,
)
from lib_cspm.gcp.shield import (
    shield_cspm_gcp_decorator,
)
from lib_cspm.gcp.utils import (
    build_vulnerabilities,
    get_storage_client_buckets,
    run_gcp_client,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    GcpCredentials,
    Vulnerabilities,
)


@shield_cspm_gcp_decorator
async def public_buckets(
    credentials: GcpCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.GCP_STORAGE_PUBLIC_BUCKETS
    vulnerabilities: Vulnerabilities = ()

    buckets, project_id = run_gcp_client(get_storage_client_buckets, credentials)
    public_roles = {"allAuthenticatedUsers", "allUsers"}

    for bucket in buckets:
        bucket_path = bucket.path

        if bucket.iam_configuration.public_access_prevention != "enforced":
            policies = bucket.get_iam_policy()
            for policy in policies.bindings:
                for role in public_roles:
                    if role in policy["members"]:
                        snippet_object: SnippetObject = {
                            "project_id": project_id,
                            "service": GcpServices.CLOUD_STORAGE,
                            "path": bucket_path,
                            "vulnerable_property": "PERMISSIONS",
                            "vulnerability": policy,
                        }

                        locations = [
                            Location(
                                access_patterns=("/vulnerability/members",),
                                uri=(
                                    f"uri://{project_id}"
                                    f"/{GcpServices.CLOUD_STORAGE.value}"
                                    f"{bucket_path}"
                                ),
                                values=(role,),
                                method=method,
                            ),
                        ]

                        vulnerabilities += build_vulnerabilities(
                            locations=locations,
                            method=method,
                            gcp_response=snippet_object,
                        )

    return (vulnerabilities, True)


CHECKS: GcpMethodsTuple = (public_buckets,)
