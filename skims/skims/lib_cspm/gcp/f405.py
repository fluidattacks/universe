from lib_cspm.gcp.model import (
    GcpMethodsTuple,
    GcpServices,
    Location,
    SnippetObject,
)
from lib_cspm.gcp.shield import (
    shield_cspm_gcp_decorator,
)
from lib_cspm.gcp.utils import (
    build_vulnerabilities,
    get_storage_client_buckets,
    run_gcp_client,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    GcpCredentials,
    Vulnerabilities,
)


@shield_cspm_gcp_decorator
async def uniform_bucket_level_access_is_disabled(
    credentials: GcpCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.UNIFORM_BUCKET_LEVEL_ACCESS_IS_DISABLED
    vulnerabilities: Vulnerabilities = ()
    buckets, project_id = run_gcp_client(get_storage_client_buckets, credentials)
    for bucket in buckets:
        bucket_path = bucket.path

        if not bucket.iam_configuration.bucket_policy_only_enabled:
            vulnerability = bucket._properties.copy()  # noqa: SLF001
            vulnerability["iamConfiguration"]["uniformBucketLevelAccess"] = {"enabled": False}
            snippet_object: SnippetObject = {
                "project_id": project_id,
                "service": GcpServices.CLOUD_STORAGE,
                "path": bucket_path,
                "vulnerable_property": "PERMISSIONS",
                "vulnerability": vulnerability,
            }

            locations = [
                Location(
                    access_patterns=(
                        "/vulnerability/iamConfiguration/uniformBucketLevelAccess/enabled",
                    ),
                    uri=(f"uri://{project_id}/{GcpServices.CLOUD_STORAGE.value}{bucket_path}"),
                    values=(False,),
                    method=method,
                ),
            ]

            vulnerabilities += build_vulnerabilities(
                locations=locations,
                method=method,
                gcp_response=snippet_object,
            )

    return (vulnerabilities, True)


CHECKS: GcpMethodsTuple = (uniform_bucket_level_access_is_disabled,)
