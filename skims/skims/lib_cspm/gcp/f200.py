from lib_cspm.gcp.model import (
    GcpMethodsTuple,
    GcpServices,
    Location,
    SnippetObject,
)
from lib_cspm.gcp.shield import (
    shield_cspm_gcp_decorator,
)
from lib_cspm.gcp.utils import (
    build_vulnerabilities,
    get_storage_client_buckets,
    run_gcp_client,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    GcpCredentials,
    Vulnerabilities,
)


@shield_cspm_gcp_decorator
async def object_versioning_is_not_enabled(
    credentials: GcpCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.OBJECT_VERSIONING_IS_NOT_ENABLED
    vulnerabilities: Vulnerabilities = ()
    buckets, project_id = run_gcp_client(get_storage_client_buckets, credentials)
    for bucket in buckets:
        bucket_path = bucket.path

        if not bucket.versioning_enabled:
            vulnerability = bucket._properties.copy()  # noqa: SLF001
            vulnerability["versioning"] = {"enabled": False}
            snippet_object: SnippetObject = {
                "project_id": project_id,
                "service": GcpServices.CLOUD_STORAGE,
                "path": bucket_path,
                "vulnerable_property": "PROTECTION",
                "vulnerability": vulnerability,
            }

            locations = [
                Location(
                    access_patterns=("/vulnerability/versioning/enabled",),
                    uri=(f"uri://{project_id}/{GcpServices.CLOUD_STORAGE.value}{bucket_path}"),
                    values=(False,),
                    method=method,
                ),
            ]

            vulnerabilities += build_vulnerabilities(
                locations=locations,
                method=method,
                gcp_response=snippet_object,
            )

    return (vulnerabilities, True)


@shield_cspm_gcp_decorator
async def lifecycle_is_not_defined(
    credentials: GcpCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.LIFECYCLE_IS_NOT_DEFINED
    vulnerabilities: Vulnerabilities = ()
    buckets, project_id = run_gcp_client(get_storage_client_buckets, credentials)
    for bucket in buckets:
        bucket_path = bucket.path
        lifecycle_rules = list(bucket.lifecycle_rules)
        vulnerability = bucket._properties.copy()  # noqa: SLF001
        vulnerability["lifecycle"] = None
        if not lifecycle_rules:
            snippet_object: SnippetObject = {
                "project_id": project_id,
                "service": GcpServices.CLOUD_STORAGE,
                "path": bucket_path,
                "vulnerable_property": "LIFECYCLE",
                "vulnerability": vulnerability,
            }

            locations = [
                Location(
                    access_patterns=("/vulnerability/lifecycle",),
                    uri=(f"uri://{project_id}/{GcpServices.CLOUD_STORAGE.value}{bucket_path}"),
                    values=(vulnerability["lifecycle"],),
                    method=method,
                ),
            ]

            vulnerabilities += build_vulnerabilities(
                locations=locations,
                method=method,
                gcp_response=snippet_object,
            )

    return (vulnerabilities, True)


@shield_cspm_gcp_decorator
async def retention_policy_is_not_configured(
    credentials: GcpCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.RETENTION_POLICY_IS_NOT_CONFIGURED
    vulnerabilities: Vulnerabilities = ()
    buckets, project_id = run_gcp_client(get_storage_client_buckets, credentials)

    for bucket in buckets:
        bucket_path = bucket.path

        if not bucket.retention_policy_locked:
            vulnerability = bucket._properties.copy()  # noqa: SLF001
            vulnerability["retentionPolicy"] = {"isLocked": False}
            snippet_object: SnippetObject = {
                "project_id": project_id,
                "service": GcpServices.CLOUD_STORAGE,
                "path": bucket_path,
                "vulnerable_property": "PROTECTION",
                "vulnerability": vulnerability,
            }

            locations = [
                Location(
                    access_patterns=("/vulnerability/retentionPolicy/isLocked",),
                    uri=(f"uri://{project_id}/{GcpServices.CLOUD_STORAGE.value}{bucket_path}"),
                    values=(False,),
                    method=method,
                ),
            ]

            vulnerabilities += build_vulnerabilities(
                locations=locations,
                method=method,
                gcp_response=snippet_object,
            )

    return (vulnerabilities, True)


@shield_cspm_gcp_decorator
async def logging_is_not_enabled_on_storage_bucket(
    credentials: GcpCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.LOGGING_IS_NOT_ENABLED_ON_STORAGE_BUCKET
    vulnerabilities: Vulnerabilities = ()

    buckets, project_id = run_gcp_client(get_storage_client_buckets, credentials)
    for bucket in buckets:
        bucket_path = bucket.path
        logging_lock_bucket = bucket.get_logging()

        if logging_lock_bucket is None:
            vulnerability = bucket._properties.copy()  # noqa: SLF001
            vulnerability["logging"] = {"logBucket": "some_bucket_id"}
            snippet_object: SnippetObject = {
                "project_id": project_id,
                "service": GcpServices.CLOUD_STORAGE,
                "path": bucket_path,
                "vulnerable_property": "LOGGING",
                "vulnerability": vulnerability,
            }

            locations = [
                Location(
                    access_patterns=("/vulnerability/logging/logBucket",),
                    uri=(f"uri://{project_id}/{GcpServices.CLOUD_STORAGE.value}{bucket_path}"),
                    values=("some_bucket_id",),
                    method=method,
                ),
            ]

            vulnerabilities += build_vulnerabilities(
                locations=locations,
                method=method,
                gcp_response=snippet_object,
            )

    return (vulnerabilities, True)


CHECKS: GcpMethodsTuple = (
    object_versioning_is_not_enabled,
    retention_policy_is_not_configured,
    logging_is_not_enabled_on_storage_bucket,
    lifecycle_is_not_defined,
)
