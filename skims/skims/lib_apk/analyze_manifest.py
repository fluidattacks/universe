from collections.abc import (
    Callable,
)

from bs4 import (
    BeautifulSoup,
    NavigableString,
    Tag,
)
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_apk.apk_utils import (
    SHIELD_BLOCKING,
    APKManifestCtx,
    Locations,
    calculate_execution_time,
    create_vulns,
)
from lib_apk.methods_enum import (
    MethodsEnumAPK as MethodsEnum,
)
from model.core import (
    FindingEnum,
    MethodExecutionResult,
)
from utils.string_handlers import (
    get_numbers,
)


def _add_android_manifest_location(
    *,
    apk_manifest: BeautifulSoup,
    method: MethodsEnum,
    locations: Locations,
    column: int,
    line: int,
    **desc_kwargs: str,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=apk_manifest.prettify(),
            viewport=SnippetViewport(
                column=column,
                line=line,
                wrap=True,
            ),
        ).content,
        **desc_kwargs,
    )


def _get_caseless_attr(tag: Tag | NavigableString, key: str, default: str) -> str:
    attr: str
    key = key.lower()
    for attr, value in tag.attrs.items():
        if attr.lower() == key:
            return value
    return default


def get_custom_permissions(manifest: BeautifulSoup) -> set[str]:
    perm_tags = manifest.find_all("permission")

    return {
        perm_name.lower()
        for perm_tag in perm_tags
        if (perm_name := _get_caseless_attr(perm_tag, "android:name", ""))
    }


@calculate_execution_time
def _backups_enabled(apk_manifest: BeautifulSoup, method: MethodsEnum) -> Locations:
    locations: Locations = Locations([])
    application: Tag | NavigableString | bool | None = None
    if application := apk_manifest.find("application"):
        allows_backup: str = _get_caseless_attr(
            application,
            key="android:allowBackup",
            default="not-set",
        ).lower()
        line, column = get_numbers(application)
        if allows_backup == "true":
            _add_android_manifest_location(
                apk_manifest=apk_manifest,
                method=method,
                locations=locations,
                column=column,
                line=line,
            )
        elif allows_backup == "not-set":
            _add_android_manifest_location(
                apk_manifest=apk_manifest,
                method=method,
                locations=locations,
                column=0,
                line=0,
            )

    return locations


@SHIELD_BLOCKING
def apk_backups_enabled(
    ctx: APKManifestCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.APK_BACKUPS_ENABLED
    locations, execution_time = _backups_enabled(ctx.apk_manifest, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


@calculate_execution_time
def _debugging_enabled(apk_manifest: BeautifulSoup, method: MethodsEnum) -> Locations:
    locations: Locations = Locations([])
    application: Tag | NavigableString | bool | None = None
    if application := apk_manifest.find("application"):
        is_debuggable: str = _get_caseless_attr(
            application,
            key="android:debuggable",
            default="false",
        ).lower()

        if is_debuggable == "true":
            line, column = get_numbers(application)
            _add_android_manifest_location(
                apk_manifest=apk_manifest,
                method=method,
                locations=locations,
                column=column,
                line=line,
            )

    return locations


@SHIELD_BLOCKING
def apk_debugging_enabled(
    ctx: APKManifestCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.APK_DEBUGGING_ENABLED
    locations, execution_time = _debugging_enabled(ctx.apk_manifest, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


@calculate_execution_time
def _exported_cp(apk_manifest: BeautifulSoup, method: MethodsEnum) -> Locations:
    locations: Locations = Locations([])
    permissions = get_custom_permissions(apk_manifest)

    for provider in apk_manifest.find_all("provider"):
        authority: str = _get_caseless_attr(
            provider,
            key="android:authorities",
            default="",
        ) or _get_caseless_attr(provider, key="android:name", default="")
        exported: str = _get_caseless_attr(
            provider,
            key="android:exported",
            default="no-value",
        ).lower()
        if exported == "no-value":
            _add_android_manifest_location(
                apk_manifest=apk_manifest,
                method=method,
                desc_authority=authority,
                locations=locations,
                column=provider.sourcepos,
                line=provider.sourceline,
            )
        elif exported == "true":
            prov_permission = _get_caseless_attr(provider, "android:permission", "").lower()

            grant_uri_permissions: str = _get_caseless_attr(
                provider,
                key="android:grantUriPermissions",
                default="false",
            ).lower()

            if grant_uri_permissions == "true" and prov_permission not in permissions:
                _add_android_manifest_location(
                    apk_manifest=apk_manifest,
                    method=method,
                    desc_authority=authority,
                    locations=locations,
                    column=provider.sourcepos,
                    line=provider.sourceline,
                )

    return locations


@SHIELD_BLOCKING
def apk_exported_cp(
    ctx: APKManifestCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.APK_EXPORTED_CP
    locations, execution_time = _exported_cp(ctx.apk_manifest, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


@calculate_execution_time
def _task_hijacking(apk_manifest: BeautifulSoup, target_sdk: int, method: MethodsEnum) -> Locations:
    locations: Locations = Locations([])
    for application in apk_manifest.find_all("application"):
        activity: Tag
        for activity in application.find_all("activity"):
            exported_act = activity.get("android:exported")
            task_affinity = activity.get("android:taskAffinity", None) or activity.get(
                "android:taskaffinity",
                "",
            )
            launch_mode: str = str(activity.get("android:launchMode", "")) or str(
                activity.get("android:launchmode", "standard"),
            )

            single_task_condition = bool(target_sdk < 28 and launch_mode in ("singleTask", "2"))

            if single_task_condition or (
                target_sdk < 29
                and exported_act == "true"
                and (launch_mode not in ("singleInstance", "3") or task_affinity != "")
            ):
                line, column = get_numbers(activity)
                _add_android_manifest_location(
                    apk_manifest=apk_manifest,
                    method=method,
                    locations=locations,
                    column=column,
                    line=line,
                    activity_name=str(activity.get("android:name", "")),
                )

    return locations


@SHIELD_BLOCKING
def apk_task_hijacking(
    ctx: APKManifestCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.APK_TASK_HIJACKING
    locations, execution_time = _task_hijacking(ctx.apk_manifest, ctx.target_sdk, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


@calculate_execution_time
def _clear_text_traffic(
    apk_manifest: BeautifulSoup,
    target_sdk: int,
    method: MethodsEnum,
) -> Locations:
    locations: Locations = Locations([])
    for application in apk_manifest.find_all("application"):
        if (
            _get_caseless_attr(
                application,
                "android:usesCleartextTraffic",
                "false" if target_sdk >= 28 else "true",
            )
            == "true"
        ):
            _add_android_manifest_location(
                apk_manifest=apk_manifest,
                method=method,
                locations=locations,
                column=application.sourcepos,
                line=application.sourceline,
            )

    return locations


@SHIELD_BLOCKING
def apk_clear_text_traffic(
    ctx: APKManifestCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.APK_CLEAR_TEXT_TRAFFIC
    locations, execution_time = _clear_text_traffic(ctx.apk_manifest, ctx.target_sdk, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


def _search_permission_in_manifest(apk_manifest: BeautifulSoup, permission: str) -> Tag | None:
    for permission_tag in apk_manifest.find_all("permission"):
        if permission_tag.get("android:name") == permission:
            return permission_tag
    return None


def _search_protection_level_in_manifest(
    apk_manifest: BeautifulSoup,
    permission_name: str,
) -> str | None:
    permission_tag = _search_permission_in_manifest(apk_manifest, permission_name)
    if permission_tag:
        permission_level = _get_caseless_attr(permission_tag, "android:protectionLevel", "")
        if permission_level:
            return permission_level.lower()
    return None


@calculate_execution_time
def _unprotected_exported_item(
    apk_manifest: BeautifulSoup,
    tag_name: str,
    method: MethodsEnum,
) -> Locations:
    locations: Locations = Locations([])
    for item in apk_manifest.find_all(tag_name):
        exported = _get_caseless_attr(item, "android:exported", "false").lower()
        permission_name = _get_caseless_attr(item, "android:permission", "")
        protection_level: str | None = None
        if permission_name:
            protection_level = _search_protection_level_in_manifest(apk_manifest, permission_name)

        if exported == "true" and (
            not permission_name
            or (protection_level is not None and protection_level != "signature")
        ):
            _add_android_manifest_location(
                apk_manifest=apk_manifest,
                method=method,
                locations=locations,
                column=item.sourcepos,
                line=item.sourceline,
                item_name=item.get("android:name"),
            )

    return locations


@SHIELD_BLOCKING
def apk_unprotected_exported_receivers(
    ctx: APKManifestCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.APK_UNPROTECTED_EXPORTED_RECEIVERS
    locations, execution_time = _unprotected_exported_item(ctx.apk_manifest, "receiver", method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


@SHIELD_BLOCKING
def apk_unprotected_exported_services(
    ctx: APKManifestCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.APK_UNPROTECTED_EXPORTED_SERVICES
    locations, execution_time = _unprotected_exported_item(ctx.apk_manifest, "service", method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


CHECKS: dict[
    FindingEnum,
    list[Callable[[APKManifestCtx], MethodExecutionResult]],
] = {
    FindingEnum.F055: [apk_backups_enabled],
    FindingEnum.F058: [apk_debugging_enabled],
    FindingEnum.F075: [apk_exported_cp],
    FindingEnum.F346: [apk_unprotected_exported_receivers, apk_unprotected_exported_services],
    FindingEnum.F347: [apk_task_hijacking],
    FindingEnum.F403: [apk_clear_text_traffic],
}
