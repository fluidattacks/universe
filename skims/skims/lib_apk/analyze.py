import os
import shutil
import time
from collections.abc import (
    Callable,
)
from concurrent.futures import (
    Future,
    ProcessPoolExecutor,
)
from contextlib import (
    suppress,
)
from typing import (
    Any,
)

import ctx
from androguard import (
    util,
)
from lib_apk.analyze_bytecodes import (
    CHECKS as BYTECODES_CHECKS,
)
from lib_apk.analyze_manifest import (
    CHECKS as MANIFEST_CHECKS,
)
from lib_apk.apk_utils import (
    APKContext,
    get_apk_context,
    get_bytecodes_ctx,
    get_manifest_ctx,
)
from lib_sast.path.analyze import (
    analyze as analyze_lib_path,
)
from lib_sast.root.analyze import (
    analyze as analyze_lib_root,
)
from loguru import (
    logger,
)
from model.core import (
    FindingEnum,
    MethodExecutionResult,
    SkimsConfig,
    Vulnerabilities,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from utils.fs import (
    resolve_paths,
)
from utils.logs import (
    log_blocking,
)
from utils.state import (
    ExecutionStores,
    get_ephemeral_store,
    get_vulnerability_ephemeral_store,
)

APK_CHECKS: tuple[
    tuple[
        Callable[[APKContext], Any],
        dict[
            FindingEnum,
            list[Callable[[Any], MethodExecutionResult]],
        ],
    ],
    ...,
] = (
    (get_bytecodes_ctx, BYTECODES_CHECKS),
    (get_manifest_ctx, MANIFEST_CHECKS),
)


def analyze_one(
    *,
    apk_path: str,
    config: SkimsConfig,
) -> list[MethodExecutionResult]:
    # Re-export config to gain visibility in child subprocesses
    ctx.SKIMS_CONFIG = config

    logger.disable("androguard")
    if config.debug:
        logger.enable("androguard")
        util.set_log("ERROR")

    apk_ctx: APKContext | None = get_apk_context(apk_path)
    if not apk_ctx:
        log_blocking("info", "Error decompiling %s APK", apk_path)
        return []

    results = []
    for get_check_ctx, checks in APK_CHECKS:
        apk_check_ctx = get_check_ctx(apk_ctx)
        if not apk_check_ctx:
            continue
        for finding, checks_list in checks.items():
            if finding not in ctx.SKIMS_CONFIG.checks:
                continue

            for method in checks_list:
                results.append(method(apk_check_ctx))  # noqa: PERF401

    return results


async def _execute_lib_sast_in_apk_files() -> Vulnerabilities:
    apk_path_vulns: Vulnerabilities
    total_paths = resolve_paths(
        working_dir=ctx.SKIMS_CONFIG.working_dir,
        include=(os.path.join("apk_files") + os.path.sep,),  # noqa: PTH118
        exclude=(),
    )

    vuln_stores = get_vulnerability_ephemeral_store()
    methods_stores = {"methods_results": get_ephemeral_store()}
    technique_stores = {"technique_times": get_ephemeral_store()}
    execution_stores = ExecutionStores(
        vuln_stores=vuln_stores,
        methods_stores=methods_stores,
        technique_stores=technique_stores,
    )
    analyze_lib_path(paths=total_paths, stores=execution_stores)
    await analyze_lib_root(
        paths=total_paths.ok_paths,
        stores=execution_stores,
        include_reachability=False,
    )
    path_vulns = execution_stores.vuln_stores.read_vulns()
    apk_path_vulns = tuple(
        vuln._replace(
            kind=VulnerabilityTechnique.DAST,
            vulnerability_type=VulnerabilityType.INPUTS,
            what=f"{vuln_path_parts[1]}.apk",
            where=(vuln_path_parts[2] + f"#{vuln.where}"),
        )
        for vuln in path_vulns
        if (vuln_path_parts := vuln.what.split("/", maxsplit=2)) and len(vuln_path_parts) == 3
    )
    return apk_path_vulns


def _store_results_callback(future: Future, stores: ExecutionStores) -> None:
    results: list[MethodExecutionResult] = future.result()
    for result in results:
        stores.vuln_stores.store_vulns(result.vulnerabilities)

        if result.method_name and result.time_coefficient:
            method_time = (result.method_name, result.time_coefficient)
            stores.methods_stores["methods_results"].store(method_time)


async def analyze(
    *,
    stores: ExecutionStores,
) -> None:
    init_time = time.time()
    paths = resolve_paths(
        working_dir=ctx.SKIMS_CONFIG.working_dir,
        include=ctx.SKIMS_CONFIG.apk.include,
        exclude=ctx.SKIMS_CONFIG.apk.exclude,
    )
    apk_paths = {path for path in paths.nv_paths if path.endswith("apk")}

    if not apk_paths or not any(
        finding in ctx.SKIMS_CONFIG.checks for _, checks in APK_CHECKS for finding in checks
    ):
        return

    log_blocking("info", "Analyzing %s APK files", len(apk_paths))

    with ProcessPoolExecutor(max_workers=ctx.CPU_CORES) as worker:
        for apk_path in apk_paths:
            future = worker.submit(
                analyze_one,
                apk_path=apk_path,
                config=ctx.SKIMS_CONFIG,
            )
            _store_results_callback(future, stores)

    if ctx.SKIMS_CONFIG.apk.use_sast_analysis:
        sast_vulnerabilities = await _execute_lib_sast_in_apk_files()
        stores.vuln_stores.store_vulns(sast_vulnerabilities)

        # Remove all decompressed apk files
        with suppress(FileNotFoundError):
            shutil.rmtree(os.path.join(os.getcwd(), "apk_files"))  # noqa: PTH109,PTH118

    apk_time = ("apk", time.time() - init_time)
    stores.technique_stores["technique_times"].store(apk_time)

    log_blocking("info", "APK analysis completed")
