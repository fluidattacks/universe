import textwrap
from collections.abc import (
    Callable,
    Iterable,
)
from operator import (
    attrgetter,
)

from androguard.core.analysis.analysis import (
    Analysis,
    MethodAnalysis,
)
from androguard.core.apk import (
    APK,
    FileNotPresent,
)
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_apk.apk_utils import (
    SHIELD_BLOCKING,
    APKBytecodesCtx,
    Locations,
    calculate_execution_time,
    create_vulns,
)
from lib_apk.methods_enum import (
    MethodsEnumAPK as MethodsEnum,
)
from model.core import (
    FindingEnum,
    MethodExecutionResult,
)


def is_method_present(
    analysis: Analysis,
    class_name: str,
    method: str,
    descriptor: str,
) -> list[str] | None:
    """Search if method is present in decompiled code."""
    met_ana: MethodAnalysis = analysis.get_method_analysis_by_name(
        class_name=class_name,
        method_name=method,
        method_descriptor=descriptor,
    )
    if not met_ana:
        return []

    return [x.name for x, _, _ in met_ana.get_xref_from() if "Activity" in x.name]


def get_activities_source(dvms: list) -> str:
    """Decompile given Dalvik VM images."""
    source = [x.get_source() for dvm in dvms for x in dvm.get_classes() if "Activity" in x.name]
    return "".join(source)


def _add_apk_unsigned_not_signed_location(
    apk_path: str,
    locations: Locations,
    method: MethodsEnum,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                >>> Using the androguard library for python to parse APK
                >>> apk = APK({apk_path!r})

                >>> Check whether any signature schema version exists
                >>> in the .apk (v1, v2, v3)
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=16, wrap=True),
        ).content,
    )


@calculate_execution_time
def _apk_unsigned(apk_obj: APK, apk_path: str, method: MethodsEnum) -> Locations:
    locations: Locations = Locations([])

    if not (apk_obj.is_signed_v1() or apk_obj.is_signed_v2() or apk_obj.is_signed_v3()):
        _add_apk_unsigned_not_signed_location(apk_path, locations, method)

    return locations


@SHIELD_BLOCKING
def apk_unsigned(ctx: APKBytecodesCtx) -> MethodExecutionResult:
    method = MethodsEnum.APK_UNSIGNED
    locations, execution_time = _apk_unsigned(ctx.apk_obj, ctx.apk_path, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


def _get_method_names(analysis: Analysis) -> list[str]:
    names: list[str] = sorted(set(map(attrgetter("name"), analysis.get_methods())))

    return names


def _add_has_fragment_injection_location(
    apk_path: str,
    locations: Locations,
    source: str,
    target_sdk_version: int,
    method: MethodsEnum,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                >>> Using the androguard library for python
                >>> to parse APK and all Dalvik Executables
                >>> apk = APK({apk_path!r})
                >>> apk.get_all_dex()
                >>> dalvik = DEX(dex, using_api=api_target)

                >>> Get the method names from all classes in each DEX file
                >>> Get the targetSdkVersion attribute
                >>> apk.get_target_sdk_version {target_sdk_version!r}
                >>> Search the dangerous method PreferenceActivity
                >>> {source!r}
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=10, wrap=True),
        ).content,
    )


@calculate_execution_time
def _has_fragment_injection(
    apk_obj: APK,
    apk_analysis: Analysis,
    apk_path: str,
    method: MethodsEnum,
) -> Locations:
    locations: Locations = Locations([])
    sdk_version = apk_obj.get_target_sdk_version()
    target_sdk_version = int(sdk_version) if sdk_version else 0

    act_source = get_activities_source(apk_analysis.vms)

    is_vulnerable: bool = target_sdk_version < 19 and "PreferenceActivity" in act_source

    if is_vulnerable:
        _add_has_fragment_injection_location(
            apk_path,
            locations,
            act_source,
            target_sdk_version,
            method,
        )

    return locations


@SHIELD_BLOCKING
def has_fragment_injection(ctx: APKBytecodesCtx) -> MethodExecutionResult:
    method = MethodsEnum.HAS_FRAGMENT_INJECTION
    if not ctx.analysis:
        return MethodExecutionResult(vulnerabilities=())

    locations, execution_time = _has_fragment_injection(
        ctx.apk_obj,
        ctx.analysis,
        ctx.apk_path,
        method,
    )

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


def _add_webview_allows_resource_access(
    apk_path: str,
    locations: Locations,
    source: Iterable[str],
    method: MethodsEnum,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                >>> Using the androguard library for python
                >>> to parse APK and all Dalvik Executables
                >>> apk = APK({apk_path!r})
                >>> apk.get_all_dex()
                >>> dalvik = DEX(dex, using_api=api_target)

                >>> Get the method names from all classes in each DEX file
                >>> sorted(set(method.name for method in dex.get_methods()))
                >>> Search if setJavaScriptEnabled method is enabled
                >>> and the following dangerous methods are enabled:
                >>> {source!r}
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=14, wrap=True),
        ).content,
    )


@calculate_execution_time
def _webview_vulnerabilities(
    apk_analysis: Analysis,
    apk_path: str,
    method: MethodsEnum,
) -> Locations:
    locations: Locations = Locations([])
    dangerous_allows = {
        "setAllowContentAccess",
        "setAllowFileAccess",
        "setAllowFileAccessFromFileURLs",
        "setAllowUniversalAccessFromFileURLs",
    }

    act_source = get_activities_source(apk_analysis.vms)

    if (
        ("setJavaScriptEnabled(true)" in act_source or "setJavaScriptEnabled(1)" in act_source)
        and (danger_methods := [method for method in dangerous_allows if method in act_source])
        and len(danger_methods) > 0
    ):
        _add_webview_allows_resource_access(apk_path, locations, danger_methods, method)

    return locations


@SHIELD_BLOCKING
def webview_vulnerabilities(ctx: APKBytecodesCtx) -> MethodExecutionResult:
    method = MethodsEnum.WEBVIEW_VULNERABILITIES
    if not ctx.analysis:
        return MethodExecutionResult(vulnerabilities=())

    locations, execution_time = _webview_vulnerabilities(ctx.analysis, ctx.apk_path, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


def _add_not_verifies_ssl_hostname(
    apk_path: str,
    locations: Locations,
    source: str,
    method: MethodsEnum,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                >>> Using the androguard library for python
                >>> to parse APK and all Dalvik Executables
                >>> apk = APK({apk_path!r})
                >>> apk.get_all_dex()
                >>> dalvik = DEX(dex, using_api=api_target)

                >>> Get the activities classes for each DEX found in the APK
                >>> dvm.get_classes()
                >>> Search SSLSocket without getDefaultHostnameVerifier
                >>> {source!r}
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=12, wrap=True),
        ).content,
    )


@calculate_execution_time
def _not_verifies_ssl_hostname(
    apk_analysis: Analysis,
    apk_path: str,
    method: MethodsEnum,
) -> Locations:
    locations: Locations = Locations([])

    if (
        (act_source := get_activities_source(apk_analysis.vms))
        and "SSLSocket" in act_source
        and "getDefaultHostnameVerifier" not in act_source
    ):
        _add_not_verifies_ssl_hostname(apk_path, locations, act_source, method)

    return locations


@SHIELD_BLOCKING
def not_verifies_ssl_hostname(ctx: APKBytecodesCtx) -> MethodExecutionResult:
    method = MethodsEnum.NOT_VERIFIES_SSL_HOSTNAME
    if not ctx.analysis:
        return MethodExecutionResult(vulnerabilities=())

    locations, execution_time = _not_verifies_ssl_hostname(ctx.analysis, ctx.apk_path, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


def _add_allow_user_ca_by_default(apk_path: str, locations: Locations, method: MethodsEnum) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                >>> Using the androguard library for python to parse APK
                >>> apk = APK({apk_path!r})

                >>> No network security config file found and SDK version
                >>> allows user-supplied CAs by default
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=10, wrap=True),
        ).content,
    )


def _add_allow_user_ca(
    apk_path: str,
    locations: Locations,
    net_confs: str,
    method: MethodsEnum,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                >>> Using the androguard library for python to parse APK
                >>> apk = APK({apk_path!r})

                >>> Get network security config file
                >>> apk.get_file("res/xml/network_security_config.xml")
                >>> Search if the file contains trust anchors and user methods
                >>> {net_confs!r}
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=11, wrap=True),
        ).content,
    )


@calculate_execution_time
def _improper_certificate_validation(apk_obj: APK, apk_path: str, method: MethodsEnum) -> Locations:
    locations: Locations = Locations([])
    net_conf = ""
    try:
        net_conf = str(apk_obj.get_file("res/xml/network_security_config.xml"))
    except FileNotPresent:
        return locations
    if "trust-anchors" in net_conf and "user" in net_conf:
        _add_allow_user_ca(apk_path, locations, net_conf, method)

    return locations


@SHIELD_BLOCKING
def improper_certificate_validation(
    ctx: APKBytecodesCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.IMPROPER_CERTIFICATE_VALIDATION
    locations, execution_time = _improper_certificate_validation(ctx.apk_obj, ctx.apk_path, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


@calculate_execution_time
def _improper_certificate_validation_default(
    apk_obj: APK,
    apk_path: str,
    method: MethodsEnum,
) -> Locations:
    locations: Locations = Locations([])
    try:
        apk_obj.get_file("res/xml/network_security_config.xml")
    except FileNotPresent:
        sdk_version = apk_obj.get_target_sdk_version()
        target_sdk = int(sdk_version) if sdk_version else 0
        if target_sdk < 24:
            _add_allow_user_ca_by_default(apk_path, locations, method)

    return locations


@SHIELD_BLOCKING
def improper_certificate_validation_default(
    ctx: APKBytecodesCtx,
) -> MethodExecutionResult:
    method = MethodsEnum.IMPROPER_CERTIFICATE_VALIDATION_DEFAULT
    locations, execution_time = _improper_certificate_validation_default(
        ctx.apk_obj,
        ctx.apk_path,
        method,
    )

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


def _add_socket_uses_get_insecure(
    apk_path: str,
    locations: Locations,
    methods: Iterable[str],
    method: MethodsEnum,
) -> None:
    locations.append(
        method=method,
        snippet=make_snippet(
            content=textwrap.dedent(
                f"""
                >>> Using the androguard library for python
                >>> to parse APK and all Dalvik Executables
                >>> apk = APK({apk_path!r})
                >>> apk.get_all_dex()
                >>> dalvik = DEX(dex, using_api=api_target)

                >>> Get the method names from all classes in each .dex file
                >>> sorted(set(method.name for method in dex.get_methods()))
                >>> Search for presence of methods that use insecure sockets
                >>> {methods!r}
                """,
            )[1:],
            viewport=SnippetViewport(column=0, line=12, wrap=True),
        ).content,
    )


@calculate_execution_time
def _uses_insecure_sockets(apk_analysis: Analysis, apk_path: str, method: MethodsEnum) -> Locations:
    locations: Locations = Locations([])
    method_names: list[str] = _get_method_names(apk_analysis)

    uses_get_insecure = is_method_present(
        apk_analysis,
        "Landroid/net/SSLCertificateSocketFactory;",
        "getInsecure",
        ("(I Landroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;"),
    )

    if uses_get_insecure:
        _add_socket_uses_get_insecure(apk_path, locations, method_names, method)

    return locations


@SHIELD_BLOCKING
def uses_insecure_sockets(ctx: APKBytecodesCtx) -> MethodExecutionResult:
    method = MethodsEnum.SOCKET_USES_GET_INSECURE
    if not ctx.analysis:
        return MethodExecutionResult(vulnerabilities=())

    locations, execution_time = _uses_insecure_sockets(ctx.analysis, ctx.apk_path, method)

    return create_vulns(
        apk_path=ctx.apk_path,
        locations=locations,
        method=method,
        execution_time=execution_time,
    )


CHECKS: dict[
    FindingEnum,
    list[Callable[[APKBytecodesCtx], MethodExecutionResult]],
] = {
    FindingEnum.F060: [not_verifies_ssl_hostname],
    FindingEnum.F082: [uses_insecure_sockets],
    FindingEnum.F103: [apk_unsigned],
    FindingEnum.F268: [webview_vulnerabilities],
    FindingEnum.F313: [
        improper_certificate_validation,
        improper_certificate_validation_default,
    ],
    FindingEnum.F398: [has_fragment_injection],
}
