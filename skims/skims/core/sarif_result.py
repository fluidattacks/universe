from pathlib import (
    Path,
)
from typing import (
    Any,
)

import ctx
import sarif_om
import yaml
from ctx import (
    CRITERIA_REQUIREMENTS,
    CRITERIA_VULNERABILITIES,
)
from fluidattacks_core.serializers.snippet import (
    Snippet,
)
from model.core import (
    Vulnerability,
    VulnerabilityTechnique,
    get_advisory_info,
)
from utils.cvss import (
    get_exploitability_score,
)
from utils.repositories import (
    get_repo_branch,
    get_repo_head_hash,
    get_repo_remote,
)
from utils.state import (
    EphemeralStore,
    VulnerabilitiesEphemeralStore,
)


def simplify_sarif(obj: Any) -> dict[str, str]:  # noqa: ANN401
    simplified_obj: Any
    if hasattr(obj, "__attrs_attrs__"):
        simplified_obj = {
            attribute.metadata["schema_property_name"]: simplify_sarif(
                obj.__dict__[attribute.name],
            )
            for attribute in obj.__attrs_attrs__
            if obj.__dict__[attribute.name] != attribute.default
        }
    elif isinstance(obj, dict):
        simplified_obj = {key: simplify_sarif(value) for key, value in obj.items()}
    elif isinstance(obj, (list | tuple | set)):
        simplified_obj = [simplify_sarif(item) for item in obj]
    else:
        simplified_obj = obj
    return simplified_obj


def _get_criteria_vulns() -> dict[str, Any]:
    with Path(CRITERIA_VULNERABILITIES).open(encoding="utf-8") as handle:
        return yaml.safe_load(handle)


def _get_criteria_requirements() -> dict[str, Any]:
    with Path(CRITERIA_REQUIREMENTS).open(encoding="utf-8") as handle:
        return yaml.safe_load(handle)


CRITERIA_VULNS = _get_criteria_vulns()


CRITERIA_REQS = _get_criteria_requirements()


def _get_level(vulnerability: Vulnerability) -> str:
    base_score = get_exploitability_score(
        str(vulnerability.skims_metadata.cvss),
    )
    level: str = "note"
    if base_score >= 7.0:
        level = "error"
    elif 4.0 <= base_score < 7.0:
        level = "warning"
    return level


def _get_rule(vuln_id: str) -> sarif_om.ReportingDescriptor:
    content = CRITERIA_VULNS[vuln_id]

    return sarif_om.ReportingDescriptor(
        id=vuln_id,
        name=content["en"]["title"],
        full_description=sarif_om.MultiformatMessageString(
            text=content["en"]["description"],
        ),
        help_uri=(
            "https://help.fluidattacks.com/portal/en/kb/articles/"
            f"criteria-vulnerabilities-{vuln_id}"
        ),
        help=sarif_om.MultiformatMessageString(
            text=content["en"]["recommendation"],
        ),
        properties={"auto_approve": True},
    )


def _get_taxa(requirement_id: str) -> sarif_om.ReportingDescriptor:
    content = CRITERIA_REQS[requirement_id]
    return sarif_om.ReportingDescriptor(
        id=requirement_id,
        name=content["en"]["title"],
        short_description=sarif_om.MultiformatMessageString(
            text=content["en"]["summary"],
        ),
        full_description=sarif_om.MultiformatMessageString(
            text=content["en"]["description"],
        ),
        help_uri=(
            "https://help.fluidattacks.com/portal/en/kb/articles/"
            f"criteria-requirements-{requirement_id}"
        ),
    )


def _rule_is_present(base: sarif_om.SarifLog, rule_id: str) -> bool:
    return any(rule.id == rule_id for rule in base.runs[0].tool.driver.rules)


def _taxa_is_present(base: sarif_om.SarifLog, taxa_id: str) -> bool:
    return any(rule.id == taxa_id for rule in base.runs[0].taxonomies[0].taxa)


def _get_sca_properties(vulnerability: Vulnerability) -> dict[str, Any]:
    properties: dict[str, Any] = {}
    if vulnerability.kind == VulnerabilityTechnique.SCA and (
        sca_info := get_advisory_info(vulnerability)
    ):
        properties = sca_info

    return properties


def _get_text_description(vulnerability: Vulnerability) -> str:
    return vulnerability.skims_metadata.description if vulnerability.skims_metadata else ""


def _get_text_snippet(vulnerability: Vulnerability) -> str:
    if vulnerability.skims_metadata:
        if isinstance(vulnerability.skims_metadata.snippet, str):
            return vulnerability.skims_metadata.snippet
        return vulnerability.skims_metadata.snippet.content
    return ""


def _get_context_region(vulnerability: Vulnerability) -> sarif_om.Region:
    if (
        (snippet := vulnerability.skims_metadata.snippet)
        and isinstance(snippet, Snippet)
        and snippet.line is not None
    ):
        region = sarif_om.Region(
            start_line=max(snippet.line - snippet.line_context, 1),
            end_line=snippet.line + snippet.line_context,
            snippet=sarif_om.ArtifactContent(
                rendered={"text": _get_text_snippet(vulnerability)},
                text=snippet.content,
            ),
            start_column=1,
            char_length=snippet.columns_per_line,
            properties={
                "offset": snippet.offset,
                "line": snippet.line,
                "column": snippet.column,
                "line_context": snippet.line_context,
                "wrap": snippet.wrap,
                "show_line_numbers": snippet.show_line_numbers,
                "highlight_line_number": snippet.highlight_line_number,
            },
        )
    else:
        region = sarif_om.Region()

    return region


def get_reachability_info(
    vulnerability: Vulnerability,
) -> dict[str, Any]:
    sca_metadata = vulnerability.skims_metadata.sca_metadata
    if not sca_metadata or not sca_metadata.pkg_id:
        return {}

    return {
        "reachability_info": {
            "package": sca_metadata.package,
            "vulnerable_version": sca_metadata.vulnerable_version,
            "cve": sca_metadata.cve,
            "pkg_id": sca_metadata.pkg_id,
        },
    }


def is_new_sca_report(vuln: Vulnerability) -> dict[str, bool]:
    if vuln.skims_metadata.sca_metadata and vuln.skims_metadata.sca_metadata.is_new_sca_report:
        return {"is_sca_new": True}
    return {}


def _get_properties(vulnerability: Vulnerability) -> dict[str, Any]:
    return {
        "cwe_ids": vulnerability.skims_metadata.cwe_ids,
        "cvss": vulnerability.skims_metadata.cvss,
        "cvss_v4": vulnerability.skims_metadata.cvss_v4,
        "kind": vulnerability.kind.value,
        "technique": vulnerability.kind.value,
        "vulnerability_type": vulnerability.vulnerability_type.value,
        "method_developer": (vulnerability.skims_metadata.developer.value),
        "source_method": (vulnerability.skims_metadata.source_method),
        "auto_approve": vulnerability.skims_metadata.auto_approve,
        "stream": vulnerability.stream,
        **get_reachability_info(vulnerability),
        **(
            vulnerability.skims_metadata.http_properties._asdict()
            if vulnerability.skims_metadata.http_properties is not None
            else {}
        ),
        **is_new_sca_report(vulnerability),
    }


def _get_web_responses(
    requests_stores: dict[str, EphemeralStore],
) -> list[sarif_om.WebResponse]:
    sarif_web_responses: list[sarif_om.WebResponse] = [
        sarif_om.WebResponse(
            no_response_received=response_info["status_code"] > 400,
            properties={
                "url": response_info["url"],
            },
            status_code=response_info["status_code"],
        )
        for web_responses in requests_stores["web_responses"].iterate()
        for response_info in web_responses
    ]

    for ssl_responses in requests_stores["ssl_responses"].iterate():
        for domain, ssl_status in ssl_responses.items():
            sarif_web_responses.append(
                sarif_om.WebResponse(
                    properties={
                        "url": domain,
                        "ssl_status": ssl_status,
                    },
                ),
            )

    return sarif_web_responses


def _get_cspm_method_errors(
    cspm_error_stores: dict[str, EphemeralStore],
) -> list[sarif_om.WebResponse]:
    aws_methods_problems = []
    for aws_problems in cspm_error_stores["aws_permissions_problems"].iterate():
        aws_methods_problems += list(aws_problems)

    aws_methods = []
    for aws_errors in cspm_error_stores["aws_errors"].iterate():
        aws_methods += list(aws_errors)

    azure_methods = []
    for azure_errors in cspm_error_stores["azure_errors"].iterate():
        azure_methods += list(azure_errors)

    gcp_methods = []
    for gcp_errors in cspm_error_stores["gcp_errors"].iterate():
        gcp_methods += list(gcp_errors)

    return [
        sarif_om.WebResponse(
            properties={
                "error_methods": {
                    "aws_permissions_problems": aws_methods_problems,
                    "aws": aws_methods,
                    "azure": azure_methods,
                    "gcp": gcp_methods,
                },
                "url": "cspm_methods",
            },
        ),
    ]


def append_taxonomies(
    base: sarif_om.SarifLog,
    rule_id: str,
) -> list[sarif_om.ReportingDescriptorReference]:
    for taxa_id in CRITERIA_VULNS[rule_id]["requirements"]:
        if not _taxa_is_present(base, taxa_id):
            base.runs[0].taxonomies[0].taxa.append(_get_taxa(taxa_id))
    return [
        sarif_om.ReportingDescriptorReference(
            id=taxa_id,
            tool_component=sarif_om.ToolComponentReference(name="criteria"),
        )
        for taxa_id in CRITERIA_VULNS[rule_id]["requirements"]
    ]


def _get_region(vulnerability: Vulnerability) -> sarif_om.Region:
    try:
        line_number = int(vulnerability.where)
        return sarif_om.Region(
            message=sarif_om.Message(text=vulnerability.where),
            snippet=sarif_om.MultiformatMessageString(
                text=_get_text_snippet(vulnerability),
            ),
            start_line=max(line_number, 1),
        )
    except ValueError:
        return sarif_om.Region(
            message=sarif_om.Message(text=vulnerability.where),
            snippet=sarif_om.MultiformatMessageString(
                text=_get_text_snippet(vulnerability),
            ),
        )


def _get_sarif(
    stores: VulnerabilitiesEphemeralStore,
    requests_stores: dict[str, EphemeralStore] | None = None,
    cspm_error_stores: dict[str, EphemeralStore] | None = None,
) -> sarif_om.SarifLog:
    base = sarif_om.SarifLog(
        version="2.1.0",
        schema_uri=("https://schemastore.azurewebsites.net/schemas/json/sarif-2.1.0-rtm.4.json"),
        runs=[
            sarif_om.Run(
                tool=sarif_om.Tool(
                    driver=sarif_om.ToolComponent(
                        name="skims",
                        rules=[],
                        version="1.0.0",
                        semantic_version="1.0.0",
                    ),
                ),
                results=[],
                version_control_provenance=[
                    sarif_om.VersionControlDetails(
                        repository_uri=get_repo_remote(
                            ctx.SKIMS_CONFIG.working_dir,
                        ),
                        revision_id=get_repo_head_hash(
                            ctx.SKIMS_CONFIG.working_dir,
                        ),
                        branch=get_repo_branch(ctx.SKIMS_CONFIG.working_dir),
                    ),
                ],
                taxonomies=[
                    sarif_om.ToolComponent(
                        name="criteria",
                        version="1",
                        information_uri=(
                            "https://help.fluidattacks.com/portal/en/kb/"
                            "articles/criteria/requirements"
                        ),
                        organization="Fluidattacks",
                        short_description=sarif_om.MultiformatMessageString(
                            text="The fluidattacks security requirements",
                        ),
                        taxa=[],
                        is_comprehensive=False,
                    ),
                ],
                original_uri_base_ids={
                    "SRCROOT": sarif_om.ArtifactLocation(
                        uri=ctx.SKIMS_CONFIG.namespace,
                    ),
                },
                web_responses=[],
            ),
        ],
    )

    for check in ctx.SKIMS_CONFIG.checks:
        base.runs[0].tool.driver.rules.append(
            _get_rule(check.name.replace("F", "")),
        )

    for vulnerability in stores.read_vulns():
        # remove F from findings
        rule_id = vulnerability.finding.name.replace("F", "")

        result = sarif_om.Result(
            rule_id=rule_id,
            level=_get_level(vulnerability),
            message=sarif_om.MultiformatMessageString(
                text=_get_text_description(vulnerability),
                properties=_get_sca_properties(vulnerability),
            ),
            locations=[
                sarif_om.Location(
                    physical_location=sarif_om.PhysicalLocation(
                        artifact_location=sarif_om.ArtifactLocation(
                            uri=vulnerability.extract_path,
                        ),
                        region=_get_region(vulnerability),
                        context_region=_get_context_region(vulnerability),
                    ),
                ),
            ],
            taxa=[],
            properties=_get_properties(vulnerability),
        )
        result.fingerprints = {"guid": str(vulnerability.digest)}

        # append rule if not is present
        if not _rule_is_present(base, rule_id):
            base.runs[0].tool.driver.rules.append(_get_rule(rule_id))

        # append requirement if not is present
        result.taxa = append_taxonomies(base, rule_id)
        if not vulnerability.skims_metadata.skip:
            base.runs[0].results.append(result)

    web_responses = _get_web_responses(requests_stores) if requests_stores else []
    cspm_errors = _get_cspm_method_errors(cspm_error_stores) if cspm_error_stores else []

    base.runs[0].web_responses = web_responses + cspm_errors

    return base


def get_sarif(
    vulns_stores: VulnerabilitiesEphemeralStore,
    requests_stores: dict[str, EphemeralStore] | None = None,
    cspm_error_stores: dict[str, EphemeralStore] | None = None,
) -> dict[str, str]:
    return simplify_sarif(
        _get_sarif(vulns_stores, requests_stores, cspm_error_stores),
    )
