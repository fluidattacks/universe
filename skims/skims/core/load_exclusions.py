import configparser
import os
from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

import confuse
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
)


class DastExclusion(NamedTuple):
    endpoint: str
    target_findings: dict[str, str]


class ScaExclusion(NamedTuple):
    dependency_name: str
    reason: str
    date_limit: datetime | None = None


class SkimsExclusions(NamedTuple):
    dast: list[DastExclusion]
    sca: list[ScaExclusion]


class SkimsExclusionsOld(NamedTuple):
    dast: dict[str, list[str]]
    sca: list[str]


def _get_dast_exclusions(dast_section: dict[str, str]) -> dict[str, list[str]]:
    excluded_endpoints: dict[str, list[str]] = {}
    for exclusion_def in dast_section:
        exclusion = exclusion_def.rsplit("/", 1)
        if len(exclusion) != 2:
            continue
        excluded_endpoints.setdefault(exclusion[0], []).append(exclusion[1])
    return excluded_endpoints


def get_exclusions_old_version(file_path: str) -> SkimsExclusionsOld | None:
    config = configparser.ConfigParser(strict=False)
    config.read(file_path)
    return SkimsExclusionsOld(
        dast=_get_dast_exclusions(dict(config["DAST"])) if "DAST" in config else {},
        sca=list(config["SCA"].keys()) if "SCA" in config else [],
    )


def _build_config(config_path: str) -> confuse.Configuration:
    template = confuse.Configuration("skims", read=False)
    template.set_file(config_path)
    template.read(user=False, defaults=False)
    return template.get(
        confuse.Template(
            {
                "dast": confuse.Template(
                    confuse.Sequence(
                        {
                            "target_findings": confuse.Sequence(
                                confuse.Template(
                                    {
                                        confuse.String(): confuse.String(),
                                    },
                                ),
                            ),
                            "endpoint": confuse.String(),
                        },
                    ),
                ),
                "sca": confuse.Template(
                    confuse.Sequence(
                        {
                            "dependency_name": confuse.String(),
                            "endpoint": confuse.String(),
                            "reason": confuse.String(),
                            "date_limit": confuse.Optional(confuse.String()),
                        },
                    ),
                ),
            },
        ),
    )


def _convert_date(date_str: str | None) -> datetime | None:
    if not date_str:
        return None
    try:
        return datetime.strptime(str(date_str), "%Y-%m-%d")  # noqa: DTZ007
    except ValueError:
        return None


def load_exclusions(config_path: str) -> SkimsExclusions | None:
    config = _build_config(config_path)

    try:
        exclusions_dast = config.pop("dast", {})
        exclusions_sca = config.pop("sca", {})

        return SkimsExclusions(
            dast=[
                DastExclusion(
                    endpoint=exclusion["endpoint"],
                    target_findings={
                        key: value
                        for sequence in exclusion["target_findings"]
                        for key, value in sequence.items()
                    },
                )
                for exclusion in exclusions_dast
            ],
            sca=[
                ScaExclusion(
                    dependency_name=exclusion["dependency_name"],
                    reason=exclusion["reason"],
                    date_limit=_convert_date(exclusion.get("date_limit")),
                )
                for exclusion in exclusions_sca
            ],
        )
    except KeyError as exc:
        log_blocking("error", "Missing key %s in an exclusion", exc)
    return None


def get_fluidattacks_exclusions(
    working_dir: str,
) -> SkimsExclusions | SkimsExclusionsOld | None:
    try:
        exclusions_old_path = os.path.join(working_dir, ".fluidattacks")  # noqa: PTH118
        if os.path.isfile(exclusions_old_path):  # noqa: PTH113
            return get_exclusions_old_version(exclusions_old_path)

        exclusions_path = os.path.join(working_dir, ".fluidattacks.yaml")  # noqa: PTH118
        if os.path.isfile(exclusions_path):  # noqa: PTH113
            return load_exclusions(exclusions_path)
    except Exception as err:  # noqa: BLE001
        msg = f"Failed to determine exclusions {err}"
        log_blocking("error", msg)
        log_to_remote_blocking(msg=msg, severity="error")
    return None
