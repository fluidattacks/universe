import os
import sys
import time
from collections import (
    Counter,
)

import aioboto3
import botocore
import ctx
from core.load_exclusions import (
    SkimsExclusions,
    SkimsExclusionsOld,
    get_fluidattacks_exclusions,
)
from core.results import (
    report_results,
)
from lib_apk.analyze import (
    analyze as analyze_apk,
)
from lib_cspm.aws.analyze import (
    analyze as analyze_aws,
)
from lib_cspm.azure.analyze import (
    analyze as analyze_azure,
)
from lib_cspm.gcp.analyze import (
    analyze as analyze_gcp,
)
from lib_dast.http.analyze import (
    analyze as analyze_http,
)
from lib_dast.ssl.analyze import (
    analyze as analyze_ssl,
)
from lib_sast.analyze import (
    analyze as analyze_sast,
)
from lib_sca.analyze import (
    analyze as analyze_sca,
)
from model.core import (
    AwsCredentials,
    AwsRole,
)
from utils.cloudwatch import (
    send_metrics_to_cloudwatch,
)
from utils.env import (
    is_fluid_batch_env,
)
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
)
from utils.repositories import (
    DEFAULT_COMMIT,
    get_repo_head_hash,
)
from utils.state import (
    EphemeralStore,
    ExecutionStores,
    VulnerabilitiesEphemeralStore,
    get_ephemeral_store,
    get_vulnerability_ephemeral_store,
)
from utils.state import (
    reset as reset_ephemeral_state,
)
from utils.statics import (
    calculate_methods_averages,
)
from utils.string_handlers import (
    filter_valid_urls,
    get_datetime_with_offset,
    get_duration,
)
from utils.string_handlers import (
    split_on_last_dot as split_last,
)


def get_exclusions() -> tuple[dict[str, list[str]] | None, list[str] | None]:
    if not ctx.SKIMS_CONFIG.sca and not ctx.SKIMS_CONFIG.dast:
        return None, None

    exclusions = get_fluidattacks_exclusions(ctx.SKIMS_CONFIG.working_dir)
    if exclusions and isinstance(exclusions, SkimsExclusionsOld):
        return exclusions.dast, exclusions.sca

    if exclusions and isinstance(exclusions, SkimsExclusions):
        sca_excluded_deps = [dep.dependency_name for dep in exclusions.sca]
        dast_exclusions: dict[str, list[str]] = {
            exclusion.endpoint: list(exclusion.target_findings.keys())
            for exclusion in exclusions.dast
        }
        return dast_exclusions, sca_excluded_deps
    return None, None


async def get_credentials(credentials: AwsRole) -> AwsCredentials | None:
    try:
        async with aioboto3.Session().client(service_name="sts") as sts_client:
            response = await sts_client.assume_role(
                ExternalId=credentials.external_id,
                RoleArn=credentials.role,
                RoleSessionName="FluidAttacksRoleVerification",
            )
            aws_credentials = AwsCredentials(
                access_key_id=response["Credentials"]["AccessKeyId"],
                secret_access_key=response["Credentials"]["SecretAccessKey"],
                session_token=response["Credentials"]["SessionToken"],
            )
    except botocore.exceptions.ClientError:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        log_blocking(
            "error",
            f"{exc_value}",
        )
        log_to_remote_blocking(
            msg=(exc_type, exc_value, exc_traceback),
            severity="error",
        )
        return None
    else:
        return aws_credentials


async def analyze_cspm(
    execution_stores: ExecutionStores,
    error_stores: dict[str, EphemeralStore],
) -> None:
    if not ctx.SKIMS_CONFIG.cspm:
        return

    init_time = time.time()

    log_blocking("info", "Running CSPM analysis")

    for aws_cred in ctx.SKIMS_CONFIG.cspm.aws_credentials:
        credentials = (
            aws_cred if isinstance(aws_cred, AwsCredentials) else await get_credentials(aws_cred)
        )
        if credentials and credentials.access_key_id:
            await analyze_aws(
                credentials=credentials,
                stores=execution_stores.vuln_stores,
                error_stores=error_stores,
            )

    for gcp_cred in ctx.SKIMS_CONFIG.cspm.gcp_credentials:
        if gcp_cred and gcp_cred.private_key:
            await analyze_gcp(
                credentials=gcp_cred,
                stores=execution_stores.vuln_stores,
                error_stores=error_stores,
            )

    for azure_cred in ctx.SKIMS_CONFIG.cspm.azure_credentials:
        if azure_cred and azure_cred.client_id:
            await analyze_azure(
                credentials=azure_cred,
                stores=execution_stores.vuln_stores,
                error_stores=error_stores,
            )

    cspm_time = ("cspm", time.time() - init_time)
    execution_stores.technique_stores["technique_times"].store(cspm_time)


async def analyze_dast(
    execution_stores: ExecutionStores,
    requests_stores: dict[str, EphemeralStore],
    dast_exclusions: dict[str, list[str]] | None,
) -> None:
    if not (ctx.SKIMS_CONFIG.dast and ctx.SKIMS_CONFIG.dast.urls):
        return

    init_time = time.time()

    valid_urls = set(filter_valid_urls(ctx.SKIMS_CONFIG.dast.urls))
    log_blocking("info", "Running DAST analysis on %s valid urls", len(valid_urls))
    if ctx.SKIMS_CONFIG.dast.ssl_checks:
        await analyze_ssl(
            vuln_stores=execution_stores.vuln_stores,
            requests_stores=requests_stores,
            urls=valid_urls,
            exclusions=dast_exclusions,
        )
    if ctx.SKIMS_CONFIG.dast.http_checks:
        await analyze_http(
            vuln_stores=execution_stores.vuln_stores,
            requests_stores=requests_stores,
            urls=valid_urls,
            exclusions=dast_exclusions,
        )

    dast_time = ("dast", time.time() - init_time)
    execution_stores.technique_stores["technique_times"].store(dast_time)


async def execute_skims() -> VulnerabilitiesEphemeralStore:
    """Execute skims according to the provided config."""
    vuln_stores = get_vulnerability_ephemeral_store()
    methods_stores = {"methods_results": get_ephemeral_store()}
    technique_stores = {"technique_times": get_ephemeral_store()}
    execution_stores = ExecutionStores(
        vuln_stores=vuln_stores,
        methods_stores=methods_stores,
        technique_stores=technique_stores,
    )

    cspm_errors = None
    # Run CSPM module first in case temporary credentials are used
    if ctx.SKIMS_CONFIG.cspm and any(
        [
            ctx.SKIMS_CONFIG.cspm.aws_credentials,
            ctx.SKIMS_CONFIG.cspm.azure_credentials,
            ctx.SKIMS_CONFIG.cspm.gcp_credentials,
        ],
    ):
        cspm_errors = {
            "aws_permissions_problems": get_ephemeral_store(),
            "aws_errors": get_ephemeral_store(),
            "azure_errors": get_ephemeral_store(),
            "gcp_errors": get_ephemeral_store(),
        }
        await analyze_cspm(execution_stores=execution_stores, error_stores=cspm_errors)

    dast_exclusions, sca_exclusions = get_exclusions()

    if ctx.SKIMS_CONFIG.sca.include:
        await analyze_sca(stores=execution_stores, sca_exclusions=sca_exclusions)
    if ctx.SKIMS_CONFIG.apk.include:
        await analyze_apk(stores=execution_stores)
    if ctx.SKIMS_CONFIG.sast.include:
        await analyze_sast(stores=execution_stores)

    requests_stores = None
    if ctx.SKIMS_CONFIG.dast and ctx.SKIMS_CONFIG.dast.urls:
        requests_stores = {
            "web_responses": get_ephemeral_store(),
            "ssl_responses": get_ephemeral_store(),
        }
        await analyze_dast(
            execution_stores=execution_stores,
            requests_stores=requests_stores,
            dast_exclusions=dast_exclusions,
        )

    log_blocking("info", "Analysis finished, writing results")
    report_results(
        stores=execution_stores.vuln_stores,
        requests_stores=requests_stores,
        cspm_error_stores=cspm_errors,
    )

    send_data_to_cloudwatch(execution_stores)
    return execution_stores.vuln_stores


def get_methods_coefficients(
    methods_stores: dict[str, EphemeralStore],
) -> dict[str, list[float]]:
    coeffs_dict: dict[str, list[float]] = {}
    for method_info in methods_stores["methods_results"].iterate():
        name, exec_time = method_info
        coeffs_dict.setdefault(name, []).append(exec_time)
    return coeffs_dict


def get_technique_times(
    technique_stores: dict[str, EphemeralStore],
) -> dict[str, float]:
    technique_dict: dict[str, float] = {}
    for technique_time in technique_stores["technique_times"].iterate():
        technique, exec_time = technique_time
        technique_dict[technique] = exec_time
    return technique_dict


def send_data_to_cloudwatch(
    stores: ExecutionStores,
) -> None:
    if is_fluid_batch_env() and len(ctx.SKIMS_CONFIG.checks) != 1:
        data_dict: dict[str, dict[str, int] | dict[str, float]] = {}
        data_dict["SkimsTechniqueTimes"] = get_technique_times(stores.technique_stores)
        data_dict["SkimsMethodCoeff"] = calculate_methods_averages(
            get_methods_coefficients(stores.methods_stores),
        )
        reported_methods = get_methods_that_reported(stores.vuln_stores)
        data_dict["SkimsMethodReport"] = dict(Counter(reported_methods))
        send_metrics_to_cloudwatch(data_dict)


def get_methods_that_reported(
    stores: VulnerabilitiesEphemeralStore,
) -> list[str]:
    return [
        split_last(result.skims_metadata.source_method)[-1]
        for result in stores.read_vulns()
        if result.skims_metadata and not result.skims_metadata.skip
    ]


def _count_vulns(stores: VulnerabilitiesEphemeralStore) -> int:
    total_vulns = 0
    for vuln in stores.read_vulns():
        if not vuln.skims_metadata.skip:
            total_vulns += 1
    return total_vulns


async def main() -> tuple[bool, int]:
    try:
        init_time = time.time()
        reset_ephemeral_state()

        log_blocking(
            "info",
            f"Scan was started at {get_datetime_with_offset(init_time)}",
        )
        log_blocking(
            "info",
            (
                "Official documentation:"
                " https://help.fluidattacks.com/portal/en/kb/"
                "find-security-vulnerabilities/use-the-cli"
            ),
        )
        log_blocking("info", f"Namespace: {ctx.SKIMS_CONFIG.namespace}")
        commit = ctx.SKIMS_CONFIG.commit or get_repo_head_hash(ctx.SKIMS_CONFIG.working_dir)
        if commit != DEFAULT_COMMIT:
            log_blocking("info", f"Running analysis over commit: {commit}")

        log_blocking("info", f"Startup work dir is: {ctx.SKIMS_CONFIG.start_dir}")
        log_blocking("info", f"Moving work dir to: {ctx.SKIMS_CONFIG.working_dir}")
        os.chdir(ctx.SKIMS_CONFIG.working_dir)

        stores = await execute_skims()

        total_vulns = _count_vulns(stores)

        log_blocking(
            "info",
            "Scan was finished at "
            f"{get_datetime_with_offset(end_time := time.time())}. "
            f"Duration: {get_duration(init_time, end_time)}.",
        )

        if is_fluid_batch_env():
            execution_time = time.time() - init_time
            execution_type = "reattack" if len(ctx.SKIMS_CONFIG.checks) == 1 else "scan"
            send_metrics_to_cloudwatch({"SkimsTimeExecution": {execution_type: execution_time}})

        return True, total_vulns

    finally:
        if ctx.SKIMS_CONFIG.start_dir:
            os.chdir(ctx.SKIMS_CONFIG.start_dir)
            reset_ephemeral_state()
