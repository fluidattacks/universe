import os

from model import (
    core,
)


def load_checks(config: dict[str, str]) -> set[core.FindingEnum]:
    # All checks by default, or the selected by the checks field
    return (
        {
            core.FindingEnum[finding]
            for finding in config.pop("checks")
            if finding in core.FindingEnum.__members__
        }
        if "checks" in config
        else set(core.FindingEnum)
    )


def load_apk_config(config_apk: dict) -> core.SkimsAPKConfig:
    return core.SkimsAPKConfig(
        exclude=config_apk.pop("exclude", []),
        include=config_apk.pop("include", []),
        use_sast_analysis=config_apk.pop("use_sast_analysis", True),
    )


def load_cspm_config(config_cspm: dict) -> core.SkimsCspmConfig:
    return core.SkimsCspmConfig(
        aws_credentials=[
            core.AwsCredentials(
                access_key_id=cred["access_key_id"],
                secret_access_key=cred["secret_access_key"],
                session_token=cred.get("session_token"),
            )
            if "access_key_id" in cred
            else core.AwsRole(external_id=cred.get("external_id"), role=cred.get("role"))
            for cred in config_cspm.get("aws_credentials", [])
        ],
        gcp_credentials=[
            core.GcpCredentials(
                private_key=cred["private_key"],
            )
            for cred in config_cspm.get("gcp_credentials", [])
        ],
        azure_credentials=[
            core.AzureCredentials(
                client_id=cred["client_id"],
                client_secret=cred["client_secret"],
                tenant_id=cred["tenant_id"],
                subscription_id=cred["subscription_id"],
            )
            for cred in config_cspm.get("azure_credentials", [])
        ],
    )


def load_dast_config(config_dast: dict) -> core.SkimsDastConfig:
    return core.SkimsDastConfig(
        urls=config_dast.pop("urls", []),
        http_checks=config_dast.get("http_checks", True),
        ssl_checks=config_dast.get("ssl_checks", True),
    )


def load_api_config(config_api: dict) -> core.SkimsApiConfig:
    return core.SkimsApiConfig(
        include=config_api.pop("include", []),
    )


def load_sca_config(config_sca: dict) -> core.SkimsScaConfig:
    return core.SkimsScaConfig(
        exclude=config_sca.pop("exclude", []),
        include=config_sca.pop("include", []),
        use_new_sca=config_sca.pop("use_new_sca", False),
    )


def load_sast_config(config_sast: dict, sbom_path: str | None) -> core.SkimsSastConfig:
    recursion_limit = config_sast.get("recursion_limit", 100)
    if not str(recursion_limit).isdigit() or recursion_limit < 0:
        recursion_limit = None

    reachability_sbom_input = (
        sbom_path if sbom_path else config_sast.get("reachability_sbom_input", "")
    )
    if not str(reachability_sbom_input).endswith(".json"):
        reachability_sbom_input = None

    return core.SkimsSastConfig(
        exclude=(*config_sast.pop("exclude", ()), "apk_files/"),
        include=config_sast.pop("include", ()),
        recursion_limit=recursion_limit,
        reachability_sbom_input=reachability_sbom_input,
    )


def load_output_config(config_output: dict) -> core.SkimsOutputConfig:
    return core.SkimsOutputConfig(
        file_path=os.path.abspath(config_output["file_path"]),  # noqa: PTH100
        format=core.OutputFormat(config_output["format"]),
    )


def load_language(config: dict) -> core.LocalesEnum:
    return core.LocalesEnum(config.pop("language", core.LocalesEnum.EN))
