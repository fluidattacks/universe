import os
from typing import (
    Literal,
)
from urllib.parse import (
    urlparse,
)

import confuse
import ctx
from core.config.loaders import (
    load_api_config,
    load_apk_config,
    load_checks,
    load_cspm_config,
    load_dast_config,
    load_language,
    load_output_config,
    load_sast_config,
    load_sca_config,
)
from model.core import (
    FindingEnum,
    LocalesEnum,
    OutputFormat,
    SkimsAPKConfig,
    SkimsConfig,
    SkimsDastConfig,
    SkimsModules,
    SkimsOutputConfig,
    SkimsSastConfig,
    SkimsScaConfig,
)
from utils.logs import (
    log_blocking,
)
from utils.repositories import (
    clone_repo_in_state_folder,
)


def _build_config(config_path: str) -> confuse.Configuration:
    template = confuse.Configuration("skims", read=False)
    template.set_file(config_path)
    template.read(user=False, defaults=False)
    return template.get(
        confuse.Template(
            {
                "working_dir": confuse.String(),
                "namespace": confuse.String(),
                "commit": confuse.String(),
                "language": confuse.Choice(LocalesEnum, LocalesEnum.EN),
                "execution_id": confuse.String(),
                "checks": confuse.Sequence(confuse.String()),
                "strict": confuse.OneOf([True, False]),
                "tracing_opt_out": confuse.OneOf([True, False]),
                "file_size_limit": confuse.OneOf([True, False]),
                "debug": confuse.OneOf([True, False]),
                "multifile": confuse.OneOf([True, False]),
                "use_report_soon": confuse.OneOf([True, False]),
                "output": confuse.Template(
                    {
                        "file_path": confuse.String(),
                        "format": confuse.OneOf([_format.value for _format in OutputFormat]),
                    },
                ),
                "api": confuse.Template(
                    {
                        "include": confuse.Sequence(confuse.String()),
                    },
                ),
                "apk": confuse.Template(
                    {
                        "exclude": confuse.Sequence(confuse.String()),
                        "include": confuse.Sequence(confuse.String()),
                        "use_sast_analysis": confuse.OneOf([True, False]),
                    },
                ),
                "cspm": confuse.Template(
                    {
                        "aws_credentials": confuse.Sequence(
                            confuse.Template(
                                {
                                    "access_key_id": confuse.Optional(confuse.String()),
                                    "secret_access_key": confuse.Optional(confuse.String()),
                                    "session_token": confuse.Optional(confuse.String()),
                                    "role": confuse.Optional(confuse.String()),
                                    "external_id": confuse.Optional(confuse.String()),
                                },
                            ),
                        ),
                        "gcp_credentials": confuse.Sequence(
                            confuse.Template(
                                {
                                    "private_key": confuse.String(),
                                },
                            ),
                        ),
                        "azure_credentials": confuse.Sequence(
                            confuse.Template(
                                {
                                    "client_id": confuse.String(),
                                    "client_secret": confuse.String(),
                                    "tenant_id": confuse.String(),
                                    "subscription_id": confuse.String(),
                                },
                            ),
                        ),
                    },
                ),
                "dast": confuse.Template(
                    {
                        "urls": confuse.Sequence(confuse.String()),
                        "ssl_checks": confuse.OneOf([True, False]),
                        "http_checks": confuse.OneOf([True, False]),
                    },
                ),
                "sast": confuse.Template(
                    {
                        "exclude": confuse.Sequence(confuse.String()),
                        "include": confuse.Sequence(confuse.String()),
                        "recursion_limit": confuse.Optional(confuse.Integer()),
                        "reachability_sbom_input": confuse.Optional(confuse.String()),
                    },
                ),
                "sca": confuse.Template(
                    {
                        "exclude": confuse.Sequence(confuse.String()),
                        "include": confuse.Sequence(confuse.String()),
                        "use_sca_new": confuse.OneOf([True, False]),
                    },
                ),
            },
        ),
    )


def load(config_path: str, scan_modules: SkimsModules, sbom_path: str | None) -> SkimsConfig:
    config = _build_config(config_path)

    try:
        config_apk = config.pop("apk", {})
        config_sast = config.pop("sast", {})
        config_sca = config.pop("sca", {})
        config_cspm = config.pop("cspm", {}) or {}
        config_dast = config.pop("dast", {}) or {}
        config_api = config.pop("api", {}) or {}
        output = config.pop("output", None)
        working_dir = str(os.path.abspath(config.pop("working_dir", "."))).replace(  # noqa: PTH100
            "/home/makes/.skims",
            ctx.STATE_FOLDER,
        )
        skims_config = SkimsConfig(
            apk=load_apk_config(config_apk if scan_modules.apk else {}),
            checks=load_checks(config),
            commit=config.pop("commit", None),
            cspm=load_cspm_config(config_cspm if scan_modules.cspm else {}),
            dast=load_dast_config(config_dast if scan_modules.dast else {}),
            api=load_api_config(config_api if scan_modules.api else {}),
            debug=config.pop("debug", False),
            language=load_language(config),
            multifile=config.pop("multifile", False),
            file_size_limit=config.pop("file_size_limit", True),
            namespace=config.pop("namespace", "namespace"),
            strict=config.pop("strict", False),
            sca=load_sca_config(config_sca if scan_modules.sca else {}),
            tracing_opt_out=config.pop("tracing_opt_out", False),
            output=load_output_config(output) if output else None,
            sast=load_sast_config(config_sast if scan_modules.sast else {}, sbom_path),
            start_dir=os.getcwd(),  # noqa: PTH109
            working_dir=working_dir,
            execution_id=config.pop("execution_id", None),
            use_report_soon=config.pop("use_report_soon", False),
        )

        if config:
            unrecognized_keys = ", ".join(config)
            msg = (
                f"Some keys were not recognized: {unrecognized_keys}."
                "The analysis will be performed only using the supported keys"
            )
            log_blocking("warning", msg)
    except KeyError as exc:
        exc_log = f"Key: {exc.args[0]} is required"
        raise confuse.ConfigError(exc_log) from exc

    return skims_config


def get_default_config(
    entrypoint: str,
    scan_type: Literal["SAST", "DAST"],
    output_config: SkimsOutputConfig | None,
    namespace: str,
    sbom_path: str | None,
) -> SkimsConfig:
    if scan_type == "DAST":
        api_config = None
        apk_config = SkimsAPKConfig(include=(), exclude=())
        sca_config = SkimsScaConfig(include=(), exclude=())
        sast_config = SkimsSastConfig(include=(), exclude=())
        dast_config = SkimsDastConfig(
            urls=(entrypoint,),
            http_checks=True,
            ssl_checks=True,
        )
        working_dir = os.getcwd()  # noqa: PTH109
    else:
        apk_config = SkimsAPKConfig(include=tuple("."), exclude=())
        sast_config = SkimsSastConfig(
            include=tuple("."),
            exclude=(),
            reachability_sbom_input=sbom_path
            if sbom_path and sbom_path.endswith(".json")
            else None,
        )
        sca_config = SkimsScaConfig(include=tuple("."), exclude=())
        api_config = None
        dast_config = None
        working_dir = entrypoint

    return SkimsConfig(
        api=api_config,
        apk=apk_config,
        checks=set(FindingEnum),
        commit="",
        cspm=None,
        dast=dast_config,
        debug=False,
        execution_id=None,
        file_size_limit=None,
        language=LocalesEnum.EN,
        multifile=False,
        namespace=namespace,
        tracing_opt_out=False,
        output=output_config,
        sast=sast_config,
        sca=sca_config,
        start_dir=os.getcwd(),  # noqa: PTH109
        strict=False,
        working_dir=working_dir,
    )


def set_execution_config(
    main_argument: str,
    output_config: SkimsOutputConfig | None,
    scan_modules: SkimsModules,
    sbom_path: str | None,
) -> SkimsConfig | None:
    config: SkimsConfig | None = None
    if os.path.isfile(main_argument):  # noqa: PTH113
        if not main_argument.endswith((".yaml", ".yml")):
            log_blocking("error", "Configuration file must be a YAML format")
        else:
            config = load(main_argument, scan_modules, sbom_path)
    elif os.path.isdir(main_argument):  # noqa: PTH112
        config = get_default_config(
            entrypoint=main_argument,
            scan_type="SAST",
            output_config=output_config,
            namespace=os.path.basename(main_argument),  # noqa: PTH119
            sbom_path=sbom_path,
        )
    elif (url_parsed := urlparse(main_argument)) and url_parsed.scheme and url_parsed.netloc:
        if url_parsed.path.endswith(".git") and (
            entrypoint := clone_repo_in_state_folder(main_argument)
        ):
            config = get_default_config(
                entrypoint=str(entrypoint),
                scan_type="SAST",
                output_config=output_config,
                namespace=os.path.basename(entrypoint),  # noqa: PTH119
                sbom_path=sbom_path,
            )
        else:
            config = get_default_config(
                entrypoint=main_argument,
                scan_type="DAST",
                output_config=output_config,
                namespace=url_parsed.netloc,
                sbom_path=sbom_path,
            )

    return config
