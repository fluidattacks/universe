import glob
import re
from pathlib import (
    Path,
)

import yaml
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.common import (
    get_advisory_details,
    resolve_source,
)
from utils.logs import (
    log_blocking,
)

RE_RANGES = re.compile(r"(?=[\(\[]).+?(?<=[\)\]])")
URL_GITLAB_ADVISORY_DATABASE = "https://gitlab.com/gitlab-org/advisories-community.git"
PLATFORMS = {
    "maven": "maven",
    "nuget": "nuget",
    "npm": "npm",
    "pypi": "pip",
    "gem": "gem",
    "go": "go",
    "packagist": "composer",
    "conan": "conan",
}
ALLOWED_RANGES = ("=", "<", ">", ">=", "<=")


def fix_operators(unformatted_range: str) -> str:
    prefix = unformatted_range[0]
    suffix = unformatted_range[-1]
    values = unformatted_range[1:-1].split(",")
    if len(values) < 2:
        return f"={values[0]}"
    min_r, max_r = values
    min_operator = ""
    max_operator = ""
    if min_r == "":
        min_r = "0"
        min_operator = ">="
    else:
        min_operator = ">" if prefix == "(" else ">="
    if max_r == "":
        return f"{min_operator}{min_r}"
    max_operator = "<" if suffix == ")" else "<="
    return f"{min_operator}{min_r} {max_operator}{max_r}"


def fix_pip_composer_range(str_range: str) -> str:
    vals_to_change = {",": " ", "==": "="}
    for target, replacement in vals_to_change.items():
        str_range = str_range.replace(target, replacement)
    return str_range


def format_platform_ranges(platform: str, range_str: str) -> str:
    if platform in ("maven", "nuget"):
        ranges = re.findall(RE_RANGES, range_str)
        str_ranges = [fix_operators(_range) for _range in ranges]
        return " || ".join(str_ranges)
    if platform in ("pypi", "packagist"):
        return fix_pip_composer_range(range_str)
    return range_str


def adjust_allowed_ranges(formatted_ranges: list[str]) -> str:
    fixed_ranges = []
    for formatted_range in formatted_ranges:
        if formatted_range.startswith("<"):
            formatted_range = f">=0 {formatted_range}"  # noqa: PLW2901
        fixed_items = []
        range_items = formatted_range.split()

        for index, item in enumerate(range_items):
            if item not in ALLOWED_RANGES and not item.startswith(ALLOWED_RANGES):
                operator = (
                    range_items[index - 1]
                    if index > 0 and range_items[index - 1] in ALLOWED_RANGES
                    else "="
                )
                fixed_items.append(operator + item)
            else:
                fixed_items.append(item)

        fixed_ranges.append(" ".join(fixed_items))

    return " || ".join(fixed_ranges)


def format_ranges(platform: str, range_str: str, description: str, affected_versions: str) -> str:
    affected_ranges = {"<0", "(,0)"}
    if (
        range_str in affected_ranges
        or description.lower() == "false positive"
        or "has been marked as a false positive" in description.lower()
        or affected_versions.lower() == "none"
    ):
        return "<0"

    if range_str == "" or affected_versions.lower() == "all versions":
        return ">=0"

    formatted_ranges = format_platform_ranges(platform, range_str).split("||")
    return adjust_allowed_ranges(formatted_ranges)


def get_platform_advisories(
    existing_advisories: set[tuple[str, str, str]],
    tmp_dirname: str,
    platform: str,
) -> list[Advisory]:
    filenames = sorted(
        glob.glob(  # noqa: PTH207
            f"{tmp_dirname}/{platform}/**/*.yml",
            recursive=True,
        ),
    )
    platform_raw_advisories = []
    for filename in filenames:
        with Path(filename).open(encoding="utf-8") as stream:
            try:
                parsed_yaml: dict = yaml.safe_load(stream)
                cve_id: str | None = parsed_yaml.get("identifier")
                if not cve_id or cve_id.startswith("GMS"):
                    continue
                pkg_slug = str(parsed_yaml.get("package_slug"))
                package_name = pkg_slug.replace(f"{platform}/", "", 1).lower()
                if platform in ("maven", "npm") and not package_name.startswith("@"):
                    package_name = package_name.replace("/", ":")

                if (PLATFORMS[platform], package_name, cve_id) in existing_advisories:
                    continue

                affected_range = str(parsed_yaml.get("affected_range"))
                description = parsed_yaml.get("description", "")
                affected_versions = parsed_yaml.get("affected_versions", "")

                formatted_ranges = format_ranges(
                    platform,
                    affected_range,
                    description,
                    affected_versions,
                ).lower()

                platform_raw_advisories.append(
                    Advisory(
                        id=cve_id,
                        package_manager=PLATFORMS[platform],
                        package_name=package_name,
                        severity=parsed_yaml.get("cvss_v3"),
                        severity_v4=parsed_yaml.get("cvss_v4"),
                        source=resolve_source(URL_GITLAB_ADVISORY_DATABASE, filename, platform),
                        vulnerable_version=formatted_ranges,
                        cwe_ids=parsed_yaml.get("cwe_ids"),
                        created_at=parsed_yaml.get("pubdate"),
                        modified_at=parsed_yaml.get("date"),
                        details=get_advisory_details(parsed_yaml, "title"),
                    ),
                )
            except yaml.YAMLError as exc:
                log_blocking("error", f"Error reading an advisory yml {exc}")

    return platform_raw_advisories


def get_advisories_community(
    existing_advisories: set[tuple[str, str, str]],
    tmp_dirname: str,
) -> list[Advisory]:
    log_blocking("info", "Processing Gitlab advisories")
    gitlab_raw_advisories = []
    for platform in PLATFORMS:
        gitlab_raw_advisories += get_platform_advisories(existing_advisories, tmp_dirname, platform)

    return gitlab_raw_advisories
