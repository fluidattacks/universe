import glob
import json
from pathlib import (
    Path,
)

from lib_sca.model import (
    Advisory,
)
from utils.logs import (
    log_blocking,
)


def get_patched_advisories(dir_path: str) -> dict[str, list[Advisory]]:
    new_advisories: list[Advisory] = []
    patched_advisories: list[Advisory] = []
    log_blocking("info", "Processing patched advisories")
    for filename in glob.iglob(dir_path, recursive=True):  # noqa: PTH207
        with Path(filename).open(encoding="utf-8") as stream:
            adv_info: dict = json.load(stream)
            adv_type = str(adv_info["type"])
            adv_id = str(adv_info["id"])
            pkg_manager = str(adv_info["package_manager"])
            pkg_name = str(adv_info["package_name"])
            vuln_version = str(adv_info["vulnerable_version"])

            if adv_type == "NEW":
                severity = str(adv_info["severity"])
                cwe_ids = adv_info["cwe_ids"]
                details = adv_info["details"]
                new_advisories.append(
                    Advisory(
                        id=adv_id,
                        package_name=pkg_name,
                        package_manager=pkg_manager,
                        vulnerable_version=vuln_version,
                        source="MANUAL",
                        cwe_ids=cwe_ids,
                        severity=severity,
                        details=details,
                    ),
                )
            elif adv_type == "PATCH":
                patched_advisories.append(
                    Advisory(
                        id=adv_id,
                        package_name=pkg_name,
                        package_manager=pkg_manager,
                        vulnerable_version=vuln_version,
                        source="MANUAL",
                    ),
                )

    num_advisories = len(patched_advisories) + len(new_advisories)
    log_blocking("info", f"Processed {num_advisories} advisories in universe repo")

    return {
        "PATCH": patched_advisories,
        "NEW": new_advisories,
    }
