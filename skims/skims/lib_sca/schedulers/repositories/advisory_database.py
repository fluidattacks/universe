import glob
import json
from pathlib import (
    Path,
)
from typing import (
    Any,
)

from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.common import (
    get_advisory_details,
    get_severity_cvss_v4,
    resolve_source,
)
from utils.logs import (
    log_blocking,
)

URL_GITHUB_ADVISORY_DATABASE = "https://github.com/github/advisory-database.git"
PLATFORMS = {
    "crates.io": "cargo",
    "rubygems": "gem",
    "github actions": "github_actions",
    "go": "go",
    "hex": "erlang",
    "maven": "maven",
    "nuget": "nuget",
    "npm": "npm",
    "packagist": "composer",
    "pub": "pub",
    "swifturl": "swift",
    "pypi": "pip",
}


def get_limit(events: list[dict[str, str]], pkg_obj: dict) -> str:
    if len(events) > 1:
        if (fixed := events[1].get("fixed")) is not None:
            return f" <{fixed}"
        if (l_affected := events[1].get("last_affected")) is not None:
            return f" <={l_affected}"
    if (db_specific := pkg_obj.get("database_specific")) and (
        l_version := db_specific.get("last_known_affected_version_range")
    ):
        return f" {l_version.replace(' ', '')}"
    return ""


def get_final_range(pkg_obj: dict) -> str:
    ranges: list[dict[str, Any]] = pkg_obj.get("ranges") or []
    final_range = ""

    versions: list[str] = pkg_obj.get("versions") or []
    formatted_versions = ["=" + ver for ver in versions]
    if formatted_versions:
        final_range = " || ".join(formatted_versions)

    for range_ver in ranges:
        events: list[dict[str, str]] = range_ver.get("events") or []
        introduced = f">={events[0].get('introduced')}"
        limit = get_limit(events, pkg_obj)
        str_range = f"{introduced}{limit}".lower()
        final_range = str_range if final_range == "" else f"{final_range} || {str_range}"

    return final_range


def spread_affected_packages(affected: list[dict]) -> dict:
    affected_packages: dict[str, dict[str, str]] = {}
    for pkg_obj in affected:
        package: dict[str, Any] = pkg_obj.get("package") or {}
        ecosystem: str = str(package.get("ecosystem"))
        if (platform := ecosystem.lower()) not in PLATFORMS:
            continue

        pkg_name: str = str(package.get("name")).lower()
        if platform == "swifturl":
            pkg_name = pkg_name.rsplit("/", 1)[-1].replace(".git", "")

        final_range = get_final_range(pkg_obj)

        if pkg_name not in affected_packages:
            affected_packages.update({pkg_name: {"version": final_range, "platform": platform}})
        else:
            cur_range = affected_packages[pkg_name]["version"]
            affected_packages.update(
                {
                    pkg_name: {
                        "version": f"{cur_range} || {final_range}",
                        "platform": platform,
                    },
                },
            )

    return affected_packages


def get_advisory_database(
    _: set[tuple[str, str, str]],
    tmp_dirname: str,
) -> list[Advisory]:
    filenames = sorted(
        glob.glob(  # noqa: PTH207
            f"{tmp_dirname}/advisories/github-reviewed/**/*.json",
            recursive=True,
        ),
    )
    log_blocking("info", "Processing Github advisory-database")
    github_raw_advisories = []
    for filename in filenames:
        with Path(filename).open(encoding="utf-8") as adv_json:
            try:
                advisory_dict: dict = json.load(adv_json)
            except json.JSONDecodeError as exc:
                log_blocking("error", "Error %s decoding advisory", exc)
                continue

            cve_id = next(
                (str(_id) for _id in advisory_dict.get("aliases", []) if _id.startswith("CVE")),
                advisory_dict.get("id"),
            )

            if not cve_id:
                continue

            withdrawn = advisory_dict.get("withdrawn")
            severity = advisory_dict.get("severity", [])
            severity_val = severity[0].get("score") if len(severity) > 0 else None

            affected_packages = spread_affected_packages(
                advisory_dict.get("affected", []),
            )
            for key_pkg, sub_dict in affected_packages.items():
                github_raw_advisories.append(
                    Advisory(
                        id=cve_id,
                        package_manager=PLATFORMS[sub_dict["platform"]],
                        package_name=key_pkg,
                        severity=severity_val,
                        severity_v4=get_severity_cvss_v4(severity),
                        source=resolve_source(URL_GITHUB_ADVISORY_DATABASE, filename),
                        vulnerable_version=sub_dict["version"] if not withdrawn else "<0",
                        cwe_ids=advisory_dict.get("database_specific", {}).get("cwe_ids", None),
                        created_at=advisory_dict.get("published"),
                        modified_at=advisory_dict.get("modified"),
                        alternative_id=None
                        if advisory_dict.get("id") == cve_id
                        else advisory_dict.get("id"),
                        details=get_advisory_details(advisory_dict),
                    ),
                )

    return github_raw_advisories
