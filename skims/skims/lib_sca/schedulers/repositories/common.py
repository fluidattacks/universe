URL_ADVISORY_OSSF: str = "https://github.com/ossf/malicious-packages.git"

URL_ADVISORIES_COMMUNITY: str = "https://gitlab.com/gitlab-org/advisories-community.git"
URL_ADVISORY_DATABASE: str = "https://github.com/github/advisory-database.git"


def transform_git_url_to_branch_url(repo_url: str, branch_name: str = "main") -> str:
    repo_url = repo_url.removesuffix(".git")

    return f"{repo_url}/blob/{branch_name}/"


def resolve_source(source_url: str, filename: str, platform: str | None = None) -> str:
    branch_url = transform_git_url_to_branch_url(source_url)
    if source_url == URL_ADVISORY_DATABASE:
        parts = filename.split("advisories/github-reviewed", 1)
        if len(parts) > 1:
            return branch_url + "advisories/github-reviewed" + parts[1]
    if source_url == URL_ADVISORIES_COMMUNITY and platform:
        parts = filename.split(platform, 1)
        if len(parts) > 1:
            return branch_url + platform + parts[1]
        return branch_url
    return source_url


def get_severity_cvss_v4(severity: list) -> str | None:
    return next(
        (_severity.get("score") for _severity in severity if _severity.get("type") == "CVSS_V4"),
        None,
    )


def get_advisory_details(parsed_yaml: dict, title_key: str = "summary") -> str | None:
    title = parsed_yaml.get(title_key)
    details = parsed_yaml.get("details")

    if not details and not title:
        return None

    return (f"{title}\n" if title else "") + (f"{details}" if details else "")
