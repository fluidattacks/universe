import gzip
import io

import requests
from lib_sca.model import (
    Advisory,
)
from utils.logs import (
    log_blocking,
)


def download_epss_scores() -> dict[str, str]:
    # Check file format at https://www.first.org/epss/data_stats.html
    try:
        response = requests.get("https://epss.cyentia.com/epss_scores-current.csv.gz", timeout=60)
        response.raise_for_status()

        compressed_file = io.BytesIO(response.content)
        with gzip.open(compressed_file, "rt", 9, "utf-8") as file:
            epss = file.read()

        epss_scores: dict[str, str] = {}
        for line in epss.splitlines():
            items = line.split(",")
            if len(items) == 3:
                key, value = items[0], items[1]
                epss_scores[key] = value

    except requests.exceptions.RequestException as exc:
        log_blocking(
            "error",
            "Error: Unable to download file due to error %s",
            exc,
        )
        return {}
    else:
        return epss_scores


def add_epss_scores(advisories: list[Advisory]) -> list[Advisory]:
    epss_scores = download_epss_scores()
    if not epss_scores:
        log_blocking("error", "Error downloading epss scores")

    log_blocking("info", "Downloaded %s epss scores", len(epss_scores))

    epss_advisories = []
    for adv in advisories:
        adv_score = epss_scores.get(adv.id, None)
        if adv.alternative_id and not adv_score:
            adv_score = epss_scores.get(adv.alternative_id, None)

        epss_advisories.append(adv._replace(epss=adv_score))

    return epss_advisories
