import json
import os
import zipfile
from contextlib import (
    suppress,
)
from pathlib import (
    Path,
)
from tempfile import (
    TemporaryDirectory,
)

import requests
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.advisory_database import (
    get_final_range,
)
from lib_sca.schedulers.repositories.common import (
    get_advisory_details,
    get_severity_cvss_v4,
)
from utils.logs import (
    log_blocking,
)

URL_OSV_ADVISORIES = "https://osv-vulnerabilities.storage.googleapis.com"

PLATFORMS = {
    "GitHub Actions": "github_actions",
    "Go": "go",
    "Hex": "erlang",
    "Maven": "maven",
    "NuGet": "nuget",
    "Packagist": "composer",
    "Pub": "pub",
    "PyPI": "pip",
    "RubyGems": "gem",
    "SwiftURL": "swift",
    "crates.io": "cargo",
    "npm": "npm",
}


def get_affected_ranges(affected: list[dict]) -> dict[str, dict[str, str]]:
    current_advisories: dict[str, dict[str, str]] = {}
    for pkg_obj in affected:
        package: dict = pkg_obj.get("package") or {}
        platform = str(package.get("ecosystem"))
        if platform not in PLATFORMS:
            continue

        pkg_name = str(package.get("name")).lower()
        final_range = get_final_range(pkg_obj)
        if not final_range:
            continue

        if pkg_name not in current_advisories:
            current_advisories.update({pkg_name: {"version": final_range, "platform": platform}})
        else:
            cur_range = current_advisories[pkg_name]["version"]
            current_advisories.update(
                {
                    pkg_name: {
                        "version": f"{cur_range} || {final_range}",
                        "platform": platform,
                    },
                },
            )
    return current_advisories


def parse_advisories_from_json(
    advisory_data: dict,
    existing_advisories: set[tuple[str, str, str]],
) -> list[Advisory]:
    if advisory_data.get("withdrawn"):
        return []

    pkg_advisories = []

    adv_id = advisory_data["id"]

    if not adv_id.startswith("MAL-") and (
        alternative_ids := [
            str(_id) for _id in advisory_data.get("aliases", []) if _id.startswith(("CVE", "GHSA"))
        ]
    ):
        adv_id = sorted(alternative_ids)[0]

    database_specific = advisory_data.get("database_specific")
    cwe_ids = database_specific.get("cwe_ids") if database_specific else None
    severity = advisory_data.get("severity", [])

    affected = advisory_data.get("affected", [])
    affected_ranges = get_affected_ranges(affected)

    for package_name, sub_dict in affected_ranges.items():
        package_manager = PLATFORMS[sub_dict["platform"]]
        if (package_manager, package_name, adv_id) in existing_advisories:
            continue
        pkg_advisories.append(
            Advisory(
                id=str(adv_id),
                package_manager=package_manager,
                package_name=package_name,
                severity=severity[0].get("score") if len(severity) > 0 else None,
                severity_v4=get_severity_cvss_v4(severity),
                source="https://osv-vulnerabilities",
                vulnerable_version=sub_dict["version"],
                cwe_ids=cwe_ids,
                created_at=advisory_data.get("published"),
                modified_at=advisory_data.get("modified"),
                details=get_advisory_details(advisory_data),
            ),
        )

    return pkg_advisories


def download_platform_advisories_from_ovs(
    platform_url: str,
    existing_advisories: set[tuple[str, str, str]],
) -> list[Advisory]:
    advisories = []
    with TemporaryDirectory() as temp_folder:
        response = requests.get(platform_url, timeout=60)
        if response.status_code == 200:
            zip_file_path = os.path.join(temp_folder, "downloaded.zip")  # noqa: PTH118
            with Path(zip_file_path).open("wb") as zip_file:
                zip_file.write(response.content)

            with zipfile.ZipFile(zip_file_path, "r") as zip_ref:
                zip_ref.extractall(temp_folder)
                extracted_files = zip_ref.namelist()
                for file in extracted_files:
                    if not file.endswith(".json"):
                        continue
                    file_path = os.path.join(temp_folder, file)  # noqa: PTH118
                    with (
                        Path(file_path).open(encoding="utf-8") as adv_json,
                        suppress(json.JSONDecodeError, KeyError),
                    ):
                        content = json.load(adv_json)
                        advisories.extend(parse_advisories_from_json(content, existing_advisories))

    return advisories


def get_osv_advisories(existing_advisories: set[tuple[str, str, str]]) -> list[Advisory]:
    osv_advisories = []
    log_blocking("info", "Downloading OSV advisories")
    for platform in PLATFORMS:
        platform_url = f"{URL_OSV_ADVISORIES}/{platform}/all.zip"
        osv_advisories += download_platform_advisories_from_ovs(platform_url, existing_advisories)

    log_blocking("info", "Downloaded %s total advisories from ovs", len(osv_advisories))
    return osv_advisories
