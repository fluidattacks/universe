import json
import sqlite3

from lib_sca.model import (
    Advisory,
)


def clean_details(cve_details: str | None) -> str | None:
    if cve_details and len(cve_details.split()) > 1000:
        return " ".join(cve_details.split()[:1000])
    return cve_details


def create_advisory_db(
    advisories: dict[str, dict[str, dict[str, Advisory]]],
    cve_findings: dict[str, str],
    db_path: str = "skims_sca_advisories.db",
) -> None:
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    cursor.execute("""
        CREATE TABLE IF NOT EXISTS advisories (
            adv_id TEXT,
            package_name TEXT,
            package_manager TEXT,
            vulnerable_version TEXT,
            source TEXT,
            cwe_ids TEXT,
            created_at TEXT,
            modified_at TEXT,
            severity TEXT,
            severity_v4 TEXT,
            alternative_id TEXT,
            epss TEXT,
            details TEXT,
            cve_finding TEXT,
            auto_approve INTEGER NOT NULL CHECK (auto_approve IN (0,1)),
            PRIMARY KEY (package_manager, package_name, adv_id)
        );
    """)

    cursor.execute("""
        CREATE INDEX IF NOT EXISTS PKG_MANAGER_PKG_NAME_IDX
        ON advisories (package_manager, package_name);
    """)
    conn.commit()

    def insert_advisory(
        package_manager: str,
        package_name: str,
        advisory: Advisory,
        adv_finding: str | None,
    ) -> None:
        cursor.execute(
            """
            INSERT OR REPLACE INTO advisories (
                adv_id, package_name, package_manager, vulnerable_version, source,
                cwe_ids, created_at, modified_at, severity, severity_v4,
                alternative_id, epss, details, cve_finding, auto_approve
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """,
            (
                advisory.id,
                package_name,
                package_manager,
                advisory.vulnerable_version,
                advisory.source,
                json.dumps(advisory.cwe_ids) if advisory.cwe_ids is not None else None,
                advisory.created_at,
                advisory.modified_at,
                advisory.severity,
                advisory.severity_v4,
                advisory.alternative_id,
                advisory.epss,
                clean_details(advisory.details),
                adv_finding,
                bool(adv_finding),
            ),
        )
        conn.commit()

    for package_manager, package_data in advisories.items():
        for package_name, adv_dict in package_data.items():
            for adv_id, adv in adv_dict.items():
                adv_finding = cve_findings.get(adv_id)
                if adv_id.startswith("MAL") and not adv_finding:
                    adv_finding = "F448"
                insert_advisory(package_manager, package_name, adv, adv_finding)

    conn.close()
