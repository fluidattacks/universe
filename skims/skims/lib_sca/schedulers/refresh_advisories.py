import json
from collections.abc import (
    Callable,
    Iterable,
)
from datetime import (
    datetime,
)
from pathlib import Path
from tempfile import (
    TemporaryDirectory,
)

from git.exc import (
    GitError,
)
from git.repo import (
    Repo,
)
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.common import (
    get_clean_severity,
    get_clean_severity_v4,
    get_clean_vulnerable_version,
)
from lib_sca.schedulers.create_adv_db import (
    create_advisory_db,
)
from lib_sca.schedulers.repositories.advisories_community import (
    URL_GITLAB_ADVISORY_DATABASE,
    get_advisories_community,
)
from lib_sca.schedulers.repositories.advisory_database import (
    URL_GITHUB_ADVISORY_DATABASE,
    get_advisory_database,
)
from lib_sca.schedulers.repositories.epss_scores import (
    add_epss_scores,
)
from lib_sca.schedulers.repositories.open_osv import (
    get_osv_advisories,
)
from lib_sca.schedulers.repositories.open_ssf import (
    URL_OSSF_ADVISORY_DATABASE,
    get_advisory_ossf,
)
from lib_sca.schedulers.repositories.patched_advisories import (
    get_patched_advisories,
)
from utils.custom_exceptions import (
    UnavailabilityError,
)
from utils.logs import (
    configure as configure_logs,
)
from utils.logs import (
    log_blocking,
)

# This order is mandatory, to use Github as main source of truth
ADVISORIES_GETTERS: dict[str, Callable[[set[tuple[str, str, str]], str], list[Advisory]]] = {
    URL_GITHUB_ADVISORY_DATABASE: get_advisory_database,
    URL_GITLAB_ADVISORY_DATABASE: get_advisories_community,
    URL_OSSF_ADVISORY_DATABASE: get_advisory_ossf,
}


def get_clean_advisories(raw_advisories: list[Advisory]) -> list[Advisory]:
    current_date = str(datetime.now())  # noqa:DTZ005
    return [
        advisory._replace(
            vulnerable_version=clean_vulnerable_version.lower(),
            severity=get_clean_severity(advisory.severity),
            severity_v4=get_clean_severity_v4(advisory.severity_v4),
            created_at=advisory.created_at or current_date,
            modified_at=advisory.modified_at or current_date,
        )
        for advisory in raw_advisories
        if (clean_vulnerable_version := get_clean_vulnerable_version(advisory))
    ]


def add_advisory(advisory: Advisory, s3_advisories: dict) -> None:
    if (
        s3_advisories.get(advisory.package_manager, {})
        .get(advisory.package_name, {})
        .get(advisory.id, None)
    ):
        return

    if advisory.package_manager not in s3_advisories:
        s3_advisories.update({advisory.package_manager: {}})
    if advisory.package_name not in s3_advisories[advisory.package_manager]:
        s3_advisories[advisory.package_manager].update({advisory.package_name: {}})

    if (alias_id := advisory.alternative_id) and (
        s3_advisories[advisory.package_manager][advisory.package_name].get(alias_id, None)
    ):
        return

    s3_advisories[advisory.package_manager][advisory.package_name].update(
        {
            advisory.id: advisory,
        },
    )


def create_advisories_dict(
    sources_advisories: Iterable[Advisory],
) -> dict[str, dict[str, dict[str, Advisory]]]:
    log_blocking("info", "Creating s3_advisories data")
    s3_advisories: dict[str, dict[str, dict[str, Advisory]]] = {}
    for adv in sources_advisories:
        add_advisory(adv, s3_advisories)
    return s3_advisories


def patch_advisories(
    s3_advisories: dict[str, dict[str, dict[str, Advisory]]],
    patched_advisories: dict[str, list[Advisory]],
) -> None:
    for adv in patched_advisories["NEW"]:
        # New advisories can't already exist in the data
        if adv.id in s3_advisories.get(adv.package_manager, {}).get(adv.package_name, {}):
            log_blocking("info", f"Existing {adv.id} advisory tried to be added as new")
            continue
        add_advisory(adv, s3_advisories)

    for adv in patched_advisories["PATCH"]:
        # Patch advisories have to exist in the data (Alternative ids might be used)
        pkg_advisories = s3_advisories.get(adv.package_manager, {}).get(adv.package_name, {})
        alternative_ids = {
            adv.alternative_id: adv_id
            for adv_id, adv in pkg_advisories.items()
            if adv.alternative_id
        }
        if adv.id not in pkg_advisories and adv.id not in alternative_ids:
            log_blocking("info", f"Non existing {adv.id} advisory tried to be patched")
            continue

        existing_adv_key = adv.id if adv.id in pkg_advisories else alternative_ids[adv.id]
        existing_adv = s3_advisories[adv.package_manager][adv.package_name][existing_adv_key]
        s3_advisories[adv.package_manager][adv.package_name][existing_adv_key] = (
            existing_adv._replace(
                source=adv.source,
                vulnerable_version=adv.vulnerable_version,
            )
        )


def get_raw_advisories() -> list[Advisory]:
    raw_advisories: list[Advisory] = []
    existing_advisories: set[tuple[str, str, str]] = set()
    for url, get_advisories_function in ADVISORIES_GETTERS.items():
        with TemporaryDirectory() as tmp_dir:
            try:
                Repo.clone_from(url, tmp_dir, depth=1)
                raw_advisories += get_advisories_function(existing_advisories, tmp_dir)

                existing_advisories = {
                    (adv.package_manager, adv.package_name, adv_id)
                    for adv in raw_advisories
                    for adv_id in (adv.id, adv.alternative_id)
                    if adv_id
                }
                log_blocking("info", f"Current advisories amount: {len(raw_advisories)}")
            except GitError as err:
                log_blocking("error", f"Error cloning repository: {url}")
                raise UnavailabilityError from err

    raw_advisories += get_osv_advisories(existing_advisories)
    log_blocking("info", f"Total raw advisories processed: {len(raw_advisories)}")
    return raw_advisories


def load_cve_findings(
    cve_findings_path: str,
) -> dict[str, str]:
    with Path(cve_findings_path).open(encoding="utf-8") as stream:
        cve_findings: dict[str, str] = json.load(stream)
        log_blocking(
            "info",
            f"Found existing {len(cve_findings)} advisories with criteria findings",
        )
        return cve_findings


def update_sca() -> None:
    raw_advisories: list[Advisory] = get_raw_advisories()
    clean_advisories = get_clean_advisories(raw_advisories)
    advisories_with_epss = add_epss_scores(clean_advisories)
    advisories_dict = create_advisories_dict(advisories_with_epss)
    patched_advisories = get_patched_advisories("skims/static/advisories/**/*.json")
    log_blocking("info", "Patching advisories data")
    patch_advisories(advisories_dict, patched_advisories)
    cve_findings = load_cve_findings("skims/static/cve_findings/cve_findings.json")
    create_advisory_db(advisories_dict, cve_findings)


def main() -> None:
    configure_logs()
    update_sca()


if __name__ == "__main__":
    main()
