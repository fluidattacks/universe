import atexit
import json
import sqlite3
import subprocess  # nosec
from pathlib import Path
from typing import (
    cast,
)

import boto3
from botocore import (
    UNSIGNED,
)
from botocore.config import (
    Config,
)
from ctx import (
    STATE_FOLDER_SCA,
)
from lib_sca.model import (
    Advisory,
)
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
)

BUCKET_NAME = "skims.sca"
DB_NAME = "skims_sca_advisories.db"
BUCKET_FILE_KEY = f"{DB_NAME}.zst"
DB_LOCAL_PATH = f"{STATE_FOLDER_SCA}/{DB_NAME}"
DB_LOCAL_COMPRESSED_PATH = f"{DB_LOCAL_PATH}.zst"

S3_CLIENT = boto3.client(
    service_name="s3",
    config=Config(
        region_name="us-east-1",
        signature_version=UNSIGNED,
    ),
)


def get_package_advisories(
    package_manager: str,
    package_name: str,
) -> list[Advisory]:
    connection = DATABASE.get_connection()
    cursor = connection.cursor()
    cursor.execute(
        """
        SELECT
            adv_id,
            source,
            vulnerable_version,
            cwe_ids,
            severity,
            severity_v4,
            alternative_id,
            epss,
            cve_finding,
            auto_approve
        FROM advisories
        WHERE package_manager = ? AND package_name = ?;
        """,
        (package_manager, package_name),
    )
    return [
        Advisory(
            id=result[0],
            package_name=package_name,
            package_manager=package_manager,
            source=result[1],
            vulnerable_version=result[2],
            cwe_ids=json.loads(result[3]) if result[3] else None,
            severity=result[4],
            severity_v4=result[5],
            alternative_id=result[6],
            epss=result[7],
            cve_finding=result[8],
            auto_approve=result[9],
        )
        for result in cursor.fetchall()
    ]


def _get_database_file() -> None:
    log_blocking("info", "Downloading advisories database to local")
    S3_CLIENT.download_file(
        Bucket=BUCKET_NAME,
        Key=BUCKET_FILE_KEY,
        Filename=DB_LOCAL_COMPRESSED_PATH,
    )
    with subprocess.Popen(  # noqa: S603
        ["zstd", "-d", "-f", DB_LOCAL_COMPRESSED_PATH],  # noqa: S607
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    ) as process:
        _, stderr = process.communicate()
        if cast(int, process.returncode) != 0:
            raise RuntimeError(stderr.decode())


def initialize_db() -> bool:
    local_database_exists = Path(DB_LOCAL_PATH).is_file()

    try:
        db_metadata = S3_CLIENT.head_object(Bucket=BUCKET_NAME, Key=BUCKET_FILE_KEY)
        up_to_date = (
            local_database_exists
            and Path(DB_LOCAL_PATH).stat().st_mtime >= db_metadata["LastModified"].timestamp()
        )

        if up_to_date:
            log_blocking("info", "Advisories database is up to date")
            return True
        _get_database_file()
        Path(DB_LOCAL_COMPRESSED_PATH).unlink()
    except Exception as error:  # noqa: BLE001
        log_blocking("error", "Unable to download database")
        log_to_remote_blocking(msg=str(error), severity="error")
        return False
    else:
        return True


class Database:
    def __init__(self) -> None:
        self.connection: sqlite3.Connection | None = None

    def initialize(self) -> None:
        if self.connection is None and initialize_db():
            self.connection = sqlite3.connect(
                DB_LOCAL_PATH,
                check_same_thread=False,
            )
            atexit.register(self.connection.close)

    def get_connection(self) -> sqlite3.Connection:
        if self.connection is not None:
            return self.connection
        self.connection = sqlite3.connect(
            DB_LOCAL_PATH,
            check_same_thread=False,
        )
        atexit.register(self.connection.close)
        return self.connection


DATABASE = Database()
