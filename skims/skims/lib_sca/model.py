from collections.abc import (
    Callable,
)
from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)

from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from utils.function import (
    shield_blocking,
)

SUPPORTED_PLATFORMS = {
    "cargo",
    "composer",
    "conan",
    "erlang",
    "gem",
    "github_actions",
    "go",
    "maven",
    "npm",
    "nuget",
    "pip",
    "pub",
    "sbom",
    "swift",
}


class Platform(Enum):
    CARGO = "CARGO"
    COMPOSER = "COMPOSER"
    CONAN = "CONAN"
    ERLANG = "ERLANG"
    GEM = "GEM"
    GITHUBACTIONS = "GITHUB_ACTIONS"
    GO = "GO"
    MAVEN = "MAVEN"
    NPM = "NPM"
    NUGET = "NUGET"
    PIP = "PIP"
    PUB = "PUB"
    SBOM = "SBOM"
    SWIFT = "SWIFT"


class Advisory(NamedTuple):
    id: str
    package_name: str
    package_manager: str
    vulnerable_version: str
    source: str
    cwe_ids: list[str] | None = None
    created_at: str | None = None
    modified_at: str | None = None
    severity: str | None = None
    severity_v4: str | None = None
    alternative_id: str | None = None
    epss: str | None = None
    details: str | None = None
    cve_finding: str | None = None
    auto_approve: bool = False


class DependencyLocation(NamedTuple):
    path: str
    line: str
    lines: list[str] | None = None
    column: str | None = None


class Dependency(NamedTuple):
    name: str
    version: str
    locations: DependencyLocation
    platform: Platform
    found_by: MethodsEnum
    depends_on: list[tuple[str, str]] | None = None


def get_path_from_attrs(*args: tuple[str, ...]) -> str:
    return args[0][-1]


SHIELD_BLOCKING: Callable = shield_blocking(
    on_error_return=(),
    get_path_from_attrs=get_path_from_attrs,
)


def identify_platform(file_name: str, file_extension: str, path: str) -> Platform | None:
    plat_dict = {
        "build.sbt": Platform.MAVEN,
        "build.gradle.kts": Platform.MAVEN,
        "gradle-wrapper.properties": Platform.MAVEN,
        "Cargo.toml": Platform.CARGO,
        "Cargo.lock": Platform.CARGO,
        "mix.exs": Platform.ERLANG,
        "mix.lock": Platform.ERLANG,
        "composer.json": Platform.COMPOSER,
        "composer.lock": Platform.COMPOSER,
        "conan.lock": Platform.CONAN,
        "conanfile.txt": Platform.CONAN,
        "conanfile.py": Platform.CONAN,
        "Gemfile.lock": Platform.GEM,
        "gems.locked": Platform.GEM,
        "go.mod": Platform.GO,
        "package.json": Platform.NPM,
        "package-lock.json": Platform.NPM,
        "pnpm-lock.yaml": Platform.NPM,
        "packages.config": Platform.NUGET,
        "packages.lock.json": Platform.NUGET,
        "Package.resolved": Platform.SWIFT,
        "Pipfile.lock": Platform.PIP,
        "poetry.lock": Platform.PIP,
        "pyproject.toml": Platform.PIP,
        "pubspec.yaml": Platform.PUB,
        "yarn.lock": Platform.NPM,
    }

    func_dict_extensions = {
        "csproj": Platform.NUGET,
        "gradle": Platform.MAVEN,
        "html": Platform.NPM,
        "xml": Platform.MAVEN,
        "json": Platform.SBOM,
        "txt": Platform.PIP,
    }

    func_dict_file_names = {
        "Pipfile": Platform.PIP,
        "Gemfile": Platform.GEM,
    }

    full_file = f"{file_name}.{file_extension}"
    if (
        platform := plat_dict.get(full_file)
        or func_dict_extensions.get(file_extension)
        or func_dict_file_names.get(file_name)
    ):
        return platform

    if ".github/workflows" in path and file_extension in {"yml", "yaml"}:
        return Platform.GITHUBACTIONS

    if file_name.endswith("sources") and file_extension in {"yml", "yaml"}:
        return Platform.PIP

    if file_name.endswith(".exe") and file_extension == "config":
        return Platform.NUGET

    return None
