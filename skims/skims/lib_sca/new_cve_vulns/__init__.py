from collections import defaultdict
from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)

import ctx
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_sca.common import (
    SHIELD_BLOCKING,
    get_sca_metadata,
    max_cvss_list,
    max_cvss_v4_list,
)
from lib_sca.model import (
    Advisory,
    Dependency,
)
from lib_sca.packages_parsers.packages import (
    parse_dependencies,
)
from lib_sca.vulnerabilities import (
    get_vulnerabilities,
)
from model.core import (
    FindingEnum,
    Vulnerabilities,
    Vulnerability,
)
from utils.build_vulns import (
    build_lines_vuln,
    build_metadata,
)
from utils.cvss import (
    set_default_temporal_scores,
    set_default_threat_scores,
)
from utils.logs import (
    log_blocking,
)
from utils.string_handlers import (
    is_exclusion,
)
from utils.translations import (
    t,
)


def get_vulnerability_for_dependency(
    content: str,
    path: str,
    dependency: Dependency,
) -> list[Vulnerability]:
    advisories = [
        adv
        for adv in get_vulnerabilities(dependency.platform, dependency.name, dependency.version)
        if adv.cve_finding
    ]
    if not advisories:
        return []

    advisories_by_finding: dict[str, list[Advisory]] = defaultdict(list)
    for advisory in advisories:
        if advisory.cve_finding:
            advisories_by_finding[advisory.cve_finding].append(advisory)

    vulnerabilities = []
    for cve_finding, advisories_group in advisories_by_finding.items():
        if cve_finding not in FindingEnum.__members__:
            msg = f"Unable to find Finding {cve_finding} in Enum model"
            log_blocking("error", msg)
            continue
        cwe_ids = list(set(chain(*[adv.cwe_ids for adv in advisories_group if adv.cwe_ids])))
        max_cvss = set_default_temporal_scores(
            max_cvss_list([adv.severity for adv in advisories_group if adv.severity]),
        )
        max_cvss_v4 = set_default_threat_scores(
            max_cvss_v4_list([adv.severity_v4 for adv in advisories_group if adv.severity_v4]),
        )
        sca_metadata = get_sca_metadata(dependency, advisories_group, is_new_sca_report=True)
        auto_approve = any(adv.auto_approve for adv in advisories_group)

        vulnerabilities.append(
            build_lines_vuln(
                method=dependency.found_by.value,
                what=path,
                where=str(dependency.locations.line),
                metadata=build_metadata(
                    method=dependency.found_by.value,
                    description=(
                        t(
                            key=dependency.found_by.name,
                            product=dependency.name,
                            version=dependency.version,
                            cve=sorted(sca_metadata.cve),
                        )
                        + f" {t(key='words.in')} "
                        f"{ctx.SKIMS_CONFIG.namespace}/{path}"
                    ),
                    snippet=make_snippet(
                        content=content,
                        viewport=SnippetViewport(line=int(dependency.locations.line), column=0),
                    ),
                    cvss=max_cvss,
                    cvss_v4=max_cvss_v4,
                    cwe_ids=cwe_ids,
                    sca_metadata=sca_metadata,
                    skip=is_exclusion(content, int(dependency.locations.line)),
                    sca_auto_approve=auto_approve,
                ),
                cve_finding=FindingEnum[cve_finding],
            ),
        )

    return vulnerabilities


def translate_dependencies_to_vulnerabilities(
    *,
    content: str,
    dependencies: Iterator[Dependency],
    path: str,
) -> Vulnerabilities:
    results: Vulnerabilities = tuple(
        chain.from_iterable(
            get_vulnerability_for_dependency(content, path, dep) for dep in dependencies
        ),
    )
    return results


@SHIELD_BLOCKING
def analyze(
    *,
    file_content: str,
    file_name: str,
    file_extension: str,
    path: str,
) -> tuple[Vulnerabilities, ...]:
    dependencies = parse_dependencies(
        file_content,
        file_name,
        file_extension,
        path,
    )
    return (
        translate_dependencies_to_vulnerabilities(
            content=file_content,
            dependencies=dependencies,
            path=path,
        ),
    )
