from lib_sca.advisories_database import (
    get_package_advisories,
)
from lib_sca.model import (
    Advisory,
    Platform,
)
from utils.match_versions import (
    match_vulnerable_versions,
)


def get_vulnerabilities(platform: Platform, product: str, version: str) -> list[Advisory]:
    vulnerabilities = []
    platform_val = platform.value.lower()

    if (
        product
        and version
        and (advisories := get_package_advisories(platform_val, product.lower()))
    ):
        vulnerabilities = [
            advisor
            for advisor in advisories
            if match_vulnerable_versions(version.lower(), advisor.vulnerable_version)
        ]

    return vulnerabilities
