from lib_sca.common import (
    SHIELD_BLOCKING,
    translate_dependencies_to_vulnerabilities,
)
from lib_sca.packages_parsers.packages import (
    parse_dependencies,
)
from model.core import (
    Vulnerabilities,
)


@SHIELD_BLOCKING
def analyze(
    *,
    file_content: str,
    file_name: str,
    file_extension: str,
    path: str,
) -> tuple[Vulnerabilities, ...]:
    dependencies = parse_dependencies(
        file_content,
        file_name,
        file_extension,
        path,
    )
    return (
        translate_dependencies_to_vulnerabilities(
            content=file_content,
            dependencies=dependencies,
            path=path,
        ),
    )
