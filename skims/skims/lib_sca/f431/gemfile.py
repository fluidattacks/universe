import os
from collections.abc import (
    Iterator,
)

from lib_sca.common import (
    locations_iterator_to_vulns,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)


@locations_iterator_to_vulns(
    MethodsEnum.GEMFILE_MISSING_PACKAGE_LOCK,
)
def gemfile_sca_attack(content: str, path: str) -> Iterator[tuple[int, int]]:
    if "gem" in content:
        pkg_path = "/".join(path.split("/")[:-1])
        lock_options = ["Gemfile.lock", "gems.locked"]
        if pkg_path != "":
            lock_options = [pkg_path + "/" + opt for opt in lock_options]

        if not any(os.path.isfile(lock_path) for lock_path in lock_options):  # noqa:PTH113
            yield 0, 0
