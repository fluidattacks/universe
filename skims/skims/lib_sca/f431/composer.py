import json
from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)
from pathlib import (
    Path,
)

from lib_sca.common import (
    locations_iterator_to_vulns,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)


@locations_iterator_to_vulns(
    MethodsEnum.COMPOSER_MISSING_PACKAGE_LOCK,
)
def composer_sca_attack(content: str, path: str) -> Iterator[tuple[int, int]]:
    with suppress(json.decoder.JSONDecodeError):
        packages = json.loads(content)
        if packages.get("require") or packages.get("require-dev"):
            pkg_path = Path(path).parent
            lock_path = pkg_path / "composer.lock"
            if not lock_path.is_file():
                yield 0, 0
