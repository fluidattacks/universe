from lib_sca.common import (
    SHIELD_BLOCKING,
)
from lib_sca.f431.cargo import (
    cargo_sca_attack,
)
from lib_sca.f431.composer import (
    composer_sca_attack,
)
from lib_sca.f431.conan import (
    conan_sca_attack,
)
from lib_sca.f431.erlang import (
    erlang_sca_attack,
)
from lib_sca.f431.gemfile import (
    gemfile_sca_attack,
)
from lib_sca.f431.npm import (
    npm_sca_attack,
)
from lib_sca.f431.nuget import (
    nuget_sca_attack,
)
from lib_sca.f431.pipfile import (
    pipfile_sca_attack,
)
from model.core import (
    Vulnerabilities,
)


@SHIELD_BLOCKING
def analyze(
    *,
    file_content: str,
    file_name: str,
    file_extension: str,
    path: str,
) -> tuple[Vulnerabilities, ...]:
    func_dict = {
        "package.json": npm_sca_attack,
        "conanfile.txt": conan_sca_attack,
        "conanfile.py": conan_sca_attack,
        "Pipfile": pipfile_sca_attack,
        "composer.json": composer_sca_attack,
        "Gemfile": gemfile_sca_attack,
        "mix.exs": erlang_sca_attack,
        "Cargo.toml": cargo_sca_attack,
        "packages.config": nuget_sca_attack,
    }
    file_desc = f"{file_name}.{file_extension}".rstrip(".")
    if callable_func := func_dict.get(file_desc):
        return (callable_func(file_content, path),)

    return ()
