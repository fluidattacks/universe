from collections.abc import (
    Iterator,
)
from pathlib import Path

from lib_sca.common import (
    locations_iterator_to_vulns,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)


@locations_iterator_to_vulns(
    MethodsEnum.CARGO_MISSING_PACKAGE_LOCK,
)
def cargo_sca_attack(content: str, path: str) -> Iterator[tuple[int, int]]:
    if "[dependencies]" in content or "[dev-dependencies]" in content:
        pkg_path = Path(path).parent
        lock_path = pkg_path / "Cargo.lock"
        if not lock_path.is_file():
            yield 0, 0
