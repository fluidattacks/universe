import json
import os
from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)

from lib_sca.common import (
    locations_iterator_to_vulns,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)


@locations_iterator_to_vulns(
    MethodsEnum.NPM_MISSING_PACKAGE_LOCK,
)
def npm_sca_attack(content: str, path: str) -> Iterator[tuple[int, int]]:
    with suppress(json.decoder.JSONDecodeError):
        packages = json.loads(content)
        if packages.get("dependencies") or packages.get("devDependencies"):
            pkg_path = "/".join(path.split("/")[:-1])
            # Search for root lock options
            lock_options = [
                "package-lock.json",
                "yarn.lock",
                "pnpm-lock.yaml",
                # For angular libraries
                "ng-package.json",
            ]
            if pkg_path != "":
                # Search package level lock files
                lock_options += [pkg_path + "/" + opt for opt in lock_options]

            if not any(os.path.isfile(lock_path) for lock_path in lock_options):  # noqa: PTH113
                yield 0, 0
