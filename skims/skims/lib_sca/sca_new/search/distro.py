from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.match import (
    Match,
)
from lib_sca.sca_new.match.details import (
    Detail,
)
from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.match.type import (
    MatchType,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)
from lib_sca.sca_new.search import (
    only_qualified_packages,
    only_vulnerable_versions,
)
from lib_sca.sca_new.version import (
    new_version_from_package,
)
from lib_sca.sca_new.vulnerability.provider import (
    IProviderByDistro,
)
from utils.logs import (
    log_blocking,
)


def by_package_distro(
    store: IProviderByDistro,
    distro: Distro | None,
    pkg: Package,
    matcher_type: MatcherType,
) -> list[Match]:
    if not distro:
        return []

    ver_object = new_version_from_package(pkg)
    if not ver_object:
        log_blocking(
            "error",
            "matcher failed to parse version pkg=%s ver=%s",
            pkg.name,
            pkg.version,
        )
        return []

    all_package_vulns = store.get_by_distro(distro, pkg)
    applicable_vulns = only_qualified_packages(distro, pkg, all_package_vulns)
    applicable_vulns = only_vulnerable_versions(ver_object, applicable_vulns)

    matches: list[Match] = [
        Match(
            vuln,
            pkg,
            [
                Detail(
                    type=MatchType.EXACT_DIRECT_MATCH,
                    matcher=matcher_type,
                    searched_by={
                        "distro": {
                            "type": distro.type,
                            "version": distro.raw_version,
                        },
                        "package": {
                            "name": pkg.name,
                            "version": pkg.version,
                        },
                        "namespace": {
                            "name": vuln.namespace,
                        },
                    },
                    found={
                        "vulnerability_id": vuln.id_,
                        "version_constraint": str(vuln.constraint),
                    },
                ),
            ],
        )
        for vuln in applicable_vulns
    ]
    return matches
