from enum import (
    Enum,
)

from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.match import (
    Match,
)
from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)
from lib_sca.sca_new.search.cpe import (
    by_package_cpe,
)
from lib_sca.sca_new.search.distro import (
    by_package_distro,
)
from lib_sca.sca_new.search.language import (
    by_package_language,
)
from lib_sca.sca_new.vulnerability.provider import (
    IProvider,
)
from utils.logs import (
    log_blocking,
)


# Define the Criteria enum
class Criteria(Enum):
    BY_CPE = "by-cpe"
    BY_LANGUAGE = "by-language"
    BY_DISTRO = "by-distro"


CommonCriteria = [Criteria.BY_LANGUAGE]


# Define the ByCriteria function
def by_criteria(
    store: IProvider,
    distro: Distro | None,
    package: Package,
    upstream_matcher: MatcherType,
    *criteria: Criteria,
) -> list[Match]:
    matches = []

    for item in criteria:
        if item == Criteria.BY_CPE:
            matches.extend(by_package_cpe(store, distro, package, upstream_matcher))

        elif item == Criteria.BY_LANGUAGE:
            matches.extend(by_package_language(store, distro, package, upstream_matcher) or [])

        elif item == Criteria.BY_DISTRO:
            try:
                matches.extend(by_package_distro(store, distro, package, upstream_matcher))
            except ValueError as err:
                log_blocking(
                    "warning",
                    f"could not match by package distro (package={package}): {err}",
                )
                continue

    return matches
