from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.version import (
    IConstraint,
    Version,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
)


@dataclass
class SemanticConstraint(IConstraint):
    raw: str

    def supported(self, ver_format: VersionFormat) -> bool:
        # gemfiles are a case of semantic version combined with non-semver
        # and that doesn't work well. Gemfile_version.go extracts the semVer
        # portion and makes a semVer object that is compatible with
        # these constraints. In practice two formats (semVer, gem version)
        # follow semVer,
        # but one of them needs extra cleanup to function (gem).
        return ver_format in (VersionFormat.SEMANTIC, VersionFormat.GEM)

    def satisfied(self, version: Version | None) -> bool:
        if version is None:
            return self.raw == ""

        if not self.supported(version.format):
            return False

        if not version.version_comparator or not version.version_comparator.semver_obj:
            return False

        if version.version_comparator.semver_obj.ver_obj:
            return version.version_comparator.semver_obj.ver_obj.match(self.raw)

        return False

    def string(self) -> str:
        if not self.raw:
            return "none (semver)"

        return f"{self.raw} (semver)"


def new_semantic_constraint(raw: str) -> SemanticConstraint:
    return SemanticConstraint(raw=raw)
