from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from typing import TYPE_CHECKING

from lib_sca.sca_new.version import (
    IConstraint,
)
from lib_sca.sca_new.version.constraint_expression import (
    ConstraintExpression,
    new_constraint_expression,
)
from lib_sca.sca_new.version.deb_version import (
    DebVersionComparator,
    new_deb_version_comparator,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
)

if TYPE_CHECKING:
    from lib_sca.sca_new.version.constraint_unit import (
        ConstraintUnit,
    )
    from lib_sca.sca_new.version.deb_version.version import (
        DebVersion,
    )


@dataclass
class DebConstraint(IConstraint):
    raw: str | None
    expression: ConstraintExpression | None = None

    def supported(self, format_: VersionFormat) -> bool:
        return format_ == VersionFormat.DEB

    def satisfied(self, version: DebVersion | None) -> bool:
        # an empty constraint is always satisfied
        if not self.raw and version is not None:
            return True

        if version is None:
            return not self.raw

        if self.expression is None:
            return False

        return self.expression.satisfied(version)

    def __str__(self) -> str:
        if not self.raw:
            return "node (deb)"

        return f"{self.raw} (deb)"

    def string(self) -> str:
        raise NotImplementedError  # pragma: no cover


def new_deb_constraint(raw: str) -> DebConstraint | None:
    if not raw:
        return DebConstraint(None, None)

    constraints = new_constraint_expression(raw, new_deb_comparator)

    return DebConstraint(raw=raw, expression=constraints)


def new_deb_comparator(unit: ConstraintUnit) -> DebVersionComparator:
    version = new_deb_version_comparator(unit.version)
    if not version:
        exc_log = f"unable to parse version: {unit.version}"
        raise ValueError(exc_log)

    return version
