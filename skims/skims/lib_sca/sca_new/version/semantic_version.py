from contextlib import (
    suppress,
)
from dataclasses import (
    dataclass,
)
from typing import TYPE_CHECKING

from lib_sca.sca_new.version.comparator import (
    IComparator,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
)
from semantic_version import (
    Version,
)

if TYPE_CHECKING:
    from lib_sca.sca_new import (
        version,
    )


@dataclass
class SemanticVersion(IComparator):
    ver_obj: Version | None = None

    def compare(self, other: "version.Version") -> int:
        if other.format != VersionFormat.SEMANTIC:
            exc_log = f"Unable to compare semantic version to given format: {other.format}"
            raise ValueError(exc_log)
        if other.version_comparator is None or other.version_comparator.semver_obj is None:
            exc_log = "Given empty semanticVersion object"
            raise ValueError(exc_log)

        with suppress(ValueError):
            if self.ver_obj:
                return other.version_comparator.semver_obj.compare(self.ver_obj)
        return -1
