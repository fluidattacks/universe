from enum import (
    Enum,
)

from lib_sca.sca_new.pkg.package import (
    PackageType,
)


class VersionFormat(Enum):
    UNKNOWN = "unknownformat"
    SEMANTIC = "semantic"
    APK = "apk"
    DEB = "deb"
    MAVEN = "maven"
    RPM = "rpm"
    PYTHON = "python"
    KB = "kb"
    GEM = "gem"
    PORTAGE = "portage"
    GOLANG = "go"


format_str = [
    "UnknownFormat",
    "Semantic",
    "Apk",
    "Deb",
    "Maven",
    "RPM",
    "Python",
    "KB",
    "Gem",
    "Portage",
    "Go",
]

formats = [
    VersionFormat.SEMANTIC,
    VersionFormat.APK,
    VersionFormat.DEB,
    VersionFormat.MAVEN,
    VersionFormat.RPM,
    VersionFormat.PYTHON,
    VersionFormat.KB,
    VersionFormat.GEM,
    VersionFormat.PORTAGE,
    VersionFormat.GOLANG,
]


def parse_format(user_str: str) -> VersionFormat:
    user_str_lower = user_str.lower()
    format_mapping = {
        VersionFormat.SEMANTIC.value: VersionFormat.SEMANTIC,
        "semver": VersionFormat.SEMANTIC,
        VersionFormat.APK.value: VersionFormat.APK,
        "apk": VersionFormat.APK,
        VersionFormat.DEB.value: VersionFormat.DEB,
        "dpkg": VersionFormat.DEB,
        VersionFormat.GOLANG.value: VersionFormat.GOLANG,
        "go": VersionFormat.GOLANG,
        VersionFormat.MAVEN.value: VersionFormat.MAVEN,
        "maven": VersionFormat.MAVEN,
        VersionFormat.RPM.value: VersionFormat.RPM,
        "rpm": VersionFormat.RPM,
        VersionFormat.PYTHON.value: VersionFormat.PYTHON,
        "python": VersionFormat.PYTHON,
        VersionFormat.KB.value: VersionFormat.KB,
        "kb": VersionFormat.KB,
        VersionFormat.GEM.value: VersionFormat.GEM,
        "gem": VersionFormat.GEM,
        VersionFormat.PORTAGE.value: VersionFormat.PORTAGE,
        "portage": VersionFormat.PORTAGE,
    }
    return format_mapping.get(user_str_lower, VersionFormat.UNKNOWN)


def version_format_from_pkg_type(pkg_type: PackageType) -> VersionFormat:
    mapping = {
        PackageType.APK_PKG: VersionFormat.APK,
        PackageType.DEB_PKG: VersionFormat.DEB,
        PackageType.JAVA_PKG: VersionFormat.MAVEN,
        PackageType.RPM_PKG: VersionFormat.RPM,
        PackageType.GEM_PKG: VersionFormat.GEM,
        PackageType.PYTHON_PKG: VersionFormat.PYTHON,
        PackageType.KB_PKG: VersionFormat.KB,
        PackageType.PORTAGE_PKG: VersionFormat.PORTAGE,
        PackageType.GO_MODULE_PKG: VersionFormat.GOLANG,
    }
    return mapping.get(pkg_type, VersionFormat.UNKNOWN)


def format_to_string(v_format: VersionFormat) -> str:
    try:
        if v_format not in VersionFormat:
            return format_str[0]
    except TypeError:
        return "UnknownFormat"
    return v_format.value
