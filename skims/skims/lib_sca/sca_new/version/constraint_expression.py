import re
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

from lib_sca.sca_new.version.comparator import (
    ComparatorGenerator,
    IComparator,
)
from lib_sca.sca_new.version.constraint_unit import (
    ConstraintUnit,
    parse_unit,
)


@dataclass
class ConstraintExpression:
    units: list[list[ConstraintUnit]]
    comparators: list[list[IComparator]]

    def satisfied(self, other: Any) -> bool:  # noqa: ANN401
        one_satisfied = False

        for idx_i, and_operand in enumerate(self.comparators):
            all_satisfied = True

            for idx_j, and_unit in enumerate(and_operand):
                result = and_unit.compare(other)
                unit = self.units[idx_i][idx_j]

                if not unit.satisfied(result):
                    all_satisfied = False

            one_satisfied = one_satisfied or all_satisfied

        return one_satisfied


def capture_version_operator_pair(buf: list[str], and_group: list[str]) -> None:
    if buf:
        ver = "".join(buf)
        and_group.append(ver)
        buf.clear()


def capture_and_group(and_group: list[str], or_groups: list[list[str]]) -> None:
    if and_group:
        or_groups.append(and_group.copy())
        and_group.clear()


def scan_expression(phrase: str) -> list[list[str]]:
    # expression examples x,y||z  <1.0, >=2.0|| 3.0 || =4.0
    or_groups: list[list[str]] = []  # all versions a group of and'd groups or'd together
    and_group: list[str] = []  # most current group of and'd versions
    buf: list[str] = []  # most current single version value
    last_char = ""

    tokens = re.finditer(r"[^\s\|,()]+|[\s\|,()]", phrase.strip())

    for match in tokens:
        current_char = match.group(0).strip()
        if current_char == ",":
            capture_version_operator_pair(buf, and_group)
        elif current_char == "|" and last_char == "|":
            capture_version_operator_pair(buf, and_group)
            capture_and_group(and_group, or_groups)
        elif current_char in ["(", ")"]:
            exc_log = "parenthetical expressions are not supported yet"
            raise ValueError(exc_log)
        elif current_char and current_char != "|":
            buf.append(current_char)
        last_char = current_char

    capture_version_operator_pair(buf, and_group)
    capture_and_group(and_group, or_groups)

    return or_groups


def new_constraint_expression(
    phrase: str,
    gen_function: ComparatorGenerator,
) -> ConstraintExpression:
    or_parts = scan_expression(phrase)

    or_units: list[list[ConstraintUnit]] = []
    or_comparators: list[list[IComparator]] = []

    for and_parts in or_parts:
        and_units: list[ConstraintUnit] = []
        and_comparators: list[IComparator] = []
        for part in and_parts:
            unit = parse_unit(part)
            if not unit:
                return ConstraintExpression([], [])

            and_units.append(unit)
            comparator = gen_function(unit)
            and_comparators.append(comparator)

        or_units.append(and_units)
        or_comparators.append(and_comparators)

    return ConstraintExpression(or_units, or_comparators)
