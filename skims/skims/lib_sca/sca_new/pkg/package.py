from enum import (
    Enum,
)
from typing import (
    Any,
    NamedTuple,
)

from cpe import (
    CPE,
)


class Location(NamedTuple):
    path: str
    line: int | None = None
    layer: int | None = None


class Language(Enum):
    UNKNOWN_LANGUAGE = ""
    CPP = "c++"
    DART = "dart"
    DOTNET = "dotnet"
    ELIXIR = "elixir"
    ERLANG = "erlang"
    GO = "go"
    HASKELL = "haskell"
    JAVA = "java"
    JAVASCRIPT = "javascript"
    PHP = "php"
    PYTHON = "python"
    R = "R"
    RUBY = "ruby"
    RUST = "rust"
    SWIFT = "swift"


class PackageType(Enum):
    UNKNOWN_PKG = "UnknownPackage"
    ALPM_PKG = "alpm"
    APK_PKG = "apk"
    BINARY_PKG = "binary"
    COCOAPODS_PKG = "pod"
    CONAN_PKG = "conan"
    DART_PUB_PKG = "dart-pub"
    DEB_PKG = "deb"
    DOTNET_PKG = "dotnet"
    ERLANG_OTP_PKG = "erlang-otp"
    GEM_PKG = "gem"
    GITHUB_ACTION_PKG = "github-action"
    GITHUB_ACTION_WORKFLOW_PKG = "github-action-workflow"
    GO_MODULE_PKG = "go-module"
    GRAALVM_NATIVE_IMAGE_PKG = "graalvm-native-image"
    HACKAGE_PKG = "hackage"
    HEX_PKG = "hex"
    JAVA_PKG = "java-archive"
    JENKINS_PLUGIN_PKG = "jenkins-plugin"
    KB_PKG = "msrc-kb"
    LINUX_KERNEL_PKG = "linux-kernel"
    LINUX_KERNEL_MODULE_PKG = "linux-kernel-module"
    NIX_PKG = "nix"
    NPM_PKG = "npm"
    PHP_COMPOSER_PKG = "php-composer"
    PHP_PECL_PKG = "php-pecl-pkg"
    PORTAGE_PKG = "portage"
    PYTHON_PKG = "python"
    R_PKG = "R-package"
    RPM_PKG = "rpm"
    RUST_PKG = "rust-crate"
    SWIFT_PKG = "swift"
    WORDPRESS_PLUGIN_PKG = "wordpress-plugin"


class Package(NamedTuple):
    id: str
    name: str  # the package name
    version: str  # the version of the package
    locations: list[Location]  # the locations that lead to the discovery of this package
    language: Language
    licenses: list[str]
    type: PackageType  # package type (e.g. Npm, Yarn, Python, Rpm, Deb, etc)
    purl: str
    cpes: list[CPE]
    metadata: dict[str, Any]
