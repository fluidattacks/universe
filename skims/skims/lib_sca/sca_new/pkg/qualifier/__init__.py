import abc

from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)


class IQualifier(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(  # pragma: no cover
        cls: type["IQualifier"],
        subclass: object,
    ) -> bool:
        return (hasattr(subclass, "compare") and callable(subclass.compare)) or NotImplemented

    @abc.abstractmethod
    def satisfied(self, distro: Distro | None, package: Package) -> bool:
        raise NotImplementedError  # pragma: no cover
