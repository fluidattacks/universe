import json
from pathlib import (
    Path,
)
from typing import (
    Any,
)

from lib_sca.sca_new.distro import (
    new_distro_from_release,
)
from lib_sca.sca_new.pkg.context import (
    Context,
)
from lib_sca.sca_new.pkg.package import (
    Language,
    Location,
    Package,
    PackageType,
)


def _load_package(item: dict[str, Any]) -> Package:
    return Package(
        id=item["id"],
        name=item["name"],
        version=item["version"],
        locations=[Location(x["path"], x.get("line"), x.get("layer")) for x in item["locations"]],
        language=Language(item["language"]) if item["language"] else Language.UNKNOWN_LANGUAGE,
        licenses=item["licenses"],
        type=PackageType(item["type"]) if item["type"] else PackageType.UNKNOWN_PKG,
        purl=item["package_url"],
        cpes=[],
        metadata=item["metadata"],
    )


def provide(sbom_file_location: str) -> tuple[list[Package], Context | None]:
    with Path(sbom_file_location).open(encoding="utf-8") as reader:
        sbom_json = json.load(reader)
    packages: list[Package] = [_load_package(x) for x in sbom_json["packages"]]
    context: Context | None = None
    if (release_data := sbom_json["sbom_details"].get("environment", {}).get("linux_release")) and (
        distro := new_distro_from_release(release_data)
    ):
        context = Context(distro)
    return packages, context
