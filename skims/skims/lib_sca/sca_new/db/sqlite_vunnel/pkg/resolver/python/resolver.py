import re

from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)


class Resolver(IResolver):
    @staticmethod
    def normalize(name: str) -> str:
        """Normalize a package name.

        Canonical naming of packages within python is defined by PEP 503 at
        https://peps.python.org/pep-0503/#normalized-names, and this code is
        derived from the official python implementation of canonical naming at
        https://packaging.pypa.io/en/latest/_modules/packaging/utils.html
        #canonicalize_name
        """
        return re.sub(r"[-_.]+", "-", name).lower()

    def resolve(self, package: Package) -> list[str]:
        """Resolve a package name.

        Canonical naming of packages within python is defined by PEP 503 at
        https://peps.python.org/pep-0503/#normalized-names, and this code is
        derived from the official python implementation of canonical naming at
        https://packaging.pypa.io/en/latest/_modules/packaging/utils.html
        #canonicalize_name
        """
        return [self.normalize(package.name)]
