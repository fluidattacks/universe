from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)


class Resolver(IResolver):
    @staticmethod
    def normalize(name: str) -> str:
        return name.lower()

    def resolve(self, package: Package) -> list[str]:
        return [package.name]
