from abc import (
    ABC,
    abstractmethod,
)

from lib_sca.sca_new.pkg.package import (
    Package,
)


class IResolver(ABC):
    @classmethod
    def __subclasshook__(cls, subclass: type) -> bool:
        return (  # pragma: no cover
            (
                hasattr(subclass, "normalize")
                and callable(subclass.normalize)
                and hasattr(subclass, "resolve")
                and callable(subclass.resolve)
            )
            or NotImplemented
        )

    @staticmethod
    @abstractmethod
    def normalize(name: str) -> str:
        """Normalize a string."""
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def resolve(self, package: Package) -> list[str]:
        """Resolve a package."""
        raise NotImplementedError  # pragma: no cover
