import logging

from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)
from packageurl import (
    PackageURL,
)


class Resolver(IResolver):
    @staticmethod
    def normalize(name: str) -> str:
        return name.lower()

    def resolve(self, package: Package) -> list[str]:
        names: set = set()

        if isinstance(package.metadata, dict):
            metadata = package.metadata
            if metadata.get("pom_group_id"):
                if metadata.get("pom_artifact_id"):
                    names.add(
                        self.normalize(
                            f"{metadata['pom_group_id']}:{metadata['pom_artifact_id']}",
                        ),
                    )
                if metadata.get("manifest_name"):
                    names.add(
                        self.normalize(
                            f"{metadata['pom_group_id']}:{metadata['manifest_name']}",
                        ),
                    )

        if package.purl:
            try:
                purl = PackageURL.from_string(package.purl)
                names.add(self.normalize(f"{purl.namespace}:{purl.name}"))
            except ValueError as exc:
                logging.warning(
                    ("unable to resolve java package identifier from purl=%s: %s"),
                    package.purl,
                    exc,
                )

        return list(names)
