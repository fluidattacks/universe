from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.db.sqlite_vunnel.store.model.base_model import (
    init_database,
)
from lib_sca.sca_new.db.sqlite_vunnel.store.model.vulnerability import (
    VulnerabilityModel,
)
from lib_sca.sca_new.db.sqlite_vunnel.store.model.vulnerability_metadata import (
    VulnerabilityMetadataModel,
)
from lib_sca.sca_new.db.sqlite_vunnel.vulnerability import (
    Vulnerability,
)
from lib_sca.sca_new.db.sqlite_vunnel.vulnerability_metadata import (
    VulnerabilityMetadata,
)
from lib_sca.sca_new.db.store import (
    IStore,
)
from peewee import (
    SqliteDatabase,
)


@dataclass
class Store(IStore):
    database: SqliteDatabase

    def get_vulnerability_namespaces(self) -> list[str]:
        query: list[VulnerabilityMetadataModel] = VulnerabilityMetadataModel.select(
            VulnerabilityMetadataModel.namespace,
        ).distinct()
        return [str(v.namespace) for v in list(query)]

    def get_vulnerability(self, namespace: str, vuln_id: str) -> list[Vulnerability]:
        query = VulnerabilityModel.select().where(
            (VulnerabilityModel.namespace == namespace) & (VulnerabilityModel.id == vuln_id),
        )
        models: list[VulnerabilityModel] = list(query)

        vulnerabilities = []
        for model in models:
            vulnerability = model.inflate()  # Convert the model to the desired format
            vulnerabilities.append(vulnerability)

        return vulnerabilities

    def search_for_vulnerabilities(self, namespace: str, package_name: str) -> list[Vulnerability]:
        query = VulnerabilityModel.select().where(
            (VulnerabilityModel.namespace == namespace)
            & (VulnerabilityModel.package_name == package_name),
        )
        models: list[VulnerabilityModel] = list(query)

        vulnerabilities = []
        for model in models:
            vulnerability = model.inflate()  # Convert the model to the desired format
            vulnerabilities.append(vulnerability)

        return vulnerabilities

    def get_vulnerability_metadata(
        self,
        vuln_id: str,
        namespace: str,
    ) -> VulnerabilityMetadata | None:
        query = VulnerabilityMetadataModel.select().where(
            (VulnerabilityMetadataModel.id == vuln_id)
            & (VulnerabilityMetadataModel.namespace == namespace),
        )
        models: list[VulnerabilityMetadataModel] = list(query)
        if len(models) > 1:
            exc_log = f"found multiple metadatas for single ID={vuln_id} Namespace={namespace}"
            raise ValueError(exc_log)
        if len(models) == 1:
            return models[0].inflate()
        return None

    def get_all_vulnerabilities(self) -> list[Vulnerability]:
        query = VulnerabilityModel.select()
        models: list[VulnerabilityModel] = list(query)
        return [m.inflate() for m in models]

    def get_all_vulnerability_metadata(
        self,
    ) -> list[VulnerabilityMetadata]:
        query = VulnerabilityMetadataModel.select()
        models: list[VulnerabilityMetadataModel] = list(query)
        return [m.inflate() for m in models]


def new_store(db_file_path: str) -> IStore:
    database = SqliteDatabase(db_file_path)
    init_database(database)
    return Store(database=database)


# def search_for_vulnerabilities(store: Store, )
