from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.db.sqlite_vunnel.namespace import (
    INamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.from_language import (
    from_language,
)
from lib_sca.sca_new.pkg.package import (
    Language,
    PackageType,
)

ID = "language"


@dataclass
class Namespace(INamespace):
    provider_: str
    language_: Language
    package_type_: PackageType
    resolver_: IResolver

    def provider(self) -> str:
        return self.provider_

    def resolver(self) -> IResolver:
        return self.resolver_

    def string(self) -> str:
        if self.package_type_ != PackageType.UNKNOWN_PKG:
            return f"{self.provider_}:{ID}:{self.language_.value}:{self.package_type_.value}"
        return f"{self.provider_}:{ID}:{self.language_.value}"


def new_namespace(provider: str, language: Language, package_type: PackageType) -> Namespace:
    resolver = from_language(language)
    return Namespace(
        provider_=provider,
        language_=language,
        package_type_=package_type,
        resolver_=resolver,
    )


def from_str(namespace_str: str) -> Namespace:
    if not namespace_str:
        exc_log = "unable to create distro namespace from empty string"
        raise ValueError(exc_log)

    components = namespace_str.split(":")

    if len(components) != 3 and len(components) != 4:
        exc_log = (
            "unable to create distro namespace from"
            f" {namespace_str}: incorrect number of components"
        )
        raise ValueError(exc_log)

    if components[1] != ID:
        exc_log = (
            f"unable to create distro namespace from "
            f"{namespace_str}: type {components[1]} is incorrect"
        )
        raise ValueError(exc_log)

    package_type = ""

    if len(components) == 4:
        package_type = components[3]

    return new_namespace(
        components[0],
        Language(components[2]),
        PackageType(package_type or "UnknownPackage"),
    )
