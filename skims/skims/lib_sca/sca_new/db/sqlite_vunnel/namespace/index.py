from __future__ import (
    annotations,
)

import logging
import re
from dataclasses import (
    dataclass,
)
from typing import TYPE_CHECKING

from lib_sca.sca_new.db.sqlite_vunnel.namespace.cpe.namespace import (
    Namespace as CPENamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.distro.namespace import (
    Namespace as DistroNamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.from_str import (
    from_str,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.language.namespace import (
    Namespace as LanguageNamespace,
)

if TYPE_CHECKING:
    from lib_sca.sca_new.db.sqlite_vunnel.namespace import (
        INamespace,
    )
    from lib_sca.sca_new.distro import (
        Distro,
    )
    from lib_sca.sca_new.pkg.package import (
        Language,
    )

# Setup logging
logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)

alpine_version_regex = re.compile(r"^(\d+)\.(\d+)\.(\d+)$")


def get_index_from_strings(namespaces_names: list[str]) -> Index | None:
    all_ns: list[INamespace] = []
    by_language: dict[Language, list[LanguageNamespace]] = {}
    by_distro_key: dict[str, list[DistroNamespace]] = {}
    cpe_namespaces: list[CPENamespace] = []

    for namespace_name in namespaces_names:
        namespace = create_namespace(namespace_name)
        if namespace is None:
            continue

        all_ns.append(namespace)
        index_namespace(
            namespace,
            namespace_name,
            by_language,
            by_distro_key,
            cpe_namespaces,
        )

    return Index(all_ns, by_language, by_distro_key, cpe_namespaces)


def create_namespace(namespace_name: str) -> INamespace | None:
    try:
        return from_str(namespace_name)
    except IndexError as err:
        logger.warning(
            "unable to create namespace object from namespace=%s: %s",
            namespace_name,
            err,
        )
        return None


def index_namespace(
    namespace: INamespace,
    namespace_name: str,
    by_language: dict[Language, list[LanguageNamespace]],
    by_distro_key: dict[str, list[DistroNamespace]],
    cpe_namespaces: list[CPENamespace],
) -> None:
    if isinstance(namespace, LanguageNamespace) and hasattr(namespace, "language_"):
        index_language_namespace(namespace, by_language)
    elif isinstance(namespace, DistroNamespace) and hasattr(namespace, "distro_type_"):
        index_distro_namespace(namespace, by_distro_key)
    elif isinstance(namespace, CPENamespace) and "cpe" in namespace_name:
        cpe_namespaces.append(namespace)
    else:
        logger.warning("unable to index namespace=%s", namespace_name)


def index_language_namespace(
    namespace: LanguageNamespace,
    by_language: dict[Language, list[LanguageNamespace]],
) -> None:
    language = namespace.language_
    if language not in by_language:
        by_language[language] = []
    by_language[language].append(namespace)


def index_distro_namespace(
    namespace: DistroNamespace,
    by_distro_key: dict[str, list[DistroNamespace]],
) -> None:
    distro_key = f"{namespace.distro_type_.value}:{namespace.version_}"
    if distro_key not in by_distro_key:
        by_distro_key[distro_key] = []
    by_distro_key[distro_key].append(namespace)


@dataclass
class Index:
    all: list[INamespace]
    by_language: dict[Language, list[LanguageNamespace]]
    by_distro_key: dict[str, list[DistroNamespace]]
    cpe: list[CPENamespace]

    def namespaces_for_language(self, language: Language) -> list[LanguageNamespace] | None:
        return self.by_language.get(language, None)

    def _namespace_for_distro_rolling(self, distro: Distro) -> list[DistroNamespace] | None:
        if distro.is_rolling():
            distro_key = f"{distro.type.value.lower()}:rolling"
            if distro_key in self.by_distro_key:
                return self.by_distro_key[distro_key]
        return None

    def _namespace_for_distro_version_segments(
        self,
        distro: Distro,
        version_segments: list[int],
    ) -> list[DistroNamespace] | None:
        if not version_segments:
            return None

        if distro.type.value.lower() == "alpine":
            namespace = self.get_alpine_namespace(distro, version_segments)
            if namespace:
                return namespace

        distro_key = f"{distro.type.value.lower()}:{distro.full_version()}"
        if distro_key in self.by_distro_key:
            return self.by_distro_key[distro_key]

        if len(version_segments) == 3:
            for key in [
                f"{distro.type.value.lower()}:{version_segments[0]}.{version_segments[1]}",
                f"{distro.type.value.lower()}:{version_segments[0]}",
            ]:
                if key in self.by_distro_key:
                    return self.by_distro_key[key]

        if distro.type.value in [
            "centos",
            "redhat",
            "fedora",
            "rockylinux",
            "almalinux",
            "gentoo",
        ]:
            distro_key = f"redhat:{version_segments[0]}"
            if distro_key in self.by_distro_key:
                return self.by_distro_key[distro_key]
        return None

    def namespaces_for_distro(self, distro: Distro | None) -> list[DistroNamespace] | None:
        if distro is None:
            return None
        namespaces: list[DistroNamespace] | None = None
        namespaces = self._namespace_for_distro_rolling(distro)
        if namespaces is not None:
            return namespaces

        version_segments = self.get_version_segments(distro)
        namespaces = self._namespace_for_distro_version_segments(distro, version_segments)

        if namespaces is not None:
            return namespaces

        if not version_segments and distro.type.value.lower() == "alpine":
            distro_key = f"{distro.type.value.lower()}:edge"
            if distro_key in self.by_distro_key:
                return self.by_distro_key[distro_key]

        if (
            not version_segments
            and distro.type.value.lower() == "debian"
            and distro.raw_version == "unstable"
        ):
            distro_key = f"{distro.type.value.lower()}:unstable"
            if distro_key in self.by_distro_key:
                return self.by_distro_key[distro_key]

        return None

    def get_alpine_namespace(
        self,
        distro: Distro,
        version_segments: list[int],
    ) -> list[DistroNamespace] | None:
        if alpine_version_regex.match(distro.raw_version):
            distro_key = f"{distro.type.value.lower()}:{version_segments[0]}.{version_segments[1]}"
            if distro_key in self.by_distro_key:
                return self.by_distro_key[distro_key]

        distro_key = f"{distro.type.value.lower()}:edge"
        if distro_key in self.by_distro_key:
            return self.by_distro_key[distro_key]

        return None

    def get_version_segments(self, distro: Distro) -> list[int]:
        if distro.version:
            return [int(seg) for seg in distro.version if isinstance(seg, int)]
        return []

    def cpe_namespaces(self) -> list[CPENamespace]:
        return self.cpe
