import abc
from abc import (
    abstractmethod,
)

from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)


class INamespace(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass: type) -> bool:
        return (  # pragma: no cover
            (
                hasattr(subclass, "provider")
                and callable(subclass.provider)
                and hasattr(subclass, "resolver")
                and callable(subclass.resolver)
                and hasattr(subclass, "string")
                and callable(subclass.string)
            )
            or NotImplemented
        )

    @abstractmethod
    def provider(self) -> str:
        """Provide the provider string."""
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def resolver(self) -> IResolver:
        """Return a Resolver instance."""
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def string(self) -> str:
        """Return the string representation."""
        raise NotImplementedError  # pragma: no cover
