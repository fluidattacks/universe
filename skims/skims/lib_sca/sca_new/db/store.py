import abc

from lib_sca.sca_new.db.sqlite_vunnel.vulnerability_metadata_store import (
    IVulnerabilityMetadataStoreReader,
)
from lib_sca.sca_new.db.sqlite_vunnel.vulnerability_store import (
    IVulnerabilityStoreReader,
)


class IStoreReader(
    IVulnerabilityStoreReader,
    IVulnerabilityMetadataStoreReader,
    metaclass=abc.ABCMeta,
):
    pass


class IStore(IStoreReader, metaclass=abc.ABCMeta):
    pass
