from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)


class FixState(Enum):
    UNKNOWN = "unknown"
    FIXED = "fixed"
    NOT_FIXED = "not-fixed"
    WONT_FIX = "wont-fix"


class Fix(NamedTuple):
    versions: list[str]
    state: FixState
