import json
import os
import shutil
import tarfile
import tempfile
from contextlib import (
    suppress,
)
from datetime import (
    datetime,
)
from pathlib import (
    Path,
)
from typing import (
    NamedTuple,
)

import aiohttp
from lib_sca.sca_new.db.listing import (
    Listing,
    ListingEntry,
    best_to_update,
)
from lib_sca.sca_new.db.metadata import (
    Metadata,
    is_superseded_by,
    metadata_from_dir,
)
from lib_sca.sca_new.db.sqlite_vunnel import (
    store,
)
from lib_sca.sca_new.db.store import (
    IStoreReader,
)
from platformdirs import (
    user_cache_dir,
)

DB_CACHE_DIR = user_cache_dir("fluid-skims-sca", "fluidattacks")


class DBEntry(NamedTuple):
    db_path: str
    db_dir: str
    validate_age: bool = False
    listing_url: str | None = None

    def get_store(self) -> IStoreReader:
        return store.new_store(self.db_path)


async def listing_from_url(entry: DBEntry) -> Listing:
    result = Listing(available={})

    database_list_url = entry.listing_url or (
        "https://toolbox-data.anchore.io/grype/databases/listing.json"
    )
    async with aiohttp.ClientSession() as session:  # noqa: SIM117
        async with session.get(database_list_url) as response:
            content = json.loads(await response.content.read())
            for version, values in content["available"].items():
                result.available[int(version)] = []
                for value in values:
                    result.available[int(version)].append(
                        ListingEntry(
                            built=datetime.fromisoformat(value["built"]),
                            version=int(version),
                            url=value["url"],
                            checksum=value["checksum"],
                        ),
                    )
    return result


async def is_update_available(
    entry: DBEntry,
) -> tuple[bool, Metadata | None, ListingEntry | None]:
    listing = await listing_from_url(entry)
    update_entry = best_to_update(listing, 5)
    if not update_entry:
        return False, None, None

    current = metadata_from_dir(entry.db_dir)

    if not current:
        return True, None, update_entry

    if is_superseded_by(current, update_entry):
        return True, current, update_entry

    return False, None, None


async def download(listing: ListingEntry) -> str:
    temp_path = tempfile.mkdtemp()
    async with aiohttp.ClientSession() as session:  # noqa: SIM117
        async with session.get(listing.url) as response:
            with tempfile.NamedTemporaryFile(delete=False) as temp_file_tar_gz:
                while True:
                    chunk = await response.content.read(1024)
                    if not chunk:
                        break
                    temp_file_tar_gz.write(chunk)

            with tarfile.open(temp_file_tar_gz.name) as tar:
                tar.extract("metadata.json", path=temp_path)
                tar.extract("vulnerability.db", path=temp_path)
            Path(temp_file_tar_gz.name).unlink()

    return temp_path


async def update_to(db_entry: DBEntry, entry: ListingEntry) -> None:
    tem_dir = await download(entry)
    with suppress(FileNotFoundError):
        shutil.rmtree(db_entry.db_dir)
    os.makedirs(db_entry.db_dir)  # noqa: PTH103
    shutil.copytree(tem_dir, db_entry.db_dir, dirs_exist_ok=True)
    shutil.rmtree(tem_dir)


async def update_db(entry: DBEntry) -> None:
    update_available, _metadata, update_entry = await is_update_available(entry)
    if update_available and update_entry:
        await update_to(entry, update_entry)


def new_db_entry() -> DBEntry:
    return DBEntry(
        db_path=os.path.join(DB_CACHE_DIR, "vulnerability.db"),  # noqa: PTH118
        db_dir=DB_CACHE_DIR,
        validate_age=True,
        listing_url=None,
    )
