from contextlib import (
    suppress,
)
from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)


class ListingEntry(NamedTuple):
    built: datetime
    version: int
    url: str
    checksum: str


class Listing(NamedTuple):
    available: dict[int, list[ListingEntry]]


def best_to_update(listing: Listing, version: int) -> ListingEntry | None:
    with suppress(KeyError):
        return sorted(listing.available[version], key=lambda x: x.built)[-1]
    return None  # pragma: no cover
