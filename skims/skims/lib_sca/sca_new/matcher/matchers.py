from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.matcher import (
    IMatcher,
    dpkg,
)


@dataclass
class Config:
    pass


def new_default_matchers(_config: Config) -> list[IMatcher]:
    return [dpkg.Matcher()]
