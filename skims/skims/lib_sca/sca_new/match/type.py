from enum import (
    Enum,
)


class MatchType(Enum):
    EXACT_DIRECT_MATCH = "exact-direct-match"
    EXACT_INDIRECT_MATCH = "exact-indirect-match"
    CPE_MATCH = "cpe-match"
