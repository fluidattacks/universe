import hashlib
import json
from dataclasses import (
    dataclass,
)


@dataclass(frozen=True)
class Fingerprint:
    vulnerability_id: str
    vulnerability_namespace: str
    vulnerability_fixes: str
    package_id: str

    def __str__(self) -> str:
        return (
            f"Fingerprint(vuln={self.vulnerability_id} "
            f"namespace={self.vulnerability_namespace}"
            f" fixes={self.vulnerability_fixes} package={self.package_id})"
        )

    def id_(self) -> str:
        # Convert the object to a dictionary
        obj_dict = {
            "vulnerability_id": self.vulnerability_id,
            "vulnerability_namespace": self.vulnerability_namespace,
            "vulnerability_fixes": self.vulnerability_fixes,
            "package_id": self.package_id,
        }

        # Convert the dictionary to a JSON string
        json_str = json.dumps(obj_dict, sort_keys=True)

        # Create a hash of the JSON string
        hash_object = hashlib.sha256(json_str.encode())
        return hash_object.hexdigest()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Fingerprint):
            return NotImplemented
        return self.id_() == other.id_()
