from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from typing import TYPE_CHECKING

from utils.logs import (
    log_blocking,
)

if TYPE_CHECKING:
    from collections.abc import Iterator

    from lib_sca.sca_new.match import (
        Match,
    )
    from lib_sca.sca_new.match.fingerprint import (
        Fingerprint,
    )


@dataclass
class Matches:
    by_fingerprint: dict[Fingerprint, Match]
    by_package: dict[str, list[Fingerprint]]

    @staticmethod
    def new_matches() -> Matches:
        return Matches({}, {})

    @staticmethod
    def new_matches_with_matches(*matches: Match) -> Matches:
        match = Matches.new_matches()
        match.add(*matches)
        return match

    def get_by_pkg_id(self, pkg_id: str) -> list[Match]:
        return [self.by_fingerprint[fingerprint] for fingerprint in self.by_package.get(pkg_id, [])]

    def all_by_pkg_id(self) -> dict[str, list[Match]]:
        matches = {}
        for pkg_id, fingerprints in self.by_package.items():
            matches[pkg_id] = [self.by_fingerprint[fingerprint] for fingerprint in fingerprints]
        return matches

    def merge(self, other: Matches) -> None:
        for fingerprints in other.by_package.values():
            for fingerprint in fingerprints:
                self.add(other.by_fingerprint[fingerprint])

    def diff(self, other: Matches) -> Matches:
        diff = Matches.new_matches()
        for fingerprint in self.by_fingerprint:
            if fingerprint not in other.by_fingerprint:
                diff.add(self.by_fingerprint[fingerprint])
        return diff

    def add(self, *matches: Match) -> None:
        if not matches:
            return

        for new_match in matches:
            fingerprint = new_match.fingerprint()

            if fingerprint in self.by_fingerprint:
                existing_match = self.by_fingerprint[fingerprint]
                try:
                    existing_match.merge(new_match)
                except ValueError as exc:
                    log_blocking(
                        "error",
                        (
                            "unable to merge matches: original"
                            f"={existing_match} new={new_match} : {exc}"
                        ),
                    )
                self.by_fingerprint[fingerprint] = existing_match
            else:
                self.by_fingerprint[fingerprint] = new_match

            self.by_package.setdefault(new_match.package.id, []).append(fingerprint)

    def enumerate(self) -> Iterator[Match]:
        yield from self.by_fingerprint.values()

    def count(self) -> int:
        return len(self.by_fingerprint)
