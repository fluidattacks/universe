from collections.abc import (
    Iterator,
)
from pathlib import (
    Path,
)

import tomlkit
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from tomlkit.exceptions import ParseError


def find_dependency_line_numbers(_content: str, dependency_name: str) -> list:
    return [
        str(line_number + 1)
        for line_number, line in enumerate(_content.splitlines(), start=1)
        if f'name = "{dependency_name}"' in line
    ]


def poetry_lock_deps(_content: str, path: str) -> Iterator[Dependency]:
    with Path(path).open(encoding="utf-8") as file:
        toml_content = tomlkit.parse(file.read())
    for package in toml_content.get("package", []):
        name = package["name"]
        version = package["version"]
        dependencies = list(package.get("dependencies", {}).keys())
        line = find_dependency_line_numbers(_content, name)
        if name and version:
            format_dep = Dependency(
                name=name.strip(),
                locations=DependencyLocation(path=path, line=line[0]),
                version=version.strip(),
                platform=Platform.PIP,
                found_by=MethodsEnum.LOCK_DEPS,
                depends_on=dependencies,
            )
            yield format_dep


def find_dep_line(_content: str, dep_name: str) -> list:
    return [
        str(line_number)
        for line_number, line in enumerate(_content.splitlines(), start=1)
        if line.strip(' "').startswith(dep_name)
    ]


def find_dep_version(dep_attrs: str | dict) -> str | None:
    dep_version = dep_attrs if isinstance(dep_attrs, str) else dep_attrs.get("version")

    if dep_version:
        return dep_version.replace(",", "")
    return None


def poetry_toml_deps(_content: str, path: str) -> Iterator[Dependency]:
    try:
        with Path(path).open(encoding="utf-8") as file:
            toml_content = tomlkit.parse(file.read())
    except ParseError:
        return

    project = toml_content.get("tool", {}).get("poetry", {})

    if not (deps := project.get("dependencies")):
        return

    if isinstance(deps, dict):
        for dep_name, dep_attrs in deps.items():
            dep_version = find_dep_version(dep_attrs)
            lines = find_dep_line(_content, dep_name)
            if not lines or not dep_version:
                continue
            format_dep = Dependency(
                name=dep_name.strip(),
                locations=DependencyLocation(path=path, line=lines[0]),
                version=dep_version.strip(),
                platform=Platform.PIP,
                found_by=MethodsEnum.POETRY_TOML_DEPS,
            )
            yield format_dep
