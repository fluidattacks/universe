import re
from collections.abc import (
    Iterator,
)

from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)

PUB_DEP: re.Pattern[str] = re.compile(r"^\s{2}(?P<pkg>[^\s]+):\s(?P<version>[^\s]*)$")


def pub_pubspec_yaml(*, content: str, path: str, is_dev: bool = False) -> Iterator[Dependency]:
    line_deps: bool = False
    look_up = "dev_dependencies:" if is_dev else "dependencies:"
    method = MethodsEnum.PUB_PUBSPEC_YAML_DEV if is_dev else MethodsEnum.PUB_PUBSPEC_YAML
    for line_number, line in enumerate(content.splitlines(), 1):
        if line.startswith(look_up):
            line_deps = True
        elif line_deps:
            if matched := PUB_DEP.match(line):
                pkg_name = matched.group("pkg")
                pkg_version = matched.group("version")
                yield Dependency(
                    name=pkg_name,
                    locations=DependencyLocation(path=path, line=str(line_number)),
                    version=pkg_version,
                    platform=Platform.PUB,
                    found_by=method,
                )
            elif not line:
                break
