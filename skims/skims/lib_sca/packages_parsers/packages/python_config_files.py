import ast
from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)

from lib_sast.sast_model import (
    Graph,
    GraphShard,
    GraphShardMetadataLanguage,
    NId,
)
from lib_sast.syntax_graph.parse_graph.parse import (
    parse_one,
)
from lib_sast.utils.graph import (
    adj,
    adj_ast,
    match_ast_group_d,
    matching_nodes,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.packages.conan import (
    get_conan_dep_info,
)
from utils.fs import (
    safe_sync_get_file_raw_content,
)


def format_conanfile_dep(
    *,
    shard: GraphShard,
    node_id: NId,
    path: str,
    dep_info: str | None = None,
    is_dev: bool = False,
) -> Dependency:
    method = MethodsEnum.CONAN_CONANFILE_PY_DEV if is_dev else MethodsEnum.CONAN_CONANFILE_PY
    dep_attrs = shard.graph.nodes[node_id]
    if dep_info is None:
        dep_info = dep_attrs.get("label_text") or node_to_str(shard.graph, node_id)
    dep_info = dep_info.replace("(", "").replace(")", "")
    product, version = get_conan_dep_info(dep_info)
    dep_line = dep_attrs["label_l"]
    return Dependency(
        name=product,
        locations=DependencyLocation(path=path, line=str(dep_line)),
        version=version,
        platform=Platform.CONAN,
        found_by=method,
    )


def get_attr_requirements(
    *,
    shard: GraphShard,
    syntax_graph: Graph,
    attr_info: dict,
    path: str,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    with suppress(SyntaxError):
        val_id = attr_info["value_id"]
        requires_info = syntax_graph.nodes[val_id]
        if requires_info["label_type"] == "Literal":
            requires_eval = ast.literal_eval(requires_info["value"])
            if isinstance(requires_eval, str):
                yield format_conanfile_dep(
                    shard=shard,
                    node_id=val_id,
                    path=path,
                    dep_info=requires_eval,
                    is_dev=is_dev,
                )
            else:
                for require in requires_eval:
                    yield format_conanfile_dep(
                        shard=shard,
                        node_id=val_id,
                        path=path,
                        dep_info=require,
                        is_dev=is_dev,
                    )
        elif requires_info["label_type"] == "ArrayInitializer":
            arr_elements_ids = adj_ast(syntax_graph, val_id)
            for elem_id in arr_elements_ids:
                elem_attrs = syntax_graph.nodes[elem_id]
                if elem_attrs["label_type"] == "ArrayInitializer":
                    elem_id = adj_ast(syntax_graph, elem_id)[0]  # noqa: PLW2901
                yield format_conanfile_dep(shard=shard, node_id=elem_id, path=path, is_dev=is_dev)


def get_method_requirements(
    *,
    shard: GraphShard,
    syntax_graph: Graph,
    method_id: NId,
    path: str,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    requirements_nodes = match_ast_group_d(syntax_graph, method_id, "MethodInvocation", depth=-1)
    requires = "self.tool_requires" if is_dev else "self.requires"
    for req_node in requirements_nodes:
        node_attrs = syntax_graph.nodes[req_node]
        if node_attrs["expression"] != requires:
            continue
        req_args_id = syntax_graph.nodes[req_node].get("arguments_id")
        req_args = adj_ast(syntax_graph, req_args_id)
        yield format_conanfile_dep(shard=shard, node_id=req_args[0], path=path, is_dev=is_dev)


def _resolve_deps(
    *,
    shard: GraphShard,
    conan_class_id: NId,
    path: str,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    syntax_graph = shard.syntax_graph
    requires = "tool_requires" if is_dev else "requires"
    requirements = "build_requirements" if is_dev else "requirements"
    class_block_id = syntax_graph.nodes[conan_class_id].get("block_id")
    class_attrs = adj_ast(syntax_graph, class_block_id)

    for attr in class_attrs:
        attr_info = syntax_graph.nodes[attr]
        if attr_info["label_type"] == "VariableDeclaration" and attr_info["variable"] == requires:
            yield from get_attr_requirements(
                shard=shard,
                syntax_graph=syntax_graph,
                attr_info=attr_info,
                path=path,
                is_dev=is_dev,
            )
        elif attr_info["label_type"] == "MethodDeclaration" and attr_info["name"] == requirements:
            yield from get_method_requirements(
                shard=shard,
                syntax_graph=syntax_graph,
                method_id=attr,
                path=path,
                is_dev=is_dev,
            )


def conan_conanfile_py(*, _content: str, path: str, is_dev: bool = False) -> Iterator[Dependency]:
    shard = parse_one(
        path,
        GraphShardMetadataLanguage.PYTHON,
        safe_sync_get_file_raw_content(path),
    )
    if not shard:
        return

    graph = shard.graph
    conan_class_id = None
    for node in matching_nodes(shard.syntax_graph, label_type="Class"):
        al_id = graph.nodes[node].get("label_field_superclasses")
        if al_id and any(
            graph.nodes[arg_id]["label_text"] == "ConanFile" for arg_id in adj(graph, al_id)
        ):
            conan_class_id = node
            break

    if not conan_class_id:
        return

    yield from _resolve_deps(shard=shard, conan_class_id=conan_class_id, path=path, is_dev=is_dev)
