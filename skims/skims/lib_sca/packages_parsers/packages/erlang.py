import re
from collections.abc import (
    Iterator,
)

from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)

MIX_DEP: re.Pattern[str] = re.compile(r"\{:(?P<dep>[\w]*),\s\"~>\s(?P<version>[\d.]+)\".+")
MIX_LOCK_DEP: re.Pattern[str] = re.compile(r"^\"(?P<dep>[\w]*)\":\{(?P<info>.+)\},")


def erlang_mix_deps(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    is_line_deps = False
    method = MethodsEnum.ERLANG_MIX_DEPS_DEV if is_dev else MethodsEnum.ERLANG_MIX_DEPS

    for line_number, line in enumerate(content.splitlines(), 1):
        line = line.strip()  # noqa: PLW2901
        if line == "defp deps do":
            is_line_deps = True
        elif is_line_deps:
            if line == "end":
                break
            if (matched := MIX_DEP.match(line)) and (
                (not is_dev and ":dev" not in line) or (is_dev and ":dev" in line)
            ):
                pkg_name = matched.group("dep")
                pkg_version = matched.group("version")
                yield Dependency(
                    name=pkg_name,
                    locations=DependencyLocation(path=path, line=str(line_number)),
                    version=pkg_version,
                    platform=Platform.ERLANG,
                    found_by=method,
                )


def erlang_mix_lock_deps(content: str, path: str) -> Iterator[Dependency]:
    for line_number, line in enumerate(content.splitlines(), 1):
        if matched := MIX_LOCK_DEP.match(line.replace(" ", "")):
            pkg_name = matched.group("dep")
            pkg_version = matched.group("info").split(",")[2].strip('"')
            yield Dependency(
                name=pkg_name,
                locations=DependencyLocation(path=path, line=str(line_number)),
                version=pkg_version,
                platform=Platform.ERLANG,
                found_by=MethodsEnum.ERLANG_MIX_LOCK_DEPS,
            )
