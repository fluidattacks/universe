from collections.abc import (
    Iterator,
)

from lib_sca.model import (
    SHIELD_BLOCKING,
    Dependency,
)
from lib_sca.packages_parsers.packages.cargo import (
    cargo_lock_deps,
    cargo_toml_deps,
)
from lib_sca.packages_parsers.packages.composer import (
    composer_json,
    composer_lock,
    composer_lock_dev,
)
from lib_sca.packages_parsers.packages.conan import (
    conan_conanfile_txt,
    conan_lock,
)
from lib_sca.packages_parsers.packages.erlang import (
    erlang_mix_deps,
    erlang_mix_lock_deps,
)
from lib_sca.packages_parsers.packages.gem import (
    gem_gemfile,
    gem_gemfile_lock,
)
from lib_sca.packages_parsers.packages.github_actions import (
    github_actions_deps,
)
from lib_sca.packages_parsers.packages.go import (
    go_mod,
)
from lib_sca.packages_parsers.packages.html_parser import (
    html_script_deps,
)
from lib_sca.packages_parsers.packages.maven import (
    gradle_wrapper_properties,
    maven_gradle,
    maven_gradle_kts_deps,
    maven_pom_xml,
    maven_sbt,
)
from lib_sca.packages_parsers.packages.npm import (
    npm_package_json,
    npm_package_lock_json,
    npm_yarn_lock,
    pnpm_package_lock,
)
from lib_sca.packages_parsers.packages.nuget import (
    net_runtime_config,
    nuget_csproj,
    nuget_pkgs_config,
    nuget_pkgs_lock_json,
)
from lib_sca.packages_parsers.packages.pip import (
    pip_requirements_txt,
)
from lib_sca.packages_parsers.packages.pipfile import (
    pipfile_deps,
    pipfile_lock,
)
from lib_sca.packages_parsers.packages.poetry import (
    poetry_lock_deps,
    poetry_toml_deps,
)
from lib_sca.packages_parsers.packages.pub import (
    pub_pubspec_yaml,
)
from lib_sca.packages_parsers.packages.python_config_files import (
    conan_conanfile_py,
)
from lib_sca.packages_parsers.packages.sbom_files import (
    sbom_deps,
)
from lib_sca.packages_parsers.packages.swift import (
    swift_package_deps,
)


@SHIELD_BLOCKING
def run_composer_json(content: str, path: str) -> Iterator[Dependency]:
    return composer_json(content, path)


@SHIELD_BLOCKING
def run_composer_lock(content: str, path: str) -> Iterator[Dependency]:
    return composer_lock(content, path)


@SHIELD_BLOCKING
def run_conan_conanfile_txt(content: str, path: str) -> Iterator[Dependency]:
    return conan_conanfile_txt(content, path)


@SHIELD_BLOCKING
def run_conan_lock(content: str, path: str) -> Iterator[Dependency]:
    return conan_lock(content, path)


@SHIELD_BLOCKING
def run_gem_gemfile(content: str, path: str) -> Iterator[Dependency]:
    return gem_gemfile(content, path)


@SHIELD_BLOCKING
def run_gem_gemfile_lock(content: str, path: str) -> Iterator[Dependency]:
    return gem_gemfile_lock(content, path)


@SHIELD_BLOCKING
def run_go_mod(content: str, path: str) -> Iterator[Dependency]:
    return go_mod(content, path)


@SHIELD_BLOCKING
def run_maven_pom_xml(content: str, path: str) -> Iterator[Dependency]:
    return maven_pom_xml(content, path)


@SHIELD_BLOCKING
def run_maven_gradle(content: str, path: str) -> Iterator[Dependency]:
    return maven_gradle(content, path)


@SHIELD_BLOCKING
def run_maven_gradle_kts(content: str, path: str) -> Iterator[Dependency]:
    return maven_gradle_kts_deps(content, path)


@SHIELD_BLOCKING
def run_maven_sbt(content: str, path: str) -> Iterator[Dependency]:
    return maven_sbt(content, path)


@SHIELD_BLOCKING
def run_npm_yarn_lock(content: str, path: str) -> Iterator[Dependency]:
    return npm_yarn_lock(content, path)


@SHIELD_BLOCKING
def run_nuget_csproj(content: str, path: str) -> Iterator[Dependency]:
    return nuget_csproj(content, path)


@SHIELD_BLOCKING
def run_nuget_pkgs_lock_json(content: str, path: str) -> Iterator[Dependency]:
    return nuget_pkgs_lock_json(content, path)


@SHIELD_BLOCKING
def run_nuget_pkgs_config(content: str, path: str) -> Iterator[Dependency]:
    return nuget_pkgs_config(content, path)


@SHIELD_BLOCKING
def run_net_runtime_config(content: str, path: str) -> Iterator[Dependency]:
    return net_runtime_config(content, path)


@SHIELD_BLOCKING
def run_npm_package_json(content: str, path: str) -> Iterator[Dependency]:
    return npm_package_json(content, path)


@SHIELD_BLOCKING
def run_npm_package_lock_json(content: str, path: str) -> Iterator[Dependency]:
    return npm_package_lock_json(content, path)


@SHIELD_BLOCKING
def run_pnpm_package_lock(content: str, path: str) -> Iterator[Dependency]:
    return pnpm_package_lock(content, path)


@SHIELD_BLOCKING
def run_pip_requirements_txt(content: str, path: str) -> Iterator[Dependency]:
    return pip_requirements_txt(content, path)


@SHIELD_BLOCKING
def run_pipfile_deps(content: str, path: str) -> Iterator[Dependency]:
    return pipfile_deps(content, path)


@SHIELD_BLOCKING
def run_pipfile_lock(content: str, path: str) -> Iterator[Dependency]:
    return pipfile_lock(content, path)


@SHIELD_BLOCKING
def run_poetry_lock_deps(content: str, path: str) -> Iterator[Dependency]:
    return poetry_lock_deps(content, path)


@SHIELD_BLOCKING
def run_poetry_toml_deps(content: str, path: str) -> Iterator[Dependency]:
    return poetry_toml_deps(content, path)


@SHIELD_BLOCKING
def run_pub_pubspec_yaml(content: str, path: str) -> Iterator[Dependency]:
    return pub_pubspec_yaml(content=content, path=path)


@SHIELD_BLOCKING
def run_cargo_toml(content: str, path: str) -> Iterator[Dependency]:
    return cargo_toml_deps(content, path)


@SHIELD_BLOCKING
def run_cargo_lock(content: str, path: str) -> Iterator[Dependency]:
    return cargo_lock_deps(content, path)


@SHIELD_BLOCKING
def run_erlang_mix_deps(content: str, path: str) -> Iterator[Dependency]:
    return erlang_mix_deps(content, path)


@SHIELD_BLOCKING
def run_erlang_mix_lock_deps(content: str, path: str) -> Iterator[Dependency]:
    return erlang_mix_lock_deps(content, path)


@SHIELD_BLOCKING
def run_swift_package_deps(content: str, path: str) -> Iterator[Dependency]:
    return swift_package_deps(content, path)


@SHIELD_BLOCKING
def run_html_script_deps(content: str, path: str) -> Iterator[Dependency]:
    return html_script_deps(content, path)


@SHIELD_BLOCKING
def run_github_actions_deps(content: str, path: str) -> Iterator[Dependency]:
    return github_actions_deps(content, path)


@SHIELD_BLOCKING
def run_sbom_deps(content: str, path: str) -> Iterator[Dependency]:
    return sbom_deps(content, path)


@SHIELD_BLOCKING
def run_conanfile_deps(content: str, path: str) -> Iterator[Dependency]:
    return conan_conanfile_py(_content=content, path=path)


@SHIELD_BLOCKING
def run_gradle_wrapper_properties(content: str, path: str) -> Iterator[Dependency]:
    return gradle_wrapper_properties(content, path)


@SHIELD_BLOCKING
def parse_dependencies(
    file_content: str,
    file_name: str,
    file_extension: str,
    path: str,
) -> Iterator[Dependency]:
    func_dict = {
        "build.sbt": run_maven_sbt,
        "Cargo.toml": run_cargo_toml,
        "Cargo.lock": run_cargo_lock,
        "mix.exs": run_erlang_mix_deps,
        "mix.lock": run_erlang_mix_lock_deps,
        "package.json": run_npm_package_json,
        "yarn.lock": run_npm_yarn_lock,
        "package-lock.json": run_npm_package_lock_json,
        "pnpm-lock.yaml": run_pnpm_package_lock,
        "packages.config": run_nuget_pkgs_config,
        "pubspec.yaml": run_pub_pubspec_yaml,
        "composer.json": run_composer_json,
        "composer.lock": run_composer_lock,
        "conanfile.txt": run_conan_conanfile_txt,
        "conan.lock": run_conan_lock,
        "Pipfile.lock": run_pipfile_lock,
        "poetry.lock": run_poetry_lock_deps,
        "pyproject.toml": run_poetry_toml_deps,
        "Gemfile.lock": run_gem_gemfile_lock,
        "gems.locked": run_gem_gemfile_lock,
        "go.mod": run_go_mod,
        "Package.resolved": run_swift_package_deps,
        "build.gradle.kts": run_maven_gradle_kts,
        "packages.lock.json": run_nuget_pkgs_lock_json,
        "conanfile.py": run_conanfile_deps,
        "gradle-wrapper.properties": run_gradle_wrapper_properties,
    }

    func_dict_extensions = {
        "csproj": run_nuget_csproj,
        "gradle": run_maven_gradle,
        "html": run_html_script_deps,
        "xml": run_maven_pom_xml,
        "json": run_sbom_deps,
        "txt": run_pip_requirements_txt,
    }

    func_dict_file_names = {
        "Pipfile": run_pipfile_deps,
        "Gemfile": run_gem_gemfile,
    }

    full_file = f"{file_name}.{file_extension}"
    if (
        callable_func := func_dict.get(full_file)
        or func_dict_extensions.get(file_extension)
        or func_dict_file_names.get(file_name)
    ):
        return callable_func(file_content, path)

    if ".github/workflows" in path and file_extension in {"yml", "yaml"}:
        return run_github_actions_deps(file_content, path)

    if file_name.endswith(".exe") and file_extension == "config":
        return run_net_runtime_config(file_content, path)

    return iter([])


@SHIELD_BLOCKING
def run_composer_json_dev(content: str, path: str) -> Iterator[Dependency]:
    return composer_json(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_composer_lock_dev(content: str, path: str) -> Iterator[Dependency]:
    return composer_lock_dev(content, path)


@SHIELD_BLOCKING
def run_conan_conanfile_txt_dev(content: str, path: str) -> Iterator[Dependency]:
    return conan_conanfile_txt(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_conan_lock_dev(content: str, path: str) -> Iterator[Dependency]:
    return conan_lock(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_gem_gemfile_dev(content: str, path: str) -> Iterator[Dependency]:
    return gem_gemfile(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_npm_package_json_dev(content: str, path: str) -> Iterator[Dependency]:
    return npm_package_json(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_npm_pkg_lock_json_dev(content: str, path: str) -> Iterator[Dependency]:
    return npm_package_lock_json(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_pnpm_lock_dev(content: str, path: str) -> Iterator[Dependency]:
    return pnpm_package_lock(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_npm_yarn_lock_dev(content: str, path: str) -> Iterator[Dependency]:
    return npm_yarn_lock(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_pub_pubspec_yaml_dev(content: str, path: str) -> Iterator[Dependency]:
    return pub_pubspec_yaml(content=content, path=path, is_dev=True)


@SHIELD_BLOCKING
def run_cargo_toml_dev(content: str, path: str) -> Iterator[Dependency]:
    return cargo_toml_deps(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_erlang_deps_dev(content: str, path: str) -> Iterator[Dependency]:
    return erlang_mix_deps(content, path, is_dev=True)


@SHIELD_BLOCKING
def run_conanfile_deps_dev(content: str, path: str) -> Iterator[Dependency]:
    return conan_conanfile_py(_content=content, path=path, is_dev=True)


@SHIELD_BLOCKING
def parse_dependencies_dev(
    file_content: str,
    file_name: str,
    file_extension: str,
    path: str,
) -> Iterator[Dependency]:
    if file_name == "Gemfile":
        return run_gem_gemfile_dev(file_content, path)

    func_dict = {
        "package.json": run_npm_package_json_dev,
        "yarn.lock": run_npm_yarn_lock_dev,
        "package-lock.json": run_npm_pkg_lock_json_dev,
        "pnpm-lock.yaml": run_pnpm_lock_dev,
        "pubspec.yaml": run_pub_pubspec_yaml_dev,
        "composer.json": run_composer_json_dev,
        "composer.lock": run_composer_lock_dev,
        "conanfile.txt": run_conan_conanfile_txt_dev,
        "conan.lock": run_conan_lock_dev,
        "Cargo.toml": run_cargo_toml_dev,
        "mix.exs": run_erlang_deps_dev,
        "conanfile.py": run_conanfile_deps_dev,
    }

    full_file = f"{file_name}.{file_extension}"
    if callable_func := func_dict.get(full_file):
        return callable_func(file_content, path)

    return iter([])
