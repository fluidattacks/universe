from collections.abc import (
    Iterator,
)

from frozendict import (
    frozendict,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.json_custom_parser import (
    loads_blocking as json_loads_blocking,
)


def adjust_version(version: str) -> str:
    return version.replace(",", "||")


def composer_json(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    require = "require-dev" if is_dev else "require"
    method = MethodsEnum.COMPOSER_JSON_DEV if is_dev else MethodsEnum.COMPOSER_JSON
    content_json = json_loads_blocking(content, default={})
    dependencies: Iterator[Dependency] = (
        Dependency(
            name=product["item"],
            locations=DependencyLocation(path=path, line=str(product["line"])),
            version=adjust_version(version["item"]),
            platform=Platform.COMPOSER,
            found_by=method,
        )
        for key in content_json
        if key["item"] == require
        for product, version in content_json[key].items()
    )

    return dependencies


def composer_lock(content: str, path: str) -> Iterator[Dependency]:
    content_json = json_loads_blocking(content, default={})
    for key in content_json:
        if key["item"] == "packages":
            for line in content_json[key]["item"]:
                cont = 0
                info = []
                for product in line.values():
                    if cont >= 2:
                        cont = 0
                        break
                    cont += 1
                    info.append(product)

                yield Dependency(
                    name=info[0]["item"],
                    locations=DependencyLocation(path=path, line=str(info[0]["line"])),
                    version=info[1]["item"],
                    platform=Platform.COMPOSER,
                    found_by=MethodsEnum.COMPOSER_LOCK,
                )


def get_package_data(package: frozendict, path: str) -> Dependency | None:
    name: frozendict[str, str | int] | None = None
    version: frozendict[str, str | int] | None = None

    for property_label, property_value in package.items():
        if name is None and property_label.get("item") == "name":
            name = property_value
        if version is None and property_label.get("item") == "version":
            version = property_value
        if name and version:
            return Dependency(
                name=str(name["item"]),
                locations=DependencyLocation(path=path, line=str(name["line"])),
                version=str(version["item"]),
                platform=Platform.COMPOSER,
                found_by=MethodsEnum.COMPOSER_LOCK_DEV,
            )
    return None


def composer_lock_dev(content: str, path: str) -> Iterator[Dependency]:
    content_json = json_loads_blocking(content, default={})

    for package_label, package in content_json.items():
        if package_label.get("item") == "packages-dev" and (dev_packages := package.get("item")):
            for dev_package in dev_packages:
                if package_data := get_package_data(dev_package, path):
                    yield package_data
