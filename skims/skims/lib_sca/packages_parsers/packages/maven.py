import glob
import os
import re
from collections.abc import (
    Iterator,
)
from pathlib import (
    Path,
)

from bs4 import (
    BeautifulSoup,
    Tag,
    element,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from utils.fs import (
    get_file_content_block,
)

# Constants
QUOTE = r'["\']'
NL = r"(\n?\s*)?"
TEXT = r'[^"\']+'

# Regexes
RE_LINE_COMMENT: re.Pattern[str] = re.compile(r"^.*" rf"{NL}//" r".*$")  # noqa: ISC001
RE_SBT: re.Pattern[str] = re.compile(
    r"^[^%]*"
    rf"{NL}{QUOTE}(?P<group>{TEXT}){QUOTE}{NL}%"
    rf"{NL}{QUOTE}(?P<name>{TEXT}){QUOTE}{NL}%"
    rf"{NL}{QUOTE}(?P<version>{TEXT}){QUOTE}{NL}"
    r".*$",
)
RE_GRADLE_KTS: re.Pattern[str] = re.compile(
    rf"(runtimeOnly|api|compile|compileOnly|implementation)\({QUOTE}"
    rf"(?P<group>{TEXT}(:{TEXT})*):(?P<name>{TEXT})"
    rf"(:{TEXT})?:(?P<version>{TEXT}){QUOTE}\)",
)


def get_line_number(content: str, match_start: int) -> int:
    return content[:match_start].count("\n") + 2


def build_regex_with_configs(configs: set) -> dict[str, re.Pattern[str]]:
    config_pattern = "|".join(configs)
    return {
        "RE_GRADLE_A": re.compile(
            rf"^{NL}(?:{config_pattern}){NL}[(]?{NL}"
            rf"group{NL}:{NL}{QUOTE}(?P<group>{TEXT}){QUOTE}{NL},"
            rf"{NL}name{NL}:{NL}{QUOTE}(?P<name>{TEXT}){QUOTE}{NL}"
            rf"(?:,{NL}version{NL}:{NL}{QUOTE}(?P<version>{TEXT}){QUOTE}{NL})"
            rf"?.*$",
        ),
        "RE_GRADLE_B": re.compile(
            rf"^.*{NL}(?:{config_pattern}){NL}[(]?{NL}{QUOTE}(?P<statement>{TEXT}){QUOTE}",
        ),
        "RE_GRADLE_C": re.compile(
            rf"{NL}(?:{config_pattern}){NL}\("
            rf"{NL}{QUOTE}(?P<statement>{TEXT}){QUOTE}{NL}\)"
            rf"{NL}{{({NL})version{NL}{{({NL})strictly{NL}\({NL}"
            rf"{QUOTE}(?P<version>{TEXT}){QUOTE}{NL}\){NL}}}{NL}}}",
            re.DOTALL,
        ),
        "BLOCK": re.compile(
            rf"{NL}(?:{config_pattern}){NL}\("
            rf"{NL}{QUOTE}(?P<statement>{TEXT}){QUOTE}{NL}\)"
            rf"{NL}\{{(.*?version{NL}\{{.*?\}}){NL}\}}",
            re.DOTALL,
        ),
        "VERSION": re.compile(
            rf"version{NL}{{({NL})strictly{NL}\("
            rf"{NL}{QUOTE}(?P<version>{TEXT}){QUOTE}{NL}\){NL}}}",
            re.DOTALL,
        ),
    }


def get_text_or_empty(tag: Tag | None) -> str:
    if not tag:
        return ""
    return tag.get_text() if isinstance(tag, Tag) else ""


def get_attrs(parent_ref: Tag) -> dict[str, str]:
    if not parent_ref:
        return {}

    return {
        "group": get_text_or_empty(parent_ref.groupid),
        "artifact": get_text_or_empty(parent_ref.artifactid),
        "version": get_text_or_empty(parent_ref.version),
    }


def extract_gradle_configs(content: str) -> set[str]:
    config_pattern = re.compile(r"configurations\s*\{([^}]+)\}", re.DOTALL)
    custom_config_pattern = re.compile(r"\s*(\w+)\s*")
    configs = {
        "runtimeOnly",
        "api",
        "compile",
        "compileOnly",
        "implementation",
    }

    config_blocks = config_pattern.findall(content)
    for block in config_blocks:
        custom_configs = custom_config_pattern.findall(block)
        configs.update(custom_configs)

    return configs


def avoid_cmt(line: str, *, is_block_cmt: bool) -> tuple[str, bool]:
    if RE_LINE_COMMENT.match(line):
        line = line.split("//", 1)[0]
    if is_block_cmt:
        if "*/" in line:
            is_block_cmt = False
            line = line.split("*/", 1).pop()
        else:
            return "", is_block_cmt
    if "/*" in line:
        line_cmt_open = line.split("/*", 1)[0]
        if "*/" in line:
            line = line_cmt_open + line.split("*/", 1).pop()
        else:
            line = line_cmt_open
            is_block_cmt = True
    return line, is_block_cmt


def get_block_deps(
    content: str,
    path: str,
    regexes: dict[str, re.Pattern[str]],
) -> Iterator[Dependency]:
    for block in regexes["BLOCK"].finditer(content):
        product = block.group("statement")
        hit = regexes["VERSION"].search(block.group())
        if hit:
            version = hit.group("version")
            line_no = get_line_number(content, block.start())

            if version == "":
                continue

            yield Dependency(
                name=product,
                locations=DependencyLocation(
                    path=path,
                    line=str(line_no),
                ),
                version=version,
                platform=Platform.MAVEN,
                found_by=MethodsEnum.MAVEN_GRADLE,
            )


def maven_gradle(content: str, path: str) -> Iterator[Dependency]:
    configs = extract_gradle_configs(content)
    regexes = build_regex_with_configs(configs)
    yield from get_block_deps(content, path, regexes)

    is_block_cmt = False
    for line_no, line in enumerate(content.splitlines(), start=1):
        line, is_block_cmt = avoid_cmt(line, is_block_cmt=is_block_cmt)  # noqa: PLW2901
        if match := regexes["RE_GRADLE_A"].match(line):
            product = match.group("group") + ":" + match.group("name")
            version = match.group("version") or ""
        elif match := regexes["RE_GRADLE_B"].match(line):
            statement = match.group("statement")
            product, version = (
                statement.rsplit(":", maxsplit=1) if statement.count(":") >= 2 else (statement, "")
            )
        else:
            continue

        # Assuming a wildcard in Maven if the version is not found can
        # result in issues.
        # https://gitlab.com/fluidattacks/universe/-/issues/5635
        if version == "" or re.match(r"\${.*}", version):
            continue

        yield Dependency(
            name=product,
            locations=DependencyLocation(
                path=path,
                line=str(line_no),
            ),
            version=version,
            platform=Platform.MAVEN,
            found_by=MethodsEnum.MAVEN_GRADLE,
        )


def get_pom_xml(content: str) -> BeautifulSoup | None:
    if (
        (root := BeautifulSoup(content, features="lxml"))
        and root.project
        and (xmlns := root.project.get("xmlns"))
        and str(xmlns) == "http://maven.apache.org/POM/4.0.0"
    ):
        return root
    return None


def _get_properties(root: BeautifulSoup) -> dict[str, str]:
    return {
        _property.name.lower(): _property.get_text()
        for properties in root.find_all("properties", limit=2)
        for _property in properties.children
        if isinstance(_property, element.Tag)
    }


def _get_deps_management(pom_tree: BeautifulSoup) -> dict[str, str]:
    deps_info: dict[str, str] = {}
    for manage in pom_tree.find_all("dependencymanagement"):
        for dependency in manage.find_all("dependency", recursive=True):
            if not (dependency.groupid and dependency.artifactid and dependency.version):
                continue

            group = dependency.groupid.get_text()
            artifact = dependency.artifactid.get_text()
            version = dependency.version.get_text()
            deps_info[f"{group}:{artifact}"] = version
    return deps_info


def _find_vars(value: str, properties: dict[str, str]) -> str:
    if not value.startswith("${"):
        return value
    value = re.sub(r"[\$\{\}]", "", value)
    return properties.get(value, "")


def _add_properties_vars(properties_vars: dict[str, str], root_pom: BeautifulSoup) -> None:
    properties = _get_properties(root_pom)
    properties_vars.update(properties)


def _is_parent_pom(root_pom: BeautifulSoup, parent_attrs: dict[str, str]) -> bool:
    project = root_pom.project
    if not isinstance(project, Tag):
        return False
    group = project.find("groupid", recursive=False)
    artifact = project.find("artifactid", recursive=False)
    version = project.find("version", recursive=False)
    if not (group and artifact and version):
        return False

    return (
        group.get_text() == parent_attrs["group"]
        and artifact.get_text() == parent_attrs["artifact"]
        and version.get_text() == parent_attrs["version"]
    )


def _is_module_parent(parent_pom: BeautifulSoup, pom_module: str, file_dir: str) -> bool:
    for modules in parent_pom.find_all("modules"):
        for module in modules.find_all("module"):
            mod_name = module.get_text()
            module_path = os.path.normpath(os.path.join(file_dir, mod_name))  # noqa: PTH118
            if module_path == pom_module:
                return True
    return False


def get_deps_and_vars(
    path: str,
    root: BeautifulSoup,
) -> tuple[dict[str, str], dict[str, str]]:
    parent_ref = root.find("parent")
    if not isinstance(parent_ref, Tag):
        return {}, {}
    parent_attrs = get_attrs(parent_ref)
    properties_vars: dict[str, str] = {}
    manage_deps: dict[str, str] = {}
    if parent_attrs:
        root_path = os.getcwd()  # noqa: PTH109
        res_path = Path(path).resolve()
        pom_files = glob.glob(f"{root_path}/**/*.xml", recursive=True)  # noqa: PTH207
        for pom_file in pom_files:
            content = get_file_content_block(pom_file)
            if (
                (pom_xml_tree := get_pom_xml(content))
                and _is_parent_pom(pom_xml_tree, parent_attrs)
                and _is_module_parent(
                    pom_xml_tree,
                    os.path.dirname(res_path),  # noqa: PTH120
                    os.path.dirname(pom_file),  # noqa: PTH120
                )
            ):
                _add_properties_vars(properties_vars, pom_xml_tree)
                manage_deps.update(_get_deps_management(pom_xml_tree))
                break

    manage_deps.update(_get_deps_management(root))
    return properties_vars, manage_deps


def _resolve_pom_xml_deps(
    dependencies: Tag,
    properties: dict[str, str],
    manage_deps: dict[str, str],
    path: str,
) -> Iterator[Dependency]:
    for dependency in dependencies.find_all("dependency", recursive=False):
        group = dependency.groupid
        artifact = dependency.artifactid
        version = dependency.find("version")
        g_text = _find_vars(group.get_text(), properties)
        a_text = _find_vars(artifact.get_text(), properties)
        product = f"{g_text}:{a_text}"
        if version is None:
            managed_version = manage_deps.get(product)
            if not managed_version:
                continue
            v_text = _find_vars(managed_version, properties)
            line = artifact.sourceline
            yield Dependency(
                name=product,
                locations=DependencyLocation(
                    path=path,
                    line=str(line),
                ),
                version=v_text,
                platform=Platform.MAVEN,
                found_by=MethodsEnum.MAVEN_POM_XML,
            )
        else:
            v_text = _find_vars(version.get_text(), properties)
            line = version.sourceline
            yield Dependency(
                name=product,
                locations=DependencyLocation(
                    path=path,
                    line=str(line),
                ),
                version=v_text,
                platform=Platform.MAVEN,
                found_by=MethodsEnum.MAVEN_POM_XML,
            )


def maven_pom_xml(content: str, path: str) -> Iterator[Dependency]:
    root = BeautifulSoup(content, features="html.parser")
    if (
        (project := root.project)
        and str(project.get("xmlns")) == "http://maven.apache.org/POM/4.0.0"
        and (dependencies := project.find("dependencies", recursive=False))
        and isinstance(dependencies, Tag)
    ):
        if root.find("parent"):
            properties, manage_deps = get_deps_and_vars(path, root)
        else:
            properties = _get_properties(root)
            manage_deps = _get_deps_management(root)

        yield from _resolve_pom_xml_deps(dependencies, properties, manage_deps, path)


def maven_sbt(content: str, path: str) -> Iterator[Dependency]:
    for line_no, line in enumerate(content.splitlines(), start=1):
        if match := RE_SBT.match(line):
            product: str = match.group("group") + ":" + match.group("name")
            version = match.group("version")
        else:
            continue

        yield Dependency(
            name=product,
            locations=DependencyLocation(
                path=path,
                line=str(line_no),
            ),
            version=version,
            platform=Platform.MAVEN,
            found_by=MethodsEnum.MAVEN_SBT,
        )


def is_comment(line: str) -> bool:
    return (
        line.strip().startswith("//")
        or line.strip().startswith("/*")
        or line.strip().endswith("*/")
    )


def maven_gradle_kts_deps(content: str, path: str) -> Iterator[Dependency]:
    is_block_comment = False
    for line_no, line in enumerate(content.splitlines(), start=1):
        line = line.strip()  # noqa: PLW2901
        # Verify if is inline comment or block comment
        if "/*" in line:
            is_block_comment = True
        if "*/" in line:
            is_block_comment = False
            continue

        if is_block_comment or is_comment(line):
            continue

        if (
            not is_block_comment
            and (match := RE_GRADLE_KTS.match(line))
            and (product := match.group("group") + ":" + match.group("name"))
            and (version := match.group("version"))
        ):
            yield Dependency(
                name=product,
                locations=DependencyLocation(
                    path=path,
                    line=str(line_no),
                ),
                version=version,
                platform=Platform.MAVEN,
                found_by=MethodsEnum.BUILD_GRADLE_KTS,
            )


def gradle_wrapper_properties(content: str, path: str) -> Iterator[Dependency]:
    gradle_dist = re.compile("^distributionUrl=.+gradle-(?P<gradle_version>[^-]+)-.+")

    for line_no, line in enumerate(content.splitlines(), start=1):
        line = line.strip()  # noqa: PLW2901
        if (match := gradle_dist.match(line)) and (version := match.group("gradle_version")):
            yield Dependency(
                name="gradle",
                locations=DependencyLocation(
                    path=path,
                    line=str(line_no),
                ),
                version=version,
                platform=Platform.MAVEN,
                found_by=MethodsEnum.GRADLE_WRAPPER_PROPERTIES,
            )
