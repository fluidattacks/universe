import re
from collections.abc import (
    Iterator,
)

from frozendict import (
    frozendict,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.json_custom_parser import (
    loads_blocking as json_loads_blocking,
)


def get_conan_dep_info(dep_line: str) -> tuple[str, str]:
    product, version = re.sub(r"[\"\]\[]", "", dep_line).strip().split("@")[0].split("/")
    if "," in version:
        version = re.sub(r",(?=[<>=])", " ", version).split(",")[0]
    return product, version


def format_conan_lock_dep(dep_info: frozendict, path: str, *, is_dev: bool = False) -> Dependency:
    product, version = dep_info["item"].split("/")
    version = version.split("#")[0]
    dep_line = dep_info["line"]
    method = MethodsEnum.CONAN_LOCK_DEV if is_dev else MethodsEnum.CONAN_LOCK
    return Dependency(
        name=product,
        version=version,
        locations=DependencyLocation(path=path, line=str(dep_line), lines=[dep_line]),
        platform=Platform.CONAN,
        found_by=method,
    )


def resolve_conan_lock_deps(
    content: str,
    path: str,
    *,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    requires = "build_requires" if is_dev else "requires"
    content_json = json_loads_blocking(content, default={})
    dependencies: Iterator[Dependency] = (
        format_conan_lock_dep(dep_info, path, is_dev=is_dev)
        for key in content_json
        if key["item"] == requires
        for dep_info in content_json[key]["item"]
    )
    return dependencies


def conan_conanfile_txt(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    line_deps: bool = False
    for line_number, line in enumerate(content.splitlines(), 1):
        if is_dev and re.search(r"^\[(tool|build)_requires\]$", line):
            line_deps = True
            method = MethodsEnum.CONAN_CONANFILE_TXT_DEV
        elif not is_dev and line.startswith("[requires]"):
            line_deps = True
            method = MethodsEnum.CONAN_CONANFILE_TXT
        elif line_deps:
            if not line or line.startswith("["):
                break
            pkg_name, pkg_version = get_conan_dep_info(line)
            yield Dependency(
                name=pkg_name,
                locations=DependencyLocation(path=path, line=str(line_number)),
                version=pkg_version,
                platform=Platform.CONAN,
                found_by=method,
            )


def conan_lock(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    return resolve_conan_lock_deps(content, path, is_dev=is_dev)
