import re
from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from bs4.element import (
    Tag,
)
from frozendict import (
    frozendict,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.json_custom_parser import (
    loads_blocking as json_loads_blocking,
)

PACKAGE = re.compile(r".+\\packages\\(?P<package_info>[^\s\\]*)\\.+")
DEP_INFO = re.compile(r"(?P<package_name>[^\d]*)\.(?P<version>[\d.]+)")
HTML_PARSER = "html.parser"


def _format_csproj_reference_deps(root: BeautifulSoup, path: str) -> Iterator[Dependency]:
    for pkg in root.find_all("reference", recursive=True):
        if dll_path := pkg.find("hintpath"):
            package = PACKAGE.match(dll_path.text)
            if package and (pkg_info := DEP_INFO.match(package.group("package_info"))):
                line = dll_path.sourceline
                yield Dependency(
                    name=pkg_info.group("package_name").lower(),
                    locations=DependencyLocation(
                        path=path,
                        line=str(
                            line,
                        ),
                    ),
                    version=pkg_info.group("version"),
                    platform=Platform.NUGET,
                    found_by=MethodsEnum.NUGET_CSPROJ,
                )

        elif (include := pkg.get("include")) and (
            include_info := include.replace(" ", "").split(",")
        ):
            pkg_name = str(include_info[0]).strip()
            version = next(
                (
                    pkg_info.lstrip("Version=")
                    for pkg_info in include_info
                    if pkg_info.startswith("Version=")
                ),
                None,
            )
            line = pkg.sourceline
            if version:
                yield Dependency(
                    name=pkg_name,
                    locations=DependencyLocation(
                        path=path,
                        line=str(
                            line,
                        ),
                    ),
                    version=version,
                    platform=Platform.NUGET,
                    found_by=MethodsEnum.NUGET_CSPROJ,
                )


def nuget_csproj(content: str, path: str) -> Iterator[Dependency]:
    root = BeautifulSoup(content, features=HTML_PARSER)

    for pkg in root.find_all("packagereference", recursive=True):
        if (id_ := pkg.get("include")) and (version := pkg.get("version")):
            line = pkg.sourceline

            yield Dependency(
                name=id_,
                locations=DependencyLocation(
                    path=path,
                    line=str(
                        line,
                    ),
                ),
                version=version,
                platform=Platform.NUGET,
                found_by=MethodsEnum.NUGET_CSPROJ,
            )

    yield from _format_csproj_reference_deps(root, path)


def nuget_pkgs_config(content: str, path: str) -> Iterator[Dependency]:
    root = BeautifulSoup(content, features="html.parser")

    for pkg in root.find_all("package", recursive=True):
        if (id_ := pkg.get("id")) and (version := pkg.get("version")):
            line = pkg.sourceline

            yield Dependency(
                name=id_,
                locations=DependencyLocation(
                    path=path,
                    line=str(
                        line,
                    ),
                ),
                version=version,
                platform=Platform.NUGET,
                found_by=MethodsEnum.NUGET_PACKAGES_CONFIG,
            )


def net_runtime_config(content: str, path: str) -> Iterator[Dependency]:
    root = BeautifulSoup(content, features=HTML_PARSER)
    net_dep = re.compile(r".NETFramework,Version=v(?P<version>[^\s,]*)")
    net_runtime = root.find("supportedruntime")
    if (
        isinstance(net_runtime, Tag)
        and isinstance(net_runtime.parent, Tag)
        and net_runtime.parent.name == "startup"
        and (runtime_info := net_runtime.get("sku", ""))
        and (version := net_dep.match(str(runtime_info)))
    ):
        line = net_runtime.sourceline
        yield Dependency(
            name="netframework",
            locations=DependencyLocation(
                path=path,
                line=str(
                    line,
                ),
            ),
            version=version.group("version"),
            platform=Platform.NUGET,
            found_by=MethodsEnum.NET_FRAMEWORK_CONFIG,
        )


def resolve_nuget_pkgs_lock_json(
    dep: frozendict,
    content: frozendict,
    path: str,
) -> Iterator[Dependency]:
    for key in content:
        if key["item"] == "resolved":
            version = content[key]["item"]
            yield Dependency(
                name=dep["item"],
                locations=DependencyLocation(
                    path=path,
                    line=str(
                        dep["line"],
                    ),
                ),
                version=version,
                platform=Platform.NUGET,
                found_by=MethodsEnum.NUGET_PKGS_LOCK_JSON,
            )


def nuget_pkgs_lock_json(content: str, path: str) -> Iterator[Dependency]:
    content_json = json_loads_blocking(content, default={})
    for key in content_json:
        if key["item"] == "dependencies":
            for pkgs in content_json[key]:
                for dep in content_json[key][pkgs]:
                    yield from resolve_nuget_pkgs_lock_json(dep, content_json[key][pkgs][dep], path)
