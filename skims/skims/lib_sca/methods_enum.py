from enum import (
    Enum,
)

from model.core import (
    DeveloperEnum,
    FindingEnum,
    MethodInfo,
    MethodOriginEnum,
    VulnerabilityTechnique,
)


class MethodsEnumSCA(Enum):
    COMPOSER_JSON = MethodInfo(
        file_name="composer",
        name="composer_json",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    COMPOSER_LOCK = MethodInfo(
        file_name="composer",
        name="composer_lock",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONAN_LOCK = MethodInfo(
        file_name="conan",
        name="conan_lock",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONAN_CONANFILE_PY = MethodInfo(
        file_name="python_config_files",
        name="conan_conanfile_py",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONAN_CONANFILE_TXT = MethodInfo(
        file_name="conan",
        name="conan_conanfile_txt",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    GEM_GEMFILE = MethodInfo(
        file_name="gem",
        name="gem_gemfile",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    GEM_GEMFILE_LOCK = MethodInfo(
        file_name="gem",
        name="gem_gemfile_lock",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    GO_MOD = MethodInfo(
        file_name="go",
        name="go_mod",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    MAVEN_POM_XML = MethodInfo(
        file_name="maven",
        name="maven_pom_xml",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.ANDRES_CUBEROS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    MAVEN_GRADLE = MethodInfo(
        file_name="maven",
        name="maven_gradle",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.ANDRES_CUBEROS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    BUILD_GRADLE_KTS = MethodInfo(
        file_name="maven",
        name="build_gradle_kts",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.EXTERNAL,
        auto_approve=True,
    )
    GRADLE_WRAPPER_PROPERTIES = MethodInfo(
        file_name="maven",
        name="gradle_wrapper_properties",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.EXTERNAL,
        auto_approve=True,
    )
    MAVEN_SBT = MethodInfo(
        file_name="maven",
        name="maven_sbt",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.ANDRES_CUBEROS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NPM_YARN_LOCK = MethodInfo(
        file_name="npm",
        name="npm_yarn_lock",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NPM_PACKAGE_JSON = MethodInfo(
        file_name="npm",
        name="npm_package_json",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NPM_PACKAGE_LOCK_JSON = MethodInfo(
        file_name="npm",
        name="npm_package_lock_json",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    PNPM_PACKAGE_LOCK = MethodInfo(
        file_name="npm",
        name="pnpm_package_lock",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NUGET_CSPROJ = MethodInfo(
        file_name="nuget",
        name="nuget_csproj",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NUGET_PACKAGES_CONFIG = MethodInfo(
        file_name="nuget",
        name="nuget_packages_config",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NUGET_PKGS_LOCK_JSON = MethodInfo(
        file_name="nuget",
        name="nuget_pkgs_lock_json",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NET_FRAMEWORK_CONFIG = MethodInfo(
        file_name="nuget",
        name="net_framework_config",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.EXTERNAL,
        auto_approve=True,
    )
    PIP_REQUIREMENTS_TXT = MethodInfo(
        file_name="pip",
        name="pip_requirements_txt",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    PIPFILE_DEPS = MethodInfo(
        file_name="pipfile",
        name="pipfile_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    PIPFILE_LOCK = MethodInfo(
        file_name="pipfile",
        name="pipfile_lock",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    LOCK_DEPS = MethodInfo(
        file_name="poetry",
        name="lock_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.EXTERNAL,
        auto_approve=True,
    )
    POETRY_TOML_DEPS = MethodInfo(
        file_name="poetry",
        name="poetry_toml_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.EXTERNAL,
        auto_approve=True,
    )
    PUB_PUBSPEC_YAML = MethodInfo(
        file_name="pub",
        name="pub_pubspec_yaml",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    SCRIPT_VULNERABLE_DEPENDENCIES = MethodInfo(
        file_name="html",
        name="script_vulnerable_dependencies",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CARGO_TOML_DEPS = MethodInfo(
        file_name="cargo",
        name="cargo_toml_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CARGO_LOCK_DEPS = MethodInfo(
        file_name="cargo",
        name="cargo_lock_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ERLANG_MIX_DEPS = MethodInfo(
        file_name="erlang",
        name="erlang_mix_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ERLANG_MIX_LOCK_DEPS = MethodInfo(
        file_name="erlang",
        name="erlang_mix_lock_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    GITHUB_ACTIONS_DEPS = MethodInfo(
        file_name="github_actions",
        name="github_actions_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    SWIFT_PACKAGE_DEPS = MethodInfo(
        file_name="swift",
        name="swift_package_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    SPDX_DEPS = MethodInfo(
        file_name="sbom_files",
        name="spdx_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CYCLONEDX_DEPS = MethodInfo(
        file_name="sbom_files",
        name="cyclonedx_deps",
        module="lib_sca",
        finding=FindingEnum.F011,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    PIP_INCOMPLETE_DEPENDENCIES_LIST = MethodInfo(
        file_name="python",
        name="pip_incomplete_dependencies_list",
        module="lib_sca",
        finding=FindingEnum.F120,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    COMPOSER_JSON_DEV = MethodInfo(
        file_name="composer",
        name="composer_json_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    COMPOSER_LOCK_DEV = MethodInfo(
        file_name="composer",
        name="composer_lock_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONAN_CONANFILE_PY_DEV = MethodInfo(
        file_name="python_config_files",
        name="conan_conanfile_py_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONAN_CONANFILE_TXT_DEV = MethodInfo(
        file_name="conan",
        name="conan_conanfile_txt_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONAN_LOCK_DEV = MethodInfo(
        file_name="conan",
        name="conan_lock_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    GEM_GEMFILE_DEV = MethodInfo(
        file_name="gem",
        name="gem_gemfile_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NPM_PKG_JSON = MethodInfo(
        file_name="npm",
        name="npm_pkg_json",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NPM_PKG_LOCK_JSON = MethodInfo(
        file_name="npm",
        name="npm_pkg_lock_json",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NPM_YARN_LOCK_DEV = MethodInfo(
        file_name="npm",
        name="npm_yarn_lock_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    PNPM_LOCK_DEV = MethodInfo(
        file_name="npm",
        name="pnpm_lock_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.EXTERNAL,
        auto_approve=True,
    )
    PUB_PUBSPEC_YAML_DEV = MethodInfo(
        file_name="pub",
        name="pub_pubspec_yaml_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.LEWIS_CONTRERAS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CARGO_TOML_DEPS_DEV = MethodInfo(
        file_name="cargo",
        name="cargo_toml_deps_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ERLANG_MIX_DEPS_DEV = MethodInfo(
        file_name="erlang",
        name="erlang_mix_deps_dev",
        module="lib_sca",
        finding=FindingEnum.F393,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NPM_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="npm",
        name="npm_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.FABIO_LAGOS,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.HACKER,
        auto_approve=True,
    )
    CARGO_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="cargo",
        name="cargo_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONAN_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="conan",
        name="conan_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    COMPOSER_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="composer",
        name="composer_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ERLANG_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="erlang",
        name="erlang_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    GEMFILE_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="gemfile",
        name="gemfile_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NUGET_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="nuget",
        name="nuget_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    PIPFILE_MISSING_PACKAGE_LOCK = MethodInfo(
        file_name="pipfile",
        name="pipfile_missing_package_lock",
        module="lib_sca",
        finding=FindingEnum.F431,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.SCA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
