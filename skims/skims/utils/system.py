import asyncio
import os
import subprocess
from typing import (
    Any,
)


def call_blocking(  # noqa: PLR0913
    binary: str,
    *binary_args: str,
    cwd: str | None = None,
    env: dict[str, str] | None = None,
    stdin: int = subprocess.DEVNULL,
    stdout: int = subprocess.PIPE,
    stderr: int = subprocess.PIPE,
    **kwargs: Any,  # noqa: ANN401
) -> subprocess.Popen:
    return subprocess.Popen(  # noqa: S603
        [binary, *binary_args],
        cwd=cwd,
        env={
            **os.environ.copy(),
            **(env or {}),
        },
        stderr=stderr,
        stdin=stdin,
        stdout=stdout,
        **kwargs,
    )


async def call(  # noqa: PLR0913
    binary: str,
    *binary_args: str,
    cwd: str | None = None,
    env: dict[str, str] | None = None,
    stdin: int = asyncio.subprocess.DEVNULL,
    stdout: int = asyncio.subprocess.PIPE,
    stderr: int = asyncio.subprocess.PIPE,
    **kwargs: Any,  # noqa: ANN401
) -> asyncio.subprocess.Process:
    return await asyncio.create_subprocess_exec(
        binary,
        *binary_args,
        cwd=cwd,
        env={
            **os.environ.copy(),
            **(env or {}),
        },
        stderr=stderr,
        stdin=stdin,
        stdout=stdout,
        **kwargs,
    )


def read_blocking(  # noqa: PLR0913
    binary: str,
    *binary_args: str,
    cwd: str | None = None,
    env: dict[str, str] | None = None,
    stdin_bytes: bytes | None = None,
    stdout: int = subprocess.PIPE,
    stderr: int = subprocess.PIPE,
    **kwargs: Any,  # noqa: ANN401
) -> tuple[int, bytes, bytes]:
    process = call_blocking(
        binary,
        *binary_args,
        cwd=cwd,
        env=env,
        stdin=(subprocess.DEVNULL if stdin_bytes is None else subprocess.PIPE),
        stdout=stdout,
        stderr=stderr,
        **kwargs,
    )

    out, err = process.communicate(input=stdin_bytes)
    code = process.returncode

    return code, out, err


async def read(  # noqa: PLR0913
    binary: str,
    *binary_args: str,
    cwd: str | None = None,
    env: dict[str, str] | None = None,
    stdin_bytes: bytes | None = None,
    stdout: int = asyncio.subprocess.PIPE,
    stderr: int = asyncio.subprocess.PIPE,
    **kwargs: Any,  # noqa: ANN401
) -> tuple[int, bytes, bytes]:
    process: asyncio.subprocess.Process = await call(
        binary,
        *binary_args,
        cwd=cwd,
        env=env,
        stdin=(asyncio.subprocess.DEVNULL if stdin_bytes is None else asyncio.subprocess.PIPE),
        stdout=stdout,
        stderr=stderr,
        **kwargs,
    )

    out, err = await process.communicate(input=stdin_bytes)
    code = -1 if process.returncode is None else process.returncode

    return code, out, err
