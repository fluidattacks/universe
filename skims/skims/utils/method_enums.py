from collections.abc import Callable

from model.core import (
    DeveloperEnum,
    FindingEnum,
    MethodInfo,
    MethodOriginEnum,
    VulnerabilityTechnique,
)


def get_method_info_interface(
    module: str,
    technique: VulnerabilityTechnique,
) -> Callable[..., MethodInfo]:
    def method_info_interface(  # noqa: PLR0913
        *,
        file_name: str,
        name: str,
        finding: FindingEnum,
        developer: DeveloperEnum,
        origin: MethodOriginEnum,
        auto_approve: bool,
        cvss: str | None = None,
        cvss_v4: str | None = None,
        cwe_ids: list[str] | None = None,
    ) -> MethodInfo:
        return MethodInfo(
            file_name=file_name,
            name=name,
            finding=finding,
            developer=developer,
            origin=origin,
            auto_approve=auto_approve,
            module=module,
            technique=technique,
            cvss=cvss,
            cvss_v4=cvss_v4,
            cwe_ids=cwe_ids,
        )

    return method_info_interface
