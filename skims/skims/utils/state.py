from collections.abc import (
    Callable,
    Iterable,
    Iterator,
)
from concurrent.futures.thread import (
    ThreadPoolExecutor,
)
from os import (
    listdir,
    makedirs,
)
from os.path import (
    join,
)
from shutil import (
    rmtree,
)
from tempfile import (
    mkdtemp,
)
from typing import (
    Any,
    NamedTuple,
    cast,
)
from uuid import (
    uuid4 as uuid,
)

import ctx
from model.core import (
    Vulnerability,
)
from utils.crypto import (
    get_hash,
)
from utils.fs import (
    mkdir,
)
from utils.serialization import (
    dump as py_dumps,
)
from utils.serialization import (
    load as py_loads,
)

# Constants
EPHEMERAL = join(ctx.STATE_FOLDER, "ephemeral", uuid().hex)  # noqa: PTH118
makedirs(EPHEMERAL, mode=0o700, exist_ok=True)  # noqa: PTH103


class EphemeralStore(NamedTuple):
    iterate: Callable[[], Iterator[Any]]
    store: Callable[[Any], None]


def read_blob(obj_location: str) -> Any:  # noqa: ANN401
    with open(obj_location, "rb") as obj_store:  # noqa: PTH123
        obj_stream = obj_store.read()
        return py_loads(obj_stream)


def store_object(folder: str, object_to_store: Any) -> None:  # noqa: ANN401
    """Write the object to a file in the given folder.

    The name of the file will be the hash of the object to ensure uniqueness
    """
    obj_stream = py_dumps(object_to_store)
    obj_id = get_hash(obj_stream).hex()
    obj_location = join(folder, obj_id)  # noqa: PTH118

    with open(obj_location, "wb") as f:  # noqa: PTH123
        f.write(obj_stream)


def get_ephemeral_store() -> EphemeralStore:
    """Create an ephemeral store of Python objects on-disk.

    :return: An object with read/write methods
    :rtype: EphemeralStore
    """
    folder = mkdtemp(dir=EPHEMERAL)

    def store(obj: Any) -> None:  # noqa: ANN401
        store_object(folder, obj)

    def iterate() -> Iterator[Any]:
        paths = [join(folder, file) for file in listdir(folder)]  # noqa: PTH118
        with ThreadPoolExecutor(max_workers=ctx.CPU_CORES * 2) as worker:
            yield from worker.map(read_blob, paths)

    return EphemeralStore(
        iterate=iterate,
        store=store,
    )


class VulnerabilitiesEphemeralStore(NamedTuple):
    store_vulns: Callable[[Iterable[Vulnerability]], None]
    read_vulns: Callable[[], Iterator[Vulnerability]]


def get_vulnerability_ephemeral_store() -> VulnerabilitiesEphemeralStore:
    """Create a class that saves and reads vulnerabilities to a file on disk.

    :return: An object with read/write methods exclusive for vulnerabilities
    """
    folder = mkdtemp(dir=EPHEMERAL)

    def store_vuln(vuln: Vulnerability) -> None:
        obj_stream = py_dumps(vuln)  # type: ignore[arg-type]
        obj_id = get_hash(obj_stream).hex()
        obj_location = join(folder, obj_id)  # noqa: PTH118
        with open(obj_location, "wb") as f:  # noqa: PTH123
            f.write(obj_stream)

    def store_vulns(vulns: Iterable[Vulnerability]) -> None:
        for vuln in vulns:
            store_vuln(vuln)

    def read_vuln(obj_location: str) -> Vulnerability:
        with open(obj_location, "rb") as obj_store:  # noqa: PTH123
            obj_stream = obj_store.read()
            return cast(Vulnerability, py_loads(obj_stream))

    def retrieve_vulns() -> Iterator[Vulnerability]:
        paths = [join(folder, file) for file in listdir(folder)]  # noqa: PTH118
        with ThreadPoolExecutor(max_workers=ctx.CPU_CORES * 2) as worker:
            yield from worker.map(read_vuln, paths)

    return VulnerabilitiesEphemeralStore(
        store_vulns=store_vulns,
        read_vulns=retrieve_vulns,
    )


class ExecutionStores(NamedTuple):
    vuln_stores: VulnerabilitiesEphemeralStore
    methods_stores: dict[str, EphemeralStore]
    technique_stores: dict[str, EphemeralStore]


def reset() -> None:
    rmtree(EPHEMERAL)
    mkdir(name=EPHEMERAL, mode=0o700, exist_ok=True)
