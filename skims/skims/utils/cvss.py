from cvss import (
    CVSS3,
    CVSS4,
    CVSS3Error,
    CVSS4Error,
)
from model.core import (
    LocalesEnum,
    MethodInfo,
)
from utils.translations import (
    t,
)


def get_cvss_vector_v3_from_criteria(fin_code: str) -> str:
    return t(
        f"criteria.vulns.{fin_code}.cvss_v3",
        locale=LocalesEnum.EN,
    )


def get_cvss_vector_v4_from_criteria(fin_code: str) -> str:
    return t(
        f"criteria.vulns.{fin_code}.cvss_v4",
        locale=LocalesEnum.EN,
    )


def set_default_threat_scores(cvss4_vector: str | None) -> str | None:
    if cvss4_vector:
        metrics = cvss4_vector.split("/")

        if first_match := next((met for met in metrics if met.startswith("E:")), None):
            metrics[metrics.index(first_match)] = "E:U"
        else:
            metrics.append("E:U")

        return "/".join(metrics)
    return None


def set_default_temporal_scores(cvss_vector: str | None) -> str | None:
    if cvss_vector:
        metrics = cvss_vector.split("/")

        if first_match := next((met for met in metrics if met.startswith("E:")), None):
            metrics[metrics.index(first_match)] = "E:U"
        else:
            metrics.append("E:U")

        if not any(met for met in metrics if met.startswith("RL:")):
            metrics.append("RL:O")

        if not any(met for met in metrics if met.startswith("RC:")):
            metrics.append("RC:C")

        return "/".join(metrics)
    return None


def get_exploitability_score(cvss_vector: str) -> float:
    try:
        score = CVSS3(cvss_vector).scores()[0]
    except CVSS3Error:
        return -1.0
    else:
        return score


def _get_ui_default(field: str) -> str:
    value = field.split(":")[1]
    if value == "N":
        return field
    return "UI:A"


def _get_ui_default_v3(field: str) -> str:
    value = field.split(":")[1]
    if value == "A":
        return "UI:R"
    return "UI:N"


def _get_ui_special_v3(field: str) -> str:
    value = field.split(":")[1]
    if value in {"A", "P"}:
        return "UI:R"
    return field


def _get_modified_ui_default(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value == "N":
        return field
    return "MUI:A"


def _get_modified_ui_special_v3(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value in {"P", "A"}:
        return "MUI:R"
    return field


def _get_modified_ui_default_v3(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value == "A":
        return "MUI:R"
    return "MUI:N"


def _get_subsystem(field: str) -> str:
    value = field.split(":")[1]
    if value == "U":
        return "SC:N/SI:N/SA:N"
    return "SC:L/SI:L/SA:L"


def _get_modified_subsystem(field: str) -> str:
    value = field.split(":")[1]
    if value == "U":
        return "MSC:N/MSI:N/MSA:N"
    return "MSC:L/MSI:L/MSA:L"


def _get_exploit_maturity(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value in {"F", "H"}:
        return "E:A"
    return field


def _get_exploit_maturity_v3(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value in {"A"}:
        return "E:H"
    return field


def _get_modified_ui_special(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value == "N":
        return field
    return "MUI:P"


def _get_ui_special(field: str) -> str:
    value = field.split(":")[1]
    if value == "N":
        return field
    return "UI:P"


def get_field_special(field: str) -> str:
    value = ""
    if field.startswith("UI:"):
        value = _get_ui_special(field)
    if any(field.startswith(starts) for starts in ["C:", "I:", "A:"]):
        value = f"V{field[:1]}:N/S{field}"
    if any(field.startswith(starts) for starts in ["MC:", "MI:", "MA:"]):
        value = field[:1] + "V" + field[1:2] + ":N/" + field[:1] + "S" + field[1:]
    if field.startswith("MUI:"):
        value = _get_modified_ui_special(field)

    return value


def get_field_default(field: str) -> str:
    value = ""
    if field.startswith("UI:"):
        value = _get_ui_default(field)
    if any(field.startswith(starts) for starts in ["C:", "I:", "A:"]):
        value = f"V{field}"
    if field.startswith("S:"):
        value = _get_subsystem(field)
    if any(field.startswith(starts) for starts in ["MC:", "MI:", "MA:"]):
        value = field[:1] + "V" + field[1:]
    if field.startswith("MS:"):
        value = _get_modified_subsystem(field)
    if field.startswith("MUI:"):
        value = _get_modified_ui_default(field)

    return value


def get_field_special_v3(field: str) -> str:
    value = ""
    if field.startswith("UI:"):
        value = _get_ui_special_v3(field)
    if any(field.startswith(starts) for starts in ["SC:", "SI:", "SA:"]):
        value = field[1:]
    if any(field.startswith(starts) for starts in ["MSC:", "MSI:", "MSA:"]):
        value = (field[:1] + field[2:]) + ("/MS:C" if field[-1] != "X" else "/MS:U")
    if field.startswith("MUI:"):
        value = _get_modified_ui_special_v3(field)

    return value


def get_field_default_v3(field: str) -> str:
    value = ""
    if field.startswith("UI:"):
        value = _get_ui_default_v3(field)
    if any(field.startswith(starts) for starts in ["VC:", "VI:", "VA:"]):
        value = field[1:]
    if any(field.startswith(starts) for starts in ["MVC:", "MVI:", "MVA:"]):
        value = (field[:1] + field[2:]) + ("/MS:C" if field[-1] in {"H", "L"} else "/MS:U")
    if field.startswith("MUI:"):
        value = _get_modified_ui_default_v3(field)

    return value


def get_field(field: str, code: str) -> str:
    value = ""
    if field in {"CVSS:3.1", "CVSS:3.0"}:
        return "CVSS:4.0"
    if any(
        field.startswith(starts)
        for starts in [
            "AV:",
            "AC:",
            "PR:",
            "MAV:",
            "MAC:",
            "MPR:",
            "CR:",
            "IR:",
            "AR:",
        ]
    ):
        return field
    if field.startswith("E:"):
        return _get_exploit_maturity(field)

    if code in ["009", "359", "367", "411"]:
        value = get_field_special(field)
    else:
        value = get_field_default(field)

    return value


def get_field_v3(field: str, code: str) -> str:
    value = ""
    if field in {"CVSS:4.0"}:
        return "CVSS:3.1"
    if any(
        field.startswith(starts)
        for starts in [
            "AV:",
            "AC:",
            "PR:",
            "MAV:",
            "MAC:",
            "MPR:",
            "CR:",
            "IR:",
            "AR:",
        ]
    ):
        return field
    if field.startswith("E:"):
        return _get_exploit_maturity_v3(field)

    if code in ["009", "359", "367", "411"]:
        value = get_field_special_v3(field)
    else:
        value = get_field_default_v3(field)

    return value


def get_cvss_v4_from_v3(field: str | None, code: str) -> str | None:
    if field is None:
        return None

    converted = (
        "/".join(
            filter(
                None,
                [get_field(_field, code) for _field in field.split("/")],
            ),
        )
        + "/AT:N"
    )

    try:
        return CVSS4(converted).clean_vector()
    except CVSS4Error:
        return None


def get_cvss_v3_from_v4(field: str | None, code: str) -> str | None:
    if field is None:
        return None
    converted = "/".join(
        filter(
            None,
            [get_field_v3(_field, code) for _field in field.split("/")],
        ),
    )
    if all(value in field for value in ["/SC:N", "/SI:N", "/SA:N"]):
        converted += "/S:U"
    else:
        converted += "/S:C"
    try:
        return set_default_temporal_scores(CVSS3(converted).clean_vector())
    except CVSS3Error:
        return get_cvss_vector_v3_from_criteria(code)


def is_cvss_v3(method: MethodInfo) -> bool:
    try:
        CVSS3(
            method.cvss
            if method.cvss is not None
            else get_cvss_vector_v3_from_criteria(method.get_finding()),
        )
    except CVSS3Error:
        return False
    else:
        return True


def is_cvss_v4(method: MethodInfo) -> bool:
    try:
        CVSS4(
            method.cvss_v4
            if method.cvss_v4 is not None
            else get_cvss_vector_v4_from_criteria(method.get_finding()),
        )
    except CVSS4Error:
        return False
    else:
        return True
