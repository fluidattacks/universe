from pathlib import Path
from typing import (
    TYPE_CHECKING,
    Any,
)

import ctx
import yaml
from ctx import (
    CRITERIA_REQUIREMENTS,
    CRITERIA_VULNERABILITIES,
    PROJECT_PATH,
)
from model.core import (
    LocalesEnum,
)

if TYPE_CHECKING:
    from enum import (
        Enum,
    )


def _get_metric_value(data: dict, field: str) -> str:
    return data["score"]["base"][field]


def _get_special_metric_value(data: dict, field: str, default_value: str) -> str:
    criteria_metric = data["score"]["temporal"][field]
    return default_value if default_value and criteria_metric == "X" else criteria_metric


def get_cvss_vector_v3_from_criteria(fin_data: dict) -> str:
    attack_vector = _get_metric_value(fin_data, "attack_vector")
    attack_complexity = _get_metric_value(fin_data, "attack_complexity")
    privileges_required = _get_metric_value(fin_data, "privileges_required")
    user_interaction = _get_metric_value(fin_data, "user_interaction")
    severity = _get_metric_value(fin_data, "scope")
    confidentiality = _get_metric_value(fin_data, "confidentiality")
    integrity = _get_metric_value(fin_data, "integrity")
    availability = _get_metric_value(fin_data, "availability")

    exploitability = _get_special_metric_value(fin_data, "exploit_code_maturity", "U")
    remediation_level = _get_special_metric_value(fin_data, "remediation_level", "O")
    report_confidence = _get_special_metric_value(fin_data, "report_confidence", "C")

    return (
        f"CVSS:3.1/AV:{attack_vector}/AC:{attack_complexity}"
        f"/PR:{privileges_required}/UI:{user_interaction}"
        f"/S:{severity}/C:{confidentiality}/I:{integrity}"
        f"/A:{availability}/E:{exploitability}/RL:{remediation_level}"
        f"/RC:{report_confidence}"
    )


def _get_v4_metric_value(data: dict, field: str) -> str:
    return data["score_v4"]["base"][field]


def _get_v4_exploit_maturity(data: dict, default_value: str) -> str:
    criteria_metric = data["score_v4"]["threat"]["exploit_maturity"]
    return default_value if default_value and criteria_metric == "X" else criteria_metric


def get_cvss_vector_v4_from_criteria(fin_data: dict) -> str:
    return (
        "CVSS:4.0"
        f"/AV:{_get_v4_metric_value(fin_data, 'attack_vector')}"
        f"/AC:{_get_v4_metric_value(fin_data, 'attack_complexity')}"
        f"/AT:{_get_v4_metric_value(fin_data, 'attack_requirements')}"
        f"/PR:{_get_v4_metric_value(fin_data, 'privileges_required')}"
        f"/UI:{_get_v4_metric_value(fin_data, 'user_interaction')}"
        f"/VC:{_get_v4_metric_value(fin_data, 'confidentiality_vc')}"
        f"/VI:{_get_v4_metric_value(fin_data, 'integrity_vi')}"
        f"/VA:{_get_v4_metric_value(fin_data, 'availability_va')}"
        f"/SC:{_get_v4_metric_value(fin_data, 'confidentiality_sc')}"
        f"/SI:{_get_v4_metric_value(fin_data, 'integrity_si')}"
        f"/SA:{_get_v4_metric_value(fin_data, 'availability_sa')}"
        f"/E:{_get_v4_exploit_maturity(fin_data, 'U')}"
    )


def load_criteria_translations() -> dict[str, dict[str, str]]:
    translations: dict[str, dict[str, str]] = {}
    with Path(CRITERIA_REQUIREMENTS).open(encoding="utf-8") as handle:
        for code, data in yaml.safe_load(handle).items():
            translations[f"criteria.requirements.{code}"] = {
                "EN": f"{code}. {data['en']['summary']}",
                "ES": f"{code}. {data['es']['summary']}",
            }

    with Path(CRITERIA_VULNERABILITIES).open(encoding="utf-8") as handle:
        for code, data in yaml.safe_load(handle).items():
            translations[f"criteria.vulns.{code}.title"] = {
                "EN": f"{code}. {data['en']['title']}",
                "ES": f"{code}. {data['es']['title']}",
            }

            for field in (
                "description",
                "impact",
                "recommendation",
                "threat",
            ):
                translations[f"criteria.vulns.{code}.{field}"] = {
                    "EN": data["en"][field],
                    "ES": data["es"][field],
                }

            cvss_v3 = get_cvss_vector_v3_from_criteria(data)
            translations[f"criteria.vulns.{code}.cvss_v3"] = {
                "EN": cvss_v3,
                "ES": cvss_v3,
            }

            cvss_v4 = get_cvss_vector_v4_from_criteria(data)
            translations[f"criteria.vulns.{code}.cvss_v4"] = {
                "EN": cvss_v4,
                "ES": cvss_v4,
            }

    return translations


def load_translations(
    *,
    include_criteria: bool = True,
) -> dict[str, dict[str, str]]:
    """Load the translations data from the static folder.

    :raises KeyError: On duplicated translations
    :return: A dictionary mapping keys to a dictionary mapping languages to
        translations
    """
    translations: dict[str, dict[str, str]] = {}
    translations_paths = {
        f"{PROJECT_PATH}/lib_apk/methods_descriptions.yaml",
        f"{PROJECT_PATH}/lib_cspm/methods_descriptions.yaml",
        f"{PROJECT_PATH}/lib_dast/methods_descriptions.yaml",
        f"{PROJECT_PATH}/lib_sast/methods_descriptions.yaml",
        f"{PROJECT_PATH}/lib_sca/methods_descriptions.yaml",
        f"{PROJECT_PATH}/utils/translations.yaml",
    }
    for path in translations_paths:
        with Path(path).open(encoding="utf-8") as handle:
            for data in yaml.safe_load(handle).values():
                for key, value in data.items():
                    if key in translations:
                        exc_log = f"Found a duplicated translation: {key}"
                        raise KeyError(exc_log)

                    translations[key] = {
                        locale_code: value[locale_code.lower()]
                        for locale in LocalesEnum
                        for locale_code in [locale.value]
                    }

    if include_criteria:
        translations.update(load_criteria_translations())

    return translations


TRANSLATIONS: dict[str, dict[str, str]] = load_translations()

IGNORED_CHARS = str.maketrans("", "", "".join({"+", "^", "*", "~"}))


def t(
    key: str,
    *args: Any,  # noqa: ANN401
    **kwargs: Any,  # noqa: ANN401
) -> str:
    locale: Enum | None = kwargs.get("locale")
    language = locale if locale else ctx.SKIMS_CONFIG.language
    return TRANSLATIONS[key][language.value].format(*args, **kwargs).translate(IGNORED_CHARS)
