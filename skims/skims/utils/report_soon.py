import json
from collections.abc import Iterable

import boto3
import ctx
from model.core import (
    Vulnerability,
)
from utils.env import (
    is_fluid_batch_env,
)
from utils.logs import (
    log_blocking,
)
from utils.repositories import (
    get_repo_head_hash,
)

SQS_URL = "https://sqs.us-east-1.amazonaws.com/205810638802/integrates_report_soon"


def get_path_on_integrates(vulnerability: Vulnerability) -> str:
    what = vulnerability.what
    if (
        (vulnerability.skims_metadata.source_method == "python.pip_incomplete_dependencies_list")
        and (sca_metadata := vulnerability.skims_metadata.sca_metadata)
        and (dependency := sca_metadata.package)
    ):
        what = f"{what} (missing dependency: {dependency})"

    return what


def get_sast_vuln_json_message(
    vulnerability: Vulnerability,
    namespace: str,
    working_dir: str,
) -> dict:
    # The json message copies the format used on integrates
    # Do not change them without adjusting report functionality first
    return {
        "repo_nickname": namespace,
        "state": "open",
        "skims_method": vulnerability.skims_metadata.source_method,
        "skims_description": vulnerability.skims_metadata.description,
        "developer": vulnerability.skims_metadata.developer.value,
        "source": "MACHINE",
        "hash": vulnerability.digest,
        "cwe_ids": vulnerability.skims_metadata.cwe_ids,
        "cvss_v3": vulnerability.skims_metadata.cvss,
        "cvss_v4": vulnerability.skims_metadata.cvss_v4,
        "technique": vulnerability.kind.value,
        "commit_hash": get_repo_head_hash(working_dir),
        "line": str(vulnerability.where),
        "path": get_path_on_integrates(vulnerability),
        "advisories": {
            "package": sca_metadata.package,
            "vulnerable_version": sca_metadata.vulnerable_version,
            "cve": sca_metadata.cve,
        }
        if (sca_metadata := vulnerability.skims_metadata.sca_metadata)
        else None,
        "message": {
            "description": vulnerability.skims_metadata.description,
            "snippet": vulnerability.skims_metadata.snippet,
        },
    }


def send_vulnerability_to_sqs(
    vulnerabilities: Iterable[Vulnerability],
) -> None:
    if not ctx.SKIMS_CONFIG.use_report_soon or (
        not (is_fluid_batch_env() and (execution_id := ctx.SKIMS_CONFIG.execution_id))
    ):
        return

    namespace = ctx.SKIMS_CONFIG.namespace
    working_dir = ctx.SKIMS_CONFIG.working_dir
    client = boto3.client("sqs")
    group_name = execution_id.split("_", maxsplit=1)[0]
    for vulnerability in vulnerabilities:
        if not vulnerability.skims_metadata.auto_approve or vulnerability.skims_metadata.skip:
            return

        vuln_json = get_sast_vuln_json_message(
            vulnerability,
            namespace,
            working_dir,
        )
        fin_code = vulnerability.finding.value.title.split(".")[2]

        try:
            message_body = json.dumps(
                {
                    "id": (f"{group_name}_{namespace} {vulnerability.digest}"),
                    "task": "report_soon",
                    "args": [group_name, namespace, fin_code, vuln_json],
                },
            )
            client.send_message(QueueUrl=SQS_URL, MessageBody=message_body)
        except Exception as err:  # noqa: BLE001
            log_blocking(
                "warning",
                "Failed to send found vulnerability %s",
                err,
            )
