import statistics
from pathlib import (
    Path,
)

from utils.env import (
    is_fluid_batch_env,
)


def calculate_methods_averages(methods_coeffs: dict[str, list[float]]) -> dict[str, float]:
    return {key: float(statistics.mean(val)) for key, val in methods_coeffs.items()}


def calculate_time_coefficient(elapsed_time: float, path: str, method_calls: int) -> float | None:
    if is_fluid_batch_env():
        return ((float(Path(path).stat().st_size) * elapsed_time) / method_calls) * 1000
    return None
