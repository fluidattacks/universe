import hashlib

HASH = hashlib.blake2b  # https://blake2.net/

INSEC_CIPHER_ALGS = {
    "blowfish",
    "bf",
    "des",
    "desede",
    "rc2",
    "rc4",
    "tripledes",
}

INSECURE_CURVES = {
    "secp112r1",
    "secp112r2",
    "secp128r1",
    "secp128r2",
    "secp160k1",
    "secp160r1",
    "secp160r2",
    "secp192k1",
    "prime192v1",
    "prime192v2",
    "prime192v3",
    "sect113r1",
    "sect113r2",
    "sect131r1",
    "sect131r2",
    "sect163k1",
    "sect163r1",
    "sect163r2",
    "sect193r1",
    "sect193r2",
    "c2pnb163v1",
    "c2pnb163v2",
    "c2pnb163v3",
    "c2pnb176v1",
    "c2tnb191v1",
    "c2tnb191v2",
    "c2tnb191v3",
    "c2pnb208w1",
    "wap-wsg-idm-ecid-wtls1",
    "wap-wsg-idm-ecid-wtls3",
    "wap-wsg-idm-ecid-wtls4",
    "wap-wsg-idm-ecid-wtls5",
    "wap-wsg-idm-ecid-wtls6",
    "wap-wsg-idm-ecid-wtls7",
    "wap-wsg-idm-ecid-wtls8",
    "wap-wsg-idm-ecid-wtls9",
    "wap-wsg-idm-ecid-wtls10",
    "wap-wsg-idm-ecid-wtls11",
    "oakley-ec2n-3",
    "oakley-ec2n-4",
    "brainpoolp160r1",
    "brainpoolp160t1",
    "brainpoolp192r1",
    "brainpoolp192t1",
}


def get_hash(stream: bytes) -> bytes:
    digestor = HASH()
    digestor.update(stream)
    return digestor.digest()


def is_vulnerable_cipher(alg: str, mode: str, pad: str) -> bool:
    alg = alg.lower()
    mode = mode.lower()
    pad = pad.lower()

    return any(
        (
            alg == "aes" and mode in {"ecb", ""},
            alg == "aes" and mode == "cbc" and pad and pad != "nopadding",
            alg == "rsa" and "oaep" not in pad,
            alg in INSEC_CIPHER_ALGS,
        ),
    )


def is_vulnerable_cipher_no_mode(alg: str) -> bool:
    alg = alg.lower()
    return alg == "rsa" or alg in INSEC_CIPHER_ALGS


def insecure_elliptic_curve(curve_name: str) -> bool:
    return curve_name in INSECURE_CURVES
