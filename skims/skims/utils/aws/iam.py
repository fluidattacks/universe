import json
import re
from collections.abc import (
    Iterable,
)
from pathlib import Path

from ctx import (
    PROJECT_PATH,
)


# Actions and privileges for AWS Services
# Last fetched on 03/24 from https://www.awsiamactions.io/
def _load_actions_static_data() -> dict:
    with Path(f"{PROJECT_PATH}/utils/aws/iam_actions.json").open(encoding="utf-8") as file:
        return json.load(file)


def _load_arn_static_data() -> dict:
    with Path(f"{PROJECT_PATH}/utils/aws/arn.json").open(encoding="utf-8") as file:
        return json.load(file)


ACTIONS = _load_actions_static_data()
ARN = _load_arn_static_data()

RESOURCES_EXCEPTIONS = {
    f"{service}:{action}"
    for service in ACTIONS
    if ACTIONS[service].get("wildcard_resource")
    for action in ACTIONS[service]["wildcard_resource"]
}
ALL_RESOURCES = {
    f"{service}:{action}"
    for service, actions in ACTIONS.items()
    for key in ("read", "write", "list")
    if actions.get(key)
    for action in actions[key]
}


def match_pattern(pattern: str, target: str, flags: int = 0) -> bool:
    # Escape everything that is not `*` and replace `*` with regex `.*`
    pattern = r".*".join(map(re.escape, pattern.split("*")))
    return bool(re.match(f"^{pattern}$", target, flags=flags))


def is_action_permissive(action: Iterable[str] | str | None) -> bool:
    if not isinstance(action, str):
        # A var or syntax error
        return False

    splitted = action.split(":", 1)  # a:b
    provider = splitted[0]  # a
    effect = splitted[1] if splitted[1:] else None  # b

    return (
        (provider == "*")
        or (effect and effect.startswith("*"))
        or ("*" in provider and effect is None)
    )


def is_resource_permissive(resource: str) -> bool:
    return resource == "*"


def is_public_principal(principals: Iterable[str] | str) -> bool:
    principal_list = principals if isinstance(principals, list) else [principals]
    return "*" in principal_list
