import ctx
from fluidattacks_core.serializers.snippet import (
    Snippet,
)
from model.core import (
    FindingEnum,
    HTTPProperties,
    MethodInfo,
    ScaAdvisoriesMetadata,
    SkimsVulnerabilityMetadata,
    Vulnerability,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from utils.cvss import (
    get_cvss_v3_from_v4,
    get_cvss_v4_from_v3,
    get_cvss_vector_v3_from_criteria,
    get_cvss_vector_v4_from_criteria,
    set_default_temporal_scores,
    set_default_threat_scores,
)


def build_metadata(  # noqa: PLR0913
    *,
    method: MethodInfo,
    description: str,
    snippet: str | Snippet,
    cvss: str | None = None,
    cvss_v4: str | None = None,
    cwe_ids: list[str] | None = None,
    http_properties: HTTPProperties | None = None,
    sca_metadata: ScaAdvisoriesMetadata | None = None,
    skip: bool = False,
    sca_auto_approve: bool | None = None,
) -> SkimsVulnerabilityMetadata:
    cwe = cwe_ids if cwe_ids else method.get_cwe()
    _cvss_v4 = cvss_v4

    if not cvss_v4:
        if method.cvss_v4:
            cvss_v4 = set_default_threat_scores(method.cvss_v4)
        else:
            cvss_v4 = (
                get_cvss_v4_from_v3(cvss, method.get_finding())
                if cvss
                else get_cvss_vector_v4_from_criteria(method.get_finding())
            )
    if not cvss:
        if method.cvss:
            cvss = set_default_temporal_scores(method.cvss)
        else:
            cvss = (
                get_cvss_v3_from_v4(cvss_v4, method.get_finding())
                if _cvss_v4
                else get_cvss_vector_v3_from_criteria(method.get_finding())
            )

    return SkimsVulnerabilityMetadata(
        source_method=method.get_name(),
        developer=method.developer,
        description=description,
        snippet=snippet,
        cvss=cvss,
        cvss_v4=cvss_v4,
        cwe_ids=cwe,
        sca_metadata=sca_metadata,
        http_properties=http_properties,
        skip=skip,
        auto_approve=sca_auto_approve if sca_auto_approve is not None else method.auto_approve,
    )


def build_inputs_vuln(
    method: MethodInfo,
    stream: str,
    what: str,
    where: str,
    metadata: SkimsVulnerabilityMetadata,
) -> Vulnerability:
    kind = VulnerabilityTechnique.DAST
    if method.file_name in {"aws", "gcp", "azure"}:
        kind = VulnerabilityTechnique.CSPM

    return Vulnerability(
        finding=method.finding,
        kind=kind,
        vulnerability_type=VulnerabilityType.INPUTS,
        namespace=ctx.SKIMS_CONFIG.namespace,
        stream=stream,
        what=what,
        where=where,
        skims_metadata=metadata,
    )


def build_lines_vuln(
    method: MethodInfo,
    what: str,
    where: str,
    metadata: SkimsVulnerabilityMetadata,
    cve_finding: FindingEnum | None = None,
) -> Vulnerability:
    kind = VulnerabilityTechnique.SAST
    if method.technique == VulnerabilityTechnique.SCA:
        kind = VulnerabilityTechnique.SCA

    return Vulnerability(
        finding=cve_finding if cve_finding else method.finding,
        kind=kind,
        vulnerability_type=VulnerabilityType.LINES,
        namespace=ctx.SKIMS_CONFIG.namespace,
        what=what,
        where=where,
        skims_metadata=metadata,
    )
