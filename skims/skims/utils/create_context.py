import os

import ctx
from model.core import (
    FindingEnum,
    LocalesEnum,
    SkimsApiConfig,
    SkimsAPKConfig,
    SkimsConfig,
    SkimsCspmConfig,
    SkimsDastConfig,
    SkimsSastConfig,
    SkimsScaConfig,
)


def create_test_context(
    include: tuple[str, ...] = (),
    exclude: tuple[str, ...] = (),
) -> None:
    ctx.SKIMS_CONFIG = SkimsConfig(
        api=SkimsApiConfig(include=()),
        apk=SkimsAPKConfig(exclude=(), include=()),
        checks=set(FindingEnum),
        commit=None,
        cspm=SkimsCspmConfig(gcp_credentials=[], aws_credentials=[], azure_credentials=[]),
        dast=SkimsDastConfig(urls=(), http_checks=False, ssl_checks=False),
        debug=False,
        language=LocalesEnum.EN,
        multifile=False,
        file_size_limit=True,
        namespace="test",
        strict=False,
        sca=SkimsScaConfig(exclude=(), include=()),
        tracing_opt_out=False,
        output=None,
        sast=SkimsSastConfig(include=include, exclude=exclude),
        start_dir=os.getcwd(),  # noqa: PTH109
        working_dir=os.getcwd(),  # noqa: PTH109
        execution_id="123456789",
    )
