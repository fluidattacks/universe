import os
from shutil import (
    rmtree,
)

from ctx import (
    NAMESPACES_FOLDER,
)
from git.exc import (
    GitError,
)
from git.repo import (
    Repo,
)
from utils.logs import (
    log_blocking,
)

# Constants
DEFAULT_COMMIT: str = "0000000000000000000000000000000000000000"
DEFAULT_REPO: str = "https://gitlab.com/fluidattacks/universe"


def get_repo(*, path: str, search_parent_directories: bool = True) -> Repo:
    return Repo(path, search_parent_directories=search_parent_directories)


def get_repo_head_hash(path: str) -> str:
    try:
        repo: Repo = get_repo(path=path)
        head_hash: str = repo.head.commit.hexsha
    except (GitError, ValueError, BrokenPipeError):
        log_blocking("warning", "Unable to find commit HEAD on analyzed directory")
    else:
        return head_hash
    return DEFAULT_COMMIT


def get_repo_branch(path: str) -> str | None:
    try:
        repo: Repo = get_repo(path=path)
    except GitError as exc:
        log_blocking("error", "Computing active branch: %s ", exc)
    except IndexError:
        return None
    else:
        return repo.active_branch.name

    return None


def get_repo_remote(path: str) -> str:
    url: str = DEFAULT_REPO
    try:
        repo: Repo = get_repo(path=path)
        remotes = repo.remotes
        if remotes:
            url = remotes[0].url
            if url and not url.startswith("http"):
                url = f"ssh://{url}"
    except GitError as exc:
        log_blocking("error", "Computing active branch: %s ", exc)

    return url


def clone_repo_in_state_folder(path: str) -> str | None:
    repo_name = path.replace(".git", "").split("/").pop()
    repo_path = os.path.join(NAMESPACES_FOLDER, repo_name)  # noqa: PTH118

    if os.path.isdir(repo_path):  # noqa: PTH112
        rmtree(repo_path)

    try:
        Repo.clone_from(url=path, to_path=repo_path, depth=1)
    except GitError:
        return None
    else:
        return repo_path
