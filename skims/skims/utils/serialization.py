import utils.safe_pickle
from fluidattacks_core.serializers.snippet import (
    Function,
    Snippet,
)
from model import (
    core,
)
from utils.safe_pickle import (
    dump,
    load,
)


def _side_effects() -> None:
    for factory in (
        core.DeveloperEnum,
        core.FindingEnum,
        core.MethodOriginEnum,
        core.VulnerabilityTechnique,
        core.VulnerabilityType,
    ):
        utils.safe_pickle.register_enum(factory)

    for factory in (  # type: ignore[assignment]
        Snippet,
        Function,
        core.FindingMetadata,
        core.HTTPProperties,
        core.ScaAdvisoriesMetadata,
        core.SkimsVulnerabilityMetadata,
        core.Vulnerability,
    ):
        utils.safe_pickle.register_namedtuple(factory)


# Side effects
_side_effects()

# Exported members
__all__ = ["dump", "load"]
