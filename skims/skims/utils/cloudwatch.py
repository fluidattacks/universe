from datetime import (
    datetime,
)

import boto3
from botocore.exceptions import (
    ClientError,
)
from utils.logs import (
    log_blocking,
)


def send_metrics_to_cloudwatch(data_dict: dict[str, dict[str, int] | dict[str, float]]) -> None:
    try:
        cloudwatch_client = boto3.client("cloudwatch", "us-east-1")

        for namespace, metric_dict in data_dict.items():
            metric_data = []

            for metric, value in metric_dict.items():
                metric_data.append(
                    {
                        "MetricName": metric,
                        "Dimensions": [],
                        "Timestamp": datetime.now(),  # noqa: DTZ005
                        "Value": value,
                        "Unit": "Count",
                    },
                )

            if metric_data:
                cloudwatch_client.put_metric_data(Namespace=namespace, MetricData=metric_data)

    except ClientError as exc:
        log_blocking("error", f"An error ocurred sending metric to cloudwatch: {exc}")
