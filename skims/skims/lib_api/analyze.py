import asyncio
import os

import ctx
from jsonschema import (
    validate,
)
from jsonschema.exceptions import (
    SchemaError,
    ValidationError,
)
from lib_api.api_model import (
    Query,
)
from lib_api.parse import (
    parse_json,
)
from lib_api.utils import (
    get_response,
    load_json_safe,
    prepare_request_headers,
    prepare_request_params,
)
from utils.fs import (
    resolve_paths,
)
from utils.logs import (
    log_blocking,
)


async def validate_queries(query: Query) -> Query | None:
    request = query.request
    response = await get_response(
        method=request.http_method.value,
        url=request.url,
        headers=prepare_request_headers(request.headers),
        params=prepare_request_params(request.body),
        variables=request.variable,
    )
    if not response:
        return None
    return Query(name=query.name, request=query.request, response=response)


async def _get_api_ctx_to_analyze(*, paths: tuple[str, ...], schema: dict) -> set[Query]:
    queries = []
    for path in paths:
        collection_data = load_json_safe(path)
        try:
            validate(instance=collection_data, schema=schema)
            api_context = parse_json(collection_data)
            queries = await asyncio.gather(
                *[
                    validate_queries(
                        query=query,
                    )
                    for query in api_context.queries
                ],
            )

        except (
            ValidationError,
            SchemaError,
        ):
            log_blocking("error", "Invalid collection %s", path)

    return {item for item in set(queries) if item is not None}


async def analyze() -> None:
    if not (ctx.SKIMS_CONFIG.api and ctx.SKIMS_CONFIG.api.include):
        return

    paths = resolve_paths(
        working_dir=ctx.SKIMS_CONFIG.working_dir,
        include=ctx.SKIMS_CONFIG.api.include,
        exclude=(),
    )
    all_paths = paths.ok_paths

    if not all_paths:
        return

    log_blocking("info", "Running API analysis")

    collection_schema = load_json_safe(
        os.path.dirname(os.path.abspath(__file__)) + "/schema.json",  # noqa: PTH100,PTH120
    )

    api_ctx_to_analyze = await _get_api_ctx_to_analyze(paths=all_paths, schema=collection_schema)
    unique_count = len(api_ctx_to_analyze)
    log_blocking("info", "Running analysis over %s available endpoints", unique_count)

    log_blocking("info", "API analysis completed!")
