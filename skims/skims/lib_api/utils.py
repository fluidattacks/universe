import json
import os
import re
import time
from pathlib import (
    Path,
)

from lib_api.api_model import (
    Body,
    DefaultAttribute,
    RequestBodyModeEnum,
    Response,
)
from utils.http_requests import (
    create_session,
    request,
)


def load_json_safe(filepath: str) -> dict:
    if not os.path.exists(filepath):  # noqa: PTH110
        return {}
    with Path(filepath).open(encoding="utf-8") as file:
        return json.load(file)


def replace_variables_in_url(url: str, variables: dict[str, str]) -> str:
    pattern = re.compile(r":(\w+)")

    def replacer(match: re.Match) -> str:
        var_name = match.group(1)
        return variables.get(var_name, match.group(0))

    return pattern.sub(replacer, url)


async def get_response(
    method: str,
    url: str,
    headers: dict,
    params: dict,
    variables: dict,
) -> Response | None:
    response = None
    async with create_session() as session:  # type: ignore[var-annotated]
        response = None
        if variables:
            url = replace_variables_in_url(url, variables)
        start_time = time.time()
        response = await request(
            session=session,
            method=method,
            url=url,
            headers=headers,
            data=params,
            timeout=3,
        )
    if not response:
        return None
    if response.status == 400 or str(response.status).startswith("5"):
        return None

    return Response(
        status_code=response.status,
        headers=[DefaultAttribute(key=key, value=value) for key, value in response.headers.items()],
        response_body=None,
        response_time=int((time.time() - start_time) * 1000),
    )


def prepare_request_params(body: Body | None) -> dict:
    body_dict = {}
    if body and body.mode == RequestBodyModeEnum.RAW:
        body_dict = json.loads(body.data)
    return body_dict


def prepare_request_headers(headers: list[DefaultAttribute]) -> dict:
    headers_dict = {}
    for attr in headers:
        if not attr.disabled:
            headers_dict[attr.key] = attr.value
    return headers_dict
