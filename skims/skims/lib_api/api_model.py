from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)


class AuthEnum(Enum):
    APIKEY = "apikey"
    AWSV4 = "awsv4"
    BASIC = "basic"
    BEARER = "bearer"
    DIGEST = "digest"
    EDGEGRID = "edgegrid"
    HAWK = "hawk"
    NOAUTH = "noauth"
    OAUTH1 = "oauth1"
    OAUTH2 = "oauth2"
    NTLM = "ntlm"


class RequestBodyModeEnum(Enum):
    RAW = "raw"
    URLENCODED = "urlencoded"
    FORMDATA = "formdata"
    FILE = "file"
    GRAPHQL = "graphql"


class HttpMethodEnum(Enum):
    GET = "GET"
    PUT = "PUT"
    POST = "POST"
    PATCH = "PATCH"
    DELETE = "DELETE"
    COPY = "COPY"
    HEAD = "HEAD"
    OPTIONS = "OPTIONS"
    LINK = "LINK"
    UNLINK = "UNLINK"
    PURGE = "PURGE"
    LOCK = "LOCK"
    UNLOCK = "UNLOCK"
    PROPFIND = "PROPFIND"
    VIEW = "VIEW"


class DefaultAttribute(NamedTuple):
    key: str
    value: str | None
    type: str | None = "string"
    disabled: bool = False


class Body(NamedTuple):
    mode: RequestBodyModeEnum
    data: str


class Auth(NamedTuple):
    type: AuthEnum
    data: list[DefaultAttribute] | None


class Request(NamedTuple):
    url: str
    http_method: HttpMethodEnum
    headers: list[DefaultAttribute]
    variable: dict[str, str]
    body: Body | None
    auth: Auth | None


class Response(NamedTuple):
    status_code: int
    headers: list[DefaultAttribute]
    response_body: str | None
    response_time: int | None


class Query(NamedTuple):
    name: str
    request: Request
    response: Response | None

    def __hash__(self) -> int:
        return hash(self.request.url)

    def __str__(self) -> str:
        return self.request.url


class APIContext(NamedTuple):
    queries: tuple[Query, ...]
