# shellcheck shell=bash

if test -n "${HOME_IMPURE-}"; then
  export HOME="${HOME_IMPURE}"
fi

function sbom {
  python '__argSrcSbom__/sbom/core/cli.py' "$@"
}
