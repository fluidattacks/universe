# shellcheck shell=bash

function main {
  local resolver_directory_name="${1-}"
  local pytest_args=(
    --disable-warnings
    --showlocals
    -vv
  )

  source __argSbomEnv__/template sbom
  info "Executing unit tests for: ${resolver_directory_name}"
  pushd skims/sbom || return 1
  pytest test/unit/src/"${resolver_directory_name}" "${pytest_args[@]}"
  popd || return 1

}

main "${@}"
