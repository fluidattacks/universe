import os

import pytest

from sbom.internal.file.zip_file_traversal import contents_from_zip

from .test_helper import ensure_nested_zip_exists, setup_zip_file_test


@pytest.mark.usefixtures("_change_test_dir")
def test_contents_from_zip() -> None:
    expected = {
        os.path.join("some-dir", "a-file.txt"): "A file! nice!",
        os.path.join("b-file.txt"): "B file...",
    }
    source_dir = os.path.join("test-fixtures", "zip-source")
    ensure_nested_zip_exists(source_dir)

    archive_file_path = setup_zip_file_test(source_dir)
    result = contents_from_zip(archive_file_path, *expected.keys())
    assert result == expected
