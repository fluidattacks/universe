import logging

import pytest

from sbom.internal.collection.toml import (
    parse_toml_with_tree_sitter,
)
from test.utils import (
    capture_logs_and_result,
)


def test_key_value_pair() -> None:
    value_str = """
key = "value"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"key": "value"}


def test_unspecified_value() -> None:
    value_str = """
key = # INVALID
    """

    log_output, result = capture_logs_and_result(
        "sbom.internal.collection.toml",
        logging.ERROR,
        parse_toml_with_tree_sitter,
        value_str,
    )
    assert any("Unexpected node type ERROR with value key =" in message for message in log_output)
    assert result is not None


def test_keys() -> None:
    value_str = """
key = "value"
bare_key = "value"
bare-key = "value"
1234 = "value"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "key": "value",
        "bare_key": "value",
        "bare-key": "value",
        "1234": "value",
    }


def test_quoted_keys() -> None:
    value_str = """
"127.0.0.1" = "value"
"character encoding" = "value"
"ʎǝʞ" = "value"
'key2' = "value"
'quoted "value"' = "value"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "127.0.0.1": "value",
        "character encoding": "value",
        "ʎǝʞ": "value",
        "key2": "value",
        'quoted "value"': "value",
    }


def test_unspecified_key() -> None:
    value_str = """
= "no key name"  # INVALID
"" = "blank"     # VALID but discouraged
    """
    log_output, result = capture_logs_and_result(
        "sbom.internal.collection.toml",
        logging.ERROR,
        parse_toml_with_tree_sitter,
        value_str,
    )
    assert any(
        'Unexpected node type ERROR with value = "no key name"' in message for message in log_output
    )
    assert result is not None


def test_dotted_keys() -> None:
    value_str = """
name = "Orange"
physical.color = "orange"
physical.shape = "round"
site."google.com" = true
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "name": "Orange",
        "physical": {"color": "orange", "shape": "round"},
        "site": {"google.com": True},
    }


def test_dotted_keys_with_space_in_keys() -> None:
    value_str = """
fruit.name = "banana"     # this is best practice
fruit. color = "yellow"    # same as fruit.color
fruit . flavor = "banana"   # same as fruit.flavor
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"fruit": {"name": "banana", "color": "yellow", "flavor": "banana"}}


def test_key_multiple_times() -> None:
    value_str = """
# DO NOT DO THIS
name = "Tom"
name = "Pradyun"
"""

    log_output, result = capture_logs_and_result(
        "sbom.internal.collection.toml",
        logging.ERROR,
        parse_toml_with_tree_sitter,
        value_str,
    )
    assert any(
        "Defining a key multiple times is invalid: name" in message for message in log_output
    )
    assert result is not None


def test_key_multiple_times_1() -> None:
    value_str = """
# THIS WILL NOT WORK
spelling = "favorite"
"spelling" = "favourite"
"""
    log_output, result = capture_logs_and_result(
        "sbom.internal.collection.toml",
        logging.ERROR,
        parse_toml_with_tree_sitter,
        value_str,
    )
    assert any(
        "Defining a key multiple times is invalid: spelling" in message for message in log_output
    )
    assert result is not None


def test_add_key_after() -> None:
    value_str = """
# This makes the key "fruit" into a table.
fruit.apple.smooth = true

# So then you can add to the table "fruit" like so:
fruit.orange = 2
"""
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"fruit": {"apple": {"smooth": True}, "orange": 2}}


def test_define_doted_keys() -> None:
    value_str = """
apple.type = "fruit"
orange.type = "fruit"

apple.skin = "thin"
orange.skin = "thick"

apple.color = "red"
orange.color = "orange"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "apple": {"type": "fruit", "skin": "thin", "color": "red"},
        "orange": {"type": "fruit", "skin": "thick", "color": "orange"},
    }


def test_float_as_dotted_key() -> None:
    value_str = """
3.14159 = "pi"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"3": {"14159": "pi"}}


def test_str_unicode_charters() -> None:
    value_str = """
str = "I'm a string. \"You can quote me\". Name\tJos\u00e9\nLocation\tSF."
    """
    result = parse_toml_with_tree_sitter(value_str)
    # The result is invalid, it is the fault of the tree-sitter sintax
    assert result == {"str": "I'm a string."}


def test_str_1() -> None:
    # On a Unix system, the above multi-line string will most likely be the
    # same as:
    # str2 = "Roses are red\nViolets are blue" it mut work but the parser
    # is invalid

    # On a Windows system, it will most likely be equivalent to:
    # str3 = "Roses are red\r\nViolets are blue" it mut work but the parser
    # is invalid
    value_str = '''
str1 = """
Roses are red
Violets are blue"""
    '''
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"str1": "Roses are red\nViolets are blue"}


def test_str_2() -> None:
    value_str = '''
# The following strings are byte-for-byte equivalent:
str1 = "The quick brown fox jumps over the lazy dog."

str2 = """
The quick brown \


  fox jumps over \
    the lazy dog."""

str3 = """\
       The quick brown \
       fox jumps over \
       the lazy dog.\
       """
    '''
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "str1": "The quick brown fox jumps over the lazy dog.",
        "str2": "The quick brown \n\n  fox jumps over     the lazy dog.",
        "str3": "The quick brown        fox jumps over        the lazy dog.",
    }


def test_integer() -> None:
    value_str = """
int1 = +99
int2 = 42
int3 = 0
int4 = -17
int5 = 1_000
int6 = 5_349_221
int7 = 53_49_221  # Indian number system grouping
int8 = 1_2_3_4_5  # VALID but discouraged
# hexadecimal with prefix `0x`
hex1 = 0xDEADBEEF
hex2 = 0xdeadbeef
hex3 = 0xdead_beef

# octal with prefix `0o`
oct1 = 0o01234567
oct2 = 0o755 # useful for Unix file permissions

# binary with prefix `0b`
bin1 = 0b11010110
"""
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "int1": 99,
        "int2": 42,
        "int3": 0,
        "int4": -17,
        "int5": 1000,
        "int6": 5349221,
        "int7": 5349221,
        "int8": 12345,
        "hex1": 3735928559,
        "hex2": 3735928559,
        "hex3": 3735928559,
        "oct1": 342391,
        "oct2": 493,
        "bin1": 47529918736,
    }


def test_float() -> None:
    value_str = """
# fractional
flt1 = +1.0
flt2 = 3.1415
flt3 = -0.01

# exponent
flt4 = 5e+22
flt5 = 1e06
flt6 = -2E-2

# both
flt7 = 6.626e-34

flt8 = 224_617.445_991_228
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "flt1": 1.0,
        "flt2": 3.1415,
        "flt3": -0.01,
        "flt4": 5e22,
        "flt5": 1000000.0,
        "flt6": -0.02,
        "flt7": 6.626e-34,
        "flt8": 224617.445991228,
    }


@pytest.mark.parametrize(
    "str_value",
    [
        ("invalid_float_1 = .7"),
        ("invalid_float_2 = 7."),
        ("invalid_float_3 = 3.e+20"),
    ],
)
def test_invalid_float(str_value: str) -> None:
    log_output, result = capture_logs_and_result(
        "sbom.internal.collection.toml",
        logging.ERROR,
        parse_toml_with_tree_sitter,
        str_value,
    )
    assert any(
        f"Unexpected node type ERROR with value {str_value.strip()}" in message
        for message in log_output
    )
    assert result is not None


def test_boolean() -> None:
    value_str = """
# true
bool1 = true

# false
bool2 = false
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "bool1": True,
        "bool2": False,
    }


def test_array_1() -> None:
    value_str = """
# array of strings
arr1 = [ "foo", "bar", "baz" ]

# array of integers
arr2 = [ 1, 2, 3 ]

# array of mixed types
arr3 = [ "foo", 1, true ]

# empty array
arr4 = []
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "arr1": ["foo", "bar", "baz"],
        "arr2": [1, 2, 3],
        "arr3": ["foo", 1, True],
        "arr4": [],
    }


def test_array_2() -> None:
    value_str = '''
integers = [ 1, 2, 3 ]
colors = [ "red", "yellow", "green" ]
nested_arrays_of_ints = [ [ 1, 2 ], [3, 4, 5] ]
nested_mixed_array = [ [ 1, 2 ], ["a", "b", "c"] ]
string_array = [ "all", 'strings', """are the same""", \'\'\'type\'\'\' ]

# Mixed-type arrays are allowed
numbers = [ 0.1, 0.2, 0.5, 1, 2, 5 ]
contributors = [
  "Foo Bar <foo@example.com>",
  { name = "Baz Qux", email = "bazqux@example.com"}
]
    '''
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "integers": [1, 2, 3],
        "colors": ["red", "yellow", "green"],
        "nested_arrays_of_ints": [[1, 2], [3, 4, 5]],
        "nested_mixed_array": [[1, 2], ["a", "b", "c"]],
        "string_array": ["all", "strings", "are the same", "type"],
        "numbers": [0.1, 0.2, 0.5, 1, 2, 5],
        "contributors": [
            "Foo Bar <foo@example.com>",
            {"name": "Baz Qux", "email": "bazqux@example.com"},
        ],
    }


def test_table_1() -> None:
    value_str = """
[table-1]
key1 = "some string"
key2 = 123

[table-2]
key1 = "another string"
key2 = 456
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "table-1": {"key1": "some string", "key2": 123},
        "table-2": {"key1": "another string", "key2": 456},
    }


def test_table_2() -> None:
    value_str = """
[dog."tater.man"]
type.name = "pug"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"dog": {"tater.man": {"type": {"name": "pug"}}}}


def test_table_3() -> None:
    value_str = """
[a.b.c]            # this is best practice
[ d.e.f ]          # same as [d.e.f]
[ g .  h  . i ]    # same as [g.h.i]
[ j . "ʞ" . 'l' ]  # same as [j."ʞ".'l']
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "a": {"b": {"c": {}}},
        "d": {"e": {"f": {}}},
        "g": {"h": {"i": {}}},
        "j": {"ʞ": {"l": {}}},
    }


def test_table_4() -> None:
    value_str = """
# [x] you
# [x.y] don't
# [x.y.z] need these
[x.y.z.w] # for this to work

[x] # defining a super-table afterward is ok
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"x": {}}


def test_table_5() -> None:
    value_str = """
# Top-level table begins.
name = "Fido"
breed = "pug"

# Top-level table ends.
[owner]
name = "Regina Dogman"
member_since = 1999-08-04
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "name": "Fido",
        "breed": "pug",
        "owner": {
            "name": "Regina Dogman",
            "member_since": "1999-08-04",
        },
    }


def test_table_6() -> None:
    value_str = """
fruit.apple.color = "red"
# Defines a table named fruit
# Defines a table named fruit.apple

fruit.apple.taste.sweet = true
# Defines a table named fruit.apple.taste
# fruit and fruit.apple were already created
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {"fruit": {"apple": {"color": "red", "taste": {"sweet": True}}}}


def test_inline_table_1() -> None:
    value_str = """
name = { first = "Tom", last = "Preston-Werner" }
point = { x = 1, y = 2 }
animal = { type.name = "pug" }
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "name": {"first": "Tom", "last": "Preston-Werner"},
        "point": {"x": 1, "y": 2},
        "animal": {"type": {"name": "pug"}},
    }


def test_array_of_tables_1() -> None:
    value_str = """
[[products]]
name = "Hammer"
sku = 738594937

[[products]]  # empty table within the array

[[products]]
name = "Nail"
sku = 284758393

color = "gray"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "products": [
            {"name": "Hammer", "sku": 738594937},
            {},
            {"name": "Nail", "sku": 284758393, "color": "gray"},
        ],
    }


def test_array_of_tables_2() -> None:
    value_str = """
[[fruits]]
name = "apple"

[fruits.physical]  # subtable
color = "red"
shape = "round"

[[fruits.varieties]]  # nested array of tables
name = "red delicious"

[[fruits.varieties]]
name = "granny smith"


[[fruits]]
name = "banana"

[[fruits.varieties]]
name = "plantain"
    """
    result = parse_toml_with_tree_sitter(value_str)
    assert result == {
        "fruits": [
            {
                "name": "apple",
                "physical": {"color": "red", "shape": "round"},
                "varieties": [
                    {"name": "red delicious"},
                    {"name": "granny smith"},
                ],
            },
            {"name": "banana", "varieties": [{"name": "plantain"}]},
        ],
    }


def test_invalid_array_table_1() -> None:
    value_str = """
# INVALID TOML DOC
[fruit.physical]  # subtable, but to which parent element should it belong?
color = "red"
shape = "round"

[[fruit]]  # parser must throw an error upon discovering that "fruit" is
           # an array rather than a table
name = "apple"
    """
    log_output, result = capture_logs_and_result(
        "sbom.internal.collection.toml",
        logging.ERROR,
        parse_toml_with_tree_sitter,
        value_str,
    )
    assert any(
        "'IndexedDict' object has no attribute 'append'" in message for message in log_output
    )
    assert result is not None


def test_invalid_array_table_2() -> None:
    value_str = """
# INVALID TOML DOC
[[fruits]]
name = "apple"

[[fruits.varieties]]
name = "red delicious"

# INVALID: This table conflicts with the previous array of tables
[fruits.varieties]
name = "granny smith"
    """
    log_output, result = capture_logs_and_result(
        "sbom.internal.collection.toml",
        logging.ERROR,
        parse_toml_with_tree_sitter,
        value_str,
    )
    assert any(
        "Defining a key multiple times is invalid: varieties" in message for message in log_output
    )
    assert result is not None
