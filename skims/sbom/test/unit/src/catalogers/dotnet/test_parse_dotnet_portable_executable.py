import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.dotnet.parse_dotnet_portable_executable import (
    DotnetPortableExecutableEntry,
    parse_dotnet_portable_executable,
)


def test_parse_dotnet_portable_executable() -> None:
    expected_packages = [
        Package(
            name="EntityFramework",
            version="6.5.1",
            language=Language.DOTNET,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/dotnet/EntityFramework.dll",
                        file_system_id=None,
                        line=None,
                    ),
                    access_path="test/lib/data/dependencies/dotnet/EntityFramework.dll",
                    annotations={},
                ),
            ],
            type=PackageType.DotnetPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=DotnetPortableExecutableEntry(
                assembly_version="6.0.0.0",
                legal_copyright=("© Microsoft Corporation. All rights reserved."),
                company_name="Microsoft Corporation",
                product_name="Microsoft Entity Framework",
                product_version="6.5.1",
                comments="EntityFramework.dll",
                internal_name="EntityFramework.dll",
            ),
            p_url="pkg:nuget/EntityFramework@6.5.1",
        ),
    ]
    fixture = os.path.join("test/lib/data/dependencies/dotnet", "EntityFramework.dll")
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, relations = parse_dotnet_portable_executable(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        for rel in relations:
            rel.from_.health_metadata = None  # type: ignore
            rel.from_.licenses = []  # type: ignore
            rel.to_.health_metadata = None  # type: ignore
            rel.to_.licenses = []  # type: ignore
        assert pkgs == expected_packages
