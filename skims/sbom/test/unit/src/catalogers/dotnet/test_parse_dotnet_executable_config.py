import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.dependency_type import DependencyType
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.file.scope import Scope
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.dotnet.parse_dotnet_exe_config import (
    parse_dotnet_config_executable,
)


def test_parse_dotnet_config_executable() -> None:
    expected_packages: list[Package] = [
        Package(
            name=".net_framework",
            version="4.5.2",
            language=Language.DOTNET,
            licenses=[],
            locations=[
                Location(
                    scope=Scope.PROD,
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/dotnet/my_project.exe.config",
                        file_system_id=None,
                        line=4,
                    ),
                    access_path="test/lib/data/dependencies/dotnet/my_project.exe.config",
                    annotations={},
                    dependency_type=DependencyType.DIRECT,
                ),
            ],
            type=PackageType.DotnetPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:nuget/.net_framework@4.5.2",
        ),
    ]
    fixture = os.path.join("test/lib/data/dependencies/dotnet", "my_project.exe.config")
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_dotnet_config_executable(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        assert pkgs == expected_packages
