from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.golang.package import (
    GolangModuleEntry,
)
from sbom.pkg.cataloger.golang.parse_go_mod import (
    parse_go_mod,
)
from sbom.sources.directory_source import (
    Directory,
)


def test_parse_go_mod() -> None:
    fixture = "test/lib/data/dependencies/go/go-sum-hashes/go.mod"
    expected_packages = [
        Package(
            name="github.com/CycloneDX/cyclonedx-go",
            version="0.6.0",
            language=Language.GO,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/go/go-sum-hashes/go.mod",
                        file_system_id=None,
                        line=11,
                    ),
                    access_path="test/lib/data/dependencies/go/go-sum-hashes/go.mod",
                    annotations={},
                ),
            ],
            type=PackageType.GoModulePkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:golang/github.com/CycloneDX/cyclonedx-go@0.6.0",
        ),
        Package(
            name="github.com/acarl005/stripansi",
            version="0.0.0-20180116102854-5a71ef0e047d",
            language=Language.GO,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/go/go-sum-hashes/go.mod",
                        file_system_id=None,
                        line=7,
                    ),
                    access_path="test/lib/data/dependencies/go/go-sum-hashes/go.mod",
                    annotations={},
                ),
            ],
            type=PackageType.GoModulePkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=GolangModuleEntry(h1_digest="h1:licZJFw2RwpHMqeKTCYkitsPqHNxTmd4SNR5r94FGM8="),
            p_url=("pkg:golang/github.com/acarl005/stripansi@0.0.0-20180116102854-5a71ef0e047d"),
        ),
        Package(
            name="github.com/mgutz/ansi",
            version="0.0.0-20200706080929-d51e80ef957d",
            language=Language.GO,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/go/go-sum-hashes/go.mod",
                        file_system_id=None,
                        line=8,
                    ),
                    access_path="test/lib/data/dependencies/go/go-sum-hashes/go.mod",
                    annotations={},
                ),
            ],
            type=PackageType.GoModulePkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=GolangModuleEntry(h1_digest="h1:5PJl274Y63IEHC+7izoQE9x6ikvDFZS2mDVS3drnohI="),
            p_url=("pkg:golang/github.com/mgutz/ansi@0.0.0-20200706080929-d51e80ef957d"),
        ),
    ]
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_go_mod(
            Directory(root="test/lib/data/dependencies/go/go-sum-hashes", exclude=()),
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        assert pkgs == expected_packages
