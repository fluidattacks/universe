import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.python.parse_pipfile_lock import (
    parse_pipfile_lock_deps,
)


def test_pip_file_lock() -> None:
    fixture = os.path.join("test/lib/data/dependencies/python", "pipfile-lock/Pipfile.lock")
    expected = [
        Package(
            name="aio-pika",
            version="6.8.0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                        file_system_id=None,
                        line=24,
                    ),
                    access_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/aio-pika@6.8.0",
        ),
        Package(
            name="aiodns",
            version="2.0.0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                        file_system_id=None,
                        line=32,
                    ),
                    access_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/aiodns@2.0.0",
        ),
        Package(
            name="aiohttp",
            version="3.7.4.post0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                        file_system_id=None,
                        line=40,
                    ),
                    access_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/aiohttp@3.7.4.post0",
        ),
        Package(
            name="aiohttp-jinja2",
            version="1.4.2",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                        file_system_id=None,
                        line=48,
                    ),
                    access_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/aiohttp-jinja2@1.4.2",
        ),
        Package(
            name="astroid",
            version="2.5.2",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                        file_system_id=None,
                        line=58,
                    ),
                    access_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/astroid@2.5.2",
        ),
        Package(
            name="autopep8",
            version="1.5.6",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                        file_system_id=None,
                        line=65,
                    ),
                    access_path="test/lib/data/dependencies/python/pipfile-lock/Pipfile.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/autopep8@1.5.6",
        ),
    ]

    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_pipfile_lock_deps(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs is not None
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
    assert pkgs == expected
