import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.python.model import (
    PythonRequirementsEntry,
)
from sbom.pkg.cataloger.python.parse_poetry_lock import (
    parse_poetry_lock,
)
from sbom.pkg.cataloger.python.parse_pyproject_toml import (
    parse_pyproject_toml,
)


def test_parse_poetry_lock_file() -> None:
    fixture = os.path.join("test/lib/data/dependencies/python", "poetry/poetry.lock")
    expected = [
        Package(
            name="added-value",
            version="0.14.2",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                        file_system_id=None,
                        line=1,
                    ),
                    access_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="added-value",
                extras=[],
                markers="pkg:pypi/added-value@0.14.2",
                version_constraint=None,
            ),
            p_url="pkg:pypi/added-value@0.14.2",
        ),
        Package(
            name="alabaster",
            version="0.7.12",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                        file_system_id=None,
                        line=25,
                    ),
                    access_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="alabaster",
                extras=[],
                markers="pkg:pypi/alabaster@0.7.12",
                version_constraint=None,
            ),
            p_url="pkg:pypi/alabaster@0.7.12",
        ),
        Package(
            name="appnope",
            version="0.1.0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                        file_system_id=None,
                        line=33,
                    ),
                    access_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="appnope",
                extras=[],
                markers="pkg:pypi/appnope@0.1.0",
                version_constraint=None,
            ),
            p_url="pkg:pypi/appnope@0.1.0",
        ),
        Package(
            name="asciitree",
            version="0.3.3",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                        file_system_id=None,
                        line=42,
                    ),
                    access_path="test/lib/data/dependencies/python/poetry/poetry.lock",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="asciitree",
                extras=[],
                markers="pkg:pypi/asciitree@0.3.3",
                version_constraint=None,
            ),
            p_url="pkg:pypi/asciitree@0.3.3",
        ),
    ]
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_poetry_lock(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs is not None
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
    assert pkgs == expected


def test_pyproject_file() -> None:
    fixture = os.path.join("test/lib/data/dependencies/python", "poetry/pyproject.toml")
    expected = [
        Package(
            name="python",
            version="^3.11",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/poetry/pyproject.toml",
                        file_system_id=None,
                        line=9,
                    ),
                    access_path="test/lib/data/dependencies/python/poetry/pyproject.toml",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/python@%5E3.11",
        ),
        Package(
            name="apkid",
            version="2.1.5",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/poetry/pyproject.toml",
                        file_system_id=None,
                        line=10,
                    ),
                    access_path="test/lib/data/dependencies/python/poetry/pyproject.toml",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:pypi/apkid@2.1.5",
        ),
    ]

    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_pyproject_toml(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs is not None
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
    assert pkgs == expected
