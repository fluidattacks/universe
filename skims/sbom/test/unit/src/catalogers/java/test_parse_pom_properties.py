from pathlib import Path

import pytest

from sbom.pkg.cataloger.java.model import (
    JavaPomProperties,
)
from sbom.pkg.cataloger.java.parse_pom_properties import (
    parse_pom_properties,
)


@pytest.mark.parametrize(
    ("fixture", "expected"),
    [
        (
            "test/lib/data/dependencies/java/pom/small.pom.properties",
            JavaPomProperties(
                name=None,
                group_id="org.anchore",
                artifact_id="example-java-app-maven",
                version="0.1.0",
                path="test/lib/data/dependencies/java/pom/small.pom.properties",
                scope=None,
                extra={},
            ),
        ),
        (
            "test/lib/data/dependencies/java/pom/extra.pom.properties",
            JavaPomProperties(
                name="something-here",
                group_id="org.anchore",
                artifact_id="example-java-app-maven",
                version="0.1.0",
                path="test/lib/data/dependencies/java/pom/extra.pom.properties",
                scope=None,
                extra={"another": "thing", "sweet": "work"},
            ),
        ),
        (
            "test/lib/data/dependencies/java/pom/colon-delimited.pom.properties",
            JavaPomProperties(
                name=None,
                group_id="org.anchore",
                artifact_id="example-java-app-maven",
                version="0.1.0",
                path="test/lib/data/dependencies/java/pom/colon-delimited.pom.properties",
                scope=None,
                extra={},
            ),
        ),
        (
            "test/lib/data/dependencies/java/pom/equals-delimited-with-colons.pom.properties",
            JavaPomProperties(
                name=None,
                group_id="org.anchore",
                artifact_id="example-java:app-maven",
                version="0.1.0:something",
                path=(
                    "test/lib/data/dependencies/java/pom/"
                    "equals-delimited-with-colons.pom.properties"
                ),
                scope=None,
                extra={},
            ),
        ),
        (
            "test/lib/data/dependencies/java/pom/colon-delimited-with-equals.pom.properties",
            JavaPomProperties(
                name=None,
                group_id="org.anchore",
                artifact_id="example-java=app-maven",
                version="0.1.0=something",
                path=(
                    "test/lib/data/dependencies/java/pom/colon-delimited-with-equals.pom.properties"
                ),
                scope=None,
                extra={},
            ),
        ),
    ],
)
def test_parse_pom_properties(
    fixture: str,
    expected: JavaPomProperties,
) -> None:
    with Path(fixture).open(encoding="utf-8") as reader:
        result = parse_pom_properties(fixture, reader.read())
        assert result is not None
        assert result == expected
