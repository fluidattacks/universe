from pathlib import Path

import pytest

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.java.model import (
    JavaArchive,
    JavaPomProject,
)
from sbom.pkg.cataloger.java.parse_gradle_lockfile import (
    parse_gradle_lockfile,
)


@pytest.mark.parametrize(
    ("fixture", "expected"),
    [
        (
            "test/lib/data/dependencies/java/gradle/gradle.lockfile",
            [
                Package(
                    name="joda-time",
                    version="2.2",
                    locations=[
                        Location(
                            coordinates=Coordinates(
                                real_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                                file_system_id=None,
                                line=4,
                            ),
                            access_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                            annotations={},
                        ),
                    ],
                    language=Language.JAVA,
                    licenses=[],
                    type=PackageType.JavaPkg,
                    metadata=JavaArchive(
                        virtual_path=None,
                        manifest=None,
                        pom_properties=None,
                        pom_project=JavaPomProject(
                            path=None,
                            group_id="joda-time",
                            artifact_id="joda-time",
                            version="2.2",
                            name="joda-time",
                            parent=None,
                            description=None,
                            url=None,
                        ),
                        archive_digests=[],
                        parent=None,
                    ),
                    p_url="pkg:maven/joda-time/joda-time@2.2",
                    dependencies=None,
                    found_by=None,
                    health_metadata=None,
                ),
                Package(
                    name="junit",
                    version="4.12",
                    locations=[
                        Location(
                            coordinates=Coordinates(
                                real_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                                file_system_id=None,
                                line=5,
                            ),
                            access_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                            annotations={},
                        ),
                    ],
                    language=Language.JAVA,
                    licenses=[],
                    type=PackageType.JavaPkg,
                    metadata=JavaArchive(
                        virtual_path=None,
                        manifest=None,
                        pom_properties=None,
                        pom_project=JavaPomProject(
                            path=None,
                            group_id="junit",
                            artifact_id="junit",
                            version="4.12",
                            name="junit",
                            parent=None,
                            description=None,
                            url=None,
                        ),
                        archive_digests=[],
                        parent=None,
                    ),
                    p_url="pkg:maven/junit/junit@4.12",
                    dependencies=None,
                    found_by=None,
                    health_metadata=None,
                ),
                Package(
                    name="hamcrest-core",
                    version="1.3",
                    locations=[
                        Location(
                            coordinates=Coordinates(
                                real_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                                file_system_id=None,
                                line=6,
                            ),
                            access_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                            annotations={},
                        ),
                    ],
                    language=Language.JAVA,
                    licenses=[],
                    type=PackageType.JavaPkg,
                    metadata=JavaArchive(
                        virtual_path=None,
                        manifest=None,
                        pom_properties=None,
                        pom_project=JavaPomProject(
                            path=None,
                            group_id="org.hamcrest",
                            artifact_id="hamcrest-core",
                            version="1.3",
                            name="hamcrest-core",
                            parent=None,
                            description=None,
                            url=None,
                        ),
                        archive_digests=[],
                        parent=None,
                    ),
                    p_url="pkg:maven/org.hamcrest/hamcrest-core@1.3",
                    dependencies=None,
                    found_by=None,
                    health_metadata=None,
                ),
                Package(
                    name="commons-text",
                    version="1.8",
                    locations=[
                        Location(
                            coordinates=Coordinates(
                                real_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                                file_system_id=None,
                                line=8,
                            ),
                            access_path="test/lib/data/dependencies/java/gradle/gradle.lockfile",
                            annotations={},
                        ),
                    ],
                    language=Language.JAVA,
                    licenses=[],
                    type=PackageType.JavaPkg,
                    metadata=JavaArchive(
                        virtual_path=None,
                        manifest=None,
                        pom_properties=None,
                        pom_project=JavaPomProject(
                            path=None,
                            group_id="org.apache.commons",
                            artifact_id="commons-text",
                            version="1.8",
                            name="commons-text",
                            parent=None,
                            description=None,
                            url=None,
                        ),
                        archive_digests=[],
                        parent=None,
                    ),
                    p_url="pkg:maven/org.apache.commons/commons-text@1.8",
                    dependencies=None,
                    found_by=None,
                    health_metadata=None,
                ),
            ],
        ),
    ],
)
def test_parse_gradle_lock_file(
    fixture: str,
    expected: list[Package],
) -> None:
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_gradle_lockfile(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        assert pkgs == expected
