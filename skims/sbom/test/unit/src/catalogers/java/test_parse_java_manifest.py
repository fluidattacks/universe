from pathlib import Path

import pytest

from sbom.pkg.cataloger.java.model import (
    JavaManifest,
)
from sbom.pkg.cataloger.java.parse_java_manifest import (
    parse_java_manifest,
)


@pytest.mark.parametrize(
    ("fixture", "expected"),
    [
        (
            "test/lib/data/dependencies/java/manifest/small",
            JavaManifest(main={"Manifest-Version": "1.0"}, sections=None),
        ),
        (
            "test/lib/data/dependencies/java/manifest/standard-info",
            JavaManifest(
                main={
                    "Manifest-Version": "1.0",
                    "Name": "the-best-name",
                    "Specification-Title": "the-spec-title",
                    "Specification-Vendor": "the-spec-vendor",
                    "Specification-Version": "the-spec-version",
                    "Implementation-Title": "the-impl-title",
                    "Implementation-Vendor": "the-impl-vendor",
                    "Implementation-Version": "the-impl-version",
                },
                sections=None,
            ),
        ),
        (
            "test/lib/data/dependencies/java/manifest/extra-info",
            JavaManifest(
                main={
                    "Manifest-Version": "1.0",
                    "Archiver-Version": "Plexus Archiver",
                    "Created-By": "Apache Maven 3.6.3",
                },
                sections=[
                    {"Name": "thing-1", "Built-By": "?"},
                    {"Build-Jdk": "14.0.1", "Main-Class": "hello.HelloWorld"},
                ],
            ),
        ),
        (
            "test/lib/data/dependencies/java/manifest/extra-empty-lines",
            JavaManifest(
                main={
                    "Manifest-Version": "1.0",
                    "Archiver-Version": "Plexus Archiver",
                    "Created-By": "Apache Maven 3.6.3",
                },
                sections=[
                    {"Name": "thing-1", "Built-By": "?"},
                    {"Name": "thing-2", "Built-By": "someone!"},
                    {"Other": "things"},
                    {"Last": "item"},
                ],
            ),
        ),
        (
            "test/lib/data/dependencies/java/manifest/continuation",
            JavaManifest(
                main={
                    "Manifest-Version": "1.0",
                    "Plugin-ScmUrl": ("https://github.com/jenkinsci/plugin-pom/example-jenkins"),
                },
                sections=None,
            ),
        ),
        (
            "test/lib/data/dependencies/java/manifest/version-with-date",
            JavaManifest(
                main={
                    "Manifest-Version": "1.0",
                    "Implementation-Version": "1.3 2244 October 5 2005",
                },
                sections=None,
            ),
        ),
        (
            "test/lib/data/dependencies/java/manifest/leading-space",
            JavaManifest(
                main={
                    "Key-keykeykey": "initialconfig:com",
                    "should": "parse",
                },
                sections=None,
            ),
        ),
    ],
)
def test_parse_java_manifest(
    fixture: str,
    expected: JavaManifest,
) -> None:
    with Path(fixture).open(encoding="utf-8") as reader:
        manifest = parse_java_manifest(reader.read())
        assert manifest == expected
