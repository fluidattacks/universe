from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.github.parse_github_actions import (
    parse_github_actions_deps,
)


def test_parse_github_actions() -> None:
    fixture = "test/lib/data/dependencies/github/dev.yaml"
    expected_packages = [
        Package(
            name="aws-actions/configure-aws-credentials",
            version="v1",
            language=Language.UNKNOWN_LANGUAGE,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/github/dev.yaml",
                        file_system_id=None,
                        line=35,
                    ),
                    access_path="test/lib/data/dependencies/github/dev.yaml",
                    annotations={},
                ),
            ],
            type=PackageType.GithubActionPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:github/aws-actions/configure-aws-credentials@v1",
        ),
        Package(
            name="actions/checkout",
            version="v2",
            language=Language.UNKNOWN_LANGUAGE,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/github/dev.yaml",
                        file_system_id=None,
                        line=45,
                    ),
                    access_path="test/lib/data/dependencies/github/dev.yaml",
                    annotations={},
                ),
            ],
            type=PackageType.GithubActionPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:github/actions/checkout@v2",
        ),
        Package(
            name="ruby/setup-ruby",
            version="v1",
            language=Language.UNKNOWN_LANGUAGE,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/github/dev.yaml",
                        file_system_id=None,
                        line=48,
                    ),
                    access_path="test/lib/data/dependencies/github/dev.yaml",
                    annotations={},
                ),
            ],
            type=PackageType.GithubActionPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:github/ruby/setup-ruby@v1",
        ),
        Package(
            name="gradle/gradle-build-action",
            version="2.4.0",
            language=Language.UNKNOWN_LANGUAGE,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/github/dev.yaml",
                        file_system_id=None,
                        line=53,
                    ),
                    access_path="test/lib/data/dependencies/github/dev.yaml",
                    annotations={},
                ),
            ],
            type=PackageType.GithubActionPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:github/gradle/gradle-build-action@2.4.0",
        ),
    ]

    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_github_actions_deps(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs == expected_packages
