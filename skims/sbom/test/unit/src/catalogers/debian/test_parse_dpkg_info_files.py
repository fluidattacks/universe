from pathlib import Path

import pytest

from sbom.model.core import (
    Digest,
)
from sbom.pkg.cataloger.debian.model import DpkgFileRecord
from sbom.pkg.cataloger.debian.parse_dpkg_info_files import (
    parse_dpkg_conffile_info,
    parse_dpkg_md5_info,
)


@pytest.mark.parametrize(
    ("fixture", "expected"),
    [
        (
            "test/lib/data/dependencies/debian/info/zlib1g.md5sums",
            [
                DpkgFileRecord(
                    path="/lib/x86_64-linux-gnu/libz.so.1.2.11",
                    digest=Digest(
                        algorithm="md5",
                        value="55f905631797551d4d936a34c7e73474",
                    ),
                ),
                DpkgFileRecord(
                    path="/usr/share/doc/zlib1g/changelog.Debian.gz",
                    digest=Digest(
                        algorithm="md5",
                        value="cede84bda30d2380217f97753c8ccf3a",
                    ),
                ),
                DpkgFileRecord(
                    path="/usr/share/doc/zlib1g/changelog.gz",
                    digest=Digest(
                        algorithm="md5",
                        value="f3c9dafa6da7992c47328b4464f6d122",
                    ),
                ),
                DpkgFileRecord(
                    path="/usr/share/doc/zlib1g/copyright",
                    digest=Digest(
                        algorithm="md5",
                        value="a4fae96070439a5209a62ae5b8017ab2",
                    ),
                ),
            ],
        ),
    ],
)
def test_md5_sum_info_parsing(
    fixture: str,
    expected: list[DpkgFileRecord],
) -> None:
    with Path(fixture).open(encoding="utf-8") as handler:
        actual = parse_dpkg_md5_info(handler)
        assert expected == actual


@pytest.mark.parametrize(
    ("fixture", "expected"),
    [
        (
            "test/lib/data/dependencies/debian/info/util-linux.conffiles",
            [
                DpkgFileRecord(path="/etc/default/hwclock", is_config_file=True),
                DpkgFileRecord(path="/etc/init.d/hwclock.sh", is_config_file=True),
                DpkgFileRecord(path="/etc/pam.d/runuser", is_config_file=True),
                DpkgFileRecord(path="/etc/pam.d/runuser-l", is_config_file=True),
                DpkgFileRecord(path="/etc/pam.d/su", is_config_file=True),
                DpkgFileRecord(path="/etc/pam.d/su-l", is_config_file=True),
            ],
        ),
    ],
)
def test_conffile_info_parsing(
    fixture: str,
    expected: list[DpkgFileRecord],
) -> None:
    with Path(fixture).open(encoding="utf-8") as handler:
        actual = parse_dpkg_conffile_info(handler)
        assert expected == actual
