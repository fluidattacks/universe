from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.php.parse_serialized import (
    parse_pecl_serialized,
)


def test_parse_composer_lock() -> None:
    fixture = "test/lib/data/dependencies/php/memcached.reg"
    expected_packages = [
        Package(
            name="memcached",
            version="3.2.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/php/memcached.reg",
                        file_system_id=None,
                        line=None,
                    ),
                    access_path="test/lib/data/dependencies/php/memcached.reg",
                    annotations={},
                ),
            ],
            language=Language.PHP,
            licenses=[],
            type=PackageType.PhpPeclPkg,
            metadata=None,
            p_url="pkg:pecl/memcached@3.2.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
    ]
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_pecl_serialized(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        assert pkgs == expected_packages
