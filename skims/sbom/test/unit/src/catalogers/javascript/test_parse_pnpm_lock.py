from pathlib import Path

from sbom.file.coordinates import Coordinates
from sbom.file.dependency_type import DependencyType
from sbom.file.location import Location, new_location
from sbom.file.location_read_closer import LocationReadCloser
from sbom.file.scope import Scope
from sbom.model.core import Digest, Language, Package, PackageType
from sbom.pkg.cataloger.javascript.model import PnpmEntry
from sbom.pkg.cataloger.javascript.parse_pnpm_lock import parse_pnpm_lock


def test_parse_pnpm_lock() -> None:
    expected_packages = [
        Package(
            name="slug",
            version="0.9.0",
            language=Language.JAVASCRIPT,
            licenses=[],
            locations=[
                Location(
                    scope=Scope.PROD,
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                        file_system_id=None,
                        line=12,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                    annotations={},
                    dependency_type=DependencyType.TRANSITIVE,
                ),
            ],
            type=PackageType.NpmPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PnpmEntry(is_dev=False, integrity=Digest(algorithm="sha-512", value=None)),
            p_url="pkg:npm/slug@0.9.0",
        ),
        Package(
            name="zod",
            version="3.21.4",
            language=Language.JAVASCRIPT,
            licenses=[],
            locations=[
                Location(
                    scope=Scope.PROD,
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                        file_system_id=None,
                        line=17,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                    annotations={},
                    dependency_type=DependencyType.TRANSITIVE,
                ),
            ],
            type=PackageType.NpmPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PnpmEntry(is_dev=False, integrity=Digest(algorithm="sha-512", value=None)),
            p_url="pkg:npm/zod@3.21.4",
        ),
        Package(
            name="@zag-js/script-manager",
            version="0.8.6",
            language=Language.JAVASCRIPT,
            licenses=[],
            locations=[
                Location(
                    scope=Scope.PROD,
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                        file_system_id=None,
                        line=20,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                    annotations={},
                    dependency_type=DependencyType.TRANSITIVE,
                ),
            ],
            type=PackageType.NpmPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PnpmEntry(is_dev=False, integrity=Digest(algorithm="sha-512", value=None)),
            p_url="pkg:npm/%40zag-js/script-manager@0.8.6",
        ),
        Package(
            name="hoek",
            version="5.0.0",
            language=Language.JAVASCRIPT,
            licenses=[],
            locations=[
                Location(
                    scope=Scope.DEV,
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                        file_system_id=None,
                        line=24,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                    annotations={},
                    dependency_type=DependencyType.TRANSITIVE,
                ),
            ],
            type=PackageType.NpmPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PnpmEntry(is_dev=True, integrity=Digest(algorithm="sha-512", value=None)),
            p_url="pkg:npm/hoek@5.0.0",
        ),
        Package(
            name="yargs",
            version="17.7.1",
            language=Language.JAVASCRIPT,
            licenses=[],
            locations=[
                Location(
                    scope=Scope.DEV,
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                        file_system_id=None,
                        line=28,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                    annotations={},
                    dependency_type=DependencyType.TRANSITIVE,
                ),
            ],
            type=PackageType.NpmPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PnpmEntry(is_dev=True, integrity=Digest(algorithm="sha-512", value=None)),
            p_url="pkg:npm/yargs@17.7.1",
        ),
        Package(
            name="@builder.io/qwik",
            version="1.2.6",
            language=Language.JAVASCRIPT,
            licenses=[],
            locations=[
                Location(
                    scope=Scope.PROD,
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                        file_system_id=None,
                        line=41,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml",
                    annotations={},
                    dependency_type=DependencyType.TRANSITIVE,
                ),
            ],
            type=PackageType.NpmPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PnpmEntry(is_dev=False, integrity=Digest(algorithm="sha-512", value=None)),
            p_url="pkg:npm/%40builder.io/qwik@1.2.6",
        ),
    ]

    fixture = "test/lib/data/dependencies/javascript/pnpm-v6/pnpm-lock.yaml"
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_pnpm_lock(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        assert pkgs == expected_packages
