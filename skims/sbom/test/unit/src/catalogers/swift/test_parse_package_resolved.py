import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.swift.package import (
    SwiftPackageManagerResolvedEntry,
)
from sbom.pkg.cataloger.swift.parse_package_resolved import (
    parse_package_resolved,
)


def test_parse_pacakge_resolved() -> None:
    fixture = os.path.join("test/lib/data/dependencies/swift", "Package.resolved")
    expected = [
        Package(
            name="swift-algorithms",
            version="1.0.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/swift/Package.resolved",
                        file_system_id=None,
                        line=3,
                    ),
                    access_path="test/lib/data/dependencies/swift/Package.resolved",
                    annotations={},
                ),
            ],
            language=Language.SWIFT,
            licenses=[],
            type=PackageType.SwiftPkg,
            metadata=SwiftPackageManagerResolvedEntry(
                revision="b14b7f4c528c942f121c8b860b9410b2bf57825e",
            ),
            p_url=("pkg:swift/github.com/apple/swift-algorithms.git/swift-algorithms@1.0.0"),
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
        Package(
            name="swift-async-algorithms",
            version="0.1.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/swift/Package.resolved",
                        file_system_id=None,
                        line=12,
                    ),
                    access_path="test/lib/data/dependencies/swift/Package.resolved",
                    annotations={},
                ),
            ],
            language=Language.SWIFT,
            licenses=[],
            type=PackageType.SwiftPkg,
            metadata=SwiftPackageManagerResolvedEntry(
                revision="9cfed92b026c524674ed869a4ff2dcfdeedf8a2a",
            ),
            p_url=(
                "pkg:swift/github.com/apple/swift-async-algorithms.git"
                "/swift-async-algorithms@0.1.0"
            ),
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
        Package(
            name="swift-atomics",
            version="1.1.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/swift/Package.resolved",
                        file_system_id=None,
                        line=21,
                    ),
                    access_path="test/lib/data/dependencies/swift/Package.resolved",
                    annotations={},
                ),
            ],
            language=Language.SWIFT,
            licenses=[],
            type=PackageType.SwiftPkg,
            metadata=SwiftPackageManagerResolvedEntry(
                revision="6c89474e62719ddcc1e9614989fff2f68208fe10",
            ),
            p_url=("pkg:swift/github.com/apple/swift-atomics.git/swift-atomics@1.1.0"),
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
        Package(
            name="swift-collections",
            version="1.0.4",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/swift/Package.resolved",
                        file_system_id=None,
                        line=30,
                    ),
                    access_path="test/lib/data/dependencies/swift/Package.resolved",
                    annotations={},
                ),
            ],
            language=Language.SWIFT,
            licenses=[],
            type=PackageType.SwiftPkg,
            metadata=SwiftPackageManagerResolvedEntry(
                revision="937e904258d22af6e447a0b72c0bc67583ef64a2",
            ),
            p_url=("pkg:swift/github.com/apple/swift-collections.git/swift-collections@1.0.4"),
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
        Package(
            name="swift-numerics",
            version="1.0.2",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/swift/Package.resolved",
                        file_system_id=None,
                        line=39,
                    ),
                    access_path="test/lib/data/dependencies/swift/Package.resolved",
                    annotations={},
                ),
            ],
            language=Language.SWIFT,
            licenses=[],
            type=PackageType.SwiftPkg,
            metadata=SwiftPackageManagerResolvedEntry(
                revision="0a5bc04095a675662cf24757cc0640aa2204253b",
            ),
            p_url=("pkg:swift/github.com/apple/swift-numerics/swift-numerics@1.0.2"),
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
    ]
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_package_resolved(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
    assert pkgs == expected
