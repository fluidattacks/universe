from pathlib import Path

import pytest

from sbom.linux.release import (
    Release,
    identify_release,
    parse_os_release,
)
from sbom.sources.directory_source import (
    new_from_directory_path,
)


@pytest.mark.parametrize(
    "test_item",
    [
        {
            "fixture": "test-fixtures/os/alpine",
            "release": {
                "PrettyName": "Alpine Linux v3.11",
                "Name": "Alpine Linux",
                "ID": "alpine",
                "IDLike": None,
                "VersionID": "3.11.6",
                "HomeURL": "https://alpinelinux.org/",
                "BugReportURL": "https://bugs.alpinelinux.org/",
            },
        },
        {
            "fixture": "test-fixtures/os/amazon",
            "release": {
                "PrettyName": "Amazon Linux 2",
                "Name": "Amazon Linux",
                "ID": "amzn",
                "IDLike": [
                    "centos",
                    "rhel",
                    "fedora",
                ],
                "Version": "2",
                "VersionID": "2",
                "HomeURL": "https://amazonlinux.com/",
                "CPEName": "cpe:2.3:o:amazon:amazon_linux:2",
            },
        },
        {
            "fixture": "test-fixtures/os/busybox",
            "release": {
                "PrettyName": "BusyBox v1.31.1",
                "Name": "busybox",
                "ID": "busybox",
                "IDLike": ["busybox"],
                "Version": "1.31.1",
                "VersionID": "1.31.1",
            },
        },
        {
            "fixture": "test-fixtures/os/centos",
            "release": {
                "PrettyName": "CentOS Linux 8 (Core)",
                "Name": "CentOS Linux",
                "ID": "centos",
                "IDLike": {
                    "rhel",
                    "fedora",
                },
                "Version": "8 (Core)",
                "VersionID": "8",
                "HomeURL": "https://www.centos.org/",
                "BugReportURL": "https://bugs.centos.org/",
                "CPEName": "cpe:/o:centos:centos:8",
            },
        },
        {
            "fixture": "test-fixtures/os/debian",
            "release": {
                "PrettyName": "Debian GNU/Linux 8 (jessie)",
                "Name": "Debian GNU/Linux",
                "ID": "debian",
                "IDLike": None,
                "Version": "8 (jessie)",
                "VersionID": "8",
                "HomeURL": "http://www.debian.org/",
                "SupportURL": "http://www.debian.org/support",
                "BugReportURL": "https://bugs.debian.org/",
            },
        },
        {
            "fixture": "test-fixtures/os/fedora",
            "release": {
                "PrettyName": "Fedora Linux 36 (Container Image)",
                "Name": "Fedora Linux",
                "ID": "fedora",
                "IDLike": None,
                "Version": "36 (Container Image)",
                "VersionID": "36",
                "Variant": "Container Image",
                "VariantID": "container",
                "HomeURL": "https://fedoraproject.org/",
                "SupportURL": "https://ask.fedoraproject.org/",
                "BugReportURL": "https://bugzilla.redhat.com/",
                "PrivacyPolicyURL": ("https://fedoraproject.org/wiki/Legal:PrivacyPolicy"),
                "CPEName": "cpe:/o:fedoraproject:fedora:36",
                "SupportEnd": "2023-05-16",
            },
        },
        {
            "fixture": "test-fixtures/os/redhat",
            "release": {
                "PrettyName": "Red Hat Enterprise Linux Server 7.3 (Maipo)",
                "Name": "Red Hat Enterprise Linux Server",
                "ID": "rhel",
                "IDLike": {"fedora"},
                "Version": "7.3 (Maipo)",
                "VersionID": "7.3",
                "HomeURL": "https://www.redhat.com/",
                "BugReportURL": "https://bugzilla.redhat.com/",
                "CPEName": "cpe:/o:redhat:enterprise_linux:7.3:GA:server",
            },
        },
        {
            "fixture": "test-fixtures/os/ubuntu",
            "release": {
                "PrettyName": "Ubuntu 20.04 LTS",
                "Name": "Ubuntu",
                "ID": "ubuntu",
                "IDLike": {"debian"},
                "Version": "20.04 LTS (Focal Fossa)",
                "VersionCodeName": "focal",
                "VersionID": "20.04",
                "HomeURL": "https://www.ubuntu.com/",
                "SupportURL": "https://help.ubuntu.com/",
                "BugReportURL": "https://bugs.launchpad.net/ubuntu/",
                "PrivacyPolicyURL": (
                    "https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
                ),
            },
        },
        {
            "fixture": "test-fixtures/os/oraclelinux",
            "release": {
                "PrettyName": "Oracle Linux Server 8.3",
                "Name": "Oracle Linux Server",
                "ID": "ol",
                "IDLike": {"fedora"},
                "Version": "8.3",
                "VersionID": "8.3",
                "Variant": "Server",
                "VariantID": "server",
                "HomeURL": "https://linux.oracle.com/",
                "BugReportURL": "https://bugzilla.oracle.com/",
                "CPEName": "cpe:/o:oracle:linux:8:3:server",
            },
        },
        {
            "fixture": "test-fixtures/os/empty",
        },
        {
            "fixture": "test-fixtures/os/custom",
            "release": {
                "PrettyName": "CentOS Linux 8 (Core)",
                "Name": "Scientific Linux",
                "ID": "scientific",
                "IDLike": {
                    "rhel",
                    "fedora",
                },
                "Version": "16 (Core)",
                "VersionID": "8",
                "HomeURL": "https://www.centos.org/",
                "BugReportURL": "https://bugs.centos.org/",
                "CPEName": "cpe:/o:centos:centos:8",
            },
        },
        {
            "fixture": "test-fixtures/os/opensuse-leap",
            "release": {
                "PrettyName": "openSUSE Leap 15.2",
                "Name": "openSUSE Leap",
                "ID": "opensuse-leap",
                "IDLike": {
                    "suse",
                    "opensuse",
                },
                "Version": "15.2",
                "VersionID": "15.2",
                "HomeURL": "https://www.opensuse.org/",
                "BugReportURL": "https://bugs.opensuse.org",
                "CPEName": "cpe:/o:opensuse:leap:15.2",
            },
        },
        {
            "fixture": "test-fixtures/os/sles",
            "release": {
                "PrettyName": "SUSE Linux Enterprise Server 15 SP2",
                "Name": "SLES",
                "ID": "sles",
                "IDLike": {"suse"},
                "Version": "15-SP2",
                "VersionID": "15.2",
                "CPEName": "cpe:/o:suse:sles:15:sp2",
            },
        },
        {
            "fixture": "test-fixtures/os/photon",
            "release": {
                "PrettyName": "VMware Photon OS/Linux",
                "Name": "VMware Photon OS",
                "ID": "photon",
                "IDLike": None,
                "Version": "2.0",
                "VersionID": "2.0",
                "HomeURL": "https://vmware.github.io/photon/",
                "BugReportURL": "https://github.com/vmware/photon/issues",
            },
        },
        {
            "fixture": "test-fixtures/os/arch",
            "release": {
                "PrettyName": "Arch Linux",
                "Name": "Arch Linux",
                "ID": "arch",
                "IDLike": None,
                "BuildID": "rolling",
                "HomeURL": "https://www.archlinux.org/",
                "SupportURL": "https://bbs.archlinux.org/",
                "BugReportURL": "https://bugs.archlinux.org/",
            },
        },
        {
            "fixture": "test-fixtures/partial-fields/missing-id",
            "release": {
                "Name": "Debian GNU/Linux",
                "IDLike": {"debian"},
                "VersionID": "8",
            },
        },
        {
            "fixture": "test-fixtures/partial-fields/unknown-id",
            "release": {
                "Name": "Debian GNU/Linux",
                "ID": "my-awesome-distro",
                "IDLike": {"debian"},
                "VersionID": "8",
            },
        },
        {
            "fixture": "test-fixtures/partial-fields/missing-version",
            "release": {
                "Name": "Debian GNU/Linux",
                "IDLike": {"debian"},
            },
        },
        {
            "fixture": "test-fixtures/os/centos6",
            "release": {
                "PrettyName": "centos",
                "Name": "centos",
                "ID": "centos",
                "IDLike": {"centos"},
                "Version": "6",
                "VersionID": "6",
                "CPEName": "cpe:/o:centos:linux:6:GA",
            },
        },
        {
            "fixture": "test-fixtures/os/centos5",
            "release": {
                "PrettyName": "CentOS",
                "Name": "centos",
                "ID": "centos",
                "IDLike": {"centos"},
                "Version": "5.7",
                "VersionID": "5.7",
            },
        },
        {
            "fixture": "test-fixtures/os/mariner",
            "release": {
                "PrettyName": "CBL-Mariner/Linux",
                "Name": "Common Base Linux Mariner",
                "ID": "mariner",
                "IDLike": None,
                "Version": "1.0.20210901",
                "VersionID": "1.0",
                "HomeURL": "https://aka.ms/cbl-mariner",
                "SupportURL": "https://aka.ms/cbl-mariner",
                "BugReportURL": "https://aka.ms/cbl-mariner",
            },
        },
        {
            "fixture": "test-fixtures/os/rockylinux",
            "release": {
                "PrettyName": "Rocky Linux 8.4 (Green Obsidian)",
                "Name": "Rocky Linux",
                "ID": "rocky",
                "IDLike": {
                    "rhel",
                    "fedora",
                },
                "Version": "8.4 (Green Obsidian)",
                "VersionID": "8.4",
                "HomeURL": "https://rockylinux.org/",
                "BugReportURL": "https://bugs.rockylinux.org/",
                "CPEName": "cpe:/o:rocky:rocky:8.4:GA",
            },
        },
        {
            "fixture": "test-fixtures/os/almalinux",
            "release": {
                "PrettyName": "AlmaLinux 8.4 (Electric Cheetah)",
                "Name": "AlmaLinux",
                "ID": "almalinux",
                "IDLike": {
                    "fedora",
                    "centos",
                    "rhel",
                },
                "Version": "8.4 (Electric Cheetah)",
                "VersionID": "8.4",
                "HomeURL": "https://almalinux.org/",
                "BugReportURL": "https://bugs.almalinux.org/",
                "CPEName": "cpe:/o:almalinux:almalinux:8.4:GA",
            },
        },
        {
            "fixture": "test-fixtures/os/wolfi",
            "release": {
                "PrettyName": "Wolfi",
                "Name": "Wolfi",
                "ID": "wolfi",
                "VersionID": "20220914",
                "HomeURL": "https://wolfi.dev",
            },
        },
    ],
)
@pytest.mark.usefixtures("_change_test_dir")
def test_identify_release(
    test_item: dict[str, dict[str, str]],
) -> None:
    if "release" not in test_item:
        pytest.skip("No release data provided")
    fixture: str = str(test_item["fixture"])
    if fixture in (
        "test-fixtures/os/centos5",
        "test-fixtures/os/centos6",
        "test-fixtures/os/busybox",
    ):
        pytest.skip("Release info format is not supported")
    expected_release = Release(
        pretty_name=test_item["release"].get("PrettyName", ""),
        name=test_item["release"].get("Name", ""),
        id_=test_item["release"].get("ID", ""),
        id_like=sorted(test_item["release"].get("IDLike", []) or []),
        version=test_item["release"].get("Version", ""),
        version_id=test_item["release"].get("VersionID", ""),
        version_code_name=test_item["release"].get("VersionCodeName", ""),
        build_id=test_item["release"].get("BuildID", ""),
        image_id=test_item["release"].get("ImageID", ""),
        image_version=test_item["release"].get("ImageVersion", ""),
        variant=test_item["release"].get("Variant", ""),
        variant_id=test_item["release"].get("VariantID", ""),
        home_url=test_item["release"].get("HomeURL", ""),
        support_url=test_item["release"].get("SupportURL", ""),
        bug_report_url=test_item["release"].get("BugReportURL", ""),
        privacy_policy_url=test_item["release"].get("PrivacyPolicyURL", ""),
        cpe_name=test_item["release"].get("CPEName", ""),
        support_end=test_item["release"].get("SupportEnd", ""),
    )
    if source := new_from_directory_path(path=fixture, exclude=()):
        resolver = source.file_resolver()
        release = identify_release(resolver)
        assert release == expected_release


@pytest.mark.parametrize(
    "test_item",
    [
        {
            "fixture": "test-fixtures/ubuntu-20.04",
            "release": {
                "PrettyName": "Ubuntu 20.04 LTS",
                "Name": "Ubuntu",
                "ID": "ubuntu",
                "IDLike": {"debian"},
                "Version": "20.04 LTS (Focal Fossa)",
                "VersionID": "20.04",
                "VersionCodename": "focal",
                "HomeURL": "https://www.ubuntu.com/",
                "SupportURL": "https://help.ubuntu.com/",
                "BugReportURL": "https://bugs.launchpad.net/ubuntu/",
                "PrivacyPolicyURL": (
                    "https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
                ),
            },
        },
        {
            "fixture": "test-fixtures/debian-8",
            "release": {
                "PrettyName": "Debian GNU/Linux 8 (jessie)",
                "Name": "Debian GNU/Linux",
                "ID": "debian",
                "IDLike": None,
                "Version": "8 (jessie)",
                "VersionID": "8",
                "HomeURL": "http://www.debian.org/",
                "SupportURL": "http://www.debian.org/support",
                "BugReportURL": "https://bugs.debian.org/",
            },
        },
        {
            "fixture": "test-fixtures/centos-8",
            "release": {
                "PrettyName": "CentOS Linux 8 (Core)",
                "Name": "CentOS Linux",
                "ID": "centos",
                "IDLike": {
                    "rhel",
                    "fedora",
                },
                "Version": "8 (Core)",
                "VersionID": "8",
                "HomeURL": "https://www.centos.org/",
                "BugReportURL": "https://bugs.centos.org/",
                "CPEName": "cpe:/o:centos:centos:8",
            },
        },
        {
            "fixture": "test-fixtures/rhel-8",
            "release": {
                "PrettyName": "Red Hat Enterprise Linux 8.1 (Ootpa)",
                "Name": "Red Hat Enterprise Linux",
                "ID": "rhel",
                "IDLike": {"fedora"},
                "Version": "8.1 (Ootpa)",
                "VersionID": "8.1",
                "HomeURL": "https://www.redhat.com/",
                "BugReportURL": "https://bugzilla.redhat.com/",
                "CPEName": "cpe:/o:redhat:enterprise_linux:8.1:GA",
            },
        },
        {
            "fixture": "test-fixtures/unprintable",
            "release": {
                "PrettyName": "Debian GNU/Linux 8 (jessie)",
                "Name": "Debian GNU/Linux",
                "ID": "debian",
                "IDLike": None,
                "Version": "8 (jessie)",
                "VersionID": "8",
                "HomeURL": "http://www.debian.org/",
                "SupportURL": "http://www.debian.org/support",
                "BugReportURL": "https://bugs.debian.org/",
            },
        },
    ],
)
@pytest.mark.usefixtures("_change_test_dir")
def test_parse_os_release(
    test_item: dict[str, dict[str, str]],
) -> None:
    fixture: str = str(test_item["fixture"])
    expected_release = Release(
        pretty_name=test_item["release"].get("PrettyName", ""),
        name=test_item["release"].get("Name", ""),
        id_=test_item["release"].get("ID", ""),
        id_like=sorted(test_item["release"].get("IDLike", []) or []),
        version=test_item["release"].get("Version", ""),
        version_id=test_item["release"].get("VersionID", ""),
        version_code_name=test_item["release"].get("VersionCodename", ""),
        build_id=test_item["release"].get("BuildID", ""),
        image_id=test_item["release"].get("ImageID", ""),
        image_version=test_item["release"].get("ImageVersion", ""),
        variant=test_item["release"].get("Variant", ""),
        variant_id=test_item["release"].get("VariantID", ""),
        home_url=test_item["release"].get("HomeURL", ""),
        support_url=test_item["release"].get("SupportURL", ""),
        bug_report_url=test_item["release"].get("BugReportURL", ""),
        privacy_policy_url=test_item["release"].get("PrivacyPolicyURL", ""),
        cpe_name=test_item["release"].get("CPEName", ""),
        support_end=test_item["release"].get("SupportEnd", ""),
    )
    with Path(fixture).open(encoding="utf-8") as handler:
        parsed_release = parse_os_release(handler.read())
    assert parsed_release == expected_release
