import os
from collections.abc import Generator
from pathlib import Path
from typing import Any

import pytest


@pytest.fixture
def _change_test_dir(
    request: pytest.FixtureRequest,
) -> Generator[None, Any, None]:
    old_cwd = Path.cwd()
    os.chdir(Path(request.module.__file__).parent)
    yield
    os.chdir(old_cwd)
