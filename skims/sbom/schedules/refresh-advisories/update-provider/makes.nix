{ inputs, makeScript, outputs, projectPath, ... }:
let
  vunnel = inputs.nixpkgs.vunnel.overrideAttrs {
    patches = [
      (projectPath
        "/skims/sbom/schedules/refresh-advisories/build-db/ossf_provider.patch")
    ];
  };
in {
  jobs."/skims/sbom/schedules/refresh-advisories/update-provider" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "sbom-schedules-refresh-advisories-update-providers";
    replace = {
      __argGrypeConfig__ = projectPath
        "/skims/sbom/schedules/refresh-advisories/build-db/grype-db.yaml";
      __argSecretsDev__ = projectPath "/common/secrets/dev.yaml";
      __argMalwareRepo__ = builtins.fetchTarball {
        url =
          "https://github.com/ossf/malicious-packages/archive/609a097f715ccb9cb9e354f0df8b5d6f05e90b5f.tar.gz";
        sha256 = "sha256:0way17nbdwj85i3s6v4n14c0xr8mhzjl2a83vssxwhyxp31lbrdl";
      };
    };
    searchPaths = {
      bin = [
        inputs.nixpkgs.git
        inputs.nixpkgs.python311
        vunnel
        outputs."/skims/sbom/schedules/refresh-advisories/build-db/grype-db"
      ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
