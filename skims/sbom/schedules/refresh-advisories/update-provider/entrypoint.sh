# shellcheck shell=bash

function main {
  local provider="${1}"

  : && aws_login "dev" "3600" \
    && export MALWARE_REPO="__argMalwareRepo__" \
    && sops_export_vars __argSecretsDev__ GITHUB_API_TOKEN \
    && temp_cache_provider=$(mktemp) \
    && if aws s3 cp --quiet "s3://fluidattacks.public.storage/sbom/grype-bd/${provider}" "${temp_cache_provider}"; then
      grype-db --config __argGrypeConfig__ cache restore --path "${temp_cache_provider}"
    else
      echo 'No data cache found'
    fi \
    && grype-db --config __argGrypeConfig__ pull -v -p "${provider}" \
    && grype-db --config __argGrypeConfig__ cache status -p "${provider}" \
    && name_backup_provider="grype-db-cache.tar.gz" \
    && grype-db --config __argGrypeConfig__ cache backup -v --path "${name_backup_provider}" -p "${provider}" \
    && aws s3 cp "${name_backup_provider}" "s3://fluidattacks.public.storage/sbom/grype-bd/${provider}" \
    && rm "${name_backup_provider}" \
    || return 1
}

main "${@}"
