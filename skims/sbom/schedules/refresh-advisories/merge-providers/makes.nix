{ inputs, makeScript, outputs, projectPath, ... }:
let
  vunnel = inputs.nixpkgs.vunnel.overrideAttrs {
    patches = [
      (projectPath
        "/skims/sbom/schedules/refresh-advisories/build-db/ossf_provider.patch")
    ];
  };
in {
  jobs."/skims/sbom/schedules/refresh-advisories/merge-providers" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "sbom-schedules-refresh-advisories-merge-providers";
    replace = {
      __argGrypeConfig__ = projectPath
        "/skims/sbom/schedules/refresh-advisories/build-db/grype-db.yaml";
      __argImproveDataQualityScript__ = projectPath
        "/skims/sbom/schedules/refresh-advisories/build-db/improve_data_quality.py";
    };
    searchPaths = {
      bin = [
        inputs.nixpkgs.zstd
        inputs.nixpkgs.python311
        vunnel
        outputs."/skims/sbom/schedules/refresh-advisories/build-db/grype-db"
      ];
      source = [ outputs."/common/utils/aws" ];
    };
  };
}
