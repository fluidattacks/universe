{ inputs, makeScript, outputs, projectPath, ... }:
let
  vunnel = inputs.nixpkgs.vunnel.overrideAttrs {
    patches = [
      (projectPath
        "/skims/sbom/schedules/refresh-advisories/build-db/ossf_provider.patch")
    ];
  };
in {
  jobs."/skims/sbom/schedules/refresh-advisories/list-providers" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "sbom-schedules-refresh-advisories-list-providers";

    searchPaths = {
      bin = [ vunnel ];
      source = [ outputs."/common/utils/aws" ];
    };
  };
}
