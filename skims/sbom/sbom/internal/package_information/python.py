from typing import cast

import requests

from sbom.internal.cache import (
    dual_cache,
)
from sbom.internal.collection.json import ParsedJSON


@dual_cache
def get_pypi_package(
    package_name: str,
    version: str | None = None,
) -> dict[str, ParsedJSON] | None:
    url = f"https://pypi.org/pypi/{package_name}{'/' + version if version else ''}/json"
    response = requests.get(url, timeout=30)
    if response.status_code == 200:
        return cast(dict[str, ParsedJSON], response.json())

    return None
