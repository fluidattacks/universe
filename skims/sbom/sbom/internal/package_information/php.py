from typing import cast

import requests

from sbom.internal.cache import dual_cache
from sbom.internal.collection.json import ParsedJSON
from sbom.internal.collection.types import IndexedDict, IndexedList

EMPTY_LIST: IndexedList[ParsedJSON] = IndexedList()
EMPTY_DICT: dict[str, ParsedJSON] = {}


@dual_cache
def get_composer_package(
    package_name: str,
    version: str | None = None,
) -> IndexedDict[str, ParsedJSON] | None:
    base_url = f"https://repo.packagist.org/p2/{package_name}.json"
    response = requests.get(base_url, timeout=30)
    if response.status_code != 200:
        return None

    response_data = cast(IndexedDict[str, ParsedJSON], response.json())
    packages_section = cast(dict[str, ParsedJSON], response_data.get("packages", EMPTY_DICT))
    package_versions = cast(IndexedList[ParsedJSON], packages_section.get(package_name, EMPTY_LIST))

    if version:
        for version_data in package_versions:
            if not isinstance(version_data, IndexedDict):
                continue
            ver = version_data.get("version")
            if isinstance(ver, str) and ver == version:
                return version_data
        return None

    if package_versions and isinstance(package_versions[0], IndexedDict):
        return package_versions[0]

    return None
