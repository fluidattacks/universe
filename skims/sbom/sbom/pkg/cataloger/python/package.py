import logging

from packageurl import (
    PackageURL,
)
from pydantic import (
    ValidationError,
)

from sbom import (
    advisories,
)
from sbom.file.location import (
    Location,
)
from sbom.file.resolver import (
    Resolver,
)
from sbom.internal.collection.json import ParsedJSON
from sbom.internal.collection.types import IndexedDict
from sbom.internal.package_information.python import (
    get_pypi_package,
)
from sbom.model.core import (
    Artifact,
    Digest,
    HealthMetadata,
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.common import (
    infer_algorithm,
)
from sbom.pkg.cataloger.python.model import (
    PythonPackage,
)
from sbom.pkg.cataloger.python.parse_wheel_egg_metadata import (
    ParsedData,
)
from sbom.utils.licenses.validation import (
    validate_licenses,
)
from sbom.utils.strings import format_exception

LOGGER = logging.getLogger(__name__)


def new_package_for_package(
    _resolver: Resolver,
    data: ParsedData,
    sources: Location,
) -> Package | None:
    name = data.python_package.name
    version = data.python_package.version

    if not name or not version:
        return None

    try:
        return Package(
            name=name,
            version=version,
            p_url=package_url(
                name,
                version,
                data.python_package,
            ),
            locations=[sources],
            language=Language.PYTHON,
            type=PackageType.PythonPkg,
            metadata=data.python_package,
            licenses=[],
        )
    except ValidationError as ex:
        LOGGER.warning(
            "Malformed package. Required fields are missing or data types are incorrect.",
            extra={
                "extra": {
                    "exception": format_exception(str(ex)),
                    "location": sources.path(),
                },
            },
        )
        return None


def package_url(name: str, version: str, package: PythonPackage | None) -> str:
    return PackageURL(
        type="pypi",
        namespace="",
        name=name,
        version=version,
        qualifiers=_purl_qualifiers_for_package(package),
        subpath="",
    ).to_string()


def _purl_qualifiers_for_package(
    package: PythonPackage | None,
) -> dict[str, str]:
    if not package:
        return {}
    if (
        hasattr(package, "direct_url_origin")
        and package.direct_url_origin
        and package.direct_url_origin.vcs
    ):
        url = package.direct_url_origin
        return {"vcs_url": f"{url.vcs}+{url.url}@{url.commit_id}"}
    return {}


def _set_health_metadata(
    package: Package,
    pypi_package: dict[str, ParsedJSON],
    current_package: dict[str, ParsedJSON] | None,
) -> None:
    info = pypi_package.get("info")
    if not isinstance(info, dict):
        return
    pypi_package_version = info.get("version")
    if not isinstance(pypi_package_version, str | None):
        return

    releases: ParsedJSON = pypi_package.get("releases")
    if not isinstance(releases, dict):
        releases = {}

    upload_time: ParsedJSON = releases.get(pypi_package_version) if pypi_package_version else []
    if not isinstance(upload_time, list):
        upload_time = []

    latest_version_created_at: str | None = None
    if upload_time:
        first_release = upload_time[0]
        if isinstance(first_release, IndexedDict):
            time_value = first_release.get("upload_time_iso_8601")
            if isinstance(time_value, str):
                latest_version_created_at = time_value

    package.health_metadata = HealthMetadata(
        latest_version=pypi_package_version,
        latest_version_created_at=latest_version_created_at,
        authors=_get_authors(pypi_package),
        artifact=_get_artifact(package, current_package) if current_package else None,
    )


def _get_artifact(
    package: Package,
    current_package: dict[str, ParsedJSON],
) -> Artifact | None:
    urls = current_package.get("urls")
    if not isinstance(urls, list):
        return None

    url: dict[str, ParsedJSON] | None = next(
        (
            x
            for x in urls
            if isinstance(x, dict)
            and "url" in x
            and isinstance(x["url"], str)
            and x["url"].endswith(".tar.gz")
        ),
        None,
    )

    digest_value: str | None = None
    if url is not None:
        digests = url.get("digests", {})
        if isinstance(digests, dict):
            val = digests.get("sha256")
            if isinstance(val, str):
                digest_value = val

    artifact_url: str = (
        url["url"]
        if url is not None and "url" in url and isinstance(url["url"], str)
        else f"https://pypi.org/pypi/{package.name}"
    )

    return Artifact(
        url=artifact_url,
        integrity=Digest(
            algorithm=infer_algorithm(digest_value),
            value=digest_value,
        ),
    )


def _get_authors(pypi_package: dict[str, ParsedJSON]) -> str | None:
    package_info = pypi_package["info"]
    if not isinstance(package_info, dict):
        return None
    if "author" in package_info:
        author: str | None = None
        package_author = package_info["author"]
        author_email = package_info.get("author_email")
        if isinstance(package_author, str) and package_author:
            author = package_author
        if not author and author_email and isinstance(author_email, str):
            author = author_email
        if isinstance(author_email, str) and author and author_email and author_email not in author:
            author = f"{author} <{author_email}>"
        return author
    return None


def _update_licenses(pypi_package: dict[str, ParsedJSON], package: Package) -> None:
    info = pypi_package.get("info")
    if not isinstance(info, dict):
        return
    licenses = info.get("license")
    if licenses and isinstance(licenses, str):
        package.licenses = validate_licenses([licenses])


def complete_package(package: Package) -> Package:
    pkg_advisories = advisories.get_package_advisories(package)
    if pkg_advisories:
        package.advisories = pkg_advisories

    pypi_package = get_pypi_package(package.name)
    if not pypi_package:
        return package

    current_package = get_pypi_package(package.name, package.version)

    _set_health_metadata(package, pypi_package, current_package)

    _update_licenses(pypi_package, package)

    return package
