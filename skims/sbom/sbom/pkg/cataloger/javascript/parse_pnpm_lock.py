import logging
import re
from copy import (
    deepcopy,
)
from typing import (
    Any,
    cast,
)

from pydantic import (
    ValidationError,
)

from sbom.artifact.relationship import (
    Relationship,
    RelationshipType,
)
from sbom.file.dependency_type import (
    DependencyType,
)
from sbom.file.location import (
    Location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.file.resolver import (
    Resolver,
)
from sbom.file.scope import (
    Scope,
)
from sbom.internal.collection.types import (
    IndexedDict,
)
from sbom.internal.collection.yaml import (
    parse_yaml_with_tree_sitter,
)
from sbom.model.core import (
    Digest,
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.generic.parser import (
    Environment,
)
from sbom.pkg.cataloger.javascript.model import (
    PnpmEntry,
)
from sbom.pkg.cataloger.javascript.package import (
    package_url,
)
from sbom.utils.strings import format_exception

LOGGER = logging.getLogger(__name__)

VERSION_PATTERN = re.compile(r"(\d+\.\d+\.\d+(-[0-9A-Za-z\.]+)?)")


def extract_package_name_from_key_dependency(item: str) -> str | None:
    # Regex pattern to extract the package name
    pattern = r"^@?[\w-]+/[\w-]+$"
    match = re.match(pattern, item)
    if match:
        return match.group(0)
    return None


def extract_version_from_value_dependency(item: str) -> str | None:
    # Regex pattern to extract the version number before any parentheses
    pattern = r"^(\d+\.\d+\.\d+)"
    match = re.match(pattern, item)
    if match:
        return match.group(1)
    return None


def _get_package(
    packages: list[Package],
    dep_name: str | None,
    dep_version: str | None,
) -> Package | None:
    return next(
        (x for x in packages if x.name == dep_name and x.version == dep_version),
        None,
    )


def _generate_relations_relationship(
    package_yaml: IndexedDict[str, IndexedDict[str, Any]],
    packages: list[Package],
) -> list[Relationship]:
    relationships: list[Relationship] = []
    for package_key, package_value in package_yaml["packages"].items():
        if match_ := re.search(r"/(@?[^@]+)@(\d+\.\d+\.\d+)", package_key):
            package_name = match_.groups()[0]
            package_version = match_.groups()[1]
            current_package = _get_package(
                packages,
                dep_name=package_name,
                dep_version=package_version,
            )
            dependencies: IndexedDict[str, str]
            if dependencies := package_value.get("dependencies"):
                for raw_dep_name, raw_dep_version in dependencies.items():
                    dep_name = extract_package_name_from_key_dependency(
                        raw_dep_name,
                    )
                    dep_version = extract_version_from_value_dependency(
                        raw_dep_version,
                    )
                    if (
                        dep := _get_package(
                            packages,
                            dep_name,
                            dep_version,
                        )
                        and current_package
                    ):
                        relationships.append(
                            Relationship(
                                from_=dep,
                                to_=current_package,
                                type=RelationshipType.DEPENDENCY_OF_RELATIONSHIP,
                            ),
                        )
    return relationships


def _get_package_metadata(
    package_value: dict[str, bool | dict[str, str]],
    *,
    is_dev: bool,
) -> PnpmEntry:
    resolution_value = package_value.get("resolution")
    integrity_value = (
        resolution_value.get("integrity") if isinstance(resolution_value, dict) else None
    )

    return PnpmEntry(
        is_dev=is_dev if isinstance(is_dev, bool) else False,
        integrity=Digest(
            algorithm="sha-512",
            value=integrity_value,
        ),
    )


def manage_coordinates(
    package_yaml: IndexedDict,
    package_key: str,
    package_name: str,
    direct_dependencies: list,
    base_location: Location,
) -> Location:
    current_location = deepcopy(base_location)
    if current_location.coordinates:
        position = package_yaml["packages"].get_key_position(package_key)
        current_location.coordinates.line = position.start.line
        is_transitive = package_name not in direct_dependencies
        current_location.dependency_type = (
            DependencyType.TRANSITIVE if is_transitive else DependencyType.DIRECT
        )
    return current_location


def process_package_string(package: str, spec: dict) -> tuple[str, str] | None:
    if package.startswith("github"):
        pkg_name = spec.get("name", "")
        pkg_version = spec.get("version", "")
    else:
        pkg_info = VERSION_PATTERN.split(package.strip("\"'"))
        if len(pkg_info) < 2:
            return None

        pkg_name = pkg_info[0].lstrip("/")[0:-1]
        pkg_version = pkg_info[1]

    return pkg_name, pkg_version


def parse_pnpm_lock(
    _: Resolver | None,
    __: Environment | None,
    reader: LocationReadCloser,
) -> tuple[list[Package], list[Relationship]]:
    package_yaml: IndexedDict = cast(
        IndexedDict,
        parse_yaml_with_tree_sitter(reader.read_closer.read()),
    )

    if not package_yaml:
        return [], []

    dependencies = list(package_yaml.get("dependencies", {}))
    dev_dependencies = list(package_yaml.get("devDependencies", {}))
    direct_dependencies = [*dev_dependencies, *dependencies]

    packages: list[Package] = []
    relationships: list[Relationship] = []
    for package_key, pkg_spec in package_yaml["packages"].items():
        if match_ := process_package_string(package_key, pkg_spec):
            package_name = match_[0]
            package_version = match_[1]

            if not package_name or not package_version:
                continue

            current_location: Location = manage_coordinates(
                package_yaml,
                package_key,
                package_name,
                direct_dependencies,
                reader.location,
            )
            is_dev = pkg_spec.get("dev")
            current_location.scope = Scope.DEV if is_dev else Scope.PROD
            try:
                packages.append(
                    Package(
                        name=package_name,
                        version=package_version,
                        locations=[current_location],
                        language=Language.JAVASCRIPT,
                        licenses=[],
                        type=PackageType.NpmPkg,
                        p_url=package_url(package_name, package_version),
                        metadata=_get_package_metadata(pkg_spec, is_dev=is_dev),
                    ),
                )
            except ValidationError as ex:
                LOGGER.warning(
                    "Malformed package. Required fields are missing or data types are incorrect.",
                    extra={
                        "extra": {
                            "exception": format_exception(str(ex)),
                            "location": current_location.path(),
                        },
                    },
                )
                continue

    relationships = _generate_relations_relationship(package_yaml, packages)

    return packages, relationships
