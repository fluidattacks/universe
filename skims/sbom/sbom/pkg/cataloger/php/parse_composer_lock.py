import logging
from copy import (
    deepcopy,
)
from typing import (
    cast,
)

from sbom.artifact.relationship import (
    Relationship,
    RelationshipType,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.file.resolver import (
    Resolver,
)
from sbom.internal.collection.json import (
    ParsedJSON,
    parse_json_with_tree_sitter,
)
from sbom.internal.collection.types import (
    IndexedDict,
)
from sbom.model.core import (
    Package,
)
from sbom.pkg.cataloger.generic.parser import (
    Environment,
)
from sbom.pkg.cataloger.php.model import (
    PhpComposerInstalledEntry,
    PhpComposerLockEntry,
)
from sbom.pkg.cataloger.php.package import (
    new_package_from_composer,
)

LOGGER = logging.getLogger(__name__)
EMPTY_DICT: IndexedDict[str, ParsedJSON] = IndexedDict()


def parse_composer_lock(
    _: Resolver | None,
    __: Environment | None,
    reader: LocationReadCloser,
) -> tuple[list[Package], list[Relationship]]:
    package_json: IndexedDict[str, ParsedJSON] = cast(
        IndexedDict[str, ParsedJSON],
        parse_json_with_tree_sitter(reader.read_closer.read()),
    )
    packages: list[Package] = []
    relationships: list[Relationship] = []
    pkgs: IndexedDict[str, ParsedJSON] = cast(
        IndexedDict[str, ParsedJSON],
        package_json.get("packages", EMPTY_DICT),
    )
    pkgs_dev: IndexedDict[str, ParsedJSON] = cast(
        IndexedDict[str, ParsedJSON],
        package_json.get("packages-dev", EMPTY_DICT),
    )
    for is_dev, package in [
        *[(False, x) for x in pkgs],
        *[(True, x) for x in pkgs_dev],
    ]:
        if not isinstance(package, IndexedDict):
            continue
        new_location = deepcopy(reader.location)

        if pkg := new_package_from_composer(package, new_location, is_dev=is_dev):
            packages.append(pkg)

    for parsed_pkg in packages:
        if not isinstance(parsed_pkg.metadata, PhpComposerInstalledEntry | PhpComposerLockEntry):
            continue
        for dep_name in parsed_pkg.metadata.require or []:
            package_dep = next(
                (x for x in packages if x.name == dep_name),
                None,
            )
            if package_dep:
                relationships.append(
                    Relationship(
                        from_=parsed_pkg,
                        to_=package_dep,
                        type=RelationshipType.DEPENDENCY_OF_RELATIONSHIP,
                    ),
                )
    return packages, relationships
