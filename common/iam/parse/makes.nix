{ inputs, makeTemplate, outputs, projectPath, ... }: {
  jobs."/common/iam/parse" = makeTemplate {
    replace = {
      __argParser__ = projectPath "/common/iam/parse/src/__init__.py";
      __argSopsData__ = projectPath "/common/iam/data.yaml";
    };
    searchPaths.bin =
      [ inputs.nixpkgs.jq inputs.nixpkgs.python39 inputs.nixpkgs.sops ];
    template = ./template.sh;
    name = "okta-parse";
  };
}
