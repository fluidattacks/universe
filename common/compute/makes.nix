# https://github.com/fluidattacks/makes
{ outputs, inputs, makeSearchPaths, ... }:
let searchPaths = makeSearchPaths { bin = [ inputs.nixpkgs.git ]; };
in {
  imports = [ ./arch/makes.nix ./schedule/makes.nix ];
  deployTerraform = {
    modules = {
      commonCompute = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonCompute"
          outputs."/secretsForTerraformFromEnv/commonCompute"
          outputs."/common/compute/schedule/parse-terraform"
        ];
        src = "/common/compute/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonCompute = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCompute"
          outputs."/secretsForTerraformFromEnv/commonCompute"
          outputs."/common/compute/schedule/parse-terraform"
        ];
        src = "/common/compute/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonCompute = {
      vars = [
        "CLOUDFLARE_WARP_AUTH_CLIENT_ID"
        "CLOUDFLARE_WARP_AUTH_SECRET_KEY"
        "GOOGLE_CHAT_WEBHOOK_SCHEDULES"
      ];
      manifest = "/common/secrets/dev.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    commonCompute = {
      cloudflareWarpAuthClientId = "CLOUDFLARE_WARP_AUTH_CLIENT_ID";
      cloudflareWarpAuthSecretKey = "CLOUDFLARE_WARP_AUTH_SECRET_KEY";
      googleChatWebhookSchedules = "GOOGLE_CHAT_WEBHOOK_SCHEDULES";
    };
  };
  testTerraform = {
    modules = {
      commonCompute = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCompute"
          outputs."/secretsForTerraformFromEnv/commonCompute"
          outputs."/common/compute/schedule/parse-terraform"
        ];
        src = "/common/compute/infra";
        version = "1.0";
      };
    };
  };
}
