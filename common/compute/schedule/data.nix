# Schedule expressions:
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html
# schedule.meta.requiredBy format is DD-MM-YYYY
{ } // (import ./data_common.nix) // (import ./data_integrates.nix)
// (import ./data_observes.nix) // (import ./data_skims.nix)
// (import ./data_sorts.nix)
