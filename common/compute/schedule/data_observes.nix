let
  observes_tags = name: {
    "Name" = name;
    "fluidattacks:line" = "cost";
    "fluidattacks:comp" = "etl";
  };
  common_env_vars = [ "CACHIX_AUTH_TOKEN" ];
  observes_scheduled_job =
    { name, command, scheduleExpression, attempts, meta, timeout, size, }: {
      inherit attempts meta scheduleExpression timeout size;
      awsRole = "prod_observes";
      command = [ "m" "gitlab:fluidattacks/universe@trunk" ] ++ command;
      enable = true;
      environment = common_env_vars;
      parallel = 1;
      tags = observes_tags name;
    };
in {
  observes_etl_code_compute_bills = observes_scheduled_job {
    name = "observes_etl_code_compute_bills";
    command = [ "/observes/etl/code/jobs/compute-bills" ];
    scheduleExpression = "cron(50 23 ? * 3-7 *)"; # workdays at 18:50 UTC-5
    timeout = 12 * 3600;
    attempts = 2;
    size = "admin_etl";
    meta = {
      description = ''
        Calculates Advanced plan billed authors for groups.
      '';
      requiredBy = [
        "Integrates to show authors consumption"
        "Integrates to bill clients"
      ];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_code_upload_per_repo_snowflake = observes_scheduled_job {
    name = "observes_etl_code_upload_per_repo_snowflake";
    command = [ "/observes/etl/code/jobs/upload-roots-on-aws" ];
    scheduleExpression = "cron(00 23 ? * * *)"; # Every day at 18:00 UTC-5
    timeout = 10 * 3600;
    attempts = 2;
    size = "admin_etl";
    meta = {
      description = ''
        Triggers code ETL jobs for each repo.
        The ETL fetch commits data from clients repositories.
      '';
      requiredBy = [ "observes_etl_code_compute_bills" ];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_code_upload_snowflake = observes_scheduled_job {
    name = "observes_etl_code_upload_snowflake";
    command = [ "/observes/etl/code/jobs/upload-groups-on-aws-snowflake" ];
    scheduleExpression = "cron(00 22 ? * * *)"; # Every day at 17:00 UTC-5
    timeout = 5 * 3600;
    attempts = 2;
    size = "admin_etl";
    meta = {
      description = ''
        Triggers code ETL jobs at a group level.
        The ETL fetch commits data from clients repositories.
      '';
      requiredBy = [ "observes_etl_code_compute_bills" ];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_announcekit = observes_scheduled_job {
    name = "observes_etl_announcekit";
    command = [ "/observes/etl/announcekit/bin" "run" ];
    scheduleExpression = "cron(0 17 ? * 2-6 *)"; # workdays at 12pm UTC-5
    timeout = 4 * 3600;
    attempts = 2;
    size = "marketing_etl";
    meta = {
      description = ''
        Extract data from Announcekit
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        integrates announces.
      ''];
    };
  } // {
    enable = false;
  };
  observes_etl_batch = observes_scheduled_job {
    name = "observes_etl_batch";
    command = [ "/observes/etl/batch" ];
    scheduleExpression = "cron(0 17 ? * 2-6 *)"; # workdays at 12pm UTC-5
    timeout = 4 * 3600;
    attempts = 1;
    size = "observes";
    meta = {
      description = ''
        Extract data from Batch
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        batch jobs executions.
      ''];
    };
  };
  observes_etl_bugsnag = observes_scheduled_job {
    name = "observes_etl_bugsnag";
    command = [ "/observes/etl/bugsnag/snowflake" ];
    scheduleExpression = "cron(0 17 ? * 2-6 *)"; # workdays at 12pm UTC-5
    timeout = 12 * 3600;
    attempts = 2;
    size = "observes";
    meta = {
      description = ''
        Extract data from bugsnag
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        products errors.
      ''];
    };
  };
  observes_etl_ce = observes_scheduled_job {
    name = "observes_etl_ce";
    command = [ "/observes/etl/ce" ];
    scheduleExpression = "cron(0 0 5 * ? *)"; # The 5th of every month
    timeout = 1 * 3600;
    attempts = 1;
    size = "observes";
    meta = {
      description = ''
        Extract data from Cost Explorer
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to be able to measure
        aws costs detect anomalies.
      ''];
    };
  };
  observes_etl_checkly = observes_scheduled_job {
    name = "observes_etl_checkly";
    command = [ "/observes/etl/checkly" ];
    scheduleExpression = "cron(0 17 ? * 2-6 *)"; # workdays at 12pm UTC-5
    timeout = 8 * 3600;
    attempts = 2;
    size = "observes";
    meta = {
      description = ''
        Extract data from Checkly
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        web stability to sites like
        integrates and the airs.
      ''];
    };
  };
  observes_etl_cloudwatch = observes_scheduled_job {
    name = "observes_etl_cloudwatch";
    command = [ "/observes/etl/cloudwatch/snowflake" ];
    scheduleExpression = "cron(0 3 ? * 7 *)"; # every FRI at 10pm UTC-5
    timeout = 1 * 3600;
    attempts = 1;
    size = "observes";
    meta = {
      description = ''
        Extract data from CloudWatch
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to be able to measure
        skims methods and detect anomalies.
      ''];
    };
  };
  observes_etl_delighted = observes_scheduled_job {
    name = "observes_etl_delighted";
    command = [ "/observes/etl/delighted/snowflake" ];
    scheduleExpression = "cron(0 17 ? * 2-6 *)"; # workdays at 12pm UTC-5
    timeout = 1 * 3600;
    attempts = 2;
    size = "admin_etl";
    meta = {
      description = ''
        Extract data from delighted
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        products errors.
      ''];
    };
  };
  observes_etl_dynamo = observes_scheduled_job {
    name = "observes_etl_dynamo";
    command =
      [ "/observes/etl/dynamo/bin" "remote" "full-pipeline" "--use-snowflake" ];
    scheduleExpression = "cron(0 12 ? * * *)"; # every day at 7am UTC-5
    timeout = 1 * 3600;
    attempts = 1;
    size = "observes";
    meta = {
      description = ''
        Triggers the mirror pipeline jobs, that
        mirrors data from dynamo DB
        into Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to perform
        data analytics.
      ''];
    };
  };
  observes_etl_dynamo_schema = observes_scheduled_job {
    name = "observes_etl_dynamo_schema";
    command = [ "/observes/etl/dynamo/bin" "local" "determine-schema" ];
    scheduleExpression = "cron(0 3 ? * 7 *)"; # every FRI at 10pm UTC-5
    timeout = 1 * 3600;
    attempts = 1;
    size = "observes";
    meta = {
      description = ''
        Triggers the job that
        determines the dynamo
        DB schema for Snowflake.
      '';
      requiredBy = [ "observes_etl_dynamo" ];
    };
  };
  observes_etl_flow = observes_scheduled_job {
    name = "observes_etl_flow";
    command = [ "/observes/etl/flow/snowflake" ];
    scheduleExpression = "cron(0 10 ? * 2-6 *)";
    timeout = 1 * 3600;
    attempts = 1;
    size = "research_etl";
    meta = {
      description = ''
        Extracts data from Flow
        and saves it to Snowflake
      '';
      requiredBy = [''
        Business to be able to measure
        developer impact and detect issues earlier.
      ''];
    };
  };
  observes_etl_gitlab_ephemeral = observes_scheduled_job {
    name = "observes_etl_gitlab_ephemeral";
    command = [ "/observes/etl/gitlab/jobs/universe/ephemeral" ];
    scheduleExpression = "cron(0 17 ? * 2-6 *)"; # workdays at 12pm UTC-5
    timeout = 3 * 3600;
    attempts = 2;
    size = "research_etl";
    meta = {
      description = ''
        Extract data from GitLab
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        product and development performance.
      ''];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_gitlab = observes_scheduled_job {
    name = "observes_etl_gitlab";
    command = [ "/observes/etl/gitlab/jobs/universe" ];
    scheduleExpression =
      "cron(0 11-23 ? * 2-6 *)"; # workdays every hour from 6am-6pm UTC-5
    timeout = 1 * 3600;
    attempts = 2;
    size = "research_etl";
    meta = {
      description = ''
        Extract data from GitLab
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        product and development performance.
      ''];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_gitlab_night = observes_scheduled_job {
    name = "observes_etl_gitlab";
    command = [ "/observes/etl/gitlab/jobs/universe" ];
    scheduleExpression =
      "cron(0 0-5 ? * 2-6 *)"; # workdays every hour from 7pm-12am UTC-5
    timeout = 1 * 3600;
    attempts = 2;
    size = "research_etl";
    meta = {
      description = ''
        Extract data from GitLab
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        product and development performance.
      ''];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_gitlab_fail_reasons_universe = observes_scheduled_job {
    name = "observes_etl_gitlab_fail_reasons_universe";
    command = [ "/observes/etl/gitlab/bin" "fail-reason" "--repo" "universe" ];
    scheduleExpression =
      "cron(0 11,17,23 ? * 2-6 *)"; # workdays at 6am, 12m and 6pm UTC-5
    timeout = 6 * 3600;
    attempts = 2;
    size = "research_etl";
    meta = {
      description = ''
        Extract failure reasons from GitLab
        job logs and save it on Snowflake.
      '';
      requiredBy = [''
        Development to track metrics about
        CI and jobs common failures.
      ''];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_gitlab_dora = observes_scheduled_job {
    name = "observes_etl_gitlab_dora";
    command = [ "/observes/etl/gitlab-dora" ];
    scheduleExpression =
      "cron(0 5 ? * 1-6 *)"; # monday to saturday at 12 am UTC-5
    timeout = 1 * 3600;
    attempts = 1;
    size = "research_etl";
    meta = {
      description = ''
        Extracts data from Fluid Attacks'
        Universe repository, computes
        calculations, and uploads
        DORA metrics to airs' bucket in S3.
      '';
      requiredBy = [''
        Business to be able to display
        DORA metrics in the repository
        main screen.
      ''];
    };
  } // {
    environment = common_env_vars;
  };
  observes_etl_google_sheets = observes_scheduled_job {
    name = "observes_etl_google_sheets";
    command = [ "/observes/etl/google-sheets/bin" "emit" ];
    scheduleExpression = "cron(0 10 ? * 2-6 *)"; # workdays at 5am UTC-5
    timeout = 4 * 3600;
    attempts = 2;
    size = "observes";
    meta = {
      description = ''
        Extract data from Google sheets
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics about
        relevant google sheets data.
      ''];
    };
  } // {
    enable = false;
  };
  observes_etl_mixpanel = observes_scheduled_job {
    name = "observes_etl_mixpanel";
    command = [ "/observes/etl/mixpanel/snowflake" ];
    scheduleExpression = "cron(0 5 * * ? *)"; # everyday at 12am UTC-5
    timeout = 24 * 3600;
    attempts = 2;
    size = "admin_etl";
    meta = {
      description = ''
        Extract data from Mixpanel
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [ "Business to track ARM events" ];
    };
  } // {
    size = "observes_large";
  };
  observes_etl_timedoctor = observes_scheduled_job {
    name = "observes_etl_timedoctor";
    command = [ "/observes/etl/timedoctor/bin" ];
    scheduleExpression = "cron(0 9 ? * 2-6 *)"; # workdays days at 4am UTC-5
    timeout = 6 * 3600;
    attempts = 2;
    size = "observes";
    meta = {
      description = ''
        Extract entire Timedoctor
        history from s3
        and update Snowflake
        for later analytics usage.
      '';
      requiredBy = [''
        Business to track metrics
        about employee working time
        and performance.
      ''];
    };
  };
  observes_etl_timedoctor_backup = observes_scheduled_job {
    name = "observes_etl_timedoctor_backup";
    command = [ "/observes/etl/timedoctor/backup" ];
    scheduleExpression =
      "cron(0 4 1 * ? *)"; # last day of the month at 11pm UTC-5
    timeout = 10 * 3600;
    attempts = 4;
    size = "observes";
    meta = {
      description = ''
        Extract Timedoctor history
        from the last month
        and add it to S3.
      '';
      requiredBy = [''
        observes_etl_timedoctor
        as it uses the provided s3 history
        for populating Snowflake.
      ''];
    };
  };
  observes_etl_zoho_crm_fluid_snowflake = observes_scheduled_job {
    name = "observes_etl_zoho_crm_snowflake";
    command = [ "/observes/etl/zoho-crm/snowflake" ];
    scheduleExpression = "cron(0 6,22 ? * 2-6 *)"; # workdays at 1am,5pm UTC-5
    timeout = 24 * 3600;
    attempts = 2;
    size = "sales_etl";
    meta = {
      description = ''
        Extract data from Zoho CRM
        to feed Snowflake
        for later analytics processing.
      '';
      requiredBy = [''
        Business to track metrics
        about opportunities, contracts
        sales, etc.
      ''];
    };
  };
  observes_etl_zoho_crm_leads_fluid = observes_scheduled_job {
    name = "observes_etl_zoho_crm_leads";
    command = [ "/observes/etl/zoho-crm-leads/bin" ];
    scheduleExpression = "cron(0 3 ? * 2-6 *)"; # workdays at 10pm UTC-5
    timeout = 12 * 3600;
    attempts = 2;
    size = "sales_etl";
    meta = {
      description = ''
        Extract data from s3 to Zoho CRM
        for add or update leads field.
      '';
      requiredBy = [''
        Business to track metrics
        about leads
      ''];
    };
  };
}
