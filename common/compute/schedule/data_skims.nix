{
  skims_benchmark_owasp = {
    attempts = 3;
    awsRole = "prod_observes";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/skims/benchmark/owasp/upload"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Skims is executed with the test data provided by the owasp
        benchmark, then the results of the execution are
        compared with the results expected by the owasp benchmark.
        A json containing the results is uploaded to the DB.
      '';
      requiredBy = [ "OWASP Foundation scanners list." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 10 ? * 6 *)";
    size = "skims";
    tags = {
      "Name" = "skims_benchmark_owasp";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "skims";
    };
    timeout = 1800;
  };
  skims_update_sca_table = {
    attempts = 3;
    awsRole = "prod_skims";
    command =
      [ "m" "gitlab:fluidattacks/universe@trunk" "/skims/sca/scheduler" ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Check sca vulnerabilities
        and update the vulnerability table in dynamo.
      '';
      requiredBy =
        [ "Machine executions that query about sca vulnerabilities." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 10 * * ? *)";
    size = "skims";
    tags = {
      "Name" = "skims_update_sca_table";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "skims";
    };
    timeout = 86400;
  };
  skims_prioritize_automation_candidates = {
    attempts = 3;
    awsRole = "prod_skims";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/skims/skims-ai"
      "prioritizes"
      "--top=-1"
      "--debug=false"
    ];
    enable = true;
    environment = [ ];
    meta = {
      description = ''
        Update the automation candidates on the Google Sheets document.
      '';
      requiredBy = [ "Developers implementing new reachability methods." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 6 ? * 2 *)";
    size = "skims";
    tags = {
      "Name" = "skims_prioritizes_automation_candidates";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "skims";
    };
    timeout = 1800;
  };
}
