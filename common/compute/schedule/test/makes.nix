{ inputs, makePythonEnvironment, makeScript, outputs, projectPath, toFileJson
, ... }:
let
  testPythonEnv = makePythonEnvironment {
    pythonProjectDir = ./.;
    pythonVersion = "3.11";
  };
in {
  jobs."/common/compute/schedule/test" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-compute-schedule-test";
    replace = {
      __argData__ = toFileJson "data.json" (import ../data.nix);
      __argSrc__ = ./src/__init__.py;
    };
    searchPaths = {
      bin = [ inputs.nixpkgs.python311 ];
      source = [ testPythonEnv ];
    };
  };
}
