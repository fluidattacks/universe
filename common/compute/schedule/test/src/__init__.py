import json
import os
import sys
from typing import Any

from aws_cron_expression_validator.validator import (
    AWSCronExpressionValidator,
)
from jsonschema import (
    validate,
)


def error(msg: str) -> None:
    print("[ERROR]", msg)


def test_data_schema(*, data: dict[str, Any]) -> None:
    schema: dict[str, Any] = {
        "additionalProperties": False,
        "patternProperties": {
            "^[a-z0-9_]+$": {
                "additionalProperties": False,
                "properties": {
                    "arch": {
                        "enum": [
                            "amd64",
                            "arm64",
                        ],
                        "type": "string",
                    },
                    "attempts": {"minimum": 1, "type": "integer"},
                    "awsRole": {"type": "string"},
                    "command": {
                        "items": {"type": "string"},
                        "minItems": 1,
                        "type": "array",
                    },
                    "enable": {"type": "boolean"},
                    "environment": {
                        "items": {"type": "string"},
                        "type": "array",
                    },
                    "meta": {
                        "additionalProperties": False,
                        "properties": {
                            "description": {"minLength": 25, "type": "string"},
                            "requiredBy": {
                                "items": {"minLength": 10, "type": "string"},
                                "minItems": 1,
                                "type": "array",
                            },
                        },
                        "required": [
                            "description",
                            "requiredBy",
                        ],
                        "type": "object",
                    },
                    "parallel": {"minimum": 1, "type": "integer"},
                    "scheduleExpression": {"type": "string"},
                    "size": {"type": "string"},
                    "tags": {"type": "object"},
                    "timeout": {"minimum": 60, "type": "integer"},
                },
                "required": [
                    "attempts",
                    "awsRole",
                    "command",
                    "enable",
                    "environment",
                    "meta",
                    "parallel",
                    "scheduleExpression",
                    "size",
                    "tags",
                    "timeout",
                ],
                "type": "object",
            }
        },
        "propertyNames": {"pattern": "^[a-z0-9_]+$"},
        "type": "object",
    }
    validate(instance=data, schema=schema)


def test_schedule_expressions(*, data: dict[str, Any]) -> bool:
    success: bool = True
    for name, values in data.items():
        expression: str = values["scheduleExpression"][5:-1]
        try:
            AWSCronExpressionValidator.validate(expression)
        except ValueError:
            error(
                f"{name}.scheduleExpression is wrong:"
                f" '{expression}'."
                " Please review and fix it."
            )
            success = False
    return success


def main() -> None:
    data: dict[str, Any] = json.loads(os.environ["DATA"])
    test_data_schema(data=data)

    success = test_schedule_expressions(data=data)

    sys.exit(0 if success else 1)


main()
