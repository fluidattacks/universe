{
  common_clean_dev_ephemerals = {
    attempts = 3;
    awsRole = "prod_common";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/common/dev/clean/ephemerals"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Destroy ephemerals by destroying resources in dev namespace.
      '';
      requiredBy = [ "CI in order to free unused resources each day." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 5 * * ? *)";
    size = "common";
    tags = {
      "Name" = "common_clean_dev_environments";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "common";
    };
    timeout = 7200;
  };
  common_clean_dev_branches = {
    attempts = 3;
    awsRole = "prod_common";
    command =
      [ "m" "gitlab:fluidattacks/universe@trunk" "/common/dev/clean/branches" ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" "CI_PROJECT_ID" ];
    meta = {
      description = ''
        Delete all repository branches except those with open merge request.
      '';
      requiredBy = [ "CI in order to free unused resources each day." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 7 * * ? *)";
    size = "common";
    tags = {
      "Name" = "common_clean_dev_branches";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "common";
    };
    timeout = 7200;
  };
  common_clean_terraform_locks = {
    attempts = 3;
    awsRole = "prod_common";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/common/dev/clean/terraform_locks"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = "Remove locks of terraform state file";
      requiredBy = [ "CI to auto unlock deployment jobs" ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 11-23/2 ? * 2-6 *)";
    size = "common";
    tags = {
      "Name" = "common_clean_terraform_locks";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "common";
    };
    timeout = 1800;
  };
  common_compute_test_environment_default = {
    attempts = 1;
    awsRole = "prod_common";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/common/compute/test/environment"
      "default"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = "Test batch default environments";
      requiredBy = [ "Most products to properly run batch jobs" ];
    };
    parallel = 25;
    scheduleExpression = "cron(0 0 1 1 ? 1995)";
    size = "test";
    tags = {
      "Name" = "common_compute_test_environment_default";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "common";
    };
    timeout = 86400;
  };
  common_compute_test_environment_warp = {
    attempts = 1;
    awsRole = "prod_common";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/common/compute/test/environment"
      "warp"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = "Test batch warp environments";
      requiredBy = [ "Integrates to properly clone repositories behind warp" ];
    };
    parallel = 25;
    scheduleExpression = "cron(0 0 1 1 ? 1995)";
    size = "test";
    tags = {
      "Name" = "common_compute_test_environment_warp";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "common";
    };
    timeout = 86400;
  };
  common_compute_clean_cloudflare_devices = {
    attempts = 1;
    awsRole = "prod_common";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/common/dev/clean/cloudflare_devices"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = "Revoke enrolled devices from ephemerals environments";
      requiredBy = [ "Cloudflare to free resources from enrolled devices" ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 0 1 1 ? 1995)";
    size = "common";
    tags = {
      "Name" = "common_compute_clean_cloudflare_devices";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "common";
    };
    timeout = 432000;
  };
}
