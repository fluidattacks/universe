variable "schedules" {
  type = string
}
variable "sizes" {
  type = string
}

locals {
  schedules = jsondecode(var.schedules)
  sizes     = jsondecode(var.sizes)
}

resource "aws_batch_job_definition" "schedule" {
  for_each = local.schedules

  name = each.key
  type = "container"

  container_properties = jsonencode(
    {
      image      = "ghcr.io/fluidattacks/makes:24.12"
      command    = each.value.command
      jobRoleArn = "arn:aws:iam::${data.aws_caller_identity.main.account_id}:role/${each.value.awsRole}"

      # WARP requires capabilities NET_ADMIN, but that granularity
      # is not supported by AWS Batch containers, so privileged flag is used
      privileged = true

      resourceRequirements = [
        {
          type  = "VCPU"
          value = tostring(local.sizes[each.value.size].cpu)
        },
        {
          type  = "MEMORY"
          value = tostring(local.sizes[each.value.size].memory)
        },
      ]

      # Mount WARP
      volumes = concat(
        [
          {
            name = "cloudflare-warp-daemon"
            host = { sourcePath = "/run/cloudflare-warp" }
          },
          {
            name = "cloudflare-warp-config"
            host = { sourcePath = "/var/lib/cloudflare-warp" }
          },
        ],
        each.key == "common_compute_test_environment_warp" ? [
          {
            name = "cloudflare-warp-cli"
            host = { sourcePath = "/usr/bin/warp-cli" }
          },
        ] : []
      )
      mountPoints = concat(
        [
          {
            sourceVolume  = "cloudflare-warp-daemon"
            containerPath = "/run/cloudflare-warp"
            readOnly      = false
          },
          {
            sourceVolume  = "cloudflare-warp-config"
            containerPath = "/var/lib/cloudflare-warp"
            readOnly      = false
          },
        ],
        each.key == "common_compute_test_environment_warp" ? [
          {
            sourceVolume  = "cloudflare-warp-cli"
            containerPath = "/usr/local/bin/warp-cli"
            readOnly      = false
          },
        ] : []
      )

      environment = concat(
        each.value.environment,
        [
          {
            Name  = "CI"
            Value = "true"
          },
          {
            Name  = "MAKES_AWS_BATCH_COMPAT"
            Value = "true"
          },
        ]
      )
    }
  )

  retry_strategy {
    attempts = each.value.attempts
    evaluate_on_exit {
      action       = "RETRY"
      on_exit_code = 1
    }
    evaluate_on_exit {
      action    = "EXIT"
      on_reason = "CannotInspectContainerError:*"
    }
    evaluate_on_exit {
      action       = "RETRY"
      on_exit_code = 128
    }
  }

  timeout {
    attempt_duration_seconds = each.value.timeout
  }

  tags = merge(
    each.value.tags,
    {
      "fluidattacks:line" = lookup(each.value.tags, "fluidattacks:line", "cost"),
      "fluidattacks:comp" = lookup(each.value.tags, "fluidattacks:comp", "common")
    }
  )
  propagate_tags = true
}


resource "aws_cloudwatch_event_rule" "main" {
  for_each = local.schedules

  name                = "schedule_${each.key}"
  is_enabled          = each.value.enable
  schedule_expression = each.value.scheduleExpression

  tags = merge(
    each.value.tags,
    {
      "fluidattacks:line" = lookup(each.value.tags, "fluidattacks:line", "cost"),
      "fluidattacks:comp" = lookup(each.value.tags, "fluidattacks:comp", "common")
    }
  )
}

resource "aws_cloudwatch_event_target" "main" {
  for_each = local.schedules

  target_id = each.key
  rule      = aws_cloudwatch_event_rule.main[each.key].name
  arn       = aws_batch_job_queue.main[local.sizes[each.value.size].queue].arn
  role_arn  = data.aws_iam_role.main["prod_common"].arn

  batch_target {
    job_name       = each.key
    job_definition = aws_batch_job_definition.schedule[each.key].arn
    array_size     = each.value.parallel > 1 ? each.value.parallel : null
  }
}
