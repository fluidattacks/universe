variable "cloudflareWarpAuthClientId" {
  type = string
}
variable "cloudflareWarpAuthSecretKey" {
  type = string
}
variable "googleChatWebhookSchedules" {
  type = string
}

locals {
  machine_sizes = {
    test = {
      max_vcpus = 10000
      instances = [
        "r6gd.medium",
        "r7gd.medium",
        "x2gd.medium",
      ]
      ami = "ami-0b8ffcd097be79836"
    }
    small = {
      max_vcpus = 10000
      instances = [
        "r6gd.medium",
        "r7gd.medium",
        "x2gd.medium",
      ]
      ami = "ami-0b8ffcd097be79836"
    }
    large = {
      max_vcpus = 10000
      instances = [
        "r6gd.large",
        "r7gd.large",
        "x2gd.large",
      ]
      ami = "ami-08b476084597865a0"
    }
  }
  config = {
    common = {
      product = "common"
      line    = "cost"
      subnets = [for subnet in data.aws_subnet.main : subnet.id]
      type    = "SPOT"
      init    = "default"
    }
    integrates = {
      product = "integrates"
      line    = "cost"
      subnets = [for subnet in data.aws_subnet.main : subnet.id]
      type    = "SPOT"
      init    = "default"
    }
    skims = {
      product = "skims"
      line    = "cost"
      subnets = [for subnet in data.aws_subnet.main : subnet.id]
      type    = "SPOT"
      init    = "default"
    }
    sorts = {
      product = "sorts"
      line    = "cost"
      subnets = [for subnet in data.aws_subnet.main : subnet.id]
      type    = "SPOT"
      init    = "default"
    }
    observes = {
      product = "etl"
      line    = "cost"
      subnets = [for subnet in data.aws_subnet.main : subnet.id]
      type    = "SPOT"
      init    = "default"
    }
    observes_on_demand = {
      product = "etl"
      line    = "cost"
      subnets = [for subnet in data.aws_subnet.main : subnet.id]
      type    = "EC2"
      init    = "default"
    }
  }
  environments = {
    common = merge(
      local.machine_sizes.small,
      local.config.common,
    )
    common_large = merge(
      local.machine_sizes.large,
      local.config.common,
    )
    integrates = merge(
      local.machine_sizes.small,
      local.config.integrates,
    )
    integrates_large = merge(
      local.machine_sizes.large,
      local.config.integrates,
    )
    observes = merge(
      local.machine_sizes.small,
      local.config.observes_on_demand,
    )
    research_etl = merge(
      local.machine_sizes.small,
      local.config.observes_on_demand,
      { line = "research" },
    )
    marketing_etl = merge(
      local.machine_sizes.small,
      local.config.observes_on_demand,
      { line = "marketing" },
    )
    admin_etl = merge(
      local.machine_sizes.small,
      local.config.observes_on_demand,
      { line = "admin" },
    )
    sales_etl = merge(
      local.machine_sizes.small,
      local.config.observes_on_demand,
      { line = "sales" },
    )
    observes_code_etl_on_demand = merge(
      local.machine_sizes.small,
      local.config.observes_on_demand,
      {
        max_vcpus = 150 # set for concurrency control
      },
    )
    observes_code_etl_new = merge(
      local.machine_sizes.small,
      local.config.observes,
      {
        max_vcpus = 150
        # This limits the number of connections the code_etl will consume
      },
      { line = "admin" },
    )
    observes_code_etl_on_demand_new = merge(
      local.machine_sizes.small,
      local.config.observes_on_demand,
      {
        max_vcpus = 150 # set for concurrency control
      },
      { line = "admin" },
    )
    observes_large = merge(
      local.machine_sizes.large,
      local.config.observes_on_demand,
    )
    skims = merge(
      local.machine_sizes.small,
      local.config.skims,
    )
    skims_large = merge(
      local.machine_sizes.large,
      local.config.skims,
    )
    sorts = merge(
      local.machine_sizes.small,
      local.config.sorts,
    )
    sorts_large = merge(
      local.machine_sizes.large,
      local.config.sorts,
    )
    test = merge(
      local.config.common,
      local.machine_sizes.test,
      { init = "test" },
    )
  }
}

resource "aws_security_group" "main" {
  name   = "compute"
  vpc_id = data.aws_vpc.main.id

  ingress = []
  // NOFLUID We need to allow wide range of port numbers for repository cloning.
  egress {
    // NOFLUID We need to allow wide range of port numbers for repository cloning.
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  # Cloudflare WARP
  # https://developers.cloudflare.com/cloudflare-one/connections/connect-devices/warp/deployment/firewall/#warp-udp-ports
  egress {
    from_port   = 2408
    to_port     = 2408
    protocol    = "udp"
    cidr_blocks = ["162.159.193.0/24"]
  }

  tags = {
    "Name"              = "compute"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "NOFLUID"           = "f024_allow_cloning_ports"
  }
}

resource "aws_iam_instance_profile" "main" {
  name = "ecsAndSsmInstanceProfileForBatch"
  role = aws_iam_role.main.name
}

resource "aws_iam_role" "main" {
  name = "ecsAndSsmRoleForBatch"
  path = "/"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM",
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role",
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  ]
  tags = {
    "Name"              = "main_aws_iam_role"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_launch_template" "main" {
  for_each = local.environments

  name                                 = "compute-${each.key}"
  key_name                             = "gitlab"
  instance_initiated_shutdown_behavior = "terminate"

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      encrypted             = true
      delete_on_termination = true
      volume_size           = 30
      volume_type           = "gp3"
    }
  }

  block_device_mappings {
    device_name  = "/dev/xvdcz"
    virtual_name = "ephemeral0"
  }

  tag_specifications {
    resource_type = "volume"

    tags = {
      "Name"              = "compute-${each.key}"
      "fluidattacks:line" = "cost"
      "fluidattacks:comp" = "common"
    }
  }

  metadata_options {
    http_endpoint = "enabled"
    http_tokens   = "required"
  }

  tags = {
    "Name"              = "compute-${each.key}"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }

  user_data = base64encode(
    templatefile(
      "${path.module}/init/${each.value.init}",
      {
        authClientId  = var.cloudflareWarpAuthClientId
        authSecretKey = var.cloudflareWarpAuthSecretKey
      }
    )
  )
  disable_api_termination = true
  vpc_security_group_ids  = [aws_security_group.main.id]
}

resource "aws_batch_compute_environment" "main" {
  for_each = local.environments

  compute_environment_name_prefix = "${each.key}_"

  service_role = data.aws_iam_role.main["prod_common"].arn
  state        = "ENABLED"
  type         = "MANAGED"

  compute_resources {
    allocation_strategy = each.value.type == "EC2" ? "BEST_FIT" : "SPOT_CAPACITY_OPTIMIZED"
    bid_percentage      = 100
    image_id            = each.value.ami
    type                = each.value.type

    max_vcpus = each.value.max_vcpus
    min_vcpus = 0

    instance_role       = aws_iam_instance_profile.main.arn
    spot_iam_fleet_role = data.aws_iam_role.main["prod_common"].arn

    instance_type      = each.value.instances
    security_group_ids = [aws_security_group.main.id]
    subnets            = each.value.subnets

    tags = {
      "Name"               = each.key
      "fluidattacks:line"  = each.value.line
      "fluidattacks:comp"  = each.value.product
      "fluidattacks:stack" = "compute"
      "NOFLUID"            = "f333_ephemeral_instances"
    }

    launch_template {
      launch_template_id = aws_launch_template.main[each.key].id
      version            = aws_launch_template.main[each.key].latest_version
    }
  }

  tags = {
    "Name"               = each.key
    "fluidattacks:line"  = "cost"
    "fluidattacks:comp"  = each.value.product
    "fluidattacks:stack" = "compute"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_batch_job_queue" "main" {
  for_each = local.environments

  name                 = each.key
  state                = "ENABLED"
  priority             = 1
  compute_environments = [aws_batch_compute_environment.main[each.key].arn]

  tags = {
    "Name"              = each.key
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = each.value.product
  }
}
