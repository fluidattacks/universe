resource "aws_cloudwatch_log_group" "job" {
  name = "/aws/batch/job"

  tags = {
    "Name"              = "job"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"

  }
}

resource "aws_cloudwatch_log_group" "batch_jobs_status" {
  name              = "batch_jobs_status"
  retention_in_days = 365

  tags = {
    "Name"              = "batch_jobs_status"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
resource "aws_cloudwatch_log_group" "cloudinit_log_group" {
  name              = "cloudinit_log_group"
  retention_in_days = 0

  tags = {
    "Name"              = "cloudinit_log_group"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_log_metric_filter" "failed_schedules" {
  for_each = local.schedules

  name           = each.key
  pattern        = "{ ($.jobName = \"${each.key}\") && ($.status = \"FAILED\") }"
  log_group_name = aws_cloudwatch_log_group.batch_jobs_status.name

  metric_transformation {
    name          = each.key
    namespace     = "failed_schedules"
    value         = "1"
    default_value = "0"
  }
}
