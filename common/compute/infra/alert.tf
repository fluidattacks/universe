resource "aws_sns_topic" "main" {
  name              = "compute_alert"
  fifo_topic        = false
  kms_master_key_id = "alias/event-bridge-key-alias"

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "eventsSns",
          Effect = "Allow",
          Principal = {
            Service = ["events.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"              = "compute_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic_subscription" "main" {
  protocol  = "email"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.main.arn
}

resource "aws_cloudwatch_event_rule" "alert" {
  name = "compute_alert"

  event_pattern = jsonencode({
    source      = ["aws.batch"]
    detail-type = ["Batch Job State Change"]
    detail = {
      status = ["FAILED"]
      containerReason = [
        { anything-but = { prefix = "CannotInspectContainerError" } },
        { exists = false }
      ],
    }
  })

  tags = {
    "Name"              = "compute_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_event_target" "alert" {
  rule      = aws_cloudwatch_event_rule.alert.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.main.arn


  input_transformer {
    input_paths = {
      containerReason = "$.detail.container.reason"
      jobId           = "$.detail.jobId"
      jobName         = "$.detail.jobName"
      jobQueue        = "$.detail.jobQueue"
      status          = "$.detail.status"
      statusReason    = "$.detail.statusReason"
    }
    input_template = <<-EOF
      {
        "jobName": <jobName>,
        "jobQueue": <jobQueue>,
        "jobUrl": "https://us-east-1.console.aws.amazon.com/batch/home?region=us-east-1#jobs/detail/<jobId>",
        "status": <status>,
        "statusReason": <statusReason>,
        "containerReason": <containerReason>
      }
    EOF
  }
}

data "aws_iam_policy_document" "batch_jobs_status_policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream"
    ]

    resources = [
      "${aws_cloudwatch_log_group.batch_jobs_status.arn}:*"
    ]

    principals {
      type = "Service"
      identifiers = [
        "events.amazonaws.com",
        "delivery.logs.amazonaws.com"
      ]
    }
  }
  statement {
    effect = "Allow"
    actions = [
      "logs:PutLogEvents"
    ]

    resources = [
      "${aws_cloudwatch_log_group.batch_jobs_status.arn}:*:*"
    ]

    principals {
      type = "Service"
      identifiers = [
        "events.amazonaws.com",
        "delivery.logs.amazonaws.com"
      ]
    }

    condition {
      test     = "ArnEquals"
      values   = [aws_cloudwatch_event_rule.alert.arn]
      variable = "aws:SourceArn"
    }
  }
}

resource "aws_cloudwatch_log_resource_policy" "batch_jobs_status_policy" {
  policy_document = data.aws_iam_policy_document.batch_jobs_status_policy.json
  policy_name     = "compute-alert-log-publishing-policy"
}

resource "aws_cloudwatch_event_target" "logs" {
  rule      = aws_cloudwatch_event_rule.alert.name
  target_id = "SendToCloudWatch"
  arn       = aws_cloudwatch_log_group.batch_jobs_status.arn

  input_transformer {
    input_paths = {
      timestamp       = "$.time",
      containerReason = "$.detail.container.reason"
      jobId           = "$.detail.jobId"
      jobName         = "$.detail.jobName"
      jobQueue        = "$.detail.jobQueue"
      status          = "$.detail.status"
      statusReason    = "$.detail.statusReason"
    }
    input_template = <<-EOF
      {
        "timestamp": <timestamp>,
        "message": "{\"jobName\": \"<jobName>\",\"jobQueue\": \"<jobQueue>\",\"jobUrl\": \"https:\/\/us-east-1.console.aws.amazon.com/batch/home?region=us-east-1#jobs/detail/<jobId>\",\"status\": \"<status>\",\"statusReason\": \"<statusReason>\",\"containerReason\": \"<containerReason>\"}"
      }
    EOF
  }
}

data "aws_iam_policy_document" "schedules_notifications_lambda_sts" {
  statement {
    sid     = "SchedulesNotificationsLambdaSTS"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "schedules_notifications_lambda_logging" {
  statement {
    sid = "CloudwatchLogging"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:${var.region}:${data.aws_caller_identity.main.account_id}:log-group:/aws/lambda/${aws_lambda_function.schedules_notifications.function_name}:log-stream:*",
      "arn:aws:logs:${var.region}:${data.aws_caller_identity.main.account_id}:log-group:/aws/lambda/${aws_lambda_function.schedules_notifications.function_name}"
    ]
  }
}

resource "aws_iam_policy" "schedules_notifications_lambda_logging_lambda_logging" {
  name   = "SchedulesNotificationLambdaLogging"
  policy = data.aws_iam_policy_document.schedules_notifications_lambda_logging.json

  tags = {
    "Name"              = "schedules_notifications_lambda_logging_lambda_logging"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_iam_role" "schedules_notifications_lambda_role" {
  name               = "SchedulesNotificationsLambdaRole"
  assume_role_policy = data.aws_iam_policy_document.schedules_notifications_lambda_sts.json
  tags = {
    "Name"              = "schedules_notifications_lambda_logging_lambda_aws_iam_role"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_iam_role_policy_attachment" "sns_lambda_logging" {
  role       = aws_iam_role.schedules_notifications_lambda_role.name
  policy_arn = aws_iam_policy.schedules_notifications_lambda_logging_lambda_logging.arn
}

data "archive_file" "schedules_notifications_lambda_package" {
  type             = "zip"
  source_file      = "schedules_notifications/notifications.py"
  output_file_mode = "0666"
  output_path      = "schedules_notifications.zip"
}

resource "aws_lambda_function" "schedules_notifications" {
  function_name    = "schedules_notifications"
  role             = aws_iam_role.schedules_notifications_lambda_role.arn
  architectures    = ["arm64"]
  description      = "Notifies Schedules Alarm to Google Chat space"
  filename         = data.archive_file.schedules_notifications_lambda_package.output_path
  handler          = "notifications.lambda_handler"
  layers           = ["arn:aws:lambda:${var.region}:770693421928:layer:Klayers-p311-requests:13"]
  runtime          = "python3.11"
  source_code_hash = data.archive_file.schedules_notifications_lambda_package.output_base64sha256
  timeout          = 15
  publish          = false

  environment {
    variables = {
      "GOOGLE_CHAT_WEBHOOK" = var.googleChatWebhookSchedules
    }
  }

  tags = {
    "Name"              = "schedules_notifications"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
resource "aws_kms_key" "schedules_notifications" {
  description              = "KMS Schedules to SNS"
  deletion_window_in_days  = 10
  enable_key_rotation      = true
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Id" : "auto-sns-1",
      "Statement" : [
        {
          "Sid" : "Key Administrators",
          "Effect" : "Allow",
          "Principal" : {
            "AWS" : [
              "arn:aws:iam::205810638802:root",
              "arn:aws:iam::205810638802:role/prod_common"
            ]
          },
          "Action" : "kms:*",
          "Resource" : "*"
        },
        {
          "Sid" : "Allow CloudWatch to use the key",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : [
              "cloudwatch.amazonaws.com"
            ]
          },
          "Action" : [
            "kms:Decrypt",
            "kms:GenerateDataKey",
            "kms:DescribeKey"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
  tags = {
    "Name"              = "schedules_notifications_kms_key"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic" "schedules_notifications" {
  name              = "schedules_notifications"
  kms_master_key_id = aws_kms_key.schedules_notifications.key_id

  tags = {
    "Name"              = "schedules_notifications_sns_topic"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_lambda_permission" "schedules_notifications_sns_trigger" {
  statement_id  = "AllowSNSSchedulesNotificationsExecution"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.schedules_notifications.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.schedules_notifications.arn
}

resource "aws_sns_topic_subscription" "schedules_notifications_lambda" {
  endpoint  = aws_lambda_function.schedules_notifications.arn
  protocol  = "lambda"
  topic_arn = aws_sns_topic.schedules_notifications.arn
}

resource "aws_cloudwatch_metric_alarm" "schedules_notifications" {
  for_each = aws_cloudwatch_log_metric_filter.failed_schedules

  alarm_actions       = [aws_sns_topic.schedules_notifications.arn]
  alarm_description   = each.value.metric_transformation[0].namespace
  alarm_name          = "schedule_${each.value.metric_transformation[0].name}_failed"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  datapoints_to_alarm = 1
  evaluation_periods  = 1
  metric_name         = each.value.metric_transformation[0].name
  namespace           = each.value.metric_transformation[0].namespace
  period              = 43200
  statistic           = "Sum"
  threshold           = 1

  tags = {
    "Name"              = "schedules_notifications"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
