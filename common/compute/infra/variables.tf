data "aws_caller_identity" "main" {}
variable "region" {
  default = "us-east-1"
  type    = string
}

# Reused infrastructure

data "aws_iam_role" "main" {
  for_each = toset([
    "dev",
    "prod_airs",
    "prod_common",
    "prod_integrates",
    "prod_observes",
    "prod_skims",
    "prod_sorts",
  ])

  name = each.key
}

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["fluid-vpc"]
  }
}
data "aws_subnet" "main" {
  for_each = toset([
    "batch_main_1",
    "batch_main_2",
    "batch_main_3",
    "batch_main_4",
    "batch_main_5",
  ])

  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = [each.key]
  }
}
