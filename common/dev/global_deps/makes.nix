{ inputs, makeSearchPaths, ... }: {
  jobs."/common/dev/global_deps" = makeSearchPaths {
    bin = [
      # Required by nixfmt vscode extension
      inputs.nixpkgs.nixfmt
    ];
  };
}
