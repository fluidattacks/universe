# shellcheck shell=bash

function main {
  sops_export_vars "common/secrets/dev.yaml" \
    CLOUDFLARE_API_KEY \
    CLOUDFLARE_EMAIL \
    CLOUDFLARE_ACCOUNT_ID \
    && python __argCleanCloudflareDevices__
}

main
