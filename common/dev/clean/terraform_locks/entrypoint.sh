# shellcheck shell=bash

function main {
  aws_login "prod_common" "3600" \
    && python __argCleanTerraformLocks__
}

main
