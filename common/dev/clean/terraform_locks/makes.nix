{ makeScript, makePythonEnvironment, projectPath, outputs, ... }: {
  jobs."/common/dev/clean/terraform_locks" = makeScript {
    replace = {
      __argCleanTerraformLocks__ =
        projectPath "/common/dev/clean/terraform_locks/src/__init__.py";
    };
    name = "common_dev_clean_terraform_locks";
    entrypoint = ./entrypoint.sh;
    searchPaths = {
      source = [
        (makePythonEnvironment {
          pythonProjectDir = ./.;
          pythonVersion = "3.11";
        })
        outputs."/common/utils/aws"
      ];
    };
  };
}
