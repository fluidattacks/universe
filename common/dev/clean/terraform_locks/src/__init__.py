import boto3
from botocore.exceptions import (
    ClientError,
)
from datetime import (
    datetime,
    timedelta,
    timezone,
)
import json
import logging

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger()


def filter_items(items: list[dict]) -> list[dict]:
    time_threshold = datetime.now(timezone.utc) - timedelta(hours=2)
    filtered_items = []
    for item in items:
        info_attr = item.get("Info", {}).get("S")
        if info_attr:
            info_data = json.loads(info_attr)
            if "Created" in info_data:
                item_datetime = datetime.fromisoformat(
                    info_data["Created"].replace("Z", "+00:00")
                )
                if item_datetime < time_threshold:
                    filtered_items.append(item)

    return filtered_items


def main(table_name: str, partition_key: str) -> None:
    try:
        dynamodb_client = boto3.client("dynamodb")
        response = dynamodb_client.scan(TableName=table_name)
        items = response.get("Items", [])
        LOGGER.info("Loaded %s items from state lock table", len(items))
        items_to_remove = filter_items(items)

        if not items_to_remove:
            LOGGER.info("No items found to be deleted")
            return

        for item in items_to_remove:
            LOGGER.info("Item to remove is %s", item)
            primary_key = {partition_key: item[partition_key]}
            dynamodb_client.delete_item(TableName=table_name, Key=primary_key)

    except ClientError as exc:
        LOGGER.error("BotoError occurred %s", str(exc))


if __name__ == "__main__":
    main("terraform_state_lock", "LockID")
