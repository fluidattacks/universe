resource "aws_s3_bucket" "public_storage" {
  bucket = "fluidattacks.public.storage"

  tags = {
    "Name"              = "fluidattacks.public.storage"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "Access"            = "public-read"
    "NOFLUID"           = "f325_public_bucket"
  }
}

resource "aws_s3_bucket_versioning" "public_storage_versioning" {
  bucket = aws_s3_bucket.public_storage.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_ownership_controls" "public_storage" {
  bucket = aws_s3_bucket.public_storage.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "public_storage" {
  bucket = aws_s3_bucket.public_storage.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "public_storage" {
  depends_on = [
    aws_s3_bucket_ownership_controls.public_storage,
    aws_s3_bucket_public_access_block.public_storage,
  ]
  bucket = aws_s3_bucket.public_storage.id

  acl = "public-read"
}


resource "aws_s3_bucket_logging" "public_storage" {
  bucket = aws_s3_bucket.public_storage.id

  target_bucket = "common.logging"
  target_prefix = "log/public_storage"
}
