{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/sorts-training";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/common/ai/sorts-training/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "training-dev";
  };
}
