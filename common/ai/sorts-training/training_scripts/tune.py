import argparse
import os
from pandas import (
    DataFrame,
)
from sklearn.preprocessing import (  # type: ignore [import]
    MinMaxScaler,
)
from training_scripts.constants import (
    FEATURES_DICTS,
    MODEL_HYPERPARAMETERS,
    MODELS,
    MODELS_DEFAULTS,
    OVERFIT_LIMIT,
    RESULT_HEADERS,
)
from training_scripts.logs import (
    log,
)
from training_scripts.typings import (
    Model as ModelType,
)
from training_scripts.utils import (
    fit_model,
    get_current_model_features,
    get_previous_training_results,
    load_training_data,
    save_model_to_s3,
    update_results_csv,
)
from typing import (
    Dict,
    List,
    Tuple,
    Union,
)


def save_model(
    model: ModelType,
    f1_score: float,
    model_features: Tuple[str, ...],
    tuned_hyperparameters: List[str],
    scaler: MinMaxScaler,
) -> None:
    model_file_name: str = "-".join(
        [
            type(model).__name__.lower(),
            str(f1_score),
            "-".join(
                [FEATURES_DICTS[feature].lower() for feature in model_features]
            ),
            "tune",
            "-".join(
                list(str(parameter) for parameter in tuned_hyperparameters)
            ),
        ]
    )
    scaler_file_name = "-".join(["scaler", model_file_name])
    save_model_to_s3(model, model_file_name)
    save_model_to_s3(scaler, scaler_file_name)


def get_model_hyperparameters(
    model_name: str, args: Dict[str, str]
) -> Dict[str, str]:
    model_hyperparameters = list(MODEL_HYPERPARAMETERS[model_name].keys())

    return {parameter: args[parameter] for parameter in model_hyperparameters}


def display_model_hyperparameters(
    model_name: str, hyperparameters_list: Dict[str, str]
) -> None:
    log(
        "info",
        "We have the following hyperparameters for our %s} model tuning:",
        model_name.upper(),
    )
    for parameter, value in hyperparameters_list.items():
        log("info", "%s: %s", parameter, value)


def check_max_leaf_nodes(value: str) -> Union[int, None]:
    if value != "0":
        return int(value)

    return None


def cli() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    # Sagemaker specific arguments. Defaults are set in environment variables.
    parser.add_argument(
        "--output-data-dir", type=str, default=os.environ["SM_OUTPUT_DATA_DIR"]
    )
    parser.add_argument(
        "--model-dir", type=str, default=os.environ["SM_MODEL_DIR"]
    )
    parser.add_argument(
        "--train", type=str, default=os.environ["SM_CHANNEL_TRAIN"]
    )
    parser.add_argument("--model", type=str, default="")

    # MLPClassifier parameters to tune
    parser.add_argument("--activation", type=str, default="")
    parser.add_argument("--solver", type=str, default="")

    # XGBoost parameters to tune
    parser.add_argument("--criterion", type=str, default="friedman_mse")
    parser.add_argument("--loss", type=str, default="deviance")
    parser.add_argument("--max_depth", type=int, default=3)
    parser.add_argument("--n_estimators", type=int, default=100)
    parser.add_argument("--learning_rate", type=float, default=0.1)

    # HistGradientBoostingClassifier parameters to tune
    parser.add_argument(
        "--max_leaf_nodes", type=check_max_leaf_nodes, default=31
    )

    # RandomForestClassifier parameters to tune
    parser.add_argument("--min_samples_leaf", type=int, default=5)

    return parser.parse_args()


def main() -> None:
    args = cli()

    model_name: str = args.model.split("-")[0]
    model_features: Tuple[str, ...] = get_current_model_features()

    hyperparameters_to_tune: dict[str, str] = get_model_hyperparameters(
        model_name,
        vars(args),
    )
    display_model_hyperparameters(model_name, hyperparameters_to_tune)
    model_class: ModelType = MODELS[model_name]
    model_parameters: dict[str, object] = {"random_state": 42}
    model_parameters.update(
        {
            **MODELS_DEFAULTS.get(model_class, {}),
            **hyperparameters_to_tune,
        }
    )
    results_filename: str = f"{model_name}_tune_results.csv"

    # Start training process
    training_data: DataFrame = load_training_data(args.train)
    training_dataset = training_data.sample(frac=0.8)
    testing_dataset = training_data.drop(training_dataset.index).sample(frac=1)
    tuned_model, model_metrics, scaler = fit_model(
        model_class(**model_parameters),
        model_features,
        training_dataset,
        testing_dataset,
        ", ".join(
            list(
                str(parameter)
                for parameter in hyperparameters_to_tune.values()
            )
        ),
    )
    print(f"Training time: {model_metrics[8]}")
    print(f"Features: {model_features}")
    print(f"Precision: {model_metrics[2]}%")
    print(f"Recall: {model_metrics[3]}%")
    print(f"F1-Score: {model_metrics[4]}%")
    print(f"Accuracy: {model_metrics[5]}%")
    print(f"Overfit: {model_metrics[6]}%")
    if float(model_metrics[6]) < OVERFIT_LIMIT:
        save_model(
            tuned_model,
            tuned_model.ff_f1,
            model_features,
            list(hyperparameters_to_tune.values()),
            scaler,
        )
    previous_results = get_previous_training_results(results_filename)
    accumulated_tuning_results: list[list[str]] = (
        previous_results if previous_results else [RESULT_HEADERS]
    )
    accumulated_tuning_results.append(model_metrics)
    update_results_csv(results_filename, accumulated_tuning_results)


if __name__ == "__main__":
    main()
