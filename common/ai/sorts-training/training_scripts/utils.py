from botocore.exceptions import (
    ClientError,
)
import csv
from joblib import (
    dump,
)
import os
import pandas as pd
from pandas import (
    DataFrame,
)
from sklearn.metrics import (  # type: ignore [import]
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.model_selection import (  # type: ignore [import]
    cross_validate,
)
from sklearn.pipeline import (  # type: ignore [import]
    make_pipeline,
)
from sklearn.preprocessing import (  # type: ignore [import]
    MinMaxScaler,
)
import tempfile
import time
from training_scripts.constants import (
    FEATURES_DICTS,
    OVERFIT_LIMIT,
    S3_BUCKET,
    S3_BUCKET_NAME,
    S3_RESOURCE,
)
from training_scripts.typings import (
    Model as ModelType,
)
from typing import (
    List,
    Tuple,
    Union,
)


def get_best_model_name(model_name_file: str, mode: str = "train") -> str:
    # Since the best model has a generic name for easier download,
    # this TXT keeps track of the model name (class, f1, features)
    # so the final artifact is only replaced if there has been
    # an improvement
    best_model_lines: list[str] = []
    index: int = {"train": 0, "tune": -1}[mode]
    S3_RESOURCE.Object(
        S3_BUCKET_NAME, "training-output/best_model.txt"
    ).download_file(model_name_file)
    with open(model_name_file, encoding="utf8") as file:
        best_model_lines = file.read().splitlines()

    return best_model_lines[index]


def get_model_performance_metrics(
    model: ModelType, features: DataFrame, labels: DataFrame
) -> Tuple[float, float, float, float, float]:
    """Get performance metrics to compare different models"""
    scores = cross_validate(
        model,
        features,
        labels,
        scoring=["precision", "recall", "f1", "accuracy"],
        n_jobs=-1,
        return_train_score=True,
    )
    train_score = scores["train_f1"].mean()
    test_score = scores["test_f1"].mean()
    return (
        scores["test_precision"].mean() * 100,
        scores["test_recall"].mean() * 100,
        test_score * 100,
        scores["test_accuracy"].mean() * 100,
        (train_score - test_score) / train_score * 100,
    )


def split_training_data(
    training_df: DataFrame, feature_list: Tuple[str, ...]
) -> Tuple[DataFrame, DataFrame]:
    """Read the training data in two DataFrames for training purposes"""
    # Separate the labels from the features in the training data
    filtered_df = pd.concat(  # type: ignore [call-overload]
        [
            # Include labels
            training_df.iloc[:, 0],
            # Include features
            training_df.loc[:, feature_list],  # type: ignore [index]
            # Include all extensions
            training_df.loc[
                :, training_df.columns.str.startswith("extension_")
            ],
        ],
        axis=1,
    )
    filtered_df.dropna(inplace=True)

    return filtered_df.iloc[:, 1:], filtered_df.iloc[:, 0]


def get_previous_training_results(results_filename: str) -> List[List[str]]:
    previous_results: list[list[str]] = []
    with tempfile.TemporaryDirectory() as tmp_dir:
        local_file: str = os.path.join(tmp_dir, results_filename)
        remote_file: str = f"training-output/results/{results_filename}"
        try:
            S3_BUCKET.Object(remote_file).download_file(local_file)
        except ClientError as error:
            if error.response["Error"]["Code"] == "404":
                return previous_results
        else:
            with open(local_file, "r", encoding="utf8") as csv_file:
                csv_reader = csv.reader(csv_file)
                previous_results.extend(csv_reader)

    return previous_results


def get_current_model_features() -> Tuple[str, ...]:
    with tempfile.TemporaryDirectory() as tmp_dir:
        model_name_file: str = os.path.join(tmp_dir, "best_model.txt")
        best_model: str = get_best_model_name(model_name_file)
        inv_features_dict: dict[str, str] = {
            value: key for key, value in FEATURES_DICTS.items()
        }

        return tuple(
            inv_features_dict[key.upper()]
            for key in best_model.split("-")[2:]
            if len(key) == 2
        )


def load_training_data(training_dir: str) -> DataFrame:
    """Load a DataFrame with the training data in CSV format stored in S3"""
    input_files: list[str] = [
        os.path.join(training_dir, file) for file in os.listdir(training_dir)
    ]
    raw_data: list[DataFrame] = [
        pd.read_csv(file, engine="python") for file in input_files
    ]

    return pd.concat(raw_data)


def update_results_csv(filename: str, results: List[List[str]]) -> None:
    with open(filename, "w", newline="", encoding="utf8") as results_file:
        csv_writer = csv.writer(results_file)
        csv_writer.writerows(results)
    S3_BUCKET.Object(f"training-output/results/{filename}").upload_file(
        filename
    )


def save_model_to_s3(
    model: Union[ModelType, MinMaxScaler], model_name: str
) -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        local_file: str = os.path.join(tmp_dir, f"{model_name}.joblib")
        dump(model, local_file)
        S3_BUCKET.Object(f"training-output/{model_name}.joblib").upload_file(
            local_file
        )


def train_combination(
    model: ModelType,
    training_data: DataFrame,
    model_features: Tuple[str, ...],
    tuned_hyperparameters: str = "n/a",
) -> List[str]:
    start_time: float = time.time()

    train_x, train_y = split_training_data(training_data, model_features)
    scaler = MinMaxScaler()
    pipeline = make_pipeline(scaler, model)
    metrics = get_model_performance_metrics(pipeline, train_x, train_y)

    training_time = time.time() - start_time
    print(f"Training time: {training_time:.2f}")
    print(f"Features: {model_features}")
    print(f"Precision: {metrics[0]}%")
    print(f"Recall: {metrics[1]}%")
    print(f"F1-Score: {metrics[2]}%")
    print(f"Accuracy: {metrics[3]}%")
    print(f"Overfit: {metrics[4]}%")
    combination_train_results = {
        "model": model.__class__.__name__,
        "features": " ".join(
            FEATURES_DICTS[feature] for feature in model_features
        ),
        "precision": round(metrics[0], 1),
        "recall": round(metrics[1], 1),
        "f_score": round(metrics[2], 1),
        "accuracy": round(metrics[3], 1),
        "overfit": round(metrics[4], 1),
        "tuned_parameters": tuned_hyperparameters,
        "training_time": training_time,
    }
    training_output = [
        str(item) for item in combination_train_results.values()
    ]

    return training_output


def get_best_combination(
    training_results: List[List[str]],
) -> Tuple[Tuple[str, ...], str]:
    inv_features_dict: dict[str, str] = {
        v: k for k, v in FEATURES_DICTS.items()
    }

    # Sort results in descending order by F1 and Overfit
    sorted_results: list[list[str]] = sorted(
        training_results[1:],
        key=lambda results_row: (float(results_row[4]), float(results_row[6])),
        reverse=True,
    )
    best_f1_score: float = 0.0
    best_model: list[str] = []
    best_f1: str = ""
    best_features: Tuple[str, ...] = tuple()
    for results_row in sorted_results:
        current_f1_score = float(results_row[4])
        overfit = float(results_row[6])
        if 0 < overfit < OVERFIT_LIMIT and current_f1_score >= best_f1_score:
            best_f1_score = float(results_row[4])
            best_model = results_row

    if best_model:
        best_features = tuple(  # pylint: disable=consider-using-generator
            [
                inv_features_dict[feature]
                for feature in best_model[1].split(" ")
            ]
        )
        best_f1 = str(float(best_model[4]))

    return best_features, best_f1


def fit_model(  # pylint: disable=too-many-locals
    model: ModelType,
    model_features: Tuple[str, ...],
    training_dataset: DataFrame,
    testing_dataset: DataFrame,
    tuned_hyperparameters: str = "n/a",
) -> Tuple[ModelType, List[str], MinMaxScaler]:
    start_time: float = time.time()
    train_x, train_y = split_training_data(training_dataset, model_features)
    scaler = MinMaxScaler()
    scaler.fit(train_x)
    transformed_train_x = pd.DataFrame(scaler.transform(train_x))

    model.fit(transformed_train_x, train_y)
    train_f1 = f1_score(train_y, model.predict(transformed_train_x))

    test_x, test_y = split_training_data(testing_dataset, model_features)
    transformed_test_x = pd.DataFrame(scaler.transform(test_x))
    test_pred_y = model.predict(transformed_test_x)
    test_f1 = f1_score(test_y, test_pred_y)
    test_precision = precision_score(test_y, test_pred_y)
    test_recall = recall_score(test_y, test_pred_y)
    test_accuracy = accuracy_score(test_y, test_pred_y)
    training_time = time.time() - start_time

    model.feature_names = list(model_features)
    model.precision = round(test_precision * 100, 2)
    model.recall = round(test_recall * 100, 2)
    model.ff_f1 = round(test_f1 * 100, 2)
    model.scaled_input = True

    best_model_train_results = {
        "model": model.__class__.__name__,
        "features": " ".join(
            FEATURES_DICTS[feature] for feature in model_features
        ),
        "precision": model.precision,
        "recall": model.recall,
        "f_score": model.ff_f1,
        "accuracy": round(test_accuracy * 100, 2),
        "overfit": round((train_f1 - test_f1) / train_f1 * 100, 2),
        "tuned_parameters": tuned_hyperparameters,
        "training_time": training_time,
    }
    model_metrics = [str(item) for item in best_model_train_results.values()]

    return model, model_metrics, scaler
