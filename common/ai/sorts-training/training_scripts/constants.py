import boto3
from sagemaker.tuner import (  # type: ignore [import]
    CategoricalParameter,
    ContinuousParameter,
    IntegerParameter,
)
from sklearn.ensemble import (  # type: ignore [import]
    GradientBoostingClassifier,
    HistGradientBoostingClassifier,
    RandomForestClassifier,
)
from sklearn.neural_network import (  # type: ignore [import]
    MLPClassifier,
)
from training_scripts.typings import (
    Model as ModelType,
)
from typing import (
    Dict,
    List,
    Tuple,
    Union,
)
from xgboost import (
    XGBClassifier,
)

OVERFIT_LIMIT: float = 8.0

# AWS-related
S3_BUCKET_NAME: str = "sorts"
S3_RESOURCE = boto3.resource("s3")
S3_BUCKET = S3_RESOURCE.Bucket(S3_BUCKET_NAME)

# Model-related
FEATURES_DICTS: Dict[str, str] = {
    "num_commits": "CM",
    "num_unique_authors": "AU",
    "file_age": "FA",
    "midnight_commits": "MC",
    "risky_commits": "RC",
    "seldom_contributors": "SC",
    "num_lines": "LC",
    "busy_file": "BF",
    "commit_frequency": "CF",
    "last_commit_days": "CD",
}
RESULT_HEADERS: List[str] = [
    "Model",
    "Features",
    "Precision",
    "Recall",
    "F1",
    "Accuracy",
    "Overfit",
    "TunedParams",
    "Time",
]
MODELS: Dict[str, ModelType] = {
    "gradientboostingclassifier": GradientBoostingClassifier,
    "histgradientboostingclassifier": HistGradientBoostingClassifier,
    "randomforestclassifier": RandomForestClassifier,
    "xgbclassifier": XGBClassifier,
    "mlpclassifier": MLPClassifier,
}


MODELS_DEFAULTS: Dict[
    ModelType, Dict[str, Union[str, int, float, Tuple[int, int, int]]]
] = {
    GradientBoostingClassifier: {
        "n_estimators": 50,
        "learning_rate": 0.75,
    },
    HistGradientBoostingClassifier: {
        "learning_rate": 0.2,
        "max_iter": 1000,
    },
    MLPClassifier: {
        "activation": "tanh",
        "alpha": 0.0001,
        "hidden_layer_sizes": (50, 50, 50),
        "learning_rate": "constant",
        "max_iter": 1000,
        "solver": "adam",
    },
}

# Hyperparameters
MODEL_HYPERPARAMETERS = {
    "gradientboostingclassifier": {
        "n_estimators": IntegerParameter(20, 150, scaling_type="Logarithmic"),
        "learning_rate": ContinuousParameter(
            0.01, 0.7, scaling_type="Logarithmic"
        ),
    },
    "histgradientboostingclassifier": {
        "max_leaf_nodes": CategoricalParameter([0, 5, 25, 50, 100, 250, 500]),
        "learning_rate": ContinuousParameter(
            0.01, 1, scaling_type="Logarithmic"
        ),
    },
    "randomforestclassifier": {
        "n_estimators": IntegerParameter(1, 200),
        "max_depth": IntegerParameter(1, 50),
        "min_samples_leaf": IntegerParameter(4, 20),
    },
    "xgbclassifier": {
        "learning_rate": ContinuousParameter(
            0.01, 0.2, scaling_type="Logarithmic"
        ),
        "max_depth": IntegerParameter(3, 7),
        "min_child_weight": IntegerParameter(3, 7),
    },
    "mlpclassifier": {
        "hidden_layer_sizes": [(50, 50, 50), (50, 100, 50), (100,)],
        "activation": CategoricalParameter(
            ["relu", "tanh", "identity", "logistic"]
        ),
        "solver": CategoricalParameter(["lbfgs", "sgd", "adam"]),
    },
}
