from sklearn.ensemble import (  # type: ignore [import]
    GradientBoostingClassifier,
    HistGradientBoostingClassifier,
    RandomForestClassifier,
)
from typing import (
    Union,
)
from xgboost import (
    XGBClassifier,
)

Model = Union[
    GradientBoostingClassifier,
    HistGradientBoostingClassifier,
    RandomForestClassifier,
    XGBClassifier,
]
