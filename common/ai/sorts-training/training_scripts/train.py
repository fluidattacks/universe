#!/usr/bin/env python3

import argparse
from itertools import (
    combinations,
)
import os
from pandas import (
    DataFrame,
)
from sklearn.neighbors import (  # type: ignore [import]
    KNeighborsClassifier,
)
from training_scripts.constants import (
    FEATURES_DICTS,
    MODELS,
    MODELS_DEFAULTS,
    RESULT_HEADERS,
)
from training_scripts.typings import (
    Model as ModelType,
)
from training_scripts.utils import (
    fit_model,
    get_best_combination,
    load_training_data,
    save_model_to_s3,
    train_combination,
    update_results_csv,
)
from typing import (
    Generator,
    List,
    Tuple,
    Union,
)


def get_features_combinations(features: List[str]) -> List[Tuple[str, ...]]:
    feature_combinations: List[Tuple[str, ...]] = []
    for idx in range(len(features) + 1):
        feature_combinations += list(combinations(features, idx))
    return list(filter(None, feature_combinations))


def get_model_instance(model_class: ModelType) -> ModelType:
    default_args: dict[str, Union[str, int, float, tuple[int, int, int]]] = {}
    if model_class != KNeighborsClassifier:
        default_args = {"random_state": 42}
    model_defaults = MODELS_DEFAULTS.get(model_class, {})
    default_args.update(model_defaults)

    return model_class(**default_args)


def save_model(
    model_class: ModelType,
    full_dataset: DataFrame,
    training_results: List[List[str]],
) -> List[str]:
    best_features, best_f1 = get_best_combination(training_results)

    if best_features:
        training_dataset = full_dataset.sample(frac=0.8, random_state=42)
        testing_dataset = full_dataset.drop(training_dataset.index).sample(
            frac=1, random_state=42
        )
        base_model = get_model_instance(model_class)
        model, model_metrics, scaler = fit_model(
            base_model, best_features, training_dataset, testing_dataset
        )
        print(f"Cross validation F1 score: {best_f1}")
        print(f"Final fit F1 score: {model.ff_f1}")
        model.cv_f1 = best_f1
        model_file_name: str = "-".join(
            [type(model).__name__.lower(), str(model.ff_f1)]
            + [FEATURES_DICTS[feature].lower() for feature in best_features]
        )
        scaler_file_name = "-".join(["scaler", model_file_name])
        save_model_to_s3(model, model_file_name)
        save_model_to_s3(scaler, scaler_file_name)

        return model_metrics

    return []


def split_list(
    list_to_split: List[Tuple[str, ...]], chunk_size: int
) -> Generator[List[Tuple[str, ...]], object, None]:
    """Split a list into several lists of equal length"""
    for i in range(0, len(list_to_split), chunk_size):
        yield list_to_split[i : i + chunk_size]


def train_model(
    model_class: ModelType,
    training_dataset: DataFrame,
    job_index: int,
) -> List[List[str]]:
    all_combinations = get_features_combinations(list(FEATURES_DICTS.keys()))
    split_combinations_list = list(split_list(all_combinations, 16))
    # Train combinations with more features first, as they are higher priority
    split_combinations_list.reverse()
    training_output: list[list[str]] = [RESULT_HEADERS]

    # Train the model
    for combination in split_combinations_list[job_index]:
        model = get_model_instance(model_class)
        training_combination_output: list[str] = train_combination(
            model, training_dataset, combination
        )
        training_output.append(training_combination_output)

    return training_output


def cli() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    # Sagemaker specific arguments. Defaults are set in environment variables.
    parser.add_argument(
        "--output-data-dir", type=str, default=os.environ["SM_OUTPUT_DATA_DIR"]
    )
    parser.add_argument(
        "--model-dir", type=str, default=os.environ["SM_MODEL_DIR"]
    )
    parser.add_argument(
        "--train", type=str, default=os.environ["SM_CHANNEL_TRAIN"]
    )

    # Model to train sent as a hyperparameter
    parser.add_argument("--model", type=str, default="")
    parser.add_argument("--index", type=int, default=0)

    return parser.parse_args()


def main() -> None:
    args = cli()

    model_name: str = args.model
    model_class: ModelType = MODELS[model_name]

    full_dataset: DataFrame = load_training_data(args.train).sample(frac=1)

    # Start training process
    results_filename: str = f"{model_name}_train_results_{args.index}.csv"
    training_output = train_model(model_class, full_dataset, args.index)
    saved_model = save_model(model_class, full_dataset, training_output)
    if saved_model:
        training_output.append(saved_model)
    update_results_csv(results_filename, training_output)


if __name__ == "__main__":
    main()
