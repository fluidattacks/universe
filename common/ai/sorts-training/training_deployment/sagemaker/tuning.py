#!/usr/bin/env python3

from botocore.exceptions import (
    ClientError,
)
import os
from sagemaker.sklearn.estimator import (  # type: ignore [import]
    SKLearn as SKLearnEstimator,
)
from sagemaker.tuner import (  # type: ignore [import]
    HyperparameterTuner,
)
from snowflake_connection import (
    db as snowflake,
)
import tempfile
from training_deployment.constants import (
    DATASET_PATH,
    MODEL_HYPERPARAMETERS,
    S3_BUCKET_NAME,
    S3_RESOURCE,
    SAGEMAKER_METRIC_DEFINITIONS,
)
from training_deployment.sagemaker.evaluation import (
    get_best_model_name,
)
from training_deployment.sagemaker.training import (
    get_estimator,
)
from training_deployment.sagemaker.utils import (
    get_previous_training_results,
)


def tune_sorts_model() -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        model_name_file: str = os.path.join(tmp_dir, "best_model.txt")
        model: str = get_best_model_name(model_name_file, "train")
        model = model.split("-")[0]
    estimator: SKLearnEstimator = get_estimator(
        model,
        "tuning",
        training_script="common/ai/sorts-training/training_scripts/tune.py",
    )

    try:
        S3_RESOURCE.Object(
            S3_BUCKET_NAME, f"training-output/results/{model}_tune_results.csv"
        ).delete()
    except ClientError as error:
        if error.response["Error"]["Code"] == "404":
            print("[INFO] No previous results to delete")

    tuner = HyperparameterTuner(
        estimator,
        max_jobs=200,
        max_parallel_jobs=10,
        metric_definitions=SAGEMAKER_METRIC_DEFINITIONS,
        objective_metric_name="fscore",
        objective_type="Maximize",
        hyperparameter_ranges=MODEL_HYPERPARAMETERS[model],
        tags=[
            {"Key": "fluidattacks:line", "Value": "cost"},
            {"Key": "fluidattacks:comp", "Value": "sorts"},
        ],
    )

    tuner.fit({"train": DATASET_PATH})

    results_filename: str = f"{model}_tune_results.csv"
    previous_results = get_previous_training_results(results_filename)
    for result in previous_results[1:]:
        combination_train_results = {
            "model": result[0],
            "features": result[1],
            "precision": result[2],
            "recall": result[3],
            "f_score": result[4],
            "overfit": result[6],
            "tuned_parameters": result[7],
            "training_time": result[8],
        }
        snowflake.insert("training", combination_train_results)

    # Here we get the best hyperparameters combination.
    # We can evaluate them and make decisions from here.
    _ = tuner.best_estimator().hyperparameters()
