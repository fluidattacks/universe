import click
from training_deployment.sagemaker.evaluation import (
    evaluate_results,
)
from training_deployment.sagemaker.training import (
    train_sorts_model,
)
from training_deployment.sagemaker.tuning import (
    tune_sorts_model,
)


@click.command()
@click.option(
    "--mode",
    type=click.Choice(
        [
            "train",
            "tune",
        ]
    ),
    default="train",
    show_default=True,
    help="Choose training or tuning",
)
def cli(
    mode: str,
) -> None:
    if mode == "train":
        train_sorts_model()
        evaluate_results("train")
    elif mode == "tune":
        tune_sorts_model()
        evaluate_results("tune")


if __name__ == "__main__":
    cli()
