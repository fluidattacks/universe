import boto3
from botocore.exceptions import (
    ClientError,
)
from click.testing import (
    CliRunner,
)
from moto import (
    mock_aws,
)
import os
import pytest
import sys
import tarfile
import tempfile
from training_deployment.cli import (
    cli,
)
from training_scripts import (
    train,
    tune,
)
from unittest.mock import (
    MagicMock,
    patch,
)

DATA_PATH: str = f"{os.path.dirname(__file__)}/test_data"


@mock_aws
@patch(
    "training_deployment.sagemaker.training.SKLearn", new_callable=MagicMock
)
@patch(
    "training_deployment.sagemaker.training.snowflake", new_callable=MagicMock
)
@patch(
    "training_deployment.sagemaker.evaluation.snowflake",
    new_callable=MagicMock,
)
def test_sagemaker_training(
    mock_eval_snowflake: MagicMock,
    mock_training_snowflake: MagicMock,
    mock_sklearn: MagicMock,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        tar_path = os.path.join(DATA_PATH, "training-output.tgz")
        with tarfile.open(tar_path, "r:gz") as training_tar:
            training_tar.extractall(tmp_dir)
            training_path = f"{tmp_dir}/training-output"
            mock_s3 = boto3.resource("s3")
            mock_bucket = mock_s3.create_bucket(Bucket="sorts")
            for root, _, files in os.walk(training_path):
                for file in files:
                    local_path = os.path.join(root, file)
                    if file == ".DS_Store" or file.startswith("._"):
                        os.remove(local_path)
                        continue
                    s3_key = os.path.relpath(local_path, tmp_dir)
                    mock_bucket.upload_file(local_path, s3_key)

    monkeypatch.setattr(
        "training_deployment.sagemaker.training.PARALLEL_JOBS",
        1,
    )
    runner = CliRunner()
    params = [
        "--mode",
        "train",
    ]
    result = runner.invoke(cli=cli, args=params)
    mock_eval_snowflake.insert.assert_called_once()
    mock_training_snowflake.insert.assert_called(),
    mock_sklearn.assert_called()
    assert result.exit_code == 0


@mock_aws
@patch(
    "training_deployment.sagemaker.training.SKLearn", new_callable=MagicMock
)
@patch(
    "training_deployment.sagemaker.tuning.HyperparameterTuner",
    new_callable=MagicMock,
)
@patch(
    "training_deployment.sagemaker.tuning.snowflake", new_callable=MagicMock
)
@patch(
    "training_deployment.sagemaker.evaluation.snowflake",
    new_callable=MagicMock,
)
def test_sagemaker_tuning(
    mock_eval_snowflake: MagicMock,
    _: MagicMock,
    mock_hyperparameter: MagicMock,
    mock_sklearn: MagicMock,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        tar_path = os.path.join(DATA_PATH, "training-output.tgz")
        with tarfile.open(tar_path, "r:gz") as training_tar:
            training_tar.extractall(tmp_dir)
            training_path = f"{tmp_dir}/training-output"
            mock_s3 = boto3.resource("s3")
            mock_bucket = mock_s3.create_bucket(Bucket="sorts")
            for root, dirs, files in os.walk(training_path):
                for file in files:
                    local_path = os.path.join(root, file)
                    if file == ".DS_Store" or file.startswith("._"):
                        os.remove(local_path)
                        continue
                    s3_key = os.path.relpath(local_path, tmp_dir)
                    mock_bucket.upload_file(local_path, s3_key)

    monkeypatch.setattr(
        "training_deployment.sagemaker.training.PARALLEL_JOBS",
        1,
    )
    runner = CliRunner()
    params = [
        "--mode",
        "tune",
    ]
    result = runner.invoke(cli=cli, args=params)
    mock_eval_snowflake.insert.assert_called_once()
    mock_hyperparameter.assert_called_once()
    mock_sklearn.assert_called()
    assert result.exit_code == 0


@mock_aws
def test_script_train(monkeypatch: pytest.MonkeyPatch) -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        tar_path = os.path.join(DATA_PATH, "script-training-output.tgz")
        with tarfile.open(tar_path, "r:gz") as training_tar:
            training_tar.extractall(tmp_dir)
            training_path = f"{tmp_dir}/training-output"
            mock_s3 = boto3.resource("s3")
            mock_bucket = mock_s3.create_bucket(Bucket="sorts")
            for root, _, files in os.walk(training_path):
                for file in files:
                    local_path = os.path.join(root, file)
                    if file == ".DS_Store" or file.startswith("._"):
                        os.remove(local_path)
                        continue
                    s3_key = os.path.relpath(local_path, tmp_dir)
                    mock_bucket.upload_file(local_path, s3_key)

    with monkeypatch.context() as m:
        monkeypatch.setenv("SM_OUTPUT_DATA_DIR", "test")
        monkeypatch.setenv("SM_MODEL_DIR", "test")
        monkeypatch.setenv(
            "SM_CHANNEL_TRAIN", os.path.join(DATA_PATH, "dataset")
        )
        m.setattr(
            sys,
            "argv",
            [
                "train.py",
                "--index",
                "1",
                "--model",
                "histgradientboostingclassifier",
            ],
        )
        train.main()
    try:
        mock_s3.Object(
            "sorts",
            "training-output/results/"
            "histgradientboostingclassifier_train_results.csv",
        ).load()
    except ClientError as exc:
        assert False, f"Results file not found: {exc}"


@mock_aws
def test_script_tune(monkeypatch: pytest.MonkeyPatch) -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        tar_path = os.path.join(DATA_PATH, "script-training-output.tgz")
        with tarfile.open(tar_path, "r:gz") as training_tar:
            training_tar.extractall(tmp_dir)
            training_path = f"{tmp_dir}/training-output"
            mock_s3 = boto3.resource("s3")
            mock_bucket = mock_s3.create_bucket(Bucket="sorts")
            for root, _, files in os.walk(training_path):
                for file in files:
                    local_path = os.path.join(root, file)
                    if file == ".DS_Store" or file.startswith("._"):
                        os.remove(local_path)
                        continue
                    s3_key = os.path.relpath(local_path, tmp_dir)
                    mock_bucket.upload_file(local_path, s3_key)

    with monkeypatch.context() as m:
        monkeypatch.setenv("SM_OUTPUT_DATA_DIR", "test")
        monkeypatch.setenv("SM_MODEL_DIR", "test")
        monkeypatch.setenv(
            "SM_CHANNEL_TRAIN", os.path.join(DATA_PATH, "dataset")
        )
        m.setattr(
            sys,
            "argv",
            [
                "tune.py",
                "--learning_rate",
                "0.09",
                "--max_leaf_nodes",
                "0",
                "--model",
                "histgradientboostingclassifier",
            ],
        )
        tune.main()
    try:
        mock_s3.Object(
            "sorts",
            "training-output/results/"
            "histgradientboostingclassifier_tune_results.csv",
        ).load()
    except ClientError as exc:
        assert False, f"Results file not found: {exc}"
