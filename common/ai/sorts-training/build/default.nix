{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      boto3
      click
      joblib
      pandas
      sagemaker
      scikit-learn
      snowflake-connection
      xgboost
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      boto3-stubs
      joblib-stubs
      moto
      mypy
      pandas-stubs
      pytest
      pytest-cov
    ];
  };
  packages = makes_inputs.makePythonPyprojectPackage {
    inherit (deps.lib) buildEnv buildPythonPackage;
    inherit pkgDeps src;
  };
  new_pkg = packages.pkg.overridePythonAttrs (_: rec { doCheck = false; });
  inherit (deps.lib) buildEnv;
  build_env = extraLibs:
    buildEnv {
      inherit extraLibs;
      ignoreCollisions = false;
    };
  new_runtime = build_env [ new_pkg ];
in packages // {
  env = {
    inherit (packages.env) dev;
    runtime = new_runtime;
    inherit (packages.check) types;
  };
}
