{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  raw_src = makes_inputs.projectPath "/common/ai/snowflake-connection";
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle = import "${raw_src}/build" {
    inherit makes_inputs nixpkgs python_version src;
  };
in bundle
