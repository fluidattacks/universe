{ makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/snowflake-connection";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/common/ai/snowflake-connection/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "snowflake-connection-dev";
  };
}
