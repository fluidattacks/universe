from contextlib import (
    contextmanager,
)
from cryptography.hazmat.backends import (
    default_backend,
)
from cryptography.hazmat.primitives import (
    serialization,
)
from os import (
    environ,
)
from snowflake import (
    connector,
)
from typing import (
    Any,
    Dict,
    Iterator,
)

Item = Dict[str, Any]


@contextmanager
def db_cursor() -> Iterator[Any]:
    private_key = serialization.load_pem_private_key(
        environ["SNOWFLAKE_PRIVATE_KEY"].encode("utf-8"),
        password=None,
        backend=default_backend(),
    )
    pkb = private_key.private_bytes(
        encoding=serialization.Encoding.DER,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
    )
    connection = connector.connect(
        user=environ["SNOWFLAKE_USER"],
        private_key=pkb,
        account=environ["SNOWFLAKE_ACCOUNT"],
        database="observes",
        warehouse="GENERIC_COMPUTE",
    )
    try:
        cursor = connection.cursor()
        try:
            yield cursor
        finally:
            cursor.close()
    finally:
        connection.close()


def prepare_connection(cursor: Any) -> None:
    cursor.execute("USE ROLE sorts")
    cursor.execute("USE DATABASE observes")
    cursor.execute("USE WAREHOUSE GENERIC_COMPUTE")


def fetch_data(query: str, parameters: tuple = ()) -> list:
    with db_cursor() as cursor:
        prepare_connection(cursor)
        cursor.execute(query, parameters)
        return cursor.fetchall()


def delete(table: str, condition: str = "true") -> None:
    with db_cursor() as cursor:
        prepare_connection(cursor)
        cursor.execute(f"DELETE FROM sorts.{table} WHERE {condition}")


def insert(table: str, item: Item) -> None:
    query_columns: str = ",\n".join(item.keys())
    query_values: str = ",\n".join(f"%({key})s" for key in item.keys())
    with db_cursor() as cursor:
        prepare_connection(cursor)
        cursor.execute(
            f"""
                INSERT INTO sorts.{table} (
                    timestamp,
                    {query_columns}
                )
                VALUES (
                    getdate(),
                    {query_values}
                )
            """,
            item,
        )
