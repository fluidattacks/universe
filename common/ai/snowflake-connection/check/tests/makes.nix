{ makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/snowflake-connection";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  check = bundle.check.types;
in {
  jobs."/common/ai/snowflake-connection/check/tests" = makeScript {
    searchPaths = { bin = [ check ]; };
    name = "snowflake-connection-check-tests";
    entrypoint = "";
  };
}
