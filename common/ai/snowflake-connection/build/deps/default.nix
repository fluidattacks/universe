{ makes_inputs, nixpkgs, python_version, }:
let
  lib = {
    buildEnv = nixpkgs."${python_version}".buildEnv.override;
    inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
    inherit (nixpkgs.python3Packages) fetchPypi;
  };
  utils = makes_inputs.pythonOverrideUtils;
  python_pkgs = utils.compose [ ] nixpkgs."${python_version}Packages";
in { inherit lib python_pkgs; }
