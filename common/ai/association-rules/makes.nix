{ fetchNixpkgs, makeScript, makeTemplate, outputs, projectPath, ...
}@makes_inputs:
let
  root = projectPath "/common/ai/association-rules";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  base_job_name = "/common/ai/association-rules";
in {
  jobs = {
    "${base_job_name}/bin" = makeScript {
      searchPaths = {
        bin = [ bundle.env.runtime ];
        source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
      };
      name = "association-rules-bin";
      entrypoint = ''association-rules "''${@}"'';
    };
    "${base_job_name}/env/dev" = makeTemplate {
      name = "association-rules-env-dev";
      searchPaths = { bin = [ bundle.env.dev ]; };
      withAction = false;
    };
    "${base_job_name}/env/runtime" = makeTemplate {
      name = "association-rules-env-runtime";
      searchPaths = { bin = [ bundle.env.runtime ]; };
      withAction = false;
    };
  };
}
