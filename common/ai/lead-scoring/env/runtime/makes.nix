{ fetchNixpkgs, makeTemplate, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/lead-scoring";
  pkg = import "${root}/entrypoint.nix" makes_inputs;
  env = pkg.env.runtime;
in {
  jobs."/common/ai/lead-scoring/env/runtime" = makeTemplate {
    name = "lead-scoring-env-runtime";
    searchPaths = { bin = [ env ]; };
  };
}
