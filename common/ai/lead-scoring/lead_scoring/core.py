import os
from dataclasses import (
    dataclass,
)
from pathlib import (
    Path,
)
from typing import Any

import boto3
import pandas as pd
import snowflake.connector
from cryptography.hazmat.backends import (
    default_backend,
)
from cryptography.hazmat.primitives import (
    serialization,
)
from snowflake.connector import (
    SnowflakeConnection,
)
from snowflake.connector.cursor import (
    SnowflakeCursor,
)

import lead_scoring.src.contact_inference_model as contact_inference
import lead_scoring.src.leads_inference_model as lead_inference

_cache: dict[str, SnowflakeConnection] = {}


@dataclass(frozen=True)
class SnowflakeCredentials:
    user: str
    private_key: str
    account: str

    def __repr__(self) -> str:
        return "[MASKED]"

    def __str__(self) -> str:
        return "[MASKED]"


@dataclass(frozen=True)
class TempCredsUserSF:
    database: str
    warehouse: str
    role: str


def get_snowflake_creds() -> SnowflakeCredentials:
    """Snowflake credentials from environment"""
    print("Using Snowflake credentials")
    private_key = os.environ["SNOWFLAKE_PRIVATE_KEY"]
    if not private_key:
        raise ValueError("Snowflake private key not found in environment variables")
    return SnowflakeCredentials(
        account=os.environ["SNOWFLAKE_ACCOUNT"],
        private_key=private_key,
        user=os.environ["SNOWFLAKE_USER"],
    )


def new_snowflake_connection() -> tuple[SnowflakeConnection, SnowflakeCursor]:
    creds = get_snowflake_creds()
    _conf = TempCredsUserSF("OBSERVES", "GENERIC_COMPUTE", "ANALYTICS_READER")
    private_key = serialization.load_pem_private_key(
        creds.private_key.encode("utf-8"),
        password=None,
        backend=default_backend(),
    )
    pkb = private_key.private_bytes(
        encoding=serialization.Encoding.DER,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
    )

    con = snowflake.connector.connect(
        user=creds.user,
        private_key=pkb,
        account=creds.account,
        database=_conf.database,
        warehouse=_conf.warehouse,
    )
    cur = con.cursor()
    cur.execute(f"USE ROLE {_conf.role}")
    cur.execute(f"USE WAREHOUSE {_conf.warehouse}")
    cur.execute(f"USE DATABASE {_conf.database}")

    return (con, cur)


def query_to_dataframe(con: Any, query: str) -> pd.DataFrame:
    df = pd.read_sql_query(query, con)
    return df


def upload_csv_to_s3(file_name: str, bucket: str, s3_key: str) -> bool:
    s3_client = boto3.client("s3")
    s3_client.upload_file(file_name, bucket, s3_key)
    return True


def process_inference(
    model_query: str,
    inference_function: Any,
    label: str,
    extract_s3: bool,
    filename_output: str,
) -> None:
    path = str(Path(__file__).parent / Path(model_query))
    print(f"Processing {label}...")
    print(f"Loading query from: {path}")

    with open(path, encoding="UTF-8") as file:
        query = file.read()
    print("SQL query loaded successfully")

    con, _ = new_snowflake_connection()
    print("Connection to the database established successfully")

    df = query_to_dataframe(con, query)

    print(f"Executing {label} inference...")
    inference_function(df, con)
    print(f"{label} inference process completed successfully")

    if extract_s3:
        file_name = filename_output  # "lead_scoring_inference.csv"
        print(file_name)
        bucket_name = "business-models"
        s3_key = f"lead-scoring/inferences/{file_name}"

        upload_csv_to_s3(file_name, bucket_name, s3_key)
        print(f"File {file_name} successfully uploaded to {bucket_name}/{s3_key}")


def get_model_query() -> str:
    with open("lead_scoring_filter_query.txt", "r+") as file:
        flag = file.read().strip()
        if flag == "1":
            file.seek(0)
            file.write("0")
            file.truncate()
            return "leads_to_inference_all.sql"

    return "leads_to_inference.sql"


def main() -> None:
    model_query = get_model_query()

    process_inference(
        model_query=model_query,
        inference_function=lead_inference.leads_inference,
        label="Leads",
        extract_s3=True,
        filename_output=lead_inference.output_path,
    )

    process_inference(
        model_query="contacts_to_inference.sql",
        inference_function=contact_inference.contacts_inference,
        label="Contacts",
        extract_s3=True,
        filename_output=contact_inference.output_path,
    )
