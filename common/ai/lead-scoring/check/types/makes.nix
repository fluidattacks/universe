{ fetchNixpkgs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/lead-scoring";
  pkg = import "${root}/entrypoint.nix" makes_inputs;
  check = pkg.check.types;
in {
  jobs."/common/ai/lead-scoring/check/types" = makeScript {
    searchPaths = { bin = [ check ]; };
    name = "lead-scoring-check-types";
    entrypoint = "";
  };
}
