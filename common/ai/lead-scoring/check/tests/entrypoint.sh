# shellcheck shell=bash

function main {
  local pytest_args=(
    ./tests
    --cov 'lead-scoring'
    --cov-branch
    --cov-report 'term'
    --cov-report 'xml:coverage.xml'
    --disable-warnings
    --no-cov-on-fail
    -vvl
  )

  pushd common/ai/lead-scoring || return 1
  pytest "${pytest_args[@]}"
}

main "${@}"
