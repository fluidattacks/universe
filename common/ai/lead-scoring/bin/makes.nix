{ inputs, fetchNixpkgs, makeScript, outputs, projectPath, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [ service.success_indicators.root ];
  root = projectPath "/common/ai/lead-scoring";
  pkg = import "${root}/entrypoint.nix" makes_inputs;
  env = pkg.env.runtime;
  deps_env = builtins.map inputs.getRuntime deps;
in {
  jobs."/common/ai/lead-scoring/bin" = makeScript {
    searchPaths = {
      bin = deps_env ++ [ env ];
      export = [
        [ "SECRETS_FILE" (projectPath "/common/ai/secrets/prod.yaml") "" ]
        [ "AWS_DEFAULT_REGION" "us-east-1" "" ]
      ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/observes/common/db-creds"
      ];

    };
    name = "lead-scoring-bin";
    entrypoint = ./entrypoint.sh;
  };
}
