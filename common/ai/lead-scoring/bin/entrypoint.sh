# shellcheck shell=bash

: \
  && sops_export_vars 'common/ai/secrets/prod.yaml' \
    SNOWFLAKE_PRIVATE_KEY \
    SNOWFLAKE_USER \
    SNOWFLAKE_ACCOUNT \
  && lead-scoring "${@}"
