{ makeSearchPaths, outputs, ... }: {
  dev = {
    sorts = { source = [ outputs."/common/ai/sorts/env/dev" ]; };
    snowflakeConnection = {
      source = [ outputs."/common/ai/snowflake-connection/env/dev" ];
    };
    sortsTraining = {
      source = [ outputs."/common/ai/sorts-training/env/dev" ];
    };
  };
}
