{ makeScript, outputs, ... }: {
  jobs."/common/ai/jobs/extract-features" = makeScript {
    name = "sorts-extract-features";
    searchPaths = {
      bin = [ outputs."/common/ai/sorts/bin" ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/common"
        outputs."/observes/common/list-groups"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
