{ makeScript, outputs, ... }: {
  jobs."/common/ai/jobs/sorts-execute/test" = makeScript {
    name = "sorts-execute-test";
    searchPaths = {
      bin = [ outputs."/common/ai/sorts/bin" ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/common"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
