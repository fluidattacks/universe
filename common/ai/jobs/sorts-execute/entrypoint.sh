# shellcheck shell=bash

function execute {
  local group="${1}"

  echo "[INFO] Running sorts on group ${group}:" \
    && if sorts --mode sub "groups/${group}"; then
      echo "[INFO] Successfully executed on: ${group}"
    else
      echo "[ERROR] While running Sorts on: ${group}"
    fi \
    && rm -rf "groups/${group}"
}

function main {
  local parallel="${1}"
  local groups_file
  local sorted_groups_file

  : \
    && sops_export_vars 'common/ai/secrets/prod.yaml' \
      'FERNET_TOKEN' \
      'SNOWFLAKE_ACCOUNT' \
      'SNOWFLAKE_PRIVATE_KEY' \
      'SNOWFLAKE_USER' \
    && aws_login "dev" "3600" \
    && groups_file="$(mktemp)" \
    && sorted_groups_file="$(mktemp)" \
    && list_groups "${groups_file}" \
    && sort "${groups_file}" -o "${sorted_groups_file}" \
    && execute_chunk_parallel execute "${sorted_groups_file}" "${parallel}" "batch"

}

main "${@}"
