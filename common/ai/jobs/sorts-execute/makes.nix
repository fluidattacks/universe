{ makeScript, outputs, ... }: {
  imports = [ ./test/makes.nix ];
  jobs."/common/ai/jobs/sorts-execute" = makeScript {
    name = "sorts-execute";
    searchPaths = {
      bin = [ outputs."/common/ai/sorts/bin" ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/common"
        outputs."/observes/common/list-groups"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
