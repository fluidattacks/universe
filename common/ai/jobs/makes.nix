{
  imports = [
    ./extract-features/makes.nix
    ./merge-features/makes.nix
    ./sorts-execute/makes.nix
  ];
}
