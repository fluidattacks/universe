{ makeScript, inputs, outputs, ... }: {
  jobs."/common/ai/jobs/merge-features" = makeScript {
    name = "sorts-merge-features";
    searchPaths = {
      bin = [ outputs."/common/ai/sorts/bin" ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
