import asyncio
from http import HTTPStatus
import os
import aiohttp
import requests


ZOHO_DESK_ARTICLES_BASE_URL = "https://desk.zoho.com/api/v1/articles"
ZOHO_TOKEN_BASE_URL = "https://accounts.zoho.com/oauth/v2/token"


def revoke_zoho_access_token(token: str) -> None:
    revoke_params = {"token": f"{token}"}
    request = requests.post(url = f"{ZOHO_TOKEN_BASE_URL}/revoke", params = revoke_params)
    data = request.json()
    print(f"Token revoke result: {data['status']}")


async def get_zoho_article(
    semaphore: asyncio.Semaphore,
    session: aiohttp.ClientSession,
    article_id: str,
    access_token: str
) -> tuple[str, str] | None:
    article_header = {
        "Authorization" : f"Zoho-oauthtoken {access_token}",
    }
    try:
        async with semaphore, session.get(
            url = f"{ZOHO_DESK_ARTICLES_BASE_URL}/{article_id}",
            headers = article_header
        ) as response:
            if response.status == HTTPStatus.OK:
                data = await response.json()
                return (article_id, data["answer"])
            else:
                print("Failed to fetch article %s: Response %s", article_id, response.status)
    except ConnectionError as exc:
        print(f"Failed to fetch article {article_id} - ConnectionError: {exc}")

    return None


async def get_all_zoho_article_ids(
    semaphore: asyncio.Semaphore,
    session: aiohttp.ClientSession,
    access_token: str,
    from_param: str,
) -> list[str]:
    article_list_header = {
        "Authorization" : f"Zoho-oauthtoken {access_token}",
    }
    all_article_ids: list[str] = []
    try:
        async with semaphore, session.get(
            url=ZOHO_DESK_ARTICLES_BASE_URL,
            headers=article_list_header,
            params={
                "from": from_param,
                "limit": 50,
                "sortBy": "createdTime",
                "status": "Published",
            }
        ) as response:
            if response.status == HTTPStatus.OK:
                data = await response.json()
                for item in data["data"]:
                    all_article_ids.append(item["id"])
                return all_article_ids
            else:
                print("Failed to fetch articles - Response", response.status)
    except ConnectionError as exc:
        print(f"Failed to fetch articles list - ConnectionError: {exc}")

    return []


def get_zoho_article_count(access_token: str) -> str:
    url = f"{ZOHO_DESK_ARTICLES_BASE_URL}/count"
    headers = {
        "Authorization" : f"Zoho-oauthtoken {access_token}",
    }
    params = {
        "status": "Published",
    }
    request = requests.get(url = url, headers=headers, params=params)
    data = request.json()

    return data["count"]


def get_zoho_access_token() -> str:
    token_params = {
        "refresh_token": os.environ["ZOHO_DESK_REFRESH_TOKEN"],
        "client_id": os.environ["ZOHO_DESK_CLIENT_ID"],
        "client_secret": os.environ["ZOHO_DESK_CLIENT_SECRET"],
        "scope": "Desk.basic.READ,Desk.articles.READ",
        "grant_type": "refresh_token",
    }
    request = requests.post(url = ZOHO_TOKEN_BASE_URL, params = token_params)
    data = request.json()

    return data["access_token"]
