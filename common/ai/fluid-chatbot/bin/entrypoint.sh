# shellcheck shell=bash

: \
  && sops_export_vars 'common/ai/secrets/prod.yaml' \
    OPENAI_API_KEY \
    ZOHO_DESK_CLIENT_ID \
    ZOHO_DESK_CLIENT_SECRET \
    ZOHO_DESK_REFRESH_TOKEN \
  && fluid-chatbot "${@}"
