{ makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/fluid-chatbot";
  pkg = import "${root}/entrypoint.nix" makes_inputs;
  check = pkg.check.types;
in {
  jobs."/common/ai/fluid-chatbot/check/types" = makeScript {
    searchPaths = { bin = [ check ]; };
    name = "fluid-chatbot-check-types";
    entrypoint = "";
  };
}
