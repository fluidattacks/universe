{
  imports = [
    ./association-rules/makes.nix
    ./dev/makes.nix
    ./fluid-chatbot/makes.nix
    ./jobs/makes.nix
    ./lead-scoring/makes.nix
    ./snowflake-connection/makes.nix
    ./sorts/makes.nix
    ./sorts-training/makes.nix
  ];
  inputs.nixpkgs-ai = let
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "f74d88a6f19ec8c29bff5cc9e42ee5ec4e0e70f6";
    src = builtins.fetchTarball {
      sha256 = "0mwhisr0gzxj3qjakiimvmzmjnkhayb3y51alddl4d1ar8b4rhb5";
      url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
    };
  in import src { };
  secretsForAwsFromGitlab.prodSorts = {
    roleArn = "arn:aws:iam::205810638802:role/prod_sorts";
    duration = 3600;
  };
}
