{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/sorts";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/common/ai/sorts/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "sorts-dev";
  };
}
