from _pytest.logging import (
    LogCaptureFixture,
)
import os
import pandas as pd
from pandas import (
    DataFrame,
)
import pytest
from sorts.features.file import (
    encode_extensions,
    extract_features,
    get_num_commits,
    get_num_lines,
    get_risky_commits,
    get_seldom_contributors,
    get_unique_authors_count,
)
from sorts.typings import (
    GitMetrics,
)

DATA_PATH: str = f"{os.path.dirname(__file__)}/data"

GIT_METRICS_MOCK: GitMetrics = {
    "author_email": [
        "john@example.com",
        "alice@example.com",
        "bob@example.com",
        "bob@example.com",
        "bob@old-domain.com",
    ],
    "commit_hash": ["abcdef1", "1234567", "a1b2c3d4", "abcdef2", "abcxyz"],
    "date_iso_format": [
        "2024-02-01T10:00:00Z",
        "2024-02-02T12:00:00Z",
        "2024-02-03T08:00:00Z",
        "2024-02-04T08:00:00Z",
        "2024-02-05T08:00:00Z",
    ],
    "current_filepath": "/path/to/current/file.py",
    "stats": [
        "5 files changed, 10 insertions (+), 200 deletions (-)",
        "7 files changed, 150 insertions (+), 50 deletions (-)",
        "3 files changed, 8 insertions (+), 2 deletions (-)",
        "9 files changed, 210 insertions (+), 5 deletions (-)",
        "9 files changed, 0 insertions (+), 5 deletions (-)",
        "1 files changed, 1 insertions (+), 5 deletions (-)",
    ],
}


@pytest.mark.usefixtures("test_clone_repo")
def test_bad_dataframe(caplog: LogCaptureFixture) -> None:
    training_df: DataFrame = pd.read_csv(
        os.path.join(DATA_PATH, "test_repo_files.csv")
    )
    extract_features(training_df)
    assert "Exception: KeyError" in caplog.text


def test_encode_extensions() -> None:
    training_df: DataFrame = pd.DataFrame(
        ["py", "java", "md", "cs", "go"], columns=["extension"]
    )
    encode_extensions(training_df)
    assert training_df.loc[0].values.tolist() == [
        "py",
        1,
        0,
        0,
        0,
        0,
        0,
        1,
        0,
        0,
        1,
    ]
    assert training_df.loc[1].values.tolist() == [
        "java",
        0,
        1,
        0,
        1,
        0,
        0,
        1,
        0,
        0,
        0,
    ]
    assert training_df.loc[2].values.tolist() == [
        "md",
        0,
        1,
        1,
        0,
        0,
        0,
        1,
        1,
        0,
        0,
    ]
    assert training_df.loc[3].values.tolist() == [
        "cs",
        0,
        0,
        0,
        1,
        1,
        1,
        1,
        1,
        1,
        0,
    ]
    assert training_df.loc[4].values.tolist() == [
        "go",
        0,
        0,
        1,
        1,
        1,
        1,
        1,
        0,
        0,
        1,
    ]


def test_get_risky_commits() -> None:
    expected_result = 2

    result = get_risky_commits(GIT_METRICS_MOCK)
    assert result == expected_result


def test_get_seldom_contributors() -> None:
    expected_result = 3

    result = get_seldom_contributors(GIT_METRICS_MOCK)
    assert result == expected_result


def test_get_unique_authors_count() -> None:
    expected_result = 3
    result: int = get_unique_authors_count(GIT_METRICS_MOCK)
    assert result == expected_result


def test_get_num_commits() -> None:
    expected_result = 5

    result = get_num_commits(GIT_METRICS_MOCK)
    assert result == expected_result


def test_get_num_lines_with_exception() -> None:
    get_num_lines("file-not-found-path")
