{ lib, python_pkgs, }:
lib.buildPythonPackage rec {
  pname = "types-jsonschema";
  version = "4.23.0.20240813";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "yT9IIG8gmlvEYI0pWsOfFy+5i54kFZzld9vSXdt5ocA=";
  };
  doCheck = false;
  propagatedBuildInputs = with python_pkgs; [ referencing setuptools ];
  format = "pyproject";
}
