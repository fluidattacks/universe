{ lib, python_pkgs, }:
lib.buildPythonPackage rec {
  pname = "joblib_stubs";
  version = "1.4.2.4.20240825";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "27tZV/qDZdyLAekWq2WNs3eDJjt/3G2S6MpruIdcPnI=";
  };
  doCheck = false;
  propagatedBuildInputs = with python_pkgs; [ hatchling typing-extensions ];
  format = "pyproject";
}
