{ lib, python_pkgs, }:
let version = "3.9.1";
in python_pkgs.bugsnag.overridePythonAttrs (old: {
  inherit version;
  src = lib.fetchPypi {
    pname = "bugsnag";
    inherit version;
    hash = "sha256-kh02pglLRqCoGQdoJj3xPLlDTvHmiGoPiX8HksSmOrY=";
  };
  propagatedBuildInputs = with python_pkgs; [ six webob ];
})
