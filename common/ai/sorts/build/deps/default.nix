{ makes_inputs, nixpkgs, python_version, }:
let
  lib = {
    buildEnv = nixpkgs."${python_version}".buildEnv.override;
    inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
    inherit (nixpkgs.python3Packages) fetchPypi;
  };

  utils = makes_inputs.pythonOverrideUtils;

  layer_1 = python_pkgs:
    python_pkgs // {
      scikit-learn = import ./scikit_learn.nix { inherit lib python_pkgs; };
    };
  layer_2 = python_pkgs:
    python_pkgs // {
      bugsnag = import ./bugsnag.nix { inherit lib python_pkgs; };
      category-encoders =
        import ./category_encoders.nix { inherit lib python_pkgs; };
      coralogix-logger =
        import ./coralogix_logger.nix { inherit lib python_pkgs; };
      joblib-stubs =
        import ./type-stubs/joblib_stubs.nix { inherit lib python_pkgs; };
      pandas-stubs =
        import ./type-stubs/pandas_stubs.nix { inherit lib python_pkgs; };
      shap = import ./shap.nix { inherit lib python_pkgs; };
      snowflake-connection = let
        result = import ./snowflake_connection.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
      types-jsonschema =
        import ./type-stubs/types_jsonschema.nix { inherit lib python_pkgs; };
    };

  python_pkgs =
    utils.compose [ layer_2 layer_1 ] nixpkgs."${python_version}Packages";
in { inherit lib python_pkgs; }
