{ lib, python_pkgs, }:
let version = "0.45.0";
in python_pkgs.shap.overridePythonAttrs (old: {
  inherit version;
  src = lib.fetchPypi {
    pname = "shap";
    inherit version;
    hash = "sha256-vo/8ITuj2hznlQeEVxNW5sp3vulERt36nGPrLHL3zp4=";
  };
  propagatedBuildInputs = with python_pkgs; [
    scipy
    scikit-learn
    pandas
    tqdm
    slicer
    numba
    cloudpickle
    setuptools-scm
  ];
})
