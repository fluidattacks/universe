{ lib, python_pkgs, }:
lib.buildPythonPackage rec {
  pname = "category_encoders";
  version = "2.6.2";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "8Bq6qOb2MHVtZ7gcvqZzASt3WaeP2q2josY/AgMzpdY=";
  };
  pythonImportsCheck = [ pname ];
  checkInputs = with python_pkgs; [ ];
  propagatedBuildInputs = with python_pkgs; [
    patsy
    pandas
    numpy
    scikit-learn
    scipy
    statsmodels
  ];
}
