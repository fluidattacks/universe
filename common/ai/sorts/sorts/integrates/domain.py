from concurrent.futures import (
    ThreadPoolExecutor,
)
from functools import (
    partial,
)
from sorts.integrates.snowflake_dal import (
    get_findings,
    get_group_roots,
    get_group_toe_lines,
    get_vulnerabilities,
)
from sorts.integrates.typing import (
    Vulnerability,
    VulnerabilityKindEnum,
)
from sorts.utils.logs import (
    log,
)
import time


def get_unique_vuln_files(group: str) -> list[str]:
    """Removes repeated files from group vulnerabilities"""
    vulnerable_filenames: list[str] = [
        vuln.where
        for vuln in get_vulnerable_lines(group)
        if vuln.kind.value == VulnerabilityKindEnum.LINES.value
    ]
    unique_vuln_filenames = set(vulnerable_filenames)

    return sorted(unique_vuln_filenames)


def get_vulnerable_files(
    group: str,
) -> list[str]:
    """Gets vulnerable files to fill the training DataFrame"""
    timer: float = time.time()
    vuln_toe_lines: list[str] = []
    group_roots = get_group_roots(group)
    group_toe_lines = get_group_toe_lines(group, group_roots)
    unique_vuln_files = get_unique_vuln_files(group)
    for vuln_file in unique_vuln_files:
        if vuln_file in group_toe_lines:
            vuln_toe_lines.append(vuln_file)
            group_toe_lines.pop(vuln_file)
    for toeline in group_toe_lines:
        if any(toeline in file for file in unique_vuln_files):
            vuln_toe_lines.append(toeline)
    log(
        "info",
        "Vulnerable files extracted after %.2f seconds",
        time.time() - timer,
    )
    return vuln_toe_lines


def get_vulnerable_lines(group: str) -> list[Vulnerability]:
    """Fetches the vulnerable files from a group"""
    vulnerabilities: list[Vulnerability] = []
    findings = get_findings(group)
    with ThreadPoolExecutor(max_workers=8) as executor:
        for finding_vulnerabilities in executor.map(
            partial(get_vulnerabilities), findings
        ):
            vulnerabilities.extend(finding_vulnerabilities)

    return vulnerabilities
