from collections.abc import (
    Iterable,
)
import os
import pandas as pd
from pandas import (
    DataFrame,
)
import shutil
from sorts.features.file import (
    extract_features,
)
from sorts.integrates.domain import (
    get_vulnerable_files,
)
from sorts.integrates.snowflake_dal import (
    get_group_roots,
    get_group_toe_lines,
)
from sorts.integrates.typing import (
    ToeLines,
)
from sorts.utils.logs import (
    log,
)
from sorts.utils.repositories import (
    download_repo,
    is_working_repo,
)
from sorts.utils.static import (
    filter_third_party_files,
    read_allowed_names,
)
import time

# Constants
FILE_MAX_RETRIES: int = 15


def _create_dataframe(files: list[str], is_vuln: int) -> DataFrame:
    """Creates a DataFrame from a list of files"""
    return pd.DataFrame(
        map(lambda x: (x, is_vuln), files), columns=["file", "is_vuln"]
    )


def _filter_allowed_files(files: Iterable[str]) -> list[str]:
    in_scope_files: list[str] = []
    extensions, composites = read_allowed_names()
    for file in files:
        filename: str = os.path.basename(file)
        file_extension: str = os.path.splitext(filename)[1].strip(".")
        if file_extension in extensions or filename in composites:
            in_scope_files.append(file)

    return filter_third_party_files(in_scope_files)


def build_training_df(group: str, subscription_path: str) -> DataFrame:
    """Creates a training DataFrame with vulnerable and safe files"""
    vuln_files: list[str] = get_vulnerable_files(group)
    safe_files = get_safe_files(
        vuln_files,
        subscription_path,
    )
    allowed_vuln_files = _filter_allowed_files(vuln_files)
    allowed_safe_files = _filter_allowed_files(safe_files)

    training_df = pd.concat(
        [
            _create_dataframe(allowed_vuln_files, 1),
            _create_dataframe(allowed_safe_files, 0),
        ]
    )
    training_df["repo"] = training_df["file"].apply(
        lambda filename: os.path.join(
            subscription_path, filename.split(os.path.sep)[0]
        )
    )
    training_df.reset_index(drop=True, inplace=True)
    return training_df


def _extract_features_from_df(
    training_df_byrepo: list[DataFrame],
    subscription_path: str,
) -> DataFrame:
    """Extracts features from a list of DataFrames"""
    group_name = os.path.basename(os.path.normpath(subscription_path))
    features_df_list = []
    for training_df in training_df_byrepo:
        repo_name: str = os.path.basename(
            training_df.reset_index(drop=True)["repo"][0]
        )
        download_repo(group_name, repo_name)
        repo_path = os.path.join(subscription_path, repo_name)
        features_df_list.append(
            extract_features(training_df.reset_index(drop=True))
        )
        if os.path.exists(repo_path):
            shutil.rmtree(repo_path)
    return pd.concat(features_df_list).reset_index(drop=True)


def _save_features_to_csv(features_df: DataFrame, group: str) -> None:
    """Saves the features DataFrame to a CSV file"""
    csv_name: str = f"{group}_files_features.csv"
    features_df.to_csv(csv_name, index=False)
    log("info", "Features extracted successfully to %s", csv_name)


def get_subscription_file_metadata(subscription_path: str) -> None:
    """Creates a CSV with the file features from the subscription"""
    group_name: str = os.path.basename(os.path.normpath(subscription_path))
    subscription_training_df: DataFrame = build_training_df(
        group_name, subscription_path
    )
    excluded_repos = [
        repo
        for repo in subscription_training_df.repo.unique()
        if not is_working_repo(os.path.basename(repo))
    ]
    subscription_training_df = subscription_training_df[
        subscription_training_df["repo"].map(lambda x: x not in excluded_repos)
    ]
    if subscription_training_df.empty:
        log(
            "warning",
            'Group %s does not have any vulnerabilities of type "lines"',
            group_name,
        )
        return
    training_df_byrepo = [
        repo_training_df
        for _, repo_training_df in subscription_training_df.groupby("repo")
    ]
    features_df: DataFrame = _extract_features_from_df(
        training_df_byrepo, subscription_path
    )
    if features_df.empty:
        log(
            "warning",
            "The roots from group %s cannot be downloaded",
            group_name,
        )
        return
    _save_features_to_csv(features_df, group_name)


def _log_time(timer: float) -> None:
    log(
        "info",
        "Safe files extracted after %.2f seconds",
        time.time() - timer,
    )


def get_safe_files(
    vuln_files: Iterable[str],
    subscription_path: str,
) -> list[str]:
    """Fetches random files that do not have any vulnerability reported"""
    timer: float = time.time()
    group_name = subscription_path.split("/")[-1]
    group_roots = get_group_roots(group_name)
    group_toe_lines: dict[str, ToeLines] = get_group_toe_lines(
        group_name, group_roots
    )
    attacked_files = [
        filename
        for filename, toeline in group_toe_lines.items()
        if toeline.attacked_lines == toeline.loc
    ]
    verified_safe_files = [
        filename for filename in attacked_files if filename not in vuln_files
    ]
    _log_time(timer)
    return verified_safe_files
