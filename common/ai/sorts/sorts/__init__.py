"""
File prioritizer according to the likelihood of finding a vulnerability

.. include:: ../README.md
"""
__version__ = "1.0.0"
