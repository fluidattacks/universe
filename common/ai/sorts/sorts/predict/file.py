import os
import pandas as pd
from pandas import (
    DataFrame,
)
import shutil
from sorts.constants import (
    S3_BUCKET,
)
from sorts.features.file import (
    extract_features,
)
from sorts.integrates.snowflake_dal import (
    get_group_roots,
)
from sorts.utils.logs import (
    log,
)
from sorts.utils.predict import (
    build_results,
    display_results,
    predict_vuln_prob,
)
from sorts.utils.repositories import (
    download_repo,
    get_repository_files,
)
from sorts.utils.static import (
    filter_third_party_files,
    get_extensions_list,
    read_allowed_names,
)
from tqdm import (
    tqdm,
)

INTERNAL_EXTENSIONS = [
    "py", "java", "go", "js", "jsx", "json", "ts", "tsx", "php", "phtml", "rb", "erb", "rake",
    "cs", "scala", "sc", "dart", "kt", "kts", "swift",
]

def get_repo_files_df(repo_path: str, internal: bool = False) -> DataFrame:
    """Build the basic DF with all the files from one repository"""
    files: list[str] = []
    if internal:
        extensions, composites = INTERNAL_EXTENSIONS, []
    else:
        extensions, composites = read_allowed_names()
    repo_files = get_repository_files(repo_path)
    filtered_repo_files = filter_third_party_files(repo_files)
    allowed_files = list(
        filter(
            lambda x: (
                x.split("/")[-1] in composites
                or x.split(".")[-1].lower() in extensions
            ),
            filtered_repo_files,
        )
    )
    if allowed_files:
        files.extend(allowed_files)
    files_df: DataFrame = pd.DataFrame(files, columns=["file"])
    files_df["repo"] = repo_path
    return files_df


def get_prediction_result(
    predict_df: DataFrame, *, enable_risky_features: bool = True
) -> DataFrame:
    extensions: list[str] = get_extensions_list()
    num_bits: int = len(extensions).bit_length()
    results_df: DataFrame = predict_vuln_prob(
        predict_df,
        [f"extension_{num}" for num in range(num_bits)],
        enable_risky_features,
    )
    return results_df


def filter_commit_files(
    repo_df: DataFrame, commit_file_paths: list[str]
) -> DataFrame:
    commit_files_rows = []
    for _, row in repo_df.iterrows():
        if any(
            commit_file_path in row["file"]
            for commit_file_path in commit_file_paths
        ):
            commit_files_rows.append(row)
    return pd.DataFrame(commit_files_rows)


def prioritize_repo(repo_path: str, out: str) -> bool:
    """
    Prioritizes files in a single repo
    according to vulnerability probability
    """
    success: bool = False
    repo_files_df: DataFrame = get_repo_files_df(repo_path)
    features_df = extract_features(repo_files_df, encrypt_filenames=False)
    if not features_df.empty:
        results_df = get_prediction_result(features_df)
        display_results(results_df)
        repo_name: str = os.path.basename(os.path.normpath(repo_path))
        if out == "json":
            json_name = f"{repo_name}_sorts_results_file.json"
            build_results(results_df, json_name)
        else:
            csv_name = f"{repo_name}_sorts_results_file.csv"
            build_results(results_df, csv_name)
        success = True

    return success


def prioritize_subscription(subscription_path: str) -> bool:
    """Prioritizes files according to the chance of finding a vulnerability"""
    success: bool = False
    group_name: str = os.path.basename(os.path.normpath(subscription_path))
    roots = get_group_roots(group_name)
    features_df_list: list[DataFrame] = []
    for nickname in tqdm(roots.values()):
        download_repo(group_name, nickname)
        repo_path = os.path.join(subscription_path, nickname)
        repo_files_df = get_repo_files_df(repo_path, internal=True)
        if not repo_files_df.empty:
            features_df_list.append(extract_features(repo_files_df))
            shutil.rmtree(repo_path)
    try:
        features_df = pd.concat(features_df_list).reset_index(drop=True)
    except ValueError:
        features_df = pd.DataFrame()
    if not features_df.empty:
        results_df = get_prediction_result(
            features_df, enable_risky_features=False
        )
        csv_name: str = f"{group_name}_sorts_results_file.csv"
        build_results(results_df, csv_name)
        S3_BUCKET.Object(f"sorts-execution-results/{csv_name}").upload_file(
            csv_name
        )
        display_results(results_df)
        success = True
    else:
        log(
            "warning",
            "Could not find any files to analyze",
        )

    return success
