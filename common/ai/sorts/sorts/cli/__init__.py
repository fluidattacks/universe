import click
import os
from snowflake_connection import (
    db as snowflake,
)
from sorts.predict.file import (
    prioritize_repo,
    prioritize_subscription,
)
from sorts.training.file import (
    get_subscription_file_metadata,
)
from sorts.utils.bugs import (
    configure_bugsnag,
)
from sorts.utils.logs import (
    log_exception,
)
from sorts.utils.merge_features import (
    merge_features,
)
import sys
import time


class UnexpectedError(Exception):
    pass


def _send_analytics(
    start_time: float,
    subject: str,
) -> None:
    execution_time: float = time.time() - start_time
    snowflake.insert(
        "executions",
        {
            "group_name": subject.split("/")[-1],
            "execution_time": execution_time,
        },
    )


@click.command(
    help="File prioritizer according to the likelihood of finding "
    "a vulnerability"
)
@click.option(
    "--mode",
    type=click.Choice(
        [
            "repo",
            "sub",
            "extract-features",
            "merge-features",
        ]
    ),
    default="repo",
    show_default=True,
    help="Choose the Sorts function you want to use",
)
@click.option(
    "--out",
    type=click.Choice(["json", "csv"]),
    default="json",
    show_default=True,
    help="Choose the output type for repo scan results",
)
@click.argument(
    "subject-path",
    type=str,
)
def cli(
    mode: str,
    out: str,
    subject_path: str,
) -> None:
    configure_bugsnag()
    start_time: float = time.time()
    success: bool = False

    if mode == "repo":
        success = prioritize_repo(subject_path, out)
    elif mode == "sub":
        success = prioritize_subscription(subject_path)
        _send_analytics(start_time, subject_path)
    elif mode == "extract-features":
        get_subscription_file_metadata(subject_path)
        success = True
        _send_analytics(start_time, subject_path)
    elif mode == "merge-features":
        merge_features()
        success = True

    if not success:
        log_exception(
            "error",
            UnexpectedError(
                f"Sorts on mode {mode} could not run on group "
                f"{os.path.basename(os.path.normpath(subject_path))} "
                "due to an unexpected error"
            ),
        )
    sys.exit(0 if success else 1)


if __name__ == "__main__":
    cli()
