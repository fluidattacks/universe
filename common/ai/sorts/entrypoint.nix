{ inputs, ... }@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-ai // { inherit (inputs) nix-filter; };
  out = import ./build {
    inherit makes_inputs nixpkgs python_version;
    src = nixpkgs.nix-filter {
      root = ./.;
      include = [ "sorts" "tests" "pyproject.toml" "mypy.ini" ];
    };
  };
in out
