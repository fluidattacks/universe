resource "aws_cloudwatch_log_group" "training_jobs_log" {
  name = "/aws/sagemaker/TrainingJobs"

  tags = {
    "Name"              = "training_jobs_log"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "sorts"
  }
}
