{ fetchNixpkgs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/sorts";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  inherit (bundle.env) types;
in {
  jobs."/common/ai/sorts/check/types" = makeScript {
    searchPaths = { bin = [ types ]; };
    name = "sorts-check-types";
    entrypoint = "";
  };
}
