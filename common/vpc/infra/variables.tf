variable "cloudflare_email" {
  type = string
}
variable "cloudflare_api_key" {
  type = string
}

data "aws_caller_identity" "main" {}

data "cloudflare_ip_ranges" "cloudflare" {
  lifecycle {
    postcondition {
      condition     = length(self.cidr_blocks) > 0
      error_message = "CloudFlare's API returned empty list of IPs"
    }
  }
}
