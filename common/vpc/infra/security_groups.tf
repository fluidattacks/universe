resource "aws_security_group" "cloudflare" {
  name        = "CloudFlare"
  description = "Access to CloudFlare IP Ranges"
  vpc_id      = aws_vpc.fluid-vpc.id
  # NOFLUID This egress rule is designed to facilitate access to resources on the Internet from our AWS environment
  ingress {
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = data.cloudflare_ip_ranges.cloudflare.ipv4_cidr_blocks
    ipv6_cidr_blocks = data.cloudflare_ip_ranges.cloudflare.ipv6_cidr_blocks
    self             = true
  }
  # NOFLUID This egress rule is designed to facilitate access to resources on the Internet from our AWS environment
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = []
    self        = true
  }

  tags = {
    "Name"              = "CloudFlare"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "NOFLUID"           = "f024_ports_used_by_cloudflare"
  }
}
# NOFLUID This egress rule is designed to facilitate access to resources on the Internet from our AWS environment
resource "aws_security_group" "kinesis-endpoint-allow" {
  name        = "kinesis-endpoint-allow"
  description = "Attach this SG to allow ingress to the observes kinesis endpoint"
  vpc_id      = aws_vpc.fluid-vpc.id
  ingress     = []
  egress      = []
  tags = {
    "Name"              = "kinesisAllowSG"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
resource "aws_security_group" "kinesis-endpoint" {
  name        = "kinesis-endpoint-sg"
  description = "SG for the kinesis endpoint"
  vpc_id      = aws_vpc.fluid-vpc.id
  ingress {
    description     = "Allow https for SG kinesis-endpoint-allow"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = [aws_security_group.kinesis-endpoint-allow.id]
  }
  ingress {
    description = "Allow CI access"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      aws_subnet.main["ci_1"].cidr_block,
      aws_subnet.main["ci_2"].cidr_block,
      aws_subnet.main["ci_3"].cidr_block,
      aws_subnet.main["ci_4"].cidr_block,
      aws_subnet.main["ci_5"].cidr_block,
    ]
  }
  # NOFLUID This egress rule is designed to facilitate access to resources on the Internet from our AWS environment
  egress {
    description = "Access to kinesis endpoints"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Name"              = "kinesisEndpointSG"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
    "NOFLUID"           = "f024_Allow_egress_to_reach_anywhere"
  }
}
