{ inputs, makeScript, ... }: {
  jobs."/common/fluidattacks-core" = makeScript {
    name = "common-fluidattacks-core";
    searchPaths.bin = [ inputs.nixpkgs.poetry ];
    entrypoint = ''
      pushd common/fluidattacks-core
      poetry install

      if test -n "''${CI:-}"; then
        poetry run ruff format --config ruff.toml --diff
        poetry run ruff check --config ruff.toml
      else
        poetry run ruff format --config ruff.toml
        poetry run ruff check --config ruff.toml --fix
      fi

      poetry run lint-imports --config import-linter.cfg

      poetry run mypy --config-file mypy.ini fluidattacks_core

      poetry run pytest test
    '';
  };
}
