---
title: Contributing
description: Guidelines for contributing to the Universe code repository
---

Please submit bug reports,
feature requests,
or general feedback to our [Bug Tracker on GitLab](https://gitlab.com/fluidattacks/universe/-/issues),
or to our [Support Email](mailto:help@fluidattacks.com).

Since most of the contributions made to the project are code,
from now on this document explains the code contributions process.

## Guide for creating issues

GitLab issues are the first step in continuously
improving and correcting our products.
That is why we have developed this comprehensive
guide to provide the necessary steps to create an issue effectively.

Below,
we will explain everything you need to know when creating an issue:

### Who can create an issue?

As our open-source code,
anyone with a GitLab account can create an issue.

### Issue title

The title of an issue is essential to clearly
and concisely communicating what you want to accomplish.
It should be descriptive enough for
readers to understand it quickly.
The syntax of issue titles is as follows:

`[Product] Brief Description of the Problem`

`[Skims] Check AWS ELB listener on http`

`[Airs] Update the images of the hacking cycle on the homepage`

### Type

**Issue:** To add new features to the product.

### Description

Here,
you can select a template that best describes the issue.
The templates are as follows:

- **Bug:** Refers to a bug or error affecting
  a product's functionality or performance.
- **Feature:** Add a new functionality in one of our products.
- **Onboarding Steps:** Create a guide for new developers
  with internal and external links to support more accurate
  access to information.
- **Request for comments:** Propose and discuss new standards,
  protocols,
  or features,
  where comments,
  suggestions,
  and observations from the community are invited before
  finalizing the implementation.
- **Skim method:** Create a new Skims method.
- **Ux prototype:** Generates a proposal from a UX prototype.

### Assignees

The assignment of the person in charge of executing this issue is given.

### Labels

The purpose of these labels is to categorize and organize the issues,
providing a quick and visual way to identify the content and status
of the elements in the project.
The labels allow better management,
tracking,
and filtering of the elements,
facilitating collaboration and decision-making within the development team.
So that you know which labels to use,
we leave you the following
[link](https://gitlab.com/fluidattacks/universe/-/labels)
with their different definitions.

## Steps

Code contributions are done using the Merge Requests features on GitLab.

As the Author of the contribution,
please read the following steps
and apply them in your day to day:

1. Configure your
   [Development Environment](/getting-started/environment).

1. Make sure that your contribution has an associated issue in the bug tracker,
   or create one.

   1. Make sure that you understand the motivation behind the issue,
      the problem it is trying to solve,
      its impact, its trade-offs,
      and that it makes sense to implement it as described.

      Issues are not set in stone,
      make sure you iterate on this as many times as needed,
      and edit the issue as you go.

   1. Make sure you enumerate all of the [components](/components/)
      that will be impacted by the issue.

   1. Make sure the issue has received sufficient feedback from the Code Owners
      of the components impacted by the issue
      before starting any implementation.

   1. Don't be afraid to ping the author or the Code Owners for clarification.
      Excellent developers do excellent
      [requirement analysis](https://en.wikipedia.org/wiki/Requirements_analysis).

1. Code, and:

   1. For each of the issue's impacted components
      and their corresponding [component page](/components/):

      1. Keep their docs updated.
      1. Make sure you follow their guidelines.
      1. Make sure you don't violate their Public Oaths.
      1. Keep their architecture updated.
      1. Add any missing information to their documentation.
         We want to be able to level up and empower other developers
         to write code autonomously and with confidence,
         but we cannot do so without documentation,
         documentation is important,
         make yourself [replaceable](https://betterprogramming.pub/programmers-make-yourself-replaceable-1b08a94bf5).

   1. Make sure that your implementation is sufficiently tested:

      1. By adding automated tests to the CI/CD.
      1. By manually testing the functionality.

      Feel free to use feature flags if appropriate.

   1. Make sure that you update the [End User documentation](/),
      particularly the [Platform](https://help.fluidattacks.com/portal/en/kb/find-security-vulnerabilities/use-the-platform)
      section.

1. Open a [Merge Request](https://gitlab.com/fluidattacks/universe/-/merge_requests),
   and feel free to ping,
   assign,
   or send a direct message with the link
   to the Code Owners of the issue's impacted components.

1. Go back to step 3 until the issue is completed.

## Review process

We conduct code reviews using the Merge Requests features on GitLab,
and discussions should happen in the open,
either on the Issue,
the Merge Request,
or the team-wide communication channel.

Reviewers are selected by the Head of Product
and in general,
a reviewer reads the code,
reads the issue,
and then reviews the modified files by the Author.

A reviewer must have the following mindset
when performing a review:

1. **Transferring knowledge to the author**.

   This can range from a small code suggestion
   on how to make the code more maintainable or faster,
   to suggesting a library,
   reminding them of the guidelines,
   suggesting a way to organize the code,
   or signaling fundamental architecture/bugs/security problems
   that should be considered
   with the current approach the author is taking.

   [There are 8 quality characteristics of good software](https://iso25000.com/index.php/en/iso-25000-standards/iso-25010).
   Help the author think about all of them.

1. **The author probably knows more than the reviewer**.

   The author is the one in the field,
   touching the code,
   and seeing the problem first-hand.
   Always give the benefit of the doubt
   and start the discussion with a question,
   rather than an affirmation that things are wrong.
   There is a chance the reviewer is not seeing the full picture.

1. **Neither the reviewer nor the author has more authority**.

   We are all [Developers](/getting-started/governance).

   When proposing something make it sound like a proposal
   and not like an order.
   If what a reviewer says has value,
   the author will probably accept it and apply it right away.
   If a discussion arises,
   keep it healthy, constructive, and argument based.
   Either the author is seeing something the reviewer doesn't see yet
   or maybe the reviewer is seeing something the author doesn't see yet.
   This ["aha" moment](https://www.wordnik.com/words/aha%20moment)
   unlocks learning,
   and a safe environment to argue is key to good decision-making.

1. **Minor improvements or fixes can come later**.

   If merging a Pull Request adds more value
   than closing it,
   go ahead and merge it.
   Just take note somewhere so that the author reminds amending it later.
   Also don't be too picky,
   especially about things that are subjective like style, formatting,
   or those that are too minor
   to even pay attention to (like a typo in a comment).

A reviewer must check:

1. That the [contributing steps](#steps) have been followed,
   not only in the Merge Request, but also in the associated Issue.
1. That the Merge Request adds more value than what it takes.
   This is subjective, but the
   [8 quality characteristics of good software](https://iso25000.com/index.php/en/iso-25000-standards/iso-25010)
   are a good starting point.

A reviewer should accept a contribution if it's been made
according to this document.

## Code Coverage

[Codecov](https://about.codecov.io/) is an invaluable too for evaluating
the quality and effectiveness of unit tests in our products.
Here's a detailed guide on how to interpret
and utilize the reports it provides:

- **Accessing Reports:** Codecov coverage reports are available
  for each Pull Request and can be accessed directly form the
  GitLab user interface.
- **Coverage Interpretation:** The coverage report provides an
  overview of the code being tested by our unit tests.
  It is represented as a percentage, where 100% means
  all lines of code are being executed by our tests.
- **Identifying Untested Areas:** Examine the coverage report
  to identify code areas not adequately tested.
  These areas may be critical points requiring further attention
  in terms of unit testing.
- **Coverage Trends:** Codecov also offers insights into coverage
  trends over time.
  Use this information to evaluate whether our product's coverage
  is improving or declining over time.
- **Corrective Actions:** If areas of code with insufficient coverage
  are identified,
  collaborate with the team to implement additional unit tests and improve
  coverage in those areas.

By effectively using this tool, we can ensure the quality
and stability of our code over time.
This practice also fosters a test-focused development
culture throughout our team.

### Integration of Coverage Results in Codecov

In our development workflow,
each time tests are run in our continuous integration (CI)
environment, artifacts containing coverage results are accumulated.
These results are generated by tools such as [pytest](https://docs.pytest.org/en/8.0.x/),
[jest](https://jestjs.io/),
and [cypress](https://www.cypress.io/),
providing a detailed view of both unit and integration test coverage.

At the end of these process,
a dedicated job for each component uploads these coverage results to Codecov.
This job is presented in each of our products.
For example,
in the case of Integrates Back,
the job is located at [`integrates/back/coverage`](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/coverage?ref_type=heads).

Additionally,
Codecov provides information about coverage deltas,
showing changes in code coverage between two consecutive
test runs.
This allows us to quickly identify which code areas have
been affected by recent changes and ensure they are adequately
tested.

You can access the coverage reports and corresponding graphs
at this [link](https://app.codecov.io/gl/fluidattacks/universe).

This integration ensures that we maintain high standards of test
coverage in all our products, contributing to the overall stability
and quality of our code.
