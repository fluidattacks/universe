---
title: Introduction
description: Integrates is the product responsible for the Fluid Attacks' platform and its API
slug: components/integrates
---

Integrates is the product responsible
for the [platform](https://help.fluidattacks.com/portal/en/kb/find-security-vulnerabilities/use-the-platform)
and its [API](https://help.fluidattacks.com/portal/en/kb/articles/things-to-know-before-using-the-api).

## Public Oath

1. The platform is accessible at
   [app.fluidattacks.com](https://app.fluidattacks.com).
1. Significant changes to the user interface of the platform
   will be announced via the appropriate communication mechanism.
1. The API is accessible at
   [app.fluidattacks.com/api](https://app.fluidattacks.com).
1. A six-month notice period will be given
   for backward incompatible changes in the API.
   This includes but is not limited to:
   deprecating attributes and entities,
   making optional arguments mandatory,
   changes in the authentication or authorization system,
   and so on.
1. The Forces container
   is accessible at [DockerHub](https://hub.docker.com/r/fluidattacks/forces/tags).
1. The Retrieves extension
   is accessible at [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=FluidAttacks.fluidattacks).

## Architecture

{/* https://drive.google.com/file/d/19wk_sRRv4qLKGYEez8zEo2yMiN_pa5SV/view?usp=sharing */}

import ThemedImage from '@components/ThemedImage.astro';

<ThemedImage
  alt="Architecture"
  paths={{
    light: "v1708716920/docs/development/components/integrates/arch-light.png",
    dark: "v1708716920/docs/development/components/integrates/arch-dark.png",
  }}
/>

---

1. Integrates is a standard client-server application
   divided into:
   - A back-end.
   - A front-end.
   - `Retrieves`, a Visual Studio Code extension.
   - `Forces`, a Docker container.
1. It declares its own infrastructure using Terraform.
1. Sensitive secrets like Cloudflare authentication tokens
   are stored in encrypted YAML files
   using Mozilla SOPS.

### Back end

1. The back-end is written in typed, functional Python.
1. It uses Starlette as its main framework.
1. It uses Hypercorn as its web server.
1. It serves a GraphQL API.
1. It has three environments:
   - `Production`: The production environment used by end users.
   - `Ephemerals`:
     A testing environment for each developer accessible via the Internet.
   - `Local`:
     A testing environment developers can run on their machine.
     Instructions for this can be found [here](#local-environment).
1. There is a `Tasks` application that performs
   out-of-band processing
   for cloning client repositories.
1. DNS records, cache, custom headers, redirections and firewall
   for both production and ephemeral environments
   are managed by `Cloudflare`.
1. A [Kubernetes](/components/common/cluster/) cluster serves:
   - `Production` environment.
   - `Ephemeral` environments.
   - `Tasks` application.
1. `CloudWatch` is used for storing production logs.
1. `CloudWatch` alerts are used to check the queue size of `Tasks`.
   If the queue size goes beyond a given limit,
   email alerts are sent to developers.
1. There is one Application Load Balancer (ALB) for `Production`
   and one for each `Ephemeral` environment.
1. DynamoDB is the main database.
   It has two tables:
   - `Main` for storing all current information.
   - `Historic` for storing historical states of entities.
1. OpenSearch is a secondary search database
   that mirrors DynamoDB.
   When changes occur in DynamoDB,
   a DynamoDB stream triggers a Lambda.
   Such lambda transforms the DynamoDB data to a compatible
   OpenSearch format and then stores it there.
1. For storage several S3 buckets are used:
   - `client-repositories` stores source code repositories from clients.
   - `storage` stores blobs upload by users
     (evidences, example files, etc.)
   - `machine-executions` stores results
     of [Skims](/components/skims/) executions
     and provided configuration files.
1. The `DynamoDB` database is backed up
   using `Backup` Vaults by Amazon Web Services (AWS)
   as promised in [1](https://help.fluidattacks.com/portal/en/kb/articles/everything-is-backed-up)
   and [2](https://help.fluidattacks.com/portal/en/kb/articles/recovery-objective).
1. Out-of-band processing `Jobs` like
   ZTNA repository cloning and machine executions are performed by
   [AWS Batch](/components/common/compute/).
1. It uses `Twilio` to send SMS OTPs.
1. It uses `Sendgrid` to send email notifications to end users.
1. Web-hooks are supported so end users can
   get machine-readable notifications to their endpoints.

### Front end

1. The front-end is written in functional TypeScript.
1. It does not have a framework.
1. It uses React for building most of its web interfaces.
1. It is deployed into s3 buckets,
   using the corresponding bucket for the environment
   (`ephemeral` or `production`).
1. The back-end serves the front-end when either
   `app.fluidattacks.com` (`Production`)
   or `branch.app.fluidattacks.com` (`Ephemeral`)
   are accessed.

### Retrieves

1. The Visual Studio Code extension is written in functional TypeScript.
1. It is deployed to the Visual Studio Code Marketplace.
1. It authenticates with the back-end API using a user-generated
   token.

### Forces

1. The Docker container is written in typed, functional Python.
1. It is deployed to DockerHub.
1. It authenticates with the back-end API using a user-generated
   token.

## Contributing

Please read the
[contributing](/getting-started/contributing) page first.

### Development Environment

Configure your
[Development Environment](/getting-started/environment).

When prompted for an AWS role, choose `dev`,
and when prompted for a Development Environment, pick `integratesBack`.

### Local Environment

Two approaches for deploying
a local environment of Integrates
are described below.
Either of them will launch a replica
of [app.fluidattacks.com](https://fluidattacks.com)
and `app.fluidattacks.com/api`
on `localhost:8001`.

#### All in one

You can use [mprocs](https://github.com/pvolok/mprocs)
for handling all components in a single terminal:

- Run `m . /integrates`.
- Jobs can be restarted using `r`.
- Jobs can be stopped using `x`.

#### Individual components

Run each of the following commands within the `universe` repository
in different terminals:

```sh
m . /integrates/back dev
```

```sh
m . /integrates/db
```

```sh
m . /integrates/front
```

```sh
m . /integrates/storage/dev
```

Each terminal will serve a key component of Integrates.

#### Accessing local environment

1. Go to `https://localhost:3000`
   and accept the self-signed certificates offered by the server.

   This will allow the back-end to fetch
   the files to render the UI.

1. Go to `https://localhost:8001`
   and, again, accept the self-signed certificates offered by the server.

   Now you should see the login portal of the application.

### Ephemeral Environment

Once you upload your local changes
to your remote branch in GitLab,
a pipeline will begin
and run some verifications on your branch.

Some of those verifications
require a complete working environment
to test against.
This environment can be found
at `https://<branch_name>.app.fluidattacks.com`,
and it will be available
once the pipeline stage `deploy-app` finishes.

In order to login to your ephemeral environment,
SSO needs to be set up for it.
You can write to **[help@fluidattacks.com](mailto:help@fluidattacks.com)**
with the URL of your environment
so it can be configured.

:::note
In case you want to deploy to your ephemeral environment back-end manually,
you must first enable permissions for the `/not-set` file
(with root permissions):

```bash
touch /not-set
chmod a+rw /not-set
```

Once this file has the required permissions, you can run deployment from
your machine:

```bash
m . /integrates/back/deploy/dev
```

:::

### Enable SSO on Ephemeral Environments

#### Google

This requires you to have access
to the Fluid Attacks organization on Google Cloud.

1. Access the [Google Cloud Console](https://console.cloud.google.com).
1. Choose the project `Integrates`.
1. On the left sidebar,
   choose `APIs & Services > Credentials`.
1. On the Credentials dashboard,
   under `OAuth 2.0 Client IDs`,
   choose the client ID not created by Google Services.
1. Finally, under `Authorized redirect URIs`,
   add the URI of the ephemeral environment
   you want to enable SSO on,
   `https://<branch_name>.app.fluidattacks.com/authz_google`.
