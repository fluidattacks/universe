---
title: Fixes
description: Overview of the LLM-assisted vulnerability remediation service
slug: components/integrates/fixes
---

import { Aside, LinkCard } from '@astrojs/starlight/components';
import ThemedImage from '@components/ThemedImage.astro';

The fixes feature allows users to remediate vulnerabilities leveraging the
power of GenAI. By requesting an Autofix, users can either get a detailed
step-by-step guide or the modifications to the code that should close the
vulnerability.

<Aside type="caution">
As with all GenAI answers, the accuracy of the solution should be reviewed
prior to being committed to the codebase.
</Aside>

## Public Oath

Fluid Attacks offers a GenAI-assisted vulnerability remediation service, no
sensitive or customer-specific information will be used or stored by a
third party, customer code will not be used to train a LLM and any results will
be viewable by the customer only.

## Architecture

{/* https://drive.google.com/file/d/19wk_sRRv4qLKGYEez8zEo2yMiN_pa5SV/view?usp=sharing */}

<ThemedImage
  alt="arch"
  paths={{
    light: "docs/development/components/integrates/fixes/arch-light.png",
    dark: "docs/development/components/integrates/fixes/arch-dark.png",
  }}
/>

---

1. **Requesting fix**: Autofixes can be requested from either Retrieves or
   Integrates. A GraphQL subscription request is sent to the API.
1. **Validation and prompt-building**: After validating the provided inputs, the
   backend gathers the context of the vulnerability and uses it to fill a
   generic prompt, this prompt also comes with a snippet of the vulnerable code
   itself.
1. **Prompting the LLM**: The Integrates backend sends the prompt through
   the boto client to [Amazon Bedrock](https://aws.amazon.com/es/bedrock/),
   then, using inference profiles, the prompt is fed to the AWS-hosted
   [LLM](https://help.fluidattacks.com/portal/en/kb/articles/claude-3-5-sonnet).
1. **LLM Response**: The LLM instance processes the input and gives an answer.
   As the full answer can take around 10 to 20 seconds to be generated, is
   returned as a constantly updated string stream.
1. **Platform response**: This stream is conveyed to the Integrates backend and
   then the Retrieves or Front client with the aforementioned GraphQL
   subscription.
1. **Displaying result**: The input is collected and shown to the user either
   as a Markdown guide or as the code to be pasted.

## Data security and privacy

As this service requires sending user code to a third party GenAI model,
measures must be taken to ensure the safety of the whole process:

### Amazon Bedrock

AWS infrastructure
[hosts](https://docs.aws.amazon.com/solutions/latest/generative-ai-application-builder-on-aws/security-1.html#using-third-party-models-on-amazon-bedrock)
the LLMs used by this service.

Amazon Bedrock doesn't store or log prompts and completions. Neither does it
use them to train any AWS LLM models and distribute them to third parties.
See the Bedrock
[data protection](https://docs.aws.amazon.com/bedrock/latest/userguide/data-protection.html)
guide.

Data both at rest and in transit is also encrypted. See the
[data encryption](https://docs.aws.amazon.com/bedrock/latest/userguide/data-encryption.html)
guide.

As an additional precaution, this service has been
[disabled](https://gitlab.com/fluidattacks/universe/-/blob/trunk/integrates/back/integrates/vulnerabilities/fixes/validations.py#L29)
for vulnerabilities related to leaked secrets in code.

### To Do

- Use [AWS GuardRails](https://aws.amazon.com/bedrock/guardrails/) to sanitize
  code snippets and remove sensitive information before feeding the prompt to the
  LLM.
- Instead of getting the context from criteria and adding it to the prompt, use
  [RAG](https://aws.amazon.com/bedrock/knowledge-bases/) to give the model a
  knowledge base to consult, improve the quality of the results and simplify
  the prompt.
- Consider using a provisioned, open source LLM on transparency grounds.

<LinkCard
  title="Supported languages"
  href="https://gitlab.com/fluidattacks/universe/-/blob/trunk/common/fluidattacks-core/fluidattacks_core/serializers/syntax.py#L28"
/>
