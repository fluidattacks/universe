---
title: Virtual Private Network (VPN)
description: Common component responsible for establishing secure connections between Fluid Attacks and its customers
---

The VPN is a common component
that is is responsible
of establishing secure connections
between [Fluid Attacks' VPC](/components/common/vpc/)
and the private network of some of Fluid Attacks' clients.

Its main purpose is to provide a method
to reach resources
that are not accessible via Internet
and are essential
for Fluid Attacks' service.

These resources are typically source code repositories
and pre-production application environments.

## Public Oath

1. Fluid Attacks will look for private-connection
   solutions that balance
   security, usability and standardization
   in order to provide a service that is
   as seamless and scalable as possible.
1. An authentication architecture that uses
   role-based access control, strong passwords and multiple factors
   will be used for controlling
   who accesses client networks.
1. When possible,
   detailed logs of network activity
   will be provided to clients
   via [Integrates](/components/integrates/).

## Architecture

1. The `vpn` component
   is managed as code using Terraform.
1. Sensitive information for establishing secure connections
   with client networks
   is stored in encrypted YAML files
   using Mozilla SOPS.
1. It implements two different architectures,
   Cloudflare and AWS.

### Cloudflare

{/* https://drive.google.com/file/d/19wk_sRRv4qLKGYEez8zEo2yMiN_pa5SV/view?usp=sharing */}

import ThemedImage from '@components/ThemedImage.astro';

<ThemedImage
  alt="Architecture"
  paths={{
    light: "docs/development/components/common/vpn/arch-cloudflare-light.png",
    dark: "docs/development/components/common/vpn/arch-cloudflare-dark.png",
  }}
/>

---

1. This architecture uses
   Fluid Attacks' Cloudflare network in order to connect
   Fluid Attacks and its clients.
1. Fluid Attacks Hackers
   use Cloudflare WARP
   to connect to the Cloudflare network.
1. Authentication to Cloudflare WARP for hackers
   is done via [Okta](/components/common/iam/).
1. A [AWS Batch](/components/common/compute/)
   compute environment called `warp`
   initializes machines with Cloudflare WARP installed
   so they can reach the networks of clients.
1. Authentication to Cloudflare WARP for AWS Batch
   is done using a service token.
1. Posture policies like having the latest OS version,
   firewall turned on,
   and encrypted hard drive,
   are enforced over all devices connected to WARP.
1. The [AWS VPC](/components/common/vpc/)
   is accessible via WARP for developers
   to perform debugging.
1. Navigation logs for traffic between Fluid Attacks
   and the networks of clients are pushed to a s3 bucket
   using Cloudflare Logpush.
1. Minimum privilege traffic rules exist so only users
   with permissions have access to networks of clients.

Networks of clients can be reached in two different ways.

#### Connector

1. The client installs `cloudflared` in
   a machine belonging to the network
   they want to share with Fluid Attacks.
1. `cloudflared`
   communicates with
   Fluid Attacks' Cloudflare network.
1. The `cloudflared` instance becomes
   a pivot machine that allows Fluid Attacks
   to reach the client network.
1. As the client is the owner
   of the `cloudflared` instance
   running in their network,
   they can set up minimum-privilege
   rules for it to only
   reach those resources that
   should be accessible.
1. Internal DNS requests are forwarded
   to the client's DNS servers.

#### Egress

1. Fluid Attacks provides a pair
   of reserved IPv4 public addresses
   that are used exclusively
   for reaching the client's network.
   These are called `egress ips`.
1. The client exposes the
   resources that need to be accessed
   to the Internet
   and only allows traffic from
   Fluid Attacks `egress ips`.

### AWS

<ThemedImage
  alt="Architecture"
  paths={{
    light: "docs/development/components/common/vpn/arch-aws-light.png",
    dark: "docs/development/components/common/vpn/arch-aws-dark.png",
  }}
/>

---

1. This architecture is deprecated
   and will be removed in the future.
1. It uses AWS Site-to-Site VPN Connections.
1. A subnet called `batch_clone`
   is routed through all the Site-to-Site VPNs
   so it can reach the networks of clients.
1. It uses Route53 to resolve domains
   within the networks of clients.
1. Fluid Attacks Hackers
   can access hosts in the networks of clients
   by using the AWS Client VPN.
1. Authentication to the AWS Client VPN Endpoint
   is done via [Okta](/components/common/iam/).

## Contributing

Please read the
[contributing](/getting-started/contributing) page first.

### General

- Any changes to [vpn][vpn] infrastructure
  must be done
  via [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/).
- Infrastructure source code
  can be found [here](https://gitlab.com/fluidattacks/universe/-/tree/trunk/common/vpc/infra).
- All [vpn][vpn] client configurations
  can be found [here](https://gitlab.com/fluidattacks/universe/-/blob/trunk/common/vpn/data.yaml).
  You can use SOPS to decrypt such values.

### Accessing the VPN

You can connect to the [vpn][vpn]
and gain access to our AWS VPC
and client private networks.
In order to do so, you need to:

1. Go to the [VPN Self-Service portal](https://self-service.clientvpn.amazonaws.com/endpoints/cvpn-endpoint-05b3ce2112d0a836a):
   - Log in with your Okta Credentials.
   - If you do not have enough permissions, please contact [help@fluidattacks.com](mailto:help@fluidattacks.com).
1. From the portal:
   - Download the [vpn][vpn] client configuration.
   - Download and install the AWS Client VPN for your Operating System.
1. Open the AWS Client VPN and import the downloaded configuration.
1. Connect to the VPN.

[vpn]: https://aws.amazon.com/vpn/
