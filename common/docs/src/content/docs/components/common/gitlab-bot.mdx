---
title: GitLab bot
description: AWS Lambda handler for GitLab Webhooks
---

import { Aside } from '@astrojs/starlight/components';

This is an AWS Lambda handler for GitLab Webhooks built with TypeScript,
which enables performing [actions](#actions)
in response to [GitLab events][webhook-events].

## Getting Started

To start developing actions, run:

```sh
cd common/ci/infra/module/gitlab-bot
npm install
```

You can find the Webhook configuration [here][webhook-config].

You can find the production logs [here][cloudwatch-logs]
or use this [link][cloudwatch-logs-insights] for easy filtering.

## Actions

### Issue review

Whenever an issue is opened or updated,
the bot will scan it's content,
and perform actions to help keep things safe and organized,
like marking issues with attachments as confidential.

### Merge request merge

Once an MR is approved,
this bot takes care of rebasing and merging so you don't have to.

### Merge request review

This bot leverages the power of Danger JS to provide automated reviews within
Merge requests.

[Danger JS][danger] provides a variety of APIs that can be used to build
and extend checks performed by the bot.

To test any changes in the danger file, use the following command:

```sh
npx danger pr --dangerfile ./src/mr-review/dangerfile.ts MERGE_REQUEST_URL
```

- Replace `MERGE_REQUEST_URL` with the actual URL of the merge request you want
  to test.
- This command runs Danger Js against the specified merge request using the
  rules defined in `mr-review/dangerfile.ts`.

### Pipeline cancel

This bot cancels unnecessary pipelines when pushing changes to the same branch.

It also cancels pipelines when rebasing an open Merge Request,
as per team policy,
only the first MR pipeline must succeed,
so these secondary pipelines aren't required
and canceling them can lead to savings in compute resources.

### Trunk pipeline failures

This bot automatically creates a new issue on the universe project whenever
it detects a failed pipeline on trunk.

It filters jobs failed due to initialization problems or deployment failures
due to state locks (Since there is a scheduler in charge of cleaning those)

## Development considerations

### Access Token

This bot relies on `UNIVERSE_API_TOKEN`,
a [project access token][pat],
to interact with GitLab's API.

The token will be rotated regularly,
and when it happens,
you will see a different account name used by the bot.

<Aside>
  The name is chosen creatively by the engineer in charge
  of performing the secret rotation.
</Aside>

After the secret is rotated,
a maintainer will need to update the Merge Request approval rule,
following these [steps][edit-approval-rule],
and if there are any open Merge Requests at that moment,
each of them will need to be updated by clicking edit,
expanding the approval rules section,
clicking `Reset to project defaults`,
and saving changes.

### Concurrency

This bot will be triggered concurrently by GitLab,
with several executions happening at the same time.

Keep that in mind when calling GitLab's API,
as some operations rely on state pre-conditions,
for instance,
you can't approve the same MR twice.

It is advisable to embrace the fact they are going to race,
and just handle the cases where GitLab may complain about certain actions
being performed more than once.

### Execution time

GitLab has a [timeout for webhooks][webhook-timeout],
meaning that executions lasting over 10 seconds will be considered as failed.

GitLab then retries the calls with backoff,
and if the failures persist,
it automatically disables the webhook.

Make the most out of concurrent promises,
rather than sequential actions where possible.

### Infinite loops

Calling GitLab APIs that result in another webhook call may be risky,
as it can cause frequent unintended invocations that can impact the cost
and performance attributes of the bot.

Ensure the function always has an exit condition that would break these loops.

[cloudwatch-logs]: https://us-east-1.console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252Fcommon-ci-infra-gitlab-bot/log-events

[cloudwatch-logs-insights]: https://us-east-1.console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:logs-insights$3FqueryDetail$3D~\(end~0~start~-3600~timeType~'RELATIVE~unit~'seconds~editorString~'~isLiveTail~false~queryId~'~source~\(~'*2faws*2flambda*2fcommon-ci-infra-gitlab-bot\)\)

[danger]: https://danger.systems/js/reference

[edit-approval-rule]: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html#edit-an-approval-rule

[pat]: https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html

[webhook-config]: https://gitlab.com/fluidattacks/universe/-/hooks

[webhook-events]: https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html

[webhook-timeout]: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#webhook-timeout-limits
