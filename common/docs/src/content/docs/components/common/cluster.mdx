---
title: Cluster
description: Responsibilities of the Common component in managing a company-wide Kubernetes Cluster
---

Cluster is the component of Common
in charge of providing a company-wide Kubernetes Cluster.

## Public Oath

Fluid Attacks will constantly look
for new stack that simplifies
serving applications to the Internet
in a secure and automated manner.

## Architecture

{/* https://drive.google.com/file/d/19wk_sRRv4qLKGYEez8zEo2yMiN_pa5SV/view?usp=sharing */}

import ThemedImage from '@components/ThemedImage.astro';

<ThemedImage
  alt="Architecture"
  paths={{
    light: "docs/development/components/common/cluster/arch-light.png",
    dark: "docs/development/components/common/cluster/arch-dark.png",
  }}
/>

---

1. The module is managed as code using Terraform.
1. We have one Kubernetes cluster
   called `common-k8s`
   that is shared by all the components.
1. The cluster is hosted on AWS.
1. The cluster is divided into namespaces,
   which keep resources in isolation from other namespaces.
   - The `default` namespace is unused,
     we try to put things into a namespace appropriate to the product.
   - The `dev` namespace
     currently holds the ephemeral environments of Integrates.
   - The `prod-integrates` namespace holds the production deployment
     of Integrates,
     and a Celery jobs server.
   - The `kube-system` namespace holds cluster-wide deployments
     for, load balancer, DNS, node termination handler,
     cloudflared, observability and autoscaler.
   - Other `kube-*` namespaces exist,
     but they are not used for anything at the moment.
1. Every namespace runs in a specific worker group
   whose physical machine instances run
   on EC2.
1. The cluster spawns machines on all availability zones within
   `us-east-1` for maximum spot availability.
1. The cluster supports autoscaling based on several metrics like
   cpu consumption, memory consumption and queue size.
1. It provides observability tools for debugging.
1. It creates AWS application load balancers and
   Cloudflare DNS records for ingress resources.
1. It supports a Cloudflare ZTNA tunnel that allows
   developers to access the AWS VPC via Cloudflare WARP.
1. Developers can access the cluster via Okta for debugging.

## Contributing

Please read the
[contributing](/getting-started/contributing) page first.

### General

1. Any changes to the
   [cluster](https://gitlab.com/fluidattacks/universe/-/tree/4ad18b78c630878afdafbf192fcbf54c7bc7a006/makes/foss/modules/makes/kubernetes)
   infrastructure and configuration
   must be done via
   [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/).
1. Any changes related to the
   [Platform][platform]
   (deployments, autoscaling, ingress...)
   for both
   [development](https://gitlab.com/fluidattacks/universe/-/tree/4ad18b78c630878afdafbf192fcbf54c7bc7a006/makes/foss/units/integrates/back/deploy/dev/k8s)
   and
   [production](https://gitlab.com/fluidattacks/universe/-/tree/4ad18b78c630878afdafbf192fcbf54c7bc7a006/makes/foss/units/integrates/back/deploy/prod/k8s)
   must be done via
   [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/).

### Components

Our cluster implements:

1. [AWS EKS Terraform module](https://github.com/terraform-aws-modules/terraform-aws-eks)
   for declaring the cluster as code
   using Terraform.
1. [AWS Load Balancer Controller](https://github.com/kubernetes-sigs/aws-load-balancer-controller)
   for automatically initializing
   AWS load balancers
   when declaring
   [ingress resources](https://kubernetes.io/docs/concepts/services-networking/ingress/).
1. [AWS Kubernetes Autoscaler](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler)
   for automatically scaling
   the cluster size based on
   [resource assignation](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/).
1. [ExternalDNS](https://github.com/kubernetes-sigs/external-dns)
   for automatically setting DNS records
   when declaring
   [ingress resources](https://kubernetes.io/docs/concepts/services-networking/ingress/).
1. [Kubernetes Metrics Server](https://github.com/kubernetes-sigs/metrics-server)
   for automatically scaling
   [deployments](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
   like production [Platform][platform]
   based on application load (CPU, Memory, custom metrics).
1. [New Relic](https://newrelic.com/)
   for monitoring both
   production [Platform][platform]
   and general infrastructure.

### Debugging

#### Connect to cluster

In order to connect
to the Kubernetes Cluster,
you must:

1. Login as an Integrates developer
   using [this guide](/getting-started/environment/#terminal).
1. Install kubectl and aws-cli with `nix-env -i awscli kubectl`.
1. Select cluster by running
   `aws eks update-kubeconfig --name common-k8s --region us-east-1`.
1. Run `kubectl get node`.

Your input should be similar to this:

```bash
kubectl get node
NAME                            STATUS   ROLES    AGE   VERSION
ip-192-168-5-112.ec2.internal   Ready    <none>   58d   v1.17.9-eks-4c6976
ip-192-168-5-144.ec2.internal   Ready    <none>   39d   v1.17.11-eks-cfdc40
ip-192-168-5-170.ec2.internal   Ready    <none>   20d   v1.17.11-eks-cfdc40
ip-192-168-5-35.ec2.internal    Ready    <none>   30d   v1.17.11-eks-cfdc40
ip-192-168-5-51.ec2.internal    Ready    <none>   30d   v1.17.11-eks-cfdc40
ip-192-168-6-109.ec2.internal   Ready    <none>   30d   v1.17.11-eks-cfdc40
ip-192-168-6-127.ec2.internal   Ready    <none>   18d   v1.17.11-eks-cfdc40
ip-192-168-6-135.ec2.internal   Ready    <none>   31d   v1.17.11-eks-cfdc40
ip-192-168-6-151.ec2.internal   Ready    <none>   30d   v1.17.11-eks-cfdc40
ip-192-168-6-221.ec2.internal   Ready    <none>   13d   v1.17.11-eks-cfdc40
ip-192-168-7-151.ec2.internal   Ready    <none>   30d   v1.17.11-eks-cfdc40
ip-192-168-7-161.ec2.internal   Ready    <none>   33d   v1.17.11-eks-cfdc40
ip-192-168-7-214.ec2.internal   Ready    <none>   61d   v1.17.9-eks-4c6976
ip-192-168-7-48.ec2.internal    Ready    <none>   30d   v1.17.11-eks-cfdc40
ip-192-168-7-54.ec2.internal    Ready    <none>   39d   v1.17.11-eks-cfdc40
```

#### Common commands

Most commands have the following syntax: `kubectl <action> <resource> -n <namespace>`

- Common actions are: `get`, `describe`, `logs`, `exec` and `edit`.
- Common resources are: `pod`, `node`, `deployment`, `ingress`, `hpa`.
- Common namespaces are: `development`, `production` and `kube-system`.
  Additionally, the `-A` flag executes `<action>` for all namespaces.

Some basic examples are:

| Command                     | Example | Description                  |
| --------------------------- | ------- | ---------------------------- |
| `kubectl get pod -A`        | `N/A`   | Get all running pods         |
| `kubectl get node -A`       | `N/A`   | Get all cluster nodes        |
| `kubectl get deployment -A` | `N/A`   | Get all cluster deployments  |
| `kubectl get hpa -A`        | `N/A`   | Get all autoscaling policies |
| `kubectl get namespace`     | `N/A`   | Get all cluster namespaces   |

Some more complex examples are:

| Command                                                             | Example                                                                               | Description                   |
| ------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | ----------------------------- |
| `kubectl describe pod -n <namespace> <pod>`                         | `kubectl describe pod -n development app-dsalazaratfluid-7c485cf565-w9gwg`            | Describe pod configurations   |
| `kubectl logs -n <namespace> <pod> -c <container>`                  | `kubectl logs -n development app-dsalazaratfluid-7c485cf565-w9gwg -c app`             | Get container logs from a pod |
| `kubectl exec -it -n <namespace> <pod> -c <container> -- <command>` | `kubectl exec -it -n development app-dsalazaratfluid-7c485cf565-w9gwg -c app -- bash` | Access a container within pod |
| `kubectl edit deployment -n <namespace> <deployment>`               | `kubectl edit deployment -n development integrates-dsalazaratfluid`                   | Edit a specific deployment    |

[platform]: https://fluidattacks.com/platform/
