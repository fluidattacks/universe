{ inputs, makeScript, ... }: {
  jobs."/common/docs" = makeScript {
    entrypoint = ''
      pushd common/docs
      npm ci
      npm run $@
    '';
    name = "common-docs";
    searchPaths.bin = [
      inputs.nixpkgs.bash
      inputs.nixpkgs.git
      inputs.nixpkgs.lychee
      inputs.nixpkgs.nodejs_20
      inputs.nixpkgs.vips
    ];
  };
}
