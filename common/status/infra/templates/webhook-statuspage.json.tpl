{
  "incident": {
    "name": "{{ALERT_TITLE}}",
    "status": "identified",
    "impact_override": "none",
    "reminder_intervals": "2",
    "metadata": {},
    "deliver_notifications": true,
    "auto_tweet_at_beginning": false,
    "auto_tweet_on_completion": false,
    "auto_tweet_on_creation": false,
    "auto_tweet_one_hour_before": false,
    "backfill_date": "string",
    "backfilled": false,
    "body": "An issue was found in {{CHECK_NAME}}.<br>Click for details: https://availability.fluidattacks.com",
    "components": {
      "3thrprrfgx5g": "major_outage"
    },
    "component_ids": "{{#eq CHECK_NAME 'AGENT'}}4111gls5s8ht{{/eq}} {{#eq CHECK_NAME 'DOCS'}}bb2pmf0j2md8{{/eq}} {{#eq CHECK_NAME 'PLATFORM'}}ljz5nvbn8tlv{{/eq}} {{#eq CHECK_NAME 'WEB'}}nrf1rq8vdtnc{{/eq}}",
    "scheduled_auto_transition": false
  }
}
