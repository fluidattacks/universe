const { expect, test } = require("@playwright/test");

test("integrates login", async ({ page }) => {
  await page.goto("https://app.fluidattacks.com/", {
    waitUntil: "domcontentloaded"
  });
  await expect(page).toHaveTitle("Login | Fluid Attacks");
});
