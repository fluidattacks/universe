resource "checkly_check" "docs" {
  name                      = "KB"
  type                      = "BROWSER"
  activated                 = true
  frequency                 = 10
  use_global_alert_settings = false
  runtime_id                = "2024.09"
  group_id                  = checkly_check_group.web_group.id
  group_order               = 2

  script = <<-EOF
    const { expect, test } = require("@playwright/test");

    test("docs", async ({ page }) => {
      await page.goto("https://help.fluidattacks.com/portal/en/kb", {
        waitUntil: "domcontentloaded"
      });
      await expect(page).toHaveTitle("| Help Center | Knowledge Base");
    });
  EOF
}
