resource "checkly_check_group" "internal_web_group" {
  name        = "Internal Web Group"
  activated   = true
  muted       = false
  runtime_id  = "2024.09"
  concurrency = 1

  tags = ["production"]

  locations = [
    "us-east-1",
    "sa-east-1",
    "eu-central-1",
    "ap-east-1",
  ]

  use_global_alert_settings = false

  environment_variable {
    key    = "CHECKLY_API_KEY"
    value  = var.apiKey
    locked = true
  }

  alert_settings {
    escalation_type = "RUN_BASED"

    run_based_escalation {
      failed_run_threshold = 2
    }
  }

  retry_strategy {
    type                 = "FIXED"
    base_backoff_seconds = 60
    max_duration_seconds = 600
    max_retries          = 2
    same_region          = true
  }

  dynamic "alert_channel_subscription" {
    for_each = {
      for user in var.alertUsers : split("@", user)[0] => user
    }
    content {
      channel_id = checkly_alert_channel.emails[alert_channel_subscription.key].id
      activated  = true
    }
  }

  alert_channel_subscription {
    channel_id = checkly_alert_channel.sms.id
    activated  = true
  }
}
