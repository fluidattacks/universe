variable "accountId" {
  sensitive = true
  type      = string
}

variable "alertSms" {
  type = string
}

variable "alertUsers" {
  type = list(string)
}

variable "apiKey" {
  sensitive = true
  type      = string
}

variable "envIntegratesApiToken" {
  sensitive = true
  type      = string
}

variable "statuspageApiKey" {
  sensitive = true
  type      = string
}
