{ inputs, makeScript, outputs, ... }: {
  jobs."/common/test/hooks/pre-push" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-hooks-pre-push";
    searchPaths.bin = [
      inputs.nixpkgs.git
      outputs."/common/test/base/job"
      outputs."/common/test/commitlint"
      outputs."/common/test/leaks"
      outputs."/common/test/secrets/forbidden"
      outputs."/common/test/secrets/commit-msg"
      outputs."/common/test/secrets/commit-changes"
      outputs."/common/test/spelling"
      outputs."/common/test/pipeline"
    ];
  };
}
