{ inputs, makeScript, outputs, ... }: {
  jobs."/common/test/machine_sast" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-machine_sast";
    searchPaths.bin =
      [ inputs.nixpkgs.git inputs.nixpkgs.gnused outputs."/skims" ];
  };
}
