{ inputs, makeScript, ... }: {
  jobs."/common/test/commitlint" = makeScript {
    name = "common-test-commitlint";
    entrypoint = ''
      pushd common/test/commitlint

      commit_hash="$(git --no-pager log --pretty=%h origin/trunk..HEAD)"

      info "Linting commit $commit_hash"
      git log -1 --pretty=%B $commit_hash | commitlint --parser-preset ./parser.js --config ./config.js
    '';
    searchPaths.bin = [ inputs.nixpkgs.commitlint inputs.nixpkgs.git ];
  };
}
