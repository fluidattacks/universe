# shellcheck shell=bash

function main {
  local commit_msg
  local changes
  local file_validate
  local all_files
  local all_files_unmodified
  local changes_content
  local change_specify
  local base_dir
  local file_opposite_regrex
  local file_opposite
  local file_content

  : && info "Checking that are no secrets that have the same name" \
    && commit_msg="$(git --no-pager log --pretty=format:%B HEAD^..HEAD)" \
    && if echo "${commit_msg}" | grep -qi -- "- no-equal-secret-same-product-test"; then
      return 0
    fi \
    && changes=$(git diff-tree --no-commit-id --name-only -r HEAD) \
    && file_validate='^(common|integrates|observes|sorts|skims|docs)/(secrets|status)/(dev|development|secrets|production|prod)?\.yaml$' \
    && all_files=$(git ls-tree -r --name-only HEAD | grep -E "$file_validate") \
    && mapfile -t changes_array <<< "$changes" \
    && all_files_unmodified="$all_files" \
    && while IFS= read -r change; do
      all_files_unmodified="${all_files_unmodified//$change/}"
    done <<< "$changes" \
    && all_files_unmodified=$(echo "$all_files_unmodified" | grep -v '^$') \
    && mapfile -t all_files_unmodified_array <<< "$all_files_unmodified" \
    && filter_file_regrex='^(common|integrates|observes|sorts|skims|docs)/secrets/(dev|development)\.yaml$' \
    && filtered_changes_array=() \
    && for change in "${changes_array[@]}"; do
      if [[ $change =~ $filter_file_regrex ]]; then
        filtered_changes_array+=("$change")
      fi
    done \
    && for file in "${changes_array[@]}"; do
      changes_content=$(git show HEAD^..HEAD -- "$file")
      change_specify=$(echo "${changes_content}" | grep -oE '^[+\-][a-zA-Z]+([[:alnum:]_]+)' \
        | sed 's/-//' | sed 's/+//')
      if [ -z "$change_specify" ]; then
        return 0
      fi
      if [[ $file =~ (common|integrates|observes|sorts|skims|docs)/secrets/(dev|development)?\.yaml$ ]]; then
        for all_file in "${all_files_unmodified_array[@]}"; do
          base_dir="${file%/secrets/*}"
          file_opposite_regrex="${base_dir}/secrets/(prod|production)?\.yaml$"
          file_opposite=$(git ls-tree -r --name-only HEAD | grep -E "$file_opposite_regrex")
          if [[ $all_file =~ $file_opposite ]]; then
            for secret_name in $change_specify; do
              file_content="$(cat "$file_opposite")"
              if echo "${file_content}" | grep -q "$secret_name"; then
                error \
                  "$secret_name name should not be repeated between files". \
                  "If it's the same value between both secrets, this must be added in the development file," \
                  "if the secret has a different value, please name them differently" \
                  "use - no-equal-secret-same-product-test if needed."
              fi
            done
          fi
        done
      fi
    done \
    && for file in "${filtered_changes_array[@]}"; do
      if [[ $file =~ (common|integrates|observes|sorts|skims|docs)/secrets/(dev|development)?\.yaml$ ]]; then
        base_dir="${file%/secrets/*}"
        file_opposite_regrex="${base_dir}/secrets/(prod|production)?\.yaml$"
        file_opposite=$(git ls-tree -r --name-only HEAD | grep -E "$file_opposite_regrex")
        changes_content=$(git show HEAD^..HEAD -- "$file")
        change_specify=$(echo "${changes_content}" | grep -oE '^[+\-][a-zA-Z]+([[:alnum:]_]+)' \
          | sed 's/-//' | sed 's/+//')
        if [ -z "$change_specify" ]; then
          return 0
        fi
        for file_edit in "${changes_array[@]}"; do
          if [[ $file_edit =~ $file_opposite ]]; then
            for secret_name in $change_specify; do
              file_content="$(cat "$file_opposite")"
              if echo "${file_content}" | grep -q "$secret_name"; then
                error \
                  "$secret_name name should not be repeated between files". \
                  "If it's the same value between both secrets, this must be added in the development file," \
                  "if the secret has a different value, please name them differently" \
                  "use - no-equal-secret-same-product-test if needed."
              fi
            done
          fi
        done
      fi
    done

  info "There are no secrets that have the same name"
}

main "${@}"
