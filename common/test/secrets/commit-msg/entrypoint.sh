# shellcheck shell=bash

function main {
  local changes
  local commit_msg
  local found
  local secret_name
  declare -A list_secret
  declare -A list_change

  : && info "Checking that the changes match the description of the commit msg" \
    && commit_msg="$(git --no-pager log --pretty=format:%B HEAD^..HEAD)" \
    && if echo "${commit_msg}" | grep -qi -- "- delete"; then
      return 0
    fi \
    && secret_name=$(echo "${commit_msg}" | grep '^-' | grep -i -- '- Rotate' \
      | sed 's/^- [Rr][Oo][Tt][Aa][Tt][Ee]//' | sed 's/^[[:space:]]//') \
    && changes_global="$(git show HEAD^..HEAD)"

  for file in $(git diff-tree --no-commit-id --name-only -r HEAD); do
    if [[ $file =~ (common|integrates|observes|sorts|skims|docs)/secrets/(dev|prod|development|production)?\.yaml$ ]]; then
      echo "Changes in $file:"
      changes=$(git show HEAD^..HEAD -- "$file")
      if echo "${changes}" | grep -q '^+#ENC\[A'; then
        return 0
      fi
      change_specify=$(echo "${changes}" | grep -oE '^[+\-][a-zA-Z]+([[:alnum:]_]+)' \
        | sed 's/-//' | sed 's/+//')

      for change_item in $change_specify; do
        found=false
        for secret_specify in $secret_name; do
          if [[ $secret_specify == "$change_item" ]]; then
            found=true
            break
          fi
        done

        if ! $found; then
          error \
            "You have changed ${change_item} in $file," \
            "but it is not included in the commit body." \
            "Please added your change in commit body as follows: - Rotate ${change_item}" \
            "or specify if it was deleted as follows: - Delete ${change_item}"
        fi
      done

      change_global_=$(echo "${changes_global}" | grep -oE '^[+\-][a-zA-Z]+([[:alnum:]_]+):' \
        | sed 's/-//' | sed 's/+//' | sed 's/://')
      for specify_item in $change_global_; do
        list_change["$specify_item"]=1
      done

      for secret_specify in $secret_name; do
        list_secret["$secret_specify"]=1
      done
    fi
  done
  list_change_str=$(
    IFS=,
    echo "${!list_change[*]}"
  )
  list_secret_str=$(
    IFS=,
    echo "${!list_secret[*]}"
  )
  if [[ $list_secret_str != "$list_change_str" ]]; then
    error \
      "You declare that you rotated ${list_secret_str} in commit body". \
      "These are your changes related to rotations: ${list_change_str}." \
      "What is added in the commit body must match the changes."
  fi
}

main "${@}"
