{ inputs, makeScript, ... }: {
  jobs."/common/test/secrets/commit-msg" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-secrets-commit-msg";
    searchPaths.bin =
      [ inputs.nixpkgs.git inputs.nixpkgs.gnugrep inputs.nixpkgs.gnused ];
  };
}
