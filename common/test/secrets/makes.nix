{ inputs, makeScript, outputs, ... }: {
  jobs."/common/test/secrets" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-secrets";
    searchPaths.bin = [
      outputs."/common/test/secrets/commit-msg"
      outputs."/common/test/secrets/equal-secrets-different-products"
      outputs."/common/test/secrets/equal-secrets-same-product"
      outputs."/common/test/secrets/forbidden"
    ];
  };
}
