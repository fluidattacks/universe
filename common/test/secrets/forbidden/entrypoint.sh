# shellcheck shell=bash

function main {
  local commit_msg
  local changes
  local secrets=(
    ACCOUNT_ID
    APPLE_ID
    ALERT_SMS
    ALERT_USERS
    ANALYTICS_TIMEDOCTOR_USER
    announcekit_fluid_proj
    announcekit_proj
    announcekit_user
    AWS_OPENSEARCH_HOST
    AWS_OPENSEARCH_HOST_DEV
    AZUREAD_OAUTH2_KEY
    AZURE_OAUTH2_REPOSITORY_APP_ID
    AZURE_OAUTH2_REPOSITORY_SECRET
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID_DEV
    BITBUCKET_OAUTH2_REPOSITORY_SECRET
    BITBUCKET_OAUTH2_REPOSITORY_SECRET_DEV
    checkly_api_user
    CHECKLY_CHECK_ID
    CHECKLY_TRIGGER_ID
    CHECKLY_USER
    CHECKLY_USER_ADMIN
    CLOUDFLARE_ACCOUNT_ID
    CLOUDFLARE_EMAIL
    CLOUDFLARE_OKTA_CLIENT_ID
    CLOUDFLARE_OKTA_SCIM_ID
    CLOUDFLARE_WARP_AUTH_CLIENT_ID
    CLOUDFLARE_WARP_AUTH_CLIENT_ID
    DEBUG
    DEBUG_DEV
    DOCKER_HUB_USER
    DYNAMODB_HOST
    DYNAMODB_PORT
    GITHUB_OAUTH2_APP_ID
    GITLAB_OAUTH2_APP_ID
    GITLAB_OAUTH2_SECRET
    GOOGLE_OAUTH2_KEY
    MAIL_CONTINUOUS
    MAIL_CONTINUOUS_DEV
    MAIL_COS
    MAIL_COS_DEV
    MAIL_CTO
    MAIL_CTO_DEV
    MAIL_CUSTOMER_EXPERIENCE
    MAIL_CUSTOMER_EXPERIENCE_DEV
    MAIL_CUSTOMER_SUCCESS
    MAIL_CUSTOMER_SUCCESS_DEV
    MAIL_CXO
    MAIL_CXO_DEV
    MAIL_FINANCE
    MAIL_FINANCE_DEV
    MAIL_PRODUCTION
    MAIL_PRODUCTION_DEV
    MAIL_PROFILING
    MAIL_PROFILING_DEV
    MAIL_PROJECTS
    MAIL_PROJECTS_DEV
    MAIL_REVIEWERS
    MAIL_REVIEWERS_DEV
    MAIL_TELEMARKETING
    MAIL_TELEMARKETING_DEV
    MANUAL_CLONING_PROJECTS
    MANUAL_CLONING_PROJECTS_DEV
    MIXPANEL_PROJECT_ID
    TEST_ORGS
    TEST_PROJECTS
    TEST_PROJECTS_DEV
    TWILIO_ACCOUNT_SID
    TWILIO_VERIFY_SERVICE_SID
    WEBHOOK_POC_ORG
    WEBHOOK_POC_ORG_DEV
    WEBHOOK_POC_URL
    WEBHOOK_POC_URL_DEV
  )

  : && info "Testing that non-rotable secrets have not been changed" \
    && commit_msg="$(git --no-pager log HEAD^..HEAD)" \
    && if echo "${commit_msg}" | grep -q -- "- necessary-rotate-secrets"; then
      return 0
    fi \
    && changes="$(git show HEAD^..HEAD)"
  for file in $(git diff-tree --no-commit-id --name-only -r HEAD); do
    if [[ $file =~ (common|integrates|observes|sorts|skims|docs)/secrets/(dev|prod|development|production)?\.yaml$ ]]; then
      for secret_name in "${secrets[@]}"; do
        if echo "${changes}" | grep -E '^[+\-]'"$secret_name" && ! echo "${changes}" | grep -q '^#ENC\[A'; then
          error \
            "Commit contains changes for non-rotate secret: ${secret_name}" \
            "Use '- necessary-rotate-secrets' if needed."
        fi
      done
    fi
  done
}

main "${@}"
