# shellcheck shell=bash

function main {

  info "Checking that the changes are for secrets only"
  local wrong_files=()

  for file in $(git diff-tree --no-commit-id --name-only -r HEAD); do
    if [[ $file =~ ^(.*)\/(secrets|iam|vpn|marketplace|status)\/(dev|development|prod|production|data|secrets)\.yaml ]]; then
      echo "Changes found for: $file"
    else
      wrong_files+=("$file")
    fi
  done
  if [ ${#wrong_files[@]} -gt 0 ]; then
    error "The following file changes found in HEAD are not secrets: ${wrong_files[*]}"
    exit 1
  fi
  info "All changes are for secrets"
}

main "${@}"
