from pathlib import (
    Path,
)

from commit_linter_base.products import (
    Product,
)

from .integrates import integrates_roots
from .observes import observes_roots
from .others import others_roots
from .skims import skims_roots


def product_roots(product: Product) -> frozenset[Path]:
    return product.map(
        others_roots,
        integrates_roots,
        skims_roots,
        observes_roots,
    )
