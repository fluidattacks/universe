from pathlib import Path


def single_path(path: Path) -> frozenset[Path]:
    return frozenset({path})


def multi_path(*paths: Path) -> frozenset[Path]:
    return frozenset(paths)
