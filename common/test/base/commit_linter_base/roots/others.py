from pathlib import (
    Path,
)

from commit_linter_base.products.core import OtherProducts

from ._utils import single_path


def others_roots(product: OtherProducts) -> frozenset[Path]:
    # Notice
    # Do NOT use a Dict as a refactor of this function.
    # Dict does not ensure that every item of `OtherProducts`
    # gets assigned a set of Path
    match product:
        case OtherProducts.AIRS:
            result = single_path(Path("airs"))
        case OtherProducts.COMMON:
            result = single_path(Path("common"))
        case OtherProducts.FORCES:
            result = single_path(Path("forces"))
        case OtherProducts.MATCHES:
            result = single_path(Path("matches"))
        case OtherProducts.MELTS:
            result = single_path(Path("melts"))
        case OtherProducts.RETRIEVES:
            result = single_path(Path("retrieves"))
        case OtherProducts.SIFTS:
            result = single_path(Path("sifts"))
        case OtherProducts.ALL:
            result = single_path(Path())

    return result
