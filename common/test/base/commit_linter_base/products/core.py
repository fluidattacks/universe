from __future__ import (
    annotations,
)

from collections.abc import Callable
from dataclasses import dataclass
from enum import Enum
from typing import TypeVar

from fa_purity import Coproduct

from .integrates import IntegratesProduct
from .observes import ObservesProduct
from .skims import SkimsProduct

_T = TypeVar("_T")


class OtherProducts(Enum):
    ALL = "all"
    AIRS = "airs"
    COMMON = "common"
    FORCES = "forces"
    MATCHES = "matches"
    MELTS = "melts"
    RETRIEVES = "retrieves"
    SIFTS = "sifts"


@dataclass(frozen=True)
class Product:
    _inner: Coproduct[
        OtherProducts,
        Coproduct[
            IntegratesProduct,
            Coproduct[
                SkimsProduct,
                ObservesProduct,
            ],
        ],
    ]

    @staticmethod
    def other(product: OtherProducts) -> Product:
        return Product(Coproduct.inl(product))

    @staticmethod
    def integrates(product: IntegratesProduct) -> Product:
        return Product(Coproduct.inr(Coproduct.inl(product)))

    @staticmethod
    def skims(product: SkimsProduct) -> Product:
        return Product(Coproduct.inr(Coproduct.inr(Coproduct.inl(product))))

    @staticmethod
    def observes(product: ObservesProduct) -> Product:
        return Product(Coproduct.inr(Coproduct.inr(Coproduct.inr(product))))

    def map(
        self,
        others_case: Callable[[OtherProducts], _T],
        integrates_case: Callable[[IntegratesProduct], _T],
        skims_case: Callable[[SkimsProduct], _T],
        observes_case: Callable[[ObservesProduct], _T],
    ) -> _T:
        """Core transform from `Product` to any other type `_T`."""
        return self._inner.map(
            others_case,
            lambda c: c.map(
                integrates_case,
                lambda c: c.map(
                    skims_case,
                    observes_case,
                ),
            ),
        )
