from __future__ import (
    annotations,
)

from collections.abc import Callable
from typing import TypeVar

from fa_purity import Result, ResultE
from git.objects import (
    Commit,
)

from commit_linter_base.products.core import OtherProducts, Product

from .integrates import IntegratesProduct
from .observes import ObservesProduct
from .skims import SkimsProduct

_T = TypeVar("_T")


def _handle_value_error(procedure: Callable[[], _T]) -> ResultE[_T]:
    try:
        return Result.success(procedure())
    except ValueError as err:
        return Result.failure(err)


def _decode_other_products(raw: str) -> ResultE[OtherProducts]:
    return _handle_value_error(lambda: OtherProducts(raw))


def _decode_integrates(raw: str) -> ResultE[IntegratesProduct]:
    return _handle_value_error(lambda: IntegratesProduct(raw))


def _decode_skims(raw: str) -> ResultE[SkimsProduct]:
    return _handle_value_error(lambda: SkimsProduct(raw))


def _decode_observes(raw: str) -> ResultE[ObservesProduct]:
    return _handle_value_error(lambda: ObservesProduct(raw))


def decode_product(raw: str) -> ResultE[Product]:
    return (
        _decode_other_products(raw)
        .map(Product.other)
        .lash(
            lambda _: _decode_integrates(raw).map(Product.integrates),
        )
        .lash(
            lambda _: _decode_skims(raw).map(Product.skims),
        )
        .lash(
            lambda _: _decode_observes(raw).map(Product.observes),
        )
    )


def _get_commit_msg(commit: Commit) -> str:
    return commit.message if isinstance(commit.message, str) else commit.message.decode("utf-8")


def from_commit(commit: Commit) -> ResultE[Product]:
    _message = _get_commit_msg(commit)
    try:
        return decode_product(_message.strip().split("\\")[0])
    except IndexError as err:
        return Result.failure(err)
