import sys

import click

from commit_linter_base._typing import (
    NoReturn,
)

from . import (
    check,
)


@click.command()
def modified_directories() -> NoReturn:
    check.modified_directories()
    sys.exit(0)


@click.group()
def main() -> None:
    # cli group entrypoint
    pass


main.add_command(modified_directories)
