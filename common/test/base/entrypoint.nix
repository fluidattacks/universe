{ inputs, ... }:
let
  nixpkgs = let
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "c8bb7b26f2c6ecc39be2c4ddde5f5d152e4abc65";
    src = builtins.fetchTarball {
      sha256 = "0rsfnh1ialqr09a4fg47dy1abah4rh7jxj4b0d26jqdrnjkf1spk";
      url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
    };
    pkgs = import src { };
  in pkgs // { inherit (inputs) nix-filter; };
  pynix = inputs.buildPynix {
    inherit nixpkgs;
    pythonVersion = "python311";
  };
  out = import ./build {
    inherit nixpkgs pynix;
    src = import ./build/filter.nix nixpkgs.nix-filter ./.;
  };
in out
