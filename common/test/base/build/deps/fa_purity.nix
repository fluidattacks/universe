{ nixpkgs, pynix, python_pkgs }:
let
  commit = "01b77f8e21d63d5d9e25a2b930748fb853418e59"; # v2.2.0
  sha256 = "062czp8cwk6mbaq4xd7mdsmjhdg9vr4yxr2i1pzfyxrffiby223w";
  raw = let
    raw_src = builtins.fetchTarball {
      inherit sha256;
      url =
        "https://gitlab.com/dmurciaatfluid/purity/-/archive/${commit}/purity-${commit}.tar";
    };
  in {
    build = import "${raw_src}/build";
    src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  };
  bundle = raw.build {
    inherit (raw) src;
    inherit nixpkgs pynix;
    scripts = { run-lint = [ ]; };
  };
  extended_python_pkgs = python_pkgs // {
    inherit (bundle.deps.python_pkgs) types-simplejson;
  };
in bundle.buildBundle { pkgDeps = bundle.requirements extended_python_pkgs; }
