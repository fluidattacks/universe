data "aws_iam_policy_document" "observes_etl" {
  statement {
    sid    = "s3Write"
    effect = "Allow"
    actions = [
      "s3:*",
    ]
    resources = [
      "arn:aws:s3:::integrates/continuous-data", # bills
      "arn:aws:s3:::integrates/continuous-data/*",
      "arn:aws:s3:::observes*",
      "arn:aws:s3:::fluidattacks.public.storage",
      "arn:aws:s3:::fluidattacks.public.storage/*"
    ]
  }
  statement {
    sid    = "lambdasRead"
    effect = "Allow"
    actions = [
      "lambda:Get*",
      "lambda:List*",
    ]
    resources = [
      "*"
    ]
  }

  statement {
    sid    = "lambdasUpdate"
    effect = "Allow"
    actions = [
      "lambda:InvokeFunction",
      "lambda:Update*"
    ]
    resources = [
      "arn:aws:lambda:${var.region}:${var.accountId}:function:data*",
      "arn:aws:lambda:${var.region}:${var.accountId}:function:observes*"
    ]
  }

  statement {
    sid    = "batchTags"
    effect = "Allow"
    actions = [
      "batch:TagResource",
      "batch:UntagResource",
    ]
    resources = [
      "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-queue/*",
      "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-definition/*",
      "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job/*",
    ]
    condition {
      test     = "StringEquals"
      variable = "aws:RequestTag/fluidattacks:comp"
      values   = ["etl"]
    }
  }
  statement {
    sid    = "batchCancel"
    effect = "Allow"
    actions = [
      "batch:CancelJob",
      "batch:TerminateJob",
    ]
    resources = [
      "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job/*",
    ]
    # Since for scheduled jobs the tag does not propagate
    # the role cannot terminate them. Uncomment the condition
    # when the tag gets propagated.
    # condition {
    #   test     = "StringEquals"
    #   variable = "aws:RequestTag/fluidattacks:comp"
    #   values   = ["observes"]
    # }
  }
  statement {
    sid    = "batchSubmit"
    effect = "Allow"
    actions = [
      "batch:SubmitJob",
    ]
    resources = [
      "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-definition/*",
      "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-queue/*",
    ]
    condition {
      test     = "StringEquals"
      variable = "aws:RequestTag/fluidattacks:comp"
      values   = ["etl"]
    }
  }
}
data "aws_iam_policy_document" "observes_console" {
  statement {
    sid    = "readBill"
    effect = "Allow"
    actions = [
      "account:Get*",
      "billing:Get*",
      "billing:List*",
      "ce:Describe*",
      "ce:Get*",
      "ce:List*",
      "consolidatedbilling:Get*",
      "consolidatedbilling:List*",
      "cur:Describe*",
      "cur:Get*",
      "cur:Validate*",
      "invoicing:Get*",
      "invoicing:List*",
      "payments:Get*",
      "payments:List*",
      "pricing:Describe*",
      "pricing:Get*",
      "tax:Get*",
      "tax:List*",
    ]
    resources = ["*"]
  }
  statement {
    sid    = "manageObservesReports"
    effect = "Allow"
    actions = [
      "ce:CreateReport",
      "ce:DeleteReport",
      "ce:DescribeReport",
      "ce:UpdateReport",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = "redshiftDescribe"
    effect = "Allow"
    actions = [
      "redshift:DescribeClusterSnapshots",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid     = "putAlarm"
    effect  = "Allow"
    actions = ["cloudwatch:PutMetricAlarm"]
    resources = [
      "arn:aws:cloudwatch:${var.region}:${var.accountId}:alarm:*",
    ]
  }
  # NOFLUID wildcards in actions are not dangerous"
  statement {
    sid    = "GlueRead"
    effect = "Allow"
    actions = [
      "glue:Get*",
      "glue:List*",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = "GlueSearchTables"
    effect = "Allow"
    actions = [
      "glue:SearchTables"
    ]
    resources = [
      "arn:aws:glue:${var.region}:${var.accountId}:catalog",
      "arn:aws:glue:${var.region}:${var.accountId}:database/*",
    ]
  }
  statement {
    sid    = "observesEcrPublicRead"
    effect = "Allow"
    actions = [
      "ecr-public:Describe*",
      "ecr-public:Get*",
      "ecr-public:List*",
    ]
    resources = [
      "arn:aws:ecr-public::${data.aws_caller_identity.main.account_id}:repository/*",
    ]
  }
  statement {
    sid    = "ecrPublicRegistry"
    effect = "Allow"
    actions = [
      "ecr-public:DescribeRegistries",
      "ecr-public:GetRegistryCatalogData"
    ]
    resources = [
      "arn:aws:ecr-public::${data.aws_caller_identity.main.account_id}:registry/*",
    ]
  }
  statement {
    sid    = "observesEcrRead"
    effect = "Allow"
    actions = [
      "ecr:Describe*",
      "ecr:Get*",
      "ecr:List*",
    ]
    resources = [
      "arn:aws:ecr:${var.region}:${data.aws_caller_identity.main.account_id}:repository/*",
    ]
  }
  statement {
    sid    = "observesEcrManage"
    effect = "Allow"
    actions = [
      "ecr:CompleteLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:InitiateLayerUpload",
      "ecr:BatchCheckLayerAvailability",
      "ecr:PutImage",
      "ecr:BatchGetImage",
      "ecr:BatchDeleteImage",
    ]
    resources = [
      "arn:aws:ecr:${var.region}:${data.aws_caller_identity.main.account_id}:repository/observes",
    ]
  }
  statement {
    sid    = "observesEcrToken"
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
    ]
    resources = ["*"] # does not have a resource to associate
  }
  statement {
    sid    = "legacyPolicy"
    effect = "Allow"
    actions = [
      "aws-portal:ViewBilling",
      "aws-portal:ViewUsage",
    ]
    resources = ["arn:aws:aws-portal:${var.region}:${data.aws_caller_identity.main.account_id}:*"]
  }
  # NOFLUID wildcards in actions are not dangerous""
  statement {
    sid    = "quicksightPoC"
    effect = "Allow"
    actions = [
      "quicksight:*",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = "KinesisGeneralRead"
    effect = "Allow"
    actions = [
      "kinesis:ListStreams",
    ]
    resources = ["*"]
  }
  statement {
    sid    = "KinesisReadStream"
    effect = "Allow"
    actions = [
      "kinesis:DescribeLimits",
      "kinesis:DescribeStream",
      "kinesis:DescribeStreamSummary",
      "kinesis:ListShards",
      "kinesis:ListStreamConsumers",
      "kinesis:ListTagsForStream",
    ]
    resources = [
      "arn:aws:kinesis:${var.region}:${data.aws_caller_identity.main.account_id}:stream/observes-mirror",
      "arn:aws:kinesis:${var.region}:${data.aws_caller_identity.main.account_id}:stream/observes-mirror-historic"
    ]
  }
  statement {
    sid    = "KinesisReadConsumers"
    effect = "Allow"
    actions = [
      "kinesis:DescribeStreamConsumer",
    ]
    resources = [
      "arn:aws:kinesis:${var.region}:${data.aws_caller_identity.main.account_id}:stream/observes-mirror/consumer/*",
      "arn:aws:kinesis:${var.region}:${data.aws_caller_identity.main.account_id}:stream/observes-mirror-historic/consumer/*",
    ]
  }
  statement {
    sid    = "Bedrock"
    effect = "Allow"
    actions = [
      "bedrock:InvokeModel",
      "bedrock:InvokeModelWithResponseStream"
    ]
    resources = [
      "arn:aws:bedrock:${var.region}::foundation-model/*",
      "arn:aws:bedrock:${var.region}:${data.aws_caller_identity.main.account_id}:provisioned-model/*"
    ]
  }
}
data "aws_iam_policy_document" "observes_general_read" {
  # NOFLUID wildcards in actions are not dangerous""
  statement {
    sid    = "generalRead"
    effect = "Allow"
    actions = [
      "ec2:Describe*",
      "ec2:Get*",
      "s3:Get*",
      "s3:List*",
    ]
    resources = ["*"]
  }
  statement {
    sid    = "generalStsRead1"
    effect = "Allow"
    actions = [
      "freetier:GetFreeTierAlertPreference",
      "freetier:GetFreeTierUsage",
      "sts:DecodeAuthorizationMessage",
      "sts:GetAccessKeyInfo",
      "sts:GetCallerIdentity",
      "sts:GetServiceBearerToken",
      "sts:GetSessionToken",
      "tag:GetResources",
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid    = "generalStsRead2"
    effect = "Allow"
    actions = [
      "sts:GetFederationToken",
    ]
    resources = [
      "arn:aws:iam::${var.accountId}:user/*"
    ]
  }
  statement {
    sid    = "generalBatchRead"
    effect = "Allow"
    actions = [
      "batch:DescribeComputeEnvironments",
      "batch:DescribeJobDefinitions",
      "batch:DescribeJobQueues",
      "batch:DescribeJobs",
      "batch:DescribeSchedulingPolicies",
      "batch:ListJobs",
      "batch:ListSchedulingPolicies",
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid    = "generalListTagsForResource"
    effect = "Allow"
    actions = [
      "batch:ListTagsForResource",
    ]
    resources = [
      "arn:aws:batch:${var.region}:${var.accountId}:compute-environment/*",
      "arn:aws:batch:${var.region}:${var.accountId}:job/*",
      "arn:aws:batch:${var.region}:${var.accountId}:job-definition/*:*",
      "arn:aws:batch:${var.region}:${var.accountId}:job-queue/*",
      "arn:aws:batch:${var.region}:${var.accountId}:scheduling-policy/*"
    ]
  }
  statement {
    sid    = "generalCloudwatchRead1"
    effect = "Allow"
    actions = [
      "cloudwatch:DescribeAlarmsForMetric",
      "cloudwatch:DescribeAnomalyDetectors",
      "cloudwatch:DescribeInsightRules",
      "cloudwatch:GetMetricData",
      "cloudwatch:GetMetricStatistics",
      "cloudwatch:GetMetricWidgetImage",
      "cloudwatch:GetTopologyMap",
      "cloudwatch:GetTopologyDiscoveryStatus",
      "cloudwatch:ListDashboards",
      "cloudwatch:ListManagedInsightRules",
      "cloudwatch:ListMetrics",
      "cloudwatch:ListMetricStreams",
      "cloudwatch:ListServices",
      "cloudwatch:ListServiceLevelObjectives",
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid    = "generalCloudwatchRead2"
    effect = "Allow"
    actions = [
      "cloudwatch:DescribeAlarms",
      "cloudwatch:DescribeAlarmHistory",
      "cloudwatch:GetInsightRuleReport",
      "cloudwatch:GetDashboard",
      "cloudwatch:GetMetricStream",
      "cloudwatch:GetService*",
      "cloudwatch:ListTagsForResource",
    ]
    resources = [
      "arn:aws:cloudwatch:${var.region}:${var.accountId}:alarm:*",
      "arn:aws:cloudwatch::${var.accountId}:dashboard/*",
      "arn:aws:cloudwatch:${var.region}:${var.accountId}:insight-rule/*",
      "arn:aws:cloudwatch:${var.region}:${var.accountId}:metric-stream/*",
      "arn:aws:cloudwatch:${var.region}:${var.accountId}:service/*-*",
      "arn:aws:cloudwatch:${var.region}:${var.accountId}:slo/*"
    ]
  }
  statement {
    sid    = "generalLogsRead1"
    effect = "Allow"
    actions = [
      "logs:DescribeAccountPolicies",
      "logs:DescribeQueries",
      "logs:DescribeQueryDefinitions",
      "logs:DescribeResourcePolicies",
      "logs:DescribeExportTasks",
      "logs:DescribeDestinations",
      "logs:DescribeDeliveries",
      "logs:DescribeDeliveryDestinations",
      "logs:DescribeDeliverySources",
      "logs:DescribeLogGroups",
      "logs:GetLogDelivery",
      "logs:ListLogDeliveries",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = "generalLogsRead2"
    effect = "Allow"
    actions = [
      "logs:Describe*Filters",
      "logs:DescribeLogStreams",
      "logs:GetDelivery*",
      "logs:GetDataProtectionPolicy",
      "logs:GetLogRecord",
      "logs:GetLogEvents",
      "logs:GetQueryResults",
      "logs:GetLogGroupFields",
      "logs:GetLogAnomalyDetector",
      "logs:Filter*",
      "logs:ListLogAnomalyDetectors",
      "logs:ListAnomalies",
      "logs:ListTags*",
    ]
    resources = [
      "arn:aws:logs:${var.region}:${var.accountId}:log-group:*",
      "arn:aws:logs:${var.region}:${var.accountId}:anomaly-detector:*",
      "arn:aws:logs:${var.region}:${var.accountId}:delivery:*",
      "arn:aws:logs:${var.region}:${var.accountId}:delivery-destination:*",
      "arn:aws:logs:${var.region}:${var.accountId}:delivery-source:*",
      "arn:aws:logs:${var.region}:${var.accountId}:log-group:*:log-stream:*",
      "arn:aws:logs:${var.region}:${var.accountId}:destination:*",
    ]
  }
  statement {
    sid    = "generalIAMRead1"
    effect = "Allow"
    actions = [
      "iam:GetAccountSummary",
      "iam:GetAccountName",
      "iam:GetAccountAuthorizationDetails",
      "iam:GetAccountPasswordPolicy",
      "iam:GetAccountEmailAddress",
      "iam:GetCloudFrontPublicKey",
      "iam:GetContextKeysForCustomPolicy",
      "iam:GetCredentialReport",
      "iam:GetOrganizationsAccessReport",
      "iam:GetServiceLastAccessedDetails",
      "iam:GetServiceLastAccessedDetailsWithEntities",
      "iam:ListAccountAliases",
      "iam:ListCloudFrontPublicKeys",
      "iam:ListGroups",
      "iam:ListInstanceProfiles",
      "iam:ListOpenIDConnectProviders",
      "iam:ListPolicies",
      "iam:ListRoles",
      "iam:ListSAMLProviders",
      "iam:ListSTSRegionalEndpointsStatus",
      "iam:ListServerCertificates",
      "iam:ListUsers",
      "iam:ListVirtualMFADevices",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = "generalIAMRead2"
    effect = "Allow"
    actions = [
      "iam:GetAccessKeyLastUsed",
      "iam:GetContextKeysForPrincipalPolicy",
      "iam:GetGroup*",
      "iam:Get*Profile",
      "iam:GetMFADevice",
      "iam:Get*Provider",
      "iam:GetPolicy*",
      "iam:GetRole*",
      "iam:GetSSHPublicKey",
      "iam:GetServerCertificate",
      "iam:GetServiceLinkedRoleDeletionStatus",
      "iam:GetUser*",
      "iam:ListAccessKeys",
      "iam:ListAttached*",
      "iam:ListEntitiesForPolicy",
      "iam:ListGroupPolicies",
      "iam:ListGroupsForUser",
      "iam:ListInstanceProfilesForRole",
      "iam:ListMFA*",
      "iam:ListPoliciesGrantingServiceAccess",
      "iam:List*Tags",
      "iam:ListPolicyVersions",
      "iam:ListRolePolicies",
      "iam:ListSSHPublicKeys",
      "iam:ListServiceSpecificCredentials",
      "iam:ListSigningCertificates",
      "iam:ListUserPolicies",
    ]
    resources = [
      "arn:aws:iam::${var.accountId}:group/*",
      "arn:aws:iam::${var.accountId}:instance-profile/*",
      "arn:aws:iam::${var.accountId}:oidc-provider/*",
      "arn:aws:iam::${var.accountId}:policy/*",
      "arn:aws:iam::${var.accountId}:role/*",
      "arn:aws:iam::${var.accountId}:saml-provider/*",
      "arn:aws:iam::${var.accountId}:server-certificate/*",
      "arn:aws:iam::${var.accountId}:user/*",
      "arn:aws:iam::${var.accountId}:mfa/*",
    ]
  }
  statement {
    sid    = "generalKMSRead1"
    effect = "Allow"
    actions = [
      "kms:CreateKey",
      "kms:DescribeCustomKeyStores",
      "kms:ListKeys",
      "kms:ListRetirableGrants",
      "kms:ListAliases",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = "generalKMSRead2"
    effect = "Allow"
    actions = [
      "kms:CreateAlias",
      "kms:DescribeKey",
      "kms:Get*",
      "kms:ListResourceTags",
      "kms:ListGrants",
      "kms:ListKeyPolicies",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:UpdateAlias",
    ]
    resources = [
      "arn:aws:kms:${var.region}:${var.accountId}:alias/*",
      "arn:aws:kms:${var.region}:${var.accountId}:key/*",
    ]
  }
}
data "aws_iam_policy_document" "observes_dynamo_read" {
  statement {
    sid    = "terraformLockWrite"
    effect = "Allow"
    actions = [
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
    ]
    resources = [
      var.terraform_state_lock_arn
    ]
  }
  statement {
    sid    = "codeEtlCheckpointsManager"
    effect = "Allow"
    actions = [
      "dynamodb:BatchGetItem",
      "dynamodb:BatchWriteItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
      "dynamodb:UpdateItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
    ]
    resources = [
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/observes_repos_checkpoints",
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/observes_repos_end_progress",
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/observes_repos_start_progress",
    ]
  }
  statement {
    sid    = "generalDynamoRead1"
    effect = "Allow"
    actions = [
      "dynamodb:DescribeEndpoints",
      "dynamodb:DescribeLimits",
      "dynamodb:DescribeReservedCapacity",
      "dynamodb:DescribeReservedCapacityOfferings",
      "dynamodb:ListBackups",
      "dynamodb:ListContributorInsights",
      "dynamodb:ListExports",
      "dynamodb:ListGlobalTables",
      "dynamodb:ListImports",
      "dynamodb:ListStreams",
      "dynamodb:ListTables",
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid    = "generalDynamoRead2"
    effect = "Allow"
    actions = [
      "dynamodb:BatchGet*",
      "dynamodb:DescribeBackup",
      "dynamodb:DescribeContinuousBackups",
      "dynamodb:DescribeContributorInsights",
      "dynamodb:DescribeExport",
      "dynamodb:DescribeGlobal*",
      "dynamodb:DescribeImport",
      "dynamodb:DescribeKinesisStreamingDestination",
      "dynamodb:DescribeStream",
      "dynamodb:DescribeTable*",
      "dynamodb:DescribeTimeToLive",
      "dynamodb:Get*",
      "dynamodb:ListTagsOfResource",
      "dynamodb:Query*",
      "dynamodb:Scan*",
    ]
    resources = [
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*",
      "arn:aws:dynamodb::${var.accountId}:global-table/*",
    ]
  }
}
data "aws_iam_policy_document" "observes_terraform_state" {
  statement {
    sid    = "terraformStateWrite"
    effect = "Allow"
    actions = [
      "s3:Create*",
      "s3:Delete*",
      "s3:Get*",
      "s3:Put*",
    ]
    resources = [
      "arn:aws:s3:::fluidattacks-terraform-states-prod/observes*",
    ]
  }
}

data "aws_iam_policy_document" "observes_secrets_read" {
  statement {
    sid    = "secretsRead"
    effect = "Allow"
    actions = [
      "secretsmanager:BatchGetSecretValue",
      "secretsmanager:GetRandomPassword",
      "secretsmanager:ListSecrets"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "secretsRead2"
    effect = "Allow"
    actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetSecretValue",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:GetResourcePolicy"
    ]
    resources = ["arn:aws:secretsmanager:${var.region}:${data.aws_caller_identity.main.account_id}:secret:*"]
  }
}

data "aws_iam_policy_document" "observes_cloudformation_permissions" {
  statement {
    sid    = "StacksPermissions"
    effect = "Allow"
    actions = [
      "cloudformation:CreateStack*",
      "cloudformation:DescribeStack*",
      "cloudformation:GetTemplateSummary",
      "cloudformation:UpdateStack*",
      "cloudformation:DeleteStack*",
      "cloudformation:List*"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "AllowCreateUser"
    effect = "Allow"
    actions = [
      "iam:CreateRole",
      "iam:TagRole"
    ]
    resources = ["arn:aws:iam::${var.accountId}:role/QuickSightAdminConsole "]
  }
}
locals {
  prod_observes = {
    policies = {
      aws = {
        ObservesEtl            = jsondecode(data.aws_iam_policy_document.observes_etl.json).Statement
        ObservesConsole        = jsondecode(data.aws_iam_policy_document.observes_console.json).Statement
        ObservesRead           = jsondecode(data.aws_iam_policy_document.observes_general_read.json).Statement
        ObservesDynamoRead     = jsondecode(data.aws_iam_policy_document.observes_dynamo_read.json).Statement
        ObservesSecretsRead    = jsondecode(data.aws_iam_policy_document.observes_secrets_read.json).Statement
        ObservesTerraformWrite = jsondecode(data.aws_iam_policy_document.observes_terraform_state.json).Statement
        ObservesCFStackCreate  = jsondecode(data.aws_iam_policy_document.observes_cloudformation_permissions.json).Statement
      }
    }
    keys = {
      prod_observes = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "prod_observes",
        ]
        tags = {
          "Name"              = "prod_observes"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "etl"
        }
      }
    }
  }
}

module "prod_observes" {
  source = "./modules/aws"

  name     = "prod_observes"
  policies = local.prod_observes.policies.aws
  tags = {
    "Name"              = "prod_observes"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
    "NOFLUID"           = "f325_wildcards_in_actions_are_not_dangerous"
  }
}

module "prod_observes_keys" {
  source   = "./modules/key"
  for_each = local.prod_observes.keys

  name       = each.key
  admins     = each.value.admins
  read_users = each.value.read_users
  users      = each.value.users
  tags       = each.value.tags
}
