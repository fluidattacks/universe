# Cross-Account role to enable CSPM
data "aws_iam_policy_document" "cspm_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "ArnEquals"
      values   = ["arn:aws:iam::205810638802:role/prod_integrates"]
      variable = "aws:PrincipalArn"
    }

    condition {
      test     = "StringEquals"
      values   = [var.cspm_external_id]
      variable = "sts:ExternalId"
    }
  }
}

resource "aws_iam_role" "cspm" {
  name                = "CSPMRole"
  assume_role_policy  = data.aws_iam_policy_document.cspm_assume_policy.json
  description         = "Role to run CSPM checks"
  managed_policy_arns = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
  tags = {
    "Name"              = "cspm_role"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "common"
  }
}
