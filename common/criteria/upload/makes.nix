{ isLinux, inputs, makeScript, projectPath, outputs, ... }: {
  jobs."/common/criteria/upload" = makeScript {
    name = "common-criteria-upload";
    replace = {
      __argCriteriaSrc__ = projectPath "/common/criteria/src";
      __argSecretsDev__ = projectPath "/common/secrets/dev.yaml";
    };
    searchPaths = {
      bin =
        [ inputs.nixpkgs.git inputs.nixpkgs.patchelf inputs.nixpkgs.poetry ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
