import asyncio
import logging
from typing import (
    Any,
    cast,
)

import aiohttp
from jinja2 import (
    Environment,
    FileSystemLoader,
)

from criteria_upload.custom_types import (
    Action,
)
from criteria_upload.utils import (
    TEMPLATES_DIR,
    get_changes,
    get_compliance,
    get_requirements,
    get_vulnerabilities,
)
from criteria_upload.zoho_desk import (
    DELAY,
    FI_ZOHO_CLIENT_ID,
    add_article,
    delete_article,
    get_article_id,
    get_categories,
    update_article,
    upload_module_intro,
)

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)


async def get_req_context(
    requirement: dict[str, Any],
    requirement_id: str,
    vulnerabilities: dict[str, Any],
    compliance: dict[str, Any],
) -> dict[str, Any]:
    vuln_refs = [
        (vuln_id, vuln["en"]["title"])
        for vuln_id, vuln in vulnerabilities.items()
        if requirement_id in vuln["requirements"]
    ]

    return {
        "src": requirement,
        "requirement_id": requirement_id,
        "compliance": compliance,
        "vulnerabilities": vuln_refs,
    }


async def generate_template(
    requirement: dict[str, Any],
    requirement_id: str,
    vulnerabilities: dict[str, Any],
    compliance: dict[str, Any],
) -> str:
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    template = env.get_template("requirements.html")
    context = await get_req_context(requirement, requirement_id, vulnerabilities, compliance)
    rendered = template.render(context)
    return rendered


async def upload_zoho_article(
    *,
    article_id: str,
    article: dict[str, Any],
    categories: dict[str, int],
    category_name: str,
    vulnerabilities: dict[str, Any],
    compliance: dict[str, Any],
    action: Action,
) -> None:
    title = article["en"]["title"]
    category = article["category"]
    subcategory = categories[category]
    LOGGER.info("Uploading file: %s", title)

    html_content = await generate_template(article, article_id, vulnerabilities, compliance)

    permalink = "criteria-" + category_name + "-" + article_id

    if action == Action.ADD:
        LOGGER.info("Adding article: %s", title)
        return await add_article(title, subcategory, permalink, html_content)

    article_id = await get_article_id(subcategory, permalink)

    return await update_article(title, subcategory, permalink, article_id, html_content)


async def upload_requirement(
    *,
    requirement_id: str,
    requirement: dict,
    categories: dict,
    module: str,
    vulnerabilities: dict,
    compliance: dict,
    action: Action,
) -> None:
    sent = False
    while not sent:
        try:
            await upload_zoho_article(
                article_id=requirement_id,
                article=requirement,
                categories=categories,
                category_name=module,
                vulnerabilities=vulnerabilities,
                compliance=compliance,
                action=action,
            )
            sent = True
        except aiohttp.ClientResponseError as exc:
            headers = cast(dict[str, str], exc.headers)
            if exc.status == 429:
                retry_after = float(headers.get("Retry-After", DELAY)) if headers else DELAY  # type: ignore[arg-type]
                LOGGER.info(
                    "Rate limit exceeded. Retrying in %s seconds.",
                    retry_after,
                )
                await asyncio.sleep(retry_after)  # type: ignore[call-overload]
            else:
                raise exc


def get_context(requirements: dict) -> dict:
    categorized: dict[str, list[str]] = {}
    for _id, requirement in requirements.items():
        categorized.setdefault(requirement["category"], []).append(_id)

    context = {
        "categorized": categorized,
        "requirements": requirements,
    }

    return context


async def upload_reqs_articles(module: str) -> bool:
    if not FI_ZOHO_CLIENT_ID:
        return False
    changes = get_changes(
        f"../src/{module}/old_data.yaml",
        f"../src/{module}/data.yaml",
    )

    categories = await get_categories(944043000006985001, module.capitalize())
    vulnerabilities = await get_vulnerabilities()
    requirements = await get_requirements()
    compliance = await get_compliance()

    for requirement_id, action in changes["modified"]:
        requirement = requirements.get(requirement_id)
        if requirement:
            await upload_requirement(
                requirement_id=requirement_id,
                requirement=requirement,
                categories=categories,
                module=module,
                vulnerabilities=vulnerabilities,
                compliance=compliance,
                action=action,
            )

    await delete_article(module, changes, categories)

    added = [item for item in changes["modified"] if item[1] == Action.ADD]
    if len(added) > 0 or len(changes["deleted"]) > 0:
        context = get_context(requirements)
        await upload_module_intro("Introduction", categories["Introduction"], module, context)

    return True
