import asyncio
import logging
from typing import (
    Any,
    cast,
)

import aiohttp
from jinja2 import (
    Environment,
    FileSystemLoader,
)

from criteria_upload.custom_types import (
    Action,
)
from criteria_upload.utils import (
    CATEGORIES_ID,
    TEMPLATES_DIR,
    get_changes,
    get_compliance,
    get_requirements,
)
from criteria_upload.zoho_desk import (
    DELAY,
    FI_ZOHO_CLIENT_ID,
    add_article,
    delete_articles,
    get_article_id,
    update_article,
    upload_module_intro,
)

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def filter_requirements(
    requirements: dict[str, Any],
    standard_id: str,
    definition_id: str,
) -> list[tuple[str, dict[str, Any]]]:
    matched_requirements = []
    for requirement_id, requirement in requirements.items():
        for reference in requirement["references"]:
            if reference == f"{standard_id}.{definition_id}":
                matched_requirements.append((requirement_id, requirement))
                break
    return matched_requirements


async def get_req_context(
    article: dict[str, Any],
    article_id: str,
    requirements: dict[str, Any],
) -> dict[str, Any]:
    filtered_requirements = {}
    for definition_id in article.get("definitions", {}).keys():
        filtered_requirements[definition_id] = filter_requirements(
            requirements,
            article_id,
            definition_id,
        )
    return {
        "src": article,
        "standard_id": article_id,
        "requirements": filtered_requirements,
    }


async def generate_template(
    article: dict[str, Any],
    article_id: str,
    requirements: dict[str, Any],
) -> str:
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    template = env.get_template("compliance.html")
    context = await get_req_context(article, article_id, requirements)
    rendered = template.render(context)
    return rendered


async def upload_zoho_article(
    *,
    article_id: str,
    article: dict[str, Any],
    category_id: int,
    category_name: str,
    requirements: dict[str, Any],
    action: Action,
) -> None:
    title = article["title"]
    LOGGER.info("Uploading file: %s", title)

    html_content = await generate_template(article, article_id, requirements)

    permalink = "criteria-" + category_name + "-" + article_id
    if action == Action.ADD:
        LOGGER.info("Adding article: %s", title)
        return await add_article(title, category_id, permalink, html_content)

    article_id = await get_article_id(category_id, permalink)

    return await update_article(title, category_id, permalink, article_id, html_content)


async def upload_compliance_article(
    *,
    compliance_id: str,
    compliance_data: dict[str, Any],
    category_id: int,
    category_name: str,
    requirements: dict[str, Any],
    action: Action,
) -> None:
    sent = False
    while not sent:
        try:
            await upload_zoho_article(
                article_id=compliance_id,
                article=compliance_data,
                category_id=category_id,
                category_name=category_name,
                requirements=requirements,
                action=action,
            )
            sent = True
        except aiohttp.ClientResponseError as exc:
            if exc.status == 429:
                headers = cast(dict[str, str], exc.headers)
                retry_after = float(headers.get("Retry-After", DELAY)) if headers else DELAY  # type: ignore[arg-type]
                LOGGER.info(
                    "Rate limit exceeded. Retrying in %s seconds.",
                    retry_after,
                )
                await asyncio.sleep(retry_after)  # type: ignore[call-overload]
            else:
                raise exc


async def delete_compliance_article(module: str, changes: dict, category: int) -> None:
    for compliance in changes["deleted"]:
        permalink = "criteria-" + module + "-" + compliance[0]

        article_id = await get_article_id(category, permalink)
        await delete_articles([article_id])


async def upload_compliance_articles(module: str) -> bool:
    if not FI_ZOHO_CLIENT_ID:
        return False
    changes = get_changes(
        f"../src/{module}/old_data.yaml",
        f"../src/{module}/data.yaml",
    )

    category_id = CATEGORIES_ID[module]
    requirements = await get_requirements()
    compliance = await get_compliance()

    for compliance_id, action in changes["modified"]:
        compliance_data = compliance.get(compliance_id)
        if compliance_data:
            await upload_compliance_article(
                compliance_id=compliance_id,
                compliance_data=compliance_data,
                category_id=category_id,
                category_name=module,
                requirements=requirements,
                action=action,
            )

    await delete_compliance_article(module, changes, category_id)

    added = [item for item in changes["modified"] if item[1] == Action.ADD]
    if len(added) > 0 or len(changes["deleted"]) > 0:
        context = {"compliance": compliance}
        await upload_module_intro("Introduction", category_id, module, context)

    return True
