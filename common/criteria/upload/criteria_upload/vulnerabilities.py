import asyncio
import logging
from typing import (
    Any,
)

import aiohttp
from cvss import (
    CVSS3,
    CVSS4,
)
from jinja2 import (
    Environment,
    FileSystemLoader,
)

from criteria_upload.custom_types import (
    Action,
)
from criteria_upload.utils import (
    LANGUAGE_MAP,
    TEMPLATES_DIR,
    get_changes,
    get_fixes,
    get_requirements,
    get_vulnerabilities,
    markdown_to_html,
)
from criteria_upload.zoho_desk import (
    DELAY,
    FI_ZOHO_CLIENT_ID,
    add_article,
    delete_article,
    get_article_id,
    get_categories,
    update_article,
    upload_module_intro,
)

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def _str(scores: tuple[float, float, float]) -> tuple[str, ...]:
    return tuple(str(score) for score in scores)


def _get_cvss3_score(vector: str) -> dict[str, dict[str, str]]:
    result: CVSS3 = CVSS3(vector)
    scores: tuple[str, ...] = _str(result.scores())
    severities: tuple[str, str, str] = result.severities()

    return {
        "score": {
            "base": scores[0],
            "temporal": scores[1],
            "environmental": scores[2],
        },
        "severity": {
            "base": severities[0],
            "temporal": severities[1],
            "environmental": severities[2],
        },
    }


def _get_cvss4_score(vector: str | None) -> dict[str, dict[str, str]] | None:
    if vector is None:
        return None

    result = CVSS4(vector)

    return {
        "score": {
            "cvss_bt": result.base_score,
        },
        "severity": {
            "cvss_bt": result.severity,
        },
    }


def _get_vector_v4_string(score: dict[str, dict[str, str]] | None) -> str | None:
    if score is None:
        return None
    attack_vector = score["base"]["attack_vector"]
    attack_complexity = score["base"]["attack_complexity"]
    attack_requirements = score["base"]["attack_requirements"]
    privileges_required = score["base"]["privileges_required"]
    user_interaction = score["base"]["user_interaction"]
    confidentiality_vc = score["base"]["confidentiality_vc"]
    integrity_vi = score["base"]["integrity_vi"]
    availability_va = score["base"]["availability_va"]
    confidentiality_sc = score["base"]["confidentiality_sc"]
    integrity_si = score["base"]["integrity_si"]
    availability_sa = score["base"]["availability_sa"]
    exploit_maturity = score["threat"]["exploit_maturity"]

    return (
        "CVSS:4.0/"
        f"AV:{attack_vector}"
        f"/AC:{attack_complexity}"
        f"/AT:{attack_requirements}"
        f"/PR:{privileges_required}"
        f"/UI:{user_interaction}"
        f"/VC:{confidentiality_vc}"
        f"/VI:{integrity_vi}"
        f"/VA:{availability_va}"
        f"/SC:{confidentiality_sc}"
        f"/SI:{integrity_si}"
        f"/SA:{availability_sa}"
        f"/E:{exploit_maturity}"
    )


def _get_vector_string(score: dict[str, dict[str, str]]) -> str:
    attack_vector = score["base"]["attack_vector"]
    attack_complexity = score["base"]["attack_complexity"]
    privileges_required = score["base"]["privileges_required"]
    user_interaction = score["base"]["user_interaction"]
    scope = score["base"]["scope"]
    confidentiality = score["base"]["confidentiality"]
    integrity = score["base"]["integrity"]
    availability = score["base"]["availability"]
    exploit_code_maturity = score["temporal"]["exploit_code_maturity"]
    remediation_level = score["temporal"]["remediation_level"]
    report_confidence = score["temporal"]["report_confidence"]
    return (
        "CVSS:3.1"
        f"/AV:{attack_vector}"
        f"/AC:{attack_complexity}"
        f"/PR:{privileges_required}"
        f"/UI:{user_interaction}"
        f"/S:{scope}"
        f"/C:{confidentiality}"
        f"/I:{integrity}"
        f"/A:{availability}"
        f"/E:{exploit_code_maturity}"
        f"/RL:{remediation_level}"
        f"/RC:{report_confidence}"
    )


async def get_vulns_context(
    vulnerability: dict[str, Any],
    vulnerability_id: str,
    requirements: dict[str, Any],
    fixes: list[dict[str, Any]],
) -> dict[str, Any]:
    vector_string = _get_vector_string(vulnerability["score"])
    cvss_score = _get_cvss3_score(vector_string)
    vector_v4_string = _get_vector_v4_string(vulnerability.get("score_v4"))
    cvss_v4_score = _get_cvss4_score(vector_v4_string)

    return {
        "src": vulnerability,
        "vulnerability_id": vulnerability_id,
        "score": cvss_score,
        "score_v4": cvss_v4_score,
        "vector_string": vector_string,
        "vector_v4_string": vector_v4_string,
        "path": vulnerability_id,
        "requirements": requirements,
        "vulnerability_fixes": [
            fix for fix in fixes if fix["vulnerability_id"] == vulnerability_id
        ],
        "languages_map": LANGUAGE_MAP,
    }


async def generate_template(
    vulnerability: dict[str, Any],
    vulnerability_id: str,
    requirements: dict[str, Any],
    fixes: list[dict[str, Any]],
) -> str:
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    env.filters["markdown_to_html"] = markdown_to_html
    template = env.get_template("vulnerabilities.html")
    context = await get_vulns_context(vulnerability, vulnerability_id, requirements, fixes)
    rendered = template.render(context)
    return rendered


async def upload_zoho_article(
    *,
    vulnerability_id: str,
    vulnerability: dict[str, Any],
    categories: dict[str, int],
    category_name: str,
    requirements: dict[str, Any],
    fixes: list[dict[str, Any]],
    action: Action,
) -> None:
    title = vulnerability["en"]["title"]
    LOGGER.info("Uploading file: %s", title)
    category = vulnerability["category"]
    subcategory = categories[category]
    permalink = "criteria-" + category_name + "-" + vulnerability_id

    html_content = await generate_template(vulnerability, vulnerability_id, requirements, fixes)

    if action == Action.ADD:
        LOGGER.info("Adding article: %s", title)
        return await add_article(title, subcategory, permalink, html_content)

    article_id = await get_article_id(subcategory, permalink)

    return await update_article(title, subcategory, permalink, article_id, html_content)


async def get_context_intro(
    vulnerabilities: dict[str, Any],
) -> dict[str, Any]:
    categorized: dict[str, list[str]] = {}
    for _id, vulnerability in vulnerabilities.items():
        categorized.setdefault(vulnerability["category"], []).append(_id)

    return {
        "categorized": categorized,
        "vulnerabilities": vulnerabilities,
    }


async def process_vulnerability(
    *,
    vulnerability_id: str,
    vulnerability: dict[str, Any],
    categories: dict[str, int],
    module: str,
    requirements: dict[str, Any],
    fixes: list[dict[str, Any]],
    action: Action,
) -> None:
    sent = False
    while not sent:
        try:
            await upload_zoho_article(
                vulnerability_id=vulnerability_id,
                vulnerability=vulnerability,
                categories=categories,
                category_name=module,
                requirements=requirements,
                fixes=fixes,
                action=action,
            )
            sent = True
        except aiohttp.ClientResponseError as exc:
            if exc.status == 429:
                LOGGER.info("Rate limit exceeded. Retrying in %s seconds.", DELAY)
                await asyncio.sleep(DELAY)  # type: ignore[call-overload]
            else:
                raise exc


async def update_articles(
    *,
    module: str,
    changes: dict,
    categories: dict[str, Any],
    vulnerabilities: dict[str, Any],
    fixes: list[dict[str, Any]],
    requirements: dict[str, Any],
) -> None:
    for vulnerability_id, action in changes["modified"]:
        vulnerability = vulnerabilities.get(vulnerability_id)
        if vulnerability:
            await process_vulnerability(
                vulnerability_id=vulnerability_id,
                vulnerability=vulnerability,
                categories=categories,
                module=module,
                requirements=requirements,
                fixes=fixes,
                action=action,
            )


async def upload_vulns_articles(module: str) -> bool:
    if not FI_ZOHO_CLIENT_ID:
        return False
    changes = get_changes(
        f"../src/{module}/old_data.yaml",
        f"../src/{module}/data.yaml",
    )

    categories = await get_categories(944043000006985001, module.capitalize())
    vulnerabilities = await get_vulnerabilities()
    fixes = await get_fixes()
    requirements = await get_requirements()

    await update_articles(
        module=module,
        changes=changes,
        categories=categories,
        vulnerabilities=vulnerabilities,
        fixes=fixes,
        requirements=requirements,
    )

    await delete_article(module, changes, categories)
    added = [item for item in changes["modified"] if item[1] == Action.ADD]
    if len(added) > 0 or len(changes["deleted"]) > 0:
        context = await get_context_intro(vulnerabilities)
        await upload_module_intro("Introduction", categories["Introduction"], module, context)

    return True
