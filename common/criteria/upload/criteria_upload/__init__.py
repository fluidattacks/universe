import asyncio
import re

from criteria_upload.compliance import (
    upload_compliance_articles,
)
from criteria_upload.errors import (
    ErrorUploadingFile,
)
from criteria_upload.fixes import (
    upload_fixes_articles,
)
from criteria_upload.requirements import (
    upload_reqs_articles,
)
from criteria_upload.vulnerabilities import (
    upload_vulns_articles,
)
from criteria_upload.zoho_desk import (
    get_article_by_id,
    update_article_body,
    update_category,
)


async def process_file_name(file_name: str) -> bool:
    match_get = re.match(r"^file-get(\d+)", file_name)
    match_update = re.match(r"^file-update(\d+)", file_name)
    match_category_update = re.match(r"^category-update(\d+)", file_name)

    if match_get:
        return await get_article_by_id(match_get.group(1))  # type: ignore[arg-type]
    if match_update:
        return await update_article_body(match_update.group(1))
    if match_category_update:
        return await update_category(match_category_update.group(1))  # type: ignore[arg-type]

    raise ErrorUploadingFile(f"No file named {file_name}")


async def main(module: str) -> bool:
    match module:
        case "vulnerabilities":
            return await upload_vulns_articles(module=module)
        case "requirements":
            return await upload_reqs_articles(module=module)
        case "compliance":
            return await upload_compliance_articles(module=module)
        case "fixes":
            return await upload_fixes_articles(module=module)
        case module if re.match(r"^(file|category)-(get|update)\d+", module):
            return await process_file_name(module)
        case _:
            raise ErrorUploadingFile(f"No module named {module}")


def run() -> None:
    import sys

    args = sys.argv[1:]
    module = args[0]
    asyncio.run(main(module=module))
