{ fromJson, fromYaml, makeDerivation, projectPath, ... }:
let
  # Load Data
  criteria = fromYaml (builtins.readFile
    (projectPath "/common/criteria/src/vulnerabilities/data.yaml"));
  skimsFindings = fromJson
    (builtins.readFile (projectPath "/skims/manifests/finding_codes.json"));

  criteriaFindings = builtins.map (x: "F${x}") (builtins.attrNames criteria);
  unsyncedFindings =
    builtins.filter (finding: !builtins.elem finding criteriaFindings)
    skimsFindings;

  areFindingsUnsynced = findings:
    if (builtins.length findings > 0) then
      abort ''

        [ERROR] Findings:
        ${builtins.concatStringsSep "\n" unsyncedFindings}
        in Skims are not in sync with the criteria''
    else
      true;
in {
  jobs."/common/criteria/test/skims-sync" = makeDerivation {
    env = { envUnsyncedFindings = areFindingsUnsynced unsyncedFindings; };
    builder = ''
      info "Criteria and Skims findings are in sync."
      touch $out
    '';
    name = "criteria-skims-sync";
  };
}
