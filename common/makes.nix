# https://github.com/fluidattacks/makes
{
  imports = [
    ./ai/makes.nix
    ./ci/makes.nix
    ./compute/makes.nix
    ./compute/schedule/parse-terraform/makes.nix
    ./compute/schedule/test/makes.nix
    ./compute/test/environment/makes.nix
    ./coverage/makes.nix
    ./criteria/makes.nix
    ./criteria/test/base/makes.nix
    ./criteria/test/skims-sync/makes.nix
    ./criteria/test/unreferenced/makes.nix
    ./criteria/upload/makes.nix
    ./dev/global_deps/makes.nix
    ./dev/clean/branches/makes.nix
    ./dev/clean/ephemerals/makes.nix
    ./dev/clean/terraform_locks/makes.nix
    ./dev/clean/cloudflare_devices/makes.nix
    ./dev/makes.nix
    ./dns/makes.nix
    ./docs/makes.nix
    ./fluidattacks-core/makes.nix
    ./foss/makes.nix
    ./gitlab/makes.nix
    ./iam/parse/makes.nix
    ./k8s/makes.nix
    ./marketplace/makes.nix
    ./iam/makes.nix
    ./observability/spyclt/makes.nix
    ./observability/makes.nix
    ./pipeline/makes.nix
    ./public_storage/makes.nix
    ./status/makes.nix
    ./test/commitlint/makes.nix
    ./test/complexipy/makes.nix
    ./test/hooks/pre-push/makes.nix
    ./test/leaks/makes.nix
    ./test/machine_sast/makes.nix
    ./test/pipeline/makes.nix
    ./test/secrets/commit-changes/makes.nix
    ./test/secrets/commit-msg/makes.nix
    ./test/secrets/equal-secrets-different-products/makes.nix
    ./test/secrets/equal-secrets-same-product/makes.nix
    ./test/secrets/forbidden/makes.nix
    ./test/secrets/makes.nix
    ./test/spelling/makes.nix
    ./test/base/makes.nix
    ./users/makes.nix
    ./utils/makes.nix
    ./vpc/makes.nix
    ./vpn/makes.nix
  ];
  secretsForAwsFromGitlab = {
    dev = {
      roleArn = "arn:aws:iam::205810638802:role/dev";
      duration = 3600;
    };
    marketplaceDeployment = {
      roleArn = "arn:aws:iam::993047037389:role/Deployment";
      duration = 3600;
    };
    marketplaceDev = {
      roleArn = "arn:aws:iam::993047037389:role/Development";
      duration = 3600;
    };
    prodCommon = {
      roleArn = "arn:aws:iam::205810638802:role/prod_common";
      duration = 3600;
    };
  };
  secretsForEnvFromSops = {
    commonCloudflare = {
      vars = [
        "CLOUDFLARE_ACCOUNT_ID"
        "CLOUDFLARE_API_KEY"
        "CLOUDFLARE_EMAIL"
        "CLOUDFLARE_OKTA_CLIENT_ID"
        "CLOUDFLARE_OKTA_CLIENT_SECRET"
        "CLOUDFLARE_TUNNEL_TOKEN"
      ];
      manifest = "/common/secrets/dev.yaml";
    };
    commonCloudflareDev = {
      vars = [ "CLOUDFLARE_OKTA_SCIM_SECRET" ];
      manifest = "/common/secrets/dev.yaml";
    };
    commonCloudflareProd = {
      vars = [ "CLOUDFLARE_OKTA_SCIM_SECRET" ];
      manifest = "/common/secrets/prod.yaml";
    };
  };
}
