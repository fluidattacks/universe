# HTTP logs
resource "cloudflare_logpush_ownership_challenge" "http" {
  account_id       = var.cloudflare.account_id
  destination_conf = "s3://${var.logs.bucket}/egress-ips/${var.name}/http?region=${var.logs.region}&sse=AES256"
}
data "aws_s3_object" "http" {
  bucket = var.logs.bucket
  key    = cloudflare_logpush_ownership_challenge.http.ownership_challenge_filename
}
resource "cloudflare_logpush_job" "http" {
  enabled             = true
  account_id          = var.cloudflare.account_id
  name                = "${var.name}.egress-ips.http"
  destination_conf    = "s3://${var.logs.bucket}/egress-ips/${var.name}/http?region=${var.logs.region}&sse=AES256"
  ownership_challenge = data.aws_s3_object.http.body
  dataset             = "gateway_http"
  frequency           = "high"

  filter = jsonencode(
    {
      where = {
        and = [
          {
            key      = "HTTPHost"
            operator = "in"
            value    = var.hosts
          },
        ]
      }
    }
  )

  logpull_options = format(
    "fields=%s&timestamps=%s",
    join(
      ",",
      [
        "AccountID",
        "Action",
        "BlockedFileHash",
        "BlockedFileName",
        "BlockedFileReason",
        "BlockedFileSize",
        "BlockedFileType",
        "Datetime",
        "DestinationIP",
        "DestinationPort",
        "DeviceID",
        "DeviceName",
        "DownloadedFileNames",
        "Email",
        "FileInfo",
        "HTTPHost",
        "HTTPMethod",
        "HTTPStatusCode",
        "HTTPVersion",
        "IsIsolated",
        "PolicyID",
        "PolicyName",
        "Referer",
        "RequestID",
        "SessionID",
        "SourceIP",
        "SourceInternalIP",
        "SourcePort",
        "URL",
        "UntrustedCertificateAction",
        "UploadedFileNames",
        "UserAgent",
        "UserID",
      ],
    ),
    "rfc3339",
  )
}
