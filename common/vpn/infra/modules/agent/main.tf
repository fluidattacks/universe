terraform {
  required_version = "~> 1.0"

  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.23.0"
    }
  }
}

# General
variable "cloudflare_account_id" {}

# Device settings policy
variable "device_settings_policy" {}
resource "cloudflare_device_settings_policy" "default" {
  account_id           = var.cloudflare_account_id
  name                 = var.device_settings_policy.name
  description          = var.device_settings_policy.description
  precedence           = var.device_settings_policy.precedence
  match                = var.device_settings_policy.match
  default              = var.device_settings_policy.default
  enabled              = var.device_settings_policy.enabled
  switch_locked        = var.device_settings_policy.switch_locked
  allow_mode_switch    = false
  allow_updates        = true
  allowed_to_leave     = false
  auto_connect         = null
  captive_portal       = 180
  service_mode_v2_mode = "warp"
}

# Split tunnel
variable "split_tunnel" {}
resource "cloudflare_split_tunnel" "default" {
  account_id = var.cloudflare_account_id
  policy_id  = cloudflare_device_settings_policy.default.id
  mode       = var.split_tunnel.mode

  dynamic "tunnels" {
    for_each = concat(
      var.split_tunnel.addresses,
      var.split_tunnel.hosts
    )

    content {
      address = contains(
        var.split_tunnel.addresses, tunnels.value
      ) ? tunnels.value : null
      host = contains(
        var.split_tunnel.hosts, tunnels.value
      ) ? tunnels.value : null
    }
  }
}

# Fallback domains
variable "fallback_domains" {}
resource "cloudflare_fallback_domain" "default" {
  account_id = var.cloudflare_account_id
  policy_id  = cloudflare_device_settings_policy.default.id

  dynamic "domains" {
    for_each = var.fallback_domains

    content {
      suffix     = domains.value.domain
      dns_server = lookup(domains.value, "servers", null)
    }
  }
}
