resource "random_password" "default" {
  length = 32
}
resource "cloudflare_tunnel" "default" {
  account_id = var.cloudflare.account_id
  name       = var.name
  secret     = base64encode(random_password.default.result)
  config_src = "cloudflare"
}
resource "cloudflare_tunnel_config" "default" {
  account_id = var.cloudflare.account_id
  tunnel_id  = cloudflare_tunnel.default.id

  config {
    warp_routing {
      enabled = true
    }
    ingress_rule {
      service = "http_status:404"
    }
  }
}
resource "cloudflare_tunnel_virtual_network" "default" {
  account_id = var.cloudflare.account_id
  name       = var.name
}
