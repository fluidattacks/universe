locals {
  # Expand all CIDRs to individual IPs
  # Inspired by https://github.com/binxio/terraform-cidr-expand
  route_ips = var.logs.logging ? distinct(flatten([
    for cidr in var.routes : [
      for n in range(pow(2, can(regex("[:]", cidr)) ? 128 : 32 - tonumber(split("/", cidr)[1])))
      : cidrhost(cidr, n)
    ]
  ])) : var.routes
}

# Network logs
resource "cloudflare_logpush_ownership_challenge" "network" {
  count            = var.logs.logging ? 1 : 0
  account_id       = var.cloudflare.account_id
  destination_conf = "s3://${var.logs.bucket}/ztna/${var.name}/network?region=${var.logs.region}&sse=AES256"
}
data "aws_s3_object" "network" {
  count  = var.logs.logging ? 1 : 0
  bucket = var.logs.bucket
  key    = cloudflare_logpush_ownership_challenge.network[count.index].ownership_challenge_filename
}
resource "cloudflare_logpush_job" "network" {
  count               = var.logs.logging ? 1 : 0
  enabled             = true
  account_id          = var.cloudflare.account_id
  name                = "${var.name}.ztna.network"
  destination_conf    = "s3://${var.logs.bucket}/ztna/${var.name}/network?region=${var.logs.region}&sse=AES256"
  ownership_challenge = data.aws_s3_object.network[count.index].body
  dataset             = "gateway_network"
  frequency           = "high"

  filter = jsonencode(
    {
      where = {
        and = [
          {
            key      = "DestinationIP"
            operator = "in"
            value    = local.route_ips
          },
          {
            key      = "PolicyID"
            operator = "in"
            value = [
              cloudflare_teams_rule.allow.id,
              cloudflare_teams_rule.block.id,
            ]
          },
        ]
      }
    }
  )

  logpull_options = format(
    "fields=%s&timestamps=%s",
    join(
      ",",
      [
        "AccountID",
        "Action",
        "Datetime",
        "DestinationIP",
        "DestinationPort",
        "DetectedProtocol",
        "DeviceID",
        "DeviceName",
        "Email",
        "OverrideIP",
        "OverridePort",
        "PolicyID",
        "PolicyName",
        "SNI",
        "SessionID",
        "SourceIP",
        "SourceInternalIP",
        "SourcePort",
        "Transport",
        "UserID",
      ],
    ),
    "rfc3339",
  )
}

# HTTP logs
resource "cloudflare_logpush_ownership_challenge" "http" {
  count            = var.logs.logging ? 1 : 0
  account_id       = var.cloudflare.account_id
  destination_conf = "s3://${var.logs.bucket}/ztna/${var.name}/http?region=${var.logs.region}&sse=AES256"
}
data "aws_s3_object" "http" {
  count  = var.logs.logging ? 1 : 0
  bucket = var.logs.bucket
  key    = cloudflare_logpush_ownership_challenge.http[count.index].ownership_challenge_filename
}
resource "cloudflare_logpush_job" "http" {
  count               = var.logs.logging ? 1 : 0
  enabled             = true
  account_id          = var.cloudflare.account_id
  name                = "${var.name}.ztna.http"
  destination_conf    = "s3://${var.logs.bucket}/ztna/${var.name}/http?region=${var.logs.region}&sse=AES256"
  ownership_challenge = data.aws_s3_object.http[count.index].body
  dataset             = "gateway_http"
  frequency           = "high"

  filter = jsonencode(
    {
      where = {
        and = [
          {
            key      = "DestinationIP"
            operator = "in"
            value    = local.route_ips
          },
          {
            key      = "PolicyID"
            operator = "in"
            value = [
              cloudflare_teams_rule.allow.id,
              cloudflare_teams_rule.block.id,
            ]
          },
        ]
      }
    }
  )

  logpull_options = format(
    "fields=%s&timestamps=%s",
    join(
      ",",
      [
        "AccountID",
        "Action",
        "BlockedFileHash",
        "BlockedFileName",
        "BlockedFileReason",
        "BlockedFileSize",
        "BlockedFileType",
        "Datetime",
        "DestinationIP",
        "DestinationPort",
        "DeviceID",
        "DeviceName",
        "DownloadedFileNames",
        "Email",
        "FileInfo",
        "HTTPHost",
        "HTTPMethod",
        "HTTPStatusCode",
        "HTTPVersion",
        "IsIsolated",
        "PolicyID",
        "PolicyName",
        "Referer",
        "RequestID",
        "SessionID",
        "SourceIP",
        "SourceInternalIP",
        "SourcePort",
        "URL",
        "UntrustedCertificateAction",
        "UploadedFileNames",
        "UserAgent",
        "UserID",
      ],
    ),
    "rfc3339",
  )
}

# Session logs
resource "cloudflare_logpush_ownership_challenge" "session" {
  count            = var.logs.logging ? 1 : 0
  account_id       = var.cloudflare.account_id
  destination_conf = "s3://${var.logs.bucket}/ztna/${var.name}/session?region=${var.logs.region}&sse=AES256"
}
data "aws_s3_object" "session" {
  count  = var.logs.logging ? 1 : 0
  bucket = var.logs.bucket
  key    = cloudflare_logpush_ownership_challenge.session[count.index].ownership_challenge_filename
}
resource "cloudflare_logpush_job" "session" {
  count               = var.logs.logging ? 1 : 0
  enabled             = true
  account_id          = var.cloudflare.account_id
  name                = "${var.name}.ztna.session"
  destination_conf    = "s3://${var.logs.bucket}/ztna/${var.name}/session?region=${var.logs.region}&sse=AES256"
  ownership_challenge = data.aws_s3_object.session[count.index].body
  dataset             = "zero_trust_network_sessions"
  frequency           = "high"

  filter = jsonencode(
    {
      where = {
        and = [
          {
            key      = "OriginIP"
            operator = "in"
            value    = local.route_ips
          },
          {
            key      = "VirtualNetworkID"
            operator = "in"
            value    = [cloudflare_tunnel_virtual_network.default.id]
          },
        ]
      }
    }
  )

  logpull_options = format(
    "fields=%s&timestamps=%s",
    join(
      ",",
      [
        "AccountID",
        "BytesReceived",
        "BytesSent",
        "ClientTCPHandshakeDurationMs",
        "ClientTLSCipher",
        "ClientTLSHandshakeDurationMs",
        "ClientTLSVersion",
        "ConnectionCloseReason",
        "ConnectionReuse",
        "DestinationTunnelID",
        "DetectedProtocol",
        "DeviceID",
        "DeviceName",
        "EgressColoName",
        "EgressIP",
        "EgressPort",
        "EgressRuleID",
        "EgressRuleName",
        "Email",
        "IngressColoName",
        "Offramp",
        "OriginIP",
        "OriginPort",
        "OriginTLSCertificateIssuer",
        "OriginTLSCertificateValidationResult",
        "OriginTLSCipher",
        "OriginTLSHandshakeDurationMs",
        "OriginTLSVersion",
        "Protocol",
        "RuleEvaluationDurationMs",
        "SessionEndTime",
        "SessionID",
        "SessionStartTime",
        "SourceIP",
        "SourceInternalIP",
        "SourcePort",
        "UserID",
        "VirtualNetworkID",
      ],
    ),
    "rfc3339",
  )
}
