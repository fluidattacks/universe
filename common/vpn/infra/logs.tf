# Bucket
resource "aws_s3_bucket" "logpush" {
  bucket = "logpush.fluidattacks.com"

  tags = {
    "Name"              = "logpush.fluidattacks.com"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "Access"            = "private"
    "NOFLUID"           = "f335_due_to_generation_significant_volume_data"
  }
}
resource "aws_s3_bucket_logging" "logpush" {
  bucket = aws_s3_bucket.logpush.id

  target_bucket = "common.logging"
  target_prefix = "log/${aws_s3_bucket.logpush.bucket}"
}
resource "aws_s3_bucket_versioning" "logpush" {
  bucket = aws_s3_bucket.logpush.id
  versioning_configuration {
    // NOFLUID Logs are disabled due to their generation of a significant volume of data.
    status = "Suspended"
  }
}
resource "aws_s3_bucket_ownership_controls" "logpush" {
  bucket = aws_s3_bucket.logpush.id

  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}
resource "aws_s3_bucket_public_access_block" "logpush" {
  bucket = aws_s3_bucket.logpush.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
resource "aws_s3_bucket_server_side_encryption_configuration" "logpush" {
  bucket = aws_s3_bucket.logpush.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}
resource "aws_s3_bucket_policy" "logpush" {
  bucket = aws_s3_bucket.logpush.id

  policy = jsonencode(
    {
      Version : "2012-10-17",
      Statement : [
        {
          Action : ["s3:PutObject"],
          Effect : "Allow",
          Principal : {
            AWS : ["arn:aws:iam::391854517948:user/cloudflare-logpush"]
          },
          Resource : "${aws_s3_bucket.logpush.arn}/*",
          Sid : "Allow Cloudflare logpush",
        }
      ]
    }
  )
}
