data "cloudflare_access_application" "warp" {
  account_id = var.cloudflare_account_id
  domain     = "${cloudflare_access_organization.default.auth_domain}/warp"
}

# Okta
resource "cloudflare_access_identity_provider" "okta" {
  account_id = var.cloudflare_account_id
  name       = "Okta"
  type       = "okta"

  config {
    client_id     = var.cloudflare_okta_client_id
    client_secret = var.cloudflare_okta_client_secret
    okta_account  = "https://fluidattacks.okta.com"
    pkce_enabled  = true
  }

  scim_config {
    enabled                  = true
    group_member_deprovision = true
    seat_deprovision         = true
    secret                   = var.cloudflare_okta_scim_secret
    user_deprovision         = true
  }
}
resource "cloudflare_access_policy" "okta" {
  account_id     = var.cloudflare_account_id
  application_id = data.cloudflare_access_application.warp.id
  name           = "Okta"
  precedence     = "1"
  decision       = "allow"

  include {
    email_domain = [
      "@fluidattacks.com",
      "@kernelship.com",
    ]
  }
}

# Compute
resource "cloudflare_access_service_token" "compute-2024_08_09" {
  account_id           = var.cloudflare_account_id
  name                 = "Compute - 2024-08-09"
  min_days_for_renewal = 0
}
resource "cloudflare_access_service_token" "compute-2024_08_08" {
  account_id           = var.cloudflare_account_id
  name                 = "Compute - 2024-08-08"
  min_days_for_renewal = 0
}
output "cloudflare_warp_token_compute_id" {
  value     = cloudflare_access_service_token.compute-2024_08_09.client_id
  sensitive = true
}
output "cloudflare_warp_token_compute_secret" {
  value     = cloudflare_access_service_token.compute-2024_08_09.client_secret
  sensitive = true
}
output "cloudflare_warp_token_compute-2024_08_08_id" {
  value     = cloudflare_access_service_token.compute-2024_08_08.client_id
  sensitive = true
}
output "cloudflare_warp_token_compute-2024_08_08_secret" {
  value     = cloudflare_access_service_token.compute-2024_08_08.client_secret
  sensitive = true
}

resource "cloudflare_access_policy" "compute" {
  account_id     = var.cloudflare_account_id
  application_id = data.cloudflare_access_application.warp.id
  name           = "Compute"
  precedence     = "2"
  decision       = "non_identity"

  include {
    service_token = [cloudflare_access_service_token.compute-2024_08_08.id]
  }
}
