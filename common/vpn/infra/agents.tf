# Warp Device settings
locals {
  default = {
    # Required format to simplify dynamic blocks
    # in cloudflare_fallback_domain resources
    fallback_domains = [
      { domain = "intranet" },
      { domain = "internal" },
      { domain = "private" },
      { domain = "localdomain" },
      { domain = "domain" },
      { domain = "lan" },
      { domain = "home" },
      { domain = "host" },
      { domain = "corp" },
      { domain = "local" },
      { domain = "localhost" },
      { domain = "home.arpa" },
      { domain = "invalid" },
      { domain = "test" },
      { domain = "intranet" },
      { domain = "intranet" },
    ],
    split_tunnel = {
      mode = "exclude"
      addresses = concat(
        # Default
        [
          "100.64.0.0/10",
          "169.254.0.0/16",
          "224.0.0.0/24",
          "240.0.0.0/4",
          "255.255.255.255/32",
          "fe80::/10",
          "fd00::/8",
          "ff01::/16",
          "ff02::/16",
          "ff03::/16",
          "ff04::/16",
          "ff05::/16",
          "17.0.0.0/8"
        ],
        # WARP + BURP + MOBILE
        [
          # Office reserved IPs
          "192.168.100.60/32",
          "192.168.100.61/32",
          "192.168.100.62/32",
          "192.168.100.63/32",
          "192.168.100.64/32",

          # Home reserved IPs
          "192.168.0.60/32",
          "192.168.0.61/32",
          "192.168.0.62/32",
          "192.168.1.60/32",
          "192.168.1.61/32",
          "192.168.1.62/32",
          "192.168.10.60/32",
          "192.168.10.61/32",
          "192.168.10.62/32",
        ],
      )
      hosts = [
        "fluidattacks.jamfcloud.com",
        "use1-jcdsdownloads.services.jamfcloud.com",
        "use1-jcds.services.jamfcloud.com",
      ]
    }
  }
  agents = {
    ztna = {
      device_settings_policy = {
        name        = "ZTNA"
        description = "Policy for accessing ZTNA tunnels"
        precedence  = 10
        match = format(
          "any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"})",
          local.okta.groups.fluid_security_tester,
          local.okta.groups.fluid_security_developer,
          local.okta.groups.fluid_engagement_manager,
          local.okta.groups.fluid_team_it,
        )
        default       = false
        enabled       = true
        switch_locked = false
      }
      split_tunnel = {
        mode = "exclude"
        addresses = concat(
          local.default.split_tunnel.addresses,
          [],
        )
        hosts = concat(
          local.default.split_tunnel.hosts,
          [],
        )
      }
      fallback_domains = concat(
        # Default fallback routes
        local.default.fallback_domains,
        # All tunnel DNS
        local.ztna_data_dns
      )
    }

    strict = {
      device_settings_policy = {
        name        = "strict"
        description = "Policy for some groups at Fluid Attacks"
        precedence  = 20
        match = join(
          " or ",
          [
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_analytics),
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_design),
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_engagement_manager),
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_marketing),
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_product),
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_prospecting),
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_security_developer),
            format("any(identity.groups.name[*] in {\"%s\"})", local.okta.groups.fluid_warp),
          ],
        )
        default       = false
        enabled       = true
        switch_locked = true
      }
      split_tunnel = {
        mode = "exclude"
        addresses = concat(
          local.default.split_tunnel.addresses,
          [],
        )
        hosts = concat(
          local.default.split_tunnel.hosts,
          [],
        )
      }
      fallback_domains = concat(
        # Default fallback routes
        local.default.fallback_domains,
      )
    }

    people = {
      device_settings_policy = {
        name          = "People"
        description   = "Policy for everyone at Fluid Attacks"
        precedence    = 30
        match         = "identity.email in {\"@fluidattacks.com\"}"
        default       = false
        enabled       = true
        switch_locked = false
      }
      split_tunnel = {
        mode = "exclude"
        addresses = concat(
          local.default.split_tunnel.addresses,
          [],
        )
        hosts = concat(
          local.default.split_tunnel.hosts,
          [],
        )
      }
      fallback_domains = concat(
        # Default fallback routes
        local.default.fallback_domains,
      )
    }

    compute = {
      device_settings_policy = {
        name          = "Compute"
        description   = "Policy for AWS Batch automated tasks"
        precedence    = 40
        match         = "identity.email == \"non_identity@fluidattacks.cloudflareaccess.com\""
        default       = false
        enabled       = true
        switch_locked = false
      }
      split_tunnel = {
        mode = "include"
        addresses = concat(
          # All tunnel routes
          distinct(flatten([for _, tunnel in local.ztna_data : tunnel.routes])),
          # All tunnel dns server routes
          distinct(formatlist(
            "%s/32",
            flatten([
              for _, dns in
              local.ztna_data_dns
              : dns.servers
            ]),
          )),
          # All Egress IPs
          distinct(formatlist(
            "%s/32",
            flatten([
              for _, org in
              local.egress_ips_data
              : org.servers
            ]),
          )),
        )
        hosts = concat(
          # Default required by Cloudflare WARP
          ["fluidattacks.cloudflareaccess.com"],
          # All tunnel domains
          distinct([
            for dns in
            local.ztna_data_dns
            : dns.domain
          ]),
          # All Egress IPs domains
          distinct(
            flatten([
              for org in
              local.egress_ips_data
              : org.domains
          ])),
        )
      }
      fallback_domains = concat(
        # Default fallback routes
        local.default.fallback_domains,
        # All tunnel DNS
        local.ztna_data_dns
      )
    }
    default = {
      device_settings_policy = {
        name          = "Default"
        description   = "Policy for everyone at Fluid Attacks"
        precedence    = null # Not allowed for default
        match         = null # Not allowed for default
        default       = true
        enabled       = true
        switch_locked = false
      }
      split_tunnel     = local.default.split_tunnel
      fallback_domains = local.default.fallback_domains
    }
  }
}

module "agents" {
  for_each = local.agents
  source   = "./modules/agent"

  cloudflare_account_id  = var.cloudflare_account_id
  device_settings_policy = each.value.device_settings_policy
  split_tunnel           = each.value.split_tunnel
  fallback_domains       = each.value.fallback_domains
}
