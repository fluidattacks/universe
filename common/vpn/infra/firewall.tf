# Malware
resource "cloudflare_teams_rule" "block_malware" {
  account_id  = var.cloudflare_account_id
  name        = "Malware Block"
  description = "Block malware DNS requests"
  precedence  = 9000
  action      = "block"
  filters     = ["dns"]
  traffic     = "any(dns.security_category[*] in {117})"
  enabled     = true

  rule_settings {
    block_page_enabled = true
    block_page_reason  = "This site was detected as Malware"
  }
}

# AWS VPC
resource "cloudflare_teams_rule" "aws_allow" {
  account_id  = var.cloudflare_account_id
  name        = "aws.allow"
  description = "Default allow authorization rule for aws"
  precedence  = 9999
  enabled     = true
  action      = "allow"
  filters     = ["l4"]
  traffic     = "net.vnet_id == \"${cloudflare_tunnel_virtual_network.aws.id}\""
  identity = format(
    "any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"})",
    local.okta.groups.fluid_security_developer,
    local.okta.groups.fluid_finance_def,
    local.okta.groups.fluid_team_it,
    local.okta.groups.fluid_analytics,
  )
  rule_settings {
    check_session {
      enforce  = true
      duration = "168h0m0s"
    }
  }
}
resource "cloudflare_teams_rule" "aws_block" {
  account_id  = var.cloudflare_account_id
  name        = "aws.block"
  description = "Default block authorization rule for aws"
  precedence  = 10000
  enabled     = true
  action      = "block"
  filters     = ["l4"]
  traffic     = "net.vnet_id == \"${cloudflare_tunnel_virtual_network.aws.id}\""

  rule_settings {
    block_page_enabled = true
    block_page_reason  = "You must be part of Fluid Attacks - Development group at Okta to access this resource"
  }
}
