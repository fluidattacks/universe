# Shared virtual network
resource "cloudflare_tunnel_virtual_network" "egress_ips" {
  account_id = var.cloudflare_account_id
  name       = "egress-ips"
}

# Routes
resource "cloudflare_teams_rule" "egress_ips" {
  account_id  = var.cloudflare_account_id
  name        = "egress-ips"
  description = "Use reserved Egress IPs"
  precedence  = 9998
  enabled     = true
  action      = "egress"
  filters     = ["egress"]
  traffic     = "net.vnet_id == \"${cloudflare_tunnel_virtual_network.egress_ips.id}\""
  identity = format(
    "identity.email == \"%s\" or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"})",
    local.cloudflare_default_email,
    local.okta.groups.fluid_security_tester,
    local.okta.groups.fluid_security_developer,
    local.okta.groups.fluid_engagement_manager,
    local.okta.groups.fluid_finance_def,
    local.okta.groups.fluid_team_it,
  )
  rule_settings {
    egress {
      ipv4          = "104.30.132.78"
      ipv6          = "2a09:bac0:1000:252::/64"
      ipv4_fallback = "104.30.134.27"
    }
  }
}
resource "cloudflare_teams_rule" "default_egress" {
  account_id  = var.cloudflare_account_id
  name        = "default - egress"
  description = "Use Cloudflare shared IPs"
  precedence  = 10000
  enabled     = true
  action      = "egress"
  filters     = ["egress"]
  traffic     = "not(net.dst.ip == 0.0.0.0)"
}
