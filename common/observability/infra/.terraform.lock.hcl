# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/coralogix/coralogix" {
  version     = "1.18.7"
  constraints = "1.18.7"
  hashes = [
    "h1:PGlmMkkWJr2Xed0NACMh5SMwsSIjP1HyhkJRBQ3jcCM=",
    "zh:03120f08143b382df94e06bd2c039af808c811d8d115b26aab163cd9ad46dc27",
    "zh:0ceef5574f62ac9ef3efd9ce55a728344b461d7f592dd85e52c770d8ae850da7",
    "zh:1c4e35e3af1f45968ebcd25ff257fa280811e21aa5362c7d3763e2af601cda4c",
    "zh:1e670b88c3604f8b94f408271e66c1679eb1c504b6d371a32fc4c280e7990b4e",
    "zh:1e9ea759e68932e37d582465b773b88448156c1c9458181b36a2ddcc13efde2d",
    "zh:26eb8a5099d06b11ce78a38d284b423617bf2946fbf8f3d1622da8581276c387",
    "zh:2f5f948a09add33b096cee5b811714c77176f3bfc2178799ec90b40d00e1434f",
    "zh:3aec0e796560303611431b4ef985a20bc840628c17072070e0fe8d8cab992b06",
    "zh:60bdef09e0c045e75a1cc95004a77c60187d5d2c9b69c6be3775d4e8efd5e86c",
    "zh:61bb5e36216bda6dce4637cfc6dcf5752eccf03a1e0023fc858b126ce3cfd187",
    "zh:647f0314e100192f1e86ad931fe4fb4b645d06b15bf9a7f0d1d30eeb96f5cf56",
    "zh:e0642df7c3bf87ff910bbd6ea718fd044b2d45cc3cc6ba25bada5259e1b81238",
    "zh:e18add1ec58dbb999be4a3778f00bdecfd13c082caf7bb540a563d3d4ff36300",
    "zh:fa06ace38d5dc459d4ff3e8095e7f6cfec4dabfe7a299fded36f3354d25b8d95",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "5.84.0"
  constraints = "5.84.0"
  hashes = [
    "h1:OJ53RNte7HLHSMxSkzu1S6H8sC0T8qnCAOcNLjjtMpc=",
    "zh:078f77438aba6ec8bf9154b7d223e5c71c48d805d6cd3bcf9db0cc1e82668ac3",
    "zh:1f6591ff96be00501e71b792ed3a5a14b21ff03afec9a1c4a3fd9300e6e5b674",
    "zh:2ab694e022e81dd74485351c5836148a842ed71cf640664c9d871cb517b09602",
    "zh:33c8ccb6e3dc496e828a7572dd981366c6271075c1189f249b9b5236361d7eff",
    "zh:6f31068ebad1d627e421c72ccdaafe678c53600ca73714e977bf45ff43ae5d17",
    "zh:7488623dccfb639347cae66f9001d39cf06b92e8081975235a1ac3a0ac3f44aa",
    "zh:7f042b78b9690a8725c95b91a70fc8e264011b836605bcc342ac297b9ea3937d",
    "zh:88b56ac6c7209dc0a775b79975a371918f3aed8f015c37d5899f31deff37c61a",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a1979ba840d704af0932f8de5f541cbb4caa9b6bbd25ed552a24e6772175ba07",
    "zh:b058c0533dae580e69d1adbc1f69e6a80632374abfc10e8634d06187a108e87b",
    "zh:c88610af9cf957f8dcf4382e0c9ca566ef10e3290f5de01d4d90b2d81b078aa8",
    "zh:e9562c055a2247d0c287772b55abef468c79f8d66a74780fe1c5e5dae1a284a9",
    "zh:f7a7c71d28441d925a25c08c4485c015b2d9f0338bc9707443e91ff8e161d3d9",
    "zh:fee533e81976d0900aa6fa443dc54ef171cbd901847f28a6e8edb1d161fa6fde",
  ]
}
