{ inputs, makeScript, makePythonEnvironment, outputs, projectPath, ... }: {
  jobs."/common/observability/spyclt" = makeScript {
    name = "common-spyderbat-notifications";
    replace = { __argCommonSecrets__ = projectPath "/common/secrets"; };
    searchPaths = {
      source = [
        (makePythonEnvironment {
          pythonVersion = "3.11";
          pythonProjectDir = ./.;
        })
        outputs."/common/utils/sops"
        outputs."/common/utils/aws"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
