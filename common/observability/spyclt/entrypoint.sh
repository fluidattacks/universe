# shellcheck shell=bash

function main {
  aws_login "prod_common" "3600" \
    && sops_export_vars __argCommonSecrets__/dev.yaml SPYDERBAT_API_TOKEN \
    && spyctl config set-apisecret \
      -k "$SPYDERBAT_API_TOKEN" \
      -u "https://api.spyderbat.com" \
      spyder_fluid_secret \
    && spyctl config set-context --org "Fluid Attacks" --secret spyder_fluid_secret fluid_context \
    && spyctl config view \
    && spyctl apply -f common/observability/spyclt/target.yaml \
    && spyctl apply -f common/observability/spyclt/configuration.yaml \
    && spyctl apply -f common/observability/spyclt/policy.yaml \
    || return 1
}

main "${@}"
