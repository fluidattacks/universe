# https://github.com/fluidattacks/makes
{ inputs, makeSearchPaths, outputs, ... }:
let
  searchPaths =
    makeSearchPaths { bin = [ inputs.nixpkgs.awscli inputs.nixpkgs.git ]; };
in {
  deployTerraform = {
    modules = {
      commonObservability = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonObservability"
          outputs."/secretsForTerraformFromEnv/commonObservability"
        ];
        src = "/common/observability/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonObservability = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonObservability"
          outputs."/secretsForTerraformFromEnv/commonObservability"
        ];
        src = "/common/observability/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonObservability = {
      vars = [ "CORALOGIX_DOMAIN" "CORALOGIX_ALERTS_API_KEY" ];
      manifest = "/common/secrets/dev.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    commonObservability = {
      coralogixDomain = "CORALOGIX_DOMAIN";
      coralogixAlertsApiKey = "CORALOGIX_ALERTS_API_KEY";
    };
  };
  testTerraform = {
    modules = {
      commonObservability = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonObservability"
          outputs."/secretsForTerraformFromEnv/commonObservability"
        ];
        src = "/common/observability/infra";
        version = "1.0";
      };
    };
  };
}
