variable "coralogixDomain" {
  type = string
}
variable "coralogixOtelApiKey" {
  type = string
}
variable "coralogixAccountId" {
  type = string
}

locals {
  kube_namespace = "kube-system"
}

# Auth

data "aws_iam_role" "monitoring" {
  name = local.monitoring_role
}

resource "kubernetes_service_account" "monitoring" {
  automount_service_account_token = true
  metadata {
    name      = local.monitoring_role
    namespace = local.kube_namespace

    annotations = {
      "eks.amazonaws.com/role-arn" = data.aws_iam_role.monitoring.arn
    }
  }
}

resource "kubernetes_cluster_role" "monitoring" {
  metadata {
    name = local.monitoring_role
  }

  rule {
    api_groups = [""]
    resources = [
      "endpoints",
      "nodes",
      "nodes/metrics",
      "nodes/proxy",
      "pods",
      "services"
    ]
    verbs = [
      "get",
      "list",
      "watch"
    ]
  }

  rule {
    non_resource_urls = ["/metrics"]
    verbs             = ["get"]
  }
}

resource "kubernetes_cluster_role_binding" "monitoring" {
  metadata {
    name = local.monitoring_role
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.monitoring.metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.monitoring.metadata[0].name
    namespace = local.kube_namespace
  }
}


data "aws_iam_policy_document" "keda_service_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]

    condition {
      test     = "StringEquals"
      variable = "${replace(module.cluster.cluster_oidc_issuer_url, "https://", "")}:aud"
      values   = ["sts.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "${replace(module.cluster.cluster_oidc_issuer_url, "https://", "")}:sub"
      values   = ["system:serviceaccount:kube-system:keda-operator"]
    }
    principals {
      type        = "Federated"
      identifiers = ["arn:aws:iam::205810638802:oidc-provider/${replace(module.cluster.cluster_oidc_issuer_url, "https://", "")}"]
    }

    effect = "Allow"
  }
}

resource "aws_iam_role" "keda-service-account" {
  name               = "keda_service_account"
  assume_role_policy = data.aws_iam_policy_document.keda_service_role_policy.json
  tags = {
    "Name"              = "keda-service-account"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_iam_role_policy_attachment" "keda-attach-service-account" {
  role       = aws_iam_role.keda-service-account.name
  policy_arn = aws_iam_policy.autoscaler.arn
}


resource "kubernetes_service_account" "keda-operator" {
  metadata {
    name      = "keda-operator"
    namespace = local.kube_namespace

    labels = {
      "app.kubernetes.io/component"  = "operator"
      "app.kubernetes.io/instance"   = "keda-autoscaler"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/name"       = "keda-operator"
      "app.kubernetes.io/part-of"    = "keda-operator"
      "app.kubernetes.io/version"    = "2.9.2"
      "helm.sh/chart"                = "keda-2.9.3"
    }
    annotations = {
      "eks.amazonaws.com/role-arn"     = aws_iam_role.keda-service-account.arn
      "meta.helm.sh/release-name"      = "keda-autoscaler"
      "meta.helm.sh/release-namespace" = "kube-system"
    }
  }

  automount_service_account_token = true
}

resource "kubernetes_secret_v1" "otel-collector-for-k8s" {
  metadata {
    name      = "coralogix-keys"
    namespace = local.kube_namespace
  }

  data = {
    PRIVATE_KEY = var.coralogixOtelApiKey
  }
}

resource "helm_release" "otel-coralogix-integration" {
  name                  = "otel-coralogix-integration"
  description           = "Coralogix collector for k8s pods and services"
  repository            = "https://cgx.jfrog.io/artifactory/coralogix-charts-virtual"
  chart                 = "otel-integration"
  version               = "0.0.136"
  namespace             = local.kube_namespace
  cleanup_on_fail       = true
  render_subchart_notes = true
  atomic                = true
  timeout               = 300

  set {
    name  = "global.domain"
    value = var.coralogixDomain
  }

  set {
    name  = "global.clusterName"
    value = "common-k8s"
  }

  depends_on = [kubernetes_secret_v1.otel-collector-for-k8s]
}

# Bucket where all the coralogix data will be stored
resource "aws_s3_bucket" "coralogix_data_archive" {
  provider = aws.west_2
  bucket   = "coralogix-data-archive"

  lifecycle {
    prevent_destroy = true
  }
  tags = {
    "Name"              = "coralogix_data_archive_bucket"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "coralogix_data_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_data_archive.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "coralogix_data_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_data_archive.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
resource "aws_s3_bucket_versioning" "coralogix_data_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_data_archive.id

  versioning_configuration {
    status = "Enabled"
  }
}
resource "aws_s3_bucket_logging" "coralogix_data_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_data_archive.id

  target_bucket = aws_s3_bucket.coralogix_logging.id
  target_prefix = "log/${aws_s3_bucket.coralogix_data_archive.id}"
}
data "aws_iam_policy_document" "coralogix_data_archive" {
  statement {
    sid    = "CoralogixData"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.coralogixAccountId}:root"]
    }
    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
      "s3:PutObjectTagging",
      "s3:GetObjectTagging"
    ]
    resources = [
      aws_s3_bucket.coralogix_data_archive.arn,
      "${aws_s3_bucket.coralogix_data_archive.arn}/*",
    ]
  }
}

resource "aws_s3_bucket_policy" "coralogix_data_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_data_archive.id
  policy   = data.aws_iam_policy_document.coralogix_data_archive.json
}

# Bucket where all the coralogix metrics will be stored
resource "aws_s3_bucket" "coralogix_metrics_archive" {
  provider = aws.west_2
  bucket   = "coralogix-metrics-archive"

  lifecycle {
    prevent_destroy = true
  }
  tags = {
    "Name"              = "coralogix_metrics_archive_bucket"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "coralogix_metrics_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_metrics_archive.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}
resource "aws_s3_bucket_logging" "coralogix_metrics_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_metrics_archive.id

  target_bucket = aws_s3_bucket.coralogix_logging.id
  target_prefix = "log/${aws_s3_bucket.coralogix_metrics_archive.id}"
}
resource "aws_s3_bucket_public_access_block" "coralogix_metrics_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_metrics_archive.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
resource "aws_s3_bucket_versioning" "coralogix_metrics_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_metrics_archive.id

  versioning_configuration {
    status = "Enabled"
  }
}

data "aws_iam_policy_document" "coralogix_metrics_archive" {
  statement {
    sid    = "CoralogixMetrics"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.coralogixAccountId}:root"]
    }
    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
      "s3:DeleteObject",
    ]
    resources = [
      aws_s3_bucket.coralogix_metrics_archive.arn,
      "${aws_s3_bucket.coralogix_metrics_archive.arn}/*",
    ]
  }
}

resource "aws_s3_bucket_policy" "coralogix_metrics_archive" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_metrics_archive.id
  policy   = data.aws_iam_policy_document.coralogix_metrics_archive.json
}

resource "aws_s3_bucket" "coralogix_logging" {
  provider = aws.west_2
  bucket   = "coralogix.logging"

  lifecycle {
    prevent_destroy = true
  }
  tags = {
    "Name"              = "coralogix.logging"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "common"
    "Access"            = "private"
  }
}
#Bucket versioning
resource "aws_s3_bucket_versioning" "coralogix_logging_versioning" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_logging.id

  versioning_configuration {
    status = "Enabled"
  }
}
resource "aws_s3_bucket_policy" "coralogix_logging_policy" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_logging.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "logging.s3.amazonaws.com"
        },
        Action   = "s3:PutObject",
        Resource = "arn:aws:s3:::coralogix.logging/*"
      }
    ]
  })
}
resource "aws_s3_bucket_lifecycle_configuration" "coralogix_logging" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_logging.id

  rule {
    id     = "delete_logs"
    status = "Enabled"

    expiration {
      days = 180
    }
  }
}
resource "aws_s3_bucket_logging" "coralogix_logging" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_logging.id

  target_bucket = aws_s3_bucket.coralogix_data_archive.id
  target_prefix = "log/${aws_s3_bucket.coralogix_logging.id}"
}
resource "aws_s3_bucket_server_side_encryption_configuration" "coralogix_logging" {
  provider = aws.west_2
  bucket   = aws_s3_bucket.coralogix_logging.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}
