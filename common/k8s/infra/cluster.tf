locals {
  worker_groups = {
    core = {
      instance_types = [
        "r6g.large",
        "r6gd.large",
        "r7g.large",
        "r7gd.large",
        "r8g.large",
        "x2gd.large",
      ]
      tags = {
        "fluidattacks:line"  = "cost"
        "fluidattacks:comp"  = "common"
        "fluidattacks:stack" = "k8s"
      }
    }
    dev = {
      max_size  = 100
      disk_size = 50
      instance_types = [
        "r6g.large",
        "r6gd.large",
        "r7g.large",
        "r7gd.large",
        "r8g.large",
        "x2gd.large",
      ]
      tags = {
        "fluidattacks:line"  = "research"
        "fluidattacks:comp"  = "integrates"
        "fluidattacks:stack" = "k8s"
      }
    }
    prod_integrates = {
      max_size = 120
      instance_types = [
        "r6g.medium",
        "r6gd.medium",
        "r7g.medium",
        "r7gd.medium",
        "r8g.medium",
        "x2gd.medium",
      ]
      tags = {
        "fluidattacks:line"  = "cost"
        "fluidattacks:comp"  = "integrates"
        "fluidattacks:stack" = "k8s"
      }
    }
    prod_skims = {
      max_size  = 50
      user_data = "${path.module}/init/prod_skims"
      subnets   = [data.aws_subnet.batch_clone.id]
      instance_types = [
        "r6gd.medium",
        "r7gd.medium",
        "x2gd.medium",
      ]
      tags = {
        "fluidattacks:line"  = "cost"
        "fluidattacks:comp"  = "skims"
        "fluidattacks:stack" = "k8s"
      }
    }
  }
}

module "cluster" {
  source                                 = "terraform-aws-modules/eks/aws"
  version                                = "20.29.0"
  cluster_name                           = local.cluster_name
  cluster_version                        = "1.31"
  cluster_endpoint_public_access         = true
  cluster_endpoint_private_access        = false
  enable_irsa                            = true
  cloudwatch_log_group_retention_in_days = 0
  cluster_enabled_log_types              = ["audit", "api", "authenticator", "controllerManager", "scheduler"]

  # Nodes
  eks_managed_node_group_defaults = {
    capacity_type          = "SPOT"
    force_update_version   = true
    ebs_optimized          = true
    enable_monitoring      = true
    vpc_security_group_ids = [data.aws_security_group.cloudflare.id]
  }

  eks_managed_node_groups = {
    for group, values in local.worker_groups : group => {
      max_size = lookup(values, "max_size", 10)

      ami_type              = lookup(values, "arch", "AL2_ARM_64")
      instance_types        = values.instance_types
      max_instance_lifetime = 86400

      iam_role_additional_policies = {
        ssm_core = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
        ssm_role = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
      }

      block_device_mappings = {
        xvda = {
          device_name = "/dev/xvda"
          ebs = {
            volume_size           = lookup(values, "disk_size", 20)
            volume_type           = "gp3"
            encrypted             = true
            delete_on_termination = true
          }
        }
      }
      user_data_template_path = lookup(values, "user_data", null)

      subnet_ids = lookup(
        values,
        "subnets",
        [for subnet in data.aws_subnet.main : subnet.id]
      )

      labels = merge(
        { worker_group = group },
        lookup(values, "labels", {}),
      )
      tag_specifications = ["instance", "volume", "network-interface", "spot-instances-request"]
      tags               = values.tags
    }
  }

  # Network
  vpc_id     = data.aws_vpc.main.id
  subnet_ids = [for subnet in data.aws_subnet.main : subnet.id]

  # Auth
  authentication_mode                      = "API_AND_CONFIG_MAP"
  enable_cluster_creator_admin_permissions = false # Make true if recreating the cluster
  access_entries = merge(
    {
      for user in local.users : user => {
        kubernetes_groups = distinct(["dev", user])
        principal_arn     = data.aws_iam_role.main[user].arn
      }
    }
  )

  # Encryption
  create_kms_key          = true
  enable_kms_key_rotation = true
  kms_key_aliases         = [local.cluster_name]
  kms_key_owners = [
    for admin in local.admins : data.aws_iam_role.main[admin].arn
  ]
  kms_key_administrators = [
    for user in local.users : data.aws_iam_role.main[user].arn
  ]

  node_security_group_additional_rules = {
    keda_metrics_server_access = {
      description                   = "Cluster access to keda operator deployment"
      protocol                      = "tcp"
      from_port                     = 9666
      to_port                       = 9666
      type                          = "ingress"
      source_cluster_security_group = true
    }
  }

  tags = {
    "Name"              = local.cluster_name
    "Environment"       = "production"
    "GithubRepo"        = "terraform-aws-eks"
    "GithubOrg"         = "terraform-aws-modules"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "NOFLUID"           = "f024.f165.f333_external_module_imposible_modify"
  }
}
