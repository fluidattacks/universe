variable "cloudflareTunnelToken" {
  sensitive = true
  type      = string
}

resource "kubernetes_deployment_v1" "ztna" {
  metadata {
    name      = "ztna"
    namespace = "kube-system"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        "app" = "ztna"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "25%"
        max_unavailable = "25%"
      }
    }
    template {
      metadata {
        labels = {
          "app" = "ztna"
        }
      }
      spec {
        automount_service_account_token = false
        node_selector = {
          "worker_group" = "core"
        }
        termination_grace_period_seconds = 60
        container {
          name  = "cloudflared"
          image = "docker.io/cloudflare/cloudflared:2024.4.1-arm64@sha256:919315f2afe4eda94e4bd97c4ca77258c07d0c9fbefedc0b34082941bbd7cfe8"
          args = [
            "tunnel",
            "--no-autoupdate",
            "--metrics",
            "0.0.0.0:2000",
            "run",
            "--token",
            var.cloudflareTunnelToken,
          ]
          resources {
            requests = {
              "cpu"    = "500m"
              "memory" = "1000Mi"
            }
            limits = {
              "cpu"    = "1000m"
              "memory" = "2000Mi"
            }
          }
          liveness_probe {
            http_get {
              path = "/ready"
              port = "2000"
            }
            failure_threshold     = 1
            initial_delay_seconds = 1
            period_seconds        = 10
          }
          # NOFLUID makes container has to run as root to function correctly
          security_context {
            allow_privilege_escalation = false
            privileged                 = false
            # NOFLUID makes container requires write permission to the root filesystem to function correctly
            read_only_root_filesystem = false
          }
        }
      }
    }
  }
}
