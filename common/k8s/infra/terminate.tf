
resource "helm_release" "aws_node_termination_handler" {
  name             = "aws-node-termination-handler"
  namespace        = "kube-system"
  repository       = "oci://public.ecr.aws/aws-ec2/helm/"
  chart            = "aws-node-termination-handler"
  version          = "0.23.0"
  create_namespace = true
  cleanup_on_fail  = true
  atomic           = true

  set {
    name  = "enableSpotInterruptionDraining"
    value = "true"
  }
  set {
    name  = "enableScheduledEventDraining"
    value = "true"
  }
}
