variable "cloudflareEmail" {
  type = string
}
variable "cloudflareApiKey" {
  type = string
}

resource "helm_release" "dns" {
  name       = "dns"
  repository = "https://kubernetes-sigs.github.io/external-dns/"
  chart      = "external-dns"
  version    = "1.14.4"
  namespace  = "kube-system"

  values = [
    yamlencode(
      {
        provider = {
          name = "cloudflare"
        }
        policy     = "sync"
        registry   = "txt"
        txtOwnerId = local.cluster_name
        txtPrefix  = "${local.cluster_name}-"
        extraArgs  = ["--cloudflare-proxied"]
        env = [
          {
            name  = "CF_API_EMAIL"
            value = var.cloudflareEmail
          },
          {
            name  = "CF_API_KEY"
            value = var.cloudflareApiKey
          },
        ]
        nodeSelector = {
          worker_group = "core"
        }
      }
    )
  ]
}


resource "helm_release" "dns-cache" {
  chart      = "node-local-dns"
  name       = "dns-cache"
  namespace  = "kube-system"
  repository = "oci://ghcr.io/deliveryhero/helm-charts"
  version    = "2.1.4"

  set {
    name  = "config.bindIp"
    value = true
  }
  set {
    name  = "config.dnsServer"
    value = "10.100.0.10"
  }
  set {
    name  = "config.enableLogging"
    value = true
  }
}
