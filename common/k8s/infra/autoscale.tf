resource "helm_release" "metrics_server" {
  chart       = "metrics-server"
  description = "Horizontal pod autoscaler"
  name        = "metrics-server"
  namespace   = "kube-system"
  repository  = "https://kubernetes-sigs.github.io/metrics-server"
  version     = "3.12.1"

  values = [
    yamlencode(
      {
        nodeSelector = {
          worker_group = "core"
        }
      }
    )
  ]
}

resource "helm_release" "keda_autoscaler" {
  chart       = "keda"
  description = "Kubernetes Event Driven Autoscaler"
  name        = "keda-autoscaler"
  namespace   = "kube-system"
  repository  = "https://kedacore.github.io/charts"
  version     = "2.14.2"

  values = [
    yamlencode(
      {
        nodeSelector = {
          worker_group = "core"
        }
      }
    )
  ]
}

# Cluster autoscaler

resource "aws_iam_policy" "autoscaler" {
  name_prefix = "${local.cluster_name}-autoscaler-"

  policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Sid    = "DescribePermissions"
          Effect = "Allow"
          Action = [
            "autoscaling:DescribeAutoScalingGroups",
            "autoscaling:DescribeAutoScalingInstances",
            "autoscaling:DescribeLaunchConfigurations",
            "autoscaling:DescribeTags",
            "ec2:DescribeLaunchTemplateVersions",
            "ec2:DescribeInstanceTypes",
            "cloudwatch:GetMetricData",
            "sqs:ListQueues"
          ]
          Resource = ["*"]
        },
        {
          Sid    = "DescribeEKS",
          Effect = "Allow",
          Action = [
            "eks:DescribeNodegroup"
          ],
          Resource = "arn:aws:eks:us-east-1:${data.aws_caller_identity.main.id}:nodegroup/*"
        },
        {
          Sid    = "ModifyAutoscaling",
          Effect = "Allow",
          Action = [
            "autoscaling:SetDesiredCapacity",
            "autoscaling:TerminateInstanceInAutoScalingGroup"
          ],
          Resource = "arn:aws:autoscaling:*:${data.aws_caller_identity.main.id}:autoScalingGroup:*:autoScalingGroupName/*"
        },
        {
          Sid    = "DescribeSQS",
          Effect = "Allow",
          Action = [
            "sqs:GetQueueAttributes"
          ],
          Resource = "arn:aws:sqs:us-east-1:${data.aws_caller_identity.main.id}:*"
        },
      ]
    }
  )
  tags = {
    "Name"              = "autoscaler_policy"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

module "autoscaler_oidc_role" {
  source           = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version          = "5.39.0"
  create_role      = true
  role_name_prefix = "${local.cluster_name}-autoscaler-"
  provider_url     = replace(module.cluster.cluster_oidc_issuer_url, "https://", "")

  role_policy_arns = [
    aws_iam_policy.autoscaler.arn,
  ]

  oidc_fully_qualified_subjects = [
    "system:serviceaccount:kube-system:autoscaler",
  ]
}

resource "kubernetes_service_account" "autoscaler" {
  automount_service_account_token = true

  metadata {
    name      = "autoscaler"
    namespace = "kube-system"

    annotations = {
      "eks.amazonaws.com/role-arn" = module.autoscaler_oidc_role.iam_role_arn
    }
  }
}

resource "helm_release" "autoscaler" {
  name       = "autoscaler"
  repository = "https://kubernetes.github.io/autoscaler"
  chart      = "cluster-autoscaler"
  version    = "9.37.0"
  namespace  = "kube-system"

  values = [
    yamlencode(
      {
        autoDiscovery = {
          clusterName = local.cluster_name
        }
        rbac = {
          serviceAccount = {
            create = false
            name   = kubernetes_service_account.autoscaler.metadata[0].name
            annotations = {
              "eks.amazonaws.com/role-arn" = module.autoscaler_oidc_role.iam_role_arn
            }
          }
        }
        extraArgs = {
          scale-down-unneeded-time = "15m"
        }
        nodeSelector = {
          worker_group = "core"
        }
      }
    )
  ]
}
