# https://github.com/fluidattacks/makes
{ inputs, makeSearchPaths, outputs, ... }:
let
  searchPaths =
    makeSearchPaths { bin = [ inputs.nixpkgs.awscli inputs.nixpkgs.git ]; };
in {
  deployTerraform = {
    modules = {
      commonK8s = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareProd"
          outputs."/secretsForEnvFromSops/commonK8s"
          outputs."/secretsForEnvFromSops/commonK8sProd"
          outputs."/secretsForTerraformFromEnv/commonK8s"
        ];
        src = "/common/k8s/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonK8s = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
          outputs."/secretsForEnvFromSops/commonK8s"
          outputs."/secretsForEnvFromSops/commonK8sDev"
          outputs."/secretsForTerraformFromEnv/commonK8s"
        ];
        src = "/common/k8s/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonK8s = {
      vars = [ "CORALOGIX_ACCOUNT_ID" "CORALOGIX_DOMAIN" "SPYDERBAT_KEY" ];
      manifest = "/common/secrets/dev.yaml";
    };
    commonK8sDev = {
      vars = [
        "ASPECTO_KEY"
        "CORALOGIX_OTEL_API_KEY"
        "HONEYCOMB_KEY"
        "TELEMETRYHUB_KEY"
      ];
      manifest = "/common/secrets/dev.yaml";
    };
    commonK8sProd = {
      vars = [
        "ASPECTO_KEY"
        "HONEYCOMB_KEY"
        "TELEMETRYHUB_KEY"
        "CORALOGIX_OTEL_API_KEY"
      ];
      manifest = "/common/secrets/prod.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    commonK8s = {
      cloudflareApiKey = "CLOUDFLARE_API_KEY";
      cloudflareEmail = "CLOUDFLARE_EMAIL";
      cloudflareTunnelToken = "CLOUDFLARE_TUNNEL_TOKEN";
      coralogixDomain = "CORALOGIX_DOMAIN";
      coralogixOtelApiKey = "CORALOGIX_OTEL_API_KEY";
      coralogixAccountId = "CORALOGIX_ACCOUNT_ID";
      spyderkey = "SPYDERBAT_KEY";
    };
  };
  testTerraform = {
    modules = {
      commonK8s = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
          outputs."/secretsForEnvFromSops/commonK8s"
          outputs."/secretsForEnvFromSops/commonK8sDev"
          outputs."/secretsForTerraformFromEnv/commonK8s"
        ];
        src = "/common/k8s/infra";
        version = "1.0";
      };
    };
  };
}
