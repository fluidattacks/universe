{ inputs, isLinux, makeScript, managePorts, ... }:
let
  version = "1.61.0";
  name = "jaeger";
  # https://github.com/NixOS/nixpkgs/issues/287096
  jaeger = inputs.nixpkgs.stdenv.mkDerivation {
    inherit version;
    inherit name;
    unpackPhase = ''
      mkdir src
      tar -xf $src -C src
      mv src/**/jaeger-all-in-one src/jaeger-all-in-one
    '';
    installPhase = ''
      mkdir -p $out/bin
      chmod +x src/jaeger-all-in-one
      mv src/jaeger-all-in-one $out/bin
      rm -rf src
    '';
    nativeBuildInputs = [ inputs.nixpkgs.makeBinaryWrapper ];
    src = let
      platformMappings = {
        aarch64-darwin = {
          platform = "darwin-arm64";
          sha256 = "sha256-TVP1cYorkxM16Qu/3chAa6QgAXwrce/XSzzf93ctqT8=";
        };
      };
      currentPlatform = builtins.currentSystem;
      platformInfo = builtins.getAttr currentPlatform platformMappings;
    in inputs.nixpkgs.fetchurl {
      url =
        "https://github.com/jaegertracing/jaeger/releases/download/v${version}/jaeger-${version}-${platformInfo.platform}.tar.gz";
      inherit (platformInfo) sha256;
    };
  };
in makeScript {
  inherit name;
  entrypoint = ./entrypoint.sh;
  replace = { __argJaeger__ = jaeger; };
  searchPaths = {
    bin = [ jaeger ];
    source = [ managePorts ];
  };
}
