{ pkgs }:
{ days ? 30, keyType ? "rsa:4096", name, options }:
pkgs.stdenv.mkDerivation {
  inherit name;
  phases = [ "buildPhase" ];
  buildPhase = ''
    mkdir "$out"

    openssl req \
      -days "${builtins.toString days}" \
      -keyout "$out/cert.key" \
      -new \
      -newkey "${keyType}" \
      -nodes \
      -out "$out/cert.crt" \
      ${pkgs.lib.strings.escapeShellArgs options} \
      -x509

    openssl x509 \
      -in "$out/cert.crt" \
      -inform 'pem' \
      -noout \
      -text
  '';
  nativeBuildInputs = [ pkgs.openssl ];
}
