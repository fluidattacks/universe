{ inputs, makeScript, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "bugsnag-announce";
  replace = {
    __argNPMPath__ = projectPath "/common/utils/bugsnag/announce/npm";
  };
  searchPaths.bin = [ inputs.nixpkgs.nodejs_20 ];
}
