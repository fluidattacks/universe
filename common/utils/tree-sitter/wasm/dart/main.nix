{ inputs, outputs, ... }:
let
  build = import ../. { inherit inputs outputs; };
  name = builtins.baseNameOf ./.;
  src = builtins.fetchTarball {
    sha256 = "sha256:0zl46vkm4p1jmivmnpyyzc58fwhx5frfgi0rfxna43h0qxdv62wy";
    url =
      "https://github.com/UserNobody14/tree-sitter-dart/archive/e81af6ab94a728ed99c30083be72d88e6d56cf9e.tar.gz";
  };
in build { inherit name src; }
