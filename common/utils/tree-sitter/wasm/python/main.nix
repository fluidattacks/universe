{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-python.wasm
  '';
  name = "tree-sitter-python";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:14401bnrjzmhhbvk300f67s1kasbasx0j4qmzhnhmnxxrjspgx2d";
    url =
      "https://github.com/tree-sitter/tree-sitter-python/releases/download/v0.23.5/tree-sitter-python.wasm";
  };
}
