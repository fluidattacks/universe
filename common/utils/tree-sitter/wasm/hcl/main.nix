{ inputs, outputs, ... }:
let
  build = import ../. { inherit inputs outputs; };
  name = builtins.baseNameOf ./.;
  src = builtins.fetchTarball {
    sha256 = "sha256:08sjjmfkkpm2crpdp2gy86daap1fbnj4l1vwvn3zz05gfq2zpkhw";
    url =
      "https://github.com/tree-sitter-grammars/tree-sitter-hcl/archive/9e3ec9848f28d26845ba300fd73c740459b83e9b.tar.gz";
  };
in build { inherit name src; }
