{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-cpp.wasm
  '';
  name = "tree-sitter-cpp";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:1gzw0g2nvav9gqkc5ran75l8x3b4jpwskkfa3f4cfbjvnzgb0khp";
    url =
      "https://github.com/tree-sitter/tree-sitter-cpp/releases/download/v0.23.4/tree-sitter-cpp.wasm";
  };
}
