{ inputs, outputs, ... }:
let version = "v0.23.2";
in inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    mv $typescript $out/tree-sitter-typescript.wasm
    mv $tsx $out/tree-sitter-tsx.wasm
  '';
  name = "tree-sitter-typescript";
  dontUnpack = true;
  sourceRoot = ".";
  tsx = builtins.fetchurl {
    sha256 = "sha256:1y1hmm8rf28a3sz3q2v3bxywisk405gnhxvisq65m1b2x9sxmrbr";
    url =
      "https://github.com/tree-sitter/tree-sitter-typescript/releases/download/${version}/tree-sitter-tsx.wasm";
  };
  typescript = builtins.fetchurl {
    sha256 = "sha256:0zgryqdjxpi61jkyin95ih2dvvfzhvj72dncih7ygq4bbbdjb03p";
    url =
      "https://github.com/tree-sitter/tree-sitter-typescript/releases/download/${version}/tree-sitter-typescript.wasm";
  };
}
