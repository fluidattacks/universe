{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-c_sharp.wasm
  '';
  name = "tree-sitter-c_sharp";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:1qs6zs0g3zs9bphv7dzp4cnk6srq8cy5wdrz4lmnplmqx3w0pznp";
    url =
      "https://github.com/tree-sitter/tree-sitter-c-sharp/releases/download/v0.23.1/tree-sitter-c_sharp.wasm";
  };
}
