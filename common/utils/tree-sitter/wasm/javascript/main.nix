{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-javascript.wasm
  '';
  name = "tree-sitter-javascript";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:08fi9s1wlf08j92v5vdmnd9hxynsfbh3n0in53pcnlvqzs9q4dsa";
    url =
      "https://github.com/tree-sitter/tree-sitter-javascript/releases/download/v0.23.1/tree-sitter-javascript.wasm";
  };
}
