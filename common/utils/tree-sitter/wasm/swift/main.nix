{ inputs, outputs, ... }:
let
  build = import ../. { inherit inputs outputs; };
  name = builtins.baseNameOf ./.;
  src = builtins.fetchTarball {
    sha256 = "sha256:1p25mkv0shp2s65ka5g80lw4v53r21kklvgn00slnqwjwq90ygch";
    url =
      "https://github.com/alex-pinkus/tree-sitter-swift/archive/ce6a915cd937ecb2c6d9f79bbf7ef7f7c1ccc61a.tar.gz";
  };
in build { inherit name src; }
