{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-scala.wasm
  '';
  name = "tree-sitter-scala";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:0wih9iyfz2j21w1997m62pj7jyrwk0rlqv4hj5a68w1fr49bprjd";
    url =
      "https://github.com/tree-sitter/tree-sitter-scala/releases/download/v0.23.3/tree-sitter-scala.wasm";
  };
}
