{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-go.wasm
  '';
  name = "tree-sitter-go";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:0r1dbsg6zgwbi07i2bsqh96x3k8rpm8qi20dml64v9fhvan8xz3d";
    url =
      "https://github.com/tree-sitter/tree-sitter-go/releases/download/v0.23.4/tree-sitter-go.wasm";
  };
}
