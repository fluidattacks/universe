{ inputs, isLinux, ... }:
let
  src = let
    version = "v0.23.2";
    arch = {
      x86_64-linux = {
        url =
          "https://github.com/tree-sitter/tree-sitter/releases/download/${version}/tree-sitter-linux-x64.gz";
        sha256 = "sha256-UBDrQfN8PsatTlkL+OlFYTx/XphxFYUKKosiXZRvTdU=";
      };
      aarch64-linux = {
        url =
          "https://github.com/tree-sitter/tree-sitter/releases/download/${version}/tree-sitter-linux-arm64.gz";
        sha256 = "sha256-UOS7nwRrecc9BH8rTnrOAwNq3qsJiTa1OtyxWH3ZHeg=";
      };
      aarch64-darwin = {
        url =
          "https://github.com/tree-sitter/tree-sitter/releases/download/${version}/tree-sitter-macos-arm64.gz";
        sha256 = "sha256-WyyQq75WCJ+DDbOci1uBJoEgvvUM2161b/Cu9b+P75k=";
      };
    };
    current = arch.${builtins.currentSystem};
  in inputs.nixpkgs.fetchurl {
    inherit (current) url;
    inherit (current) sha256;
  };
in inputs.nixpkgs.stdenv.mkDerivation {
  unpackPhase = "gzip -d -c $src > tree-sitter";
  buildPhase = ''
    mkdir -p $out/bin
    chmod +x tree-sitter
    mv tree-sitter $out/bin
  '';
  name = "tree-sitter";
  buildInputs = [ inputs.nixpkgs.stdenv.cc.cc.lib ];
  nativeBuildInputs =
    if isLinux then [ inputs.nixpkgs.autoPatchelfHook ] else [ ];
  propagatedNativeBuildInputs = [ inputs.nixpkgs.emscripten ];
  inherit src;
}
