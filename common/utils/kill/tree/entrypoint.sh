# shellcheck shell=bash

function main() {
  local name
  local signal="${1}"
  local pid="${2}"

  # Neither pgrep nor pkill are available on procps for mac
  for child in $(ps -e -o pid,ppid | awk -v PPID="${pid}" '$2 == PPID {print $1}'); do
    main "${signal}" "${child}" \
      || return 1
  done \
    && name=$(ps -p "${pid}" -o comm=) \
    && kill -s "${signal}" "${pid}" \
    && echo "[INFO] signal ${signal} sent to ${name} with pid ${pid}" \
    || return 1
}

main "${@}"
