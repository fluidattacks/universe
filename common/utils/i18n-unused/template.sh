# shellcheck shell=bash

function run_i18_unused {
  local output
  output=$(npm run i18n-unused)
  info "$output"

  if [[ $output =~ "Unused translations count: 0" ]]; then
    return 0
  else
    critical "ERROR: Unused translations count is not zero.
     Remove unused keys from translation file"
    return 1
  fi
}

function check_unused_translations {
  local dir="${1-}"
  local project_path="common/utils/i18n-unused"

  if running_in_ci_cd_provider; then
    project_path=__argProjectPath__
  fi

  : \
    && pushd "${project_path}/npm" \
    && npm ci \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && popd \
    && copy "${project_path}/i18n-unused.config.cjs" "${dir}/i18n-unused.config.cjs" \
    && info "Checking unused translations in ${dir}" \
    && pushd "${dir}" \
    && rm -rf node_modules \
    && copy "${project_path}/npm/node_modules" node_modules \
    && TZ=UTC \
    && run_i18_unused \
    && rm i18n-unused.config.cjs \
    && rm -rf node_modules \
    && popd || exit
}
