module.exports = {
  customChecker: (matchKeysSet, translationsKeys) => {
    const interpolationRegex = /(?=.*`)(?=.*\$)[^/\\]*$/gium;
    const translationsKeysCopy = [...translationsKeys];
    const removeKey = (obj, keyToRemove) => {
      obj.splice(obj.indexOf(keyToRemove), 1);
    };
    const compareKeys = (key, matchedKey) => {
      if (matchedKey === undefined) return false;
      const pattern = matchedKey.replace(/\$\{[^}]+\}/g, '(\\w*)');
      const regex = new RegExp(`^${pattern}$`);

      return regex.test(key);
    }

    translationsKeysCopy.forEach((key) => {
      [...matchKeysSet].forEach((matchKey) => {
        const matchedKey = matchKey.startsWith('"') && matchKey.endsWith('"') ? matchKey.slice(1, -1) : matchKey
        if (matchedKey.includes(key)) {
          removeKey(translationsKeys, key);
        }
        else if (key.startsWith(matchedKey) && matchedKey.endsWith(".")) {
          removeKey(translationsKeys, key);
        }
        else if (matchedKey.match(interpolationRegex)) {
          const content = matchedKey.match(/`([^`]+)`/g)?.[0].replace(/`/g, "");
          if (compareKeys(key, content)) {
            removeKey(translationsKeys, key);
          }
        }
      });
    });
  },
  flatTranslations: true,
  localeNameResolver: (name) => {
    return name === "en.json" || name === "es.json";
  },
  localesExtensions: ["json"],
  localesPath: "src/utils/translations",
  srcExtensions: ["ts", "tsx"],
  srcPath: "src",
  translationKeyMatcher:
    /(?:translate\.)?t\(\s*`([^`]*)`|"(?=.*\.)[\w\s.]*"/giu,
};
