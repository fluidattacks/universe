{ inputs, makeTemplate, outputs, projectPath, ... }:
makeTemplate {
  name = "check-unused-translations";
  replace = { __argProjectPath__ = projectPath "/common/utils/i18n-unused"; };
  searchPaths.bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ];
  template = ./template.sh;
}
