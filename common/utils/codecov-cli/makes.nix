{ __system__, inputs, isLinux, ... }:
let
  srcs = {
    aarch64-darwin = {
      url =
        "https://github.com/codecov/codecov-cli/releases/download/v9.1.1/codecovcli_macos";
      sha256 = "sha256-OKwiOWv8hWlLvT4rGTFjyLvqqmL5PU6g5SLBh0SiplE=";
    };
    aarch64-linux = {
      url =
        "https://github.com/codecov/codecov-cli/releases/download/v9.1.1/codecovcli_linux_arm64";
      sha256 = "sha256-/xqQWN4vfA5OhLo4dmg7biO1Tz5UnCqQk42q1NvZlmg=";
    };
  };
in {
  inputs.codecov-cli = inputs.nixpkgs.stdenv.mkDerivation {
    buildInputs = [ inputs.nixpkgs.libz ];
    dontStrip = true;
    dontUnpack = true;
    installPhase = ''
      mkdir -p $out/bin
      cp $src $out/bin/codecov-cli
      chmod +x $out/bin/codecov-cli
    '';
    name = "codecov-cli";
    nativeBuildInputs =
      if isLinux then [ inputs.nixpkgs.autoPatchelfHook ] else [ ];
    src = inputs.nixpkgs.fetchurl srcs.${__system__};
  };
}
