module "github_makes" {
  source = "./modules/github"

  description = "A software supply chain framework powered by Nix."
  homepage    = "https://makes.fluidattacks.tech/"
  name        = "makes"
  token       = var.githubToken
  topics      = ["build", "cd", "ci", "devops", "devsecops", "nix"]

  secrets = {}

  pages = {
    cname = "makes.fluidattacks.tech"
  }
}

module "github_awesome_cvelabs" {
  source = "./modules/github"

  description = "A list of all awesome CVELabs"
  homepage    = "https://github.com/fluidattacks/awesome-cvelabs"
  name        = "awesome-cvelabs"
  token       = var.githubToken
  topics      = ["research-teams", "cvelabs", "cve", "awesome", "awesome-lists", "list"]

  secrets = {}

  pages = {}
}

module "github_compare_versions" {
  source = "./modules/github"

  description = "Allows you to compare versions using different standards"
  homepage    = "https://github.com/fluidattacks/compare_version"
  name        = "compare_version"
  token       = var.githubToken
  topics      = []

  secrets = {}

  pages = {}
}

module "github_benchmark_infra" {
  source = "./modules/github"

  description = "The infrastructure for the benchmark includes a set of Vulnerable by Design (VbD) Targets of Evaluation (ToEs) used to measure the speed and accuracy of automated Application Security Testing (AST) tools."
  homepage    = "https://github.com/fluidattacks/benchmark-infra"
  name        = "benchmark-infrastructure"
  token       = var.githubToken
  topics      = ["benchmark", "compare", "ast", "sast", "dast", "sca", "cspm", "scr", "mpt", "re"]

  secrets = {}

  pages = {}
}
