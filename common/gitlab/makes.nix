# This module is intended for local-admin execution only
# since this module targets to the Organization group
# Directly and can only be managed by organization-admins.
# https://github.com/fluidattacks/makes
{ inputs, makeSearchPaths, outputs, ... }:
let
  searchPaths =
    makeSearchPaths { bin = [ inputs.nixpkgs.awscli inputs.nixpkgs.git ]; };
in {
  deployTerraform = {
    modules = {
      commonGitlab = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonGitlabProd"
          outputs."/secretsForTerraformFromEnv/commonGitlab"
        ];
        src = "/common/gitlab/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonGitlabProd = {
      vars = [ "FLUIDATTACKS_GITLAB_API_TOKEN" ];
      manifest = "/common/secrets/prod.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    commonGitlab = {
      fluidattacksGitlabApiToken = "FLUIDATTACKS_GITLAB_API_TOKEN";
    };
  };
  testTerraform = {
    modules = {
      commonGitlab = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonGitlabProd"
          outputs."/secretsForTerraformFromEnv/commonGitlab"
        ];
        src = "/common/gitlab/infra";
        version = "1.0";
      };
    };
  };
}
