locals {
  runners = {
    airs = {
      tags = ["airs"]
    }
    common = {
      tags = ["common"]
    }
    common-large = {
      tags = ["common-large"]
    }
    common-x86 = {
      tags = ["common-x86"]
    }
    common-x86-large = {
      tags = ["common-x86-large"]
    }
    integrates = {
      tags = ["integrates"]
    }
    observes = {
      tags = ["observes"]
    }
    skims = {
      tags = ["skims"]
    }
    sorts = {
      tags = ["sorts"]
    }
  }
}
