from analytics.workflows.custom.integrates.e2e.workflow import (
    E2eWorkflow,
)
from analytics.workflows.custom.integrates.functional.workflow import (
    FunctionalTestsWorkflow,
)
from analytics.workflows.default.workflow import (
    DefaultWorkflow,
)
from analytics.workflows.workflow import (
    WorkFlow,
)

__enabled_workflow_classes = [
    DefaultWorkflow(),
    E2eWorkflow(),
    FunctionalTestsWorkflow(),
]


def load_workflows() -> list[WorkFlow]:
    return __enabled_workflow_classes
