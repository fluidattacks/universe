import asyncio
import logging
import os
from pathlib import (
    Path,
)

from pandas import (
    DataFrame,
    unique,
)

from analytics.plot.fig_result import (
    FailureReason,
    FigFailed,
    FigResult,
    FigSkipped,
    SkipReason,
)
from analytics.plot.plotter import (
    DynamicSize,
    Plotter,
    PlotterParams,
)
from analytics.plot.utils import (
    FigureOptions,
    filter_with_match_type,
    save_boxplot_fig,
    summarize_data,
)

PIPELINES_FIG_SAVE_DIR = "output/figures"
LOGGER = logging.getLogger(__name__)


def filter_dev_success_pipelines(data: DataFrame) -> DataFrame:
    return data[(data["ref"].str.endswith("atfluid")) & (data["status"] == "SUCCESS")]


class PipelinesPlotter(Plotter):
    def __create_base_dir(self) -> None:
        if not os.path.exists(PIPELINES_FIG_SAVE_DIR):
            os.makedirs(PIPELINES_FIG_SAVE_DIR)

    def __init__(self) -> None:
        self._data: DataFrame | None = None
        self.params = PlotterParams()
        self.__create_base_dir()

    def set_params(self, params: PlotterParams) -> None:
        self.params = params

    def __filter_product(self, data: DataFrame, product: str) -> DataFrame:
        return filter_with_match_type(
            data=data,
            match_type=self.params.match_type,
            field="product",
            match_str=product,
        )

    async def _gen_pipeline_duration_over_time_for_product(self, data: DataFrame) -> FigResult:
        data = filter_dev_success_pipelines(data)
        if self.params.product is None:
            return FigSkipped(
                SkipReason("Skipping pipeline duration by product (product not found in args)"),
            )
        fig_name = f"pipelines_duration_over_time_{self.params.product}.png"
        hue_len = len(unique(data[self.params.time_scale.value]))
        filtered_and_sorted = self.__filter_product(data, product=self.params.product).sort_values(
            by=self.params.time_scale.value,
        )
        LOGGER.info(
            "Durations over time matching: %s\n%s",
            self.params.product,
            summarize_data(
                data=filtered_and_sorted,
                group_fields=[self.params.time_scale.value, "product"],
                target_field="duration",
            ),
        )
        return await save_boxplot_fig(
            data=filtered_and_sorted,
            x_field=self.params.time_scale.value,
            y_field="duration",
            hue_field="product",
            fig_path=Path(f"{PIPELINES_FIG_SAVE_DIR}/{fig_name}"),
            options=FigureOptions(
                size=DynamicSize(
                    hue_len=hue_len,
                    base_width_inches=10,
                    width_per_hue_inches=1,
                ),
                title=(
                    f"Pipelines over time matching: {self.params.product}"
                    f" ({self.params.match_type.value} match)"
                ),
                log_scale=True,
                rotation=20,
                legend_move=True,
            ),
        )

    async def _gen__global_pipeline_duration_for_product(self, data: DataFrame) -> FigResult:
        data = filter_dev_success_pipelines(data)
        if self.params.product is None:
            return FigSkipped(
                SkipReason(
                    "Skipping pipeline global duration by product (product not found in args)",
                ),
            )
        fig_name = f"pipelines_global_duration_{self.params.product}.png"
        hue_len = len(unique(data[self.params.time_scale.value]))
        target_data = self.__filter_product(data, product=self.params.product).sort_values(
            by=self.params.time_scale.value,
        )
        return await save_boxplot_fig(
            data=target_data,
            x_field="product",
            y_field="duration",
            fig_path=Path(f"{PIPELINES_FIG_SAVE_DIR}/{fig_name}"),
            options=FigureOptions(
                size=DynamicSize(
                    hue_len=hue_len,
                    base_width_inches=3,
                    width_per_hue_inches=1,
                ),
                title=(
                    f"Pipelines matching: {self.params.product}"
                    f" ({self.params.match_type.value} match)"
                    f" Average duration: {target_data['duration'].mean():.2f}"
                ),
                log_scale=True,
            ),
        )

    async def _gen_pipeline_duration_over_time(self, data: DataFrame) -> FigResult:
        data = filter_dev_success_pipelines(data)
        fig_name = "pipelines_duration_over_time.png"
        hue_len = len(unique(data[self.params.time_scale.value]))
        LOGGER.info(
            "Durations over time:\n%s",
            summarize_data(
                data=data,
                group_fields=[self.params.time_scale.value, "product"],
                target_field="duration",
            ),
        )
        return await save_boxplot_fig(
            data=data.sort_values(by=self.params.time_scale.value),
            x_field=self.params.time_scale.value,
            y_field="duration",
            hue_field="product",
            fig_path=Path(f"{PIPELINES_FIG_SAVE_DIR}/{fig_name}"),
            options=FigureOptions(
                size=DynamicSize(hue_len=hue_len, height_inches=10),
                legend_move=True,
                title="Pipeline duration over time",
                log_scale=True,
            ),
        )

    async def _gen_global_pipeline_durations(self, data: DataFrame) -> FigResult:
        data = filter_dev_success_pipelines(data)
        fig_name = "pipelines_durations.png"
        hue_len = len(unique(data["product"]))
        LOGGER.info(
            "Global durations: between %s and %s \n%s",
            data["finishedAt"].min(),
            data["finishedAt"].max(),
            summarize_data(
                data=data,
                group_fields=["product"],
                target_field="duration",
            ),
        )
        return await save_boxplot_fig(
            data=data.sort_values(by=self.params.time_scale.value),
            x_field="product",
            y_field="duration",
            fig_path=Path(f"{PIPELINES_FIG_SAVE_DIR}/{fig_name}"),
            options=FigureOptions(
                size=DynamicSize(hue_len=hue_len),
                title="Pipeline durations",
                log_scale=True,
            ),
        )

    def set_dataframe(self, data: DataFrame) -> None:
        self._data = data

    async def gen_all(self) -> tuple[FigResult, ...]:
        if self._data is None:
            return (FigFailed(FailureReason("Empty data in plotter")),)
        return await asyncio.gather(
            self._gen_global_pipeline_durations(self._data),
            self._gen_pipeline_duration_over_time(self._data),
            self._gen_pipeline_duration_over_time_for_product(self._data),
            self._gen__global_pipeline_duration_for_product(self._data),
        )
