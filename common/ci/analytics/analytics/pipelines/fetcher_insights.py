import logging
from typing import Any

from analytics.errors.api_errors import (
    GitlabApiConnectionError,
)
from analytics.fetch.client import (
    ApiClient,
)
from analytics.fetch.fetcher import (
    EmptyParamsInFetcherError,
    Fetcher,
    FetcherParameters,
    MissingCommitRefError,
)
from analytics.fetch.utils.caching import (
    read_cache,
    renew_cache,
)
from analytics.parse.errors import (
    JsonFormattingError,
)

from .queries import PIPELINES_INSIGHTS_QUERY

CACHE_TYPE = "pipelines_insights"
MAX_JOB_PAGES = 10

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def _parse_jobs_data(pipeline_jobs: list[dict[str, Any]]) -> list[dict[str, Any]]:
    jobs_data = []
    for job in pipeline_jobs:
        job["needs"] = [edge["node"]["name"] for edge in job["needs"]["edges"]]
        jobs_data.append(job)
    return jobs_data


def _parse_elements(
    data: dict[str, Any],
) -> tuple[dict[str, Any], list[dict[str, Any]], str, str, str]:
    pipeline_metadata = data["data"]["projects"]["nodes"][0]["pipelines"]["nodes"][0]
    pipeline_metadata["commitTitle"] = pipeline_metadata["commit"]["title"]
    del pipeline_metadata["commit"]
    pipeline_jobs = _parse_jobs_data(pipeline_metadata["jobs"]["nodes"])
    jobs_page_info = pipeline_metadata["jobs"]["pageInfo"]
    has_next_page = jobs_page_info["hasNextPage"]
    after = jobs_page_info["endCursor"]
    start_cursor = jobs_page_info["startCursor"]

    return pipeline_metadata, pipeline_jobs, has_next_page, after, start_cursor


class PipelinesInsightsFetcher(Fetcher):
    def __init__(self) -> None:
        self.params: FetcherParameters | None = None

    def set_params(self, params: FetcherParameters) -> None:
        self.params = params

    async def fetch(self, cache: bool = False) -> list[dict]:
        if self.params is None:
            raise EmptyParamsInFetcherError()

        if cache:
            return await read_cache(CACHE_TYPE)

        query = PIPELINES_INSIGHTS_QUERY.replace("#ID", f'"{self.params.project_id}"')

        if self.params.ref is None:
            raise MissingCommitRefError("Missing commit ref in fetcher parameters.")

        query = query.replace("#REF", f'"{self.params.ref}"')
        after = ""
        all_pipeline_jobs: list[dict[str, Any]] = []
        first_cursor = ""
        pipeline_metadata: dict[str, Any] = {}
        try:
            api_client = ApiClient(access_token=self.params.access_token)
            for _ in range(MAX_JOB_PAGES):
                req_query = query.replace("#AFTER", f'"{after}"')
                api_response = await api_client.post(req_query)
                if not api_response.is_success:
                    raise GitlabApiConnectionError(
                        "Error connecting to gitlab API: "
                        f"{api_response.json()['errors']}-"
                        f"({api_response.status_code})",
                    )

                data = api_response.json()

                (pipeline_metadata, pipeline_jobs, has_next_page, after, start_cursor) = (
                    _parse_elements(data)
                )
                if first_cursor == "":
                    first_cursor = start_cursor
                all_pipeline_jobs.extend(pipeline_jobs)

                if not has_next_page:
                    pipeline_metadata["jobs"] = all_pipeline_jobs
                    break
            await api_client.close()

        except Exception as exc:
            raise JsonFormattingError(f"Could not parse jobs from data: {data}.") from exc

        await renew_cache(
            data=[pipeline_metadata],
            start_cursor=first_cursor,
            end_cursor=after,
            cache_type=CACHE_TYPE,
        )
        return [pipeline_metadata]
