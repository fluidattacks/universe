class GitlabApiConnectionError(Exception):
    pass


class GitlabApiTokenNotFound(Exception):
    pass
