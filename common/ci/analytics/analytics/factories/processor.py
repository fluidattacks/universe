from abc import (
    ABC,
    abstractmethod,
)

from analytics.fetch.fetcher import (
    Fetcher,
)
from analytics.parse.parser import (
    DataframeParser,
)
from analytics.plot.plotter import (
    Plotter,
)


class ProcessorsFactory(ABC):
    @abstractmethod
    def get_fetcher(self) -> Fetcher:
        pass

    @abstractmethod
    def get_plotter(self) -> Plotter:
        pass

    @abstractmethod
    def get_parser(self) -> DataframeParser:
        pass
