import logging
from dataclasses import (
    dataclass,
)

from pandas import (
    DataFrame,
    unique,
)

from analytics.entities.jobs import (
    JobStatus,
)
from analytics.entities.timescale import (
    TimeScale,
    parse_time_scale_str,
)
from analytics.fetch.fetcher import (
    FetcherParameters,
)
from analytics.jobs.fetcher import (
    JobsFetcher,
)
from analytics.jobs.parser import (
    JobDataframeParser,
)
from analytics.plot.fig_result import (
    log_fig_results,
)
from analytics.plot.plotter import (
    PlotterParams,
)
from analytics.workflows.custom.integrates.e2e.plotting import (
    E2ePlotter,
)
from analytics.workflows.errors import (
    NoParamsInWorkflowError,
    WorkflowPopulationError,
)
from analytics.workflows.workflow import (
    WorkFlow,
)

LOGGER = logging.getLogger(__name__)
KEEP_COLS = [
    "name",
    "status",
    "duration",
    "startedAt",
    "week",
    "day",
    "hour",
    "month",
]


def filter_cols(data: DataFrame) -> DataFrame:
    return data[KEEP_COLS]


def filter_e2e_tests(data: DataFrame) -> DataFrame:
    return data[data["name"].str.contains("web/e2e run")]


def is_flaky(data: DataFrame) -> bool:
    return len(unique(data["status"])) > 1


@dataclass
class E2eWorkflowParams:
    pages: int
    time_scale: TimeScale
    cache: bool = False


class E2eWorkflow(WorkFlow):
    tag = "e2e"
    help = "End to end analytics"

    def __init__(self) -> None:
        self.params: E2eWorkflowParams | None = None

    def populate(self, workflow_data: dict) -> None:
        time_scale = parse_time_scale_str(workflow_data["time_scale"])
        if time_scale is None:
            raise WorkflowPopulationError(["time_scale"])
        self.params = E2eWorkflowParams(
            pages=workflow_data["pages"],
            time_scale=time_scale,
            cache=workflow_data["cache"],
        )

    async def run(
        self,
        project_id: str,
        access_token: str,
    ) -> None:
        if not self.params:
            raise NoParamsInWorkflowError()
        jobs_fetcher = JobsFetcher()
        jobs_fetcher.set_params(
            FetcherParameters(
                access_token=access_token,
                pages=self.params.pages,
                project_id=project_id,
                statuses=[JobStatus.SUCCESS, JobStatus.FAILED],
            ),
        )
        jobs = await jobs_fetcher.fetch(cache=self.params.cache)
        jobs_parser = JobDataframeParser()
        jobs_parser.parse(raw_data=jobs)
        data_frame = jobs_parser.pop()
        data_frame_e2e = filter_cols(filter_e2e_tests(data_frame))
        with_flakiness = is_flaky(data_frame_e2e)
        plotter = E2ePlotter()
        plotter.set_params(
            PlotterParams(
                time_scale=self.params.time_scale,
                with_flakiness=with_flakiness,
            ),
        )
        plotter.set_dataframe(data_frame_e2e)
        saved_figures = await plotter.gen_all()
        log_fig_results(LOGGER, saved_figures)
