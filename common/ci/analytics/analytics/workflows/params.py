from dataclasses import (
    dataclass,
)

from analytics.cli.resources import (
    Resources,
)
from analytics.entities.timescale import (
    TimeScale,
)


@dataclass
class WorkflowParams:
    pages: int
    time_scale: TimeScale
    resource: Resources | None
    target_job: str | None
    product: str | None
