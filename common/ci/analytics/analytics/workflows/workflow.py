from abc import (
    ABC,
    abstractmethod,
)


class WorkFlow(ABC):
    tag: str
    help: str

    @abstractmethod
    def populate(self, workflow_data: dict) -> None:
        pass

    @abstractmethod
    async def run(
        self,
        project_id: str,
        access_token: str,
    ) -> None:
        pass
