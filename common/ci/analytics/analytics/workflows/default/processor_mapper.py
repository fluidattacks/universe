from analytics.cli.resources import (
    Resources,
)
from analytics.factories.processor import (
    ProcessorsFactory,
)
from analytics.fetch.fetcher import (
    Fetcher,
)
from analytics.jobs.fetcher import (
    JobsFetcher,
)
from analytics.jobs.parser import (
    JobDataframeParser,
)
from analytics.jobs.plotting import (
    JobPlotter,
)
from analytics.merge_requests.fetcher import (
    MergeRequestsFetcher,
)
from analytics.merge_requests.parser import (
    MergeRequestDataframeParser,
)
from analytics.merge_requests.plotting import (
    MergeRequestsPlotter,
)
from analytics.parse.parser import (
    DataframeParser,
)
from analytics.pipelines.fetcher import (
    PipelinesFetcher,
)
from analytics.pipelines.fetcher_insights import PipelinesInsightsFetcher
from analytics.pipelines.parser import (
    PipelinesDataframeParser,
)
from analytics.pipelines.parser_insights import PipelineInsightsDataframeParser
from analytics.pipelines.plotting import (
    PipelinesPlotter,
)
from analytics.pipelines.plotting_insights import PipelineInsightsPlotter
from analytics.plot.plotter import (
    Plotter,
)


class JobsProcessorsFactory(ProcessorsFactory):
    def get_fetcher(self) -> Fetcher:
        return JobsFetcher()

    def get_plotter(self) -> Plotter:
        return JobPlotter()

    def get_parser(self) -> DataframeParser:
        return JobDataframeParser()


class PipelinesProcessorsFactory(ProcessorsFactory):
    def get_fetcher(self) -> Fetcher:
        return PipelinesFetcher()

    def get_plotter(self) -> Plotter:
        return PipelinesPlotter()

    def get_parser(self) -> DataframeParser:
        return PipelinesDataframeParser()


class PipelinesInsightsProcessorsFactory(ProcessorsFactory):
    def get_fetcher(self) -> Fetcher:
        return PipelinesInsightsFetcher()

    def get_plotter(self) -> Plotter:
        return PipelineInsightsPlotter()

    def get_parser(self) -> DataframeParser:
        return PipelineInsightsDataframeParser()


class MergeRequestsProcessorsFactory(ProcessorsFactory):
    def get_fetcher(self) -> Fetcher:
        return MergeRequestsFetcher()

    def get_plotter(self) -> Plotter:
        return MergeRequestsPlotter()

    def get_parser(self) -> DataframeParser:
        return MergeRequestDataframeParser()


def read_processor(resource: Resources) -> ProcessorsFactory:
    match resource:
        case Resources.JOBS:
            return JobsProcessorsFactory()
        case Resources.PIPELINES:
            return PipelinesProcessorsFactory()
        case Resources.MERGE_REQUESTS:
            return MergeRequestsProcessorsFactory()
        case Resources.PIPELINE_INSIGHTS:
            return PipelinesInsightsProcessorsFactory()
