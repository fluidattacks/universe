import logging
from dataclasses import (
    dataclass,
)

from analytics.cli.resources import (
    Resources,
    parse_resource_str,
)
from analytics.entities.jobs import (
    JobStatus,
)
from analytics.entities.match_type import (
    MatchType,
    parse_match_type_str,
)
from analytics.entities.timescale import (
    TimeScale,
    parse_time_scale_str,
)
from analytics.fetch.fetcher import (
    FetcherParameters,
)
from analytics.plot.fig_result import (
    log_fig_results,
)
from analytics.plot.plotter import (
    PlotterParams,
)
from analytics.workflows.errors import (
    FactoryFromNoneResourceError,
    NoParamsInWorkflowError,
    WorkflowPopulationError,
)
from analytics.workflows.workflow import (
    WorkFlow,
)

from .processor_mapper import (
    read_processor,
)

LOGGER = logging.getLogger()


@dataclass
class DefaultWorkflowParams:
    pages: int
    time_scale: TimeScale
    match_type: MatchType
    resource: Resources
    target_job: str | None
    product: str | None
    ref: str | None = None
    cache: bool = False


class DefaultWorkflow(WorkFlow):
    tag = "default"
    help = "Default workflow"

    def __init__(self) -> None:
        self.params: DefaultWorkflowParams | None = None

    def populate(self, workflow_data: dict) -> None:
        mandatory_keys = [
            "pages",
            "time_scale",
            "resource",
            "cache",
            "match_type",
        ]

        missing_keys = [key for key in mandatory_keys if key not in workflow_data]
        if len(missing_keys) > 0:
            raise WorkflowPopulationError(missing_keys)

        resource = parse_resource_str(workflow_data["resource"])
        time_scale = parse_time_scale_str(workflow_data["time_scale"])
        match_type = parse_match_type_str(workflow_data["match_type"])

        self.params = DefaultWorkflowParams(
            pages=workflow_data["pages"],
            time_scale=time_scale,
            match_type=match_type,
            resource=resource,
            product=workflow_data["product"] if "product" in workflow_data else None,
            target_job=workflow_data["target_job"] if "target_job" in workflow_data else None,
            cache=workflow_data["cache"],
            ref=workflow_data["ref"] if "ref" in workflow_data else None,
        )

    async def run(
        self,
        project_id: str,
        access_token: str,
    ) -> None:
        if not self.params:
            raise NoParamsInWorkflowError()
        if self.params.resource:
            processors_factory = read_processor(resource=self.params.resource)
            fetcher = processors_factory.get_fetcher()
            data_parser = processors_factory.get_parser()
            plotter = processors_factory.get_plotter()

            fetcher.set_params(
                FetcherParameters(
                    project_id=project_id,
                    access_token=access_token,
                    pages=self.params.pages,
                    statuses=[JobStatus.SUCCESS, JobStatus.FAILED],
                    ref=self.params.ref,
                ),
            )

            data_parser.parse(raw_data=await fetcher.fetch(cache=self.params.cache))
            data_parser.save_data()
            dataframe = data_parser.pop()

            plotter.set_params(
                PlotterParams(
                    time_scale=self.params.time_scale,
                    match_type=self.params.match_type,
                    product=self.params.product,
                    target_job=self.params.target_job,
                ),
            )
            plotter.set_dataframe(dataframe)
            saved_figures = await plotter.gen_all()
            log_fig_results(LOGGER, saved_figures)
            return
        raise FactoryFromNoneResourceError()
