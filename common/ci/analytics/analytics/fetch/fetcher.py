from abc import (
    ABC,
    abstractmethod,
)
from dataclasses import (
    dataclass,
)

from analytics.entities.jobs import (
    JobStatus,
)


class EmptyParamsInFetcherError(Exception):
    pass


class MissingCommitRefError(Exception):
    pass


@dataclass
class FetcherParameters:
    project_id: str
    access_token: str
    pages: int = 1
    statuses: list[JobStatus] | None = None
    ref: str | None = None


class Fetcher(ABC):
    @abstractmethod
    def set_params(self, params: FetcherParameters) -> None:
        pass

    @abstractmethod
    async def fetch(self, cache: bool = False) -> list[dict]:
        pass
