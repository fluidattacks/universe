from enum import (
    Enum,
)

from analytics.workflows.errors import WorkflowPopulationError


class MatchType(Enum):
    STARTS_WITH = "starts_with"
    ENDS_WITH = "ends_with"
    CONTAINS = "contains"
    EXACT = "exact"


def parse_match_type_str(scale_str: str) -> MatchType:
    if scale_str not in [
        MatchType.STARTS_WITH.value,
        MatchType.ENDS_WITH.value,
        MatchType.CONTAINS.value,
        MatchType.EXACT.value,
    ]:
        raise WorkflowPopulationError(["match_type"])
    return MatchType[scale_str.upper()]
