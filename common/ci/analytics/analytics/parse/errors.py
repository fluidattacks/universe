class EmptyDataInParserError(Exception):
    def __init__(self) -> None:
        msg = "Error - Current parser is empty and cannot pop data"
        super().__init__(msg)


class ParsingError(Exception):
    pass


class DataframeSavingError(Exception):
    pass


class JsonFormattingError(Exception):
    pass
