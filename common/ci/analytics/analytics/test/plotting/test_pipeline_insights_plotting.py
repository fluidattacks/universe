from pathlib import Path

import pytest

from analytics.pipelines.parser_insights import PipelineInsightsDataframeParser
from analytics.pipelines.plotting_insights import PipelineInsightsPlotter
from analytics.plot.plotter import PlotterParams
from analytics.test.mocks.pipelines_insights import FETCHED_PIPELINES_INSIGHTS


@pytest.mark.asyncio
async def test_pipeline_insights_plotting() -> None:
    pipelines_parser = PipelineInsightsDataframeParser()
    pipelines_parser.parse(raw_data=FETCHED_PIPELINES_INSIGHTS)
    dataframe = pipelines_parser.pop()
    plotter = PipelineInsightsPlotter()
    plotter.set_params(PlotterParams())
    plotter.set_dataframe(dataframe)
    results = await plotter.gen_all()
    assert len(results) == 1
    assert results[0].saved() == Path("output/figures/pipeline_dependencies_analysis.png")
