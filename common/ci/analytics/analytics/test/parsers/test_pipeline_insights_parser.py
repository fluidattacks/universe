import pytest

from analytics.pipelines.parser_insights import PipelineInsightsDataframeParser
from analytics.test.mocks.pipelines_insights import FETCHED_PIPELINES_INSIGHTS


@pytest.mark.parametrize(
    ("expected_cols"),
    (
        (
            [
                "name",
                "duration",
                "startedAt",
                "finishedAt",
                "needs",
                "hour",
                "day",
                "week",
                "month",
                "ref",
            ]
        ),
    ),
)
def test_pipeline_insights_parser(expected_cols: list[str]) -> None:
    pipelines_parser = PipelineInsightsDataframeParser()
    pipelines_parser.parse(raw_data=FETCHED_PIPELINES_INSIGHTS)
    dataframe = pipelines_parser.pop()
    assert sorted(dataframe.columns) == sorted(expected_cols)
    assert len(dataframe) == 18
