import pytest

from analytics.pipelines.parser import (
    PipelinesDataframeParser,
)
from analytics.test.mocks.pipelines import (
    FETCHED_PIPELINES,
)


@pytest.mark.parametrize(
    ("expected_cols"),
    (
        (
            [
                "startedAt",
                "finishedAt",
                "duration",
                "user",
                "commit",
                "commitTitle",
                "product",
                "hour",
                "day",
                "week",
                "month",
            ]
        ),
    ),
)
def test_pipelines_parser(expected_cols: list[str]) -> None:
    pipelines_parser = PipelinesDataframeParser()
    pipelines_parser.parse(raw_data=FETCHED_PIPELINES)
    dataframe = pipelines_parser.pop()
    assert sorted(dataframe.columns) == sorted(expected_cols)
    assert len(dataframe) == 10
