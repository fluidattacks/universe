MERGE_REQUESTS_GITLAB_RESPONSE = {
    "data": {
        "projects": {
            "nodes": [
                {
                    "id": "gid://gitlab/Project/20741933",
                    "mergeRequests": {
                        "nodes": [
                            {
                                "approved": True,
                                "createdAt": "2024-03-12T15:16:10Z",
                                "mergedAt": "2024-03-12T15:32:32Z",
                                "state": "merged",
                                "author": {"username": "dmurciaatfluid"},
                                "title": ("common\\feat(conf): #11595 re enable schedule"),
                            },
                            {
                                "approved": True,
                                "createdAt": "2024-03-12T15:12:54Z",
                                "mergedAt": "2024-03-12T15:17:39Z",
                                "state": "merged",
                                "author": {"username": "dacevedoatfluid"},
                                "title": (
                                    "integrates-jira\\feat(cross): #11488 create and view issue"
                                ),
                            },
                            {
                                "approved": True,
                                "createdAt": "2024-03-12T15:08:43Z",
                                "mergedAt": "2024-03-12T15:18:04Z",
                                "state": "merged",
                                "author": {"username": "auribeatfluid"},
                                "title": ("common\\feat(infra): #11706 configure ztna"),
                            },
                            {
                                "approved": True,
                                "createdAt": "2024-03-12T15:05:10Z",
                                "mergedAt": "2024-03-12T15:17:09Z",
                                "state": "merged",
                                "author": {"username": "dacostaatfluid"},
                                "title": ("common\\refac(build): #11710 update users and groups"),
                            },
                            {
                                "approved": True,
                                "createdAt": "2024-03-12T14:51:06Z",
                                "mergedAt": "2024-03-12T15:05:33Z",
                                "state": "merged",
                                "author": {"username": "dacevedoatfluid"},
                                "title": ("integrates-jira\\feat(cross): #11488 prepare issues"),
                            },
                            {
                                "approved": True,
                                "createdAt": "2024-03-12T14:48:24Z",
                                "mergedAt": "2024-03-12T14:56:13Z",
                                "state": "merged",
                                "author": {"username": "slizcanoatfluid"},
                                "title": ("integrates-front\\refac(front): #11683 include msg"),
                            },
                            {
                                "approved": False,
                                "createdAt": "2024-03-12T14:41:31Z",
                                "mergedAt": None,
                                "state": "closed",
                                "author": {"username": "jperezatfluid"},
                                "title": ("skims\\refac(back): #11666 check if is ci job"),
                            },
                        ],
                        "pageInfo": {
                            "startCursor": (
                                "eyJjcmVhdGVkX2F0IjoiMjAyNC0wMy0x"
                                "MyAxMzozOTozOC41MDY4MzMwMDAgKzAw"
                                "MDAiLCJpZCI6IjI4ODM0ODI5OCJ9"
                            ),
                            "endCursor": (
                                "eyJjcmVhdGVkX2F0IjoiMjAyNC0wMy0xMi"
                                "AxNDo0MTozMS44NjE5OTEwMDAgKzAwMDAi"
                                "LCJpZCI6IjI4ODA5NzQ4OSJ9"
                            ),
                            "hasPreviousPage": False,
                            "hasNextPage": True,
                        },
                    },
                },
            ],
        },
    },
}

FETCHED_MERGE_REQUESTS = [
    {
        "approved": True,
        "createdAt": "2024-03-12T15:16:10Z",
        "mergedAt": "2024-03-12T15:32:32Z",
        "state": "merged",
        "author": "dmurciaatfluid",
        "title": "common\\feat(conf): #11595 re enable schedule",
    },
    {
        "approved": True,
        "createdAt": "2024-03-12T15:12:54Z",
        "mergedAt": "2024-03-12T15:17:39Z",
        "state": "merged",
        "author": "dacevedoatfluid",
        "title": "integrates-jira\\feat(cross): #11488 create and view issue",
    },
    {
        "approved": True,
        "createdAt": "2024-03-12T15:08:43Z",
        "mergedAt": "2024-03-12T15:18:04Z",
        "state": "merged",
        "author": "auribeatfluid",
        "title": "common\\feat(infra): #11706 configure ztna",
    },
    {
        "approved": True,
        "createdAt": "2024-03-12T15:05:10Z",
        "mergedAt": "2024-03-12T15:17:09Z",
        "state": "merged",
        "author": "dacostaatfluid",
        "title": "common\\refac(build): #11710 update users and groups",
    },
    {
        "approved": True,
        "createdAt": "2024-03-12T14:51:06Z",
        "mergedAt": "2024-03-12T15:05:33Z",
        "state": "merged",
        "author": "dacevedoatfluid",
        "title": "integrates-jira\\feat(cross): #11488 prepare issues",
    },
    {
        "approved": True,
        "createdAt": "2024-03-12T14:48:24Z",
        "mergedAt": "2024-03-12T14:56:13Z",
        "state": "merged",
        "author": "slizcanoatfluid",
        "title": "integrates-front\\refac(front): #11683 include msg",
    },
    {
        "approved": False,
        "createdAt": "2024-03-12T14:41:31Z",
        "mergedAt": None,
        "state": "closed",
        "author": "jperezatfluid",
        "title": "skims\\refac(back): #11666 check if is ci job",
    },
]
