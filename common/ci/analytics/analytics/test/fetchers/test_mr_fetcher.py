from unittest.mock import (
    patch,
)

import pytest
from httpx import (
    Response,
)

from analytics.fetch.fetcher import (
    FetcherParameters,
)
from analytics.merge_requests.fetcher import (
    MergeRequestsFetcher,
)
from analytics.test.mocks.merge_requests import (
    FETCHED_MERGE_REQUESTS,
    MERGE_REQUESTS_GITLAB_RESPONSE,
)

MODULE_AT_TEST = "analytics.merge_requests.fetcher."


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("project_id", "access_token", "gitlab_response", "fetched_data"),
    (
        (
            "test_project_id",
            "1234",
            MERGE_REQUESTS_GITLAB_RESPONSE,
            FETCHED_MERGE_REQUESTS,
        ),
    ),
)
async def test_merge_request_fetching(
    project_id: str,
    access_token: str,
    gitlab_response: dict,
    fetched_data: list[dict],
) -> None:
    pipelines_fetcher = MergeRequestsFetcher()
    pipelines_fetcher.set_params(
        FetcherParameters(project_id=project_id, access_token=access_token),
    )
    with patch(MODULE_AT_TEST + "ApiClient.post") as mocked_method:
        mocked_method.return_value = Response(200, json=gitlab_response)
        pipelines = await pipelines_fetcher.fetch()
        assert pipelines == fetched_data
        mocked_method.assert_awaited_once()
