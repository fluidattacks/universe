from unittest.mock import (
    patch,
)

import pytest
from httpx import (
    Response,
)

from analytics.fetch.fetcher import (
    FetcherParameters,
)
from analytics.jobs.fetcher import (
    JobsFetcher,
)
from analytics.test.mocks.jobs import (
    FETCHED_JOBS,
    JOBS_GITLAB_RESPONSE,
)

MODULE_AT_TEST = "analytics.jobs.fetcher."


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("project_id", "access_token", "gitlab_response", "fetched_data"),
    (("test_project_id", "1234", JOBS_GITLAB_RESPONSE, FETCHED_JOBS),),
)
async def test_job_fetching(
    project_id: str,
    access_token: str,
    gitlab_response: dict,
    fetched_data: list[dict],
) -> None:
    jobs_fetcher = JobsFetcher()
    jobs_fetcher.set_params(FetcherParameters(project_id=project_id, access_token=access_token))
    with patch(MODULE_AT_TEST + "ApiClient.post") as mocked_method:
        mocked_method.return_value = Response(200, json=gitlab_response)
        jobs = await jobs_fetcher.fetch()
        assert jobs == fetched_data
        mocked_method.assert_awaited_once()
