import logging

from progressbar import (
    progressbar,
)

from analytics.entities.jobs import (
    JobStatus,
)
from analytics.errors.api_errors import (
    GitlabApiConnectionError,
)
from analytics.fetch.client import (
    ApiClient,
)
from analytics.fetch.fetcher import (
    EmptyParamsInFetcherError,
    Fetcher,
    FetcherParameters,
)
from analytics.fetch.utils.caching import (
    read_cache,
    renew_cache,
)
from analytics.parse.errors import (
    JsonFormattingError,
)

from .queries import (
    JOBS_QUERY,
)

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)

CACHE_TYPE = "jobs"


def _parse_job_statuses(statuses: list[JobStatus]) -> str:
    elements_str = ", ".join(status.value for status in statuses)
    result_str = "[" + elements_str + "]"
    return result_str


def parse_page_info(data: dict) -> tuple[str, str, str, list[dict]]:
    start_cursor = data["data"]["projects"]["nodes"][0]["jobs"]["pageInfo"]["startCursor"]
    after = data["data"]["projects"]["nodes"][0]["jobs"]["pageInfo"]["endCursor"]
    has_next_page = data["data"]["projects"]["nodes"][0]["jobs"]["pageInfo"]["hasNextPage"]
    jobs_raw_batch = data["data"]["projects"]["nodes"][0]["jobs"]["nodes"]
    return start_cursor, after, has_next_page, jobs_raw_batch


def transform_raw_batch(jobs_raw_batch: list[dict]) -> list[dict]:
    for i, _ in enumerate(jobs_raw_batch):
        jobs_raw_batch[i]["commitTitle"] = jobs_raw_batch[i]["pipeline"]["commit"]["title"]
        jobs_raw_batch[i]["stage"] = jobs_raw_batch[i]["stage"]["name"]
        jobs_raw_batch[i]["detailedStatus"] = jobs_raw_batch[i]["detailedStatus"]["name"]
    return jobs_raw_batch


class JobsFetcher(Fetcher):
    def __init__(self) -> None:
        self.params: FetcherParameters | None = None

    def set_params(self, params: FetcherParameters) -> None:
        self.params = params

    async def fetch(self, cache: bool = False) -> list[dict]:
        if self.params is None:
            raise EmptyParamsInFetcherError()

        if cache:
            return await read_cache(CACHE_TYPE)

        query = JOBS_QUERY.replace("#ID", f'"{self.params.project_id}"')

        query = (
            query.replace(", statuses: #STATUSES", "")
            if self.params.statuses is None
            else query.replace(
                "#STATUSES",
                f"{_parse_job_statuses(self.params.statuses)}",
            )
        )
        after = ""
        jobs_raw = []
        first_cursor = ""
        try:
            api_client = ApiClient(access_token=self.params.access_token)
            for _ in progressbar(range(self.params.pages)):
                req_query = query.replace("#AFTER", f'"{after}"')
                api_response = await api_client.post(req_query)
                if not api_response.is_success:
                    raise GitlabApiConnectionError(
                        "Error connecting to gitlab API: "
                        f"{api_response.json()['errors']}-"
                        f"({api_response.status_code})",
                    )

                data = api_response.json()

                (
                    start_cursor,
                    after,
                    has_next_page,
                    jobs_raw_batch,
                ) = parse_page_info(data)
                if first_cursor == "":
                    first_cursor = start_cursor

                jobs_raw_batch = transform_raw_batch(jobs_raw_batch)
                jobs_raw.extend(jobs_raw_batch)

                if not has_next_page:
                    break
            await api_client.close()

        except Exception as exc:
            raise JsonFormattingError("Could not parse jobs from data.") from exc

        await renew_cache(
            data=jobs_raw,
            start_cursor=first_cursor,
            end_cursor=after,
            cache_type=CACHE_TYPE,
        )
        return jobs_raw
