import asyncio
import logging
import os
from pathlib import (
    Path,
)

from pandas import (
    DataFrame,
)

from analytics.plot.fig_result import (
    FailureReason,
    FigFailed,
    FigResult,
    FigSkipped,
    SkipReason,
)
from analytics.plot.plotter import (
    FixedSize,
    Plotter,
    PlotterParams,
)
from analytics.plot.utils import (
    FigureOptions,
    HorizontalAlignment,
    filter_with_match_type,
    save_boxplot_fig,
    save_countplot_fig,
    save_countplot_multi_fig,
    save_simple_barplot_fig,
)

FIG_SAVE_DIR = "output/figures"
LOGGER = logging.getLogger()


class JobPlotter(Plotter):
    def __create_base_dir(self) -> None:
        if not os.path.exists(FIG_SAVE_DIR):
            os.makedirs(FIG_SAVE_DIR)

    def __init__(self) -> None:
        self.params = PlotterParams()
        self.__create_base_dir()
        self.data: DataFrame | None = None

    def set_params(self, params: PlotterParams) -> None:
        self.params = params

    def set_dataframe(self, data: DataFrame) -> None:
        self.data = data

    def __filter_matching_job(self, data: DataFrame, match_job: str) -> DataFrame:
        return filter_with_match_type(
            data=data,
            match_type=self.params.match_type,
            field="name",
            match_str=match_job,
        )

    def __filter_matching_product(self, data: DataFrame, match_product: str) -> DataFrame:
        return filter_with_match_type(
            data=data,
            match_type=self.params.match_type,
            field="product",
            match_str=match_product,
        )

    async def gen_statuses_for_matching_job(self, data: DataFrame) -> FigResult:
        if not self.params.target_job:
            return FigSkipped(SkipReason("No target job specified. Skipping job statuses plot."))
        fig_file_name = f"jobs_{self.params.target_job.replace('/', '_')}_statuses.png"
        filtered_data = self.__filter_matching_job(data, self.params.target_job)
        if len(filtered_data) == 0:
            return FigFailed(
                FailureReason(
                    "Could not find jobs which match: "
                    f"{self.params.target_job} "
                    f"in {len(data.index)} jobs",
                ),
            )
        return await save_countplot_fig(
            data=filtered_data,
            ax_field="status",
            hue_field="name",
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            options=FigureOptions(
                size=FixedSize(18, 6),
                title=(
                    f"Job statuses for jobs matching: {self.params.target_job}"
                    f" ({self.params.match_type.value} match)"
                    f" ({len(filtered_data)} records)"
                ),
            ),
        )

    async def gen_slowest_jobs(self, data: DataFrame) -> FigResult:
        fig_file_name = "slowest_jobs.png"
        averaged_data = data[["name", "duration"]].groupby("name")["duration"].mean()
        slowest_jobs = averaged_data.nlargest(10)
        LOGGER.info("Slowest jobs: %s", slowest_jobs)
        return await save_simple_barplot_fig(
            data=slowest_jobs,
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            options=FigureOptions(
                size=FixedSize(20, 16),
                rotation=-15,
                x_ticks_font_size=5,
                horizontal_alignment=HorizontalAlignment.LEFT,
                title=f"Top 10 slowest jobs ({len(data)} records)",
            ),
        )

    async def gen_most_fragile_jobs(self, data: DataFrame) -> FigResult:
        fig_file_name = "jobs_most_fragile.png"
        failed_jobs = data[data["status"] != "SUCCESS"]
        most_fragile_jobs = failed_jobs["name"].value_counts().nlargest(10)
        LOGGER.info("Most fragile jobs: %s", most_fragile_jobs)
        return await save_simple_barplot_fig(
            data=most_fragile_jobs,
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            options=FigureOptions(
                size=FixedSize(20, 16),
                rotation=-15,
                x_ticks_font_size=5,
                horizontal_alignment=HorizontalAlignment.LEFT,
                title=(f"Top 10 most fragile jobs ({len(most_fragile_jobs)} records)"),
            ),
        )

    async def gen_most_fragile_jobs_for_product(self, data: DataFrame) -> FigResult:
        if self.params.product is None:
            return FigSkipped(
                SkipReason("No product specified. Skipping most fragile jobs for product."),
            )
        fig_file_name = f"jobs_most_fragile_for_{self.params.product}.png"
        filtered_data = self.__filter_matching_product(data, self.params.product)
        failed_jobs = filtered_data[filtered_data["status"] != "SUCCESS"]
        most_fragile_jobs_for_product = failed_jobs["name"].value_counts().nlargest(10)
        LOGGER.info("Most fragile jobs: %s", most_fragile_jobs_for_product)
        return await save_simple_barplot_fig(
            data=most_fragile_jobs_for_product,
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            options=FigureOptions(
                size=FixedSize(20, 16),
                rotation=-15,
                x_ticks_font_size=5,
                horizontal_alignment=HorizontalAlignment.LEFT,
                title=(
                    "Top 10 most fragile jobs for product: "
                    f"{self.params.product}. "
                    f"Match type: {self.params.match_type.value}"
                    f"({len(most_fragile_jobs_for_product)} records)"
                ),
            ),
        )

    async def gen_statuses_for_matching_job_over_time(self, data: DataFrame) -> FigResult:
        if not self.params.target_job:
            return FigSkipped(SkipReason("No target job specified. Skipping job statuses plot."))
        fig_file_name = f"jobs_{self.params.target_job.replace('/', '_')}_statuses_over_time.png"
        filtered_data = self.__filter_matching_job(data, self.params.target_job).sort_values(
            by="startedAt",
        )
        if len(filtered_data) == 0:
            return FigFailed(
                FailureReason(
                    "Could not find jobs which match: "
                    f"{self.params.target_job} "
                    f"in {len(data.index)} jobs",
                ),
            )
        return await save_countplot_fig(
            data=filtered_data,
            ax_field=self.params.time_scale.value,
            hue_field="status",
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            options=FigureOptions(
                size=FixedSize(18, 6),
                title=(
                    f"Job statuses over time matching {self.params.target_job}"
                    f" {self.params.match_type.value} match"
                    f" ({len(filtered_data)} records)"
                ),
            ),
        )

    async def gen_durations_for_matching_job(self, data: DataFrame) -> FigResult:
        if not self.params.target_job:
            return FigSkipped(SkipReason("No target job specified. Skipping job duration plot."))
        cleaned_name = self.params.target_job.replace("/", "_")
        fig_file_name = f"jobs_{cleaned_name}_duration_analysis.png"
        filtered_data = self.__filter_matching_job(data, self.params.target_job)
        if len(filtered_data) == 0:
            return FigFailed(
                FailureReason(
                    "Could not find jobs which matched: "
                    f"{self.params.target_job} "
                    f"in {len(data.index)} jobs",
                ),
            )

        return await save_boxplot_fig(
            data=filtered_data.sort_values(by=self.params.time_scale.value),
            x_field=self.params.time_scale.value,
            y_field="duration",
            hue_field="name",
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            options=FigureOptions(
                size=FixedSize(18, 6),
                title=(
                    f"Durations for jobs matching: {self.params.target_job}"
                    f"{self.params.match_type.value} match"
                    f" ({len(filtered_data)} records)"
                ),
            ),
        )

    async def gen_job_detailed_statuses_plot(self, data: DataFrame) -> FigResult:
        fig_name = "job_detailed_statuses.png"
        return await save_countplot_fig(
            data=data,
            ax_field="detailedStatus",
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_name}"),
            options=FigureOptions(
                size=FixedSize(42, 7),
                title=(
                    "Detailed job statuses jobs between "
                    f"{data['finishedAt'].min()} and "
                    f"{data['finishedAt'].max()}"
                    f" ({len(data)} records)"
                ),
            ),
        )

    async def gen_job_statuses_plot(self, data: DataFrame) -> FigResult:
        fig_name = "job_statuses.png"
        return await save_countplot_fig(
            data=data,
            ax_field="status",
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_name}"),
            options=FigureOptions(
                size=FixedSize(22, 7),
                title=(
                    "Job statuses between "
                    f"{data['finishedAt'].min()} and "
                    f"{data['finishedAt'].max()}"
                    f" ({len(data)} records)"
                ),
            ),
        )

    async def gen_job_statuses_plot_over_time(self, data: DataFrame) -> FigResult:
        fig_file_name = "job_statuses_over_time.png"
        return await save_countplot_fig(
            data=data.sort_values(by="startedAt"),
            ax_field=self.params.time_scale.value,
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            hue_field="status",
            options=FigureOptions(
                size=FixedSize(28, 12),
                rotation=45,
                palette="deep",
                title="Total jobs by status result",
            ),
        )

    async def gen_job_statuses_by_product_plot_over_time(self, data: DataFrame) -> FigResult:
        fig_file_name = "job_statuses_over_time_by_product.png"
        return await save_countplot_multi_fig(
            data=data.sort_values(by="startedAt"),
            ax_field=self.params.time_scale.value,
            multi_plot_field="status",
            fig_path=Path(f"{FIG_SAVE_DIR}/{fig_file_name}"),
            titles=[
                "Succeeded jobs by product",
                "Script failure jobs by product",
                "Runner failure jobs by product",
            ],
            hue_field="product",
            values_to_eval=["SUCCESS", "Runner failure", "Script Failure"],
            options=FigureOptions(
                size=FixedSize(),
                palette="deep",
                rotation=45,
            ),
        )

    async def gen_all(self) -> tuple[FigResult, ...]:
        if self.data is None:
            return (FigFailed(FailureReason("Empty data in plotter")),)
        return tuple(
            await asyncio.gather(
                self.gen_job_statuses_plot_over_time(data=self.data),
                self.gen_job_statuses_by_product_plot_over_time(data=self.data),
                self.gen_job_statuses_plot(data=self.data),
                self.gen_job_detailed_statuses_plot(data=self.data),
                self.gen_slowest_jobs(data=self.data),
                self.gen_durations_for_matching_job(data=self.data),
                self.gen_statuses_for_matching_job(data=self.data),
                self.gen_statuses_for_matching_job_over_time(data=self.data),
                self.gen_most_fragile_jobs(data=self.data),
                self.gen_most_fragile_jobs_for_product(data=self.data),
            ),
        )
