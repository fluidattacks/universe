import argparse
from typing import (
    NamedTuple,
)


class OptionArgs(NamedTuple):
    name: str
    help: str
    action: str = "store"
    var_type: type | None = None
    default: str | int | None = None
    choices: list[str | int] | None = None

    def add_to_parser(self, parser: argparse.ArgumentParser) -> None:
        if self.var_type is not None:
            parser.add_argument(
                self.name,
                help=self.help,
                action=self.action,
                type=self.var_type,
                default=self.default,
                choices=self.choices,
            )
        else:
            parser.add_argument(
                self.name,
                help=self.help,
                action=self.action,
                default=self.default,
            )
