import argparse

from analytics.cli.errors import (
    MissingSetupInCliWorkflow,
)
from analytics.cli.setup import (
    get_arg_setup_by_workflow,
)
from analytics.workflows.workflow import (
    WorkFlow,
)


def configure_cli_workflow(workflow_tag: str, subparser: argparse.ArgumentParser) -> None:
    workflow_args = get_arg_setup_by_workflow(workflow_tag)
    if workflow_args is None:
        raise MissingSetupInCliWorkflow(workflow_tag)
    for arg in workflow_args:
        arg.add_to_parser(subparser)


def build_arg_parser(
    enabled_workflows: list[WorkFlow],
) -> argparse.ArgumentParser:
    main_parser = argparse.ArgumentParser(
        prog="Gitlab graphql api",
        description="Retrieves data and produces plots from gitlab api.",
    )
    subparsers = main_parser.add_subparsers(title="workflows", dest="workflow")
    for workflow in enabled_workflows:
        command_subparser = subparsers.add_parser(workflow.tag, help=workflow.help)
        configure_cli_workflow(workflow.tag, command_subparser)

    return main_parser
