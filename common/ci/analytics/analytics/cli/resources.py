from enum import (
    Enum,
)

from analytics.workflows.errors import WorkflowPopulationError


class Resources(Enum):
    PIPELINES = "pipelines"
    PIPELINE_INSIGHTS = "pipeline_insights"
    JOBS = "jobs"
    MERGE_REQUESTS = "merge_requests"


def parse_resource_str(resource_str: str) -> Resources:
    if resource_str not in [
        Resources.JOBS.value,
        Resources.PIPELINES.value,
        Resources.PIPELINE_INSIGHTS.value,
        Resources.MERGE_REQUESTS.value,
    ]:
        raise WorkflowPopulationError(["resource"])
    return Resources[resource_str.upper()]
