from enum import (
    Enum,
)


class MergeRequestState(Enum):
    MERGED = "merged"
    OPENED = "opened"
    CLOSED = "closed"
    LOCKED = "locked"
