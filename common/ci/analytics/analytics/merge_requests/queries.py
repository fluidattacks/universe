MERGE_REQUESTS_QUERY = """
  {
    projects(ids: [#ID]) {
      nodes {
      id
      mergeRequests {
        nodes {
          approved
          createdAt
          mergedAt
          state
          author {
            username
          }
          title
        }
        pageInfo {
          startCursor
          endCursor
          hasPreviousPage
          hasNextPage
        }
      }
    }
    }
  }
"""
