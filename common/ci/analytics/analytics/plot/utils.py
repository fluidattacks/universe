from dataclasses import (
    dataclass,
)
from enum import (
    Enum,
)
from pathlib import (
    Path,
)
from typing import (
    cast,
)

from matplotlib import (
    pyplot as plt,
)
from matplotlib.container import (
    BarContainer,
)
from matplotlib.dates import DateFormatter
from pandas import (
    DataFrame,
    Timedelta,
    crosstab,
)
from seaborn import (
    barplot,
    boxplot,
    countplot,
    heatmap,
    move_legend,
)
from tabulate import (
    tabulate,
)

from analytics.entities.match_type import (
    MatchType,
)
from analytics.plot.fig_result import (
    FigResult,
    FigSaved,
)
from analytics.plot.plotter import (
    FigSize,
    FixedSize,
)


class HorizontalAlignment(Enum):
    LEFT = "left"
    CENTER = "center"
    RIGHT = "right"


BARPLOT_FONT_SIZE = 10
X_TICK_DEFAULT_FONT_SIZE = 8.0
DEFAULT_H_ALIGNMENT = HorizontalAlignment.RIGHT
LEGEND_LOCATION = "lower center"
DEFAULT_LOG_SCALE = False
VERTICAL_OFFSET_SCALING = 0.05
H_BARPLOT_OFFSET_SECONDS = 20


@dataclass
class FigureOptions:
    size: FigSize
    title: str = ""
    legend_move: bool = False
    horizontal_alignment: HorizontalAlignment = DEFAULT_H_ALIGNMENT
    x_ticks_font_size: float = X_TICK_DEFAULT_FONT_SIZE
    palette: str | None = None
    rotation: float | None = None
    log_scale: bool = DEFAULT_LOG_SCALE


class InvalidTitleValuesLenError(Exception):
    pass


def apply_cross_tab(data: DataFrame, pivots: tuple[str, str]) -> DataFrame:
    return crosstab(
        data[pivots[0]],
        data[pivots[1]],
    )


async def save_countplot_fig(
    data: DataFrame,
    ax_field: str,
    fig_path: Path,
    hue_field: str | None = None,
    options: FigureOptions = FigureOptions(size=FixedSize()),
) -> FigResult:
    fig = plt.figure(figsize=options.size.size_inches)
    fig_ax = fig.add_subplot(111)
    countplot(
        data=data,
        x=ax_field,
        hue=hue_field,
        ax=fig_ax,
        palette=options.palette,
    )
    if options.legend_move:
        move_legend(fig_ax, LEGEND_LOCATION, bbox_to_anchor=(1, 1))
    fig_ax.grid()
    if options.rotation is not None:
        _ = fig_ax.set_xticklabels(
            fig_ax.get_xticklabels(),
            rotation=options.rotation,
            ha=options.horizontal_alignment.value,
        )
    fig_ax.set_title(options.title)
    for container in fig_ax.containers:
        barplot_container = cast(BarContainer, container)
        fig_ax.bar_label(barplot_container, fontsize=BARPLOT_FONT_SIZE)
    fig.savefig(fig_path)
    return FigSaved(fig_path)


async def save_countplot_multi_fig(
    data: DataFrame,
    ax_field: str,
    multi_plot_field: str,
    fig_path: Path,
    titles: list[str],
    values_to_eval: list[str],
    hue_field: str | None = None,
    options: FigureOptions = FigureOptions(size=FixedSize()),
) -> FigResult:
    if len(values_to_eval) != len(titles):
        raise InvalidTitleValuesLenError(
            f"Lenght of values {len(values_to_eval)} "
            f"should be equal to lenght of titles {len(titles)}",
        )
    fig, axes = plt.subplots(nrows=len(values_to_eval), ncols=1)
    fig.set_size_inches(options.size.size_inches)

    for fig_ax, value, title in zip(axes, values_to_eval, titles, strict=False):
        countplot(
            data=data[data[multi_plot_field] == value],
            x=ax_field,
            hue=hue_field,
            palette=options.palette,
            ax=fig_ax,
        )

        fig_ax.grid()
        fig_ax.set_title(title)
        fig_ax.set_xticks(fig_ax.get_xticks())
        if options.legend_move:
            move_legend(fig_ax, LEGEND_LOCATION, bbox_to_anchor=(1, 1))
        if options.rotation is not None:
            _ = fig_ax.set_xticklabels(
                fig_ax.get_xticklabels(),
                rotation=options.rotation,
                ha=options.horizontal_alignment.value,
            )
    fig.savefig(fig_path)
    return FigSaved(fig_path)


async def save_boxplot_fig(
    data: DataFrame,
    y_field: str,
    fig_path: Path,
    x_field: str | None = None,
    hue_field: str | None = None,
    options: FigureOptions = FigureOptions(size=FixedSize()),
) -> FigResult:
    figsize = options.size.size_inches
    fig = plt.figure(figsize=figsize)
    fig_ax = fig.add_subplot(111)
    boxplot(
        data=data,
        x=x_field,
        hue=hue_field,
        y=y_field,
        ax=fig_ax,
        palette=options.palette,
        log_scale=options.log_scale,
    )
    if x_field:
        means = data.groupby([x_field])[y_field].mean()
        means_str = list(map(lambda mean: f"{mean:.2f}", means))
        vertical_offset = data[y_field].mean() * VERTICAL_OFFSET_SCALING

        for x_tick in fig_ax.get_xticks():
            fig_ax.text(
                x_tick,
                data[y_field].max() - vertical_offset,
                means_str[x_tick],
                ha="center",
                size="x-small",
                color="black",
                weight="bold",
            )
    fig_ax.grid()
    if options.legend_move:
        move_legend(fig_ax, LEGEND_LOCATION, bbox_to_anchor=(1, 0.5))
    if options.rotation is not None:
        _ = fig_ax.set_xticklabels(
            fig_ax.get_xticklabels(),
            rotation=options.rotation,
            ha=options.horizontal_alignment.value,
        )
    fig_ax.set_title(options.title)
    fig.savefig(fig_path)
    return FigSaved(fig_path)


async def save_heatmap_fig(
    data: DataFrame,
    fig_path: Path,
    pivots: tuple[str, str],
    options: FigureOptions = FigureOptions(size=FixedSize()),
) -> FigResult:
    cross_tab = apply_cross_tab(data, pivots=pivots)
    fig = plt.figure(figsize=options.size.size_inches)
    fig_ax = fig.add_subplot(111)
    heatmap(cross_tab, ax=fig_ax, annot=True)
    fig_ax.set_title(options.title)
    plt.savefig(fig_path)
    return FigSaved(fig_path)


async def save_simple_barplot_fig(
    data: DataFrame,
    fig_path: Path,
    options: FigureOptions = FigureOptions(size=FixedSize()),
) -> FigResult:
    fig = plt.figure(figsize=options.size.size_inches)
    fig_ax = fig.add_subplot(111)
    barplot(
        data=data,
        ax=fig_ax,
    )
    fig_ax.set_title(options.title)
    fig_ax.grid()
    if options.rotation is not None:
        _ = fig_ax.set_xticklabels(
            fig_ax.get_xticklabels(),
            rotation=options.rotation,
            ha=options.horizontal_alignment.value,
        )
    _ = map(
        lambda x_tick_item: x_tick_item.set_fontsize(options.x_ticks_font_size),
        fig_ax.get_xticklabels(),
    )
    fig.savefig(fig_path)
    return FigSaved(fig_path)


async def save_barplot_fig(
    data: DataFrame,
    fig_path: Path,
    options: FigureOptions | None = None,
    x_field: str | None = None,
    y_field: str | None = None,
    hue_field: str | None = None,
) -> FigResult:
    if options is None:
        options = FigureOptions(size=FixedSize())
    fig = plt.figure(figsize=options.size.size_inches)
    fig_ax = fig.add_subplot(111)
    barplot(data=data, ax=fig_ax, hue=hue_field, x=x_field, y=y_field)
    fig_ax.set_title(options.title)
    fig_ax.grid()
    if options.rotation is not None:
        _ = fig_ax.set_xticklabels(
            fig_ax.get_xticklabels(),
            rotation=options.rotation,
            ha=options.horizontal_alignment.value,
        )
    _ = map(
        lambda x_tick_item: x_tick_item.set_fontsize(options.x_ticks_font_size),
        fig_ax.get_xticklabels(),
    )
    fig.savefig(fig_path)
    return FigSaved(fig_path)


def filter_with_match_type(
    data: DataFrame,
    match_type: MatchType,
    field: str,
    match_str: str,
) -> DataFrame:
    match match_type:
        case MatchType.CONTAINS:
            return data[data[field].str.contains(match_str)]
        case MatchType.STARTS_WITH:
            return data[data[field].str.startswith(match_str)]
        case MatchType.ENDS_WITH:
            return data[data[field].str.endswith(match_str)]
        case MatchType.EXACT:
            return data[data[field] == match_str]


def summarize_data(data: DataFrame, group_fields: list[str], target_field: str) -> str:
    return tabulate(
        data.groupby(by=group_fields).agg(
            duration_mean=(target_field, "mean"),
            duration_min=(target_field, "min"),
            duration_max=(target_field, "max"),
            duration_q25=(target_field, lambda x: x.quantile(q=0.25)),
            duration_median=(target_field, "median"),
            duration_q75=(target_field, lambda x: x.quantile(q=0.75)),
            total_samples=(target_field, "count"),
        ),
        headers="keys",
        tablefmt="mixed_grid",
    )


async def save_dep_pipelines_analysis(
    data: DataFrame,
    fig_path: Path,
    ref: str,
) -> FigResult:
    dependency_groups = (
        data.groupby("needs")
        .agg(start_time=("startedAt", "min"), end_time=("finishedAt", "max"))
        .reset_index()
    )

    dependency_groups["total_duration"] = (
        dependency_groups["end_time"] - dependency_groups["start_time"]
    ).dt.total_seconds()

    fig, ax = plt.subplots(figsize=(20, 12))
    for i, row in dependency_groups.iterrows():
        start_time_mpl = row["start_time"].to_pydatetime()

        ax.barh(
            y=i,
            width=row["total_duration"] / (3600 * 24),
            left=start_time_mpl,
            color="#bf0b1a",
            edgecolor="black",
        )
        ax.text(
            x=start_time_mpl + Timedelta(seconds=row["total_duration"] / 2),
            y=i,
            s=f"{int(row['total_duration'] // 60)}m",
            ha="center",
            va="center",
            color="white",
            fontweight="bold",
        )

    ax.set_yticks(range(len(dependency_groups)))
    ax.set_yticklabels(dependency_groups["needs"])

    ax.set_xlabel("Time")
    ax.set_title(f"Job Dependencies and Durations for ref {ref}")
    ax.set_ylabel("Needs jobs")

    ax.xaxis.set_major_formatter(DateFormatter("%Y-%m-%d %H:%M:%S"))
    fig.autofmt_xdate()
    ax.set_xlim(data["startedAt"].min(), data["finishedAt"].max())
    ax.grid()

    fig.savefig(fig_path)
    return FigSaved(fig_path)
