from abc import (
    ABC,
    abstractmethod,
)
from dataclasses import (
    dataclass,
)

from pandas import (
    DataFrame,
)

from analytics.entities.match_type import (
    MatchType,
)
from analytics.entities.timescale import (
    TimeScale,
)

from .fig_result import (
    FigResult,
)

DEFAULT_WIDTH = 16
DEFAULT_HEIGHT = 5
DYNAMIC_FIG_WIDTH_PER_HUE = 2
DYNAMIC_FIG_BASE_WIDTH = 5
DYNAMIC_FIG_HEIGHT = 6


class FigSize(ABC):
    @property
    @abstractmethod
    def size_inches(self) -> tuple[float, float]:
        pass


@dataclass
class FixedSize(FigSize):
    width_inches: float = DEFAULT_WIDTH
    height_inches: float = DEFAULT_HEIGHT

    @property
    def size_inches(self) -> tuple[float, float]:
        return (self.width_inches, self.height_inches)


@dataclass
class DynamicSize(FigSize):
    hue_len: int
    height_inches: float = DYNAMIC_FIG_HEIGHT
    base_width_inches: float = DYNAMIC_FIG_HEIGHT
    width_per_hue_inches: float = DYNAMIC_FIG_WIDTH_PER_HUE

    @property
    def size_inches(self) -> tuple[float, float]:
        return (
            self.base_width_inches + self.width_per_hue_inches * self.hue_len,
            self.height_inches,
        )


@dataclass
class PlotterParams:
    time_scale: TimeScale = TimeScale.DAY
    match_type: MatchType = MatchType.CONTAINS
    product: str | None = None
    target_job: str | None = None
    with_flakiness: bool | None = False


class Plotter:
    @abstractmethod
    def set_dataframe(self, data: DataFrame) -> None:
        pass

    @abstractmethod
    def set_params(self, params: PlotterParams) -> None:
        pass

    @abstractmethod
    async def gen_all(self) -> tuple[FigResult, ...]:
        pass
