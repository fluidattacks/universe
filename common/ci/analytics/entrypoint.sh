# shellcheck shell=bash

function _run {
  aws_login dev 3600
  sops_export_vars __argCommonSecretsDev__ UNIVERSE_ANALYTICS_TOKEN
  poetry run analytics "${@}"
}

function _lint {
  if [[ ${CI:-} == "" ]]; then
    poetry run ruff format --config ruff.toml
    poetry run ruff check --config ruff.toml --fix
  else
    poetry run ruff format --config ruff.toml --diff
    poetry run ruff check --config ruff.toml
  fi

  poetry run mypy --config-file mypy.ini analytics
}

function _test {
  poetry run pytest -rP analytics/test
}

function main {
  local dir="common/ci/analytics"

  pushd "${dir}" || error "${dir} directory not found"
  poetry install

  case "${1:-}" in
    run) _run "${@:2}" ;;
    lint) _lint "${@:2}" ;;
    test) _test "${@:2}" ;;
    *) error "Must provide either 'run', 'lint', or 'test' as the first argument" ;;
  esac
}

main "${@}"
