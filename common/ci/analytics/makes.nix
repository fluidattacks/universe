{ inputs, makeScript, outputs, projectPath, ... }: {
  jobs."/common/ci/analytics" = makeScript {
    name = "common-ci-analytics";
    replace.__argCommonSecretsDev__ = projectPath "/common/secrets/dev.yaml";
    searchPaths = {
      bin = [ inputs.nixpkgs.patchelf inputs.nixpkgs.poetry ];
      rpath = [ inputs.nixpkgs.gcc.cc.lib ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
