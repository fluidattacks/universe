{ config, inputs, pkgs, ... }:
let utils = import inputs.utils { inherit pkgs; };
in {
  cachix = utils.cachix.config.default;

  packages = [
    # Python dependencies outside of poetry
    config.languages.python.package.pkgs.ruff

    utils.shell.aws
    utils.shell.sops
  ];

  languages.python = {
    enable = true;
    package = pkgs.python311;
    poetry = {
      enable = true;
      install = {
        enable = true;
        installRootPackage = true;
      };
      activate.enable = true;
      package = pkgs.poetry;
    };
  };

  scripts = {
    lint.exec = utils.shell.strict ''
      if [[ ''${CI:-} == "" ]]; then
        ruff format --config ruff.toml
        ruff check --config ruff.toml --fix
      else
        ruff format --config ruff.toml --diff
        ruff check --config ruff.toml
      fi

      mypy --config-file mypy.ini analytics
    '';
    tests.exec = utils.shell.strict "pytest -rP analytics/test";
  };

  enterTest = ''
    ${config.scripts.lint.exec}
    ${config.scripts.tests.exec}
  '';

  enterShell = ''
    . utils-aws aws_login dev 3600
    . utils-sops sops_export_vars ${inputs.secrets}/dev.yaml UNIVERSE_ANALYTICS_TOKEN
  '';
}
