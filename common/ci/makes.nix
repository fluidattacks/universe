# https://github.com/fluidattacks/makes
{ inputs, makeScript, makeSearchPaths, outputs, projectPath, ... }:
let
  infraSearchPaths = makeSearchPaths {
    bin = [
      inputs.nixpkgs.awscli
      inputs.nixpkgs.bash
      inputs.nixpkgs.git
      inputs.nixpkgs.jq
      inputs.nixpkgs.nodejs_20
    ];
  };
in {
  deployTerraform = {
    modules = {
      commonCi = {
        setup = [
          infraSearchPaths
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonCi"
          outputs."/secretsForTerraformFromEnv/commonCi"
        ];
        src = "/common/ci/infra";
        version = "1.0";
      };
    };
  };
  imports = [ ./analytics/makes.nix ];
  jobs."/common/ci/infra/module/gitlab-bot/lint" = makeScript {
    entrypoint = ''
      pushd common/ci/infra/module/gitlab-bot

      npm clean-install
      npx tsc -p tsconfig.json --noEmit
      npx eslint .
    '';
    name = "common-ci-infra-gitlab-bot-lint";
    searchPaths.bin =
      [ inputs.nixpkgs.bash inputs.nixpkgs.git inputs.nixpkgs.nodejs_20 ];
  };
  lintTerraform = {
    modules = {
      commonCi = {
        setup = [
          infraSearchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCi"
          outputs."/secretsForTerraformFromEnv/commonCi"
        ];
        src = "/common/ci/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonCi = {
      vars = [
        "GITLAB_RUNNER_TOKEN"
        "UNIVERSE_API_TOKEN"
        "FLUIDATTACKS_GITLAB_API_TOKEN"
      ];
      manifest = "/common/secrets/dev.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    commonCi = {
      gitlabApiToken = "UNIVERSE_API_TOKEN";
      gitlabRunnerToken = "GITLAB_RUNNER_TOKEN";
      fluidattacksGitlabApiToken = "FLUIDATTACKS_GITLAB_API_TOKEN";
    };
  };
  testTerraform = {
    modules = {
      commonCi = {
        setup = [
          infraSearchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCi"
          outputs."/secretsForTerraformFromEnv/commonCi"
        ];
        src = "/common/ci/infra";
        version = "1.0";
      };
    };
  };
}
