module "gitlab_bot" {
  source = "./module/gitlab-bot"

  gitlab_api_token = var.gitlabApiToken
  name             = "common-ci-infra-gitlab-bot"
  region           = var.region

  tags = {
    "Name"              = "common-ci-infra-gitlab-bot"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "common"
  }
}

output "gitlab_bot_endpoint" {
  value = module.gitlab_bot.endpoint
}
