import { Gitlab } from "@gitbeaker/rest";

const gitlab = {
  api: new Gitlab({ token: String(process.env["GITLAB_API_TOKEN"]) }),
};

export { gitlab };
