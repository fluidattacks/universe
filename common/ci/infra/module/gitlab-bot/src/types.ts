interface IBaseEvent {
  project: {
    id: number;
    path_with_namespace: string;
  };
}

type TMergeRequestEventAction =
  | "approval"
  | "approved"
  | "close"
  | "merge"
  | "open"
  | "reopen"
  | "unapproval"
  | "unapproved"
  | "update";

/**
 * {@link https://docs.gitlab.com/ee/api/merge_requests.html#merge-status}
 */
type TMergeRequestStatus =
  | "blocked_status"
  | "broken_status"
  | "checking"
  | "ci_must_pass"
  | "ci_still_running"
  | "conflict"
  | "discussions_not_resolved"
  | "draft_status"
  | "external_status_checks"
  | "jira_association_missing"
  | "mergeable"
  | "need_rebase"
  | "not_approved"
  | "not_open"
  | "policies_denied"
  | "preparing"
  | "requested_changes"
  | "unchecked";

/**
 * {@link https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#merge-request-events}
 */
interface IMergeRequestEvent extends IBaseEvent {
  object_kind: "merge_request";
  object_attributes: {
    action: TMergeRequestEventAction;
    detailed_merge_status: TMergeRequestStatus;
    iid: number;
    state: "closed" | "locked" | "merged" | "opened";
  };
}

type TWorkItemEventAction = "close" | "open" | "reopen" | "update";

/**
 * {@link https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#work-item-events}
 */
interface IWorkItemEvent extends IBaseEvent {
  changes: Record<string, { previous: string; current: string } | undefined>;
  event_type: string;
  object_kind: "issue" | "work_item";
  object_attributes: {
    action: TWorkItemEventAction;
    confidential: boolean;
    description: string | null;
    iid: number;
  };
}

type TPipelineSource = "merge_request_event" | "push";

type TPipelineStatus =
  | "canceled"
  | "created"
  | "failed"
  | "manual"
  | "pending"
  | "preparing"
  | "running"
  | "scheduled"
  | "skipped"
  | "success"
  | "waiting_for_resource";

/**
 * {@link https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#pipeline-events}
 */
interface IPipelineEvent extends IBaseEvent {
  merge_request: { iid: number } | null;
  object_kind: "pipeline";
  object_attributes: {
    id: number;
    ref: string;
    source: TPipelineSource;
    status: TPipelineStatus;
    url: string;
  };
  commit: {
    id: number;
    message: string;
  };
}

/**
 * {@link https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html}
 */
type TWebhookEvent = IMergeRequestEvent | IPipelineEvent | IWorkItemEvent;

export type {
  IMergeRequestEvent,
  IWorkItemEvent,
  IPipelineEvent,
  TWorkItemEventAction,
  TMergeRequestEventAction,
  TMergeRequestStatus,
  TPipelineSource,
  TPipelineStatus,
  TWebhookEvent,
};
