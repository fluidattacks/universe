import { gitlab } from "../gitlab";
import type { IWorkItemEvent, TWorkItemEventAction } from "../types";

async function handleWorkItemEvent(body: IWorkItemEvent): Promise<void> {
  const relevantActions: TWorkItemEventAction[] = ["open", "update"];

  if (!relevantActions.includes(body.object_attributes.action)) {
    console.log(
      "[issue-review] Ignoring event for issue",
      body.object_attributes.iid,
      "with action",
      body.object_attributes.action,
    );
    return;
  }

  const hasAttachments =
    typeof body.object_attributes.description === "string" &&
    (body.object_attributes.description.includes(
      "](https://gitlab.com/fluidattacks/universe/uploads/",
    ) ||
      body.object_attributes.description.includes('src="/uploads/'));

  if (hasAttachments && !body.object_attributes.confidential) {
    if (
      body.object_attributes.action === "update" &&
      body.changes["description"] === undefined
    ) {
      return;
    }

    await gitlab.api.Issues.edit(body.project.id, body.object_attributes.iid, {
      confidential: true,
    });
    await gitlab.api.IssueNotes.create(
      body.project.id,
      body.object_attributes.iid,
      [
        "Hi, I've noticed this issue contains attachments.",
        "To prevent accidentally exposing sensitive information,",
        "I've marked it as confidential.",
        "",
        "Not the case? You can turn it off in the right-side menu.",
      ].join("\n"),
    );
  }
}

export const issueReview = {
  handleWorkItemEvent,
};
