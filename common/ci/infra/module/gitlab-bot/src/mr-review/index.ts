import { spawnSync } from "child_process";

import { gitlab } from "../gitlab";
import type {
  IMergeRequestEvent,
  IWorkItemEvent,
  TMergeRequestEventAction,
} from "../types";

function runDanger(mergeRequestIID: string, projectPath: string): void {
  spawnSync(
    "./node_modules/danger/distribution/commands/danger.js",
    ["ci", "--dangerfile", "./mr-review/dangerfile.ts", "--verbose"],
    {
      env: {
        ...process.env,
        CI_MERGE_REQUEST_IID: mergeRequestIID,
        CI_PROJECT_PATH: projectPath,
        DANGER_GITLAB_API_TOKEN: process.env["GITLAB_API_TOKEN"],
        GITLAB_CI: "true",
      },
      stdio: "inherit",
    },
  );
}

async function handleWorkItemEvent(body: IWorkItemEvent): Promise<void> {
  if (body.object_attributes.action !== "update") {
    console.log(
      "[mr-review] Ignoring event for issue",
      body.object_attributes.iid,
      "with action",
      body.object_attributes.action,
    );
    return;
  }

  const mergeRequests = await gitlab.api.MergeRequests.all({
    projectId: body.project.id,
    state: "opened",
  });
  const relevantMergeRequest = mergeRequests.find((mr) => {
    return mr.title.includes(` #${body.object_attributes.iid} `);
  });

  if (relevantMergeRequest === undefined) {
    console.log(
      "[mr-review] No relevant MR found for issue",
      body.object_attributes.iid,
    );
    return;
  }

  runDanger(
    relevantMergeRequest.iid.toString(),
    body.project.path_with_namespace,
  );
}

function handleMergeRequestEvent(body: IMergeRequestEvent): void {
  const relevantActions: TMergeRequestEventAction[] = [
    "open",
    "reopen",
    "update",
  ];
  if (!relevantActions.includes(body.object_attributes.action)) {
    console.log(
      "[mr-review] Ignoring event for MR",
      body.object_attributes.iid,
      "with action",
      body.object_attributes.action,
    );
    return;
  }

  runDanger(
    body.object_attributes.iid.toString(),
    body.project.path_with_namespace,
  );
}

export const mrReview = {
  handleMergeRequestEvent,
  handleWorkItemEvent,
};
