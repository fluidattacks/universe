import type { ExpandedPipelineSchema, IssueSchema } from "@gitbeaker/core";
import { danger, fail, results } from "danger";

function checkAuthorSyntax(): void {
  const re = /^[A-Z][a-z]+ [A-Z][a-z]+$/u;
  const author = danger.gitlab.mr.author.name;

  if (!re.test(author)) {
    fail(
      [
        `Your GitLab user name is ${author}.`,
        "Please make sure to use the following syntax:\n",
        "Capitalized name, space and capitalized last name.",
        "(avoid accents and ñ).",
        "For example: Aureliano Buendia.\n",
        "You can change your GitLab user name here:",
        "https://gitlab.com/-/profile",
      ].join("\n"),
    );
  }
}

function checkBranchEqualsUsername(): void {
  const branch = danger.gitlab.mr.source_branch;
  const { username } = danger.gitlab.mr.author;

  if (branch !== username) {
    fail(
      [
        "Branch must be equal to Username\n",
        `Branch: ${branch}`,
        `Username: ${username}`,
      ].join("\n"),
    );
  }
}

function checkCompletedChecklist(): void {
  const checkboxes = Array.from(
    danger.gitlab.mr.description.match(/- \[[x ]\]/gu) ?? [],
  );
  const checked = checkboxes.filter((checkbox) => {
    return checkbox.toLowerCase().includes("x");
  });

  if (checkboxes.length === 0) {
    fail("Please select the correct MR template including the checklists.");
  } else if (checked.length < checkboxes.length) {
    fail("Please complete the MR checklists");
  }
}

async function checkFirstPipelineSucceeded(): Promise<void> {
  const pipelines = await danger.gitlab.api.MergeRequests.allPipelines(
    danger.gitlab.mr.project_id,
    danger.gitlab.mr.iid,
  );

  const firstPipeline = pipelines.at(-1);
  if (firstPipeline === undefined) {
    fail("This MR has no pipelines.");
    return;
  }

  const firstPipelineDetails = (await danger.gitlab.api.Pipelines.show(
    danger.gitlab.mr.project_id,
    firstPipeline.id,
  )) as ExpandedPipelineSchema;

  if (firstPipeline.status !== "success") {
    fail(
      [
        "First MR pipeline must be successful\n",
        "First pipeline:",
        firstPipelineDetails.web_url,
      ].join("\n"),
    );
  }
}

interface IIssueData {
  issueDueDate: string;
  issueLabels: string[];
}

async function getIssueLabels(issueId: number): Promise<IIssueData> {
  const issue = await danger.gitlab.api.Issues.show(issueId, {
    projectId: danger.gitlab.mr.project_id,
  });

  const issueLabels = String(issue.labels)
    .split(",")
    .map((label): string => label.split("::")[0] ?? "");

  const issueDueDate = String(issue.due_date);

  return {
    issueDueDate,
    issueLabels,
  };
}
function getIssueMilestone(issue: IssueSchema): boolean {
  return Boolean(issue.milestone);
}

async function getProjectLabels(): Promise<string[]> {
  const allLabels = await danger.gitlab.api.ProjectLabels.all(
    danger.gitlab.mr.project_id,
  );

  return Array.from(
    new Set(allLabels.map((label): string => label.name.split("::")[0] ?? "")),
  );
}
function checkIfIssueBug(issue: IssueSchema): boolean {
  const issueLabels = String(issue.labels).split(",");

  return issueLabels.includes("type::bug");
}

function isDueDateLaterThanToday(dueDate: string): boolean {
  const due = new Date(dueDate);

  if (isNaN(due.getTime())) {
    return false;
  }

  const today = new Date();

  if (
    due.getUTCFullYear() < today.getUTCFullYear() ||
    (due.getUTCFullYear() === today.getUTCFullYear() &&
      due.getUTCMonth() < today.getUTCMonth())
  ) {
    return false;
  }

  if (
    due.getUTCFullYear() === today.getUTCFullYear() &&
    due.getUTCMonth() === today.getUTCMonth() &&
    due.getUTCDate() < today.getUTCDate()
  ) {
    return false;
  }

  return true;
}

function getIssueIId(): number {
  return Number((danger.gitlab.mr.title.match(/#\d+/gu) ?? ["#0"])[0].slice(1));
}

async function checkIssueHasLabelsAndDueDate(): Promise<void> {
  const validMissing = ["request-for-comments"];
  const projectLabels = await getProjectLabels();
  const issueIId = getIssueIId();

  const { issueDueDate, issueLabels } = await getIssueLabels(issueIId);
  const missingLabels = projectLabels.filter(
    (label): boolean =>
      !issueLabels.includes(label) && !validMissing.includes(label),
  );

  if (missingLabels.length > 0) {
    fail(
      [
        "Issue associated with the MR should have all labels\n",
        "Missing labels:",
        `[${missingLabels.toString()}]\n`,
        "See all the project labels at: https://gitlab.com/fluidattacks/universe/-/labels",
      ].join("\n"),
    );
  }

  if (!isDueDateLaterThanToday(issueDueDate)) {
    fail(
      "Issue associated with the MR should have a due date later than today",
    );
  }
}
async function checkIssueHasMilestone(): Promise<void> {
  const issueIId = getIssueIId();
  const issue = await danger.gitlab.api.Issues.show(issueIId, {
    projectId: danger.gitlab.mr.project_id,
  });
  const issueIsBug = checkIfIssueBug(issue as IssueSchema);
  const issueMilestone = getIssueMilestone(issue as IssueSchema);
  if (!issueIsBug && !issueMilestone) {
    fail("Please add a milestone to the associated issue.");
  }
}

function normalizeEscapedCharacters(str: string): string {
  return str.replace(/\\\\/gu, "\\");
}

function checkMrMessageEqualsCommitMessage(): void {
  const mrMessage = `${danger.gitlab.mr.description.split("---")[0]}`;
  const commitMessage = danger.git.commits[0]?.message ?? "";
  const mrTitle = danger.gitlab.mr.title;
  const [commitTitle] = commitMessage.split("\n");
  const normalizedMrMessage = normalizeEscapedCharacters(mrMessage).trim();
  const normalizedCommitMessage =
    normalizeEscapedCharacters(commitMessage).trim();

  if (mrTitle !== commitTitle) {
    fail(
      [
        "MR title must be equal to Commit title\n",
        `MR title: ${mrTitle}`,
        `Commit title: ${commitTitle}`,
      ].join("\n"),
    );
  }

  if (normalizedMrMessage !== normalizedCommitMessage) {
    fail(
      [
        "MR message must be equal to Commit message\n",
        "MR message:",
        `${mrMessage}\n`,
        "Commit message:",
        commitMessage,
      ].join("\n"),
    );
  }
}

function checkReviewersApproved(): void {
  const approvals =
    danger.gitlab.approvals.approved_by?.map((approval) => {
      return approval.user.username;
    }) ?? [];
  const reviewers =
    danger.gitlab.mr.reviewers?.map((reviewer) => {
      return reviewer.username;
    }) ?? [];
  const missing = reviewers.filter((reviewer) => !approvals.includes(reviewer));

  if (missing.length > 0) {
    fail(`Missing approval from reviewers: ${missing.toString()}`);
  }
}

async function approve(): Promise<void> {
  await danger.gitlab.api.MergeRequestApprovals.approve(
    danger.gitlab.mr.project_id,
    danger.gitlab.mr.iid,
  ).catch((error: unknown) => {
    if (error instanceof Error && error.message === "Unauthorized") {
      console.log("Already approved, skipping...");
    } else {
      console.error(error);
    }
  });
}

async function unapprove(): Promise<void> {
  await danger.gitlab.api.MergeRequestApprovals.unapprove(
    danger.gitlab.mr.project_id,
    danger.gitlab.mr.iid,
  ).catch((error: unknown) => {
    if (
      error instanceof Error &&
      ["Not Found", "Unauthorized"].includes(error.message)
    ) {
      console.log("Already unapproved, skipping...");
    } else {
      console.error(error);
    }
  });
}

async function main(): Promise<void> {
  checkAuthorSyntax();
  checkBranchEqualsUsername();
  checkCompletedChecklist();
  checkMrMessageEqualsCommitMessage();
  checkReviewersApproved();
  await Promise.all([
    checkFirstPipelineSucceeded(),
    checkIssueHasLabelsAndDueDate(),
    checkIssueHasMilestone(),
  ]);

  if (results.fails.length === 0) {
    await approve();
  } else {
    await unapprove();
  }
}

void main();
