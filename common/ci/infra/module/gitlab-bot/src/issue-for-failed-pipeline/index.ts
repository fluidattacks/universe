import { getJobsFailedDueToStateLock } from "./webhook_alerts";

import { gitlab } from "../gitlab";
import type { IPipelineEvent } from "../types";

// Gitlab's user ID's are public, so we can use them to identify members.
const defaultAssignee = 19100235;
const productsAssignees = {
  airs: 11765016,
  common: 9108643,
  integrates: 11765016,
  observes: 4437643,
  skims: 9108643,
};

async function findIssueByExactTitle(
  projectId: number,
  issueTitle: string,
): Promise<boolean> {
  const issues = await gitlab.api.Issues.all({
    projectId,
    search: issueTitle,
    state: "opened",
  }).catch((error: unknown) => {
    console.error("Error retrieving open issues", error);
    return [{ title: issueTitle }];
  });

  const matchingIssues = issues.filter((issue) => issue.title === issueTitle);

  return matchingIssues.length > 0;
}

async function hasNoFailedJobsForIssue(
  projectId: number,
  pipelineId: number,
): Promise<boolean> {
  const jobs = await gitlab.api.Jobs.all(projectId, {
    pipelineId,
    scope: "failed",
  });

  // Runner failures do not require an issue
  const filteredJobs = jobs.filter(
    (job) => job.failure_reason !== "runner_system_failure",
  );

  if (filteredJobs.length === 0) {
    return true;
  }

  const failedDeploymentJobs = filteredJobs
    .filter((job) => job.name.includes("deployTerraform"))
    .map((job) => job.id);

  console.log("Found failed deployment jobs in pipeline", pipelineId);
  // Deployment jobs that failed for state lock are notified through google chat
  if (
    failedDeploymentJobs.length > 0 &&
    (await getJobsFailedDueToStateLock(projectId, failedDeploymentJobs))
      .length === filteredJobs.length
  ) {
    return true;
  }

  return false;
}

async function handlePipelineEvent(body: IPipelineEvent): Promise<void> {
  const [productName = "all"] = body.commit.message.split(/[-\\]/u);
  const issueTitle = `Trunk pipeline is failing for product ${productName}`;

  if (
    !(productName in productsAssignees) ||
    body.object_attributes.status !== "failed" ||
    body.object_attributes.ref !== "trunk" ||
    (await hasNoFailedJobsForIssue(
      body.project.id,
      body.object_attributes.id,
    )) ||
    (await findIssueByExactTitle(body.project.id, issueTitle))
  ) {
    console.log(
      "No need to create issue for this pipeline event",
      body.object_attributes.id,
      productName,
    );
    return;
  }

  const issueDescription = `Pipeline failure detected

Details:
- Pipeline link: ${body.object_attributes.url}
- Product name: ${productName}
  `;

  const issueLabels = [
    "arena::architecture",
    "audience::engineering",
    "invest::enhancements",
    "origin::engineering",
    "priority::high",
    `product::${productName}`,
    "roadmap::unplanned",
    "type::bug",
  ].join(",");

  const options = {
    assigneeIds: [
      defaultAssignee,
      productsAssignees[productName as keyof typeof productsAssignees],
    ],
    description: issueDescription,
    issueType: "issue",
    labels: issueLabels,
  };

  await gitlab.api.Issues.create(body.project.id, issueTitle, options).catch(
    (error: unknown) => {
      console.error("Error creating issue", error);
    },
  );

  console.log(
    "Created an issue for failed pipeline in product",
    body.object_attributes.id,
    productName,
  );
}

export const issueForFailedPipeline = {
  handlePipelineEvent,
};
