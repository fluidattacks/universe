/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable functional/no-classes */
import type {
  GitlabAPIResponse,
  MergeRequestRebaseSchema,
  ShowExpanded,
  Sudo,
} from "@gitbeaker/core";
import type { BaseResource } from "@gitbeaker/requester-utils";

import type { TMergeRequestStatus } from "../types";

declare module "@gitbeaker/core" {
  /**
   * Patch needed while danger updates gitbeaker to at least version 40.0.0
   * where {@link https://github.com/jdalrymple/gitbeaker/pull/3552} is included
   */
  declare class MergeRequests<
    C extends boolean = false,
  > extends BaseResource<C> {
    public rebase<E extends boolean = false>(
      projectId: number | string,
      mergerequestIId: number,
      options?: ShowExpanded<E> & Sudo & { skipCi?: boolean },
    ): Promise<GitlabAPIResponse<MergeRequestRebaseSchema, C, E, void>>;
  }

  /**
   * Patch needed while danger updates gitbeaker to at least version 39.26.1
   * where {@link https://github.com/jdalrymple/gitbeaker/pull/3488} is included
   */
  interface MergeRequestSchema {
    detailed_merge_status: TMergeRequestStatus;
  }
}
