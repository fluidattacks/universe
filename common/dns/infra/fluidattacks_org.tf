data "cloudflare_zone" "fluidattacks_org" {
  name = "fluidattacks.org"
}


resource "cloudflare_zone_settings_override" "fluidattacks_org" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id

  settings {
    always_online            = "on"
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    browser_check            = "on"
    cache_level              = "aggressive"
    email_obfuscation        = "on"
    hotlink_protection       = "on"
    ip_geolocation           = "on"
    ipv6                     = "on"
    opportunistic_encryption = "on"
    min_tls_version          = "1.2"
    ssl                      = "flexible"
    tls_1_3                  = "zrt"
    universal_ssl            = "on"
    challenge_ttl            = 1800

    security_header {
      enabled            = true
      include_subdomains = true
      nosniff            = false
      max_age            = 31536000
      preload            = true
    }
  }
}


resource "cloudflare_zone_dnssec" "fluidattacks_org" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id
}

# Workers
resource "cloudflare_worker_script" "mta_sts_records_org" {
  account_id = var.cloudflareAccountId
  name       = "mta_sts_records_org"
  content    = data.local_file.mta_sts_org.content
}
resource "cloudflare_worker_route" "mta_sts_records_org" {
  zone_id     = data.cloudflare_zone.fluidattacks_org.id
  pattern     = "mta-sts.${data.cloudflare_zone.fluidattacks_org.name}/*"
  script_name = cloudflare_worker_script.mta_sts_records_org.name
}

# CNAME Records

resource "cloudflare_record" "mta_sts_records_org" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id
  name    = "mta-sts.${data.cloudflare_zone.fluidattacks_org.name}"
  type    = "CNAME"
  value   = "mta_sts_records_org.fluidattacks.workers.dev"
  proxied = false
  ttl     = 1
}

# TXT Records

resource "cloudflare_record" "mta_sts_txt_record_org" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id
  name    = "_mta-sts.${data.cloudflare_zone.fluidattacks_org.name}"
  type    = "TXT"
  value   = "v=STSv1;id=172540442862Z;"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "report_diagnostic_org" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id
  name    = "_smtp._tls.${data.cloudflare_zone.fluidattacks_org.name}"
  type    = "TXT"
  value   = "TLSRPTv1;rua=mailto:smtp-tls-reports@fluidattacks.com;"
  ttl     = 1
  proxied = false
}
resource "cloudflare_record" "zoho_verify" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id
  name    = data.cloudflare_zone.fluidattacks_org.name
  type    = "TXT"
  value   = "zoho-verification=zb77477722.zmverify.zoho.com"
  ttl     = 1
  proxied = false
}
resource "cloudflare_record" "zoho_spf_record" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id
  name    = data.cloudflare_zone.fluidattacks_org.name
  type    = "TXT"
  value   = "v=spf1 include:zoho.com include: ~all"

  ttl     = 1
  proxied = false
}
resource "cloudflare_record" "zoho_dmarc" {
  zone_id = data.cloudflare_zone.fluidattacks_org.id
  name    = "_dmarc"
  type    = "TXT"
  value   = "v=DMARC1; p=reject; rua=mailto:noreply@fluidattacks.org; ruf=mailto:noreply@fluidattacks.org; sp=reject; adkim=s; aspf=s"
  proxied = false
  ttl     = 1
}

# MX Records

resource "cloudflare_record" "main_zoho_1" {
  zone_id  = data.cloudflare_zone.fluidattacks_org.id
  name     = data.cloudflare_zone.fluidattacks_org.name
  type     = "MX"
  value    = "mx.zoho.com"
  ttl      = 1
  proxied  = false
  priority = 10
}

resource "cloudflare_record" "main_zoho_2" {
  zone_id  = data.cloudflare_zone.fluidattacks_org.id
  name     = data.cloudflare_zone.fluidattacks_org.name
  type     = "MX"
  value    = "mx2.zoho.com"
  ttl      = 1
  proxied  = false
  priority = 20
}

resource "cloudflare_record" "main_zoho_3" {
  zone_id  = data.cloudflare_zone.fluidattacks_org.id
  name     = data.cloudflare_zone.fluidattacks_org.name
  type     = "MX"
  value    = "mx3.zoho.com"
  ttl      = 1
  proxied  = false
  priority = 50
}
