data "cloudflare_zone" "fluidattacks_tech" {
  account_id = var.cloudflareAccountId
  name       = "fluidattacks.tech"
}

resource "cloudflare_zone_settings_override" "fluidattacks_tech" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id

  settings {
    always_online            = "on"
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    browser_check            = "on"
    cache_level              = "aggressive"
    email_obfuscation        = "on"
    hotlink_protection       = "on"
    ip_geolocation           = "on"
    ipv6                     = "on"
    opportunistic_encryption = "on"
    min_tls_version          = "1.2"
    ssl                      = "flexible"
    tls_1_3                  = "zrt"
    universal_ssl            = "on"
    challenge_ttl            = 1800

    security_header {
      enabled            = true
      include_subdomains = true
      nosniff            = false
      max_age            = 31536000
      preload            = true
    }
  }
}

resource "cloudflare_zone_dnssec" "fluidattacks_tech" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
}

# Workers

resource "cloudflare_worker_script" "headers" {
  account_id = var.cloudflareAccountId
  name       = "makes_headers"
  content    = data.local_file.headers.content
}

resource "cloudflare_worker_route" "go_headers" {
  zone_id     = data.cloudflare_zone.fluidattacks_tech.zone_id
  pattern     = "go.${data.cloudflare_zone.fluidattacks_tech.name}/*"
  script_name = cloudflare_worker_script.headers.name
}

# CNAME Records

resource "cloudflare_record" "www_fluidattacks_tech" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "www.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = data.cloudflare_zone.fluidattacks_tech.name
  proxied = true
}

resource "cloudflare_record" "rebrandly" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "go.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "rebrandlydomain.com"
  proxied = true
  ttl     = 1
}

resource "cloudflare_record" "announcekit" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "news.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "cname.announcekit.app"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "gd_domainconnect" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "_domainconnect.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "_domainconnect.gd.domaincontrol.com"
  proxied = true
}

resource "cloudflare_record" "secureserver" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "email.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "email.secureserver.net"
  proxied = true
}

resource "cloudflare_record" "godaddy_paylink" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "pay.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "paylinks.commerce.godaddy.com"
  proxied = true
}

resource "cloudflare_record" "mailgun" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "track.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "mailgun.org"
  proxied = true
}

resource "cloudflare_record" "unbounce" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "try.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "4aeee6a9e5a245a09a937d23a15e7caf.unbouncepages.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "forms" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "forms.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "forms.cs.zohohost.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "juiceshop" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "juiceshop.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "ec2-54-211-55-139.compute-1.amazonaws.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "juiceshop-gcp" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "juiceshop-gcp.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "A"
  value   = "34.42.234.189"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "webgoat" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "webgoat.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "ec2-54-211-55-139.compute-1.amazonaws.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "webwolf" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "webwolf.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "ec2-54-211-55-139.compute-1.amazonaws.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "dvws-node" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "dvws-node.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "ec2-54-211-55-139.compute-1.amazonaws.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "crapi" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "crapi.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "ec2-54-211-55-139.compute-1.amazonaws.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "crapi-mail" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "crapi-mail.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "ec2-54-211-55-139.compute-1.amazonaws.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "trust_center" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "trust.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "641c9b99a330118d4fc1a0d2.cname.vantatrust.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "status" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "status.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "ssg1m9rm289m.stspg-customer.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "status_checkly" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "availability.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "checkly-dashboards.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "makes" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "makes.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "fluidattacks.github.io"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "events_backstage" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "events.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "backstage.cs.zohohost.com"
  proxied = false
  ttl     = 1
}
# TXT Records

resource "cloudflare_record" "makes_verify" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "_github-pages-challenge-fluidattacks.makes.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "TXT"
  value   = "36e748536f7e4601c6c7922393d6a6"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "dmarc" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "_dmarc.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "TXT"
  value   = "v=DMARC1; p=reject;"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "pic_domainkey" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "pic._domainkey"
  type    = "TXT"
  value   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWESKfHJ7mhabvLjIC149sXmbq17kxA/Zivg57SkPtleeANMKK4WbAKl1WtQHnGqSoNsQ8WqoKCPqeSyrc4Sqjg6TJQi7mSq6tNR+gCALB6wSHRoAXwAoOcWkTJTyBUvzZjYaLk4bU7OlykNbYbN6BYudZ7mhIArimrXyttGzwIDAQAB"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "juiceshop_verification" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "juiceshop.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "TXT"
  value   = "detectify-verification=e8c2784140d50eaeb5d1e877dbb21d04"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "juiceshop_verification_probely" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "juiceshop.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "TXT"
  value   = "Probely=9c66bc8c-f783-4695-8c57-c256b50d83c6"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "juiceshop_verification_astra" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "juiceshop.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "TXT"
  value   = "astra-target-verification=dccf0bd952f1aa708f499f0e9ad384a038379059574c70ebc504baababa157bb"
  ttl     = 1
  proxied = false
}


resource "cloudflare_record" "thinkific" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "training.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "CNAME"
  value   = "fluidattacksplatform.thinkific.com"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "trust_center_verify" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "_cf-custom-hostname.trust.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "TXT"
  value   = "5e43b619-a7eb-4c13-8ca8-c92fe72ff4bb"
  ttl     = 1
  proxied = false
}
resource "cloudflare_record" "status_subdomain_verification" {
  zone_id = data.cloudflare_zone.fluidattacks_tech.zone_id
  name    = "status.${data.cloudflare_zone.fluidattacks_tech.name}"
  type    = "TXT"
  value   = "status-page-domain-verification=6kpcnfxs2v7f"
  proxied = false
  ttl     = 1
}
resource "cloudflare_page_rule" "redirect_landing" {
  zone_id  = data.cloudflare_zone.fluidattacks_com.id
  target   = "landing.${data.cloudflare_zone.fluidattacks_com.name}/*"
  status   = "active"
  priority = 100

  actions {
    forwarding_url {
      url         = "https://try.${data.cloudflare_zone.fluidattacks_tech.name}/$1"
      status_code = 301
    }
  }
}
