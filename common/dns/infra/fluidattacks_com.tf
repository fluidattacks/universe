resource "cloudflare_zone" "fluidattacks_com" {
  account_id = var.cloudflareAccountId
  zone       = "fluidattacks.com"
}

resource "cloudflare_zone_settings_override" "fluidattacks_com" {
  zone_id = cloudflare_zone.fluidattacks_com.id

  settings {
    always_online            = "on"
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    browser_cache_ttl        = 1800
    browser_check            = "on"
    cache_level              = "aggressive"
    challenge_ttl            = 1800
    ciphers = [
      "ECDHE-RSA-AES128-GCM-SHA256",
      "ECDHE-RSA-CHACHA20-POLY1305",
      "ECDHE-RSA-AES256-GCM-SHA384",
      "ECDHE-ECDSA-AES128-GCM-SHA256",
      "ECDHE-RSA-AES128-GCM-SHA256",
      "ECDHE-ECDSA-AES256-GCM-SHA384",
    ]
    cname_flattening            = "flatten_at_root"
    development_mode            = "off"
    email_obfuscation           = "on"
    early_hints                 = "on"
    h2_prioritization           = "on"
    hotlink_protection          = "on"
    http2                       = "on"
    http3                       = "on"
    image_resizing              = "off"
    ip_geolocation              = "on"
    ipv6                        = "on"
    max_upload                  = 100
    min_tls_version             = "1.2"
    mirage                      = "off"
    opportunistic_encryption    = "on"
    opportunistic_onion         = "on"
    origin_error_page_pass_thru = "off"
    polish                      = "off"
    pseudo_ipv4                 = "off"
    prefetch_preload            = "off"
    privacy_pass                = "on"
    response_buffering          = "off"
    rocket_loader               = "off"
    security_level              = "medium"
    server_side_exclude         = "on"
    sort_query_string_for_cache = "off"
    ssl                         = "flexible"
    tls_1_3                     = "zrt"
    tls_client_auth             = "off"
    true_client_ip_header       = "off"
    universal_ssl               = "on"
    webp                        = "off"
    websockets                  = "on"
    zero_rtt                    = "on"

    security_header {
      enabled            = true
      preload            = true
      include_subdomains = true
      nosniff            = false
      max_age            = 31536000
    }
  }
}
resource "cloudflare_tiered_cache" "fluidattacks_com" {
  zone_id    = cloudflare_zone.fluidattacks_com.id
  cache_type = "smart"
}
resource "cloudflare_argo" "fluidattacks_com" {
  zone_id       = cloudflare_zone.fluidattacks_com.id
  smart_routing = "on"
}

resource "cloudflare_zone_dnssec" "fluidattacks_com" {
  zone_id = cloudflare_zone.fluidattacks_com.id
}
resource "cloudflare_bot_management" "fluidattacks_com" {
  zone_id                         = cloudflare_zone.fluidattacks_com.id
  enable_js                       = true
  optimize_wordpress              = false
  suppress_session_score          = false
  sbfm_likely_automated           = "allow"
  sbfm_definitely_automated       = "allow"
  sbfm_verified_bots              = "allow"
  sbfm_static_resource_protection = false
}
# Workers
resource "cloudflare_worker_script" "mta_sts_records" {
  account_id = var.cloudflareAccountId
  name       = "mta_sts_records"
  content    = data.local_file.mta_sts.content
}
resource "cloudflare_worker_route" "mta_sts_records" {
  zone_id     = cloudflare_zone.fluidattacks_com.id
  pattern     = "mta-sts.${cloudflare_zone.fluidattacks_com.zone}/*"
  script_name = cloudflare_worker_script.mta_sts_records.name
}
resource "cloudflare_worker_script" "redirect_worker" {
  account_id = var.cloudflareAccountId
  name       = "redirect-foss"
  content    = data.local_file.doc_redirect.content
}
resource "cloudflare_worker_route" "redirect_route" {
  zone_id     = cloudflare_zone.fluidattacks_com.id
  pattern     = "docs.${cloudflare_zone.fluidattacks_com.zone}/*"
  script_name = cloudflare_worker_script.redirect_worker.name
}
# CNAME Records
resource "cloudflare_record" "mta_sts_records" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "mta-sts.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "mta_sts_records.fluidattacks.workers.dev"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "dev" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "dev.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "fluidattacks.gitlab.io."
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "lab" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "lab.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "fluidattacks.gitlab.io."
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "www" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "www.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = cloudflare_zone.fluidattacks_com.zone
  proxied = true
  ttl     = 1
}

resource "cloudflare_record" "landing" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "landing.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "try.${cloudflare_zone.fluidattacks_com.zone}"
  proxied = true
  ttl     = 1
}

resource "cloudflare_record" "try" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "try.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "3e5c7bed0ee948b1af14bbc3dd692011.unbouncepages.com"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "zoho_verify_directory" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "zb62268970.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "zmverify.zoho.com"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "mail" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "mail.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "ghs.googlehosted.com"
  proxied = true
  ttl     = 1
}


resource "cloudflare_record" "status_email_1" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "spg._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "spg.domainkey.u23920825.wl076.sendgrid.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "status_email_2" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "spg2._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "spg2.domainkey.u23920825.wl076.sendgrid.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "status_email_3" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "statuspage-notifications.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "u23920825.wl076.sendgrid.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "status_checkly_com" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "availability.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "availability.${data.cloudflare_zone.fluidattacks_tech.name}"
  proxied = true
  ttl     = 1
}

resource "cloudflare_record" "stripe_bounce" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "bounce.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "custom-email-domain.stripe.com"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "news" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "news.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "news.${data.cloudflare_zone.fluidattacks_tech.name}"
  proxied = true
  ttl     = 1
}

resource "cloudflare_record" "help_com" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "help.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "desk.cs.zohohost.com"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "stripe_dkim" {
  for_each = toset([
    "dyh5er647prfc3gs4euuquiva6xt6ibs",
    "267uahsbpj2i5rih3zpkbiuojg7rs6db",
    "w6y3bzgl5l4n3ngwdikziuawrataai56",
    "yhvbslrtgegr2wwcwp6vjdhbkgv2kouk",
    "zhdj3ymxnqzcknajrmaoxxisxkqhsgph",
    "nfnr3mqbqtdijdfekbxwm5zc4f7guhts",
  ])

  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "${each.key}._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "${each.key}.dkim.custom-email-domain.stripe.com"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "zcmp" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "zcmp.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "843401174.send.zcsend.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "sendgrid" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "em9609.fluidattacks.com"
  type    = "CNAME"
  value   = "u42278721.wl222.sendgrid.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "sendgrid_domainkey_1" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "s1._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "s1.domainkey.u42278721.wl222.sendgrid.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "sendgrid_domainkey_2" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "s2._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "s2.domainkey.u42278721.wl222.sendgrid.net"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "learn" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "learn.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "9abaeb2e1f2dfb80c2748ca8ec34c7bc.learn.cs.zohohost.com"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "trust_center_com" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "trust.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "trust.${data.cloudflare_zone.fluidattacks_tech.name}"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "status_com" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "status.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "status.${data.cloudflare_zone.fluidattacks_tech.name}"
  proxied = true
  ttl     = 1
}
resource "cloudflare_record" "training_com" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "training.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "training.${data.cloudflare_zone.fluidattacks_tech.name}"
  proxied = true
  ttl     = 1
}
resource "cloudflare_record" "docs" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "docs.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = data.cloudflare_zone.fluidattacks_com.name
  proxied = true
  ttl     = 1
}
# MX Records

resource "cloudflare_record" "gmail_1" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  name     = cloudflare_zone.fluidattacks_com.zone
  type     = "MX"
  value    = "aspmx.l.google.com"
  ttl      = 1
  proxied  = false
  priority = 1
}

resource "cloudflare_record" "gmail_2" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  name     = cloudflare_zone.fluidattacks_com.zone
  type     = "MX"
  value    = "alt1.aspmx.l.google.com"
  ttl      = 1
  proxied  = false
  priority = 5
}

resource "cloudflare_record" "gmail_3" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  name     = cloudflare_zone.fluidattacks_com.zone
  type     = "MX"
  value    = "alt2.aspmx.l.google.com"
  ttl      = 1
  proxied  = false
  priority = 5
}

resource "cloudflare_record" "gmail_4" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  name     = cloudflare_zone.fluidattacks_com.zone
  type     = "MX"
  value    = "aspmx2.googlemail.com"
  ttl      = 1
  proxied  = false
  priority = 10
}

resource "cloudflare_record" "gmail_5" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  name     = cloudflare_zone.fluidattacks_com.zone
  type     = "MX"
  value    = "aspmx3.googlemail.com"
  ttl      = 1
  proxied  = false
  priority = 10
}

# TXT Records
resource "cloudflare_record" "status_domain_verification" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "TXT"
  value   = "status-page-domain-verification=6kpcnfxs2v7f"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "dev_verification" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "_gitlab-pages-verification-code.dev.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "gitlab-pages-verification-code=804a0aa7d41b6a40e5a77d2bb1fc22a2"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "lab_verification" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "_gitlab-pages-verification-code.lab.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "gitlab-pages-verification-code=f4833dc1a9325c2b83a1a6f8ad808c9f"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "status_email_dkim" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "krs._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDE43uN5a+xOycKvS5mscryes/Lc8c2Z7d+YiiyiGuAWe2fxfVKHjlq5XDaBZqZCd86m6deX3MjOkZcREOUg3Et+dbMPc82tvHH1MN8vMH+YBQy98AcKNJSYFKIWxIjTanH2vH26hjfb20a6+nTFMPa/DTuo6XGIX5qchXPLkxhwIDAQAB"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "pic_dkim" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "pic._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+1T1FFxUSsB4ZmrqgXHwWydxZSan8i6KmhRNgQSLDnNnLXGHM0BgjHrGxTF8kd5ULNorWAS23c4MsJUIh92e3uuP+MFcS6iABRhOogWIPU6S03cMJPh7zPetHhFeFHSZyE6H8EnpW7Unnu0N2upjlKdhU6aNw7ReGlkSUI9navwIDAQAB"
  proxied = false
  ttl     = 1
}
resource "cloudflare_record" "spf_allowed" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "TXT"
  value   = "v=spf1 include:emsd1.com include:_spf.google.com include:servers.mcsv.net include:transmail.net include:stspg-customer.com include:zcsend.net -all"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "verify_zendesk" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "zendeskverification.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "27f6e2e3b646cce6"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "verify_google_1" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "TXT"
  value   = "google-site-verification=SK6CMgAtuuw7tR6eCev6XY8D6rjn9BW8AGd5KWS1b5g"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "mta_sts_txt_record" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "_mta-sts.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "v=STSv1;id=1676413653341;"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "report_diagnostic" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "_smtp._tls.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "TLSRPTv1;rua=mailto:smtp-tls-reports@fluidattacks.com;"
  ttl     = 1
  proxied = false
}


resource "cloudflare_record" "mail_dmarc" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "_dmarc.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "v=DMARC1; p=reject;"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "mail_dkim_google" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "google._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoVfDxzz1BbwFFyeQvKe7B4YMSR1HWmjCu4PQzESyAAc9XQDSbtoYQNCHaHisTJNgh4OGEWvgRcpsVljffC5jO3tHcra8xW8ls5O16sClQtfitcKhC1VxNbqYoAnUSNv9FBcsldK96jQgeMrsZUMo6SdldCDOkX7vOjgLzDw6dOMAENSoU3NsMfRwoDaanCf2gkFb+5mOtDUZCHukM5rpj+ePc3GJAzX8bakMdWD7BlZnPT0fRVcSQGOAM1GVcSDYR465hdBkADJg3KM2TdPTC/XLwEQXgqRZXVWMtSu/Rb/DcHILZNmzKxUk/B4eKjXGQDbs9hshgsqsZGYEbhOvrwIDAQAB"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "mail_dkim_domainkey" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "dk._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhkOo8s6fh9Byz1uy69tfQ6eUnzi/5P22EWccwI1PdmCpiyNwZcq3vOS2MHbVYB+ZY6wbBlAFym8EHbZY9OTlJ3+dzt8qTUNW5olkNVl4ecDv3XO2ML8q5sxQL+dwQU6UAQiDAAC/ZRWwiXHrSsr90pqH1Q0vhB7Kp6DHrWYJquQIDAQAB"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "zoho_verify_dkim" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "1522905413783._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCr6KMgdxxgg7oT3ulMwPJs9RXgXDrI9UWU118pHEMohl3UbL3Jwp4oxp/9N3thh/3WCJnYV134zbEVolZwqaT3JsFEq/mQ/RpW/JnOZ3rnxqJPurb2bcfJol4SDxiWVObzHX31xnANzFcXnq1/5dMK5QvW4Jh7n0fm4+4ywqiy2QIDAQAB"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "stripe_verify" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "TXT"
  value   = "stripe-verification=88c1426add983346960b78687bad1c70e9c9f6733116c8be02c1a7aa5139ceef"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "asana" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "TXT"
  value   = "asv=1911d62bb63361b54caabbf670e14627"
  ttl     = 86400
  proxied = false
}

resource "cloudflare_record" "dkim_active" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "atlassian-5b50a1._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "atlassian-5b50a1.dkim.atlassian.net"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "dkim_fallback" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "atlassian-3abedb._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "atlassian-3abedb.dkim.atlassian.net"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "zcsend_dkim" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "20735._domainkey.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZTSINKNZ0E6BltNNGexLyzKr0gU+BF2cZGGcGll9FcTUg2NwSBZ1ZY/mWHaoFaF9qUChu2hRHGrCXKNeZjex7eyNiDY8P8LNkJyYsztMyJ1NSlFOpd4jkWnigKMD+vnihE0ACIU67lj5HEwxno7/66poh9iOu8a1zznI1rhI/oQIDAQAB"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "cname_bounce" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "atlassian-bounces.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "CNAME"
  value   = "bounces.mail-us.atlassian.net"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "txt_verify" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "TXT"
  value   = "atlassian-sending-domain-verification=73e9db4b-86d9-4dfb-b64e-1cd7af6bf6e6"
  ttl     = 1
  proxied = false
}
resource "cloudflare_record" "gitlab_verify" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "_gitlab-pages-verification-code.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "gitlab-pages-verification-code=782bd06748639d96f5b7e3ec8da1396e"
  ttl     = 1
  proxied = false
}
resource "cloudflare_record" "twilio_verify" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "TXT"
  value   = "twilio-domain-verification=2a7421aeba40be6bc580e6f4d58d0f78"
  ttl     = 1
  proxied = false
}
resource "cloudflare_record" "vscode_marketplace_verify" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = "_visual-studio-marketplace-fluidattacks.${cloudflare_zone.fluidattacks_com.zone}"
  type    = "TXT"
  value   = "045ddd0e-ad51-4169-b1f5-90e65d9d54e5"
  ttl     = 1
  proxied = false
}
# CAA Records

resource "cloudflare_record" "gts_caa" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "CAA"
  data {
    tag   = "issue"
    value = "pki.goog"
    flags = "0"
  }
  ttl     = 3600
  proxied = false
}

resource "cloudflare_record" "amazon_caa" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "CAA"
  data {
    tag   = "issue"
    value = "amazon.com"
    flags = "0"
  }
  ttl     = 3600
  proxied = false
}

resource "cloudflare_record" "digicert_caa" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "CAA"
  data {
    tag   = "issue"
    value = "digicert.com"
    flags = "0"
  }
  ttl     = 3600
  proxied = false
}

resource "cloudflare_record" "letsencrypt_caa" {
  zone_id = cloudflare_zone.fluidattacks_com.id
  name    = cloudflare_zone.fluidattacks_com.zone
  type    = "CAA"
  data {
    tag   = "issue"
    value = "letsencrypt.org"
    flags = "0"
  }
  ttl     = 3600
  proxied = false
}

# Page Rules

resource "cloudflare_page_rule" "redirect_www" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  target   = "www.${cloudflare_zone.fluidattacks_com.zone}/*"
  status   = "active"
  priority = 100

  actions {
    forwarding_url {
      url         = "https://${cloudflare_zone.fluidattacks_com.zone}/$1"
      status_code = 301
    }
  }
}

resource "cloudflare_page_rule" "redirect_news" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  target   = "news.${cloudflare_zone.fluidattacks_com.zone}/*"
  status   = "active"
  priority = 100

  actions {
    forwarding_url {
      url         = "https://news.${data.cloudflare_zone.fluidattacks_tech.name}/$1"
      status_code = 301
    }
  }
}
resource "cloudflare_page_rule" "redirect_trust" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  target   = "trust.${cloudflare_zone.fluidattacks_com.zone}/*"
  status   = "active"
  priority = 100

  actions {
    forwarding_url {
      url         = "https://trust.${data.cloudflare_zone.fluidattacks_tech.name}/$1"
      status_code = 301
    }
  }
}
resource "cloudflare_page_rule" "redirect_status" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  target   = "status.${cloudflare_zone.fluidattacks_com.zone}/*"
  status   = "active"
  priority = 100

  actions {
    forwarding_url {
      url         = "https://status.${data.cloudflare_zone.fluidattacks_tech.name}/$1"
      status_code = 301
    }
  }
}
resource "cloudflare_page_rule" "training_status" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  target   = "training.${cloudflare_zone.fluidattacks_com.zone}/*"
  status   = "active"
  priority = 100

  actions {
    forwarding_url {
      url         = "https://training.${data.cloudflare_zone.fluidattacks_tech.name}/$1"
      status_code = 301
    }
  }
}
resource "cloudflare_page_rule" "redirect_availability" {
  zone_id  = cloudflare_zone.fluidattacks_com.id
  target   = "availability.${cloudflare_zone.fluidattacks_com.zone}/*"
  status   = "active"
  priority = 100

  actions {
    forwarding_url {
      url         = "https://availability.${data.cloudflare_zone.fluidattacks_tech.name}/$1"
      status_code = 301
    }
  }
}

# Certificates
resource "cloudflare_certificate_pack" "main" {
  zone_id               = cloudflare_zone.fluidattacks_com.id
  type                  = "advanced"
  validation_method     = "txt"
  validity_days         = 14
  certificate_authority = "google"
  cloudflare_branding   = false

  hosts = [
    cloudflare_zone.fluidattacks_com.zone,
    "*.${cloudflare_zone.fluidattacks_com.zone}",
    "*.app.${cloudflare_zone.fluidattacks_com.zone}",
    "integrates.front.${cloudflare_zone.fluidattacks_com.zone}",
    "web.eph.${cloudflare_zone.fluidattacks_com.zone}",
  ]

  lifecycle {
    create_before_destroy = true
  }
}
# Configure a ruleset at the zone level for the "http_request_firewall_managed" phase
resource "cloudflare_ruleset" "zone_level_managed_waf" {
  zone_id     = cloudflare_zone.fluidattacks_com.id
  name        = "Managed WAF entry point ruleset"
  description = "Zone-level WAF Managed Rules config"
  kind        = "zone"
  phase       = "http_request_firewall_managed"

  # Execute Cloudflare Managed Ruleset
  rules {
    action = "execute"
    action_parameters {
      id      = "efb7b8c949ac4650a09736fc376e9aee"
      version = "latest"
      overrides {
        #DotNetNuke - File Inclusion - CVE:CVE-2018-9126, CVE:CVE-2011-1892, CVE:CVE-2022-31474
        rules {
          id      = "e35c9a670b864a3ba0203ffb1bc977d1"
          enabled = false
        }
        #DotNetNuke - File Inclusion - CVE:CVE-2018-9126, CVE:CVE-2011-1892 2
        rules {
          id      = "e7e4b386797e417c998d872956c390a1"
          enabled = false
        }
        #Anomaly:Header:X-Forwarded-For
        rules {
          id      = "609b158b7e0f4d67ba7cde1d4d4a06fe"
          enabled = false

        }
        #XSS, HTML Injection - Script Tag
        rules {
          id      = "9c8dda9708cc4452ac76e7be7b58420b"
          action  = "log"
          enabled = true
        }
        categories {
          category = "drupal"
          enabled  = false
        }
        categories {
          category = "joomla"
          enabled  = false
        }
        categories {
          category = "magento"
          enabled  = false
        }
        categories {
          category = "php"
          enabled  = false
        }
        categories {
          category = "plone"
          enabled  = false
        }
        categories {
          category = "wordpress"
          enabled  = false
        }
      }
    }
    expression  = "true"
    description = "Execute Cloudflare Managed Ruleset on my zone-level phase entry point ruleset"
    enabled     = true
  }
  # Execute Cloudflare OWASP Core Ruleset
  rules {
    action = "execute"
    action_parameters {
      id = "4814384a9e5d4991b9815dcfc25d2f1f"
      overrides {
        # By default, all PL1 to PL4 rules are enabled.
        # Set the paranoia level to PL2 by disabling rules with
        # tags "paranoia-level-3" and "paranoia-level-4".
        categories {
          category = "paranoia-level-3"
          enabled  = false
        }
        categories {
          category = "paranoia-level-4"
          enabled  = false
        }
        rules {
          id              = "6179ae15870a4bb7b2d480d4843b323c"
          action          = "log"
          score_threshold = 25
        }
      }
    }
    expression  = "true"
    description = "zone"
    enabled     = true
  }
}

resource "cloudflare_ruleset" "account_firewall_custom_ruleset" {
  zone_id     = cloudflare_zone.fluidattacks_com.id
  name        = "Custom HTTP Request Firewall Ruleset"
  description = "Provides customized protection for the http_request_firewall_custom phase with a set of rules."
  kind        = "zone"
  phase       = "http_request_firewall_custom"

  rules {
    action      = "block"
    expression  = "(http.host eq \"${cloudflare_zone.fluidattacks_com.zone}\" and http.user_agent contains \"Bytespider\")"
    description = "Block spider agent"
    enabled     = true
  }
}
